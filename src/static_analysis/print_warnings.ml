let pp_info ppf number =
  Format.fprintf ppf "%s%s%sInfo %d%s" Factori_utils.enable_underline
    Factori_utils.enable_bold Factori_utils.enable_yellow number
    Factori_utils.reset_format

let pp_warning ppf number =
  Format.fprintf ppf "%s%s%sWarning %d%s" Factori_utils.enable_underline
    Factori_utils.enable_bold Factori_utils.enable_yellow number
    Factori_utils.reset_format

let pp_description ppf number =
  Format.fprintf ppf "@.@[<2>%sTips%s: %s@]" Factori_utils.enable_underline
    Factori_utils.reset_format (description number)

module Permission = Inter_procedural.Make (Perm_test)

let report_lint_warning ppf (lint_warning, occurence) =
  let number = number lint_warning in
  match lint_warning with
  | MultipleTypeForAnnotation { location; origin; annotation; expected; actual }
    ->
    Format.fprintf ppf
      "%a@.@[<2>%a: %a, annotation %S was defined like a %a, but %a the same \
       annotation is of type %a@] %a@."
      Micheline_parser.print_location origin pp_warning number
      Micheline_parser.print_location origin annotation
      Micheline_printer.print_expr expected Micheline_parser.print_location
      location Micheline_printer.print_expr actual pp_description number
  | MissingAnnotation { location; on } ->
    Format.fprintf ppf
      "%a@.@[<1>%a: there is no annotation on type %a (%d different \
       location(s))@] %a@."
      Micheline_parser.print_location location pp_warning number
      Micheline_printer.print_expr on occurence pp_description number
  | ForbiddenAnnotation { location; annotation; actual = _ } ->
    Format.fprintf ppf "%a@.@[<2>%a: %S is a forbidden annotation@] %a@."
      Micheline_parser.print_location location pp_warning number annotation
      pp_description number
  | WrongOrder (None, expected, actual) ->
    Format.fprintf ppf
      "@.@[<2>%a: the first section of a contract was expected to be the %s \
       but got the %s section@] %a@."
      pp_warning number expected actual pp_description number
  | WrongOrder (Some before, expected, actual) ->
    Format.fprintf ppf
      "@.@[<2>%a: after the %s section, the %s was expected but got the %s \
       section@] %a@."
      pp_warning number before expected actual pp_description number
  | AbsCheck ->
    Format.fprintf ppf "@.@[<2>%a: Abs is present %d time(s)@] %a@." pp_warning
      number occurence pp_description number
  | StorageCheck { ty; annot; nb } ->
    let annot =
      match annot with
      | None -> ""
      | Some a -> a in
    Format.fprintf ppf "@.@[<2>%a: StorageCheck: %s %S used %d time(s)@] %a@."
      pp_warning number ty annot nb pp_description number
  | ReentrancyCheck ->
    Format.fprintf ppf
      "@.@[<2>%a: There are %d \"CALL_CONTRACT\"\" in this contract, check for \
       reentrancy@] %a@."
      pp_warning number occurence pp_description number
  | UnusedFieldCheck { var_name; code } ->
    Format.fprintf ppf "@.@[<2>%a: Unused field: %S (%S)@] %a@." pp_warning
      number var_name code pp_description number
  | PermissionCheck { entrypoint; parameter; result } ->
    Format.fprintf ppf
      "@.@[<2>%a: Permission for entrypoint: %s%s%s@. @[<2>Parameter: \
       @[<2>%s@]@]@. Result %S @] %a@."
      pp_info number Factori_utils.enable_italic entrypoint
      Factori_utils.reset_format parameter
      (Perm_test.print_result ~verbose:false ~nested_printer)
      result pp_description number

let report_warnings kt1_or_michelson ppf warnings =
  Warnings.iter
    (fun w occ ->
      let ty, name =
        match kt1_or_michelson with
        | MichelsonFile f -> ("File", f)
        | KT1 k -> ("Contract", k) in
      Format.fprintf ppf "%s%s %S, %a%s@." Factori_utils.enable_bold ty name
        report_lint_warning (w, occ) Factori_utils.reset_format)
    warnings
