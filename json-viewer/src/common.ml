open Kt1s_tests_types

module Step = struct
  include Step

  type nonrec t = t =
    | Import
    | Compilation
    | Test
  [@@deriving jsoo]

  let all = [Import; Compilation; Test]

  let pretty = function
    | Import -> "import"
    | Compilation -> "compilation"
    | Test -> "test"

  let pp ppf step = Format.pp_print_string ppf @@ pretty step

  let icon = function
    | Import -> "bi-download"
    | Compilation -> "bi-gear"
    | Test -> "bi-clipboard2-check"
end

module TestLog = struct
  let make dir kt1 test_name =
    Format.sprintf "runs/%s/logs/%s_%s.html" dir kt1 test_name
end

module Test = struct
  include Test

  type nonrec t = t =
    | OCaml
    | Typescript
    | Python
    | Csharp
    | Analyze
    | Lint
  [@@deriving jsoo]

  let all = [OCaml; Typescript; Python; Csharp; Analyze; Lint]

  let pretty = function
    | OCaml -> "OCaml"
    | Typescript -> "Typescript"
    | Python -> "Python"
    | Csharp -> "C#"
    | Analyze -> "Analyze"
    | Lint -> "Lint"

  let pp ppf step = Format.pp_print_string ppf @@ pretty step

  let name = function
    | OCaml -> "ocaml"
    | Typescript -> "typescript"
    | Python -> "python"
    | Csharp -> "csharp"
    | Analyze -> "analyze"
    | Lint -> "lint"

  let steps =
    let open Step in
    function
    | OCaml -> [Import; Compilation; Test]
    | Typescript -> [Import; Compilation; Test]
    | Python -> [Import; Test]
    | Csharp -> [Import; Compilation; Test]
    | Analyze -> [Test]
    | Lint -> [Test]

  let test_file date kt1 test =
    match test with
    | OCaml | Typescript | Python | Csharp ->
      Some (TestLog.make date kt1 (name test ^ "_test"))
    | Analyze | Lint -> None

  let log_file date kt1 test = TestLog.make date kt1 @@ name test
end

module TestMap = struct
  include TestMap

  type 'a binding = Test.t * 'a [@@deriving jsoo]

  class type ['a] jsoo = ['a binding_jsoo Ezjs_min.t] Ezjs_min.js_array

  let to_jsoo conv tests =
    Ezjs_min.of_listf (binding_to_jsoo conv) (bindings tests)

  let of_jsoo conv tests =
    List.fold_left
      (fun tests (test, v) -> add test v tests)
      empty
      (Ezjs_min.to_listf (binding_of_jsoo conv) tests)

  let conv = (to_jsoo, of_jsoo)

  let pp pp_value ppf tests =
    let pp_binding ppf (test, value) =
      Format.fprintf ppf "Test %a: %a" Test.pp test pp_value value in
    Format.pp_print_list ~pp_sep:Format.pp_print_newline pp_binding ppf
      (bindings tests)
end

module Result = struct
  include Result

  type nonrec t = t =
    | Failed of int
    | Succeeded
    | Passed
  [@@deriving jsoo]

  let has_timeout = function
    | Failed 143 -> true
    | _ -> false

  let has_failed = function
    | Failed _ -> true
    | _ -> false

  let has_succeeded = function
    | Succeeded -> true
    | _ -> false
end

module Time = struct
  include Time

  type t = float [@@deriving jsoo]

  let pp ppf t = Format.fprintf ppf "%is" (Float.to_int t)
end

module Results = struct
  type nonrec step = step = {
    kind : Step.t;
    time : Time.t;
    result : Result.t;
  }
  [@@deriving jsoo]

  type t = test_results = {
    final_result : Result.t;
    steps : step list;
  }
  [@@deriving jsoo]

  let pp_step ppf { kind; time; result } =
    Format.fprintf ppf "%a %a in %a" Step.pp kind Result.pp result Time.pp time

  let pp ppf { final_result; steps } =
    let pp_steps =
      Format.pp_print_list ~pp_sep:Format.pp_print_newline pp_step in
    Format.fprintf ppf "%a\nFinal result: %a" pp_steps steps Result.pp
      final_result

  let has_timeout { final_result; _ } = Result.has_timeout final_result

  let has_failed { final_result; _ } = Result.has_failed final_result

  let has_succeeded { final_result; _ } = Result.has_succeeded final_result
end

module Kt1Entry = struct
  include Kt1Entry

  type nonrec t = t = {
    hash : string;
    kt1 : string;
    occurence : int;
    network : string;
  }
  [@@deriving jsoo]

  let pp ppf { hash; kt1; occurence; network } =
    Format.fprintf ppf "Hash %s\nKt1 %s\nOccurence %i\nNetwork %s" hash kt1
      occurence network
end

module Kt1EntryMap = struct
  include Kt1EntryMap

  type 'a binding = Kt1Entry.t * 'a [@@deriving jsoo]

  class type ['a] jsoo = ['a binding_jsoo Ezjs_min.t] Ezjs_min.js_array

  let to_jsoo conv kt1s =
    Ezjs_min.of_listf (binding_to_jsoo conv) (bindings kt1s)

  let to_sorted_jsoo ~compare conv kt1s =
    let bindings = bindings kt1s in
    let bindings = List.sort compare bindings in
    Ezjs_min.of_listf (binding_to_jsoo conv) bindings

  let of_jsoo conv kt1s =
    List.fold_left
      (fun kt1s (kt1, v) -> add kt1 v kt1s)
      empty
      (Ezjs_min.to_listf (binding_of_jsoo conv) kt1s)

  let conv = (to_jsoo, of_jsoo)

  let pp pp_value ppf kt1s =
    let pp_binding ppf (kt1, value) =
      Format.fprintf ppf "Kt1_entry %a: %a" Kt1Entry.pp kt1 pp_value value in
    Format.pp_print_list ~pp_sep:Format.pp_print_newline pp_binding ppf
      (bindings kt1s)
end

module Outputs = struct
  include Outputs

  type results = Results.t TestMap.t [@@deriving jsoo]

  type output = results Kt1EntryMap.binding [@@deriving jsoo]

  type t = results Kt1EntryMap.t [@@deriving jsoo]

  let to_sorted_jsoo = Kt1EntryMap.to_sorted_jsoo results_jsoo_conv

  let pp ppf =
    Format.fprintf ppf "%a" @@ Kt1EntryMap.pp @@ TestMap.pp Results.pp

  let get dir =
    Request.get ~__FUNCTION__ (Format.sprintf "runs/%s/result.json" dir) enc
end

module Version = struct
  include Version

  type nonrec t = t = {
    version : string;
    commit : string;
  }
  [@@deriving jsoo]
end

module Synopsis = struct
  include Synopsis

  type nonrec t = t = {
    version : Version.t;
    nb_runs : int;
    nb_processed : int;
    nb_succeeded_tests : int TestMap.t; [@map Test.enc]
  }
  [@@deriving jsoo]

  let pp ppf { version; nb_runs; nb_processed; nb_succeeded_tests } =
    Format.fprintf ppf
      "Version %a\nNb_runs %i\nNb_processed %i\nNb_success:\n%a@." Version.pp
      version nb_runs nb_processed
      (TestMap.pp Format.pp_print_int)
      nb_succeeded_tests

  let get dir =
    let get () =
      Request.get ~__FUNCTION__ (Format.sprintf "runs/%s/synopsis.json" dir) enc
    in
    let default _ =
      let version = Version.{ version = "version"; commit = "commit" } in
      Lwt.return
        {
          version;
          nb_runs = 0;
          nb_processed = 0;
          nb_succeeded_tests = TestMap.empty;
        } in
    Lwt.catch get default
end

module Date = struct
  (* yyyy-mm-dd *)
  type raw = string [@@deriving encoding, jsoo]

  type t = {
    year : int;
    month : int;
    day : int;
  }
  [@@deriving jsoo]

  let to_raw { year; month; day } = Format.sprintf "%i-%i-%i" year month day

  let of_raw raw =
    try
      Scanf.sscanf raw "%i-%i-%i" @@ fun year month day -> { year; month; day }
    with _ ->
      let error =
        Format.sprintf "Wrong format: expected format (yyyy-mm-dd), receive %s"
          raw in
      raise @@ Json_encoding.Cannot_destruct ([], Failure error)

  let enc = Json_encoding.conv to_raw of_raw raw_enc

  let pp ppf { year; month; day } =
    Format.fprintf ppf "%02i-%02i-%i" day month year

  let to_string = Format.asprintf "%a" pp

  let compare d1 d2 =
    match Int.compare d1.year d2.year with
    | 0 -> begin
      match Int.compare d1.month d2.month with
      | 0 -> Int.compare d1.day d2.day
      | c -> c
    end
    | c -> c
end

module Index = struct
  type t = {
    date : Date.t;
    name : string;
  }
  [@@deriving encoding, jsoo]

  let pp ppf { date; name } =
    Format.fprintf ppf "Name:%s\nDate:\n%a" name Date.pp date

  let get () =
    Request.get ~__FUNCTION__ "runs/index.json" Json_encoding.(list enc)

  let compare i1 i2 = Date.compare i2.date i1.date
end

module Run = struct
  type t = {
    index : Index.t;
    synopsis : Synopsis.t;
  }
  [@@deriving jsoo]

  let pp ppf { index; synopsis } =
    Format.fprintf ppf "Index:%a\nSynopsis:\n%a" Index.pp index Synopsis.pp
      synopsis

  let get index =
    let synopsis = Synopsis.get index.Index.name in
    Lwt.bind synopsis @@ fun synopsis -> Lwt.return { index; synopsis }

  let compare r1 r2 = Index.compare r1.index r2.index

  let number_of_test_success ~test { synopsis; _ } =
    Option.value ~default:0
    @@ TestMap.find_opt test synopsis.Synopsis.nb_succeeded_tests
end
