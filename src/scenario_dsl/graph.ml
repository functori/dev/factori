(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `Graph` module in Factori provides functions for linearizing scenarios
with forks using topological sort. It contains a graph data type represented as
a list of pairs, an exception `CycleFound` raised when a cycle is found in the
graph, and two functions: `dfs` for performing depth-first search and `toposort`
for performing topological sort on a graph. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

(* TODO: unify this with infer_entrypoints (maybe use tsort for
   topological sorting too *)
(* The Graph module is used to linearize scenarios with forks, using
   the topological sort *)
exception CycleFound

type 'a graph = ('a * 'a list) list

let dfs graph visited start_node =
  let rec explore path visited node =
    if List.mem node path then
      raise CycleFound
    else if List.mem node visited then
      visited
    else
      let new_path = node :: path in
      let edges =
        match List.assoc_opt node graph with
        | None -> []
        | Some l -> l in
      let visited = List.fold_left (explore new_path) visited edges in
      node :: visited in
  explore [] visited start_node

let toposort graph =
  List.fold_left (fun visited (node, _) -> dfs graph visited node) [] graph
