
from importlib.metadata import metadata
import oven_factory_python_interface
import blockchain

def exec():
    storage = oven_factory_python_interface._storage_generator()
    storage['state'] = 0
    kt1 = oven_factory_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= storage,
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 oven_factory " + str(kt1) + " sent to a node.")
    return kt1
