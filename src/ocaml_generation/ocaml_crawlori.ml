(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains utilities for crawling the TzFunctori blockchain
and generating OCaml code from the contracts found in the blockchain. The code
is divided into three main sections: `converters`, `crawler`, and `bm_utils`,
and there are various utility functions and variables used by these modules.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

let converters =
  {|
(
 (
  (Or
   (
    (Rule (typnam zarith))
    (Rule (colnam token_id))
    (Rule (colnam ledger_id))
    (Rule (colnam token_metadata_id))
    (Rule (colnam metadata_id))
    (Rule (colnam balance))
    (Rule (colnam diff))
    (Rule (colnam amount))
    (Rule (argnam token_id))
    (Rule (argnam metadata_id))
    )
   )
  (
   (serialize Z.to_string)
   (deserialize Z.of_string)
  )
 )
 (
  (Or
   (
    (Rule (typnam jsonb))
    (Rule (colnam license))
    (Rule (colnam source_code))
    (Rule (colnam errors))
    (Rule (colnam views))
    (Rule (colnam metadata))
    (Rule (colnam royalties))
    (Rule (colnam value))
    (Rule (colnam input))
    (Rule (colnam updates))
    (Rule (colnam creators))
    (Rule (colnam formats))
    (Rule (colnam attributes))
    (Rule (colnam royalties))
    (Rule (argnam metadata))
    (Rule (argnam input))
    (Rule (argnam updates))
    )
   )
  (
   (serialize json_to_string)
   (deserialize json_of_string)
  )
 )
)
|}

let crawler =
  {|open Crawlori
open Crp
open Common

let filename = ref None

let dummy_extra_config = object
  method port = None
  method node = None
  method originator_address = Some "tz1..."
  method batch_timeout = 60.
  method batch_load = 100L
end

let dummy_config = {
  nodes = [ "https://tz.functori.com" ]; sleep = 6.; start = Some 13650000l;
  forward = None; verbose = 0; db_kind = `pg; step_forward = 20;
  confirmations_needed = 10l; plugins = []; retry = Some 5;
  retry_timeout = 10.; no_forward = false; extra = dummy_extra_config
}

let usage =
  Format.sprintf
    "usage: \027[0;93m./crawler <conf.json>\027[0m\n\n<config.json>:\n\027[0;96m%s\027[0m\n" @@
  EzEncoding.construct ~compact:false (config_enc extra_config_enc) dummy_config

let crawler config =
  Register.register_all_plugins config

(* TODO : WEBSOCKET ? *)
(* let api input =
 *   match input#port with
 *   | None -> Lwt.return_unit
 *   | Some port -> EzAPIServer.(server [ port, API Taktick_plugin.ppx_dir ]) *)

let () =
  let () = Pg.PG.Pool.init ~database:Crp.database () in
  Arg.parse [] (fun f -> filename := Some f) usage;
  match !filename with
  | None -> Arg.usage [] usage
  | Some filename ->
    EzLwtSys.run @@ fun () ->
    let> r = get_crawler_config filename in
    match r with
    | Error e -> Rp.print_error e; Lwt.return_unit
    | Ok (config, ext) ->
      Format.eprintf "Crawler Config: %s@." @@
      EzEncoding.construct extra_config_enc ext ;
      let config =
        { config with
          extra = { E.contracts = CSet.of_list config.extra ;
                    originator_address = Option.get ext#originator_address } } in
      Lwt.join [ crawler config(* ; api input *) ]
|}

let update =
  {|
let () =
  let database = Option.value ~default:Db_env.database (Sys.getenv_opt "PGDATABASE") in
  let upgrades =
    List.map (fun (v, (u, downgrade)) ->
        v, (fun dbh version -> EzPG.upgrade ~dbh ~version ~downgrade u)) @@
    Tables.merge all in
  EzPGUpdater.main database ~upgrades
|}

let bm_utils =
  {|open Tzfunc
open Proto
open Crp
open Common

let big_map_allocs l =
  let f id key_type value_type =
    match Mtyped.parse_type key_type, Mtyped.parse_type value_type with
    | Error _, _ | _, Error _ -> id, None
    | Ok k, Ok v ->
      id, Some (Mtyped.short k, Mtyped.short v) in
  List.sort (fun (id1, _) (id2, _) -> compare id1 id2) @@
  List.filter_map (function
      | Big_map { id; diff = SDAlloc {key_type; value_type; _} } ->
        if id < Z.zero then None
        else Some (f id key_type value_type)
      | Big_map { id; diff = SDCopy {source; _} } ->
        List.find_map (function
            | Big_map { id = idc; diff = SDAlloc {key_type; value_type; _} }
              when source = Z.to_string idc ->
              Some (f id key_type value_type)
            | _ -> None) l
      | _ -> None) l

let get_big_map_id ~allocs bm_index k v =
  let rec aux i = function
    | [] -> None
    | _ :: t when i < bm_index -> aux (i+1) t
    | (id, t) :: _ ->
      match t with
      | Some (k2, v2) when k = k2 && v = v2 -> Some id
      | _ -> None in
  aux 0 allocs

let unexpected_michelson = Error `unexpected_michelson

let get_code_elt p = function
  | Mseq l ->
    List.find_map
      (function
        | Mprim { prim; args = [ arg ]; _} when prim = p -> Some arg
        | _ -> None) l
  | _ -> None

let get_storage_fields script =
  let rec aux bm_index acc = function
    | {Mtyped.typ=`tuple []; name = None} -> bm_index, acc
    | {Mtyped.typ=`tuple (h :: t); _} ->
      let bm_index, acc = aux bm_index acc h in
      aux bm_index acc {Mtyped.name=None; typ=`tuple t}
    | {Mtyped.name=Some n; typ = `big_map (k, v)} ->
      bm_index + 1, (n, Some (
          bm_index, Mtyped.short k, Mtyped.short v)) :: acc
    | {Mtyped.name=Some n; _} ->
      bm_index, (n, None) :: acc
    | _ -> bm_index, acc in
  match get_code_elt `storage script.code with
  | None -> unexpected_michelson
  | Some m -> match Mtyped.parse_type m with
    | Error e -> Error e
    | Ok t -> Ok (List.rev @@ snd @@ aux 0 [] t)

let match_fields ~expected ~allocs script =
  match get_storage_fields script, get_code_elt `storage script.code with
  | Error _, _ | _, None ->
    unexpected_michelson
  | Ok fields, Some storage_type ->
    let$ storage_type = try Mtyped.parse_type storage_type with _ -> unexpected_michelson in
    let$ storage_value = try Mtyped.(parse_value (short storage_type) script.storage) with _ -> unexpected_michelson in
    Ok (List.map (fun name ->
        match List.assoc_opt name fields with
        | None -> None, None
        | Some None ->
          Mtyped.search_value ~name storage_type storage_value, None
        | Some (Some (bm_index, k, v)) ->
          Mtyped.search_value ~name storage_type storage_value,
          (match get_big_map_id ~allocs bm_index k v with
           | None -> None
           | Some bm_id -> Some {bm_id; bm_types = {bmt_key=k; bmt_value=v}})
      ) expected)

let find_opt_storage_field ~allocs ~fields ~storage_type ~storage_value name =
  match List.assoc_opt name fields with
  | None -> None, None
  | Some None ->
    Mtyped.search_value ~name storage_type storage_value, None
  | Some (Some (bm_index, k, v)) ->
    Mtyped.search_value ~name storage_type storage_value,
    (match get_big_map_id ~allocs bm_index k v with
     | None -> None
     | Some bm_id -> Some {bm_id; bm_types = {bmt_key=k; bmt_value=v}})

let get_bm_updates ~bm_id ?key_decode ?value_decode l =
  List.flatten @@
  List.filter_map (function
      | Big_map { id=bid; diff = SDUpdate l }
      | Big_map { id=bid; diff = SDAlloc {updates=l; _} } when bid = bm_id ->
        Some (List.filter_map (fun {bm_key; bm_value; _} ->
            match bm_value with
            | None -> None
            | Some bmv ->
              try
                match key_decode, value_decode with
                | Some kd, Some vd ->
                  Some (kd bm_key, vd bmv)
                | _, _ ->
                  Some (bm_key, bmv)
              with _ -> None) l)
      | _ -> None) l|}

let common =
  {|open Tzfunc
open Proto
open Pg
open Crp

module Info = Info

open Info

type bigmap_types = {
  bmt_key: Mtyped.stype; [@encoding Mtyped.stype_enc.json]
  bmt_value: Mtyped.stype; [@encoding Mtyped.stype_enc.json]
} [@@deriving encoding]

type bigmap_info = {
  bm_id: z;
  bm_types: bigmap_types
} [@@deriving encoding]

module CSet = Set.Make(struct
type t = contract_info
let compare c1 c2 = String.compare c1.ci_address c2.ci_address
end)

type extra = {
mutable contracts : CSet.t [@set contract_info_enc] ;
} [@@deriving encoding]

type extra_config = <
  node: string option;
  originator_address : string option ;
  port: int option;
  batch_timeout: float; [@dft 60.]
  batch_load: A.uint64; [@dft 100L] [@encoding A.uint64_enc.json]
> [@@deriving encoding {ignore}]

module E = struct
  type extra = {
    mutable contracts : CSet.t [@set contract_info_enc] ;
    originator_address : string ;
} [@@deriving encoding]
end

let short ?(len=8) h =
  if String.length h > len then String.sub h 0 len else h

let use dbh f = match dbh with
  | None -> PG.Pool.use f
  | Some dbh -> f dbh

let get_info ?dbh () =
  use dbh @@ fun dbh ->
  let|>? l = [%pgsql.object dbh "select * from contracts"] in
  List.map Info.info_of_row l

let parse_file filename enc f =
  try f @@ EzEncoding.destruct enc filename
  with _ ->
    let ic = open_in filename in
    let s = really_input_string ic (in_channel_length ic) in
    close_in ic;
    try f @@ EzEncoding.destruct enc s
    with exn -> rerr @@ `generic ("config_error", Printexc.to_string exn)

let get_crawler_config filename =
  let aux c =
    let|>? extra = get_info () in
    { c with extra }, c.extra in
  parse_file filename (config_enc extra_config_enc) aux|}

let static_tables =
  {|let downgrade =
  [
    "drop state cascade";
    "drop all_operations cascade";
  ]

let upgrade =
  [
    "create domain zarith as numeric";
    "create or replace function zadd(z1 zarith, z2 zarith) returns zarith as \
     $$ begin return z1 + z2; end; $$ language plpgsql";
    "create or replace function zsub(z1 zarith, z2 zarith) returns zarith as \
     $$ begin return z1 - z2; end; $$ language plpgsql";
    "create or replace function zmul(z1 zarith, z2 zarith) returns zarith as \
     $$ begin return z1 * z2; end; $$ language plpgsql";
    "create or replace function zdiv(z1 zarith, z2 zarith) returns zarith as \
     $$ begin return z1 / z2; end; $$ language plpgsql";
    "create table state(level int not null, chain_id varchar not null, tsp \
     timestamp not null)";
    "create index state_level_index on state(level)";
    "create index state_chain_id_index on state(chain_id)";
    "create index state_tsp_index on state(tsp)";
    "create table all_operations(contract varchar not null, kind varchar not \
     null, transaction varchar not null, block varchar not null, index int not \
     null, level int not null, tsp timestamp not null, main boolean not null)";
    "create index all_operations_contract_index on all_operations(contract)";
    "create index all_operations_kind_index on all_operations(kind)";
    "create index all_operations_block_index on all_operations(block)";
    "create index all_operations_level_index on all_operations(level)";
    "create index all_operations_tsp_index on all_operations(tsp)";
    "create index all_operations_main_index on all_operations(main)";
  ]|}
