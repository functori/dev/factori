(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file provides a subcommand called "sandbox start" to start a
Flextesa sandbox, which is a private Tezos network for testing. It includes two
functions, `sandbox_script` which returns a string containing a shell script for
starting a sandbox with a given block time, and `sandbox_start` which defines
the subcommand `sandbox start`. The file also includes an example of how to
create a `sandbox` command using `sandbox_start`. Warning: this short summary
was automatically generated and could be incomplete or misleading. If you feel
this is the case, please open an issue. *)

open Factori_command

let sandbox_script block_time =
  Format.sprintf
    {|#!/bin/sh

image=oxheadalpha/flextesa:latest
script=nairobibox
docker run --rm --name \
  factori-sandbox \
  --detach \
  -p 20000:20000 \
  -e block_time=%d \
  -e flextesa_node_cors_origin='*' \
  "$image" "$script" \
  start|}
    block_time

let sandbox_start =
  subcommand ~name:"sandbox start" ~args:[Factori_options.block_time_option]
    ~doc:
      "Start a Flextesa sandbox. You will need docker installed as well as \
       proper permissions (see for instance \
       https://docs.docker.com/engine/install/linux-postinstall/)" (fun () ->
      let cmd =
        Factori_utils.command_if_exists
          ~error_msg:"Please install docker first." ~cmd_name:"docker"
          (sandbox_script !Factori_options.block_interval) in
      let _ = Sys.command cmd in
      ())
