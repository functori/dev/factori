(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains utility functions for working with tuples in C#. It
defines a custom data structure called `tupletree` that represents tuples of
arbitrary length and provides functions for generating boilerplate code for
creating, encoding, and decoding custom tuples in C#. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Factori_utils

type 'a tupletree =
  | Leaf of 'a
  | T of 'a tupletree list

let tupletree_of_list (l : 'a list) : 'a tupletree =
  let rec aux accu = function
    | 7, [] -> T accu
    | 7, l -> T (accu @ [aux [] (0, l)])
    | _, [] -> T accu
    | k, x :: xs -> aux (accu @ [Leaf x]) (k + 1, xs) in
  aux [] (0, l)

let f_leaf ppf x = Format.fprintf ppf "T%d" x

let rec f_t f_leaf ppf t =
  match t with
  | Leaf x -> f_leaf ppf x
  | T l ->
    Format.fprintf ppf "Tuple<%a>"
      (Format.pp_print_list ~pp_sep:(tag ",") (f_t f_leaf))
      l

let rec f_mytuple_items ~initial ~name_item f_t ppf t =
  match t with
  | Leaf x -> Format.fprintf ppf "%s%d" name_item x
  | T l ->
    if initial then
      Format.fprintf ppf "%a"
        (Format.pp_print_list ~pp_sep:(tag ",")
           (f_mytuple_items ~initial:false ~name_item f_t))
        l
    else
      Format.fprintf ppf "new Tuple<%a>(%a)"
        (Format.pp_print_list ~pp_sep:(tag ",") f_t)
        l
        (Format.pp_print_list ~pp_sep:(tag ",")
           (f_mytuple_items ~initial:false ~name_item f_t))
        l

(* We need a way to get around the fact that tuples are limited to 8 elements in C# *)
let custom_tuple_define ppf i =
  let open Format in
  let tt = tupletree_of_list (list_integers i) in
  let f_t = f_t f_leaf in
  let f_mytuple_items = f_mytuple_items ~initial:true f_t in
  fprintf ppf
    {|
public class MyTuple<%a> : %a, IFactoriT %a{

public MyTuple(%a) : base(%a){ }
public MyTuple(MyTuple<%a> tp) : this(%a) { }
%a
%a
%a
%a

}
|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i) f_t tt
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "where T%d : IFactoriT, new()" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d item%d" i i))
    (list_integers i)
    (f_mytuple_items ~name_item:"item")
    tt
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "tp.%a" print_item i))
    (list_integers i)

let tuple_encode_generate ppf i =
  let open Format in
  fprintf ppf
    {|
public IMicheline Encode(){
           return new MichelinePrim {
                Prim = PrimType.Pair,
                Args = new List<IMicheline>{
                    %a
                   }

            };
        }
|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "this.%a.Encode()" print_item i))
    (list_integers i)

let tuple_decode_generate ppf i =
  let open Format in
  fprintf ppf
    {|
            public static MyTuple<%a> Decode(IMicheline im){
                var error = new DecodeError($"In Tuple%dDecode, failed on {im}");
                List<IMicheline> args;
                switch (im) {
                case MichelinePrim mp:
                    switch(mp.Prim){
                        case PrimType.Pair:
                          if(mp.Args == null){
                            throw new DecodeError($"Can't have a Pair with Args == null : {FactoriTypes.Utils.GetIMichelineString(mp)}");
                          }
                          args = mp.Args;
                          break;
                        default:
                          throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                        }
                    break;
                default:
                    throw new DecodeError($"This is neither a Pair nor a Micheline Array, can't decode");
                }
                if(args.Count < 2){
                    throw new DecodeError($"In Tuple%dDecode, there are less than two elements in {args}");
                    }
                else{
                            List<IMicheline> new_arg = FactoriTypes.Utils.flatPair(%d,args);
                            return new MyTuple<%a>(%a);
               }
           }
     public MyTuple(IMicheline im) : this(Decode(im)) {}

public static IFactoriT GenericDecode(IMicheline im){
     return MyTuple<%a>.Decode(im);
}

|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> Format.fprintf ppf "T%d" i))
    (list_integers i) i i i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> Format.fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i ->
         Format.fprintf ppf "(T%d) T%d.GenericDecode(new_arg[%d])" i i (i - 1)))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> Format.fprintf ppf "T%d" i))
    (list_integers i)

let tuple_generator_generate ppf i =
  let open Format in
  fprintf ppf {|
public MyTuple() : this(%a){}
|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "new T%d()" i))
    (list_integers i)

(* public static Func<Tuple<T1,T2>> tuple2_generator<T1,T2>(Func<T1> t1_generator,Func<T2> t2_generator){
 *             return () => {return Tuple.Create(t1_generator(),t2_generator());};
 *         } *)

let tuple_micheline_generate ppf num =
  let all_arg =
    List.init num (fun i -> Format.sprintf "T%d.GetMicheline()" (i + 1)) in
  Format.fprintf ppf
    "public static IMicheline GetMicheline(){\n\
     \treturn new MichelinePrim{Prim=PrimType.pair, Args= new \
     List<IMicheline>(){%a}};\n\
     }\n\n\n"
    (Format.pp_print_list ~pp_sep:(tag ", ") (fun ppf s ->
         Format.fprintf ppf "%s" s))
    all_arg

let repeat n x = List.map (fun _ -> x) (list_integers ~from:1 n)

let tuple_boilerplate ppf n =
  let open Format in
  (pp_print_list
     ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
     (fun ppf i ->
       custom_tuple_define ppf i tuple_generator_generate i
         tuple_decode_generate i tuple_encode_generate i
         tuple_micheline_generate i))
    ppf (list_integers ~from:2 n)
