(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file contains a type called 'mapper' and several functions that
use it to map over control flow graphs and their nodes in the Michelson language
used in the Tezos blockchain. The 'mapper' type is a record type with five
fields, and the functions include 'apply_control_node', 'apply_node',
'apply_node_l', and 'default_mapper'. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Unstack_micheline

(* apply_arg : 'a -> ('a,'env mapper) -> 'env -> 'a; *)
type ('a, 'env) mapper = {
  apply_use_arg : 'env -> 'a -> 'a;
  apply_assign_arg : 'env -> 'a -> 'a;
  apply_node : ('a, 'env) mapper -> 'env -> 'a node -> 'a node;
  apply_node_l : ('a, 'env) mapper -> 'env -> 'a node list -> 'a node list;
  apply_control_node :
    ('a, 'env) mapper -> 'env -> 'a control_node -> 'a control_node;
}

let identity _env n = n

let apply_control_node map env control =
  match control with
  | N_ITER
      {
        arg_iter;
        acc_iter;
        pat_acc_iter;
        pat = pat, lab_pat;
        body_iter;
        label_iter;
      } ->
    let arg_iter = map.apply_use_arg env arg_iter in
    let acc_iter = List.map (map.apply_use_arg env) acc_iter in
    let pat_acc_iter = List.map (map.apply_assign_arg env) pat_acc_iter in
    let pat = map.apply_assign_arg env pat in
    let body_iter = map.apply_node_l map env body_iter in
    N_ITER
      {
        arg_iter;
        acc_iter;
        pat_acc_iter;
        pat = (pat, lab_pat);
        body_iter;
        label_iter;
      }
  | N_MAP (arg, label, pat, node_l, label_map) ->
    let arg = map.apply_use_arg env arg in
    let pat = map.apply_assign_arg env pat in
    let node_l = map.apply_node_l map env node_l in
    N_MAP (arg, label, pat, node_l, label_map)
  | N_LOOP_LEFT
      {
        arg_lf;
        pat_lf = pat_lf, lab_pat_lf;
        acc_lf;
        pat_acc_lf;
        node_loop;
        pat_res_lf = pat_res_lf, lab_pat_res;
        label_lf;
      } ->
    let arg_lf = map.apply_use_arg env arg_lf in
    let acc_lf = List.map (map.apply_use_arg env) acc_lf in
    let pat_lf = map.apply_assign_arg env pat_lf in
    let pat_acc_lf = List.map (map.apply_assign_arg env) pat_acc_lf in
    let pat_res_lf = map.apply_assign_arg env pat_res_lf in
    let node_loop = map.apply_node_l map env node_loop in
    N_LOOP_LEFT
      {
        arg_lf;
        pat_lf = (pat_lf, lab_pat_lf);
        acc_lf;
        pat_acc_lf;
        node_loop;
        pat_res_lf = (pat_res_lf, lab_pat_res);
        label_lf;
      }
  | N_FOLD_MAP
      {
        arg_fold_map;
        acc_fold_map;
        pat_acc_fold_map = pat_acc_fold_map, lab_pat_acc;
        pat_fold_map = pat_fold_map, lab_pat;
        body_fold_map;
        label_map;
      } ->
    let body_fold_map = map.apply_node_l map env body_fold_map in
    let pat_fold_map = map.apply_assign_arg env pat_fold_map in
    let pat_acc_fold_map =
      List.map (map.apply_assign_arg env) pat_acc_fold_map in
    let acc_fold_map = List.map (map.apply_use_arg env) acc_fold_map in
    let arg_fold_map = map.apply_use_arg env arg_fold_map in
    N_FOLD_MAP
      {
        arg_fold_map;
        acc_fold_map;
        pat_acc_fold_map = (pat_acc_fold_map, lab_pat_acc);
        pat_fold_map = (pat_fold_map, lab_pat);
        body_fold_map;
        label_map;
      }
  | N_LOOP { arg; acc; pat_acc; body; label_loop; label_pat_l } ->
    let body = map.apply_node_l map env body in
    let arg = map.apply_use_arg env arg in
    let acc = List.map (map.apply_use_arg env) acc in
    let pat_acc = List.map (map.apply_assign_arg env) pat_acc in
    N_LOOP { arg; acc; pat_acc; body; label_loop; label_pat_l }
  | N_IF_NONE (arg, label_then, label_else, node_b1, node_b2, pat, lab_pat) ->
    let arg = map.apply_use_arg env arg in
    let pat = map.apply_assign_arg env pat in
    let node_b1 = map.apply_node_l map env node_b1 in
    let node_b2 = map.apply_node_l map env node_b2 in
    N_IF_NONE (arg, label_then, label_else, node_b1, node_b2, pat, lab_pat)
  | N_IF_CONS
      ( arg,
        label_then,
        label_else,
        node_b1,
        node_b2,
        pat_x,
        label_x,
        pat_xs,
        label_xs ) ->
    let arg = map.apply_use_arg env arg in
    let pat_x = map.apply_assign_arg env pat_x in
    let pat_xs = map.apply_assign_arg env pat_xs in
    let node_b1 = map.apply_node_l map env node_b1 in
    let node_b2 = map.apply_node_l map env node_b2 in
    N_IF_CONS
      ( arg,
        label_then,
        label_else,
        node_b1,
        node_b2,
        pat_x,
        label_x,
        pat_xs,
        label_xs )
  | N_IF (arg, label_then, label_else, node_b1, node_b2) ->
    let arg = map.apply_use_arg env arg in
    let node_b1 = map.apply_node_l map env node_b1 in
    let node_b2 = map.apply_node_l map env node_b2 in
    N_IF (arg, label_then, label_else, node_b1, node_b2)
  | N_IF_LEFT
      ( arg,
        label_then,
        label_else,
        (pat_l, lab_l, node_b1),
        (pat_r, lab_r, node_b2) ) ->
    let arg = map.apply_use_arg env arg in
    let pat_l = map.apply_assign_arg env pat_l in
    let pat_r = map.apply_assign_arg env pat_r in
    let node_b1 = map.apply_node_l map env node_b1 in
    let node_b2 = map.apply_node_l map env node_b2 in
    N_IF_LEFT
      ( arg,
        label_then,
        label_else,
        (pat_l, lab_l, node_b1),
        (pat_r, lab_r, node_b2) )

let apply_node map env n =
  match n with
  | N_CREATE_CONTRACT (opt_k, mutz, ty2, contract, ope, addr, label) ->
    let opt_k = map.apply_use_arg env opt_k in
    let mutz = map.apply_use_arg env mutz in
    let ty2 = map.apply_use_arg env ty2 in
    let ope = map.apply_assign_arg env ope in
    let addr = map.apply_assign_arg env addr in
    N_CREATE_CONTRACT (opt_k, mutz, ty2, contract, ope, addr, label)
  | N_LAMBDA (pat, lab_pat, node_l, res, lab_in, lab_end) ->
    let pat = map.apply_assign_arg env pat in
    let res = map.apply_assign_arg env res in
    let node_l = map.apply_node_l map env node_l in
    N_LAMBDA (pat, lab_pat, node_l, res, lab_in, lab_end)
  | FAIL_CONTROL (n, label) ->
    let new_node = map.apply_control_node map env n in
    FAIL_CONTROL (new_node, label)
  | N_START (arg, n, label) ->
    let n = map.apply_control_node map env n in
    N_START (arg, n, label)
  | N_SELF (str_opt, res, label) ->
    let res = map.apply_assign_arg env res in
    N_SELF (str_opt, res, label)
  | N_CONTRACT (str_opt, ctrt, res, label) ->
    let ctrt = map.apply_use_arg env ctrt in
    let res = map.apply_assign_arg env res in
    N_CONTRACT (str_opt, ctrt, res, label)
  | N_NEVER (n, label) ->
    let n = map.apply_use_arg env n in
    N_NEVER (n, label)
  | N_FAILWITH (f, lab) ->
    let f = map.apply_use_arg env f in
    N_FAILWITH (f, lab)
  | N_CALL_VIEW (view_name, arg, view, res, label) ->
    let arg = map.apply_use_arg env arg in
    let view = map.apply_use_arg env view in
    let res = map.apply_assign_arg env res in
    N_CALL_VIEW (view_name, arg, view, res, label)
  | N_SIMPLE (prim, args, param, res, label) ->
    let param = List.map (map.apply_use_arg env) param in
    let res = List.map (map.apply_assign_arg env) res in
    N_SIMPLE (prim, args, param, res, label)
  | N_END (res_l, label) ->
    let res_l = List.map (map.apply_use_arg env) res_l in
    N_END (res_l, label)
  | N_CONCAT (arg1, arg2, res, label) ->
    let arg1 = map.apply_use_arg env arg1 in
    let arg2 = map.apply_use_arg env arg2 in
    let res = map.apply_assign_arg env res in
    N_CONCAT (arg1, arg2, res, label)
  | N_CONCAT_LIST (arg, res, label) ->
    let arg = map.apply_use_arg env arg in
    let res = map.apply_assign_arg env res in
    N_CONCAT_LIST (arg, res, label)
  | N_EXEC (arg, lambda, res, lab_call, lab_ret) ->
    let arg = map.apply_use_arg env arg in
    let lambda = map.apply_use_arg env lambda in
    let res = map.apply_assign_arg env res in
    N_EXEC (arg, lambda, res, lab_call, lab_ret)

let apply_node_l map env node_l = List.map (map.apply_node map env) node_l

let default_mapper =
  {
    apply_node;
    apply_node_l;
    apply_control_node;
    apply_use_arg = identity;
    apply_assign_arg = identity;
  }
