open Common
include Version

type nonrec t = t = {
  version : string;
  commit : string;
}
[@@deriving jsoo]

{%%template|
<a :href="link" target="_blank">
  V.{{ version.version }} ({{ short_commit }})
</a>
|}

let%prop version : t = { req }

let%comp short_commit this : string =
  let { commit; _ } = Version.of_jsoo this##.version in
  try String.sub commit 0 10 with _ -> commit

and link this =
  let { commit; _ } = Version.of_jsoo this##.version in
  Format.sprintf "https://gitlab.com/functori/dev/factori/-/tree/%s" commit

[%%comp { name = "v-version"; conv; components }]
