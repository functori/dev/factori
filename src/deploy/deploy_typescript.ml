(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Typescript *)
open Format
open Factori_utils
open Factori_config

module Generate = struct
  let blockchain_typescript_storage = sprintf "initial_blockchain_storage"

  let random_typescript_storage = sprintf "storage_generator()"

  let storage_typescript ~storage_type =
    match storage_type with
    | Random -> random_typescript_storage
    | Blockchain -> blockchain_typescript_storage

  (* The typescript script that will deploy the KT1 by using the generated interface *)
  let deploy_typescript ~contract_name ~network ~from ~storage_type ppf () =
    let get_network_config network = sprintf "functolib.%s_config" network in
    let get_from from = sprintf "functolib.%s_flextesa" from in
    fprintf ppf
      {|
import {
    TezosToolkit,
    MichelsonMap,
  } from "@taquito/taquito";
import { MichelsonV1Expression } from "@taquito/rpc"
import * as %s from "../%s/%s_interface";
import * as functolib from "../%s/functolib";
const config = %s;
const tezosKit = new TezosToolkit(config.node_addr);
|}
      contract_name Typescript_SDK.interface_dir_name contract_name
      Typescript_SDK.libraries_dir_name
      (get_network_config network) ;

    fprintf ppf
      {|
export async function deploy(){
    functolib.setSigner(tezosKit,%s.sk);
    let storage = %s.%s;
    let kt1 = await %s.deploy_%s(tezosKit,storage,config);
    console.log("Deploy %s contract at %%s on %s", kt1);
    return kt1;
}

  |}
      (get_from from) contract_name
      (storage_typescript ~storage_type)
      contract_name contract_name contract_name network

  let scenario_typescript ~contract_name ppf () =
    Format.fprintf ppf
      {|
import * as %s from "../%s/%s";
async function main(){
    let kt1 = await %s.deploy()
}

main()|}
      contract_name Typescript_SDK.deploy_dir_name contract_name contract_name
end

let deploy ~storage_type ~contract_name ~network ~from () =
  let dir = get_dir () in
  let deploy_file =
    asprintf "%a"
      (Generate.deploy_typescript ~contract_name ~network ~from ~storage_type)
      () in
  let deploy_dir = Typescript_SDK.deploy_dir ~dir () in
  make_dir deploy_dir ;
  let deploy_name_ts = sprintf "%s.ts" contract_name in
  let full_deploy_path = deploy_dir // deploy_name_ts in
  write_file ~filename:full_deploy_path deploy_file ;
  let scenario_file =
    asprintf "%a" (Generate.scenario_typescript ~contract_name) () in
  let scenario_name_ts = sprintf "main_deploy_%s.ts" contract_name in
  let scenario_name_js = sprintf "main_deploy_%s.js" contract_name in
  let full_scenario_path =
    Typescript_SDK.scenarios_dir ~relative:true ~dir:"" () // scenario_name_ts
  in
  write_file ~filename:full_scenario_path scenario_file ;
  sprintf
    "cd %s && make ts-deps && make ts && node src/typescript_sdk/dist/%s/%s" dir
    Typescript_SDK.scenarios_dir_name scenario_name_js
