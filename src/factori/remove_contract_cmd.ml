(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This is an OCaml file that defines the `remove_contract` subcommand for the
`Factori` application. It allows users to remove an existing contract from the
project, either logically or by purging the file. There are no monadic binders
in this file. Warning: this short summary was automatically generated and could
be incomplete or misleading. If you feel this is the case, please open an issue.
*)

open Factori_command
open Factori_options
open Factori_errors
open Factori_utils
open Factori_config

let ( // ) = concat

let remove_contract =
  subcommand ~name:"remove contract"
    ~args:[dir_anonymous_option; purge_option; contract_name_option]
    ~doc:
      "Remove an existing contract from the project. If the --purge option is \
       activated, it will remove the file, otherwise it will only remove it \
       logically from the build infrastructure." (fun () ->
      try
        let dir = Factori_config.get_dir () in
        match !contract_name with
        | None ->
          Format.eprintf
            "Please provide a contract name at the end of the remove_contract \
             command\n\
             %!"
        | Some contract_name ->
          let contract_name = sanitize_basename contract_name in
          (match Factori_config.get_contract_opt contract_name with
          | None ->
            Format.eprintf "No contract found with this name: %s\n%!"
              (show_sanitized_name contract_name)
          | Some _c ->
            (* let contract_path =
             *   File_structure.get_contract_path ~contract_name
             *     ~format:c.original_format in *)
            if !Factori_options.purge then (
              (* Sys.remove contract_path ; *)
              let ocaml_abstract_interface_file =
                OCaml_SDK.abstract_interface_path ~dir ~contract_name () in
              let ocaml_interface_file =
                OCaml_SDK.interface_path ~dir ~contract_name () in
              let ocaml_interface_file_mli =
                OCaml_SDK.interface_mli_path ~dir ~contract_name () in
              let ocaml_code_file = OCaml_SDK.code_path ~dir ~contract_name () in
              let ocaml_code_mli_file =
                OCaml_SDK.code_mli_path ~dir ~contract_name () in
              let py_interface_file =
                Python_SDK.interface_path ~dir ~contract_name () in
              let py_code_file = Python_SDK.code_path ~dir ~contract_name () in

              let ts_interface_file =
                Typescript_SDK.interface_path ~dir ~contract_name () in
              let csharp_interface_file =
                Csharp_SDK.interface_path ~dir ~contract_name () in
              let csharp_code_file =
                Csharp_SDK.code_path ~dir ~contract_name () in
              let ts_code_file =
                Typescript_SDK.code_path ~dir ~contract_name () in
              output_verbose
              @@ Format.sprintf "Removing OCaml abstract interface file %s\n%!"
                   ocaml_abstract_interface_file ;
              remove_file ocaml_abstract_interface_file ;

              output_verbose
              @@ Format.sprintf "Removing OCaml interface file %s\n%!"
                   ocaml_interface_file ;
              remove_file ocaml_interface_file ;

              output_verbose
              @@ Format.sprintf "Removing OCaml interface mli file %s\n%!"
                   ocaml_interface_file ;
              remove_file ocaml_interface_file_mli ;

              output_verbose
              @@ Format.sprintf "Removing OCaml code file %s\n%!"
                   ocaml_code_file ;
              remove_file ocaml_code_file ;

              output_verbose
              @@ Format.sprintf "Removing OCaml code mli file %s\n%!"
                   ocaml_code_file ;
              remove_file ocaml_code_mli_file ;

              output_verbose
              @@ Format.sprintf "Removing Python code file %s\n%!" py_code_file ;
              remove_file py_code_file ;
              output_verbose
              @@ Format.sprintf "Removing Python interface file %s\n%!"
                   py_interface_file ;
              remove_file py_interface_file ;

              output_verbose
              @@ Format.sprintf "Removing Typescript code file %s\n%!"
                   ts_code_file ;
              remove_file ts_code_file ;
              output_verbose
              @@ Format.sprintf "Removing Typescript interface file %s\n%!"
                   ts_interface_file ;
              remove_file ts_interface_file ;
              output_verbose
              @@ Format.sprintf "Removing C# interface file %s\n%!"
                   csharp_interface_file ;
              remove_file csharp_interface_file ;
              output_verbose
              @@ Format.sprintf "Removing C# code file %s\n%!" csharp_code_file ;
              remove_file csharp_code_file
            ) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:true
              ~path:(OCaml_SDK.interface_dir ~dir () // "dune")
              (OCaml_SDK.interface_basename ~contract_name) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:true
              ~path:(OCaml_SDK.interface_dir ~dir () // "dune")
              (OCaml_SDK.abstract_interface_basename ~contract_name) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
              ~path:(OCaml_SDK.scenarios_dir ~dir () // "dune")
              (OCaml_SDK.interface_basename ~contract_name) ;
            Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
              ~path:(OCaml_SDK.scenarios_dir ~dir () // "dune")
              (OCaml_SDK.abstract_interface_basename ~contract_name) ;
            (* crawlori files *)
            let crawlori_dir = OCaml_SDK.crawlori_dir ~dir () in
            if is_dir crawlori_dir then begin
              Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
                ~path:(OCaml_SDK.crawlori_dir ~dir () // "dune")
                (OCaml_SDK.crawlori_plugin_basename ~contract_name) ;
              Ocaml_dune.remove_library_by_name ~fail_if_doesnt_exist:false
                ~path:(OCaml_SDK.crawlori_dir ~dir () // "dune")
                (OCaml_SDK.crawlori_tables_basename ~contract_name) ;
              Import_kt1.remove_upgrade_downgrade_tables dir contract_name ;
              Import_kt1.remove_plugin_register dir contract_name
            end ;
            (* dipdup files *)
            let dipdup_dir = Factori_config.get_dipdup_dir ~dir in
            if is_dir dipdup_dir then
              Import_kt1.remove_dipdup_indexing ~dir ~contract_name ;
            Factori_config.remove_contract_from_config_file ~dir
              ~name:contract_name) ;
          Format.eprintf "Successfully removed contract.\n%!"
      with e -> handle_exception e)
