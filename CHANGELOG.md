# Changelog

All notable changes to this project will be documented in this file.

## [0.7.1] - 2023-09-08

### New features
- Functions in SDK to retrieve value from storage's big_map
- New C# SDK

### Fixed
- Fix generators problem for string, set
- Encoding/Decoding problem in Python

## [0.7.0] - 2023-07-04

### New features
- Deploy command for python and csharp
- Examples in factori repository

### Fixed
- Fix problem with unsued analysis
- Multiple fix in printing

## [0.6.2] - 2023-05-23

### Fixed
- Improved analysis and linting print
- Fusion between linting and analysis
- Fixing more contract analysis

## [0.6.1] - 2023-04-19

### Fixed
- Bug fixes mainly C#
- Fixing analysis missing instructions
- Fixing analysis stack errors

### Blog link

https://www.functori.com/blog/blog-nft-contract-static-analysis-with-factori.html

## [0.6] - 2023-03-30

### New features

- Linter and static analysis

### Blog link

https://www.functori.com/blog/blog-factori-analysis-secure-and-enhance-smart-contracts.html

## [0.5.2] - 2023-02-02

### Added

- Support for Python generation
- Support for C# generation

### Blog link

https://www.functori.com/blog/blog-csharp-python-now-supported-in-factori.html

## [0.5.1.1] - 2023-02-01

### Fixed

- Bug where C# code path was absolute, which made it incorrect in the Docker version

## [0.5.1] - 2023-01-31

### Added

- Beta support for C# code generation using the Netezos framework

## [0.4] - 2022-11-21

### Added

- DSL for scenarios

### Blog link

https://www.functori.com/blog/blog-factori-tutorial-for-scenarios.html

## [0.3] - 2022-09-01

### Added

- Document mli files
- Generate handlers for Crawlori
- Generate handlers for Dip{Dup}
- UI improvements
- Steps towards feature parity in Typescript wrt OCaml:
  - Generate Micheline types for every generated type
  - Generate random generators for every generated type
- New contracts added to test suite
- Better handling of names and sanitizing
- Refactoring of AST types to make future code generation less painful
- Generate an explorer-like web interface for any contract, with access to storage + forms integrated with Beacon
- More options when calling entrypoints in OCaml: you may now forge an operation without injecting it, and assert_failwith

### Blog link

https://www.functori.com/blog/blog-new-features-in-factori-more-free-stuff-for-smart-contract-programmers.html

## [0.2] - 2022-07-06

### Changes

- Big redesign of record inference and tuple decoding, yielding faster import times as well as more intuitive interfaces
- Integrate a test suite comprising all mainnet smart contracts (currently at 99.9% import success and around >98% interface compilation success)
- Fix various minor bugs, such as not including missing rare types such as never, ticket, chain_id or sapling types
- Sum type constructors starting not starting with a letter are now supported
- Updated README
- Tolerate types called "storage" even if they are not the storage

### Blog link

https://www.functori.com/blog/blog-factori-tutorial-ocaml.html

## [0.1.5] - 2022-06-29

### Changes

No changes documented.

## [0.1.4] - 2022-06-14

### Changes

No changes documented.

## [0.1.2] - 2022-06-09

### Fixed

- Bug with base type storage
- Lwt.unix bug
- Missing files when importing OCaml after Typescript

### Blog link

https://www.functori.com/blog/blog-factori-introduction-and-tutorial.html

## [0.1] - 2022-05-10

Initial release.
