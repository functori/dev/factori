(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file provides functions for generating boilerplate code for
tuples of various sizes in the Tezos blockchain, including tuple generators,
encoders, decoders, and Micheline representations. It includes functions for
generating each of these components individually, as well as a function that
generates the complete set of boilerplate code for tuples of size `n`. Warning:
this short summary was automatically generated and could be incomplete or
misleading. If you feel this is the case, please open an issue. *)

open Factori_utils

let tuple_generator_generate ppf i =
  let open Format in
  fprintf ppf "let tuple%d_generator %a () = (%s)" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "ex%d" i))
    (list_integers i)
    (String.concat ","
       (List.map (fun i -> Format.sprintf "ex%d ()" i) (list_integers i)))

let tuple_encode_generate ppf i =
  let open Format in
  fprintf ppf
    "let tuple%d_encode %a (%a) = Mprim {prim = `Pair;args=[%s];annots=[]}" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "enc%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "x%d" i))
    (list_integers i)
    (String.concat ";"
       (List.map (fun i -> Format.sprintf "enc%d x%d" i i) (list_integers i)))

let ntuple_to_Sntuple ppf n =
  let open Format in
  fprintf ppf "let ntuple_to_Sntuple =\nfun x (%a) -> (x,%a) in"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "x%d" i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "x%d" i))
    (list_integers n)

let tuple_decode_generate_complex ppf i =
  let open Format in
  fprintf ppf
    "let tuple%d_decode %a =\n\
    \     %a\n\
    \     function\n\
    \  | Mprim {prim = `Pair; args = [m1;m2]; _} | Mseq [m1;m2] ->\n\
    \     let d1 = dec1 m1 in\n\
    \     let d2 = tuple%d_decode %a (Mseq [m2]) in\n\
    \     ntuple_to_Sntuple d1 d2\n\
    \  | Mprim {prim = `Pair; args = m1::m; _} | Mseq (m1::m) ->\n\
    \     let d1 = dec1 m1 in\n\
    \     let d2 = tuple%d_decode %a (Mseq m) in\n\
    \     ntuple_to_Sntuple d1 d2\n\
    \  | expr -> fail_on_micheline \"Invalid tuple2\" expr" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "dec%d" i))
    (list_integers i) ntuple_to_Sntuple (i - 1) (i - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "dec%d" i))
    (list_integers ~from:2 i) (i - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "dec%d" i))
    (list_integers ~from:2 i)

(* let tuple3_micheline a_mich b_mich c_mich = Mprim {prim = `pair; args = [a_mich;b_mich;c_mich]; annots = []} *)
let tuple_micheline_generate ppf i =
  let open Format in
  fprintf ppf
    "let tuple%d_micheline %a = Mprim {prim = `pair; args = [%s]; annots = []}"
    i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "mich%d" i))
    (list_integers i)
    (String.concat ";"
       (List.map (fun i -> Format.sprintf "mich%d" i) (list_integers i)))

let tuple_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:3 in
  Format.asprintf "\n%a\n\n%a\n%a\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> tuple_generator_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> tuple_encode_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> tuple_decode_generate_complex ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> tuple_micheline_generate ppf i))
    (list_integers n)
