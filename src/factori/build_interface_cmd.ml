(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `factori_command.ml` module defines a `subcommand` function that can be
used to define subcommands for the `Factori` application, along with imports of
the `Factori_options` and `Factori_errors` modules. The file includes commented-
out code for a `build_interface` subcommand, which is not currently being used
in the application. Warning: this short summary was automatically generated and
could be incomplete or misleading. If you feel this is the case, please open an
issue. *)

open Factori_command
open Factori_options
open Factori_errors

let ( // ) = Filename.concat

(* let build_interface =
 *   subcommand ~name:"build interface" ~args:[contract_name_option]
 *     ~doc:
 *       "Build (or re-build) a SDK for an existing contract (either created \
 *        inside the project or already imported)." (fun () ->
 *       try
 *         let contract_name =
 *           match !contract_name with
 *           | None ->
 *             raise
 *               (GenericError
 *                  ( "[build_interface]",
 *                    " No name provided for the contract whose interface you \
 *                     want to build" ))
 *           | Some contract_name -> contract_name in
 *         match Factori_config.get_contract_opt contract_name with
 *         | None ->
 *           Format.eprintf
 *             "The contract specified does not belong to the project or has not \
 *              been correctly added to the local configuration file (%s)"
 *             (Factori_config.get_local_config_name
 *                ~dir:(Factori_config.get_dir ()))
 *         | Some { contract_name; original_format; original_kt1 } ->
 *           let filename =
 *             File_structure.get_contract_path ~contract_name
 *               ~format:original_format in
 *           michelson_file := Some filename ;
 *           Factori_utils.output_verbose
 *             (Format.sprintf "Building interface from file: %s\n" filename) ;
 *           Factori_utils.try_overflow ~msg:"import_from_michelson"
 *             (Import_kt1.import_from_michelson ~original_kt1
 *                ~ctrct_name:contract_name)
 *       with e -> handle_exception e) *)
