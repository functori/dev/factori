(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module provides functions to generate boilerplate code for
encoding and decoding tuples in Python. The module contains three functions, two
of which generate Python functions that encode and decode tuples with a
specified number of elements, and one generates a string containing boilerplate
code for encoding and decoding tuples with a specified number of elements.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Factori_utils

let tuple_type_generate ppf i =
  let open Format in
  let l_i = list_integers i in
  let str_l_i = List.map (Format.sprintf "class%d") l_i in
  let pp_class ppf l =
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "%s" i))
      ppf l in
  fprintf ppf
    "def tuple%d_type(%a) -> Type[PairType] :\n\
     \treturn PairType.create_type([%a])\n\n"
    i pp_class str_l_i pp_class str_l_i
(* (pp_print_list
 *    ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
 *    (fun ppf i -> fprintf ppf "x%d" i))
 * (list_integers i) *)

let tuple_encode_generate ppf i =
  let open Format in
  fprintf ppf
    "def tuple%d_encode(%a):\n\
     \tdef result(arg : Tuple) -> PairType:\n\
     \t\ttry:\n\
     \t\t\treturn PairType.from_comb([%s])\n\
     \t\texcept Exception as e:\n\
     \t\t\tprint('Error in tuple%d_encode_full')\n\
     \t\t\tprint(e)\n\
     \t\t\traise(e)\n\n\
     \treturn result" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "enc%d" i))
    (list_integers i)
    (* (pp_print_list
     *    ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
     *    (fun ppf i -> fprintf ppf "x%d" i))
     * (list_integers i) *)
    (String.concat ", "
       (List.map
          (fun i -> Format.sprintf "enc%d(arg[%d])" i (i - 1))
          (list_integers i)))
    i

let tuple_encode_full_generate ppf i =
  let open Format in
  fprintf ppf
    "def tuple%d_encode_full(%a):\n\
     \tencode = tuple%d_encode(%a)\n\
     \tdef result(arg : Tuple) -> Micheline:\n\
     \t\ttry:\n\
     \t\t\treturn encode(arg).to_micheline_value(lazy_diff=None)\n\
     \t\texcept Exception as e:\n\
     \t\t\tprint('Error in tuple%d_encode_full')\n\
     \t\t\tprint(e)\n\
     \t\t\traise(e)\n\n\
     \treturn result" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "enc%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "enc%d" i))
    (list_integers i)
    (* (pp_print_list
     *    ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
     *    (fun ppf i -> fprintf ppf "x%d" i))
     * (list_integers i) *)
    i

let repeat n x = List.map (fun _ -> x) (list_integers ~from:1 n)

let tuple_decode_generate ppf i =
  let open Format in
  fprintf ppf
    "def tuple%d_decode(%a):\n\
     \tdef result(p) -> Tuple[%a]:\n\
     \t\ttry:\n\
     \t\t\tx1 = p.items[0]\n\
     \t\t\tprerest : Tuple[%a] = tuple%d_decode(%a)(p.items[1])\n\
     \t\t\tfirst : Tuple[T1] = (dec1(x1),)\n\
     \t\t\trest : Tuple[%a] = first + prerest\n\
     \t\t\treturn rest\n\
     \t\texcept Exception as e:\n\
     \t\t\tprint('Error in tuple%d_decode_full')\n\
     \t\t\tprint(e)\n\
     \t\t\traise e\n\n\
     \treturn result\n\n\n"
    i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "dec%d : Callable[[MichelsonType],T%d]" i i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:2 i) (i - 1)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "dec%d" i))
    (list_integers ~from:2 i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers ~from:1 i) i

let tuple_decode_from_micheline_generate ppf i =
  let open Format in
  let l_i = list_integers i in
  let str_l_i = List.map (Format.sprintf "class%d") l_i in
  let pp_class ppf l_i =
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf s -> fprintf ppf "%s : Type[MichelsonType]" s))
      ppf l_i in
  fprintf ppf
    "def tuple%d_decode_from_micheline(%a):\n\
     \tdef result(p) -> PairType:\n\
     \t\tcls = tuple%d_type(%a)\n\
     \t\treturn cls.from_micheline_value(p)\n\
     \treturn result\n\n\n"
    i pp_class str_l_i i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf s -> fprintf ppf "%s" s))
    str_l_i

let tuple_decode_full_generate ppf i =
  let open Format in
  fprintf ppf
    "def tuple%d_decode_full(%a,%a):\n\
     \tdecode_from_micheline=tuple%d_decode_from_micheline(%a)\n\
     \tdecode=tuple%d_decode(%a)\n\n\n\
     \tdef result(p) -> Tuple[%a]:\n\
     \t\ttry:\n\
     \t\t\tp_ty = decode_from_micheline(p)\n\
     \t\t\treturn decode(p_ty)\n\
     \t\texcept Exception as e:\n\
     \t\t\tprint('Error in tuple%d_decode_full')\n\
     \t\t\tprint(e)\n\
     \t\t\traise e\n\n\
     \treturn result\n\n\n\
     assert tuple%d_decode_full(%a)(tuple%d_encode_full(%a)((%a))) == (%a)\n"
    i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "dec%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "class%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "class%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "dec%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "T%d" i))
    (list_integers i) i i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf s -> fprintf ppf "%s" s))
    (repeat i "int_michelson_decode" @ repeat i "int_michelson_type()")
    i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf s -> fprintf ppf "%s" s))
    (repeat i "int_michelson_encode")
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "%d" i))
    (list_integers ~from:1 i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
       (fun ppf i -> fprintf ppf "%d" i))
    (list_integers ~from:1 i)

let tuple_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:3 in
  Format.asprintf "%a\n\n%a\n\n%a\n\n%a\n\n%a\n\n%a\n\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_type_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_encode_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_decode_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_decode_from_micheline_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n\n")
       (fun ppf i -> tuple_encode_full_generate ppf i))
    (list_integers n)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> tuple_decode_full_generate ppf i))
    (list_integers n)
