(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml module `python_types.ml` provides encoding and decoding functions
for different Tezos types, including base types, `map`, and `big_map` types. It
also offers helper functions to generate random values of these types, defines
the `Set` class, and functions for encoding and decoding lists and tuples.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Python_generator_generation
open Format

let python_types =
  let prelude =
    {|

from typing import Type, Any, List, NewType, TypeVar, Dict, Callable, Optional, Union, Generic, Tuple, cast

from random import random
from math import floor
from pytezos.michelson.parse import michelson_to_micheline
from pytezos.michelson.types.base import MichelsonType
from pytezos.context.abstract import AbstractContext
from pytezos.michelson.types.domain import KeyType, TimestampType, MutezType, SignatureType
from pytezos.michelson.types.core import UnitType
from pytezos.michelson.types.core import NeverType
from pytezos.michelson.types.core import StringType
from pytezos.michelson.types.core import IntType
from pytezos.michelson.types.core import NatType
from pytezos.michelson.types.core import BytesType
from pytezos.michelson.types.core import BoolType
from pytezos.michelson.types.core import UnitLiteral, unit
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.option import OptionType
from pytezos.michelson.micheline import Micheline
from pytezos.michelson.types.list import ListType
from pytezos.michelson.types.set import SetType
from pytezos.michelson.types.map import MapType
from pytezos.michelson.types.big_map import BigMapType
from pytezos.michelson.types.sum import OrType
from pytezos.michelson.types.domain import KeyHashType
from pytezos.michelson.types.domain import ChainIdType
from pytezos.michelson.types.domain import ContractType
from pytezos.michelson.types.domain import AddressType
from pytezos.michelson.types.domain import LambdaType
from pytezos.michelson.types.operation import OperationType
from pytezos.michelson.types.ticket import TicketType



# Exceptions


class NeverHappens(Exception):
    pass


class NotElementOfType(Exception):
    pass


# Utils
T = TypeVar('T')
Y = TypeVar('Y')

def decode_from_list(x_decode : Callable[[MichelsonType],T]) -> Callable[...,list[T]]:
    def result(l):
        res = []
        for x in l:
            res.append(x_decode(x))
        return res

    return result


def make_left(x, right_type=MichelsonType):
    return OrType.from_left(x,right_type)


def make_right(y, left_type=MichelsonType):
    return OrType.from_right(y, left_type)

# Bigmaps
# Abstract big maps are represented by an int
# Literal big maps are represented by a list of pairs of (key,value)


# Contract = NewType('Contract', str)

KT = TypeVar('KT')
VT = TypeVar('VT')


class Map(Generic[KT, VT]):
    def __init__(self, mydict=None):
        # Initialize the dictionary that will store the key-value pairs
        if mydict is None:
            mydict = {}
        self.dictionary = mydict

    def set(self, key: KT, value: VT):
        # Set the value for the given key in the dictionary
        self.dictionary[key] = value

    def get(self, key: KT) -> VT:
        # Return the value for the given key from the dictionary
        value: VT = self.dictionary[key]
        return value

    def is_empty(self) -> bool:
        # Return True if the dictionary is empty, False otherwise
        return len(self.dictionary) == 0

    def __eq__(self, other):
        # Check if the other object is a Map and if its dictionary is equal to this Map's dictionary
        return isinstance(other, Map) and self.dictionary == other.dictionary

# This class BigMap is the python representation of the Michelson BigMap
class BigMap(Generic[KT, VT]):
    def __init__(self, value: Map[KT, VT] | int):
        self.value = value

    # Define the BigMap class as a subtype of Union, which can be either a Map or an int
    pass

#
# preBigMap = Union[Map[KT, VT], int]
#
#
# class BigMap(preBigMap):
#     pass


# Map = Dict[X, Y]


def string_encode(s: str) -> StringType:
    return StringType.from_value(s)


def string_decode(m: MichelsonType) -> str:
    if isinstance(m,StringType):
        return str(m)
    else:
        raise(ValueError("[string_decode] Not a String"))
    
def string_decode_from_micheline(m: Micheline) -> StringType:
    return StringType.from_micheline_value(m)

def string_encode_full(s : str):
    str_ty = string_encode(s)
    return str_ty.to_micheline_value()

def string_decode_full(s : Micheline):
    str_ty = string_decode_from_micheline(s)
    return string_decode(str_ty)

def string_type() -> Type[MichelsonType] :
    return StringType.create_type([])


assert (string_decode_full(string_encode_full('coucou')) == 'coucou')

assert (string_decode(string_encode('coucou')) == 'coucou')

signature = str


def signature_encode(s):
    return SignatureType.from_value(s)


def signature_decode(s: MichelsonType) -> str:
    if isinstance(s, SignatureType):
        return s.to_python_object()
    else:
        raise ValueError("Input is not a valid SignatureType")
    
def signature_decode_from_micheline(s : Micheline) -> MichelsonType:
    return SignatureType.from_micheline_value(s)

def signature_type() -> Type[MichelsonType]:
    return SignatureType.create_type([])

def signature_encode_full(s : signature):
    sig_ty = signature_encode(s)
    return sig_ty.to_micheline_value()

def signature_decode_full(s : Micheline):
    sig_ty = signature_decode_from_micheline(s)
    return signature_decode(sig_ty)

assert(signature_decode(signature_encode('edsigthTzJ8X7MPmNeEwybRAvdxS1pupqcM5Mk4uCuyZAe7uEk68YpuGDeViW8wSXMrCi5CwoNgqs8V2w8ayB5dMJzrYCHhD8C7')) == 'edsigthTzJ8X7MPmNeEwybRAvdxS1pupqcM5Mk4uCuyZAe7uEk68YpuGDeViW8wSXMrCi5CwoNgqs8V2w8ayB5dMJzrYCHhD8C7')

key = str


def key_encode(k):
    return KeyType.from_value(k)


def key_decode(k: MichelsonType) -> key:
    if isinstance(k, KeyType):
        return key(k)
    else:
        raise ValueError("Input is not a valid KeyType")

def key_decode_from_micheline(k : Micheline) -> MichelsonType :
    return KeyType.from_micheline_value(k)

def key_type() -> Type[MichelsonType]:
    return KeyType.create_type([])

def key_encode_full(k : key):
    key_ty = key_encode(k)
    return key_ty.to_micheline_value()

def key_decode_full(k : Micheline):
    key_ty = key_decode_from_micheline(k)
    return key_decode(key_ty)

assert(key_decode(key_encode('edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn')) == 'edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn')

key_hash = str


def key_hash_encode(k):
    return KeyHashType.from_value(k)


def key_hash_decode(k: MichelsonType) -> key_hash:
    if isinstance(k, KeyHashType):
        return key_hash(k)
    else:
        raise ValueError("Input is not a valid KeyType")

def key_hash_decode_from_micheline(k : Micheline) -> MichelsonType :
    return KeyHashType.from_micheline_value(k)

def key_hash_type() -> Type[MichelsonType]:
    return KeyHashType.create_type([])

def key_hash_encode_full(k_h : key_hash):
    return key_hash_encode(k_h).to_micheline_value()

def key_hash_decode_full(k_h : Micheline):
    return key_hash_decode(key_hash_decode_from_micheline(k_h))

assert(key_hash_decode(key_hash_encode('tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb')) == 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb')

chain_id = str | bytes

def chain_id_encode(c):
    return ChainIdType.from_value(c)


def chain_id_decode(c: MichelsonType) -> str:
    if isinstance(c, ChainIdType):
        return str(c)
    else:
        raise ValueError("Input is not a valid ChainIdType")

def chain_id_decode_from_micheline(c):
    return ChainIdType.from_micheline_value(c)


def chain_id_type() -> Type[MichelsonType]:
    return ChainIdType.create_type([])

def chain_id_encode_full(c : chain_id):
     return chain_id_encode(c).to_micheline_value()

def chain_id_decode_full(c : Micheline):
     return chain_id_decode(chain_id_decode_from_micheline(c))

#assert(chain_id_decode(chain_id_decode_from_micheline(chain_id_encode(bytes.fromhex('7a06a770')).to_micheline_value())) == bytes.fromhex('7a06a770'))

assert(chain_id_decode(chain_id_encode('NetXynUjJNZm7wi')) == 'NetXynUjJNZm7wi')
contract = str

def contract_type(arg_class : Type[MichelsonType]) -> Type[MichelsonType]:
    return ContractType.create_type([arg_class])

def contract_encode(c: contract):
    try:
        return ContractType.from_value(c)
    except Exception as e:
        print("Error in contract_encode on input " + str(c) + ' with error: ' + str(e))
        raise e

def contract_decode(c: MichelsonType) -> Any:
    if isinstance(c, ContractType):
        try:
            return c.to_python_object()
        except Exception as e:
            print("Error in contract_decode on input" + str(c.to_micheline_value()) + ' and error: ' + str(e))
            raise e
    else:
        raise ValueError("Input is not a valid ContractType")
    
def contract_decode_from_micheline(c: Micheline) -> MichelsonType:
    try:
        return ContractType.from_micheline_value(c)
    except Exception as e:
        print("Error in contract_encode on input " + str(c) + ' with error: ' + str(e))
        raise e

def contract_encode_full(c : contract):
    return contract_encode(c).to_micheline_value()

def contract_decode_full(c : Micheline):
    return contract_decode(contract_decode_from_micheline(c))

Lambda = LambdaType

def lambda_encode(x=None,y=None):
    def result(l: Lambda) -> LambdaType:
        return l

    return result

def lambda_decode(x=None,y=None):
    def result(l: MichelsonType) -> Lambda:
        if isinstance(l, LambdaType):
            return l
        else:
            raise ValueError("Input is not a valid LambdaType")
    return result

def lambda_decode_from_micheline(x=None,y=None):
    def result(l: Micheline) -> MichelsonType:
        return LambdaType.from_micheline_value(l)
    return result

def lambda_type(k_type : Type[MichelsonType], v_type : Type[MichelsonType]) -> Type[MichelsonType]:
    return LambdaType.create_type([k_type,v_type])

def lambda_encode_full(l : Lambda):
    return lambda_encode(l).to_micheline_value()

def lambda_decode_full(l : Micheline):
    return lambda_decode(lambda_decode_from_micheline(l))

Operation = OperationType


def operation_encode(o: Operation) -> OperationType:
    return o


def operation_decode(o: MichelsonType) -> Operation:
    if isinstance(o, OperationType):
        return o
    else:
        raise ValueError("Input is not a valid OperationType")

def operation_decode_from_micheline(m : Micheline) -> MichelsonType:
    return OperationType.from_micheline_value(m)

def operation_type() -> Type[MichelsonType]:
    return OperationType.create_type([])

def operation_encode_full(op : Operation):
    return operation_encode(op).to_micheline_value()

def operation_decode_full(op : Micheline):
    return operation_decode(operation_decode_from_micheline(op))

Ticket = TicketType

def ticket_type(arg_class : Type[MichelsonType]) -> Type[MichelsonType]:
    return TicketType.create_type([arg_class])

def ticket_encode(t: Ticket) -> TicketType:
    return t

def ticket_decode(t: MichelsonType) -> Ticket:
    if isinstance(t, TicketType):
        return t
    else:
        raise ValueError("Input is not a valid TicketType")
    
def ticket_decode_from_micheline(t: Micheline, arg_class : Type[MichelsonType]) -> MichelsonType:
    cls = ticket_type(arg_class)
    return cls.from_micheline_value(t)

def ticket_encode_full(op : Ticket):
    return ticket_encode(op).to_micheline_value()

def ticket_decode_full(op : Micheline, arg_class):
    return ticket_decode(ticket_decode_from_micheline(op,arg_class))
    
timestamp = int | str

def timestamp_type() -> Type[MichelsonType]:
    return TimestampType.create_type([])

def timestamp_encode(t: timestamp) -> MichelsonType:
    return TimestampType.from_python_object(t)


def timestamp_decode(t: MichelsonType) -> int:
    if isinstance(t, TimestampType):
        return TimestampType.to_python_object(t)
    else:
        raise ValueError("Input is not a valid TimestampType")
    
def timestamp_decode_from_micheline(t: Micheline) -> MichelsonType:
    return TimestampType.from_micheline_value(t)

def timestamp_encode_full(op : timestamp):
    return timestamp_encode(op).to_micheline_value()

def timestamp_decode_full(op : Micheline):
    return timestamp_decode(timestamp_decode_from_micheline(op))

assert(timestamp_decode(timestamp_encode(1702474034)) == 1702474034)

assert(timestamp_decode(timestamp_encode("2023-06-22T14:23:25Z")) == 1687443805)


tez = int

def tez_type() -> Type[MichelsonType] :
    return MutezType.create_type([])
    
def tez_encode(t : tez):
    return MutezType.from_value(t)


def tez_decode(t : MichelsonType) -> tez:
    if isinstance(t, MutezType):
        res : tez = int(t)
        return res
    else:
        raise ValueError("Input is not a valid MutezType")

def tez_decode_from_micheline(t : Micheline) -> MichelsonType :
    return MutezType.from_micheline_value(t)

def tez_encode_full(op : tez):
    return tez_encode(op).to_micheline_value()

def tez_decode_full(op : Micheline):
    return tez_decode(tez_decode_from_micheline(op))


assert (tez_decode(tez_encode(42)) == 42)

address = str

def address_type() -> Type[MichelsonType] :
    return AddressType.create_type([])
    
def address_encode(t : address):
    return AddressType.from_value(t)


def address_decode(t : MichelsonType) -> address:
    if isinstance(t, AddressType):
        return t.to_python_object()
    else:
        raise ValueError("Input is not a valid AddressType")

def address_decode_from_micheline(t : Micheline) -> MichelsonType :
    return AddressType.from_micheline_value(t)

def address_encode_full(op : address):
    return address_encode(op).to_micheline_value()

def address_decode_full(op : Micheline):
    return address_decode(address_decode_from_micheline(op))


assert (address_decode(address_encode('tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb')) == 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb')

int_michelson = int

def int_michelson_type() -> Type[MichelsonType]:
    return IntType.create_type([])

def int_michelson_encode(i: int_michelson) -> IntType:
    return IntType.from_value(i)


def int_michelson_decode(m: MichelsonType) -> int_michelson:
    if isinstance(m, IntType):
        return int(m)
    else:
        raise ValueError("Input is not a valid IntType")

def int_michelson_decode_from_micheline(i: Micheline) -> IntType:
    return IntType.from_micheline_value(i)

def int_michelson_encode_full(op : int_michelson):
    return int_michelson_encode(op).to_micheline_value()

def int_michelson_decode_full(op : Micheline):
    return int_michelson_decode(int_michelson_decode_from_micheline(op))

# print("int_michelson_decode: " + str(int_michelson_decode(int_michelson_encode(42))))
assert (int_michelson_decode(int_michelson_encode(42)) == 42)

nat = int

def nat_type() -> Type[MichelsonType]:
    return NatType.create_type([])

def nat_encode(i: nat) -> NatType:
    return NatType.from_value(i)


def nat_decode(m: MichelsonType) -> nat:
    if isinstance(m, NatType):
        return int(m)
    else:
        raise ValueError("Input is not a valid NatType")

def nat_decode_from_micheline(i: Micheline) -> NatType:
    return NatType.from_micheline_value(i)


def nat_encode_full(op : nat):
    return nat_encode(op).to_micheline_value()

def nat_decode_full(op : Micheline):
    return nat_decode(nat_decode_from_micheline(op))

assert (nat_decode(nat_encode(42)) == 42)


def bytes_type() -> Type[MichelsonType]:
    return BytesType.create_type([])

def bytes_encode(b: bytes) -> BytesType:
    return BytesType.from_value(b)


def bytes_decode(m: MichelsonType) -> bytes:
    if isinstance(m, BytesType):
        return bytes(m)
    else:
        raise ValueError("Input is not a valid BytesType")

def bytes_decode_from_micheline(b: Micheline) -> BytesType:
    return BytesType.from_micheline_value(b)


def bytes_encode_full(op : bytes):
    return bytes_encode(op).to_micheline_value()

def bytes_decode_full(op : Micheline):
    return bytes_decode(bytes_decode_from_micheline(op))

assert (bytes_decode(bytes_encode(bytes.fromhex('ff'))) == bytes.fromhex('ff'))

def bool_type() -> Type[MichelsonType]:
    return BoolType.create_type([])

def bool_encode(b: bool) -> BoolType:
    return BoolType.from_value(b)


def bool_decode(m: MichelsonType) -> bool:
    if isinstance(m, BoolType):
        return bool(m)
    else:
        raise ValueError("Input is not a valid BoolType")

def bool_decode_from_micheline(b: Micheline) -> BoolType:
    return BoolType.from_micheline_value(b)

def bool_encode_full(op : bool):
    return bool_encode(op).to_micheline_value()

def bool_decode_full(op : Micheline):
    return bool_decode(bool_decode_from_micheline(op))

assert (bool_decode(bool_encode(True)) == True)
assert (bool_decode(bool_encode(False)) == False)

def unit_type() -> Type[MichelsonType]:
    return UnitType.create_type([])

def unit_encode(u: Any) -> UnitType:
    return UnitType.dummy(u)


def unit_decode(m: MichelsonType) -> unit:
    if isinstance(m, UnitType):
        return unit()
    else:
        raise ValueError("Input is not a valid UnitType")
    
def unit_decode_from_micheline(u: Micheline) -> MichelsonType:
    return UnitType.from_micheline_value(u)

def unit_encode_full(op : Any):
    return unit_encode(op).to_micheline_value()

def unit_decode_full(op : Micheline):
    return unit_decode(unit_decode_from_micheline(op))

assert (unit_decode(unit_encode(None)) == unit())
assert (unit_decode(unit_encode([])) == unit())


def never_type() -> Type[MichelsonType]:
    return NeverType.create_type([])

def never_encode(x) -> NeverType:
    raise (NeverHappens("never say never"))


def never_decode(m: MichelsonType) -> Any:
    if isinstance(m, NeverType):
        raise (NeverHappens("never say never"))
    else:
        raise ValueError("Input is not a valid NeverType")
    
def never_decode_from_micheline(x : Micheline) -> NeverType:
    raise (NeverHappens("never say never"))

def never_encode_full(op):
    return never_encode(op).to_micheline_value()

def never_decode_full(op : Micheline):
    return never_decode(never_decode_from_micheline(op))

def option_type(arg_class : Type[MichelsonType]) -> Type[MichelsonType]:
    return OptionType.create_type([arg_class])


def option_encode(x_encode : Callable[[Optional[T]],MichelsonType], a_class : Type[MichelsonType]): # MichelsonType must be the same
    def result(x: Optional[T]):
        if x is None:
            return OptionType.none(a_class)
        else:
            return OptionType.from_some(x_encode(x))

    return result


def option_decode(x_decode: Callable[[MichelsonType], T]) -> Callable[[MichelsonType], Optional[T]]:
    def result(x: MichelsonType) -> Optional[T]:
        if isinstance(x, OptionType):
            if x.is_none():
                return None
            else:
                return x_decode(x.get_some())
        else:
            raise ValueError("Input is not a valid OptionType")
    return result

def option_decode_from_micheline(x_class : Type[MichelsonType]): # MichelsonType must be the same
    def result(x : Micheline):
        cls = option_type(x_class)
        return cls.from_micheline_value(x)
    return result

def option_encode_full(x_encode : Callable[[Optional[T]],MichelsonType],x_class):
    encode = option_encode(x_encode,x_class)
    def result(x : Optional[T]):
        return encode(x).to_micheline_value(lazy_diff=None)
    return result

def option_decode_full(x_decode: Callable[[MichelsonType], T] , x_class : Type[MichelsonType]):
    decode_from_micheline = option_decode_from_micheline(x_class)
    decode = option_decode(x_decode)
    def result(x : Micheline):
        return decode(decode_from_micheline(x))
    return result

# def option_decode(x_decode):
#     def result(x: OptionType[T]):
#         if x.is_none():
#             return None
#         else:
#             return x.get_some()
#
#     return result


assert (option_decode(int_michelson_decode)(option_encode(int_michelson_encode,int_michelson_type())(3)) == 3)
assert (option_decode_full(int_michelson_decode, int_michelson_type())(option_encode_full(int_michelson_encode,int_michelson_type())(None)) is None)

# Michelson Key type
MKT = TypeVar('MKT')
# Michelson Value type
MVT = TypeVar('MVT')

def map_type(k_class : Type[MichelsonType], v_class : Type[MichelsonType]) -> Type[MichelsonType]:
    return MapType.create_type([k_class,v_class])

def map_encode(k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType], k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    def result(m: Map[KT, VT]):
        if m.is_empty():
            return MapType.empty(k_class, v_class)
        else:
            encoded_keys = map(k_encode, m.dictionary.keys())
            encoded_values = map(v_encode, m.dictionary.values())
            pre_res = list(zip(encoded_keys, encoded_values))
            res: MapType = MapType.from_items(pre_res)
            return res

    return result


def map_decode(k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT]):
    def result(m: MichelsonType) -> Map:
        if isinstance(m, MapType):
            preres: dict = {}
            for k, v in m.items:
                preres[k_decode(k)] = v_decode(v)
            res: Map = Map(preres)
            return res
        else:
            raise ValueError("Input is not a valid MapType")
    return result

def map_decode_from_micheline(k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    cls = map_type(k_class,v_class)
    def result(m: Micheline):
        return cls.from_micheline_value(m)
    return result

def map_encode_full(k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType], k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    encode = map_encode(k_encode,v_encode,k_class,v_class)
    def result(x : Map):
        return encode(x).to_micheline_value(lazy_diff=None)
    return result

def map_decode_full(k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT], k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    decode_from_micheline = map_decode_from_micheline(k_class,v_class)
    decode = map_decode(k_decode,v_decode)
    def result(x : Micheline):
        return decode(decode_from_micheline(x))
    return result


#
# def map_encode(k_encode, v_encode):
#     def result(m: Map):
#         res = MapType.empty(X, Y)
#         for k in m.keys():
#             res = res.update(k_encode(k), v_encode(m[k]))
#         return res
#
#     return result



assert (map_decode(int_michelson_decode, string_decode)(map_encode(int_michelson_encode, string_encode,int_michelson_type(),string_type())(Map({1: 'mary'}))) == Map(
    {1: 'mary'}))

def big_map_type(k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    return BigMapType.create_type([k_class,v_class])

# Big map encoding is a special case where we need the class of the key and value because is just an integer and to benefit some useful
# pytezos functions on big_map we need to know the types (maybe in the future other encoding will also need class)

def big_map_encode(k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType],k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    bm_class = big_map_type(k_class, v_class)
    def result(m: BigMap[KT, VT]):
        if isinstance(m.value, int):
            #here we make the choice to return an empty bigmap to make deployment easier
            return bm_class(ptr=m.value, items=[])
        elif isinstance(m.value,Map):
            if m.value.is_empty():
                return BigMapType.empty(k_class, v_class)
            else:
                encoded_keys = map(k_encode, m.value.dictionary.keys())
                encoded_values = map(v_encode, m.value.dictionary.values())
                pre_res = list(zip(encoded_keys, encoded_values))
                res = bm_class(items=pre_res)
                return res

    return result


def big_map_decode(k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT]):
    def result(m: MichelsonType) -> BigMap:
        if isinstance(m, BigMapType):
            bm = cast(BigMapType,m)
            if bm.ptr is not None:
                return BigMap(int(bm.ptr))
            preres: dict = {}
            for k, v in bm.items:
                preres[k_decode(k)] = v_decode(v)
            res: BigMap = BigMap(Map(preres))
            return res
        else:
            raise ValueError("Input is not a valid MyBigMapType")
    return result

def big_map_decode_from_micheline(k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    bm_class = big_map_type(k_class, v_class)
    def result(m: Micheline):
        return bm_class.from_micheline_value(m)
    return result

def big_map_encode_full(k_encode: Callable[[KT], MichelsonType], v_encode: Callable[[VT], MichelsonType], k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    encode = big_map_encode(k_encode,v_encode,k_class,v_class)
    def result(x : BigMap):
        return encode(x).to_micheline_value(lazy_diff=None)
    return result

def big_map_decode_full(k_decode: Callable[[MichelsonType], KT], v_decode: Callable[[MichelsonType], VT], k_class : Type[MichelsonType], v_class : Type[MichelsonType]):
    decode_from_micheline = big_map_decode_from_micheline(k_class,v_class)
    decode = big_map_decode(k_decode,v_decode)
    def result(x : Micheline):
        return decode(decode_from_micheline(x))
    return result

def list_type(arg_class : Type[MichelsonType]) -> Type[MichelsonType]:
    return ListType.create_type([arg_class])

def list_encode(x_encode, x_class):
    cls = ListType.create_type([x_class])
    def result(l) -> ListType:
        lres = []
        for i in l:
            lres.append(x_encode(i))
        res = cls(lres)
        return res
    return result


def list_decode(x_decode: Callable[[MichelsonType], T]):
    def result(l: MichelsonType) -> List[T]:
        if isinstance(l, ListType):
            return decode_from_list(x_decode)(l)
        else:
            raise ValueError("Input is not a valid ListType")
    return result

def list_decode_from_micheline(x_class):
    cls = list_type(x_class)
    def result(l) -> ListType:
        return cls.from_micheline_value(l)
    return result

def list_encode_full(x_encode : Callable[[T],MichelsonType], x_class : Type[MichelsonType]):
    encode = list_encode(x_encode,x_class)
    def result(x : List[T]):
        return encode(x).to_micheline_value(lazy_diff=None)
    return result

def list_decode_full(x_decode: Callable[[MichelsonType], T] , x_class : Type[MichelsonType]):
    decode_from_micheline = list_decode_from_micheline(x_class)
    decode = list_decode(x_decode)
    def result(x : Micheline):
        return decode(decode_from_micheline(x))
    return result

toto = (list_encode(int_michelson_encode,int_michelson_type())([1, 2, 3, 4]))
tata = list_decode(int_michelson_decode)(toto)
assert (list_decode(int_michelson_decode)(list_encode(
    int_michelson_encode,int_michelson_type())([1, 2, 3, 4])) == [1, 2, 3, 4])


class Set(Generic[T]):
    def __init__(self, mylist: List[T]):
        self.list = mylist

def set_type(arg_class : Type[MichelsonType]):
    return SetType.create_type([arg_class])

def set_encode(x_encode: Callable[[T], MichelsonType], a_class : Type[MichelsonType]):
    cls = SetType.create_type([a_class])
    def result(x: Set[T]) -> SetType:
        res: SetType = cls([x_encode(i) for i in x.list])
        return res
    return result


def set_decode(x_decode: Callable[[MichelsonType], T]):
    def result(x: MichelsonType) -> Set:
        if isinstance(x, SetType):
            preres = [x_decode(i) for i in x.items]
            return Set(preres)
        else:
            raise ValueError("Input is not a valid SetType")
    return result

def set_decode_from_micheline(x_class : Type[MichelsonType]):
    cls = set_type(x_class)
    def result(x: Micheline) -> SetType:
        return cls.from_micheline_value(x)
    return result


def set_encode_full(x_encode : Callable[[T],MichelsonType], x_class : Type[MichelsonType]):
    encode = set_encode(x_encode,x_class)
    def result(x : Set[T]):
        return encode(x).to_micheline_value(lazy_diff=None)
    return result

def set_decode_full(x_decode: Callable[[MichelsonType], T] , x_class : Type[MichelsonType]):
    decode_from_micheline = set_decode_from_micheline(x_class)
    decode = set_decode(x_decode)
    def result(x : Micheline):
        return decode(decode_from_micheline(x))
    return result

def tuple2_type(x_class,y_class):
    return PairType.create_type([x_class,y_class])

def tuple2_encode(x_encode, y_encode):
    def result(arg) -> PairType:
        x = x_encode(arg[0])
        # print("[tuple2_encode] x : " + str(x))
        y = y_encode(arg[1])
        return PairType.from_comb([x, y])

    return result


def tuple2_decode(x_decode, y_decode):
    def result(p: MichelsonType) -> Tuple:
        if isinstance(p, PairType):
            x = p.items[0]
            y = p.items[1]
            res = (x_decode(x), y_decode(y))
            return res
        else:
            raise ValueError("Input is not a valid PairType")
    return result

def tuple2_decode_from_micheline(x_class, y_class):
    cls = tuple2_type(x_class,y_class)
    def result(arg : Micheline) -> PairType:
        return cls.from_micheline_value(arg)
    return result

def tuple2_encode_full(x_encode : Callable[[T],MichelsonType],y_encode : Callable[[Y],MichelsonType]):
    encode = tuple2_encode(x_encode,y_encode)
    def result(p):
        return encode(p).to_micheline_value(lazy_diff=None)
    return result

def tuple2_decode_full(x_decode: Callable[[MichelsonType], T] ,y_decode: Callable[[MichelsonType], T], x_class : Type[MichelsonType],y_class : Type[MichelsonType]):
    decode_from_micheline = tuple2_decode_from_micheline(x_class,y_class)
    decode = tuple2_decode(x_decode,y_decode)
    def result(x : Micheline):
        return decode(decode_from_micheline(x))
    return result


#     tuple3_encode(string_encode, int_michelson_encode, bytes_encode)("toto", 3, bytes.fromhex('ff'))) == ["toto", 3,
#                                                                                                 bytes.fromhex('ff')])


#x1 = int_michelson_encode(2)
# print("x1 : " + str(x1))
#x2 = int_michelson_encode(3)
#x3 = int_michelson_encode(4)
#p1 = PairType.from_comb([x1, x2, x3])
#ep1 = tuple2_encode(string_encode, int_michelson_encode)("toto", 3)
#dp1 = tuple2_decode(string_decode, int_michelson_decode)(ep1)

# print(dp1)
# res = (tuple2_encode(int_michelson_encode, int_michelson_encode)(2, 3))
# print(res)
# resinv = tuple2_decode(string_decode, int_michelson_decode)(res)
# print(resinv)


assert tuple2_decode(string_decode, int_michelson_decode)(
    tuple2_encode(string_encode, int_michelson_encode)(("toto", 3))) == ("toto", 3)


#
# # The from_comb is probably not the right way to build a pair with three elements
# def tuple3_encode(x_encode, y_encode, z_encode):
#     def result(x, y, z) -> PairType:
#         x = x_encode(x)
#         y = y_encode(y)
#         z = z_encode(z)
#         return PairType.from_comb([x, y, z])
#
#     return result
#
#
# # This doesn't type well and there's a chance it won't work on data from the blockchain.. TODO: investigate
# def tuple3_decode(x_decode, y_decode, z_decode):
#     def result(p: PairType):
#         x = p.items[0]
#         rest = tuple2_decode(y_decode, z_decode)((p.items[1]))
#         rest.insert(0, x_decode(x))
#         return rest
#
#     return result


# print(tuple3_decode(string_decode, int_michelson_decode, bytes_decode)(
#     tuple3_encode(string_encode, int_michelson_encode, bytes_encode)("toto", 3, bytes.fromhex('ff'))))
# assert (tuple3_decode(string_decode, int_michelson_decode, bytes_decode)(
|}
  in
  Format.asprintf "%s\n%a\n%a\n\n%a\n%s" prelude auxiliary_functions ()
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n") (fun ppf i ->
         fprintf ppf "T%d = TypeVar('T%d')" i i))
    (Factori_utils.list_integers ~from:1 25)
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n\n") (fun ppf x ->
         fprintf ppf "%a" build_generator x))
    (generate_all_kinds 2 25)
    (Python_tuple_boilerplate.tuple_boilerplate 25)
