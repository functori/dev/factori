BUILD=dune
FACTORI_BUILD=./_build/default/src/factori
TESTS_BUILD=./_build/default/tests
BIN=_bin
SCRIPTS=scripts
GIT=$(shell git log --pretty=format:'%H' -n 1)
RELEASE_VERSION=0.7.1
BRANCH=$(shell git branch --show-current)
ALL_EXAMPLES=$(wildcard examples/*)
ALL_EXAMPLES_MAKE=$(patsubst %,%_example,$(ALL_EXAMPLES))


ifneq ($(BRANCH),master)
	RELEASE_VERSION:=$(RELEASE_VERSION)+dev
endif

.PHONY: test test-run examples

all: version build build-test build-json-viewer copy-binaries

format:
	@dune build @fmt --auto-promote 2> /dev/null || exit 0

install: all
	dune install
	@if [ -d "${HOME}/.local/bin" ]; \
	then \
		cp -f $(BIN)/factori.asm ${HOME}/.local/bin/factori; \
		echo "Factori is installed inside ${HOME}/.local/bin/factori"; \
	else \
		echo "The default directory ${HOME}/.local/bin does not exist"; \
		read -p "Provide a directory where factori will be installed(provide full path): " INSTALL_DIR; \
		cp -f $(BIN)/factori.asm $$INSTALL_DIR/factori; \
	fi \

build:
	@$(BUILD) build src

build-test: build
	@$(BUILD) build tests
	@mkdir -p $(BIN)
	@cp -f $(TESTS_BUILD)/kt1s_tests.exe $(BIN)/factori_kt1s_tests

opam-switch:
	@opam switch create . ocaml-base-compiler.4.14.1 -y --no-install

build-deps:
	$(SCRIPTS)/install-deps.sh

build-json-viewer: build-test
	@make -C json-viewer

examples:
	@make -C examples

copy-binaries:
	@mkdir -p $(BIN)
	@cp -f $(FACTORI_BUILD)/main.exe $(BIN)/factori.asm

test-build-deps-ocaml-only:
	@make -C test/ deps

test-build-deps:
	@make -C test/ deps ts-deps

test-init:
	@mkdir -p test
	./_build/default/tests/test_init.exe
	./_build/default/tests/test.exe

test-python-format:
	@make -C test format-python

test-python-static:
	@make -C test python-static-check

test-python-compile:
	@make -C test python-compile

test-ts:
	@make -C test/ ts
	@node test/src/typescript_sdk/dist/test.js

test-csharp:
	@make -C test/ csharp-init
	@make -C test/ csharp-build
	@dotnet run --project test/src/csharp_sdk

test-run: version
	@make -C test/
	@dune exec test/src/tests/tests.exe

test-analyze:
	./_build/default/tests/test_analyze.exe
	./_build/default/tests/test_permission_tool.exe

test-clean:
	-make -C test/ drop
	@rm -rf ./test/*
	@rm -rf ./testweb/*

test-ocaml-only: version test-clean test-init test-build-deps-ocaml-only test-run

test: test-clean test-init test-csharp test-build-deps test-ts test-run test-analyze

clean:
	$(BUILD) clean

cleanall: clean
	@rm -f _bin/factori.asm

lint:
	@$(SCRIPTS)/lint.sh

version:
	@echo "let version = \"$(RELEASE_VERSION)\"" > src/factori/version.ml
	@echo "let commit = \"$(GIT)\"" >> src/factori/version.ml
