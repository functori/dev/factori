from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

Withdraw = factori_types.tez

def withdraw_generator() -> Withdraw:
	return factori_types.tez_generator ()

def withdraw_encode (arg: Withdraw):
	try:
		return factori_types.tez_encode(arg)
	except Exception as e:
		print('Error in withdraw_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def withdraw_decode (arg) -> Withdraw:
	try:
		return factori_types.tez_decode(arg)
	except Exception as e:
		print('Error in withdraw_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_withdraw():
	temp=withdraw_generator()
	temp_encoded=withdraw_encode(temp)
	try:
		print('++++ withdraw: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of withdraw: " + str(temp))
	temp1=withdraw_decode(temp_encoded)
	if temp1 == temp:
		print('withdraw check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('withdraw check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_withdraw()
#except Exception as e:
#	print('Error in assert_identity_withdraw' + str(e))




def callWithdraw(kt1 : str, _from : blockchain.identity, param : Withdraw, networkName = "ghostnet", debug=False):
	input = withdraw_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "withdraw", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateState = Tuple[factori_types.address,Tuple[factori_types.nat,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,bool]]]]

def updateState_generator() -> UpdateState:
	return (factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
factori_types.bool_generator))))))))()

def updateState_encode(arg : UpdateState) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(factori_types.nat_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.bool_encode))))(arg) 
	except Exception as e:
		print('Error in updateState_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateState_decode(arg) -> UpdateState:
	try:
		return factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(factori_types.nat_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.bool_decode))))(arg) 
	except Exception as e:
		print('Error in updateState_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateState():
	temp=updateState_generator()
	temp_encoded=updateState_encode(temp)
	try:
		print('++++ updateState: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateState: " + str(temp))
	temp1=updateState_decode(temp_encoded)
	if temp1 == temp:
		print('updateState check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateState check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateState()
#except Exception as e:
#	print('Error in assert_identity_updateState' + str(e))




def callUpdateState(kt1 : str, _from : blockchain.identity, param : UpdateState, networkName = "ghostnet", debug=False):
	input = updateState_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateState", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetDelegate = Optional[factori_types.key_hash]

def setDelegate_generator() -> SetDelegate:
	return (factori_types.option_generator (factori_types.key_hash_generator)) ()

def setDelegate_encode (arg: SetDelegate):
	try:
		return factori_types.option_encode(factori_types.key_hash_encode)(arg)
	except Exception as e:
		print('Error in setDelegate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setDelegate_decode (arg) -> SetDelegate:
	try:
		return factori_types.option_decode(factori_types.key_hash_decode)(arg)
	except Exception as e:
		print('Error in setDelegate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setDelegate():
	temp=setDelegate_generator()
	temp_encoded=setDelegate_encode(temp)
	try:
		print('++++ setDelegate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setDelegate: " + str(temp))
	temp1=setDelegate_decode(temp_encoded)
	if temp1 == temp:
		print('setDelegate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setDelegate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setDelegate()
#except Exception as e:
#	print('Error in assert_identity_setDelegate' + str(e))




def callSetDelegate(kt1 : str, _from : blockchain.identity, param : SetDelegate, networkName = "ghostnet", debug=False):
	input = setDelegate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setDelegate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Repay = factori_types.nat

def repay_generator() -> Repay:
	return factori_types.nat_generator ()

def repay_encode (arg: Repay):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in repay_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def repay_decode (arg) -> Repay:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in repay_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_repay():
	temp=repay_generator()
	temp_encoded=repay_encode(temp)
	try:
		print('++++ repay: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of repay: " + str(temp))
	temp1=repay_decode(temp_encoded)
	if temp1 == temp:
		print('repay check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('repay check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_repay()
#except Exception as e:
#	print('Error in assert_identity_repay' + str(e))




def callRepay(kt1 : str, _from : blockchain.identity, param : Repay, networkName = "ghostnet", debug=False):
	input = repay_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "repay", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Liquidate = factori_types.unit

def liquidate_generator() -> Liquidate:
	return factori_types.unit_generator ()

def liquidate_encode (arg: Liquidate):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in liquidate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def liquidate_decode (arg) -> Liquidate:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in liquidate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_liquidate():
	temp=liquidate_generator()
	temp_encoded=liquidate_encode(temp)
	try:
		print('++++ liquidate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of liquidate: " + str(temp))
	temp1=liquidate_decode(temp_encoded)
	if temp1 == temp:
		print('liquidate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('liquidate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_liquidate()
#except Exception as e:
#	print('Error in assert_identity_liquidate' + str(e))




def callLiquidate(kt1 : str, _from : blockchain.identity, param : Liquidate, networkName = "ghostnet", debug=False):
	input = liquidate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "liquidate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = Liquidate


def _default_generator() -> Constructor_default:
	return liquidate_generator ()


def _default_encode (arg: Constructor_default):
	try:
		return liquidate_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def _default_decode (arg) -> Constructor_default:
	try:
		return liquidate_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Borrow = Repay


def borrow_generator() -> Borrow:
	return repay_generator ()


def borrow_encode (arg: Borrow):
	try:
		return repay_encode(arg)
	except Exception as e:
		print('Error in borrow_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def borrow_decode (arg) -> Borrow:
	try:
		return repay_decode(arg)
	except Exception as e:
		print('Error in borrow_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_borrow():
	temp=borrow_generator()
	temp_encoded=borrow_encode(temp)
	try:
		print('++++ borrow: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of borrow: " + str(temp))
	temp1=borrow_decode(temp_encoded)
	if temp1 == temp:
		print('borrow check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('borrow check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_borrow()
#except Exception as e:
#	print('Error in assert_identity_borrow' + str(e))




def callBorrow(kt1 : str, _from : blockchain.identity, param : Borrow, networkName = "ghostnet", debug=False):
	input = borrow_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "borrow", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Owner = factori_types.address

def owner_generator() -> Owner:
	return factori_types.address_generator ()

def owner_encode (arg: Owner):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in owner_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def owner_decode (arg) -> Owner:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in owner_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_owner():
	temp=owner_generator()
	temp_encoded=owner_encode(temp)
	try:
		print('++++ owner: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of owner: " + str(temp))
	temp1=owner_decode(temp_encoded)
	if temp1 == temp:
		print('owner check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('owner check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_owner()
#except Exception as e:
#	print('Error in assert_identity_owner' + str(e))




IsLiquidated = bool

def isLiquidated_generator() -> IsLiquidated:
	return factori_types.bool_generator ()

def isLiquidated_encode (arg: IsLiquidated):
	try:
		return factori_types.bool_encode(arg)
	except Exception as e:
		print('Error in isLiquidated_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def isLiquidated_decode (arg) -> IsLiquidated:
	try:
		return factori_types.bool_decode(arg)
	except Exception as e:
		print('Error in isLiquidated_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_isLiquidated():
	temp=isLiquidated_generator()
	temp_encoded=isLiquidated_encode(temp)
	try:
		print('++++ isLiquidated: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of isLiquidated: " + str(temp))
	temp1=isLiquidated_decode(temp_encoded)
	if temp1 == temp:
		print('isLiquidated check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('isLiquidated check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_isLiquidated()
#except Exception as e:
#	print('Error in assert_identity_isLiquidated' + str(e))




StabilityFeeTokens = factori_types.int_michelson

def stabilityFeeTokens_generator() -> StabilityFeeTokens:
	return factori_types.int_generator ()

def stabilityFeeTokens_encode (arg: StabilityFeeTokens):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in stabilityFeeTokens_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def stabilityFeeTokens_decode (arg) -> StabilityFeeTokens:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in stabilityFeeTokens_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_stabilityFeeTokens():
	temp=stabilityFeeTokens_generator()
	temp_encoded=stabilityFeeTokens_encode(temp)
	try:
		print('++++ stabilityFeeTokens: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of stabilityFeeTokens: " + str(temp))
	temp1=stabilityFeeTokens_decode(temp_encoded)
	if temp1 == temp:
		print('stabilityFeeTokens check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('stabilityFeeTokens check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_stabilityFeeTokens()
#except Exception as e:
#	print('Error in assert_identity_stabilityFeeTokens' + str(e))




class Constructor_storage(TypedDict):
	borrowedTokens: Repay
	interestIndex: StabilityFeeTokens
	isLiquidated: IsLiquidated
	ovenProxyContractAddress: Owner
	owner: Owner
	stabilityFeeTokens: StabilityFeeTokens

def _storage_generator() -> Constructor_storage:
	return {'borrowedTokens' : repay_generator (),'interestIndex' : stabilityFeeTokens_generator (),'isLiquidated' : isLiquidated_generator (),'ovenProxyContractAddress' : owner_generator (),'owner' : owner_generator (),'stabilityFeeTokens' : stabilityFeeTokens_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([(repay_encode(arg['borrowedTokens'])), PairType.from_comb([(stabilityFeeTokens_encode(arg['interestIndex'])), (isLiquidated_encode(arg['isLiquidated']))])]), PairType.from_comb([(owner_encode(arg['ovenProxyContractAddress'])), PairType.from_comb([(owner_encode(arg['owner'])), (stabilityFeeTokens_encode(arg['stabilityFeeTokens']))])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode (repay_decode,(factori_types.tuple2_decode (stabilityFeeTokens_decode,isLiquidated_decode)))),(factori_types.tuple2_decode (owner_decode,(factori_types.tuple2_decode (owner_decode,stabilityFeeTokens_decode))))))(arg)
		return {'borrowedTokens' : (before_projection[0][0]),
'interestIndex' : (before_projection[0][1][0]),
'isLiquidated' : (before_projection[0][1][1]),
'ovenProxyContractAddress' : (before_projection[1][0]),
'owner' : (before_projection[1][1][0]),
'stabilityFeeTokens' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract oven")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/oven_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1