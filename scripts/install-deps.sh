#!/bin/bash

echo "Installing OCaml dependencies"

echo "CI_PIPELINE_SOURCE is set to $CI_PIPELINE_SOURCE"
echo "CI_MERGE_REQUEST_TARGET_BRANCH_NAME is set to $CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
echo "CI_COMMIT_BRANCH is set to $CI_COMMIT_BRANCH"

if [ -z "$CI_PIPELINE_SOURCE" ]
then
    echo "CI_PIPELINE_SOURCE variable is empty"
else
    echo "CI_PIPELINE_SOURCE is set to $CI_PIPELINE_SOURCE"
fi

if [ -z "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" ]
then
    echo "CI_MERGE_REQUEST_TARGET_BRANCH_NAME variable is empty"
else
    echo "CI_MERGE_REQUEST_TARGET_BRANCH_NAME is set to $CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
fi

if [ -z "$CI_COMMIT_BRANCH" ]
then
    echo "CI_COMMIT_BRANCH variable is empty"
else
    echo "CI_COMMIT_BRANCH is set to $CI_COMMIT_BRANCH"
fi

if [ "$CI_PIPELINE_SOURCE" == "merge_request_event" ]
then
    if [ "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" == "master" ]
    then
        echo " MR + master"
	opam install . --deps-only --locked --with-test -y
    else
        echo " MR + !master"
	opam install . --deps-only --with-test -y
    fi
else
    if [ "$CI_COMMIT_BRANCH" == "master" ]
    then
        echo " master"
	opam install . --deps-only --locked --with-test -y
    fi
    if [ "$CI_COMMIT_BRANCH" == "next" ]
    then
        echo "next"
	opam install . --deps-only --with-test -y
    fi
fi

if [ -z "$CI_COMMIT_BRANCH" ] && [ -z "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" ] && [ -z "$CI_PIPELINE_SOURCE" ]
then
   opam install . --deps-only --with-test -y
fi
