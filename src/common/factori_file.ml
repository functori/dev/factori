(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains two modules, `File` and `Console`, which provide
utility functions for file handling and console output formatting, respectively.
The `File` module includes functions for checking if a file exists, creating a
new file, and writing to a file, while the `Console` module includes functions
for logging messages to the console with different colors and prefixes. Warning:
this short summary was automatically generated and could be incomplete or
misleading. If you feel this is the case, please open an issue. *)

module File = struct
  let exists = Sys.file_exists

  let create_file filename =
    if exists filename then
      Format.printf "Replacing [%s]@." filename
    else
      Format.printf "Generating [%s]@." filename ;
    Format.formatter_of_out_channel @@ open_out filename

  let write ?(sp = 0) oc fmt =
    let spaces = String.make (2 * sp) ' ' in
    Format.fprintf oc "%s" spaces ;
    Format.fprintf oc fmt
end

module Console = struct
  let log fmt = Format.printf fmt

  let red fmt = "\027[31m" ^^ fmt ^^ "\027[0m%!"

  let green fmt = "\027[32m" ^^ fmt ^^ "\027[0m%!"

  let yellow fmt = "\027[33m" ^^ fmt ^^ "\027[0m%!"

  let blue fmt = "\027[34m" ^^ fmt ^^ "\027[0m%!"

  let magenta fmt = "\027[35m" ^^ fmt ^^ "\027[0m%!"

  let error fmt = log (red ("[Error] " ^^ fmt))

  let success fmt = log (green fmt)

  let warn fmt = log (yellow ("[Warning] " ^^ fmt))

  let info fmt = log (blue ("[Information] " ^^ fmt))

  let debug fmt = log (magenta ("[Debug] " ^^ fmt))
end
