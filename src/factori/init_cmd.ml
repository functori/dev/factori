(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module provides the `cmd_init` function which defines a
subcommand for creating an empty Factori project with a given name and database
name. The `init_fun` function is called by `cmd_init` and creates an empty
project according to the specified options. An example is provided with a custom
subcommand named `myinit`. Warning: this short summary was automatically
generated and could be incomplete or misleading. If you feel this is the case,
please open an issue. *)

(* open Ezcmd.V2 *)
open Factori_command
open Factori_options
open Factori_errors

let ( // ) = concat

let init_fun () =
  language_shenanigans () ;
  try
    let name = !project_name in
    let db_name = db_name () in
    File_structure.create ~library:!library_mode ~crawlori:!crawlori
      ~dipdup:!dipdup ~python:!python ~csharp:!csharp ~web_mode:!web_mode
      ~typescript:!typescript ~ocaml:!ocaml ~verbose:!verbosity
      ~overwrite:!overwrite ~name ~db_name ()
    (* Format.eprintf "Successfully created empty project.\n%!" *)
  with e -> handle_exception e

let cmd_init =
  subcommand ~name:"empty project"
    ~args:
      [
        dir_anonymous_option;
        library_option;
        ocaml_language_option;
        typescript_language_option;
        python_language_option;
        csharp_language_option;
        crawlori_option;
        crawlori_db_name_option;
        web_option;
      ]
    ~doc:
      "Create an empty Factori project. In most cases you don't need to do \
       this, you can just start importing your contracts."
    init_fun
