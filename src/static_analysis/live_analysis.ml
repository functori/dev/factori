(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions for performing live variable analysis on
Tezos smart contracts. The `Live` module defines a type `t` representing a set
of live variables, as well as several functions for manipulating and accessing
these sets. The `ResultAnalyze` module is used to represent sets of name
arguments and option types. The file also includes utility functions for live
variable analysis of Tezos smart contracts, such as `kill`, `iota`, and `flow`.
An example of using the `Live` module to perform liveness analysis on a
Michelson code is also provided. Warning: this short summary was automatically
generated and could be incomplete or misleading. If you feel this is the case,
please open an issue. *)

open Auxilary
open Unstack_micheline
open Factori_representation
open Factori_errors
open Nested_arg
module NameArg = Name_arg
module Arg = Nested_arg

module ResultAnalyze = Set.Make (struct
  type t = NameArg.t * NameArg.t option

  let compare (a, opt_a) (b, opt_b) =
    let a_b = NameArg.compare a b in
    match a_b with
    | 0 -> (
      match (opt_a, opt_b) with
      | Some sub_a, Some sub_b -> NameArg.compare sub_a sub_b
      | None, None -> 0
      | None, Some _ -> -1
      | Some _, None -> 1)
    | _ -> a_b
end)

module Live = struct
  module SA = Auxilary.Aux

  type t = ResultAnalyze.t

  let print ppf t =
    let elems = ResultAnalyze.elements t in
    Format.fprintf ppf "{%a}"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
         (fun ppf ((s, _), opt) ->
           Format.fprintf ppf "(%s, %s)" s
             (match opt with
             | None -> "None"
             | Some (elt, _) -> "Some(" ^ elt ^ ")")))
      elems

  let find ((n, _) : NameArg.t) (res : ResultAnalyze.t) =
    let l = ResultAnalyze.filter (fun ((name, _), _) -> name = n) res in
    ResultAnalyze.choose_opt l

  let only_name_arg (l : SA.arg list) : NameArg.t list =
    List.fold_left
      (fun acc -> function
        | Name (n, ty) -> (n, ty) :: acc
        | _ -> acc)
      [] l

  let alive_in_previous name_assign name_l previous =
    let name_assign = Nested_arg.name_string_list name_assign in
    let name_l = Nested_arg.name_string_list name_l in
    try
      List.fold_left2
        (fun acc name_a name_r ->
          match find name_a previous with
          | Some (_, Some res) when compare name_r res <> 0 ->
            ResultAnalyze.add (name_r, Some res) acc
          | Some (_, None) -> ResultAnalyze.add (name_r, None) acc
          | _ -> acc)
        ResultAnalyze.empty name_assign name_l
    with Invalid_argument _ ->
      raise
        (GenericError
           ( "Storage use analysis",
             "Fail analysis due to wrong return in Michelson Unstack" ))

  let lub (a : t) (b : t) = ResultAnalyze.union a b

  let leq (a : t) (b : t) =
    ResultAnalyze.for_all (fun elt -> ResultAnalyze.mem elt b) a

  let bot : t = ResultAnalyze.empty

  let union (a : t) (b : t) : t = ResultAnalyze.union a b

  let diff (a : t) (b : t) : t =
    ResultAnalyze.fold
      (fun (elt, arg) acc ->
        if Option.is_none @@ find elt b then
          ResultAnalyze.add (elt, arg) acc
        else
          acc)
      a ResultAnalyze.empty

  let simple_use (use : NameArg.t list) =
    let l = List.map (fun u -> (u, None)) use in
    ResultAnalyze.of_list l

  let iota_view (tool : SA.analysis_tools) (lab : label) : t =
    let view_labels = SA.view_labels tool in
    let opt =
      List.find_map
        (function
          | _, l_end, res when l_end = lab -> Some res
          | _ -> None)
        view_labels in
    match opt with
    | None -> failwith "Cannot find view"
    | Some res -> simple_use (name_string_list [res])

  let ext_view (tool : SA.analysis_tools) : label list =
    List.map (fun (_, l_end, _) -> l_end) (SA.view_labels tool)

  let one_is_used ~previous ~assign ~use =
    let use = Nested_arg.name_string_list use in
    if
      List.for_all
        (fun e -> Option.is_none (find e previous))
        (Nested_arg.name_string_list assign)
    then
      ResultAnalyze.empty
    else
      simple_use use

  let gen (previous : t) (lab : label) (tool : SA.analysis_tools) : t =
    let block = SA.block_of_label lab tool in
    match block with
    | BAssign (N_SIMPLE (_prim, _, use, assign, _), _) ->
      one_is_used ~previous ~assign ~use
    | BThen (name, _) | BElse (name, _) -> simple_use (only_name_arg [name])
    | BAnnoAssign _ -> ResultAnalyze.empty
    | BReturnAssign (name_assign, name_l, _) -> (
      try alive_in_previous name_assign name_l previous
      with _ -> failwith "Return assgin")
    | BEnd _ -> ResultAnalyze.empty
    | BAssign _ -> failwith "TODO : Assign is not a simple node"
    | BStartExec (_arg, _lambda, _label_call) -> ResultAnalyze.empty
    | BEndExec (_res, _lambda, _label_ret) -> ResultAnalyze.empty
    | BAnnonymous (res, arg_l, _label) ->
      one_is_used ~assign:res ~use:arg_l ~previous
    | BLoop (arg, res, acc, _) ->
      let first = simple_use (only_name_arg [arg]) in
      let res =
        try alive_in_previous res acc previous with _ -> failwith "Loop" in
      union first res
    | BIs _ -> ResultAnalyze.empty
    | BPattern (pat_l, acc_l, _) -> alive_in_previous pat_l acc_l previous
    | BEndLambda _ -> ResultAnalyze.empty

  let f_entry_analyze (tool : SA.analysis_tools) (chain : chain_call)
      (li : label) (le : label) (lab : label) (base_analyze : t) : t =
    let block = SA.block_of_label lab tool in
    match block with
    | BEndExec (res, name, _label) -> (
      let complete_lambdas =
        SA.find_lambda tool chain (Option.get @@ Nested_arg.to_name_arg name)
      in
      let opt =
        List.find_opt
          (fun lambda ->
            match lambda.lambda_code with
            | None -> false
            | Some l -> l.label_start = li && l.label_end = le)
          complete_lambdas in
      match opt with
      | None -> ResultAnalyze.empty (* je ne peux rien dire sur ca *)
      | Some l -> (
        let code = try Option.get l.lambda_code with _ -> failwith "poet" in
        match code.res with
        | None -> ResultAnalyze.empty (* pas de res, par exemple un failwith *)
        | Some res_in_lambda ->
          (* bind le resultat si c'est res est une paire certiain element de cette paire ne sont peut etre pas utilisé *)
          alive_in_previous [res] [res_in_lambda] base_analyze))
    | _ -> failwith "Should be an end exec block"

  let f_return_analyze (tool : SA.analysis_tools) (chain : chain_call)
      (li : label) (le : label) (lab : label)
      ((before_call, after_call) : t * t) : t =
    (* quand je suis dans cette fonction je sais que je parle d'une lambda qui existe *)
    let block = SA.block_of_label lab tool in
    match block with
    | BStartExec (arg, name, _label) -> (
      let complete_lambdas =
        SA.find_lambda tool chain (Option.get @@ Nested_arg.to_name_arg name)
      in
      let opt =
        List.find_opt
          (fun lambda ->
            match lambda.lambda_code with
            | None -> false
            | Some l -> l.label_start = li && l.label_end = le)
          complete_lambdas in
      match opt with
      | None -> ResultAnalyze.empty (* This case should never happen *)
      | Some l ->
        let all_arg = arg :: l.in_between in
        let code =
          match l.lambda_code with
          | None -> failwith "Lambda code equal None"
          | Some c -> c in
        let lambda_arg = [code.arg] in
        let fv = alive_in_previous lambda_arg all_arg after_call in
        union before_call fv)
    | _ -> failwith "Should be an end exec block"

  let kill (lab : label) (tool : SA.analysis_tools) : t =
    let block = SA.block_of_label lab tool in
    match block with
    | BAssign (N_SIMPLE (_, _, _, assign, _), _) ->
      simple_use (name_string_list assign)
    | BAnnoAssign (_name_l, _) -> ResultAnalyze.empty
    | BReturnAssign (name_l, _, _) -> simple_use (name_string_list name_l)
    | BEnd _ -> ResultAnalyze.empty
    | BThen _ -> ResultAnalyze.empty
    | BElse _ -> ResultAnalyze.empty
    | BStartExec (_, _, _) -> ResultAnalyze.empty
    | BEndExec (_, _, _) -> ResultAnalyze.empty
    | BAssign _ -> failwith "TODO : implement pattern assign ?"
    | BAnnonymous (res, _, _label) -> simple_use (name_string_list res)
    | BIs _ -> ResultAnalyze.empty
    | BLoop (_arg, res_l, _acc_l, _) -> simple_use (name_string_list res_l)
    | BPattern (_name_l, _, _) -> ResultAnalyze.empty
    | BEndLambda _ -> ResultAnalyze.empty

  let lambda_entrance (_, _, le, lret) = (lret, le)

  let lambda_return (lc, li, _, _) = (lc, li)

  let iota (tool : SA.analysis_tools) : t =
    let final = fst (SA.get_final tool) in
    let begin_stack = fst (SA.get_init tool) in
    let res = Nested_arg.gets_pair (Z.of_int 2) begin_stack in
    let iota =
      Result.bind res (fun storage ->
          let storage = name_string_list [storage] in
          let l = List.map (fun s -> Some s) storage in
          try
            let res =
              List.fold_left2
                (fun acc elt arg ->
                  match arg with
                  | Some arg when compare arg elt = 0 -> acc
                  | _ -> ResultAnalyze.add (elt, arg) acc)
                ResultAnalyze.empty (name_string_list [final]) (None :: l) in
            Ok res
          with Invalid_argument _ ->
            Error
              (GenericError
                 ( "permissions analysis",
                   "Fail analysis due to wrong return in Michelson Unstack" )))
    in
    match iota with
    | Error exn -> raise exn
    | Ok elt -> elt

  let ext (tool : SA.analysis_tools) : label list = [snd (SA.get_final tool)]

  let flow (tool : SA.analysis_tools) : FlowSet.t =
    FlowSet.map (fun (l1, l2) -> (l2, l1)) (SA.get_flow tool)

  let f (tool : SA.analysis_tools) (lab : label) (k : t) : t =
    union (diff k (kill lab tool)) (gen k lab tool)
end

include Live
