import {
    TezosToolkit,
    MichelsonMap,
} from "@taquito/taquito";
import { MichelsonV1Expression } from "@taquito/rpc"
import * as fa2_x from "./fa2_x_interface";
import * as functolib from "./functolib";
const config = functolib.flextesa_config;
const tezosKit = new TezosToolkit(config.node_addr);

async function main(tezosKit: TezosToolkit) {
    let admin = functolib.alice_flextesa;
    let bob = functolib.bob_flextesa;
    functolib.setSigner(tezosKit, admin.sk);
    let storage = {
        administrator: admin.pkh,
        all_tokens: BigInt("0"),
        ledger: functolib.make_literal_bm([]),
        metadata: functolib.make_literal_bm([]),
        operators: functolib.make_literal_bm([]),
        paused: false,
        payout: functolib.bob_flextesa.pkh,
        token_metadata: functolib.make_literal_bm([])
    };
    let kt1 = await fa2_x.deploy_fa2_x(tezosKit, storage, config);
    console.log("KT1: %s", kt1);
    functolib.setSigner(tezosKit, bob.sk);
    let operator: fa2_x.add_operator = { token_id: BigInt("0"), _operator: admin.pkh, owner: bob.pkh };
    let update_param: fa2_x.type0 = { kind: "Add_operator_constructor", Add_operator_element: operator };
    let res = await fa2_x.call_update_operators(tezosKit, kt1, [update_param], 0);
    console.log("Operation sent to a node. Its hash is: " + res.hash);
    return;
}

main(tezosKit)