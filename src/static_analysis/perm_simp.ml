(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `perm_simp.ml` file has functions for simplifying permission formulas on
the Tezos blockchain, including extracting permission from a logical formula,
comparing two free permissions, generating a new environment replace, returning
all the paths in a nested argument, folding a permission logic into a set of
nested arguments, mapping a permission logic with a new environment, and
performing simplification on a given permission logic formula. Warning: this
short summary was automatically generated and could be incomplete or misleading.
If you feel this is the case, please open an issue. *)

open Factori_permission_lib
open Permission_tool
open Permission_ast
open Factori_representation
open Perm_test
module Constant = Factori_representation.Nested_arg.Constant

let get_prop perm =
  match perm with
  | Is_true l | Is_none l | Is_left l | Not_empty l | Is_cons l -> l

let compare_free_perm perm1 perm2 =
  let prop1 = get_prop perm1 in
  let prop2 = get_prop perm2 in
  Permission_ast.equal_prop prop1 prop2

let generate_env_replace actual env_replace =
  match actual with
  | Logic_not (P l) -> MapTest.add l true env_replace
  | P l -> MapTest.add l false env_replace
  | _ -> env_replace

module SetStr = Set.Make (String)

let all_real_names prop =
  let rec aux_get_all acc prop =
    match prop with
    | Propagate (_prim, _args, prop_l) -> List.fold_left aux_get_all acc prop_l
    | In_Nested (_, prop) -> aux_get_all acc prop
    | Equal (_, prop) -> aux_get_all acc prop
    | Real_name n -> SetStr.add n acc
    | Ident _ -> acc in
  aux_get_all SetStr.empty prop

module MapStr = Map.Make (String)

type color =
  | White
  | Gray
  | Black

let rec dfs (base_pop : 'a MapStr.t) l acc id =
  match MapStr.find_opt id l with
  | Some Black -> (l, acc)
  | _ ->
    let l = MapStr.add id Gray l in
    let pop = MapStr.find id base_pop in
    let s = all_real_names pop in
    let l, acc =
      SetStr.fold
        (fun id (l, in_acc) ->
          match MapStr.find_opt id l with
          | Some White -> dfs base_pop l in_acc id
          | _ -> (l, in_acc))
        s (l, acc) in
    let l = MapStr.add id Black l in
    (l, (id, pop) :: acc)

let topological_sort pop_l =
  let l = MapStr.map (fun _ -> White) pop_l in
  let pop_list = MapStr.bindings pop_l in
  let _, l =
    List.fold_left
      (fun (l, acc) (id, _pop) -> dfs pop_l l acc id)
      (l, []) pop_list in
  List.rev l

let find_cst_or_solo arg actual env =
  match Constant.find_opt arg env with
  | None -> Ident arg
  | Some formule_env -> (
    match Formule_map.cardinal formule_env with
    | 1 -> snd @@ Formule_map.choose formule_env
    | _ -> find_cst_formula arg actual env)

let formule_simp env_replace actual =
  let replaced_actual =
    Permission_tool.replace_true
      (fun perm -> MapTest.find_opt perm env_replace)
      actual (*use env_replace to replace some var *) in
  formula_simplification replaced_actual

let var_cpt = ref 0

let all_same comp f_map =
  let rec all_same acc f_map =
    match f_map with
    | [] -> acc
    | x :: xs -> (
      match acc with
      | None -> all_same (Some x) xs
      | Some x_prec when comp x x_prec = 0 -> all_same acc xs
      | _ -> None) in
  all_same None f_map

let new_name () =
  let res = !var_cpt in
  incr var_cpt ;
  "var_" ^ string_of_int res

let replace_name map prop =
  Permission_ast.map_prop
    ~f_prop:(function
      | Real_name n -> (
        match MapStr.find_opt n map with
        | Some new_prop -> new_prop
        | None -> Real_name n)
      | prop -> prop)
    prop

let replace_formula map f = map_formula (replace_name map) f

let rec f_prop cst_env cps set x =
  let cst = Constant.find_opt x cst_env in
  match cst with
  | None -> (set, cps (Ident x))
  | Some elt -> (
    (* this means that we have some knowledge on x *)
    let card = Formule_map.cardinal elt in
    match card with
    | 0 -> (* There is no more information on x *) (set, cps (Ident x))
    | 1 ->
      (* if x has only one formula that defines it, it means that we don't need that formula because we are in a formula
         that contains x so we know that we are in a good formula *)
      let new_elt = snd @@ Formule_map.choose elt in
      f_fold_prop cst_env cps set new_elt
    | _ -> (
      (* There are multiple definitions of x depending on the formula for example : if a then x = 5 else x = 6
         here x depends on a *)
      match Constant.find_opt x set with
      | Some (name_var, _) -> (set, cps (Real_name name_var))
      | None ->
        let name_prop = new_name () in
        let set = Constant.add x (name_prop, elt) set in
        (set, cps (Real_name name_prop))))

and f_fold_prop env cps already_treated prop =
  Permission_ast.map_fold_prop (f_prop env) cps already_treated prop

and f_in_logic env cps already_treated perm =
  let prop = get_prop perm in
  let f_perm = fun_perm perm in
  let set, prop =
    Permission_ast.map_fold_prop (f_prop env) (fun a -> a) already_treated prop
  in
  (set, cps (P (f_perm prop)))

and fold_logic env already_treated formule =
  map_fold_in_logic_cps (fun a -> a) (f_in_logic env) already_treated formule

let make_replace_and_set env_replace cst_env set =
  let rec aux_constant set (replace, acc, all_false) next_to_do =
    match next_to_do with
    | [] -> (replace, acc)
    | (x, (name_prop, elt)) :: xs -> (
      let in_replace = MapStr.mem name_prop replace in
      let in_acc = Constant.mem x acc in
      let in_all_false = SetStr.mem name_prop all_false in
      match in_replace || in_acc || in_all_false with
      | true -> aux_constant set (replace, acc, all_false) xs
      | false ->
        let set, formule_map, _ =
          let form_map = Formule_map.bindings elt in
          List.fold_left
            (fun (set, acc, self_env_replace) (f, prop) ->
              let f = formule_simp self_env_replace f in
              let new_env_replace = generate_env_replace f self_env_replace in
              let set, f = fold_logic cst_env set f in
              let set, prop = f_fold_prop cst_env (fun a -> a) set prop in
              (set, (f, prop) :: acc, new_env_replace))
            (set, [], env_replace) form_map in
        let new_xs = List.rev_append (Constant.bindings set) xs in
        let formule_map =
          List.filter
            (fun (f, _) -> not (Permission_tool.is_false f))
            formule_map in
        let acc =
          let all_same =
            all_same Permission_ast.equal_prop (List.map snd formule_map) in
          match all_same with
          | Some prop -> (MapStr.add name_prop prop replace, acc, all_false)
          | _ when formule_map = [] ->
            (replace, acc, SetStr.add name_prop all_false)
          | _ ->
            (replace, Constant.add x (name_prop, formule_map) acc, all_false)
        in
        aux_constant set acc new_xs) in
  aux_constant set
    (MapStr.empty, Constant.empty, SetStr.empty)
    (Constant.bindings set)

let main_simplification env_replace actual env =
  (* here in f_prop x is all the nested arg that i found in the formula *)
  let set, new_formule = fold_logic env Constant.empty actual in
  let replace_env, set = make_replace_and_set env_replace env set in
  let order = topological_sort replace_env in
  let replace_env =
    List.fold_left
      (fun new_replace (id, prop) ->
        let prop = replace_name new_replace prop in
        MapStr.add id prop new_replace)
      MapStr.empty order in
  let new_formule = replace_formula replace_env new_formule in
  let new_formule = formule_simp env_replace new_formule in
  let set =
    Constant.map
      (fun (a, elt) ->
        let elt =
          List.map
            (fun (f, prop) ->
              let f = replace_formula replace_env f in
              let prop = replace_name replace_env prop in
              (formule_simp env_replace f, prop))
            elt in
        (a, elt))
      set in
  (set, new_formule)

let all_simplification map_test l =
  let rec aux_simp place env_replace acc l =
    match l with
    | [] -> acc
    | (_key, (env, original_actual, arg)) :: xs ->
      let replaced_actual =
        (* re simplification *) formule_simp env_replace original_actual in
      let set, actual =
        main_simplification env_replace replaced_actual env
        (* transform var to Michelson primitive *) in
      let env_replace =
        generate_env_replace replaced_actual env_replace
        (* find new variables to set to false/true in env_replace *) in
      let arg = find_cst_or_solo arg original_actual env in
      let new_one = (actual, arg) in
      let acc = Result.add (place, []) (new_one, set) acc in
      aux_simp (place + 1) env_replace acc xs in
  aux_simp 0 map_test Result.empty l

let simplification map_test
    (result :
      (Perm_test.cst_propagation * prop permission logic * Nested_arg.t)
      Result.t) =
  let l = Result.bindings result in
  let l =
    List.sort
      (fun (_, (_, a, _)) (_, (_, b, _)) ->
        let a = all_free_var compare_free_perm a in
        let b = all_free_var compare_free_perm b in
        List.compare_lengths a b)
      l in
  all_simplification map_test l
