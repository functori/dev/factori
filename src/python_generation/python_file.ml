(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines various types and functions related to generating
Python code, including types for fields, classes, and functions, as well as
functions for printing them and writing them to files. It also includes
functions for reading and writing backup files. Example usage is provided.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Format
open Factori_errors

type python_tortoise_field_type =
  | TextField
  | CharField
  | IntField
  | BigIntField
  | DatetimeField
  | JSONField
  | BooleanField
[@@deriving encoding]

type python_class_field = {
  pcf_name : string;
  pcf_kind : python_tortoise_field_type;
  pcf_index : bool option;
  pcf_max_length : int option;
}
[@@deriving encoding]

type python_class = {
  pc_name : string;
  pc_inheritances : string list;
  pc_fields : python_class_field list;
}
[@@deriving encoding]

type python_import = {
  pi_from : string option;
  pi_imports : string list;
  pi_as : string option;
}
[@@deriving encoding]

type python_def_arg = {
  pd_arg_name : string;
  pd_arg_type : string;
}
[@@deriving encoding]

type python_def = {
  pd_async : bool option;
  pd_name : string;
  pd_args : python_def_arg list;
  pd_return_type : string;
  pd_body : string;
}
[@@deriving encoding]

type python_file = {
  python_imports : python_import list;
  python_classes : python_class list;
  python_defs : python_def list;
}
[@@deriving encoding]

let empty_python_file =
  { python_imports = []; python_classes = []; python_defs = [] }

let print_import ppf import =
  fprintf ppf "%simport %a%s"
    (match import.pi_from with
    | Some s -> sprintf "from %s " s
    | None -> "")
    (pp_print_list
       ~pp_sep:(fun ppf _ -> pp_print_string ppf ",")
       pp_print_string)
    import.pi_imports
    (match import.pi_as with
    | None -> ""
    | Some s -> sprintf " as %s" s)

let print_imports ppf imports =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
    print_import ppf imports

let print_inheritances ppf = function
  | [] -> ()
  | inheritances ->
    fprintf ppf "(%a)"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> pp_print_string ppf ",")
         pp_print_string)
      inheritances

let print_tortoise_kind ppf = function
  | TextField -> fprintf ppf "TextField"
  | CharField -> fprintf ppf "CharField"
  | BigIntField -> fprintf ppf "BigIntField"
  | IntField -> fprintf ppf "IntField"
  | DatetimeField -> fprintf ppf "DatetimeField"
  | JSONField -> fprintf ppf "JSONField"
  | BooleanField -> fprintf ppf "BooleanField"

let print_tortoise_extra ppf field =
  match (field.pcf_index, field.pcf_max_length) with
  | None, None -> ()
  | Some b, None ->
    fprintf ppf "index=%s" (String.capitalize_ascii @@ string_of_bool b)
  | None, Some m -> fprintf ppf "max_length=%d" m
  | Some b, Some m ->
    fprintf ppf "index=%s,max_length=%d"
      (String.capitalize_ascii @@ string_of_bool b)
      m

let print_tortoise_field ppf field =
  fprintf ppf "fields.%a(%a)" print_tortoise_kind field.pcf_kind
    print_tortoise_extra field

let print_class_field ppf field =
  fprintf ppf "\t%s = %a" field.pcf_name print_tortoise_field field

let print_class_fields ppf fields =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
    print_class_field ppf fields

let print_class ppf cl =
  fprintf ppf "class %s%a:\n%a"
    (String.capitalize_ascii cl.pc_name)
    print_inheritances cl.pc_inheritances print_class_fields cl.pc_fields

let print_classes ppf classes =
  pp_print_list
    ~pp_sep:(fun ppf _ ->
      pp_print_newline ppf () ;
      pp_print_newline ppf ())
    print_class ppf classes

let print_def_arg ppf arg = fprintf ppf "%s:%s" arg.pd_arg_name arg.pd_arg_type

let print_def_args ppf args =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_string ppf ",")
    print_def_arg ppf args

let print_def ppf def =
  fprintf ppf "%sdef %s(%a) -> %s:\n%s"
    (match def.pd_async with
    | None | Some false -> ""
    | _ -> "async ")
    def.pd_name print_def_args def.pd_args def.pd_return_type def.pd_body

let print_defs ppf defs =
  pp_print_list
    ~pp_sep:(fun ppf _ ->
      pp_print_newline ppf () ;
      pp_print_newline ppf ())
    print_def ppf defs

let print_python_file ppf file =
  fprintf ppf "%a\n%a\n%a%!" print_imports file.python_imports print_classes
    file.python_classes print_defs file.python_defs

let read_backup_file ?(check_if_already_there = true) ~filename () =
  let backup_file = Factori_utils.get_backup_file_name ~filename in
  if not (Sys.file_exists backup_file) then
    if check_if_already_there then
      failwith
      @@ sprintf
           "[decode_python_file] trying to decode a non-existing backup file %s"
           backup_file
    else
      empty_python_file
  else
    try
      Factori_utils.deserialize_file ~silent:false ~filename:backup_file
        ~enc:python_file_enc
    with CouldNotDestruct _ | CouldNotReadAsJson _ ->
      Factori_utils.output_verbose
        "Could not deserialize file, will behave as if it were not present" ;
      empty_python_file

let read_enc_backup_file ?(check_if_already_there = true) ~filename ~enc () =
  let backup_file = Factori_utils.get_backup_file_name ~filename in
  if not (Sys.file_exists backup_file) then
    if check_if_already_there then
      failwith
      @@ sprintf
           "[decode_python_file] trying to decode a non-existing backup file %s"
           backup_file
    else
      None
  else
    try Some (Factori_utils.deserialize_file ~filename:backup_file ~enc)
    with CouldNotDestruct _ | CouldNotReadAsJson _ ->
      Factori_utils.output_verbose
        "Could not deserialize file, will behave as if it were not present" ;
      None

(** Take the filename of the python file, and write the backup file by
   its side *)
let write_backup_file ~python_file ~filename =
  let backup_filename = Factori_utils.get_backup_file_name ~filename in
  Factori_utils.serialize_file ~filename:backup_filename ~enc:python_file_enc
    python_file

(** Write an Python file <path/filename.py> and, by its side, write
   <path/filename.json> which contains a json encoded version of the Python
   file, making it easy to read and update it. If with_readable_backup
   is set to false, then no json file is written *)
let write_python_file ?(with_readable_backup = true) ~python_file ~path () =
  Factori_utils.write_file ~filename:path
    (asprintf "%a" print_python_file python_file) ;
  if with_readable_backup then write_backup_file ~python_file ~filename:path
