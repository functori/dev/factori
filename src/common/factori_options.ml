(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines command line options for the Factori program, which
is used to generate interfaces for smart contracts. The options are divided into
categories such as verbosity, directories, project options, language options,
and other options, and include settings for generating interfaces in different
programming languages, generating web interfaces, and generating crawlori
plugins. Examples of how to use the options are provided. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Ezcmd.V2
open EZCMD.TYPES

(* let verbose = ref false *)

(* Verbosity will have to coexist with verbose for a while, this is
   for compatibility with EZCMD *)
let verbosity = ref 0

let dir = ref (None : string option)

let project_name = ref (None : string option)

let block_interval = ref 1

let overwrite = ref false

let purge = ref false

let contract_name = ref (None : string option)

(* Determines whether record fields are prefixed by the entrypoint
   name to disambiguate when several entrypoints have the same
   parameter names *)
let field_prefixes = ref false

(* Determines whether we are only generating a bare bones library *)
let library_mode = ref false

let web_mode = ref false

let concat = Filename.concat

let default_config_dir () =
  let home = Unix.getenv "HOME" in
  concat home ".factori/"

let config_file = ref (Some (default_config_dir ()))

let kt1 = ref (None : string option)

let michelson_file = ref (None : string option)

let network = ref "mainnet"

let sender = ref "alice"

let storage_type = ref "blockchain"

(* Interface languages *)
let ocaml = ref false

let python = ref false

let csharp = ref false

(* Factori can generate a crawlori plugin (this can only be use
   with ocaml flag) *)
let crawlori = ref false

let dipdup = ref false

let crawlori_db_name = ref None

let typescript = ref false
(* By default, Factori generates a
   typescript interface only *)

(* By default, regroup similar interfaces *)
let do_not_regroup = ref false

(** This function determines what languages should be turned on:
- If no language is selected, then Factori defaults to Typescript
- If at least one language is selected, then Factori only generates SDKs for selected languages
- If crawlori is selected, then OCaml is automatically selected (because it is needed)
- If web_mode is selected, then Typescript is automatically selected (because it is needed) *)
let language_shenanigans () =
  if !verbosity > 0 then
    Format.eprintf "ocaml: %b; typescript: %b; python : %b@; csharp: %b.\n"
      !ocaml !typescript !python !csharp ;
  if !crawlori then ocaml := true ;
  if !web_mode then typescript := true ;
  match (!ocaml, !typescript, !python, !csharp) with
  | false, false, false, false ->
    if !verbosity > 0 then
      Format.eprintf "No language selected, defaulting to Typescript\n%!" ;
    typescript := true
  | _, _, _, _ -> ()

let db_name () =
  Option.value
    ~default:
      (Option.fold ~none:"db_generic_name"
         ~some:(fun s ->
           if s = "." || s = ".." || s = "~" then
             "db_generic_name"
           else
             Filename.basename s)
         !dir)
    !crawlori_db_name

let overwrite_option =
  ( ["force"; "f"],
    Arg.Bool (fun b -> overwrite := b),
    EZCMD.info ~docv:"FORCE"
      "If set to true, Factori will overwrite the input folder for commands \
       that write to files. Defaults to false." )

(* let verbose_option =
 *   ( ["verbose"],
 *     Arg.Bool (fun b -> verbose := b),
 *     EZCMD.info
 *       ~docv:"VERBOSE"
 *       "If true, Factori will print messages to explicit what it is doing on \
 *        the standard error output. Defaults to false." ) *)

(* let config_dir_option =
 *   [ "config-dir"], Arg.String (fun s -> config_file := Some s),
 *   EZCMD.info ~docv:"CONFIG_FILE"
 *     "If different from ~/.factori, this is where the Factori \
 *      configuration file is located." *)

let dir_anonymous_option =
  ( [],
    Arg.Anon (0, fun s -> dir := Some s),
    EZCMD.info ~docv:"DIRECTORY"
      "Directory where the Factori project is stored." )

let ocaml_language_option =
  ( ["ocaml"],
    Arg.Set ocaml,
    EZCMD.info ~docv:"OCAML"
      "If activated, an OCaml interface for the smart contract(s) will be \
       generated." )

let python_language_option =
  ( ["python"],
    Arg.Set python,
    EZCMD.info ~docv:"PYTHON"
      "If activated, a Python interface for the smart contract(s) will be \
       generated." )

let typescript_language_option =
  ( ["typescript"],
    Arg.Set typescript,
    EZCMD.info ~docv:"TYPESCRIPT"
      "If activated, a Typescript interface for the smart contract(s) will be \
       generated" )

let csharp_language_option =
  ( ["csharp"],
    Arg.Set csharp,
    EZCMD.info ~docv:"C#"
      "If activated, a C# interface for the smart contract(s) will be generated"
  )

let contract_name_option =
  ( ["name"],
    Arg.String (fun name -> contract_name := Some name),
    EZCMD.info ~docv:"CONTRACT_NAME"
      "Provide the contract name for which you would like to perform the \
       operation." )

let contract_name_anonymous =
  ( [],
    Arg.Anon (0, fun name -> contract_name := Some name),
    EZCMD.info ~docv:"CONTRACT_NAME"
      "Provide a contract name; by default, the file name or KT1 will be used \
       to create a new name." )

let michelson_file_anonymous0 =
  ( [],
    Arg.Anon (0, fun name -> michelson_file := Some name),
    EZCMD.info ~docv:"MICHELSON_FILE"
      "The Michelson file you would like to use for this operation." )

let michelson_file_anonymous1 =
  ( [],
    Arg.Anon (1, fun name -> michelson_file := Some name),
    EZCMD.info ~docv:"MICHELSON_FILE"
      "The Michelson file you would like to use for this operation." )

(* KT1 anonymous argument in first position *)
let kt1_anonymous0 =
  ( [],
    Arg.Anon (0, fun s -> kt1 := Some s),
    EZCMD.info ~docv:"KT1"
      "Provide an on-chain KT1 you wish to download and import." )

(* KT1 anonymous argument in second position *)
let kt1_anonymous1 =
  ( [],
    Arg.Anon (1, fun s -> kt1 := Some s),
    EZCMD.info ~docv:"KT1"
      "Provide an on-chain KT1 you wish to download and import." )

let purge_option =
  ( ["purge"],
    Arg.Bool (fun b -> purge := b),
    EZCMD.info ~docv:"purge"
      "Actually remove the source files related to the contract you wish to \
       remove. Defaults to false." )

let network_option =
  ( ["network"],
    Arg.String (fun s -> network := s),
    EZCMD.info ~docv:"network"
      "Specify on which network (mainnet,ithacanet,etc...) you would like to \
       perform the operation." )

let field_prefixes_option =
  ( ["field_prefixes"],
    Arg.Set_bool field_prefixes,
    EZCMD.info ~docv:"field_prefixes"
      "Determines whether record fields are prefixed by the entrypoint\n\
      \       name to disambiguate when several entrypoints have the same\n\
      \       parameter names" )

let library_option =
  ( ["library"],
    Arg.Bool (fun b -> library_mode := b),
    EZCMD.info ~docv:"library"
      "Determines whether the OCaml code is generated only as a library." )

let crawlori_option =
  ( ["crawlori"],
    Arg.Bool (fun b -> crawlori := b),
    EZCMD.info ~docv:"crawlori"
      "If activated, a crawlori plugin that can crawl the smart contract(s) \
       will be generated (needs ocaml option)." )

let crawlori_db_name_option =
  ( ["db-name"],
    Arg.String (fun s -> crawlori_db_name := Some s),
    EZCMD.info ~docv:"db-name"
      "This is the name for the psql database (default is project directory \
       basename)." )

let web_option =
  ( ["web"],
    Arg.Bool (fun b -> web_mode := b),
    EZCMD.info ~docv:"web"
      "Determines whether the Web page of the smart contract is generated." )

let dipdup_option =
  ( ["dipdup"],
    Arg.Bool (fun b -> dipdup := b),
    EZCMD.info ~docv:"dipdup"
      "If activated, necessary files to use didup will be generated." )

let from_option =
  ( ["from"],
    Arg.String (fun s -> sender := s),
    EZCMD.info ~docv:"from"
      "Agent who will do the current operation. Note that if the network is \
       mainnet, this will most likely fail" )

let storage_option =
  ( ["storage"],
    Arg.String (fun s -> storage_type := s),
    EZCMD.info ~docv:"storage"
      "Storage type used for the `factori deploy` command." )

let project_name_option =
  ( ["project_name"],
    Arg.String (fun name -> project_name := Some name),
    EZCMD.info ~docv:"PROJECT_NAME"
      "Provide a project name. This will be used e.g. for the opam file name" )

let block_time_option =
  ( ["block-interval"],
    Arg.Int (fun i -> block_interval := i),
    EZCMD.info ~docv:"BLOCK_INTERVAL"
      "Provide a block time to start the flextesa sandbox with" )

let do_not_regroup_option =
  ( ["dont-regroup"],
    Arg.Set do_not_regroup,
    EZCMD.info ~docv:"DONT_REGROUP_INTERFACES"
      "Even if the imported contract has the same entrypoints and storage type \
       as an existing imported contract, create a separate indepedent \
       interface with incompatible types" )
