from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

SetOvenRegistryContract = factori_types.address

def setOvenRegistryContract_generator() -> SetOvenRegistryContract:
	return factori_types.address_generator ()

def setOvenRegistryContract_encode (arg: SetOvenRegistryContract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in setOvenRegistryContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setOvenRegistryContract_decode (arg) -> SetOvenRegistryContract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in setOvenRegistryContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setOvenRegistryContract():
	temp=setOvenRegistryContract_generator()
	temp_encoded=setOvenRegistryContract_encode(temp)
	try:
		print('++++ setOvenRegistryContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setOvenRegistryContract: " + str(temp))
	temp1=setOvenRegistryContract_decode(temp_encoded)
	if temp1 == temp:
		print('setOvenRegistryContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setOvenRegistryContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setOvenRegistryContract()
#except Exception as e:
#	print('Error in assert_identity_setOvenRegistryContract' + str(e))




def callSetOvenRegistryContract(kt1 : str, _from : blockchain.identity, param : SetOvenRegistryContract, networkName = "ghostnet", debug=False):
	input = setOvenRegistryContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setOvenRegistryContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetOvenProxyContract = SetOvenRegistryContract


def setOvenProxyContract_generator() -> SetOvenProxyContract:
	return setOvenRegistryContract_generator ()


def setOvenProxyContract_encode (arg: SetOvenProxyContract):
	try:
		return setOvenRegistryContract_encode(arg)
	except Exception as e:
		print('Error in setOvenProxyContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setOvenProxyContract_decode (arg) -> SetOvenProxyContract:
	try:
		return setOvenRegistryContract_decode(arg)
	except Exception as e:
		print('Error in setOvenProxyContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setOvenProxyContract():
	temp=setOvenProxyContract_generator()
	temp_encoded=setOvenProxyContract_encode(temp)
	try:
		print('++++ setOvenProxyContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setOvenProxyContract: " + str(temp))
	temp1=setOvenProxyContract_decode(temp_encoded)
	if temp1 == temp:
		print('setOvenProxyContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setOvenProxyContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setOvenProxyContract()
#except Exception as e:
#	print('Error in assert_identity_setOvenProxyContract' + str(e))




def callSetOvenProxyContract(kt1 : str, _from : blockchain.identity, param : SetOvenProxyContract, networkName = "ghostnet", debug=False):
	input = setOvenProxyContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setOvenProxyContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetMinterContract = SetOvenRegistryContract


def setMinterContract_generator() -> SetMinterContract:
	return setOvenRegistryContract_generator ()


def setMinterContract_encode (arg: SetMinterContract):
	try:
		return setOvenRegistryContract_encode(arg)
	except Exception as e:
		print('Error in setMinterContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setMinterContract_decode (arg) -> SetMinterContract:
	try:
		return setOvenRegistryContract_decode(arg)
	except Exception as e:
		print('Error in setMinterContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setMinterContract():
	temp=setMinterContract_generator()
	temp_encoded=setMinterContract_encode(temp)
	try:
		print('++++ setMinterContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setMinterContract: " + str(temp))
	temp1=setMinterContract_decode(temp_encoded)
	if temp1 == temp:
		print('setMinterContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setMinterContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setMinterContract()
#except Exception as e:
#	print('Error in assert_identity_setMinterContract' + str(e))




def callSetMinterContract(kt1 : str, _from : blockchain.identity, param : SetMinterContract, networkName = "ghostnet", debug=False):
	input = setMinterContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setMinterContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetInitialDelegate = Optional[factori_types.key_hash]

def setInitialDelegate_generator() -> SetInitialDelegate:
	return (factori_types.option_generator (factori_types.key_hash_generator)) ()

def setInitialDelegate_encode (arg: SetInitialDelegate):
	try:
		return factori_types.option_encode(factori_types.key_hash_encode)(arg)
	except Exception as e:
		print('Error in setInitialDelegate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setInitialDelegate_decode (arg) -> SetInitialDelegate:
	try:
		return factori_types.option_decode(factori_types.key_hash_decode)(arg)
	except Exception as e:
		print('Error in setInitialDelegate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setInitialDelegate():
	temp=setInitialDelegate_generator()
	temp_encoded=setInitialDelegate_encode(temp)
	try:
		print('++++ setInitialDelegate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setInitialDelegate: " + str(temp))
	temp1=setInitialDelegate_decode(temp_encoded)
	if temp1 == temp:
		print('setInitialDelegate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setInitialDelegate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setInitialDelegate()
#except Exception as e:
#	print('Error in assert_identity_setInitialDelegate' + str(e))




def callSetInitialDelegate(kt1 : str, _from : blockchain.identity, param : SetInitialDelegate, networkName = "ghostnet", debug=False):
	input = setInitialDelegate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setInitialDelegate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = SetOvenRegistryContract


def setGovernorContract_generator() -> SetGovernorContract:
	return setOvenRegistryContract_generator ()


def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return setOvenRegistryContract_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return setOvenRegistryContract_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

MakeOven_minterCallback = factori_types.nat

def makeOven_minterCallback_generator() -> MakeOven_minterCallback:
	return factori_types.nat_generator ()

def makeOven_minterCallback_encode (arg: MakeOven_minterCallback):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in makeOven_minterCallback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def makeOven_minterCallback_decode (arg) -> MakeOven_minterCallback:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in makeOven_minterCallback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_makeOven_minterCallback():
	temp=makeOven_minterCallback_generator()
	temp_encoded=makeOven_minterCallback_encode(temp)
	try:
		print('++++ makeOven_minterCallback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of makeOven_minterCallback: " + str(temp))
	temp1=makeOven_minterCallback_decode(temp_encoded)
	if temp1 == temp:
		print('makeOven_minterCallback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('makeOven_minterCallback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_makeOven_minterCallback()
#except Exception as e:
#	print('Error in assert_identity_makeOven_minterCallback' + str(e))




def callMakeOven_minterCallback(kt1 : str, _from : blockchain.identity, param : MakeOven_minterCallback, networkName = "ghostnet", debug=False):
	input = makeOven_minterCallback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "makeOven_minterCallback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

MakeOven = factori_types.unit

def makeOven_generator() -> MakeOven:
	return factori_types.unit_generator ()

def makeOven_encode (arg: MakeOven):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in makeOven_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def makeOven_decode (arg) -> MakeOven:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in makeOven_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_makeOven():
	temp=makeOven_generator()
	temp_encoded=makeOven_encode(temp)
	try:
		print('++++ makeOven: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of makeOven: " + str(temp))
	temp1=makeOven_decode(temp_encoded)
	if temp1 == temp:
		print('makeOven check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('makeOven check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_makeOven()
#except Exception as e:
#	print('Error in assert_identity_makeOven' + str(e))




def callMakeOven(kt1 : str, _from : blockchain.identity, param : MakeOven, networkName = "ghostnet", debug=False):
	input = makeOven_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "makeOven", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = MakeOven


def _default_generator() -> Constructor_default:
	return makeOven_generator ()


def _default_encode (arg: Constructor_default):
	try:
		return makeOven_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def _default_decode (arg) -> Constructor_default:
	try:
		return makeOven_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

State = factori_types.int_michelson

def state_generator() -> State:
	return factori_types.int_generator ()

def state_encode (arg: State):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in state_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def state_decode (arg) -> State:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in state_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_state():
	temp=state_generator()
	temp_encoded=state_encode(temp)
	try:
		print('++++ state: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of state: " + str(temp))
	temp1=state_decode(temp_encoded)
	if temp1 == temp:
		print('state check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('state check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_state()
#except Exception as e:
#	print('Error in assert_identity_state' + str(e))




class Constructor_storage(TypedDict):
	ovenProxyContractAddress: SetOvenRegistryContract
	minterContractAddress: SetOvenRegistryContract
	makeOvenOwner: Optional[SetOvenRegistryContract]
	initialDelegate: Optional[factori_types.key_hash]
	governorContractAddress: SetOvenRegistryContract
	ovenRegistryContractAddress: SetOvenRegistryContract
	state: State

def _storage_generator() -> Constructor_storage:
	return {'ovenProxyContractAddress' : setOvenRegistryContract_generator (),'minterContractAddress' : setOvenRegistryContract_generator (),'makeOvenOwner' : (factori_types.option_generator(setOvenRegistryContract_generator)) (),'initialDelegate' : (factori_types.option_generator(factori_types.key_hash_generator)) (),'governorContractAddress' : setOvenRegistryContract_generator (),'ovenRegistryContractAddress' : setOvenRegistryContract_generator (),'state' : state_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([(setOvenRegistryContract_encode(arg['governorContractAddress'])), PairType.from_comb([(factori_types.option_encode(factori_types.key_hash_encode)(arg['initialDelegate'])), (factori_types.option_encode(setOvenRegistryContract_encode)(arg['makeOvenOwner']))])]), PairType.from_comb([PairType.from_comb([(setOvenRegistryContract_encode(arg['minterContractAddress'])), (setOvenRegistryContract_encode(arg['ovenProxyContractAddress']))]), PairType.from_comb([(setOvenRegistryContract_encode(arg['ovenRegistryContractAddress'])), (state_encode(arg['state']))])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode (setOvenRegistryContract_decode,(factori_types.tuple2_decode (factori_types.option_decode(factori_types.key_hash_decode),factori_types.option_decode(setOvenRegistryContract_decode))))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (setOvenRegistryContract_decode,setOvenRegistryContract_decode)),(factori_types.tuple2_decode (setOvenRegistryContract_decode,state_decode))))))(arg)
		return {'governorContractAddress' : (before_projection[0][0]),
'initialDelegate' : (before_projection[0][1][0]),
'makeOvenOwner' : (before_projection[0][1][1]),
'minterContractAddress' : (before_projection[1][0][0]),
'ovenProxyContractAddress' : (before_projection[1][0][1]),
'ovenRegistryContractAddress' : (before_projection[1][1][0]),
'state' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract oven_factory")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/oven_factory_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1