(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines a `value` type with several variants that represent
different types of values that can be used in Michelson, including integers,
strings, addresses, and more. It also includes a function `show_value` to
display the contents of a value in a human-readable format. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Types
open Pair_type
open Factori_utils

type value =
  | VUnit of unit
  | VInt of Z.t
  | VString of string
  | VBytes of Hex.hex
  | VTimestamp of string
  | VBool of bool
  | VAddress of string
  | VTez of Z.t
  | VKey of string
  | VKeyHash of string
  | VSignature of string
  | VOperation of string
  | VTuple of type_ list * value list
  | VBigMap of type_ * type_ * Z.t
  | VMap of type_ * type_ * (value * value) list
  | VSet of type_ * value list
  | VList of type_ * value list
  | VOption of type_ * value option
  | VLambda of Tzfunc.Proto.script_expr
  | VRecord of (type_ * value) paired_type
  | VSumtype of
      string
      * Factori_utils.sanitized_name
      * type_
      * value (* constructor, typename *)
  | VChain_id of string
  | VTicket of string
  | VSaplingState of string

let rec show_value ppf = function
  | VUnit _ -> Format.fprintf ppf "VUnit"
  | VInt n -> Format.fprintf ppf "VInt %s" (Z.to_string n)
  | VString s -> Format.fprintf ppf "VString %s" s
  | VBytes h -> Format.fprintf ppf "VBytes %s" (h :> string)
  | VTimestamp t -> Format.fprintf ppf "VTimestamp %s" t
  | VBool b -> Format.fprintf ppf "VBool %b" b
  | VAddress a -> Format.fprintf ppf "VAddress %s" a
  | VTez t -> Format.fprintf ppf "VTez %s" (Z.to_string t)
  | VKey k -> Format.fprintf ppf "VKey %s" k
  | VKeyHash kh -> Format.fprintf ppf "VKeyHash %s" kh
  | VSignature sign -> Format.fprintf ppf "VSignature %s" sign
  | VOperation op -> Format.fprintf ppf "VOperation %s" op
  | VTuple (_, lst) ->
    Format.fprintf ppf "VTuple (%a)"
      (Format.pp_print_list ~pp_sep:(tag ",") show_value)
      lst
  | VBigMap (_, _, n) -> Format.fprintf ppf "VBigMap %s" (Z.to_string n)
  | VMap (_, _, lst) ->
    Format.fprintf ppf "VMap (%a)"
      (Format.pp_print_list (fun ppf (v1, v2) ->
           Format.fprintf ppf "(%a, %a)" show_value v1 show_value v2))
      lst
  | VSet (_, lst) ->
    Format.fprintf ppf "VSet (%a)" (Format.pp_print_list show_value) lst
  | VList (_, lst) ->
    Format.fprintf ppf "VList (%a)" (Format.pp_print_list show_value) lst
  | VOption (_, v) ->
    Format.fprintf ppf "VOption (%a)" (Format.pp_print_option show_value) v
  | VLambda _le -> Format.fprintf ppf "VLambda"
  | VRecord r ->
    Format.fprintf ppf "VRecord(%s)"
      (show_paired_type (fun ppf (_, v) -> show_value ppf v) r)
  | VSumtype (s, typename, _, v) ->
    Format.fprintf ppf "VSumtype (%s, %s, %a)" s
      (show_sanitized_name typename)
      show_value v
  | VChain_id cid -> Format.fprintf ppf "VChain_id %s" cid
  | VTicket t -> Format.fprintf ppf "VTicket %s" t
  | VSaplingState s -> Format.fprintf ppf "VSaplingState %s" s
