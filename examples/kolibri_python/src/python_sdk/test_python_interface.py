from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

Callback = factori_types.nat

def callback_generator() -> Callback:
	return factori_types.nat_generator ()

def callback_encode (arg: Callback):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def callback_decode (arg) -> Callback:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_callback():
	temp=callback_generator()
	temp_encoded=callback_encode(temp)
	try:
		print('++++ callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of callback: " + str(temp))
	temp1=callback_decode(temp_encoded)
	if temp1 == temp:
		print('callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_callback()
#except Exception as e:
#	print('Error in assert_identity_callback' + str(e))




def callCallback(kt1 : str, _from : blockchain.identity, param : Callback, networkName = "ghostnet", debug=False):
	input = callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Makecall = factori_types.unit

def makecall_generator() -> Makecall:
	return factori_types.unit_generator ()

def makecall_encode (arg: Makecall):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in makecall_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def makecall_decode (arg) -> Makecall:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in makecall_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_makecall():
	temp=makecall_generator()
	temp_encoded=makecall_encode(temp)
	try:
		print('++++ makecall: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of makecall: " + str(temp))
	temp1=makecall_decode(temp_encoded)
	if temp1 == temp:
		print('makecall check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('makecall check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_makecall()
#except Exception as e:
#	print('Error in assert_identity_makecall' + str(e))




def callMakecall(kt1 : str, _from : blockchain.identity, param : Makecall, networkName = "ghostnet", debug=False):
	input = makecall_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "makecall", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Set_contract = factori_types.address

def set_contract_generator() -> Set_contract:
	return factori_types.address_generator ()

def set_contract_encode (arg: Set_contract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in set_contract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def set_contract_decode (arg) -> Set_contract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in set_contract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_set_contract():
	temp=set_contract_generator()
	temp_encoded=set_contract_encode(temp)
	try:
		print('++++ set_contract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of set_contract: " + str(temp))
	temp1=set_contract_decode(temp_encoded)
	if temp1 == temp:
		print('set_contract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('set_contract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_set_contract()
#except Exception as e:
#	print('Error in assert_identity_set_contract' + str(e))




def callSet_contract(kt1 : str, _from : blockchain.identity, param : Set_contract, networkName = "ghostnet", debug=False):
	input = set_contract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "set_contract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_storage = Tuple[Set_contract,factori_types.int_michelson]

def storage_generator() -> Constructor_storage:
	return (factori_types.tuple2_generator(set_contract_generator,
factori_types.int_generator))()

def storage_encode(arg : Constructor_storage) -> PairType:
	try:
		return factori_types.tuple2_encode(set_contract_encode,factori_types.int_michelson_encode)(arg) 
	except Exception as e:
		print('Error in storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def storage_decode(arg) -> Constructor_storage:
	try:
		return factori_types.tuple2_decode(set_contract_decode,factori_types.int_michelson_decode)(arg) 
	except Exception as e:
		print('Error in storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_storage():
	temp=storage_generator()
	temp_encoded=storage_encode(temp)
	try:
		print('++++ storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of storage: " + str(temp))
	temp1=storage_decode(temp_encoded)
	if temp1 == temp:
		print('storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_storage()
#except Exception as e:
#	print('Error in assert_identity_storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract test")
	storage_encoded = storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/test_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1