opam-version: "2.0"
name: "factori"
version: "dev"
synopsis: "Factori"
maintainer: "Functori <contact@functori.com>"
authors: "Functori <contact@functori.com>"
license: "MIT"
homepage: "https://gitlab.com/functori/dev/factori"
bug-reports: "https://gitlab.com/functori/dev/factori/issues"
depends: [
  "aches" {= "1.0.0"}
  "alcotest" {= "1.7.0" & with-test}
  "angstrom" {= "0.15.0"}
  "astring" {= "0.8.5"}
  "base" {= "v0.16.3"}
  "base-bigarray" {= "base"}
  "base-bytes" {= "base"}
  "base-threads" {= "base"}
  "base-unix" {= "base"}
  "bigstringaf" {= "0.9.1"}
  "calendar" {= "3.0.0"}
  "camlp-streams" {= "5.0.1"}
  "cmdliner" {= "1.2.0"}
  "conf-gmp" {= "4"}
  "conf-libcurl" {= "2"}
  "cppo" {= "1.6.9"}
  "csexp" {= "1.5.2"}
  "cstruct" {= "6.2.0"}
  "data-encoding" {= "0.7.1"}
  "dot-merlin-reader" {= "4.9"}
  "dune" {= "3.10.0"}
  "dune-build-info" {= "3.10.0"}
  "dune-configurator" {= "3.10.0"}
  "either" {= "1.0.0"}
  "ez_api" {= "dev"}
  "ez_cmdliner" {= "0.4.3"}
  "ez_subst" {= "0.2.1"}
  "ezjs_fetch" {= "0.3"}
  "ezjs_min" {= "dev"}
  "ezjsonm" {= "1.3.0"}
  "fix" {= "20230505"}
  "fmt" {= "0.9.0"}
  "fpath" {= "0.7.3"}
  "gen" {= "1.1"}
  "hacl_func" {= "~dev"}
  "hex" {= "1.5.0"}
  "jane-street-headers" {= "v0.16.0"}
  "js_of_ocaml" {= "5.4.0"}
  "js_of_ocaml-compiler" {= "5.4.0"}
  "js_of_ocaml-lwt" {= "5.4.0"}
  "js_of_ocaml-ppx" {= "5.4.0"}
  "json-data-encoding" {= "0.12.1"}
  "json-data-encoding-bson" {= "0.12.1"}
  "jsonm" {= "1.0.2"}
  "jst-config" {= "v0.16.0"}
  "lwt" {= "5.7.0"}
  "lwt-canceler" {= "0.3"}
  "menhir" {= "20230608"}
  "menhirLib" {= "20230608"}
  "menhirSdk" {= "20230608"}
  "merlin" {= "4.10-414"}
  "merlin-lib" {= "4.10-414"}
  "ocaml" {= "4.14.1"}
  "ocaml-compiler-libs" {= "v0.12.4"}
  "ocaml-config" {= "2"}
  "ocaml-options-vanilla" {= "1"}
  "ocaml-syntax-shims" {= "1.0.0"}
  "ocaml-version" {= "3.6.1"}
  "ocamlbuild" {= "0.14.2"}
  "ocamlfind" {= "1.9.6"}
  "ocamlformat" {= "0.26.0"}
  "ocamlformat-lib" {= "0.26.0"}
  "ocp-indent" {= "1.8.1"}
  "ocplib-endian" {= "1.2"}
  "ocplib_stuff" {= "dev"}
  "ocurl" {= "0.9.2"}
  "ppx_assert" {= "v0.16.0"}
  "ppx_base" {= "v0.16.0"}
  "ppx_cold" {= "v0.16.0"}
  "ppx_compare" {= "v0.16.0"}
  "ppx_derivers" {= "1.2.1"}
  "ppx_deriving" {= "5.2.1"}
  "ppx_deriving_encoding" {= "dev"}
  "ppx_deriving_jsoo" {= "dev"}
  "ppx_enumerate" {= "v0.16.0"}
  "ppx_expect" {= "v0.16.0"}
  "ppx_globalize" {= "v0.16.0"}
  "ppx_hash" {= "v0.16.0"}
  "ppx_here" {= "v0.16.0"}
  "ppx_inline_test" {= "v0.16.0"}
  "ppx_optcomp" {= "v0.16.0"}
  "ppx_sexp_conv" {= "v0.16.0"}
  "ppxlib" {= "0.30.0"}
  "re" {= "1.11.0"}
  "result" {= "1.5"}
  "ringo" {= "1.0.0"}
  "sedlex" {= "3.2"}
  "seq" {= "base"}
  "seqes" {= "0.2"}
  "sexplib0" {= "v0.16.0"}
  "stdio" {= "v0.16.0"}
  "stdlib-shims" {= "0.3.0"}
  "stringext" {= "1.6.0"}
  "tezos-error-monad" {= "17.3"}
  "tezos-lwt-result-stdlib" {= "17.3"}
  "tezos-micheline" {= "17.3"}
  "tezos-stdlib" {= "17.3"}
  "time_now" {= "v0.16.0"}
  "topkg" {= "1.0.7"}
  "tzfunc" {= "~dev"}
  "uri" {= "4.2.0"}
  "uucp" {= "15.0.0"}
  "uuidm" {= "0.9.8"}
  "uuseg" {= "15.0.0"}
  "uutf" {= "1.0.3"}
  "vue-ppx" {= "~dev"}
  "xml-light" {= "2.5"}
  "yojson" {= "2.1.0"}
  "zarith" {= "1.12"}
  "zarith_stubs_js" {= "v0.16.0"}
]
build: [
  ["dune" "subst"] {pinned}
  ["make" "version"]
  [
    "dune"
    "build"
    "-p"
    name
    "-j"
    jobs
    "@install"
    "@runtest" {with-test}
    "@doc" {with-doc}
  ]
]
dev-repo: "git+https://gitlab.com/functori/dev/factori"
pin-depends: [
  ["ez_api.dev" "git+https://github.com/ocamlpro/ez_api"]
  ["ezjs_min.dev" "git+https://github.com/ocamlpro/ezjs_min"]
  ["hacl_func.~dev" "git+https://gitlab.com/functori/dev/hacl.git"]
  ["ocplib_stuff.dev" "git+https://github.com/ocamlpro/ocplib_stuff.git"]
  [
    "ppx_deriving_encoding.dev"
    "git+https://gitlab.com/o-labs/ppx_deriving_encoding.git"
  ]
  [
    "ppx_deriving_jsoo.dev"
    "git+https://gitlab.com/o-labs/ppx_deriving_jsoo.git"
  ]
  ["tzfunc.~dev" "git+https://gitlab.com/functori/tzfunc.git"]
  ["vue-ppx.~dev" "git+https://gitlab.com/functori/dev/vue-ppx.git"]
]
