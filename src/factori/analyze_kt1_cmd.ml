(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This file contains the implementation of the `analyze kt1` subcommand in
OCaml for Factori, which is used to lint a KT1 smart contract using its
Michelson compiled form. It depends on several modules and defines two
functions: `get_code_from_KT1` and `analyze_kt1`. No monad binders are used in
this file. Warning: this short summary was automatically generated and could be
incomplete or misleading. If you feel this is the case, please open an issue. *)

open Factori_command
open Factori_options
open Factori_errors
open Factori_analyze
open Factori_analyze_types.Analyze_types

(* Special version of get_code_from_KT1 without using dir. Should
   probably be merged, with an option to distinguish both use cases
   (with or without a factori local repo with a contracts.json), with
   the one in src/tezos/import_kt1.ml *)
(* let get_code_from_KT1 ~network () : script_expr = *)
(*   Factori_utils.get_code_from_KT1 !kt1 ~network () *)

let action () =
  add_exception_handler stack_overflow_handle ;
  try
    match !kt1 with
    | None -> raise NoKT1Provided
    | Some kt1 -> Analyze.run (KT1 kt1)
  with e ->
    handle_exception e ;
    exit 1

let analyze_kt1 =
  subcommand ~name:"analyze kt1"
    ~args:[kt1_anonymous0; network_option]
    ~doc:"Lint a KT1 using its Michelson compiled form." action

(* Merged with `analyze kt1`, but we keep the command for backward compatibility
   reasons *)
let lint_kt1 =
  subcommand ~name:"lint kt1"
    ~args:[kt1_anonymous0; network_option]
    ~doc:"Lint a KT1 using its Michelson compiled form." action
