
from importlib.metadata import metadata
import test_python_interface
import blockchain

def exec():
    kt1 = test_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= test_python_interface.storage_generator(),
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 " + str(kt1) + " sent to a node.")
    return kt1
