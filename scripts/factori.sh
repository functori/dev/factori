#!/bin/bash

YELLOW='\033[1;33m'
LIGHT_BLUE='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

BOLD_START="\e[1m"
BOLD_END='\e[0m'

USEROPT=$(id -u):$(id -g)
DOCKER="docker"

# One can use the gitlab registry by setting the environnement variable
# REPOSITORY to "dev"
REPOSITORY=registry.gitlab.com/functori/dev/factori
printf "${GREEN}Notice:${NC} using repository ${BOLD_START}$REPOSITORY${BOLD_END}.\n"
INITIAL_COMMAND="$@"

###
### Some checks about factori version
###

## A specific version can be given via an environnement variable, e.g., with VERSION=x.y.z factori <CMD>

get_latest_released_tag (){
    TMPFILE=`mktemp /tmp/factori.XXXXXX`
    curl -s "https://gitlab.com/api/v4/projects/32966608/repository/tags?per_page=1" | \
        cut -d '"' -f4 | cut -d "v" -f 2 > $TMPFILE

    LATEST_RELEASE=`cat $TMPFILE`
    rm $TMPFILE
    printf "$LATEST_RELEASE"
}

PULLED_VERSIONS="`docker image ls --format "{{.Tag}}"  ${REPOSITORY} | sort -n -r`"
LATEST_PULLED_VERSION="`echo "$PULLED_VERSIONS" |  grep "\." | head -n 1`"

download_latest_release_message(){
    LATEST_VERSION="$1"
    printf "The tag of the latest release is ${BOLD_START}$LATEST_VERSION${BOLD_END}."
    printf " Would you like to download it with ${LIGHT_BLUE}docker pull ${REPOSITORY}:$LATEST_VERSION${NC}? [Y/n] "
}



# To remove the following warning on MacOS:
# WARNING: The requested image's platform (linux/amd64) does not match the detected host platform (linux/arm64/v8) and no specific platform was requested
if [ "`uname -m`" = "arm64" ]; then
    macos_m1_extra_flag="--platform linux/amd64"
else
    macos_m1_extra_flag=""
fi

###
### Main script starts here
###

IMAGE=${REPOSITORY}:$VERSION

# Given two arguments <relative-to> <path>, this function returns <path> if it's
# absolute or the absolute path representing <relative-to>/<path> otherwise. All
# returned paths are normalized with "readlink -f"
realpath_custom() {
    relative=$1
    path=$2

    if [[ "$path" == /* ]]; then # path is already absolute
        printf "$(realpath "$path")"
    else
        if [ "$relative" = "" ]; then
            relative="."
        fi
        printf "$(realpath "$relative/$path")"
    fi
}

run_image_without_dir(){
    # echo "run_image_without_dir: " $DOCKER run -it $macos_m1_extra_flag --network="host" --add-host=host.docker.internal:host-gateway --user $USEROPT $IMAGE "$@"
    $DOCKER run -it $macos_m1_extra_flag --network="host" --add-host=host.docker.internal:host-gateway --user $USEROPT $IMAGE "$@"
}

run_image_with_dir(){
    dir=$3
    mkdir -p $dir

    fullpath=$(realpath_custom $dir "")
    upper_dir=$(dirname $fullpath)
    subdir=$(basename $dir)
    shift
    shift
    shift

    if [ $# -eq 0 ]; then

	$DOCKER run -it $macos_m1_extra_flag --network="host" --add-host=host.docker.internal:host-gateway --user $USEROPT -v $fullpath":/home/functori/tmp" $IMAGE import kt1 tmp
    exit 1
    fi
    $DOCKER run -it $macos_m1_extra_flag --network="host" --add-host=host.docker.internal:host-gateway --user $USEROPT -v $fullpath":/home/functori/tmp" $IMAGE import kt1 tmp "$@"
}

import_kt1(){
    dir="$3"
    args="$@"
    run_image_with_dir import kt1 $args
}

import_michelson(){
    mkdir -p $1
    sdk_dir="$(realpath_custom "${PWD}" "$1")"
    shift
    contract_path="$(realpath_custom "${PWD}" "$1")"
    contract_dir="$(dirname $contract_path)"
    shift
    $DOCKER run -it $macos_m1_extra_flag \
        --network="host" \
        --add-host=host.docker.internal:host-gateway \
        --user $USEROPT  \
        -v $sdk_dir:$sdk_dir \
        -v $contract_dir:"$contract_dir" \
        $IMAGE import michelson $sdk_dir $contract_path $@
}

analyze_kt1(){
    $DOCKER run -it $macos_m1_extra_flag \
        --network="host" \
        --add-host=host.docker.internal:host-gateway \
        --user $USEROPT  \
        $IMAGE analyze kt1 $@
}

lint_kt1(){
    $DOCKER run -it $macos_m1_extra_flag \
        --network="host" \
        --add-host=host.docker.internal:host-gateway \
        --user $USEROPT  \
        $IMAGE lint kt1 $@
}

analyze_michelson(){
    contract_path="$(realpath_custom "${PWD}" "$1")"
    shift
    $DOCKER run -it $macos_m1_extra_flag \
        --network="host" \
        --add-host=host.docker.internal:host-gateway \
        --user $USEROPT  \
	-v $contract_path:"$contract_path" \
        $IMAGE analyze michelson $contract_path $@
}

lint_michelson(){
    contract_path="$(realpath_custom "${PWD}" "$1")"
    shift
    $DOCKER run -it $macos_m1_extra_flag \
        --network="host" \
        --add-host=host.docker.internal:host-gateway \
        --user $USEROPT  \
	-v $contract_path:"$contract_path" \
        $IMAGE lint michelson $contract_path $@
}


empty_project(){
    dir="$3"
    args="$@"
    run_image_with_dir empty project $args
}

rename_variables(){
    run_image_without_dir rename variables "$@"
}

remove_contract(){
    run_image_without_dir remove contract "$@"
}

deploy() {
    # --factori-root-path cannot be changed for this command
    dir="."
    fullpath=$(realpath_custom $dir "")

    $DOCKER run \
        -it $macos_m1_extra_flag \
        --network="host" \
        --add-host=host.docker.internal:host-gateway \
        -v $fullpath":/home/functori/tmp" \
	--workdir "/home/functori/tmp" \
        $IMAGE "$@"
}

start_sandbox() {
  # Check if block-time argument is passed
  block_time=3
  case "$1" in
      --block-interval)
          shift
          block_time=$1
          ;;
      *)
          echo "The only argument allowed for `factori sandbox start` is --block-interval <value>, $1 is forbidden"
          exit 1
  esac

  # Run the docker command, using the block-time variable in the appropriate place
  $DOCKER run -it $macos_m1_extra_flag --rm --name \
    factori-sandbox \
    --detach \
    -p 20000:20000 \
    -e block_time=$block_time \
    -e flextesa_node_cors_origin='*' \
    oxheadalpha/flextesa:latest kathmandubox \
    start
}

stop_sandbox() {
  docker kill factori-sandbox
}

# check if there are any arguments at all

usage(){
    echo "usage"
}

process(){
    case "$1" in
    deploy)
        deploy "${@}"
        exit 0
        ;;
	import)
	    shift
	    case "$1" in
		kt1)
		    shift
		    dir="$1"
		    args="${@}"
		    import_kt1 "$args"
                    exit 0
		    ;;
		michelson)
		    shift
		    target_dir="$1"
		    shift
		    mich_contract="$1"
		    shift
		    relpath_mich=$(realpath_custom "${PWD}" "$mich_contract")
		    args="${@}"
		    import_michelson "$target_dir" $relpath_mich "$args"
                    exit 0
		    ;;
	    esac
	    ;;
	analyze)
	    shift
	    case "$1" in
		michelson)
		    shift
		    contract="$1"
		    analyze_michelson "$contract"
		    exit 0
		    ;;
		kt1)
            shift
		    analyze_kt1 "$@"
		    exit 0
		    ;;
	    esac
	    ;;
	lint)
	    shift
	    case "$1" in
		michelson)
		    shift
		    contract="$1"
		    lint_michelson "$contract"
		    exit 0
		    ;;
		kt1)
		    shift
		    lint_kt1 "$@"
		    exit 0
		    ;;
	    esac
	    ;;
	empty)
	    shift
	    case "$1" in
		project)
		    shift
		    dir="$1"
		    shift
		    relpath=$(realpath_custom ${PWD} "$dir")
		    args="${@}"
		    empty_project $relpath "$args"
                    exit 0
		    ;;
		*)
		    echo "The command you are looking for is `factori empty project`"
	    esac
	    ;;
	rename)
	    shift
	    case "$1" in
		variables)
		    shift
		    rename_variables "${@}"
                    exit 0
		    ;;
	    esac
	    ;;
	build)
	    shift
	    case $1 in
		interface)
		    shift
		    run_image_without_dir build interface "${@}"
                    exit 0
		    ;;
	    esac
	    ;;
	remove)
	    shift
	    case "$1" in
		contract)
		    shift
		    remove_contract "${@}"
                    exit 0
		    ;;
	    esac
	    ;;
        sandbox)
            shift
            case "$1" in
                start)
                    shift
                    start_sandbox "${@}"
                    exit 0
                    ;;
                stop)
                    shift
                    stop_sandbox "${@}"
                    exit 0
                    ;;
            esac
            ;;
	help)
	    run_image_without_dir help
            exit 0
	    ;;
	*)
            run_image_without_dir "${@}"
            exit 0
            exit 1
    esac
}

prompt_docker_pull() {
    echo -n "Press 'Y' or 'y' to download, any other key to skip: "
    IFS= read -r -s -n 1 response
    echo
    if [[ "$response" =~ ^([yY])$ ]]; then
        echo docker pull "${REPOSITORY}:$LATEST_VERSION"
        docker pull "${REPOSITORY}:$LATEST_VERSION"
        VERSION=$LATEST_VERSION
        IMAGE=${REPOSITORY}:$VERSION
        # echo "VERSION: ${VERSION}"
        process $INITIAL_COMMAND
    fi
}

# Test if enviromment variable VERSION is set
case $VERSION in
latest|next)
    printf "${YELLOW}Warning:${NC} you are using the '$VERSION' version tag of factori. "
    printf "Consider upgrading regularly with ${LIGHT_BLUE}docker pull ${REPOSITORY}:$VERSION${NC} \n";;

show-pulled)
    printf "List of pulled versions:\n${BOLD_START}$PULLED_VERSIONS${BOLD_END}\n"
    exit 0;;

show-latest-release)
    LATEST_VERSION=`get_latest_released_tag`
    if [ "$LATEST_VERSION" = "" ]; then
        printf "Failed to get the tag of the latest release.\n"
    else
        if [ "$LATEST_VERSION" = "$LATEST_PULLED_VERSION" ]; then
            printf "${GREEN}Notice:${NC} you already pulled the tag of the latest release ${BOLD_START}$LATEST_VERSION${BOLD_END}."
            printf " You can use it with ${LIGHT_BLUE}VERSION=$LATEST_VERSION factori <CMD>${NC}\n"
        else
            download_latest_release_message $LATEST_VERSION
            prompt_docker_pull
        fi
    fi
    exit 0;;

*)

    if [ "$LATEST_PULLED_VERSION" = "" ]; then
        printf "${YELLOW}Warning:${NC} It seems that you didn't pull any factori image for the moment.\n"
        LATEST_VERSION=`get_latest_released_tag`
        download_latest_release_message $LATEST_VERSION
        prompt_docker_pull

        exit 1
    else
        if [ "$VERSION" != "" ]; then
            printf "${GREEN}Notice:${NC} using the given factori version ${BOLD_START}$VERSION${BOLD_END}.\n"
            if [ "$VERSION" != "$LATEST_PULLED_VERSION" ]; then
                printf "${YELLOW}Warning:${NC} the latest pulled version is ${BOLD_START}$LATEST_PULLED_VERSION${BOLD_END}, but your are using ${BOLD_START}$VERSION${BOLD_END}.\n"
            fi
            if [ `echo "$PULLED_VERSIONS" | grep  -c "^$VERSION$"` -eq 0 ] ; then
                printf "${YELLOW}Warning:${NC} the given version ${BOLD_START}$VERSION${BOLD_END} is not pulled."
                printf " If you want to use, start with ${LIGHT_BLUE}docker pull ${REPOSITORY}:$VERSION${NC}\n"
                printf "You can also run ${LIGHT_BLUE}VERSION=show-latest-release factori${NC} to know the latest released version.\n"
                exit 1
            fi
        else
            VERSION="$LATEST_PULLED_VERSION"
            printf "${GREEN}Notice:${NC} using the latest pulled factori version ${BOLD_START}$VERSION${BOLD_END}.\n"
        fi

        LATEST_VERSION=`get_latest_released_tag`

        if [ "$LATEST_VERSION" !=  "$LATEST_PULLED_VERSION" ] ; then
            printf "${YELLOW}Warning:${NC} a newer factori version (${BOLD_START}$LATEST_VERSION${BOLD_END}) is available."
            printf " Consider upgrading with ${LIGHT_BLUE}docker pull ${REPOSITORY}:$LATEST_VERSION${NC}\n"
        fi
    fi
esac

IMAGE=${REPOSITORY}:$VERSION

process "$@"
