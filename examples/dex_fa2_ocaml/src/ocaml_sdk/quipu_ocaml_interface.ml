open Quipu_code
open Factori_types
open Tzfunc.Proto

type set_position_pause = unit

let set_position_pause_encode : set_position_pause -> micheline = unit_encode

let set_position_pause_decode (m : micheline) : set_position_pause =
  unit_decode m

let set_position_pause_micheline = unit_micheline

let set_position_pause_generator () = unit_generator ()

type type0 =
  | Set_position_pause
  | Update_position_pause
  | X_to_y_pause
  | Y_to_x_pause

let type0_encode arg =
  match arg with
  | Set_position_pause ->
    Mprim
      {
        prim = `Left;
        args = [Mprim {prim = `Left; args = [unit_encode ()]; annots = []}];
        annots = [];
      }
  | Update_position_pause ->
    Mprim
      {
        prim = `Left;
        args = [Mprim {prim = `Right; args = [unit_encode ()]; annots = []}];
        annots = [];
      }
  | X_to_y_pause ->
    Mprim
      {
        prim = `Right;
        args = [Mprim {prim = `Left; args = [unit_encode ()]; annots = []}];
        annots = [];
      }
  | Y_to_x_pause ->
    Mprim
      {
        prim = `Right;
        args = [Mprim {prim = `Right; args = [unit_encode ()]; annots = []}];
        annots = [];
      }

let type0_decode = function
  | Mprim
      {
        prim = `Left;
        args =
          [
            Mprim
              {
                prim = `Left;
                args = [Mprim {prim = `Unit; args = []; annots = []}];
                annots = [];
              };
          ];
        annots = [];
      } -> Set_position_pause
  | Mprim
      {
        prim = `Left;
        args =
          [
            Mprim
              {
                prim = `Right;
                args = [Mprim {prim = `Unit; args = []; annots = []}];
                annots = [];
              };
          ];
        annots = [];
      } -> Update_position_pause
  | Mprim
      {
        prim = `Right;
        args =
          [
            Mprim
              {
                prim = `Left;
                args = [Mprim {prim = `Unit; args = []; annots = []}];
                annots = [];
              };
          ];
        annots = [];
      } -> X_to_y_pause
  | Mprim
      {
        prim = `Right;
        args =
          [
            Mprim
              {
                prim = `Right;
                args = [Mprim {prim = `Unit; args = []; annots = []}];
                annots = [];
              };
          ];
        annots = [];
      } -> Y_to_x_pause
  | _ -> failwith "unknown primitive in output_sumtype_decode"

let type0_micheline =
  Mprim
    {
      prim = `or_;
      args =
        [
          Mprim
            {
              prim = `or_;
              args =
                [
                  unit_micheline_with_annot "set_position_pause";
                  unit_micheline_with_annot "update_position_pause";
                ];
              annots = [];
            };
          Mprim
            {
              prim = `or_;
              args =
                [
                  unit_micheline_with_annot "x_to_y_pause";
                  unit_micheline_with_annot "y_to_x_pause";
                ];
              annots = [];
            };
        ];
      annots = [];
    }

let type0_generator =
  chooseFrom
    [
      (fun () -> Set_position_pause);
      (fun () -> Update_position_pause);
      (fun () -> X_to_y_pause);
      (fun () -> Y_to_x_pause);
    ]

type set_pause = type0 set

let set_pause_encode (arg : set_pause) : micheline =
  (set_encode type0_encode) arg

let set_pause_decode = set_decode type0_decode

let set_pause_micheline = set_micheline type0_micheline

let set_pause_generator () = (set_generator type0_generator) ()

let call_set_pause ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_pause) =
  let param =
    {
      entrypoint = EPnamed "set_pause";
      value = Micheline (set_pause_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_set_pause ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_pause) =
  let param =
    {
      entrypoint = EPnamed "set_pause";
      value = Micheline (set_pause_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_set_pause ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_set_pause ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_set_pause ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_set_pause ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_set_pause ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_set_pause ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_set_pause ~assert_success ~msg ~scenario ~from ~amount ~kt1 ~network
    ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_pause"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:(mk_micheline scenario (set_pause_encode param))

type set_owner = address

let set_owner_encode : set_owner -> micheline = address_encode

let set_owner_decode (m : micheline) : set_owner = address_decode m

let set_owner_micheline = address_micheline

let set_owner_generator () = address_generator ()

let call_set_owner ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_owner) =
  let param =
    {
      entrypoint = EPnamed "set_owner";
      value = Micheline (set_owner_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_set_owner ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_owner) =
  let param =
    {
      entrypoint = EPnamed "set_owner";
      value = Micheline (set_owner_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_set_owner ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_set_owner ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_set_owner ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_set_owner ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_set_owner ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_set_owner ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_set_owner ~assert_success ~msg ~scenario ~from ~amount ~kt1 ~network
    ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_owner"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:(mk_micheline scenario (set_owner_encode param))

type set_dev_fee = nat

let set_dev_fee_encode : set_dev_fee -> micheline = nat_encode

let set_dev_fee_decode (m : micheline) : set_dev_fee = nat_decode m

let set_dev_fee_micheline = nat_micheline

let set_dev_fee_generator () = nat_generator ()

let call_set_dev_fee ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_dev_fee) =
  let param =
    {
      entrypoint = EPnamed "set_dev_fee";
      value = Micheline (set_dev_fee_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_set_dev_fee ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_dev_fee) =
  let param =
    {
      entrypoint = EPnamed "set_dev_fee";
      value = Micheline (set_dev_fee_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_set_dev_fee ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_set_dev_fee ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_set_dev_fee ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_set_dev_fee ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_set_dev_fee ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_set_dev_fee ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_set_dev_fee ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_dev_fee"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:(mk_micheline scenario (set_dev_fee_encode param))

type fa2 = {
  token_id : set_dev_fee;
  token_address : set_owner;
}

let fa2_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [set_dev_fee_encode arg.token_id; set_owner_encode arg.token_address];
      annots = [];
    }

let fa2_decode (m : micheline) : fa2 =
  let token_id, token_address =
    (tuple2_decode set_dev_fee_decode set_owner_decode) m in
  {token_id : set_dev_fee; token_address : set_owner}

let fa2_micheline =
  Mprim
    {
      prim = `pair;
      args = [set_dev_fee_micheline; set_owner_micheline];
      annots = [];
    }

let fa2_generator () =
  {token_id = set_dev_fee_generator (); token_address = set_owner_generator ()}

type token_y =
  | Fa12 of set_owner
  | Fa2 of fa2

let token_y_encode arg =
  match arg with
  | Fa12 x -> Mprim {prim = `Left; args = [set_owner_encode x]; annots = []}
  | Fa2 x -> Mprim {prim = `Right; args = [fa2_encode x]; annots = []}

let token_y_decode = function
  | Mprim {prim = `Left; args = [x]; annots = []} when is_leaf x ->
    Fa12 (set_owner_decode x)
  | Mprim {prim = `Right; args = [x]; annots = []} when is_leaf x ->
    Fa2 (fa2_decode x)
  | _ -> failwith "unknown primitive in output_sumtype_decode"

let token_y_micheline =
  Mprim
    {
      prim = `or_;
      args =
        [
          mich_annot ~annot:"fa12" set_owner_micheline;
          mich_annot ~annot:"fa2" fa2_micheline;
        ];
      annots = [];
    }

let token_y_generator =
  chooseFrom
    [
      (fun () -> Fa12 (set_owner_generator ()));
      (fun () -> Fa2 (fa2_generator ()));
    ]

type cur_tick_index = int_michelson

let cur_tick_index_encode : cur_tick_index -> micheline = int_michelson_encode

let cur_tick_index_decode (m : micheline) : cur_tick_index =
  int_michelson_decode m

let cur_tick_index_micheline = int_michelson_micheline

let cur_tick_index_generator () = int_michelson_generator ()

type deploy_pool = {
  cur_tick_index : cur_tick_index;
  token_x : token_y;
  token_y : token_y;
  fee_bps : set_dev_fee;
  tick_spacing : set_dev_fee;
  extra_slots : set_dev_fee;
}

let deploy_pool_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          cur_tick_index_encode arg.cur_tick_index;
          token_y_encode arg.token_x;
          token_y_encode arg.token_y;
          set_dev_fee_encode arg.fee_bps;
          set_dev_fee_encode arg.tick_spacing;
          set_dev_fee_encode arg.extra_slots;
        ];
      annots = [];
    }

let deploy_pool_decode (m : micheline) : deploy_pool =
  let cur_tick_index, token_x, token_y, fee_bps, tick_spacing, extra_slots =
    (tuple6_decode
       cur_tick_index_decode
       token_y_decode
       token_y_decode
       set_dev_fee_decode
       set_dev_fee_decode
       set_dev_fee_decode)
      m in
  {
    cur_tick_index : cur_tick_index;
    token_x : token_y;
    token_y : token_y;
    fee_bps : set_dev_fee;
    tick_spacing : set_dev_fee;
    extra_slots : set_dev_fee;
  }

let deploy_pool_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          cur_tick_index_micheline;
          token_y_micheline;
          token_y_micheline;
          set_dev_fee_micheline;
          set_dev_fee_micheline;
          set_dev_fee_micheline;
        ];
      annots = [];
    }

let deploy_pool_generator () =
  {
    cur_tick_index = cur_tick_index_generator ();
    token_x = token_y_generator ();
    token_y = token_y_generator ();
    fee_bps = set_dev_fee_generator ();
    tick_spacing = set_dev_fee_generator ();
    extra_slots = set_dev_fee_generator ();
  }

let call_deploy_pool ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : deploy_pool) =
  let param =
    {
      entrypoint = EPnamed "deploy_pool";
      value = Micheline (deploy_pool_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_deploy_pool ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : deploy_pool) =
  let param =
    {
      entrypoint = EPnamed "deploy_pool";
      value = Micheline (deploy_pool_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_deploy_pool ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_deploy_pool ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_deploy_pool ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_deploy_pool ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_deploy_pool ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_deploy_pool ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_deploy_pool ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"deploy_pool"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:(mk_micheline scenario (deploy_pool_encode param))

type confirm_owner = set_position_pause

let confirm_owner_encode : confirm_owner -> micheline =
  set_position_pause_encode

let confirm_owner_decode = set_position_pause_decode

let confirm_owner_micheline = set_position_pause_micheline

let confirm_owner_generator () = set_position_pause_generator ()

let call_confirm_owner ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : confirm_owner) =
  let param =
    {
      entrypoint = EPnamed "confirm_owner";
      value = Micheline (confirm_owner_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_confirm_owner ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : confirm_owner) =
  let param =
    {
      entrypoint = EPnamed "confirm_owner";
      value = Micheline (confirm_owner_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_confirm_owner ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_confirm_owner ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_confirm_owner ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_confirm_owner ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_confirm_owner ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_confirm_owner ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_confirm_owner ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"confirm_owner"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:(mk_micheline scenario (confirm_owner_encode param))

type type4 = {
  fee_bps : set_dev_fee;
  token_x : token_y;
  token_y : token_y;
}

let type4_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          set_dev_fee_encode arg.fee_bps;
          token_y_encode arg.token_x;
          token_y_encode arg.token_y;
        ];
      annots = [];
    }

let type4_decode (m : micheline) : type4 =
  let fee_bps, token_x, token_y =
    (tuple3_decode set_dev_fee_decode token_y_decode token_y_decode) m in
  {fee_bps : set_dev_fee; token_x : token_y; token_y : token_y}

let type4_micheline =
  Mprim
    {
      prim = `pair;
      args = [set_dev_fee_micheline; token_y_micheline; token_y_micheline];
      annots = [];
    }

let type4_generator () =
  {
    fee_bps = set_dev_fee_generator ();
    token_x = token_y_generator ();
    token_y = token_y_generator ();
  }

type _storage = {
  pool_ids : (type4, set_dev_fee) big_map;
  pool_count : set_dev_fee;
  owner : set_owner;
  dev_fee_bps : set_dev_fee;
  pause_state : type0 set;
  pending_owner : set_owner option;
  pools : (set_dev_fee, set_owner) big_map;
}

let _storage_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          Mprim
            {
              prim = `Pair;
              args =
                [
                  Mprim
                    {
                      prim = `Pair;
                      args =
                        [
                          set_dev_fee_encode arg.dev_fee_bps;
                          set_owner_encode arg.owner;
                        ];
                      annots = [];
                    };
                  (set_encode type0_encode) arg.pause_state;
                  (option_encode set_owner_encode) arg.pending_owner;
                ];
              annots = [];
            };
          Mprim
            {
              prim = `Pair;
              args =
                [
                  set_dev_fee_encode arg.pool_count;
                  (big_map_encode type4_encode set_dev_fee_encode) arg.pool_ids;
                ];
              annots = [];
            };
          (big_map_encode set_dev_fee_encode set_owner_encode) arg.pools;
        ];
      annots = [];
    }

let _storage_decode (m : micheline) : _storage =
  let ( ((dev_fee_bps, owner), pause_state, pending_owner),
        (pool_count, pool_ids),
        pools ) =
    (tuple3_decode
       (tuple3_decode
          (tuple2_decode set_dev_fee_decode set_owner_decode)
          (set_decode type0_decode)
          (option_decode set_owner_decode))
       (tuple2_decode
          set_dev_fee_decode
          (big_map_decode type4_decode set_dev_fee_decode))
       (big_map_decode set_dev_fee_decode set_owner_decode))
      m in
  {
    pool_ids : (type4, set_dev_fee) big_map;
    pool_count : set_dev_fee;
    owner : set_owner;
    dev_fee_bps : set_dev_fee;
    pause_state : type0 set;
    pending_owner : set_owner option;
    pools : (set_dev_fee, set_owner) big_map;
  }

let _storage_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          Mprim
            {
              prim = `pair;
              args =
                [
                  Mprim
                    {
                      prim = `pair;
                      args = [set_dev_fee_micheline; set_owner_micheline];
                      annots = [];
                    };
                  set_micheline type0_micheline;
                  option_micheline set_owner_micheline;
                ];
              annots = [];
            };
          Mprim
            {
              prim = `pair;
              args =
                [
                  set_dev_fee_micheline;
                  big_map_micheline type4_micheline set_dev_fee_micheline;
                ];
              annots = [];
            };
          big_map_micheline set_dev_fee_micheline set_owner_micheline;
        ];
      annots = [];
    }

let _storage_generator () =
  {
    pool_ids = (big_map_generator type4_generator set_dev_fee_generator) ();
    pool_count = set_dev_fee_generator ();
    owner = set_owner_generator ();
    dev_fee_bps = set_dev_fee_generator ();
    pause_state = (set_generator type0_generator) ();
    pending_owner = (option_generator set_owner_generator) ();
    pools = (big_map_generator set_dev_fee_generator set_owner_generator) ();
  }

let deploy ?(amount = 0L) ?(node = "https://tz.functori.com")
    ?(name = "No name provided") ?(from = Blockchain.bootstrap1) storage =
  let storage = _storage_encode storage in
  Blockchain.deploy ~amount ~node ~name ~from ~code (Micheline storage)

let mk_deploy scenario ~from ~amount ~network ~storage =
  let open Scenario_dsl.AstInterface in
  mk_deploy
    ~scenario
    ~from
    ~amount
    ~network
    ~storage:(mk_micheline scenario (_storage_encode storage))
    ~contract_name:"quipu"

let test_storage_download ~kt1 ~node () =
  let open Tzfunc.Rp in
  let open Blockchain in
  Lwt_main.run
  @@ let>? storage =
       get_storage ~node ~debug:(!Factori_types.debug > 0) kt1 _storage_decode
     in
     let storage_reencoded = _storage_encode storage in
     Lwt.return_ok @@ Factori_types.output_debug
     @@ Format.asprintf
          "Done downloading storage: %s."
          (Ezjsonm_interface.to_string
             (Json_encoding.construct micheline_enc.json storage_reencoded))

let initial_blockchain_storage =
  {
    dev_fee_bps = Z.of_string "3000";
    owner = "tz1dNhPvn8KmnYGV97G55bzDqnvJt6K2dVwf";
    pause_state = [];
    pending_owner = None;
    pool_count = Z.of_string "68";
    pool_ids = Abstract (Z.of_string "380855");
    pools = Abstract (Z.of_string "380856");
  }
(*
 {"prim":"Pair","args":[{"prim":"Pair","args":[{"prim":"Pair","args":[{"int":"3000"},{"string":"tz1dNhPvn8KmnYGV97G55bzDqnvJt6K2dVwf"}]},[],{"prim":"None"}]},{"prim":"Pair","args":[{"int":"68"},{"int":"380855"}]},{"int":"380856"}]}
*)
