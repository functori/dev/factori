
from importlib.metadata import metadata
import governor_python_interface
import blockchain

def exec(target):
    storage = {'targetAddress' : target,'multisigAddress' : governor_python_interface.breakGlass_generator (),'daoAddress' : blockchain.bob['pkh']}
    kt1 = governor_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= storage,
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 governor " + str(kt1) + " sent to a node.")
    return kt1
