(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions that generate C# code for testing the
Tezos client against various input types, including base, unary, and binary
types. It depends on the `Types` module and includes functions for generating
random and constant generators for different types. Warning: this short summary
was automatically generated and could be incomplete or misleading. If you feel
this is the case, please open an issue. *)

open Types
open Base_type

let int_base = [Nat; Int; Mutez; Timestamp]

let string_base = [String; Address; Keyhash; Key; Chain_id; Signature]

let utils =
  {|    

public class Utils
    {

public class ShakespeareGenerator
{
    private static readonly string[] Quotes = new[] {
        "To be or not to be",
        "All the world's a stage",
        "To thine own self be true",
        "All that glitters is not gold",
        "There is nothing either good or bad",
        "To err is human, to forgive divine",
        "The lady doth protest too much",
        "To be, and not to be",
        "All's well that ends well",
        "A rose by any other name",
        "To sleep, perchance to dream",
        "To be, or not to be: that is the question"
    };

    public static string GetRandomQuote()
    {
        var random = new Random();
        var index = random.Next(Quotes.Length);
        return Quotes[index];
    }
}

   public class LocalRandom {
    private static System.Random random = new System.Random();
    public static int randomInt(int low, int high){
      int num = random.Next(0,high);
      return num;
   }
   }
   

   public static T chooseFrom<T>(List<T> l){
   System.Random random = new System.Random();
   int num = LocalRandom.randomInt(0,l.Count);
   return l[num];
   }

      public static byte[] StringToByteArray(string hex) {
          return Enumerable.Range(0, hex.Length)
                           .Where(x => x % 2 == 0)
                           .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                           .ToArray();
      }
    
    public static List<IMicheline> flatPair(int number_to_flat,List<IMicheline> am){
             int n = number_to_flat - am.Count;
             if(n <= 0){
                int size = number_to_flat - 1;
                IEnumerable<IMicheline> first = am.Take(size);
                IEnumerable<IMicheline> rest = am.Skip(size);
                IMicheline m;
                if(rest.Count() == 1){
                   m = rest.Last();
                }
                else{
                   m = new MichelinePrim{Prim = PrimType.Pair, Args=new List<IMicheline>(rest)};
                }
                List<IMicheline> result = new List<IMicheline>(first);
                result.Add(m);
                return result;
             }else{
                switch(am.Last()) {
                    case MichelinePrim mp:
                       switch(mp.Prim){
                           case PrimType.Pair:
                             if(mp.Args == null){
                                throw new DecodeError($"Can't have a Pair with Args == null : {GetIMichelineString(mp)}");
                             }
                             IEnumerable<IMicheline> ienum = am.Take(am.Count() - 1);
                             List<IMicheline> next = new List<IMicheline>(ienum);
                             next.AddRange(mp.Args);
                             return flatPair(number_to_flat,next);
                           default:
                               throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {mp}");
                       }
                    default:
                        throw new DecodeError($"This is a prim but not a pair, can't be in TupleDecode {am.Last()}");
                }
             }
    }

   public static List<T> ListGenerator<T>() where T : new(){
                    var res = new List<T> ();
                        int size = LocalRandom.randomInt(0,5);
                        for (int i = 0; i < size; i++)
                        {
                            res.Add(new T());
                        }
                        return res;
              }

   public static Dictionary<TKey,TValue> DictionaryGenerator<TKey,TValue>() where TKey : notnull, new() where TValue : new(){
            int size = LocalRandom.randomInt(0,5);
            var dict = new Dictionary<TKey,TValue> ();
            for (int i = 0; i < size; i++){
                var key = new TKey();
                if(!dict.ContainsKey(key)){
                  dict.Add(key,new TValue());
                }
            }
            return dict;
        }


    public static string GetIMichelineString(IMicheline im)
    {
        var sb = new StringBuilder();
        if (im is MichelinePrim mp)
        {
            sb.AppendLine($"MichelinePrim: {{ Prim: {mp.Prim} }}");
            sb.AppendLine("(Args:");
            if(mp.Args == null){
                sb.Append("Null");
            }
            else{
            foreach (var arg in mp.Args)
            {
                sb.Append(GetIMichelineString(arg));
            }
            }
            sb.AppendLine("EndArgs)\n");
        }
        else if (im is MichelineArray ma)
        {
            sb.AppendLine("MichelineArray: [");
            foreach (var item in ma)
            {
                sb.Append(GetIMichelineString(item));
                sb.AppendLine(",");
            }
            sb.AppendLine("]");
        }
        else if (im is MichelineInt mi)
        {
            sb.AppendLine($"MichelineInt: {mi.Value}");
        }
        else if (im is MichelineString ms)
        {
            sb.AppendLine($"MichelineString: \"{ms.Value}\"");
        }
        else if (im is MichelineBytes mb)
        {
            sb.AppendLine($"MichelineBytes");
        }
        else
        {
            sb.AppendLine($"Unknown IMicheline object: {im.ToString()}");
        }
        return sb.ToString();
    }

    public static void PrintIMicheline(IMicheline im)
    {
        string imString = GetIMichelineString(im);
        Console.Write(imString);
    }


             }|}

let interface =
  {|
   
   public interface IFactoriT {
        static abstract IMicheline GetMicheline();
        IMicheline Encode();
   
        static abstract IFactoriT GenericDecode(IMicheline im);
   }
   |}

let base_class ppf b =
  let literal = base_literal_type b in
  let pp_csharp_base_type =
    pp_base_type ~language:Factori_config.Csharp ~abstract:false ?prefix:None
  in
  match literal with
  | Cunknown ->
    Format.fprintf ppf
      "public class %a : IFactoriT { \n\
       \tpublic %a() {}\n\
       \t%a\n\
       \t%a\n\
       \t%a\n\
       \t%a\n\n\
       %a\n\n\
       }"
      pp_csharp_base_type b pp_csharp_base_type b
  | _ ->
    let str_name = Format.asprintf "%a" Types.pp_literal_base_type b in
    Format.fprintf ppf
      {|public class %a : IFactoriT{
     private %s _value;
     public %a(%s value){
     _value = value;
     }
     public static implicit operator %s(%a obj) => obj._value;
     public static implicit operator %a(%s value) => new %a(value);
       %a
       %a
       %a
       %a
       %a
}|}
      pp_csharp_base_type b str_name pp_csharp_base_type b str_name str_name
      pp_csharp_base_type b pp_csharp_base_type b str_name pp_csharp_base_type b

let unary_class ppf u =
  match u with
  | List ->
    Format.fprintf ppf
      {|public class MList<T> : IFactoriT where T : notnull, IFactoriT, new(){
            public List<T> _value;

            public MList(List<T> value) { this._value = new List<T>(value); }
       
            public static implicit operator MList<T>(List<T> l) => new MList<T>(l);
            public static implicit operator List<T>(MList<T> l) => l._value;
       %a
       %a
       %a
       %a
       %a
       }|}
  | Set ->
    Format.fprintf ppf
      {|public class Set<T> : List<T>, IFactoriT where T : notnull, IFactoriT, new(){

            public Set(List<T> value) : base(value) { }
       %a
       %a
       %a
       %a
       %a
       }|}
  | Option ->
    Format.fprintf ppf
      {|
public abstract class ContentOption<T>  where T : notnull, IFactoriT, new()
    {  
        public abstract IMicheline subEncode();
        }

    public class Some<T> : ContentOption<T> where T : notnull, IFactoriT, new(){
            public T Value {get; set;}
            public Some(T value){Value = value;}
            public Some() { Value = new T();}

        public override IMicheline subEncode()
        {
            return new MichelinePrim{ Prim = PrimType.Some, Args = new List<IMicheline> { Value.Encode() } };
        }
    }

        public class None<T> : ContentOption<T> where T : notnull, IFactoriT, new(){
        public override IMicheline subEncode()
        {
            return new MichelinePrim{Prim = PrimType.None};
        }
        }

        public class Option<T> : IFactoriT
        where T : notnull, IFactoriT, new()
        { public ContentOption<T> _contentOption;
            public Option(ContentOption<T> contentOption) { _contentOption = contentOption; }
       
        public static implicit operator Option<T>(ContentOption<T> contentOption) => new Option<T>(contentOption);
            public static implicit operator ContentOption<T>(Option<T> option) => option._contentOption;

       %a
 
       %a

        %a

        %a

       %a
        }
       |}
  | Ticket ->
    Format.fprintf ppf
      {|public class Ticket<T> : IFactoriT where T : IFactoriT, new()
    {
        private Address _addr;
        private T _value;
        private Nat _amount;

        public Ticket(Address addr, T value, Nat amount){
            _addr = addr;
            _value = value;
            _amount = amount;
        }
       %a
       %a
       %a
       %a
       %a
     }|}

let binary_class ppf bin =
  match bin with
  | Map ->
    Format.fprintf ppf
      {|public class Map<TKey,TValue> : Dictionary<TKey, TValue>, IFactoriT
       where TKey : notnull, IFactoriT, new()
       where TValue : notnull, IFactoriT, new(){
          public Map(Dictionary<TKey,TValue> dict) : base(dict){ }
          
       %a
       %a
       %a
       %a
       %a
       }|}
  | BigMap ->
    Format.fprintf ppf
      {|public abstract class ContentBigMap<TKey, TValue>  where TKey : notnull, IFactoriT, new()
        where TValue : notnull, IFactoriT, new()
    {  
        public abstract IMicheline subEncode();
        }

        public class LiteralBigMap<TKey, TValue> : ContentBigMap<TKey, TValue>
        where TKey : notnull, IFactoriT, new()
        where TValue : notnull, IFactoriT, new()
        {
            public Dictionary<TKey, TValue> _value;
            public LiteralBigMap(Dictionary<TKey, TValue> dict) { _value = dict; }
            public LiteralBigMap() { _value = new Dictionary<TKey, TValue>(); }
            public static implicit operator LiteralBigMap<TKey,TValue>(Dictionary<TKey,TValue> dict) => new LiteralBigMap<TKey,TValue>(dict);
            public static implicit operator Dictionary<TKey,TValue> (LiteralBigMap<TKey,TValue> bm) => bm._value;
            public override IMicheline subEncode(){
                return DictEncode<TKey,TValue>(_value);
            }
        }

        public class AbstractBigMap<TKey, TValue> : ContentBigMap<TKey,TValue>
        where TKey : notnull, IFactoriT, new()
        where TValue : notnull, IFactoriT, new()
        {
            public BigInteger _value;
            public AbstractBigMap(BigInteger n)
            {
                _value = n;
            }

            public static implicit operator AbstractBigMap<TKey,TValue>(BigInteger i) => new AbstractBigMap<TKey,TValue>(i);
            public static implicit operator BigInteger(AbstractBigMap<TKey,TValue> bm) => bm._value;

            public override IMicheline subEncode()
            {
                return new MichelineInt(_value);
            }
        }

        public class BigMap<TKey, TValue> : IFactoriT  where TKey : notnull, IFactoriT, new()
        where TValue : notnull, IFactoriT, new()
    {  
        public ContentBigMap<TKey,TValue> _contentBigMap;

        public BigMap(ContentBigMap<TKey,TValue> contentBigMap){
            _contentBigMap = contentBigMap;
        }
       public async Task<TValue?> Get(TKey key,string network){
         return await Blockchain.Functions.BigMapValue<TKey,TValue>(key,this,network);
       }
       public static implicit operator BigMap<TKey,TValue>(ContentBigMap<TKey,TValue> i) => new BigMap<TKey,TValue>(i);
       public static implicit operator ContentBigMap<TKey,TValue>(BigMap<TKey,TValue> i) => i._contentBigMap;
       %a
       %a
       %a
       %a
       %a
       }|}
  | Lambda ->
    Format.fprintf ppf
      {|
       public class Lambda<TKey,TValue> : IFactoriT  where TKey : notnull, IFactoriT
        where TValue : notnull, IFactoriT{
            public IMicheline _body;
            public Lambda(IMicheline body) {this._body = body;}
       %a
       %a
       %a
       %a
       %a
       }
       |}

let contract_class ppf () =
  Format.fprintf ppf
    "public class Contract<T> : IFactoriT where T : IFactoriT {\n\
     \t%a\n\
     \t%a\n\
     \t%a\n\
     \t%a\n\
     \t%a\n\
     }"

let generate_types ppf () =
  let open Csharp_side_generation in
  Format.pp_print_list ~pp_sep:Format.pp_print_newline
    (fun ppf ty ->
      let class_type =
        match ty with
        | ABase b -> base_class ppf b
        | AUnary u -> unary_class ppf u
        | ABinary bin -> binary_class ppf bin
        | AContract -> contract_class ppf ()
        | ATuple _ -> failwith "No Tuple in this generation" in
      class_type build_micheline ty build_generator ty build_decoder ty
        build_encoder ty generic_decode ty)
    ppf generate_all_kinds

let generate_factori_types ppf () =
  Format.fprintf ppf
    {|

using Netezos.Encoding;
using System.Numerics;
using System.Text;
using System.Collections;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Asn1.Cmp;

namespace FactoriTypes
{


    public class EncodeError : Exception {
        public EncodeError(){
        }

        public EncodeError(string msg) : base(msg) {}
    }

    public class DecodeError : Exception {
        public DecodeError(){
        }

        public DecodeError(string msg) : base(msg) {}
    }

    public class HandleErrors{

    public void HandleEncodingException(EncodeError e){
        Console.WriteLine($"Encoding error: {e}");
    }

    public void HandleDecodingException(EncodeError e){
        Console.WriteLine($"Decoding error: {e}");
    }

    }

    %s
public class Types
    {
    %s
                    
    %s

    %s

    %a

    %a
 }
}
                    |}
    utils interface Csharp_side_generation.encode_lib
    Csharp_side_generation.decode_lib generate_types ()
    Csharp_tuple_boilerplate.tuple_boilerplate 25
