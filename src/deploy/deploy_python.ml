(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Python *)

open Format
open Factori_utils
open Factori_config

module Generate = struct
  let blockchain_python_storage = sprintf "initial_blockchain_storage"

  let random_python_storage = sprintf "storage_generator()"

  let storage_python ~storage_type =
    match storage_type with
    | Random -> random_python_storage
    | Blockchain -> blockchain_python_storage

  (* The python script that will deploy the KT1 by using the generated interface *)
  let deploy_python ~contract_name ~network ~from ~storage_type ppf () =
    let get_from from = sprintf "blockchain.%s" from in
    let interface contract = sprintf "%s_python_interface" contract in
    let contract_interface = interface contract_name in
    fprintf ppf
      {|
from importlib.metadata import metadata
from %s import %s
from %s import blockchain

def deploy():
    kt1 = %s.deploy(
        _from=%s,
        initial_storage= %s.%s,
        amount=10000,
        network=%S,
        debug=False,
    )
    print("Deploy %s contract at " + str(kt1) + " on %s.")
    return kt1
|}
      Python_SDK.interface_dir_name contract_interface
      Python_SDK.libraries_dir_name contract_interface (get_from from)
      contract_interface
      (storage_python ~storage_type)
      network contract_name network

  let main_file ~scenario_name =
    Format.sprintf
      {|
from %s import %s

%s.main()
                  |}
      Python_SDK.scenarios_dir_name scenario_name scenario_name

  let scenario_file ~contract_name =
    Format.sprintf
      {|
from %s import %s

def main():
    %s.deploy()

|}
      Python_SDK.deploy_dir_name contract_name contract_name
end

let deploy ~storage_type ~contract_name ~network ~from () =
  let dir = get_dir () in
  let deploy_file =
    asprintf "%a"
      (Generate.deploy_python ~contract_name ~network ~from ~storage_type)
      () in
  let deploy_dir = Python_SDK.deploy_dir ~dir () in
  Python_SDK.generate_deploy ~dir () ;
  let deploy_name_python = sprintf "%s.py" contract_name in
  let full_deploy_path = deploy_dir // deploy_name_python in
  write_file ~filename:full_deploy_path deploy_file ;
  let scenario_name = sprintf "deploy_scenario_%s" contract_name in
  let scenario_file = Generate.scenario_file ~contract_name in
  let filename_scenario =
    Python_SDK.scenarios_dir ~dir () // Format.sprintf "%s.py" scenario_name
  in
  write_file ~filename:filename_scenario scenario_file ;
  let main_file = Generate.main_file ~scenario_name in
  let name_file_main = Format.sprintf "main_deploy_%s.py" contract_name in
  let base_dir = Python_SDK.base_dir ~dir () in
  write_file ~filename:(base_dir // name_file_main) main_file ;
  let cmd = sprintf "cd %s && python3 %s" base_dir name_file_main in
  cmd
