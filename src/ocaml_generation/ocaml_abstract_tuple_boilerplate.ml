(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file provides utilities for generating boilerplate code for
abstract tuples of different lengths in the Factori codebase. It includes
functions to generate type definitions, lift functions, and conversion functions
for abstract tuples. Warning: this short summary was automatically generated and
could be incomplete or misleading. If you feel this is the case, please open an
issue. *)

open Factori_utils

let abstract_tuple ppf i =
  let open Format in
  fprintf ppf "  type (%a) tuple%d = (%a) id_or_concrete"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "'x%d" i))
    (list_integers i) i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " * ")
       (fun ppf i -> fprintf ppf "'x%d" i))
    (list_integers i)

let lift_abstract_tuple ppf i =
  let open Format in
  fprintf ppf "  let lift_tuple%d %a (%a : %a) = Concrete (%a)" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf " ")
       (fun ppf i -> fprintf ppf "lift%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "x%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "*")
       (fun ppf i -> fprintf ppf "'a%d" i))
    (list_integers i)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf i -> fprintf ppf "lift%d x%d" i i))
    (list_integers i)

let abstract_tuple_value_of ppf i =
  let open Format in
  let rec aux ppf = function
    | [] -> failwith "[abstract_tuple_value_of] never happens: empty list"
    | [i] -> fprintf ppf "Single(f%d x%d)" i i
    | i :: l -> fprintf ppf "Multi(f%d x%d,%a)" i i aux l in

  if i = 1 then
    fprintf ppf
      "  let value_of_tuple1 f =\n\
       bind_abstract_value\n\
       (fun a -> VTuple (Single (f a)))"
  else
    fprintf ppf
      "  let value_of_tuple%d %a =\n\
       bind_abstract_value\n\
       (fun (%a) -> VTuple (%a))" i
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf " ")
         (fun ppf i -> fprintf ppf "f%d" i))
      (list_integers i)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf i -> fprintf ppf "x%d" i))
      (list_integers i) aux (list_integers i)

let abstract_tuple_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:1 in
  Format.asprintf "\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> abstract_tuple ppf i))
    (list_integers n)

let abstract_tuple_values_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:1 in
  Format.asprintf "\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> abstract_tuple_value_of ppf i))
    (list_integers n)

let abstract_tuple_lifts_boilerplate n =
  let open Format in
  let list_integers = list_integers ~from:1 in
  Format.asprintf "\n%a\n"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf i -> lift_abstract_tuple ppf i))
    (list_integers n)
