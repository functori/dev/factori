using FactoriTypes;
using System.Numerics;
using Netezos.Encoding;
using Netezos.Rpc;
using Netezos.Keys;
namespace rarible
{
    public class Type0 { }
    class Type0_Right_constructor_subtype : Type0
    {
        public string kind = "Right_constructor";
        public Types.Address? Right_element { get; set; }
    }

    class Type0_Left_constructor_subtype : Type0
    {
        public string kind = "Left_constructor";
        public Types.Address? Left_element { get; set; }
    }






    public class Update_operators_for_all : List<Type0> { }





    public class Token_id : Types.Nat
    {
        public Token_id(Types.Nat v) : base(v) { }
        public static implicit operator BigInteger(Token_id a) => (BigInteger)(Types.Nat)a;
        public static implicit operator Token_id(BigInteger a) => (Token_id)(Types.Nat)a;
    }



    public class Constructor_operator : Types.Address
    {
        public Constructor_operator(Types.Address v) : base(v) { }
        public static implicit operator string(Constructor_operator a) => (string)(Types.Address)a;
        public static implicit operator Constructor_operator(string a) => (Constructor_operator)(Types.Address)a;
    }



    public record Add_operator
    {
        public Token_id token_id { get; set; }
        public Constructor_operator _operator { get; set; }
        public Constructor_operator owner { get; set; }
        public Add_operator(Token_id token_id, Constructor_operator _operator, Constructor_operator owner)
        {
            this.token_id = token_id;
            this._operator = _operator;
            this.owner = owner;
        }
    }



    public class Type1 { }
    class Type1_Remove_operator_constructor_subtype : Type1
    {
        public string kind = "Remove_operator_constructor";
        public Add_operator? Remove_operator_element { get; set; }
    }

    class Type1_Add_operator_constructor_subtype : Type1
    {
        public string kind = "Add_operator_constructor";
        public Add_operator? Add_operator_element { get; set; }
    }






    public class Update_operators : List<Type1> { }





    public class Unpause : Types.Unit
    {
        public Unpause() : base() { }
        public Unpause(Types.Unit u) : base() { }
    };





    public record Type3
    {
        public Token_id amount { get; set; }
        public Token_id token_id { get; set; }
        public Constructor_operator _to { get; set; }
        public Type3(Token_id amount, Token_id token_id, Constructor_operator _to)
        {
            this.amount = amount;
            this.token_id = token_id;
            this._to = _to;
        }
    }



    public class Transfer_gasless : List<Types.MyTuple<List<Types.MyTuple<Constructor_operator, List<Type3>>>, Types.MyTuple<Types.Key, Types.Signature>>> { }





    public class Transfer : List<Types.MyTuple<Constructor_operator, List<Type3>>> { }





    public class Idata : Types.Bytes
    {
        public Idata(Types.Bytes v) : base(v) { }
        public static implicit operator byte[](Idata a) => (byte[])(Types.Bytes)a;
        public static implicit operator Idata(byte[] a) => (Idata)(Types.Bytes)a;
    }



    public class Ikey
    {
        private string _value;
        public Ikey(string str)
        {
            _value = str;
        }
        public static implicit operator string(Ikey v) => v._value;
        public static implicit operator Ikey(string s) => new Ikey(s);
    }



    public record Set_metadata
    {
        public Ikey ikey { get; set; }
        public Idata idata { get; set; }
        public Set_metadata(Ikey ikey, Idata idata)
        {
            this.ikey = ikey;
            this.idata = idata;
        }
    }





    public record Set_expiry
    {
        public Types.Option<Token_id> v { get; set; }
        public Types.Option<Idata> p { get; set; }
        public Set_expiry(Types.Option<Token_id> v, Types.Option<Idata> p)
        {
            this.v = v;
            this.p = p;
        }
    }





    public class Set_default_expiry : Token_id
    {
        public Set_default_expiry(BigInteger v) : base(v) { }
    }





    public class Constructor_sig : Types.Signature
    {
        public Constructor_sig(Types.Signature v) : base(v) { }

    }



    public class Pk : Types.Key
    {
        public Pk(Types.Key v) : base(v) { }

    }



    public record Permit
    {
        public Idata data { get; set; }
        public Constructor_sig _sig { get; set; }
        public Pk pk { get; set; }
        public Permit(Idata data, Constructor_sig _sig, Pk pk)
        {
            this.data = data;
            this._sig = _sig;
            this.pk = pk;
        }
    }





    public class Pause : Unpause
    {
        public Pause() : base() { }
    }





    public record Type7
    {
        public Constructor_operator partAccount { get; set; }
        public Token_id partValue { get; set; }
        public Type7(Constructor_operator partAccount, Token_id partValue)
        {
            this.partAccount = partAccount;
            this.partValue = partValue;
        }
    }



    public record Mint
    {
        public List<Type7> iroyalties { get; set; }
        public Types.Map<Ikey, Idata> itokenMetadata { get; set; }
        public Token_id iamount { get; set; }
        public Constructor_operator iowner { get; set; }
        public Token_id itokenid { get; set; }
        public Mint(List<Type7> iroyalties, Types.Map<Ikey, Idata> itokenMetadata, Token_id iamount, Constructor_operator iowner, Token_id itokenid)
        {
            this.iroyalties = iroyalties;
            this.itokenMetadata = itokenMetadata;
            this.iamount = iamount;
            this.iowner = iowner;
            this.itokenid = itokenid;
        }
    }





    public class Declare_ownership : Constructor_operator
    {
        public Declare_ownership(string addr) : base(addr) { }
        public static implicit operator string(Declare_ownership a) => a;
        public static implicit operator Declare_ownership(string a) => a;
    }





    public class Claim_ownership : Unpause
    {
        public Claim_ownership() : base() { }
    }





    public record Burn
    {
        public Token_id itokenid { get; set; }
        public Token_id iamount { get; set; }
        public Burn(Token_id itokenid, Token_id iamount)
        {
            this.itokenid = itokenid;
            this.iamount = iamount;
        }
    }





    public record Type10
    {
        public Constructor_operator owner { get; set; }
        public Token_id token_id { get; set; }
        public Type10(Constructor_operator owner, Token_id token_id)
        {
            this.owner = owner;
            this.token_id = token_id;
        }
    }



    public class Balance_of : Types.MyTuple<List<Type10>, Types.Contract>
    {
        public Balance_of(List<Type10> x0, Types.Contract x1) : base(x0, x1) { }
        public Balance_of(Types.MyTuple<List<Type10>, Types.Contract> tuple) : base(tuple.Item1, tuple.Item2) { }
    }





    public class Created_at : Types.Timestamp
    {
        public Created_at(Types.Timestamp v) : base(v) { }

        public static implicit operator Created_at(string s) => new Created_at(new Types.Timestamp(s));
    }



    public record Type11
    {
        public Types.Option<Token_id> expiry { get; set; }
        public Created_at created_at { get; set; }
        public Type11(Types.Option<Token_id> expiry, Created_at created_at)
        {
            this.expiry = expiry;
            this.created_at = created_at;
        }
    }



    public record Type12
    {
        public Types.Map<Idata, Type11> user_permits { get; set; }
        public Types.Option<Token_id> user_expiry { get; set; }
        public Token_id counter { get; set; }
        public Type12(Types.Map<Idata, Type11> user_permits, Types.Option<Token_id> user_expiry, Token_id counter)
        {
            this.user_permits = user_permits;
            this.user_expiry = user_expiry;
            this.counter = counter;
        }
    }



    public record Type13
    {
        public Token_id token_id { get; set; }
        public Types.Map<Ikey, Idata> token_info { get; set; }
        public Type13(Token_id token_id, Types.Map<Ikey, Idata> token_info)
        {
            this.token_id = token_id;
            this.token_info = token_info;
        }
    }



    public class Paused : Types.Bool
    {
        public Paused(Types.Bool v) : base(v) { }
        public static implicit operator bool(Paused a) => (bool)(Types.Bool)a;
        public static implicit operator Paused(bool a) => (Paused)(Types.Bool)a;
    }



    public record Constructor_storage
    {
        public Types.BigMap<Ikey, Idata> metadata { get; set; }
        public Token_id default_expiry { get; set; }
        public Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause> operator_for_all { get; set; }
        public Types.BigMap<Constructor_operator, Type12> permits { get; set; }
        public Types.BigMap<Token_id, Type13> token_metadata { get; set; }
        public Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause> _operator { get; set; }
        public Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id> ledger { get; set; }
        public Types.BigMap<Token_id, List<Type7>> royalties { get; set; }
        public Paused paused { get; set; }
        public Types.Option<Constructor_operator> owner_candidate { get; set; }
        public Constructor_operator owner { get; set; }
        public Constructor_storage(Types.BigMap<Ikey, Idata> metadata, Token_id default_expiry, Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause> operator_for_all, Types.BigMap<Constructor_operator, Type12> permits, Types.BigMap<Token_id, Type13> token_metadata, Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause> _operator, Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id> ledger, Types.BigMap<Token_id, List<Type7>> royalties, Paused paused, Types.Option<Constructor_operator> owner_candidate, Constructor_operator owner)
        {
            this.metadata = metadata;
            this.default_expiry = default_expiry;
            this.operator_for_all = operator_for_all;
            this.permits = permits;
            this.token_metadata = token_metadata;
            this._operator = _operator;
            this.ledger = ledger;
            this.royalties = royalties;
            this.paused = paused;
            this.owner_candidate = owner_candidate;
            this.owner = owner;
        }
    }



    public class Functions
    {
        public static Type0 type0Generator()
        {
            Type0_Left_constructor_subtype res0 = new Type0_Left_constructor_subtype();
            res0.kind = "Left_constructor";
            res0.Left_element = Types.AddressGenerator();
            ;
            Type0_Right_constructor_subtype res1 = new Type0_Right_constructor_subtype();
            res1.kind = "Right_constructor";
            res1.Right_element = Types.AddressGenerator();
            ;
            return Types.chooseFrom<Type0>(new List<Type0>{
res0,res1
});
        }

        public static IMicheline type0Encode(Type0 arg)
        {
            try
            {
                switch (arg)
                {
                    case (Type0_Left_constructor_subtype localarg):
                        return Types.make_left(Types.AddressEncode(localarg.Left_element == null ? throw new EncodeError("null") : localarg.Left_element));
                    case (Type0_Right_constructor_subtype localarg):
                        return Types.make_right(Types.AddressEncode(localarg.Right_element == null ? throw new EncodeError("null") : localarg.Right_element));
                    default:
                        throw new EncodeError("Unknown case in sumtype_encode");
                }
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type0Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type0 type0Decode(IMicheline arg)
        {

            switch (arg)
            {//Left case
                case MichelinePrim mp_:
                    switch (mp_.Prim)
                    {
                        case PrimType.Left:
                            if (mp_.Args == null)
                            {
                                throw new DecodeError($"{FactoriTypes.Utils.GetIMichelineString(mp_)}PrimType.Left should have an argument");
                            }
                            arg = mp_.Args[0];
                            Type0_Left_constructor_subtype res = new Type0_Left_constructor_subtype();
                            res.kind = "Left_constructor";
                            res.Left_element = Types.AddressDecode(arg);
                            return res;
                            ;

                        case PrimType.Right:
                            break;
                        default:
                            throw new DecodeError($"{mp_} should be Left or Right");
                    }
                    break;
                default:
                    throw new DecodeError($"Could not decode {arg}, it should be a Prim");
            }
            switch (arg)
            {//Right case

                case MichelinePrim mp_:
                    switch (mp_.Prim)
                    {
                        case PrimType.Right:
                            if (mp_.Args == null)
                            {
                                throw new DecodeError($"{FactoriTypes.Utils.GetIMichelineString(mp_)}PrimType.Right should have an argument");
                            }
                            arg = mp_.Args[0];
                            Type0_Right_constructor_subtype res = new Type0_Right_constructor_subtype();
                            res.kind = "Right_constructor";
                            res.Right_element = Types.AddressDecode(arg);
                            return res;
                            ;
                        case PrimType.Left:
                            break;
                        default:
                            throw new DecodeError($"{mp_} should be Left or Right");
                    }
                    break;
                default:
                    throw new DecodeError($"Could not decode {arg}, it should be a Prim");
            };
            throw new DecodeError($"Could not identify any known sumtype path for this value: {arg}");
        }

        /**/

        public static Update_operators_for_all update_operators_for_allGenerator()
        {
            return (Update_operators_for_all)(Types.ListGenerator(type0Generator))();
        }

        public static IMicheline update_operators_for_allEncode(Update_operators_for_all arg)
        {
            try
            {
                return Types.ListEncode<Type0>(type0Encode)(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in update_operators_for_allEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Update_operators_for_all update_operators_for_allDecode(IMicheline arg) { return (Update_operators_for_all)(Types.ListDecode<Type0>(type0Decode))(arg); }

        /**/



        public static Token_id token_idGenerator()
        {
            return (Token_id)new Token_id(Types.NatGenerator());
        }

        public static IMicheline token_idEncode(Token_id arg)
        {
            try
            {
                return Types.NatEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in token_idEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Token_id token_idDecode(IMicheline m) { return new Token_id((BigInteger)(Types.NatDecode(m))); }

        /**/

        public static Constructor_operator _operatorGenerator()
        {
            return (Constructor_operator)new Constructor_operator(Types.AddressGenerator());
        }

        public static IMicheline _operatorEncode(Constructor_operator arg)
        {
            try
            {
                return Types.AddressEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in _operatorEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Constructor_operator _operatorDecode(IMicheline m) { return new Constructor_operator((string)(Types.AddressDecode(m))); }

        /**/

        public static Add_operator add_operatorGenerator()
        {
            return new Add_operator(token_id: token_idGenerator(),
                _operator: _operatorGenerator(),
                owner: _operatorGenerator());
        }

        public static IMicheline add_operatorEncode(Add_operator arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.owner)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (_operatorEncode(arg._operator)),
    (token_idEncode(arg.token_id))
}
}
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in add_operatorEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Add_operator add_operatorDecode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Constructor_operator, Types.MyTuple<Constructor_operator, Token_id>>(_operatorDecode, Types.Tuple2Decode<Constructor_operator, Token_id>(_operatorDecode, token_idDecode))(arg);
            return new Add_operator(owner: (before_projection.Item1),
            _operator: (before_projection.Item2.Item1),
            token_id: (before_projection.Item2.Item2))
            ;
        }

        /**/

        public static Type1 type1Generator()
        {
            Type1_Add_operator_constructor_subtype res0 = new Type1_Add_operator_constructor_subtype();
            res0.kind = "Add_operator_constructor";
            res0.Add_operator_element = add_operatorGenerator();
            ;
            Type1_Remove_operator_constructor_subtype res1 = new Type1_Remove_operator_constructor_subtype();
            res1.kind = "Remove_operator_constructor";
            res1.Remove_operator_element = add_operatorGenerator();
            ;
            return Types.chooseFrom<Type1>(new List<Type1>{
res0,res1
});
        }

        public static IMicheline type1Encode(Type1 arg)
        {
            try
            {
                switch (arg)
                {
                    case (Type1_Add_operator_constructor_subtype localarg):
                        return Types.make_left(add_operatorEncode(localarg.Add_operator_element == null ? throw new EncodeError("null") : localarg.Add_operator_element));
                    case (Type1_Remove_operator_constructor_subtype localarg):
                        return Types.make_right(add_operatorEncode(localarg.Remove_operator_element == null ? throw new EncodeError("null") : localarg.Remove_operator_element));
                    default:
                        throw new EncodeError("Unknown case in sumtype_encode");
                }
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type1Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type1 type1Decode(IMicheline arg)
        {

            switch (arg)
            {//Left case
                case MichelinePrim mp_:
                    switch (mp_.Prim)
                    {
                        case PrimType.Left:
                            if (mp_.Args == null)
                            {
                                throw new DecodeError($"{FactoriTypes.Utils.GetIMichelineString(mp_)}PrimType.Left should have an argument");
                            }
                            arg = mp_.Args[0];
                            Type1_Add_operator_constructor_subtype res = new Type1_Add_operator_constructor_subtype();
                            res.kind = "Add_operator_constructor";
                            res.Add_operator_element = add_operatorDecode(arg);
                            return res;

                            ;

                        case PrimType.Right:
                            break;
                        default:
                            throw new DecodeError($"{mp_} should be Left or Right");
                    }
                    break;
                default:
                    throw new DecodeError($"Could not decode {arg}, it should be a Prim");
            }
            switch (arg)
            {//Right case

                case MichelinePrim mp_:
                    switch (mp_.Prim)
                    {
                        case PrimType.Right:
                            if (mp_.Args == null)
                            {
                                throw new DecodeError($"{FactoriTypes.Utils.GetIMichelineString(mp_)}PrimType.Right should have an argument");
                            }
                            arg = mp_.Args[0];
                            Type1_Remove_operator_constructor_subtype res = new Type1_Remove_operator_constructor_subtype();
                            res.kind = "Remove_operator_constructor";
                            res.Remove_operator_element = add_operatorDecode(arg);
                            return res;

                            ;
                        case PrimType.Left:
                            break;
                        default:
                            throw new DecodeError($"{mp_} should be Left or Right");
                    }
                    break;
                default:
                    throw new DecodeError($"Could not decode {arg}, it should be a Prim");
            };
            throw new DecodeError($"Could not identify any known sumtype path for this value: {arg}");
        }

        /**/

        public static Update_operators update_operatorsGenerator()
        {
            return (Update_operators)(Types.ListGenerator(type1Generator))();
        }

        public static IMicheline update_operatorsEncode(Update_operators arg)
        {
            try
            {
                return Types.ListEncode<Type1>(type1Encode)(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in update_operatorsEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Update_operators update_operatorsDecode(IMicheline arg) { return (Update_operators)(Types.ListDecode<Type1>(type1Decode))(arg); }

        /**/



        public static Unpause unpauseGenerator()
        {
            return (Unpause)new Unpause();
        }

        public static IMicheline unpauseEncode(Unpause arg)
        {
            try
            {
                return Types.UnitEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in unpauseEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Unpause unpauseDecode(IMicheline m) { return new Unpause((Types.Unit)(Types.UnitDecode(m))); }

        /**/



        public static Type3 type3Generator()
        {
            return new Type3(amount: token_idGenerator(),
                token_id: token_idGenerator(),
                _to: _operatorGenerator());
        }

        public static IMicheline type3Encode(Type3 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg._to)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (token_idEncode(arg.token_id)),
    (token_idEncode(arg.amount))
}
}
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type3Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type3 type3Decode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Constructor_operator, Types.MyTuple<Token_id, Token_id>>(_operatorDecode, Types.Tuple2Decode<Token_id, Token_id>(token_idDecode, token_idDecode))(arg);
            return new Type3(_to: (before_projection.Item1),
            token_id: (before_projection.Item2.Item1),
            amount: (before_projection.Item2.Item2))
            ;
        }

        /**/

        public static Transfer_gasless transfer_gaslessGenerator()
        {
            return (Transfer_gasless)(Types.ListGenerator((Types.Tuple2Generator<List<Types.MyTuple<Constructor_operator, List<Type3>>>, Types.MyTuple<Types.Key, Types.Signature>>((Types.ListGenerator<Types.MyTuple<Constructor_operator, List<Type3>>>((Types.Tuple2Generator<Constructor_operator, List<Type3>>(_operatorGenerator,
        (Types.ListGenerator<Type3>(type3Generator)))))),
        (Types.Tuple2Generator<Types.Key, Types.Signature>(Types.KeyGenerator,
        Types.SignatureGenerator))))))();
        }

        public static IMicheline transfer_gaslessEncode(Transfer_gasless arg)
        {
            try
            {
                return Types.ListEncode<Types.MyTuple<List<Types.MyTuple<Constructor_operator, List<Type3>>>, Types.MyTuple<Types.Key, Types.Signature>>>(Types.Tuple2Encode<List<Types.MyTuple<Constructor_operator, List<Type3>>>, Types.MyTuple<Types.Key, Types.Signature>>(Types.ListEncode<Types.MyTuple<Constructor_operator, List<Type3>>>(Types.Tuple2Encode<Constructor_operator, List<Type3>>(_operatorEncode, Types.ListEncode<Type3>(type3Encode))), Types.Tuple2Encode<Types.Key, Types.Signature>(Types.KeyEncode, Types.SignatureEncode)))(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in transfer_gaslessEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Transfer_gasless transfer_gaslessDecode(IMicheline arg) { return (Transfer_gasless)(Types.ListDecode<Types.MyTuple<List<Types.MyTuple<Constructor_operator, List<Type3>>>, Types.MyTuple<Types.Key, Types.Signature>>>(Types.Tuple2Decode<List<Types.MyTuple<Constructor_operator, List<Type3>>>, Types.MyTuple<Types.Key, Types.Signature>>(Types.ListDecode(Types.Tuple2Decode<Constructor_operator, List<Type3>>(_operatorDecode, Types.ListDecode(type3Decode))), Types.Tuple2Decode<Types.Key, Types.Signature>(Types.KeyDecode, Types.SignatureDecode))))(arg); }

        /**/



        public static Transfer transferGenerator()
        {
            return (Transfer)(Types.ListGenerator((Types.Tuple2Generator<Constructor_operator, List<Type3>>(_operatorGenerator,
        (Types.ListGenerator<Type3>(type3Generator))))))();
        }

        public static IMicheline transferEncode(Transfer arg)
        {
            try
            {
                return Types.ListEncode<Types.MyTuple<Constructor_operator, List<Type3>>>(Types.Tuple2Encode<Constructor_operator, List<Type3>>(_operatorEncode, Types.ListEncode<Type3>(type3Encode)))(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in transferEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Transfer transferDecode(IMicheline arg) { return (Transfer)(Types.ListDecode<Types.MyTuple<Constructor_operator, List<Type3>>>(Types.Tuple2Decode<Constructor_operator, List<Type3>>(_operatorDecode, Types.ListDecode(type3Decode))))(arg); }

        /**/



        public static Idata idataGenerator()
        {
            return (Idata)new Idata(Types.BytesGenerator());
        }

        public static IMicheline idataEncode(Idata arg)
        {
            try
            {
                return Types.BytesEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in idataEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Idata idataDecode(IMicheline m) { return new Idata((byte[])(Types.BytesDecode(m))); }

        /**/

        public static Ikey ikeyGenerator()
        {
            return (Ikey)new Ikey(Types.StringGenerator());
        }

        public static IMicheline ikeyEncode(Ikey arg)
        {
            try
            {
                return Types.StringEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in ikeyEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Ikey ikeyDecode(IMicheline m) { return (Types.StringDecode(m)); }

        /**/

        public static Set_metadata set_metadataGenerator()
        {
            return new Set_metadata(ikey: ikeyGenerator(),
                idata: idataGenerator());
        }

        public static IMicheline set_metadataEncode(Set_metadata arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (ikeyEncode(arg.ikey)),
    (idataEncode(arg.idata))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in set_metadataEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Set_metadata set_metadataDecode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Ikey, Idata>(ikeyDecode, idataDecode)(arg);
            return new Set_metadata(ikey: (before_projection.Item1),
            idata: (before_projection.Item2))
            ;
        }

        /**/



        public static Set_expiry set_expiryGenerator()
        {
            return new Set_expiry(v: (Types.OptionGenerator<Token_id>(token_idGenerator))(),
                p: (Types.OptionGenerator<Idata>(idataGenerator))());
        }

        public static IMicheline set_expiryEncode(Set_expiry arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (Types.OptionEncode<Token_id>(token_idEncode)(arg.v)),
    (Types.OptionEncode<Idata>(idataEncode)(arg.p))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in set_expiryEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Set_expiry set_expiryDecode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Types.Option<Token_id>, Types.Option<Idata>>(Types.OptionDecode<Token_id>(token_idDecode), Types.OptionDecode<Idata>(idataDecode))(arg);
            return new Set_expiry(v: (before_projection.Item1),
            p: (before_projection.Item2))
            ;
        }

        /**/




        public static Set_default_expiry set_default_expiryGenerator()
        {
            return (Set_default_expiry)token_idGenerator();
        }


        public static IMicheline set_default_expiryEncode(Set_default_expiry arg)
        {
            try
            {
                return token_idEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in set_default_expiryEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }


        public static Set_default_expiry set_default_expiryDecode(IMicheline arg) { return (Set_default_expiry)token_idDecode(arg); }

        /**/



        public static Constructor_sig _sigGenerator()
        {
            return (Constructor_sig)new Constructor_sig(Types.SignatureGenerator());
        }

        public static IMicheline _sigEncode(Constructor_sig arg)
        {
            try
            {
                return Types.SignatureEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in _sigEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Constructor_sig _sigDecode(IMicheline m) { return new Constructor_sig((Types.SignatureDecode(m))); }

        /**/

        public static Pk pkGenerator()
        {
            return (Pk)new Pk(Types.KeyGenerator());
        }

        public static IMicheline pkEncode(Pk arg)
        {
            try
            {
                return Types.KeyEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in pkEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Pk pkDecode(IMicheline m) { return new Pk((Types.KeyDecode(m))); }

        /**/

        public static Permit permitGenerator()
        {
            return new Permit(data: idataGenerator(),
                _sig: _sigGenerator(),
                pk: pkGenerator());
        }

        public static IMicheline permitEncode(Permit arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (pkEncode(arg.pk)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (_sigEncode(arg._sig)),
    (idataEncode(arg.data))
}
}
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in permitEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Permit permitDecode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Pk, Types.MyTuple<Constructor_sig, Idata>>(pkDecode, Types.Tuple2Decode<Constructor_sig, Idata>(_sigDecode, idataDecode))(arg);
            return new Permit(pk: (before_projection.Item1),
            _sig: (before_projection.Item2.Item1),
            data: (before_projection.Item2.Item2))
            ;
        }

        /**/




        public static Pause pauseGenerator()
        {
            return (Pause)unpauseGenerator();
        }


        public static IMicheline pauseEncode(Pause arg)
        {
            try
            {
                return unpauseEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in pauseEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }


        public static Pause pauseDecode(IMicheline arg) { return (Pause)unpauseDecode(arg); }

        /**/



        public static Type7 type7Generator()
        {
            return new Type7(partAccount: _operatorGenerator(),
                partValue: token_idGenerator());
        }

        public static IMicheline type7Encode(Type7 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.partAccount)),
    (token_idEncode(arg.partValue))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type7Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type7 type7Decode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Constructor_operator, Token_id>(_operatorDecode, token_idDecode)(arg);
            return new Type7(partAccount: (before_projection.Item1),
            partValue: (before_projection.Item2))
            ;
        }

        /**/

        public static Mint mintGenerator()
        {
            return new Mint(iroyalties: (Types.ListGenerator<Type7>(type7Generator))(),
                itokenMetadata: (Types.MapGenerator<Ikey, Idata>(ikeyGenerator, idataGenerator))(),
                iamount: token_idGenerator(),
                iowner: _operatorGenerator(),
                itokenid: token_idGenerator());
        }

        public static IMicheline mintEncode(Mint arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (token_idEncode(arg.itokenid)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (_operatorEncode(arg.iowner)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (token_idEncode(arg.iamount)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.MapEncode<Ikey,Idata>(ikeyEncode,idataEncode)(arg.itokenMetadata)),
    (Types.ListEncode<Type7>(type7Encode)(arg.iroyalties))
}
}
}
}
}
}
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in mintEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Mint mintDecode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Token_id, Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Types.MyTuple<Types.Map<Ikey, Idata>, List<Type7>>>>>(token_idDecode, Types.Tuple2Decode<Constructor_operator, Types.MyTuple<Token_id, Types.MyTuple<Types.Map<Ikey, Idata>, List<Type7>>>>(_operatorDecode, Types.Tuple2Decode<Token_id, Types.MyTuple<Types.Map<Ikey, Idata>, List<Type7>>>(token_idDecode, Types.Tuple2Decode<Types.Map<Ikey, Idata>, List<Type7>>(Types.MapDecode(ikeyDecode, idataDecode), Types.ListDecode(type7Decode)))))(arg);
            return new Mint(itokenid: (before_projection.Item1),
            iowner: (before_projection.Item2.Item1),
            iamount: (before_projection.Item2.Item2.Item1),
            itokenMetadata: (before_projection.Item2.Item2.Item2.Item1),
            iroyalties: (before_projection.Item2.Item2.Item2.Item2))
            ;
        }

        /**/




        public static Declare_ownership declare_ownershipGenerator()
        {
            return (Declare_ownership)_operatorGenerator();
        }


        public static IMicheline declare_ownershipEncode(Declare_ownership arg)
        {
            try
            {
                return _operatorEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in declare_ownershipEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }


        public static Declare_ownership declare_ownershipDecode(IMicheline arg) { return (Declare_ownership)_operatorDecode(arg); }

        /**/




        public static Claim_ownership claim_ownershipGenerator()
        {
            return (Claim_ownership)unpauseGenerator();
        }


        public static IMicheline claim_ownershipEncode(Claim_ownership arg)
        {
            try
            {
                return unpauseEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in claim_ownershipEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }


        public static Claim_ownership claim_ownershipDecode(IMicheline arg) { return (Claim_ownership)unpauseDecode(arg); }

        /**/



        public static Burn burnGenerator()
        {
            return new Burn(itokenid: token_idGenerator(),
                iamount: token_idGenerator());
        }

        public static IMicheline burnEncode(Burn arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (token_idEncode(arg.itokenid)),
    (token_idEncode(arg.iamount))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in burnEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Burn burnDecode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Token_id, Token_id>(token_idDecode, token_idDecode)(arg);
            return new Burn(itokenid: (before_projection.Item1),
            iamount: (before_projection.Item2))
            ;
        }

        /**/



        public static Type10 type10Generator()
        {
            return new Type10(owner: _operatorGenerator(),
                token_id: token_idGenerator());
        }

        public static IMicheline type10Encode(Type10 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.owner)),
    (token_idEncode(arg.token_id))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type10Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type10 type10Decode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Constructor_operator, Token_id>(_operatorDecode, token_idDecode)(arg);
            return new Type10(owner: (before_projection.Item1),
            token_id: (before_projection.Item2))
            ;
        }

        /**/

        public static Balance_of balance_ofGenerator()
        {
            return (Balance_of)(Types.Tuple2Generator<List<Type10>, Types.Contract>((Types.ListGenerator<Type10>(type10Generator)),
        Types.ContractGenerator))();
        }

        public static IMicheline balance_ofEncode(Balance_of arg)
        {
            try
            {
                return Types.Tuple2Encode<List<Type10>, Types.Contract>(Types.ListEncode<Type10>(type10Encode), Types.ContractEncode)(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in balance_ofEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Balance_of balance_ofDecode(IMicheline arg) { return new Balance_of(Types.Tuple2Decode<List<Type10>, Types.Contract>(Types.ListDecode(type10Decode), Types.ContractDecode)(arg)); }

        /**/



        public static Created_at created_atGenerator()
        {
            return (Created_at)new Created_at(Types.TimestampGenerator());
        }

        public static IMicheline created_atEncode(Created_at arg)
        {
            try
            {
                return Types.TimestampEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in created_atEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Created_at created_atDecode(IMicheline m) { return new Created_at((Types.TimestampDecode(m))); }

        /**/

        public static Type11 type11Generator()
        {
            return new Type11(expiry: (Types.OptionGenerator<Token_id>(token_idGenerator))(),
                created_at: created_atGenerator());
        }

        public static IMicheline type11Encode(Type11 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (Types.OptionEncode<Token_id>(token_idEncode)(arg.expiry)),
    (created_atEncode(arg.created_at))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type11Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type11 type11Decode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Types.Option<Token_id>, Created_at>(Types.OptionDecode<Token_id>(token_idDecode), created_atDecode)(arg);
            return new Type11(expiry: (before_projection.Item1),
            created_at: (before_projection.Item2))
            ;
        }

        /**/

        public static Type12 type12Generator()
        {
            return new Type12(user_permits: (Types.MapGenerator<Idata, Type11>(idataGenerator, type11Generator))(),
                user_expiry: (Types.OptionGenerator<Token_id>(token_idGenerator))(),
                counter: token_idGenerator());
        }

        public static IMicheline type12Encode(Type12 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (token_idEncode(arg.counter)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.OptionEncode<Token_id>(token_idEncode)(arg.user_expiry)),
    (Types.MapEncode<Idata,Type11>(idataEncode,type11Encode)(arg.user_permits))
}
}
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type12Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type12 type12Decode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Token_id, Types.MyTuple<Types.Option<Token_id>, Types.Map<Idata, Type11>>>(token_idDecode, Types.Tuple2Decode<Types.Option<Token_id>, Types.Map<Idata, Type11>>(Types.OptionDecode<Token_id>(token_idDecode), Types.MapDecode(idataDecode, type11Decode)))(arg);
            return new Type12(counter: (before_projection.Item1),
            user_expiry: (before_projection.Item2.Item1),
            user_permits: (before_projection.Item2.Item2))
            ;
        }

        /**/

        public static Type13 type13Generator()
        {
            return new Type13(token_id: token_idGenerator(),
                token_info: (Types.MapGenerator<Ikey, Idata>(ikeyGenerator, idataGenerator))());
        }

        public static IMicheline type13Encode(Type13 arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (token_idEncode(arg.token_id)),
    (Types.MapEncode<Ikey,Idata>(ikeyEncode,idataEncode)(arg.token_info))
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in type13Encode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Type13 type13Decode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Token_id, Types.Map<Ikey, Idata>>(token_idDecode, Types.MapDecode(ikeyDecode, idataDecode))(arg);
            return new Type13(token_id: (before_projection.Item1),
            token_info: (before_projection.Item2))
            ;
        }

        /**/

        public static Paused pausedGenerator()
        {
            return (Paused)new Paused(Types.BoolGenerator());
        }

        public static IMicheline pausedEncode(Paused arg)
        {
            try
            {
                return Types.BoolEncode(arg);
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in pausedEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Paused pausedDecode(IMicheline m) { return new Paused((bool)(Types.BoolDecode(m))); }

        /**/

        public static Constructor_storage _storageGenerator()
        {
            return new Constructor_storage(metadata: (Types.BigMapGenerator<Ikey, Idata>(ikeyGenerator, idataGenerator))(),
                default_expiry: token_idGenerator(),
                operator_for_all: (Types.BigMapGenerator<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>((Types.Tuple2Generator<Constructor_operator, Constructor_operator>(_operatorGenerator,
        _operatorGenerator)), unpauseGenerator))(),
                permits: (Types.BigMapGenerator<Constructor_operator, Type12>(_operatorGenerator, type12Generator))(),
                token_metadata: (Types.BigMapGenerator<Token_id, Type13>(token_idGenerator, type13Generator))(),
                _operator: (Types.BigMapGenerator<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>((Types.Tuple2Generator<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>(_operatorGenerator,
        (Types.Tuple2Generator<Token_id, Constructor_operator>(token_idGenerator,
        _operatorGenerator)))), unpauseGenerator))(),
                ledger: (Types.BigMapGenerator<Types.MyTuple<Token_id, Constructor_operator>, Token_id>((Types.Tuple2Generator<Token_id, Constructor_operator>(token_idGenerator,
        _operatorGenerator)), token_idGenerator))(),
                royalties: (Types.BigMapGenerator<Token_id, List<Type7>>(token_idGenerator, (Types.ListGenerator<Type7>(type7Generator))))(),
                paused: pausedGenerator(),
                owner_candidate: (Types.OptionGenerator<Constructor_operator>(_operatorGenerator))(),
                owner: _operatorGenerator());
        }

        public static IMicheline _storageEncode(Constructor_storage arg)
        {
            try
            {
                return new MichelinePrim
                {
                    Prim = PrimType.Pair,
                    Args = new List<IMicheline>{
    (_operatorEncode(arg.owner)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.OptionEncode<Constructor_operator>(_operatorEncode)(arg.owner_candidate)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (pausedEncode(arg.paused)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.BigMapEncode<Token_id,List<Type7>>(token_idEncode,Types.ListEncode<Type7>(type7Encode))(arg.royalties)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.BigMapEncode<Types.MyTuple<Token_id,Constructor_operator>,Token_id>(Types.Tuple2Encode<Token_id,Constructor_operator>(token_idEncode,_operatorEncode),token_idEncode)(arg.ledger)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.BigMapEncode<Types.MyTuple<Constructor_operator,Types.MyTuple<Token_id,Constructor_operator>>,Unpause>(Types.Tuple2Encode<Constructor_operator,Types.MyTuple<Token_id,Constructor_operator>>(_operatorEncode,Types.Tuple2Encode<Token_id,Constructor_operator>(token_idEncode,_operatorEncode)),unpauseEncode)(arg._operator)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.BigMapEncode<Token_id,Type13>(token_idEncode,type13Encode)(arg.token_metadata)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.BigMapEncode<Constructor_operator,Type12>(_operatorEncode,type12Encode)(arg.permits)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (Types.BigMapEncode<Types.MyTuple<Constructor_operator,Constructor_operator>,Unpause>(Types.Tuple2Encode<Constructor_operator,Constructor_operator>(_operatorEncode,_operatorEncode),unpauseEncode)(arg.operator_for_all)),
    new MichelinePrim {
    Prim = PrimType.Pair,
    Args= new List<IMicheline>{
    (token_idEncode(arg.default_expiry)),
    (Types.BigMapEncode<Ikey,Idata>(ikeyEncode,idataEncode)(arg.metadata))
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
                };
            }
            catch (Exception e)
            {
                throw new EncodeError($"Error in _storageEncode, with input:{arg}, and error: {e}");
                throw;
            }
        }

        public static Constructor_storage _storageDecode(IMicheline arg)
        {
            var before_projection = Types.Tuple2Decode<Constructor_operator, Types.MyTuple<Types.Option<Constructor_operator>, Types.MyTuple<Paused, Types.MyTuple<Types.BigMap<Token_id, List<Type7>>, Types.MyTuple<Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>, Types.MyTuple<Types.BigMap<Token_id, Type13>, Types.MyTuple<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>>>>>>>>(_operatorDecode, Types.Tuple2Decode<Types.Option<Constructor_operator>, Types.MyTuple<Paused, Types.MyTuple<Types.BigMap<Token_id, List<Type7>>, Types.MyTuple<Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>, Types.MyTuple<Types.BigMap<Token_id, Type13>, Types.MyTuple<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>>>>>>>(Types.OptionDecode<Constructor_operator>(_operatorDecode), Types.Tuple2Decode<Paused, Types.MyTuple<Types.BigMap<Token_id, List<Type7>>, Types.MyTuple<Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>, Types.MyTuple<Types.BigMap<Token_id, Type13>, Types.MyTuple<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>>>>>>(pausedDecode, Types.Tuple2Decode<Types.BigMap<Token_id, List<Type7>>, Types.MyTuple<Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>, Types.MyTuple<Types.BigMap<Token_id, Type13>, Types.MyTuple<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>>>>>(Types.BigMapDecode(token_idDecode, Types.ListDecode(type7Decode)), Types.Tuple2Decode<Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>, Types.MyTuple<Types.BigMap<Token_id, Type13>, Types.MyTuple<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>>>>(Types.BigMapDecode(Types.Tuple2Decode<Token_id, Constructor_operator>(token_idDecode, _operatorDecode), token_idDecode), Types.Tuple2Decode<Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>, Types.MyTuple<Types.BigMap<Token_id, Type13>, Types.MyTuple<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>>>(Types.BigMapDecode(Types.Tuple2Decode<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>(_operatorDecode, Types.Tuple2Decode<Token_id, Constructor_operator>(token_idDecode, _operatorDecode)), unpauseDecode), Types.Tuple2Decode<Types.BigMap<Token_id, Type13>, Types.MyTuple<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>>(Types.BigMapDecode(token_idDecode, type13Decode), Types.Tuple2Decode<Types.BigMap<Constructor_operator, Type12>, Types.MyTuple<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>>(Types.BigMapDecode(_operatorDecode, type12Decode), Types.Tuple2Decode<Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>, Types.MyTuple<Token_id, Types.BigMap<Ikey, Idata>>>(Types.BigMapDecode(Types.Tuple2Decode<Constructor_operator, Constructor_operator>(_operatorDecode, _operatorDecode), unpauseDecode), Types.Tuple2Decode<Token_id, Types.BigMap<Ikey, Idata>>(token_idDecode, Types.BigMapDecode(ikeyDecode, idataDecode)))))))))))(arg);
            return new Constructor_storage(owner: (before_projection.Item1),
            owner_candidate: (before_projection.Item2.Item1),
            paused: (before_projection.Item2.Item2.Item1),
            royalties: (before_projection.Item2.Item2.Item2.Item1),
            ledger: (before_projection.Item2.Item2.Item2.Item2.Item1),
            _operator: (before_projection.Item2.Item2.Item2.Item2.Item2.Item1),
            token_metadata: (before_projection.Item2.Item2.Item2.Item2.Item2.Item2.Item1),
            permits: (before_projection.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item1),
            operator_for_all: (before_projection.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item1),
            default_expiry: (before_projection.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item1),
            metadata: (before_projection.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item2.Item2))
            ;
        }

        /**/

        public static async Task<string?> Deploy(Netezos.Keys.Key from, Constructor_storage initial_storage, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var encoded_initial_storage = _storageEncode(initial_storage);
            var options = new System.Text.Json.JsonSerializerOptions();
            options.Converters.Add(new Netezos.Encoding.Serialization.MichelineConverter());
            string json1 = System.Text.Json.JsonSerializer.Serialize(encoded_initial_storage, options);
            if (debug)
            {
                Console.WriteLine(json1);
            }
            string? kt1 = await Blockchain.Functions.DeployContract("src/csharp_sdk/rarible_code.json", from, encoded_initial_storage, fee, gasLimit, storageLimit, amount, networkName, debug);
            return kt1;
        }




        public static async Task<string?> CallUpdate_operators_for_all(Netezos.Keys.Key from, string kt1, Update_operators_for_all param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = update_operators_for_allEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = update_operators_for_allEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "update_operators_for_all", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to update_operators_for_all: {opHash}");
            else { Console.WriteLine("Failed call to update_operators_for_all"); }
            return opHash;
        }











        public static async Task<string?> CallUpdate_operators(Netezos.Keys.Key from, string kt1, Update_operators param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = update_operatorsEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = update_operatorsEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "update_operators", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to update_operators: {opHash}");
            else { Console.WriteLine("Failed call to update_operators"); }
            return opHash;
        }



        public static async Task<string?> CallUnpause(Netezos.Keys.Key from, string kt1, Unpause param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = unpauseEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = unpauseEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "unpause", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to unpause: {opHash}");
            else { Console.WriteLine("Failed call to unpause"); }
            return opHash;
        }





        public static async Task<string?> CallTransfer_gasless(Netezos.Keys.Key from, string kt1, Transfer_gasless param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = transfer_gaslessEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = transfer_gaslessEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "transfer_gasless", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to transfer_gasless: {opHash}");
            else { Console.WriteLine("Failed call to transfer_gasless"); }
            return opHash;
        }



        public static async Task<string?> CallTransfer(Netezos.Keys.Key from, string kt1, Transfer param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = transferEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = transferEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "transfer", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to transfer: {opHash}");
            else { Console.WriteLine("Failed call to transfer"); }
            return opHash;
        }







        public static async Task<string?> CallSet_metadata(Netezos.Keys.Key from, string kt1, Set_metadata param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = set_metadataEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = set_metadataEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "set_metadata", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to set_metadata: {opHash}");
            else { Console.WriteLine("Failed call to set_metadata"); }
            return opHash;
        }



        public static async Task<string?> CallSet_expiry(Netezos.Keys.Key from, string kt1, Set_expiry param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = set_expiryEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = set_expiryEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "set_expiry", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to set_expiry: {opHash}");
            else { Console.WriteLine("Failed call to set_expiry"); }
            return opHash;
        }



        public static async Task<string?> CallSet_default_expiry(Netezos.Keys.Key from, string kt1, Set_default_expiry param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = set_default_expiryEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = set_default_expiryEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "set_default_expiry", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to set_default_expiry: {opHash}");
            else { Console.WriteLine("Failed call to set_default_expiry"); }
            return opHash;
        }







        public static async Task<string?> CallPermit(Netezos.Keys.Key from, string kt1, Permit param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = permitEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = permitEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "permit", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to permit: {opHash}");
            else { Console.WriteLine("Failed call to permit"); }
            return opHash;
        }



        public static async Task<string?> CallPause(Netezos.Keys.Key from, string kt1, Pause param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = pauseEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = pauseEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "pause", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to pause: {opHash}");
            else { Console.WriteLine("Failed call to pause"); }
            return opHash;
        }





        public static async Task<string?> CallMint(Netezos.Keys.Key from, string kt1, Mint param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = mintEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = mintEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "mint", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to mint: {opHash}");
            else { Console.WriteLine("Failed call to mint"); }
            return opHash;
        }



        public static async Task<string?> CallDeclare_ownership(Netezos.Keys.Key from, string kt1, Declare_ownership param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = declare_ownershipEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = declare_ownershipEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "declare_ownership", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to declare_ownership: {opHash}");
            else { Console.WriteLine("Failed call to declare_ownership"); }
            return opHash;
        }



        public static async Task<string?> CallClaim_ownership(Netezos.Keys.Key from, string kt1, Claim_ownership param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = claim_ownershipEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = claim_ownershipEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "claim_ownership", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to claim_ownership: {opHash}");
            else { Console.WriteLine("Failed call to claim_ownership"); }
            return opHash;
        }



        public static async Task<string?> CallBurn(Netezos.Keys.Key from, string kt1, Burn param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = burnEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = burnEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "burn", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to burn: {opHash}");
            else { Console.WriteLine("Failed call to burn"); }
            return opHash;
        }





        public static async Task<string?> CallBalance_of(Netezos.Keys.Key from, string kt1, Balance_of param, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug = false)
        {
            var input = balance_ofEncode(param).ToJson();
            var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));
            var encodedParam = balance_ofEncode(param);
            var opHash = await Blockchain.Functions.contractCall(from, kt1, "balance_of", encodedParam, fee, gasLimit, storageLimit, amount, networkName, debug);
            if (opHash != null) Console.WriteLine($"Successful call to balance_of: {opHash}");
            else { Console.WriteLine("Failed call to balance_of"); }
            return opHash;
        }













    }


    public static class Test
    {
        public async static Task test_storage_download(String kt1, String network)
        {
            var storage = await Blockchain.Functions.getStorage(kt1, Functions._storageDecode, network);
        }
    }

    public class initialBlockchainStorage
    {

        public static Constructor_storage initial_blockchain_storage()
        {
            return (Constructor_storage)(new(owner: new Constructor_operator("tz1PyW1EznU9ADpocaauSi41NCPynBuqf1Kc"),
            owner_candidate: (Types.Option<Constructor_operator>)new Types.None<Constructor_operator>(),
            paused: new Paused(false),
            royalties: (Types.BigMap<Token_id, List<Type7>>)(Types.BigMap<Token_id, List<Type7>>)new Types.AbstractBigMap<Token_id, List<Type7>>(System.Numerics.BigInteger.Parse("55541")),
            ledger: (Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>)(Types.BigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>)new Types.AbstractBigMap<Types.MyTuple<Token_id, Constructor_operator>, Token_id>(System.Numerics.BigInteger.Parse("55542")),
            _operator: (Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>)(Types.BigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>)new Types.AbstractBigMap<Types.MyTuple<Constructor_operator, Types.MyTuple<Token_id, Constructor_operator>>, Unpause>(System.Numerics.BigInteger.Parse("55543")),
            token_metadata: (Types.BigMap<Token_id, Type13>)(Types.BigMap<Token_id, Type13>)new Types.AbstractBigMap<Token_id, Type13>(System.Numerics.BigInteger.Parse("55544")),
            permits: (Types.BigMap<Constructor_operator, Type12>)(Types.BigMap<Constructor_operator, Type12>)new Types.AbstractBigMap<Constructor_operator, Type12>(System.Numerics.BigInteger.Parse("55545")),
            operator_for_all: (Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>)(Types.BigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>)new Types.AbstractBigMap<Types.MyTuple<Constructor_operator, Constructor_operator>, Unpause>(System.Numerics.BigInteger.Parse("55546")),
            default_expiry: new Token_id(System.Numerics.BigInteger.Parse("31556952")),
            metadata: (Types.BigMap<Ikey, Idata>)(Types.BigMap<Ikey, Idata>)new Types.AbstractBigMap<Ikey, Idata>(System.Numerics.BigInteger.Parse("55547"))));
        }
        /* {"prim":"Pair","args":[{"string":"tz1PyW1EznU9ADpocaauSi41NCPynBuqf1Kc"},{"prim":"None"},{"prim":"False"},{"int":"55541"},{"int":"55542"},{"int":"55543"},{"int":"55544"},{"int":"55545"},{"int":"55546"},{"int":"31556952"},{"int":"55547"}]}
        VRecord((Pair_type.PairP
           [(Pair_type.LeafP
               ({ Factori_utils.original = "%owner"; sanitized = "owner";
                  accessors = [] },
                VAddress tz1PyW1EznU9ADpocaauSi41NCPynBuqf1Kc));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%owner_candidate";
                   sanitized = "owner_candidate"; accessors = [] },
                 VOption ()));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%paused"; sanitized = "paused";
                   accessors = [] },
                 VBool false));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%royalties"; sanitized = "royalties";
                   accessors = [] },
                 VBigMap 55541));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%ledger"; sanitized = "ledger";
                   accessors = [] },
                 VBigMap 55542));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%operator"; sanitized = "_operator";
                   accessors = [] },
                 VBigMap 55543));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%token_metadata";
                   sanitized = "token_metadata"; accessors = [] },
                 VBigMap 55544));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%permits"; sanitized = "permits";
                   accessors = [] },
                 VBigMap 55545));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%operator_for_all";
                   sanitized = "operator_for_all"; accessors = [] },
                 VBigMap 55546));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%default_expiry";
                   sanitized = "default_expiry"; accessors = [] },
                 VInt 31556952));
             (Pair_type.LeafP
                ({ Factori_utils.original = "%metadata"; sanitized = "metadata";
                   accessors = [] },
                 VBigMap 55547))
             ]))*/


    }
}