(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains a function called `reentrancy_detection` used to
detect potential reentrancy problems in smart contracts on the Tezos blockchain.
It counts the number of times `TRANSFER_TOKENS` is used in the contract and
prints a warning message to the console if the count is greater than zero. An
example usage is provided. Warning: this short summary was automatically
generated and could be incomplete or misleading. If you feel this is the case,
please open an issue. *)

open Unstack_iterator
open Unstack_micheline

let node_fold folder env acc n =
  match n with
  | N_SIMPLE (`TRANSFER_TOKENS, [], [_; _; _], [_], _) -> acc + 1
  | _ -> default_folder.node_fold folder env acc n

let folder_reentrancy = { default_folder with node_fold }

let reentrancy_detection sc =
  let nb_transfer =
    folder_reentrancy.node_seq folder_reentrancy () 0 sc.node_l in
  match nb_transfer with
  | 0 -> ()
  | 1 ->
    Format.eprintf
      "There is a CALL_CONTRACT in this contract, check for reentrancy@."
  | n ->
    Format.eprintf
      "There are %d CALL_CONTRACT in this contract, check for reentrancy@." n
