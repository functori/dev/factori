# Factori &mdash; Automate dApps generation from Michelson smart contracts

## What is Factori?

**Factori** is a key Tezos dApps development framework. It 
is designed around the needs of Tezos smart contract & dApp 
developers based on our long experience in this domain. Factori's mantra is **low code**. Its 
primary goal is to **automate**, as much as possible, boilerplate 
code generation so that you can focus on writing, testing, and deploying your smart contracts. 

The figure below summarizes Factori's main features: given Michelson smart contracts
(whether you wrote them yourself or found them on a Tezos network), it automates the following tasks:
- SDKs generation in various languages, like OCaml, Typescript, Python, and C#;
- Basic Web dApp generation to graphically interact with the contracts;
- Plugins generation for [Crawlori](https://functori.com/docs/crawlori/) and [Dip dup](https://dipdup.io/) crawlers to easily index the calls to the smart
contracts into a relational database;
- A powerful scenario language to write tests;
- Factori will also provide a light static analysis module for Michelson contracts (e.g., detection of entrypoints
permissions and unused storage fields, use of sets/maps instead of big maps).

<p style="width:80%; margin:auto;">
  <img src="doc/src/factori.png" alt="Factori features" title="Factori features" />
</p>


Factori's long-term goal is to provide an equivalent of [Truffle](https://trufflesuite.com/docs/truffle/) and [Ganache](https://trufflesuite.com/ganache/) tools for Tezos' native smart contracts language, with an additional focus on meta-programming and automatic code generation.

## Installation and documentation

Please refer to our [online documentation](https://functori.com/docs/factori).

## Getting help and contributing

Factori is a relatively young tool, so please bear with us in case you went to some troubles while using it. So, don't hesitate to report them on our [issues tracker](https://gitlab.com/functori/dev/factori/-/issues) or to ask questions on the **#factori** channel of Tezos developers' slack.

## Copyright
Factori is available for free under the terms of the **MIT license**. Please, make sure to read and agree with the content of the [LICENSE](LICENSE) file before any use.

## Support

The development of Factori has been boosted thanks to a grant from the Tezos Foundation. We warmly thank them for their support. 

If you would like to support the development of Factori or receive commercial assistance, please don't hesitate to [contact us](mailto:contact@functori.com).
