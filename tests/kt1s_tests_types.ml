module Result = struct
  type t =
    | Failed of int [@wrap "failed"]
    | Succeeded
    | Passed
  [@@deriving encoding]

  let pp ppf = function
    | Failed code -> Format.fprintf ppf "failed with code %d" code
    | Succeeded -> Format.fprintf ppf "succeeded"
    | Passed -> Format.fprintf ppf "passed"

  let make exit_code =
    if exit_code <> 0 then
      Failed exit_code
    else
      Succeeded
end

module Time = struct
  type t = float [@@deriving encoding]

  let none = 0.
end

module Step = struct
  type t =
    | Import
    | Compilation
    | Test
  [@@deriving encoding]
end

module Test = struct
  type t =
    | OCaml
    | Typescript
    | Python
    | Csharp
    | Analyze
    | Lint
  [@@deriving encoding]

  let compare = compare
end

module TestMap = Map.Make (Test)

module Kt1Entry = struct
  type t = {
    hash : string;
    kt1 : string;
    occurence : int;
    network : string; [@dft "mainnet"]
  }
  [@@deriving encoding]

  let compare k1 k2 = String.compare k1.hash k2.hash

  let compare_by_occurrence k1 k2 = Int.compare k2.occurence k1.occurence
end

module Kt1EntryMap = Map.Make (Kt1Entry)

type step = {
  kind : Step.t;
  time : Time.t; [@dft Time.none]
  result : Result.t;
}
[@@deriving encoding]

type test_results = {
  final_result : Result.t;
  steps : step list;
}
[@@deriving encoding]

module Outputs = struct
  type t =
    ((test_results TestMap.t[@map Test.enc]) Kt1EntryMap.t[@map Kt1Entry.enc])
  [@@deriving encoding]

  let empty = Kt1EntryMap.empty

  let add = Kt1EntryMap.add

  let to_string = EzEncoding.construct enc
end

module Version = struct
  type t = {
    version : string;
    commit : string;
  }
  [@@deriving encoding]

  let current = Version.{ version; commit }

  let pp ppf { version; commit } = Format.fprintf ppf "%s (%s)" version commit
end

module Synopsis = struct
  type t = {
    version : Version.t;
    nb_runs : int;
    nb_processed : int;
    nb_succeeded_tests : int TestMap.t; [@map Test.enc]
  }
  [@@deriving encoding]

  let empty ~nb_runs =
    let version = Version.current in
    let nb_processed = 0 in
    let nb_succeeded_tests = TestMap.empty in
    { version; nb_runs; nb_processed; nb_succeeded_tests }

  let add tests synopsis =
    let nb_processed = synopsis.nb_processed + 1 in
    let nb_succeeded_tests =
      TestMap.merge
        (fun _ nb_succeeded -> function
          | Some { final_result = Succeeded; _ } ->
            let nb_succeeded = Option.value nb_succeeded ~default:0 in
            Some (nb_succeeded + 1)
          | _ -> nb_succeeded)
        synopsis.nb_succeeded_tests tests in
    { synopsis with nb_processed; nb_succeeded_tests }

  let to_string = EzEncoding.construct enc
end
