(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file contains functions for inferring Michelson expression types
and building a dependency graph, as well as encoding, decoding, and generating
Michelson types in OCaml. It also includes a function for calling an entrypoint
of a smart contract and a function for downloading and decoding the storage of a
contract. The file generates an interface and original blockchain storage and
provides a function called `process_entrypoints` to print out information about
the final storage type and interface. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Format
open Infer_entrypoints
open Pair_type
open Value
open Or_type
open Types
open Factori_utils
open Factori_errors
open Factori_network

let rec pp_value_to_ocaml ppf = function
  | VSaplingState s -> fprintf ppf "{|%s|}" s
  | VTicket s -> fprintf ppf "{|%s|}" s
  | VUnit _ -> fprintf ppf "()"
  | VKeyHash kh -> fprintf ppf "\"%s\"" kh
  | VKey k -> fprintf ppf "\"%s\"" k
  | VChain_id s -> fprintf ppf "\"%s\"" s
  | VSignature s -> fprintf ppf "\"%s\"" s
  | VOperation o -> fprintf ppf "\"%s\"" o
  | VOption (_, None) -> fprintf ppf "None"
  | VOption (_, Some x) -> fprintf ppf "Some (%a)" pp_value_to_ocaml x
  | VInt z -> print_z_int_ocaml ppf z
  | VTez z -> print_z_int_ocaml ppf z
  | VString s -> fprintf ppf "{|%s|}" s
  | VBytes s -> fprintf ppf "Crypto.H.mk {|%s|}" (s :> string)
  | VTimestamp s -> fprintf ppf "\"%s\"" s
  | VBool b -> fprintf ppf "%b" b
  | VAddress addr -> fprintf ppf "\"%s\"" addr
  | VSumtype (constructor, _, _, VUnit _) -> fprintf ppf "%s" constructor
  | VSumtype (constructor, _, _, v) ->
    fprintf ppf "%s (%a)" constructor pp_value_to_ocaml v
  | VMap (_, _, m) ->
    fprintf ppf "[%a]"
      (pp_print_list ~pp_sep:(tag ";") (fun ppf (x, y) ->
           fprintf ppf "%a,%a" pp_value_to_ocaml x pp_value_to_ocaml y))
      m
  | VSet (_, m) ->
    fprintf ppf "[%a]" (pp_print_list ~pp_sep:(tag ";") pp_value_to_ocaml) m
  | VList (_, m) ->
    fprintf ppf "[%a]" (pp_print_list ~pp_sep:(tag ";") pp_value_to_ocaml) m
  | VBigMap (_, _, i) -> fprintf ppf "Abstract (%a)" print_z_int_ocaml i
  | VTuple (_, vl) ->
    fprintf ppf "(%a)" (pp_print_list ~pp_sep:(tag ",") pp_value_to_ocaml) vl
  | VLambda l ->
    fprintf ppf
      "Lambda {from = Mint Z.zero; to_ = Mint Z.zero; body = \
       EzEncoding.destruct Tzfunc.Proto.micheline_enc.json {|%s|}}"
      (EzEncoding.construct Tzfunc.Proto.script_expr_enc.json l)
  | VRecord (LeafP (field_name, (_, v))) ->
    fprintf ppf "{%s = %a}" (show_sanitized_name field_name) pp_value_to_ocaml v
  | VRecord (PairP _ as p) ->
    fprintf ppf "{%a}"
      (pp_pairedtype_ocaml
         ~f_leaf:(fun name ppf (_, x) ->
           fprintf ppf "%s = %a" (show_sanitized_name name) pp_value_to_ocaml x)
         ~f_pair:(fun ppf _d -> fprintf ppf "")
         ~delimiters:Nothing ~sep:";\n")
      p

(* Get a tuple for pattern matching;
 *)
let rec pp_ep ?prefix ppf ep =
  let rec aux (ppf : formatter) = function
    | Base b ->
      fprintf ppf "%a"
        (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix)
        b
    | Pair al ->
      surround Parens
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf x -> aux ppf x))
        ppf al
    | Record (PairP _ as p) ->
      pp_pairedtype_ocaml ~delimiters:Parens ~sep:","
        ~f_leaf:(fun name ppf _ -> fprintf ppf "%s" (show_sanitized_name name))
        ~f_pair:(fun ppf _d -> fprintf ppf "")
        ppf p
    | Unary (List, _ep) -> fprintf ppf "l"
    | Or _ -> fprintf ppf "ovar"
    | Annot (ep, annot) ->
      if is_record_leaf ep then
        fprintf ppf "%s" (show_sanitized_name (bind_sanitize_annot annot))
      else
        fprintf ppf "%a" (pp_ep ?prefix) ep
    | _ -> eprintf "ignoring type %a%!\n" pp_type_ ep in
  let res = aux ppf ep in
  res

(* Write down the name of the type in a concise way *)
let rec output_type_pretty ppf = function
  | Base b ->
    fprintf ppf "%a"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _name) -> fprintf ppf "%a" output_type_pretty ep
  | Pair al ->
    fprintf ppf "(%a)"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf " *\n ")
         output_type_pretty)
      al
  | Record _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s" (show_sanitized_name (fst tn))
  | Unary (u, a) -> fprintf ppf "%a %s" output_type_pretty a (str_of_unary u)
  | Binary (Lambda, (_x, _y)) ->
    fprintf ppf "Factori_types.lambda"
    (* output_type_pretty x
       * output_type_pretty y *)
  | Contract _c -> fprintf ppf "contract" (* pp_micheline_ c *)
  | Binary (b, (k, v)) ->
    fprintf ppf "(%a,%a) %s" output_type_pretty k output_type_pretty v
      (str_of_binary b)

and pp_sumtype_ocaml f path ppf x =
  let rec aux ppf = function
    | true :: l ->
      fprintf ppf "(Mprim {prim = `Left;\nargs = [%a];annots=[]})\n" aux l
    | false :: l ->
      fprintf ppf " (Mprim {prim = `Right;\nargs = [%a];\nannots=[]})\n" aux l
    | [] -> fprintf ppf "%a" f x in
  aux ppf path

and constructor_from_path path =
  String.concat "_"
    (List.map
       (fun b ->
         if b then
           "Left"
         else
           "Right")
       path)

and output_sumtype_encode ppf t_ =
  output_verbose ~level:2
    (asprintf "[derive_constructor] Entering with %a" pp_type_ t_) ;
  let l = sumtype_to_list t_ in
  fprintf ppf "(match arg with%a)"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "")
       (fun ppf (t_, path) ->
         match t_ with
         | Annot (u_, name) when is_unit u_ ->
           fprintf ppf "\n| %s -> %a"
             (show_sanitized_name (bind_sanitize_capitalize name))
             (pp_sumtype_ocaml
                (fun ppf _x -> fprintf ppf "unit_encode ()")
                path)
             u_
         | Annot (t_, name) ->
           fprintf ppf "\n| %s x -> %a"
             (show_sanitized_name (bind_sanitize_capitalize name))
             (pp_sumtype_ocaml
                (fun ppf x -> fprintf ppf "%a x" output_type_encode x)
                path)
             t_
         | t_ ->
           fprintf ppf "\n| %s x -> %a x"
             (constructor_from_path path)
             output_type_encode t_))
    l

and output_sumtype_generator ppf t_ =
  let lst = sumtype_to_list t_ in
  fprintf ppf "\nchooseFrom\n[%a]\n"
    (pp_print_list ~pp_sep:(tag ";") (fun ppf (t_, path) ->
         fprintf ppf "(fun () -> %a)"
           (fun ppf at ->
             match at with
             | Annot (u_, name) when is_unit u_ ->
               fprintf ppf "%s"
                 (show_sanitized_name (bind_sanitize_capitalize name))
             | Annot (t_, name) ->
               fprintf ppf "%s (%a ())"
                 (show_sanitized_name (bind_sanitize_capitalize name))
                 output_type_generator t_
             | t ->
               fprintf ppf "%s (%a ())"
                 (constructor_from_path path)
                 output_type_generator t)
           t_))
    lst

and output_sumtype_micheline ppf or_t =
  pp_ortype_ocaml
    (fun ppf t_ ->
      match t_ with
      | Annot (u_, annot) when is_unit u_ ->
        fprintf ppf "unit_micheline_with_annot \"%s\""
          (show_sanitized_name annot)
      | t_ -> fprintf ppf "%a" output_type_micheline t_)
    ppf or_t

and output_sumtype_decode ppf t_ =
  let l = sumtype_to_list t_ in
  fprintf ppf
    "(function%a\n\
     | _ -> failwith \"unknown primitive in output_sumtype_decode\")"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "")
       (fun ppf (t_, path) ->
         match t_ with
         | Annot (u_, name) when is_unit u_ ->
           fprintf ppf "\n| %a -> %s"
             (pp_sumtype_ocaml
                (fun ppf _x ->
                  fprintf ppf "Mprim {prim = `Unit;args=[];annots=[]}")
                path)
             u_
             (show_sanitized_name (bind_sanitize_capitalize name))
         | Annot (t_, name) ->
           fprintf ppf "\n| %a when is_leaf x -> %s (%a x)"
             (pp_sumtype_ocaml (fun ppf _x -> fprintf ppf "x") path)
             t_
             (show_sanitized_name (bind_sanitize_capitalize name))
             output_type_decode t_
         | t_ ->
           fprintf ppf "\n| %a -> %s (%a x)"
             (pp_sumtype_ocaml (fun ppf _x -> fprintf ppf "x") path)
             t_
             (constructor_from_path path)
             output_type_decode t_))
    l

and output_type_micheline ppf = function
  | Base b ->
    fprintf ppf "%a_micheline"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, annot) ->
    fprintf ppf "(mich_annot ~annot:\"%s\" %a)"
      (show_sanitized_name annot)
      output_type_micheline ep
  | Pair al ->
    fprintf ppf "(tuple%d_micheline %a)" (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
         output_type_micheline)
      al
  | Record _ as t ->
    fprintf ppf "%s_micheline"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or (_ as t) -> output_sumtype_micheline ppf t
  | TVar tn -> fprintf ppf "%s_micheline" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s_micheline (%a))" (str_of_unary u) output_type_micheline a
  | Contract c ->
    fprintf ppf
      "(contract_micheline (EzEncoding.destruct micheline_enc.json {|%a|}))"
      pp_micheline_ c
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_micheline %a %a)" (str_of_binary b) output_type_micheline k
      output_type_micheline v

and output_type_generator ppf = function
  | Base b ->
    fprintf ppf "%a_generator"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" output_type_generator ep
  | Pair al ->
    fprintf ppf "(tuple%d_generator %a)" (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
         output_type_generator)
      al
  | Record _ as t ->
    fprintf ppf "%s_generator"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_sumtype_generator ppf t
  | TVar tn -> fprintf ppf "%s_generator" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s_generator %a)" (str_of_unary u) output_type_generator a
  | Contract _c -> fprintf ppf "contract_generator"
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_generator %a %a)" (str_of_binary b) output_type_generator k
      output_type_generator v

and output_type_encode ppf = function
  | Base b ->
    fprintf ppf "%a_encode"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" output_type_encode ep
  | Pair al ->
    fprintf ppf "(tuple%d_encode %a)" (List.length al)
      (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf "\n") output_type_encode)
      al
  | Record _ as t ->
    fprintf ppf "%s_encode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_sumtype_encode ppf t
  | TVar tn -> fprintf ppf "%s_encode" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s_encode %a)" (str_of_unary u) output_type_encode a
  | Contract c ->
    fprintf ppf
      "(contract_encode (EzEncoding.destruct micheline_enc.json {|%a|}))"
      pp_micheline_ c
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_encode %a %a)" (str_of_binary b) output_type_encode k
      output_type_encode v

and output_type_encode_pretty ppf = function
  | Base b ->
    fprintf ppf "%a_encode"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _name) -> fprintf ppf "%a" output_type_encode ep
  | Pair al ->
    fprintf ppf "(tuple%d_encode %a)" (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
         output_type_encode_pretty)
      al
  | Record _ as t ->
    fprintf ppf "%s_encode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s_encode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s_encode" (show_sanitized_name (fst tn))
  | Contract c ->
    fprintf ppf
      "(contract_encode (EzEncoding.destruct micheline_enc.json {|%a|}))"
      pp_micheline_ c
  | Unary (u, a) ->
    fprintf ppf "(%s_encode %a)" (str_of_unary u) output_type_encode a
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_encode %a %a)" (str_of_binary b) output_type_encode k
      output_type_encode v

and output_type_decode ppf = function
  | Base b ->
    fprintf ppf "%a_decode"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" output_type_decode ep
  | Pair al ->
    fprintf ppf "(tuple%d_decode %a)" (List.length al)
      (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf "\n") output_type_decode)
      al
  | Record _ as t ->
    fprintf ppf "%s_decode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_sumtype_decode ppf t
  | TVar tn -> fprintf ppf "%s_decode" (show_sanitized_name (fst tn))
  | Contract c ->
    fprintf ppf
      "(contract_decode (EzEncoding.destruct micheline_enc.json {|%a|}))"
      pp_micheline_ c
  | Unary (u, a) ->
    fprintf ppf "(%s_decode %a)" (str_of_unary u) output_type_decode a
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_decode %a %a)" (str_of_binary b) output_type_decode k
      output_type_decode v

and output_type_decode_pretty ppf = function
  | Base b ->
    fprintf ppf "%a_decode"
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (_, name) -> fprintf ppf "%s_decode" (show_sanitized_name name)
  | Pair al ->
    fprintf ppf "(tuple%d_decode %a)" (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
         output_type_decode_pretty)
      al
  | Record _ as t ->
    fprintf ppf "%s_decode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s_decode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s_decode" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s_decode %a)" (str_of_unary u) output_type_decode a
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s_decode %a %a)" (str_of_binary b) output_type_decode k
      output_type_decode v
  | Contract c ->
    fprintf ppf
      "(contract_decode (EzEncoding.destruct micheline_enc.json\"%a\"))"
      pp_micheline_ c

and output_sumtype_content ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "|")
       (fun ppf x ->
         let constructor_name, value =
           match x with
           | Annot (u_, name), _path when is_unit u_ ->
             (bind_sanitize_capitalize name, "")
           | Annot (t, name), _path ->
             ( bind_sanitize_capitalize name,
               Format.asprintf " of %a" output_type_pretty t )
           | t, path ->
             ( sanitized_of_str @@ constructor_from_path path,
               Format.asprintf " of %a" output_type_pretty t
               (* hopefully this does not happen too much or we will need another strategy *)
             ) in
         fprintf ppf "%s%s" (show_sanitized_name constructor_name) value))
    lst

(* Is this a duplicate of output_type_pretty? No *)
and output_type_content ppf = function
  | Base _ as t -> output_type_pretty ppf t
  | Annot (_, name) as t ->
    fprintf ppf "(* michelson name %s*)%a" (show_sanitized_name name)
      output_type_pretty t
  | Pair al ->
    fprintf ppf "(%a)"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf " *\n ")
         output_type_content)
      al
  | Record (LeafP (_n, x)) -> fprintf ppf "%a" output_type_pretty x
  | Record (PairP l) as m ->
    let d =
      if not @@ is_one_field_record m then
        Curly
      else
        Nothing in
    let l = flattenP [] l in
    fprintf ppf "%s%a%s" (opening d)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ";")
         (fun ppf (n, x) ->
           fprintf ppf "%s : %a" (show_sanitized_name n) output_type_pretty x))
      l (closing d)
  | Or _ as t -> output_sumtype_content ppf t
  | TVar tn -> fprintf ppf "%s" (show_sanitized_name (fst tn))
  | Unary (u, a) -> fprintf ppf "(%a %s)" output_type_content a (str_of_unary u)
  | Binary (Lambda, (_x, _y)) ->
    fprintf ppf "Factori_types.lambda"
    (* output_type_pretty x
       * output_type_pretty y *)
  | Binary (b, (k, v)) ->
    fprintf ppf "(%a,%a) %s" output_type_pretty k output_type_pretty v
      (str_of_binary b)
  | Contract _c -> fprintf ppf "contract"

let rec extract_tuple argname ppf t =
  match t with
  | Annot (_, name) ->
    fprintf ppf "%s.%s" argname (show_sanitized_name (bind_sanitize_annot name))
  | Pair al ->
    fprintf ppf "(%a)"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (extract_tuple argname))
      al
  | t ->
    eprintf "In extract_tuple (ocaml):\n\t\t\t\tXXXX\n%a%!\n" pp_type_ t ;
    raise (ElementShouldHaveName (show_type_ t))

and encode_record ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" output_type_encode t
    | PairP _ as p ->
      pp_pairedtype_ocaml_build
        (fun s ppf t ->
          fprintf ppf "(%a arg.%s)" output_type_encode_pretty t
            (show_sanitized_name s))
        ppf p in
  aux ep

and generator_record ppf ep =
  let aux = function
    | LeafP (_n, x) -> fprintf ppf "%a ()" output_type_generator x
    | PairP l ->
      let l = flattenP [] l in
      fprintf ppf "{%a}"
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ";")
           (fun ppf (n, x) ->
             fprintf ppf "%s = %a ()" (show_sanitized_name n)
               output_type_generator x))
        l in
  aux ep

and micheline_record ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" output_type_micheline t
    | PairP _ as p ->
      pp_pairedtype_ocaml_build ~mich_type:true
        (fun _s ppf t -> fprintf ppf "(%a)" output_type_micheline t)
        ppf p in
  aux ep

and decode_record ppf ep =
  let aux = function
    | LeafP (_, t) -> fprintf ppf "%a" output_type_decode t
    | PairP _ as p ->
      pp_pairedtype_ocaml
        ~f_leaf:(fun _name ppf x -> output_type_decode ppf x)
        ~f_pair:(fun ppf d -> fprintf ppf "tuple%d_decode " d)
        ~delimiters:Parens ~sep:" " ppf p in
  aux ep

and declare_sumtype ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "|")
       (fun ppf x ->
         let constructor_name, value =
           match x with
           | Annot (u_, name), _path when is_unit u_ ->
             (bind_sanitize_capitalize name, "")
           | Annot (t, name), _path ->
             ( bind_sanitize_capitalize name,
               Format.asprintf " of %a" output_type_pretty t )
           | t, path ->
             ( sanitized_of_str @@ constructor_from_path path,
               Format.asprintf " of %a" output_type_pretty t
               (* hopefully this does not happen too much or we will need another strategy *)
             ) in
         fprintf ppf "%s%s" (show_sanitized_name constructor_name) value))
    lst

and declare_type_ (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  match m with
  | Or _ ->
    let sumtype_content = Format.asprintf "%a" declare_sumtype m in
    fprintf ppf "\ntype %s = %s\n" san_name sumtype_content
  | Base b ->
    fprintf ppf "type %s = %a" san_name
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | TVar (vname, _) ->
    fprintf ppf "type %s = %s" san_name (show_sanitized_name vname)
  | Binary (Lambda, (_x, _y)) ->
    fprintf ppf "type %s = Factori_types.lambda" san_name
    (* output_type_pretty x output_type_pretty y *)
  | Binary (b, (k, v)) ->
    fprintf ppf "type %s = (%a,%a) %s" san_name output_type_pretty k
      output_type_pretty v (str_of_binary b)
  | Unary (u, t) ->
    fprintf ppf "type %s = %a %s" san_name output_type_pretty t (str_of_unary u)
  | Contract _t -> fprintf ppf "type %s = contract" san_name
  | Annot (ep, _) -> declare_type_ ep ppf name
  | Pair _ -> fprintf ppf "type %s = %a" san_name output_type_content m
  | Record _ ->
    (* Case of a record type *)
    let record_content =
      Format.asprintf "%a" (fun ppf type_ -> output_type_content ppf type_) m
    in
    fprintf ppf "\ntype %s = %s\n" (show_sanitized_name name) record_content

and micheline_type_decl (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  let prelude = asprintf "let %s_micheline = " san_name in
  match m with
  | Base b ->
    fprintf ppf "%s%a_micheline" prelude
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (micheline_type_decl ep) name
  | TVar tn ->
    fprintf ppf "\n%s%s_micheline" prelude (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s(%s_micheline (%a))" prelude (str_of_unary u)
      output_type_micheline a
  | Contract c ->
    fprintf ppf
      "%s(contract_micheline (EzEncoding.destruct micheline_enc.json {|%a|}))"
      prelude pp_micheline_ c
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(%s_micheline (%a) (%a))" prelude (str_of_binary b)
      output_type_micheline k output_type_micheline v
  | Or (_ as t) ->
    fprintf ppf "let %s_micheline = %a" san_name output_sumtype_micheline t
  | Pair _ as t ->
    fprintf ppf "let %s_micheline = %a" san_name output_type_micheline t
  | Record r -> fprintf ppf "let %s_micheline = %a" san_name micheline_record r

and generator_type_decl (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  let prelude = asprintf "let %s_generator () = " san_name in
  match m with
  | Base b ->
    fprintf ppf "%s%a_generator ()" prelude
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (generator_type_decl ep) name
  | TVar tn ->
    fprintf ppf "\n%s%s_generator ()" prelude (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s(%s_generator (%a)) ()" prelude (str_of_unary u)
      output_type_generator a
  | Contract _c ->
    fprintf ppf "%scontract_generator ()" prelude (* output_type_generator c *)
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(%s_generator (%a) (%a)) ()" prelude (str_of_binary b)
      output_type_generator k output_type_generator v
  | Or _ as t ->
    fprintf ppf "let %s_generator = %a" san_name output_sumtype_generator t
  | Pair _ as t ->
    fprintf ppf "let %s_generator = %a" san_name output_type_generator t
  | Record r ->
    fprintf ppf "let %s_generator () = %a" san_name generator_record r

and encode_type_decl (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  let prelude =
    asprintf "let %s_encode : %s -> micheline = " san_name san_name in
  let prelude_with_arg =
    asprintf "let %s_encode (arg : %s) : micheline = " san_name san_name in
  match m with
  | Base b ->
    fprintf ppf "%s%a_encode" prelude
      (pp_base_type ~language:Factori_config.OCaml ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (encode_type_decl ep) name
  | TVar tn ->
    fprintf ppf "\n%s%s_encode" prelude (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s(%s_encode (%a)) arg" prelude_with_arg (str_of_unary u)
      output_type_encode_pretty a
  | Contract c ->
    fprintf ppf
      "%s(contract_encode (EzEncoding.destruct micheline_enc.json {|%a|}) arg)"
      prelude_with_arg pp_micheline_ c
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(%s_encode (%a) (%a))" prelude (str_of_binary b)
      output_type_encode k output_type_encode v
  | Or _ as t ->
    fprintf ppf "let %s_encode arg = %a" san_name output_sumtype_encode t
  | Pair _ as t ->
    fprintf ppf "let %s_encode = %a" san_name output_type_encode t
  | Record r -> fprintf ppf "let %s_encode arg = %a" san_name encode_record r

and decode_type_decl ?prefix (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  let prelude = asprintf "let %s_decode = " san_name in
  match m with
  | Or _ as t -> fprintf ppf "%s%a" prelude output_type_decode t
  | Annot (ep, _) -> fprintf ppf "%a" (decode_type_decl ep) name
  | TVar tn ->
    fprintf ppf "\n%s%s_decode" prelude (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s(%s_decode (%a))" prelude (str_of_unary u) output_type_decode
      a
  | Contract _c ->
    fprintf ppf "%s(contract_decode ())" prelude (* output_type_decode c *)
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(%s_decode (%a) (%a))" prelude (str_of_binary b)
      output_type_decode k output_type_decode v
  | Base _ as t ->
    fprintf ppf "let %s_decode (m : micheline) : %s = %a m" san_name san_name
      output_type_decode_pretty t
  | Pair _ as t ->
    fprintf ppf "let %s_decode = %a" san_name output_type_decode t
  | Record r ->
    let type_content =
      Format.asprintf "%a" (fun ppf type_ -> output_type_content ppf type_) m
    in
    let record_decode =
      Format.asprintf "%a" (fun ppf type_ -> decode_record ppf type_) r in
    fprintf ppf "let %s_decode (m : micheline) : %s =\nlet %a = %s m in\n%s%!\n"
      san_name san_name (pp_ep ?prefix) m record_decode type_content

let deploy ~storage_name ~network ppf () =
  fprintf ppf
    "let deploy ?(amount=0L) ?(node=\"%s\") ?(name=\"No name provided\") \
     ?(from=Blockchain.bootstrap1) storage =\n\
    \               let storage = %s_encode storage in\n\
    \               Blockchain.deploy ~amount ~node ~name ~from ~code \
     (Micheline storage)\n\n\
     let deploy_bytes ?(amount=0L) ?(node=%S) ?(name=\"No name provided\") ~pk \
     storage =\n\
    \               let storage = %s_encode storage in\n\
    \               Blockchain.deploy_bytes ~amount ~node ~name ~pk ~code \
     (Micheline storage)"
    (get_raw_network network)
    (show_sanitized_name storage_name)
    (get_raw_network network)
    (show_sanitized_name storage_name)

let mk_deploy ~storage_name ~contract_name ppf () =
  fprintf ppf
    "let mk_deploy scenario ~from ~amount ~network ~storage =\n\
    \  let open Scenario_dsl.AstInterface in\n\
    \   mk_deploy ~scenario ~from ~amount ~network ~storage:(mk_micheline \
     scenario (%s_encode storage)) ~contract_name:\"%s\""
    (show_sanitized_name storage_name)
    contract_name

let call_entrypoint ~contract_name ppf name =
  let name =
    {
      name with
      sanitized = String.uncapitalize_ascii (show_sanitized_name name);
    } in
  let real_name =
    try Infer_entrypoints.Naming.get_real_name name with _ -> name in
  let name = show_sanitized_name name in
  let entrypoint_call ~prefix =
    fprintf ppf
      "let %scall_%s ?(node = Blockchain.default_node) ?(debug=false) \
       ?(amount=0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)?(storage_limit = \
       Z.minus_one) ~from ~kt1 (param : %s) =\n\
      \     let param =\n\
      \     {\n\
      \     entrypoint = EPnamed \"%s\";\n\
      \     value = Micheline (%s_encode param);\n\
      \     } in\n\
      \     Blockchain.%scall_entrypoint ~debug ~node ~amount ~fee ~gas_limit \
       ~storage_limit ~from ~dst:kt1 param@.@."
      prefix name name
      (show_sanitized_name real_name)
      name prefix in
  entrypoint_call ~prefix:"" ;
  entrypoint_call ~prefix:"forge_" ;
  fprintf ppf
    "let assert_failwith_str_%s ?(node = Blockchain.default_node) \
     ?(debug=false) ?(amount=0L) ~from ~kt1 ~expected ~prefix ~msg param =\n\
    \     Blockchain.assert_failwith_str ~expected ~prefix ~msg (forge_call_%s \
     ~debug ~node ~amount ~from ~kt1) param\n\n"
    name name ;
  fprintf ppf
    "let assert_failwith_str_list_%s ?(node = Blockchain.default_node) \
     ?(debug=false) ?(amount=0L) ~from ~kt1 ~expected ~prefix ~msg param =\n\
    \     Blockchain.assert_failwith_str_list ~expected ~prefix ~msg \
     (forge_call_%s ~debug ~node ~amount ~from ~kt1) param\n\n"
    name name ;
  fprintf ppf
    "let assert_failwith_generic_%s ?(node = Blockchain.default_node) \
     ?(debug=false) ?(amount=0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param =\n\
    \  Blockchain.assert_failwith_generic ~enc ~expected ~prefix ~msg \
     (forge_call_%s ~debug ~node ~amount ~from ~kt1) param\n\n"
    name name ;
  fprintf ppf
    "let mk_call_%s ~assert_success ~msg ~scenario ~from ~amount ~kt1 ~network \
     ~param =\n\
    \  let open Scenario_dsl.AstInterface in\n\
    \  mk_call ~assert_success ~msg ~scenario ~from ~amount ~entrypoint:\"%s\" \
     ~kt1 ~network ~contract_name:\"%s\" ~param:(mk_micheline scenario \
     (%s_encode param))\n\n"
    name name contract_name name

let download_and_decode_storage ~storage_name ppf () =
  fprintf ppf
    {|let storage_download ~kt1 ~node =
     let open Tzfunc.Rp in
     let open Blockchain in
     get_storage ~node ~debug:(!Factori_types.debug > 0) kt1 %s_decode

     let test_storage_download ~kt1 ~node () =
     let open Tzfunc.Rp in
     let open Blockchain in
     Lwt_main.run @@@@
     let>? storage = storage_download ~kt1 ~node in
     let storage_reencoded = %s_encode storage in
     Lwt.return_ok @@@@ Factori_types.output_debug @@@@ Format.asprintf "Done downloading storage: %%s."
     (Ezjsonm_interface.to_string
     (Json_encoding.construct
     micheline_enc.json
     storage_reencoded))|}
    (show_sanitized_name storage_name)
    (show_sanitized_name storage_name)

let print_original_blockchain_storage ~final_storage_type_ ~original_storage ppf
    () =
  let open Tzfunc.Proto in
  try
    let value =
      value_of_typed_micheline
        (* ~debug:(!Factori_options.verbosity > 0) *)
        original_storage final_storage_type_ in
    fprintf ppf "\nlet initial_blockchain_storage = %a\n(*\n %s\n*)\n"
      pp_value_to_ocaml value
      (EzEncoding.construct micheline_enc.json original_storage)
  with e ->
    handle_exception e ;
    fprintf ppf
      "\n\
       let initial_blockchain_storage = failwith \"Could not download initial \
       storage, this is a bug\"\n\
       (*%s*)\n"
      (EzEncoding.construct micheline_enc.json original_storage)

let rec pp_interface_instruction_mli ~storage_name ppf ii =
  match ii with
  | Big_map_access { bm = name_field; position = _; key; value; _ } ->
    let store_name = show_sanitized_name storage_name in
    fprintf ppf
      "val big_map_%s_find_opt : ?node:string ->\n\
       %a ->\n\
       %s -> \n\
       (%a option,Tzfunc__.Rp.error) result Lwt.t"
      (show_sanitized_name name_field)
      output_type_pretty key store_name output_type_pretty value
  | Define_type (name, type_) ->
    let san_name = String.uncapitalize_ascii (show_sanitized_name name) in
    fprintf ppf
      "(*Type definition for %s *)\n\
       %a\n\n\
       (** Encode elements of type %s into micheline *)\n\
       val %s_encode : %s -> micheline\n\n\
       (** Decode elements of type micheline as %s *)\n\
       val %s_decode : micheline -> %s\n\n\
       (** Generate random elements of type %s*)\n\
       val %s_generator : unit -> %s\n\n\
       (** The micheline type corresponding to type %s*)\n\
       val %s_micheline : micheline\n"
      san_name (declare_type_ type_) name san_name san_name san_name san_name
      san_name san_name san_name san_name san_name san_name san_name
  | Deploy ->
    fprintf ppf
      "\n\
       (** A function to deploy the smart contract.\n\
      \           - amount is the initial balance of the contract\n\
      \           - node allows to choose on which chain we are deploying\n\
      \           - name allows to choose a name for the contract you are \
       deploying\n\
      \           - from is the account which will originate the contract (and \
       pay for its origination)\n\
      \           The function returns a pair (kt1,op_hash) where kt1 is the \
       address of the contract\n\
      \           and op_hash is the hash of the origination operation\n\
      \       *)\n\
       val deploy :             ?amount:int64 ->\n\
      \                         ?node:string ->\n\
      \                         ?name:string ->\n\
      \                         ?from:Blockchain.identity ->\n\
      \                         %s -> (string * string, Tzfunc__.Rp.error) \
       result Lwt.t\n\
       val deploy_bytes :             ?amount:int64 ->\n\
      \                         ?node:string ->\n\
      \                         ?name:string ->\n\
      \                         pk:string ->\n\
      \                         %s -> (string, Tzfunc__.Rp.error) result Lwt.t\n"
      (show_sanitized_name storage_name)
      (show_sanitized_name storage_name) ;
    fprintf ppf
      "\n\
       val mk_deploy : Scenario_dsl.AstInterface.scenario ->\n\
       from:Scenario_value.identity Ast.astobj ->\n\
       amount:Z.t Ast.astobj ->\n\
       network:Scenario_value.network Ast.astobj ->\n\
       storage:%s -> string Ast.astobj\n"
      (show_sanitized_name storage_name)
  | Seq ii ->
    pp_print_list
      ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
      (pp_interface_instruction_mli ~storage_name)
      ppf ii
  | Entrypoint (name, _) ->
    let name = String.uncapitalize_ascii (show_sanitized_name name) in
    let entrypoint_signature ~prefix ~return =
      fprintf ppf
        "\n\
         (** %scall entrypoint %s of the smart contract. *)\n\
         val %scall_%s :   ?node:string -> ?debug:bool -> ?amount:int64 -> \
         ?fee:int64 -> ?gas_limit:Z.t -> ?storage_limit:Z.t -> \
         from:Blockchain.identity ->\n\
        \                kt1:Tzfunc.Proto.A.contract ->\n\
        \                %s -> (%s, Tzfunc__.Rp.error) result Lwt.t@.@." prefix
        name prefix name name return in
    entrypoint_signature ~prefix:"" ~return:"string" ;
    entrypoint_signature ~prefix:"forge_"
      ~return:
        "Crypto.Raw.t * string * string *\n\
        \             Proto.script_expr Proto.manager_operation list" ;
    fprintf ppf
      "val assert_failwith_str_%s : ?node:string -> ?debug:bool -> \
       ?amount:int64 -> from:Blockchain.identity ->\n\
      \                kt1:Tzfunc.Proto.A.contract ->\n\
      \                expected:string ->\n\
      \                prefix:string ->\n\
      \                msg:string ->\n\
      \                %s -> (unit, Tzfunc__.Rp.error) result Lwt.t@.@.\n\
      \                 " name name ;
    fprintf ppf
      "(** Same as above, but the error may belong to a list of options *)\n\
       val assert_failwith_str_list_%s : ?node:string -> ?debug:bool -> \
       ?amount:int64 -> from:Blockchain.identity ->\n\
      \                kt1:Tzfunc.Proto.A.contract ->\n\
      \                expected:(string list) ->\n\
      \                prefix:string ->\n\
      \                msg:string ->\n\
      \                %s -> (unit, Tzfunc__.Rp.error) result Lwt.t@.@.\n\
      \                 " name name ;
    fprintf ppf
      "val assert_failwith_generic_%s : ?node:string -> ?debug:bool -> \
       ?amount:int64 -> enc:('a Json_encoding.encoding) -> \
       from:Blockchain.identity ->\n\
      \                kt1:Tzfunc.Proto.A.contract ->\n\
      \                expected:'a ->\n\
      \                prefix:string ->\n\
      \                msg:string ->\n\
      \                %s -> (unit, Tzfunc__.Rp.error) result Lwt.t@.@.\n\
      \                " name name ;
    fprintf ppf
      "val mk_call_%s :\n\
      \       assert_success:bool ->\n\
      \       msg:string ->\n\
      \       scenario:Scenario_dsl.AstInterface.scenario ->\n\
      \       from:'a Ast.astobj ->\n\
      \       amount:'b Ast.astobj ->\n\
      \       kt1:'c Ast.astobj ->\n\
      \       network:'d Ast.astobj ->\n\
      \       param:%s -> string Ast.astobj" name name

let pp_interface__mli ?(has_original_blockchain_storage = false) ~storage_name
    ppf i =
  fprintf ppf
    "open Tzfunc.Proto\n\
     open Factori_types\n\
     %a\n\
     (** Downloads and decodes the storage, and then reencodes it.\n\
     Allows to check the robustness of the encoding and decoding functions. *)\n\
     val storage_download :\n\
     kt1:Proto.A.contract -> node:string -> (%s, Tzfunc__.Rp.error) result \
     Lwt.t\n\n\
     val test_storage_download :\n\
     kt1:Proto.A.contract -> node:string -> unit -> (unit, Tzfunc__.Rp.error) \
     result%s"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (pp_interface_instruction_mli ~storage_name))
    i
    (show_sanitized_name storage_name)
    (if has_original_blockchain_storage then
       sprintf
         "\n\n\
          (** Initial storage as seen on the blockchain at the time of import. \
          One possible use of this value is to copy it and modify it easily. *)\n\
          val initial_blockchain_storage : %s\n\
         \        "
         (show_sanitized_name storage_name)
     else
       "")

let pp_big_map_access ~encode ~decode ~typ ~storage ~name_big_map ~network
    ~last_name ppf f elt =
  fprintf ppf
    {|let big_map_%s_find_opt ?(node = "%s") key (%s : %s) =
     %a
     Blockchain.big_map_find_opt ~node %s key %s %s %s|}
    (show_sanitized_name name_big_map)
    (get_raw_network network)
    (show_sanitized_name storage)
    (show_sanitized_name storage)
    f elt
    (show_sanitized_name last_name)
    (show_sanitized_name typ)
    (show_sanitized_name encode)
    (show_sanitized_name decode)

let pp_pattern_position position previous_name =
  match position with
  | In_pair (i, Pair t_l) ->
    let wild_card = sanitized_of_str "_" in
    let string_for_pos pos _ty =
      if pos = i then
        sanitized_of_str
          (Format.sprintf "%s_%d" (show_sanitized_name previous_name) i)
      else
        wild_card in
    let l = List.mapi string_for_pos t_l in
    let new_name =
      Option.get
      @@ List.find_map
           (fun s ->
             if eq_sanitized wild_card s then
               None
             else
               Some s)
           l in
    ( (fun ppf ->
        Format.fprintf ppf "let (%a) = %s in@.%a"
          (pp_print_list
             ~pp_sep:(fun ppf () -> Format.fprintf ppf ",")
             (fun ppf s -> Format.fprintf ppf "%s" (show_sanitized_name s)))
          l
          (show_sanitized_name previous_name)),
      new_name )
  | In_record ((name_field, _type_), Record _) ->
    let new_name =
      sanitized_of_str
        (Format.sprintf "%s_%s"
           (show_sanitized_name previous_name)
           (show_sanitized_name name_field)) in
    ( (fun ppf ->
        Format.fprintf ppf "let %s = %s.%s in@.%a"
          (show_sanitized_name new_name)
          (show_sanitized_name previous_name)
          (show_sanitized_name name_field)),
      new_name )
  | _ ->
    raise (Factori_errors.GenericError ("Output_OCaml", "Incoherent types"))

let big_map_access ~name_field pos ~key ~value ~network ppf =
  let typ = sanitized_of_str "key_micheline" in
  let key_micheline ppf () =
    Format.fprintf ppf "let %s = %a" (show_sanitized_name typ)
      output_type_micheline key in
  let encode = sanitized_of_str "encode_key" in
  let encoding_key ppf () =
    Format.fprintf ppf "let %s = %a"
      (show_sanitized_name encode)
      output_type_encode key in
  let decode = sanitized_of_str "decode_value" in
  let decoding_value ppf () =
    Format.fprintf ppf "let %s = %a"
      (show_sanitized_name decode)
      output_type_decode value in
  let store_name = sanitized_of_str "storage" in
  let print_suite, big_map_name =
    List.fold_left
      (fun (f_acc, previous_name) pos ->
        (* La j'ai la fonction de print t qui prend ppf suite et element ainsi que le prochain *)
        let print_actual, new_one = pp_pattern_position pos previous_name in
        ( (fun ppf next_f actual_elt ->
            f_acc ppf
              (fun ppf next_element -> print_actual ppf next_f next_element)
              actual_elt),
          new_one ))
      ((fun ppf f r -> Format.fprintf ppf "%a" f r), store_name)
      pos in
  let f =
    pp_big_map_access ~encode ~decode ~typ ~storage:store_name
      ~name_big_map:name_field ~network ~last_name:big_map_name in
  let prelude ppf () =
    Format.fprintf ppf "%a in@.%a in@.%a in" key_micheline () encoding_key ()
      decoding_value () in
  f ppf (fun ppf () -> print_suite ppf prelude ()) ()

let rec pp_interface_instruction_ocaml ~contract_name ~storage_name ~network ppf
    ii =
  match ii with
  | Big_map_access { bm = name_field; position = pos; key; value; _ } ->
    big_map_access ~name_field pos ~key ~value ~network ppf
  | Define_type (name, type_) ->
    fprintf ppf "%a\n%a\n%a\n%a\n%a" (declare_type_ type_) name
      (encode_type_decl type_) name (decode_type_decl type_) name
      (micheline_type_decl type_)
      name
      (generator_type_decl type_)
      name
  | Deploy ->
    deploy ~storage_name ~network ppf () ;
    mk_deploy ~contract_name ~storage_name ppf ()
  | Seq ii -> (pp_interface_ocaml ~contract_name ~storage_name ~network) ppf ii
  | Entrypoint (name, _) -> call_entrypoint ~contract_name ppf name

and pp_interface_ocaml ~network ~contract_name ~storage_name (ppf : formatter)
    (i : interface) =
  output_verbose ~level:3
    (asprintf "Entering pp_interface_ocaml with interface %a" pp_interface i) ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_ocaml ~contract_name ~storage_name ~network)
    ppf i

let regroup ~contract_to_include ~final_storage_type_ ?(original_storage = None)
    ppf () =
  let original_blockchain_storage =
    match original_storage with
    | None -> ""
    | Some original_storage ->
      Format.asprintf "\n%a"
        (print_original_blockchain_storage ~final_storage_type_
           ~original_storage)
        () in
  fprintf ppf
    "open Factori_types\nopen Tzfunc.Proto\ninclude %s_ocaml_interface\n%s"
    (String.capitalize_ascii contract_to_include)
    original_blockchain_storage

let process_entrypoints ~contract_name ~storage_name ~final_storage_type_
    ~network ?(original_storage = None) ppf interface =
  let contract_name = show_sanitized_name contract_name in
  output_verbose ~level:2
    (asprintf
       "[ocaml] Entering process_entrypoints with final storage: %a\n\
       \ and interface %a" pp_type_ final_storage_type_ pp_interface interface) ;
  let interface =
    try_overflow ~msg:"extract interface ocaml" (fun () -> interface) in
  let _ = check_interface_for_doubles interface in
  let original_blockchain_storage =
    match original_storage with
    | None -> ""
    | Some original_storage ->
      Format.asprintf "\n%a"
        (print_original_blockchain_storage ~final_storage_type_
           ~original_storage)
        () in
  fprintf ppf "open Factori_types\nopen Tzfunc.Proto\n%a\n\n%a%s"
    (pp_interface_ocaml ~contract_name ~storage_name ~network)
    interface
    (download_and_decode_storage ~storage_name)
    () original_blockchain_storage
