using Netezos.Encoding;
using Netezos.Forging;
using Netezos.Forging.Models;
using Netezos.Keys;
using Netezos.Rpc;
using System.Numerics;

namespace Blockchain
{
    public static class ContractHash
    {
        public static string ComputeOriginatedContractHash(string opgHash, int index)
        {
            var decodedOpgHash = Netezos.Encoding.Base58.Parse(opgHash);
            var opgHashBytes = decodedOpgHash.Skip(2).ToArray();
            var indexBytes = BitConverter.GetBytes(index).Reverse().ToArray();
            var concatBytes = opgHashBytes.Concat(indexBytes).ToArray();
            var blake2bHash = Netezos.Utils.Blake2b.GetDigest(concatBytes, 20 * 8);
            var prefix = new byte[] { 2, 90, 121 };
            var resultBytes = prefix.Concat(blake2bHash).ToArray();
            return Base58.Convert(resultBytes);
        }
    }

    public class Identities
    {
        public static Key aliceFlextesa = Key.FromBase58("edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq");
        public static Key bobFlextesa = Key.FromBase58("edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt");
    }

    public static class NodeUrls
    {
        public static readonly string sandbox_node = "http://127.0.0.1:18731";

        public static readonly string flextesa_node = "http://localhost:20000";

        public static readonly string ithaca_node = "https://ithacanet.tezos.marigold.dev";

        public static readonly string ithacanet_node = ithaca_node;

        public static readonly string ghostnet_node = "https://ghostnet.tezos.marigold.dev";

        public static readonly string jakarta_node = "https://jakartanet.ecadinfra.com";

        public static readonly string lima_node = "https://limanet.ecadinfra.com";

        public static readonly string jakartanet_node = jakarta_node;

        public static readonly string mainnet_node = "https://tz.functori.com";

        public static readonly string default_node = flextesa_node;
    }

    public static class Functions
    {

        public static string GetNode(string network)
        {
            switch (network)
            {
                case "sandbox":
                    return NodeUrls.sandbox_node;
                case "flextesa":
                    return NodeUrls.flextesa_node;
                case "ithaca":
                    return NodeUrls.ithaca_node;
                case "ghostnet":
                    return NodeUrls.ghostnet_node;
                case "jakarta":
                    return NodeUrls.jakarta_node;
                case "lima":
                    return NodeUrls.lima_node;
                case "mainnet":
                    return NodeUrls.mainnet_node;
                default:
                    return NodeUrls.default_node;
            }
        }

        public static string get_default_identity(string network)
        {
            switch (network)
            {
                case "ithacanet":
                case "ghostnet":
                case "flextesa":
                    return "alice_flextesa";
                case "sandbox":
                    return "bootstrap1";
                default:
                    throw new System.Exception(string.Format("No default identity for {0}", network));
            }
        }

        public static async Task<long> getBalance(string address, string network = "flextesa")
        {
            var rpc = new TezosRpc(network);
            var balance = await rpc.Blocks.Head.Context.Contracts[address].Balance.GetAsync<long>();
            return balance;
        }

        public static async Task<StorageType> getStorage<StorageType>(string address, Func<IMicheline, StorageType> decodeStorage, string network = "flextesa")
        {
            var rpc = new TezosRpc(GetNode(network));
            IMicheline storage = await rpc.Blocks.Head.Context.Contracts[address].Storage.GetAsync();
            var decoded_storage = decodeStorage(storage);
            return decoded_storage;
        }

        class FeesCalculator
        {
            private static readonly BigInteger minimalFees = new BigInteger(100);
            private static readonly BigInteger nanotzPerGasUnit = new BigInteger(100);
            private static readonly BigInteger nanotzPerByte = new BigInteger(1000);
            private static BigInteger ToNanotz(BigInteger m) => m * 1000;
            private static BigInteger OfNanotz(BigInteger n) => (n + 999) / 1000;
            public static BigInteger ComputeFees(BigInteger gasLimit, int size)
            {
                BigInteger minimalFeesInNanotz = ToNanotz(minimalFees);
                BigInteger feesForGasInNanotz = nanotzPerGasUnit * gasLimit;
                BigInteger feesForSizeInNanotz = nanotzPerByte * size;
                BigInteger feesInNanotz = minimalFeesInNanotz + feesForGasInNanotz + feesForSizeInNanotz;
                return OfNanotz(feesInNanotz);
            }
        }

        public static async Task makeTransfer(Key from, string to, long fee, long gasLimit, long storageLimit, long amount = 0, string network = "flextesa", bool debug = false)
        {
            var rpc = new TezosRpc(GetNode(network));
            var mycounter = await rpc.Blocks.Head.Context.Contracts[from.PubKey.Address].Counter.GetAsync<int>();
            var branch = await rpc.Blocks.Head.Hash.GetAsync<string>();
            if (branch == null)
            {
                throw new Exception($"makeTransfer failed, branch is null");
            }
            var tx = new TransactionContent
            {
                Source = from.PubKey.Address,
                Counter = ++mycounter,
                Amount = amount, // 1 tez
                Destination = to,
                GasLimit = (int)gasLimit,
                Fee = fee // 0.001 tez
            };
            var operation = await rpc.Blocks.Head.Helpers.Scripts.RunOperation.PostAsync(tx);
            string status = operation.contents[0].metadata.operation_result.status;
            if (status == "failed")
            {
                Console.WriteLine($"Transaction simulation failed: {operation}");
                return;
            }
            if (debug)
            {
                Console.WriteLine($"Operation is {operation}");
            }
            var content = new ManagerOperationContent[] { tx };
            var bytes = await new LocalForge().ForgeOperationGroupAsync(branch, content);
            byte[] signature = from.SignOperation(bytes);
            var result = await rpc.Inject.Operation.PostAsync(bytes.Concat(signature));
            var opSig = from.SignOperation(bytes);

            var opHash = await rpc.Inject.Operation.PostAsync(bytes.Concat((byte[])opSig));

        }

        public static async Task<string?> contractCall(Key from, string to, string entrypoint, IMicheline param, long fee, long gasLimit, long storageLimit, long amount = 0, string network = "flextesa", bool debug = false)
        {
            var rpc = new TezosRpc(GetNode(network));
            var mycounter = await rpc.Blocks.Head.Context.Contracts[from.PubKey.Address].Counter.GetAsync<int>();
            var head = await rpc.Blocks.Head.Hash.GetAsync<string>();
            if (head == null)
            {
                throw new Exception($"Fetching head failed");
            }
            var tx = new Netezos.Forging.Models.TransactionContent
            {
                Source = from.PubKey.Address,
                Counter = ++mycounter,
                GasLimit = (int)gasLimit,
                StorageLimit = (int)storageLimit,
                Fee = (int)fee,
                Destination = to,
                Parameters = new Parameters
                {
                    Entrypoint = entrypoint,
                    Value = param
                }
            };
            var operation = await rpc.Blocks.Head.Helpers.Scripts.RunOperation.PostAsync(tx);
            string status = operation.contents[0].metadata.operation_result.status;
            if (status == "failed")
            {
                Console.WriteLine($"Transaction simulation failed: {operation}");
                return null;
            }
            if (debug)
            {
                Console.WriteLine($"Operation is {operation}");
            }

            var content = new ManagerOperationContent[] { tx };
            var bytes = await new LocalForge().ForgeOperationGroupAsync(head, content);
            byte[] signature = from.SignOperation(bytes);
            var result = await rpc.Inject.Operation.PostAsync(bytes.Concat(signature));
            var opSig = from.SignOperation(bytes);

            var opHash = await rpc.Inject.Operation.PostAsync(bytes.Concat((byte[])opSig));
            return opHash;
        }

        public static async Task<string?> DeployContract(string codeFileName, Key from, IMicheline initial_storage, long fee, long gasLimit, long storageLimit, long amount = 0, string network = "flextesa", bool debug = false)
        {
            string jsonString = File.ReadAllText(codeFileName);
            var node = GetNode(network);
            if (debug) { Console.WriteLine($"Node: {node}"); }
            if (debug) Console.WriteLine($"Entering DeployContract, getting rpc for node {node}");
            var rpc = new TezosRpc(node);
            if (debug) { Console.WriteLine($"Got Rpc: {rpc.ToString()}"); }
            if (debug) { Console.WriteLine($"Address: {from.PubKey.Address}"); }
            var query = rpc.Blocks.Head.Context.Contracts[from.PubKey.Address].Counter;
            if (debug) { Console.WriteLine(query); }
            int mycounter;
            try
            {
                if (debug) { Console.WriteLine($"Getting counter"); }
                mycounter = await rpc.Blocks.Head.Context.Contracts[from.PubKey.Address].Counter.GetAsync<int>();
                if (debug) { Console.WriteLine($"Got Counter: {mycounter}"); }
                //Console.WriteLine(jsonString);
                var code = Micheline.FromJson(jsonString) as MichelineArray ?? throw new Exception("Error: Code is not an array");
                OriginationContent? tx = null;
                tx = new OriginationContent
                {
                    Source = from.PubKey.Address,
                    Counter = ++mycounter,
                    GasLimit = (int)gasLimit,
                    StorageLimit = (int)storageLimit,
                    Fee = (int)fee,
                    Script = new()
                    {
                        Code = code,
                        Storage = initial_storage
                    }
                };
                var operation = await rpc.Blocks.Head.Helpers.Scripts.RunOperation.PostAsync(tx);
                string status = operation.contents[0].metadata.operation_result.status;
                if (status == "failed")
                {
                    throw new Exception($"Transaction simulation failed: {operation}");
                }
                if (debug)
                {
                    Console.WriteLine($"Operation is {operation}");
                }
                var content = new ManagerOperationContent[] { tx };
                if (debug) { Console.WriteLine($"Getting head"); }
                var head = await rpc.Blocks.Head.Hash.GetAsync<string>();
                if (head == null)
                {
                    throw new Exception($"Fetching head failed");
                }
                if (debug) { Console.WriteLine($"[DeployContract] Got: {head}"); }
                var bytes = await new LocalForge().ForgeOperationGroupAsync(head, content);
                byte[] signature = from.SignOperation(bytes);
                if (debug)
                {
                    foreach (byte b in bytes)
                    {
                        Console.Write(b);
                    }
                    Console.WriteLine("");
                }
                var result = await rpc.Inject.Operation.PostAsync(bytes.Concat(signature));
                Console.WriteLine($"Result: {result}");
                var kt1 = ContractHash.ComputeOriginatedContractHash(result, 0);
                Console.WriteLine($"KT1 : {kt1}");
                if (debug) { Console.WriteLine($"Got result: {result}"); }
                return kt1;
            }
            catch (System.Exception e)
            {
                Console.WriteLine($"[DeployContract]Error for {codeFileName}: {e}");
                return null;
            }

        }

    }

}
