(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines a module with printer functions for different types
and nodes used in the Tezos Michelson language. The module includes functions to
print elements, lists, Micheline expressions, annotations, and nodes. Warning:
this short summary was automatically generated and could be incomplete or
misleading. If you feel this is the case, please open an issue. *)

open Unstack_micheline
open Factori_micheline_lib

module type A = sig
  type t

  val to_string : t -> string
end

module Make (A : A) = struct
  let print_elt ppf elt = Format.fprintf ppf "%s" (A.to_string elt)

  let list_of_arg =
    Format.pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
      print_elt

  let list_of_micheline =
    Format.pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
      (fun ppf elt ->
        Format.fprintf ppf "%s"
          (Micheline_lib.string_of_micheline ~simple:false elt))

  let list_of_res ppf = function
    | [] -> Format.fprintf ppf "()"
    | l ->
      Format.pp_print_list
        ~pp_sep:(fun ppf () -> Format.fprintf ppf ",")
        print_elt ppf l

  let print_annot ppf = function
    | None -> Format.fprintf ppf ""
    | Some s -> Format.fprintf ppf "%%%s" s

  let rec pp_print_control ppf control_node =
    let sub_fun ppf control_node =
      match control_node with
      | N_IF (arg, _, label, node_then, node_else) ->
        Format.fprintf ppf "IF %s [%d] THEN@,%a@,%a" (A.to_string arg) label
          pp_print_node_l node_then pp_print_node_l node_else
      | N_IF_NONE (arg, _, label, node_none, node_some, pat_some, lab_pat) ->
        Format.fprintf ppf
          "IF_NONE (%s) [%d]@,\
           |NONE -> @[<v 0>%a@]@,\
           |SOME %s [%d] -> @[<v 0>%a@]" (A.to_string arg) label pp_print_node_l
          node_none (A.to_string pat_some) lab_pat pp_print_node_l node_some
      | N_IF_CONS
          (arg, _, label, node_cons, node_nil, pat_hd, lab_x, pat_tl, lab_xs) ->
        Format.fprintf ppf
          "IF_CONS %s [%d]@,\
           |Cons(%s,%s) [%d,%d] -> @[<v 0>%a@]@,\
           |Nil -> @[<v 0>%a@]" (A.to_string arg) label (A.to_string pat_hd)
          (A.to_string pat_tl) lab_x lab_xs pp_print_node_l node_cons
          pp_print_node_l node_nil
      | N_IF_LEFT (arg, _, label, (pat_l, lab_l, node_l), (pat_r, lab_r, node_r))
        ->
        Format.fprintf ppf
          "IF_LEFT (%s) [%d]@,\
           |Left %s [%d] -> @[<v 0>%a@]@,\
           |Right %s [%d] -> @[<v 0>%a@]" (A.to_string arg) label
          (A.to_string pat_l) lab_l pp_print_node_l node_l (A.to_string pat_r)
          lab_r pp_print_node_l node_r
      | N_ITER
          {
            arg_iter;
            acc_iter;
            pat_acc_iter;
            pat = pat, lab_pat;
            body_iter;
            label_iter;
          } ->
        Format.fprintf ppf
          "ITER ON %s [%d] PAT : %s [%d] PAT_ACC : (%a) %a (%a)"
          (A.to_string arg_iter) label_iter (A.to_string pat) lab_pat
          list_of_res pat_acc_iter pp_print_node_l body_iter list_of_res
          acc_iter
      | N_LOOP { arg; acc; pat_acc; body; label_pat_l; label_loop } ->
        Format.fprintf ppf "LOOP ON %s [%d] PAT_ACC : (%a) [%d] %a (%a)"
          (A.to_string arg) label_loop list_of_res pat_acc label_pat_l
          pp_print_node_l body list_of_res acc
      | N_LOOP_LEFT
          {
            arg_lf;
            pat_lf = pat_lf, lab_pat;
            acc_lf;
            pat_acc_lf;
            node_loop;
            pat_res_lf = pat_res_lf, lab_pat_res;
            label_lf;
          } ->
        let return_pat_res = A.to_string pat_res_lf in
        Format.fprintf ppf
          "LOOP_LEFT ON %s [%d] PAT_ACC : (%a) |Right %s [%d] -> (%s,%a)@,\
           |Left %s [%d] -> %a (%a)" (A.to_string arg_lf) label_lf list_of_res
          pat_acc_lf return_pat_res lab_pat_res return_pat_res list_of_res
          pat_acc_lf (A.to_string pat_lf) lab_pat pp_print_node_l node_loop
          list_of_res acc_lf
      | N_MAP (arg, label_pat, pat, node_l, label) ->
        Format.fprintf ppf "MAP %s [%d] WITH PAT : %s [%d] %a" (A.to_string arg)
          label (A.to_string pat) label_pat pp_print_node_l node_l
      | N_FOLD_MAP
          {
            arg_fold_map;
            acc_fold_map;
            pat_acc_fold_map = pat_acc_fold_map, _lab_pat_acc;
            pat_fold_map = pat_fold_map, lab_pat;
            body_fold_map;
            label_map;
          } ->
        Format.fprintf ppf "MAP %s [%d] PAT : %s [%d] PAT_ACC : (%a) %a (%a)"
          (A.to_string arg_fold_map) label_map (A.to_string pat_fold_map)
          lab_pat list_of_res pat_acc_fold_map pp_print_node_l body_fold_map
          list_of_res acc_fold_map in
    Format.fprintf ppf "@[<v 2>%a@]" sub_fun control_node

  and pp_print_node_l ppf node_l =
    let print_list =
      Format.pp_print_list
        ~pp_sep:(fun format () -> Format.fprintf format " ;@,")
        pp_print_node in
    Format.fprintf ppf "@[<v 2>{@,%a@]@,}" print_list node_l

  and pp_print_node ppf node =
    match node with
    | N_SIMPLE (prim, args, arg_l, res_l, label) ->
      let str = Micheline_lib.prim_name ~prim in
      Format.fprintf ppf "%a = %s %a %a [%d]" list_of_res res_l str
        list_of_micheline args list_of_arg arg_l label
    | N_START (arg_l, control_node, start) ->
      Format.fprintf ppf "%a [%d] =@,  @[<v 0>%a@]" list_of_res arg_l start
        pp_print_control control_node
    | FAIL_CONTROL (control_node, _label) ->
      Format.fprintf ppf "%a" pp_print_control control_node
    | N_END (return_l, label) ->
      Format.fprintf ppf "return %a [%d]" list_of_res return_l label
    | N_LAMBDA (pat, lab_pat, node_l, lambda, lab_in, lab_out) ->
      Format.fprintf ppf "%s = LAMBDA WITH %s [%d]@,%a [%d][%d]"
        (A.to_string lambda) (A.to_string pat) lab_pat pp_print_node_l node_l
        lab_in lab_out
    | N_CONCAT (str1, str2, res, label) ->
      Format.fprintf ppf "%s = CONCAT %s %s [%d]" (A.to_string res)
        (A.to_string str1) (A.to_string str2) label
    | N_CONCAT_LIST (str_l, res, label) ->
      Format.fprintf ppf "%s = CONCAT_LIST %s [%d]" (A.to_string res)
        (A.to_string str_l) label
    | N_SELF (str_opt, res, label) ->
      Format.fprintf ppf "%s = SELF %a [%d]" (A.to_string res) print_annot
        str_opt label
    | N_CONTRACT (str_opt, ctrt, res, label) ->
      Format.fprintf ppf "%s = CONTRACT %a %s [%d]" (A.to_string res)
        print_annot str_opt (A.to_string ctrt) label
    | N_CREATE_CONTRACT (_opt_k, _mutz, _ty2, _contract, _ope, _addr, _) ->
      Format.fprintf ppf "CREATE_CONTRACT TODO"
    | N_NEVER (n, label) ->
      Format.fprintf ppf "NEVER %s [%d]" (A.to_string n) label
    | N_FAILWITH (fail, label) ->
      Format.fprintf ppf "FAILWITH %s [%d]" (A.to_string fail) label
    | N_CALL_VIEW (str, arg, view, res, label) ->
      Format.fprintf ppf "%s = CALL_VIEW %s %s %s [%d]" (A.to_string res) str
        (A.to_string arg) (A.to_string view) label
    | N_EXEC (arg, lambda, res, label_call, label_ret) ->
      Format.fprintf ppf "%s = EXEC %s %s [%d][%d]" (A.to_string res)
        (A.to_string arg) (A.to_string lambda) label_call label_ret

  let view_printer format view =
    let s_l =
      Micheline_lib.string_of_micheline ~simple:false view.view_parameter in
    Format.fprintf format "view %s %s START WITH %s" view.node_name s_l
      (A.to_string view.view_stack) ;
    pp_print_node_l format view.node_code_v

  let smart_printer format (sc : A.t sc_node) =
    let print_miche = Micheline_lib.print_micheline ~simple:false in
    Format.fprintf format
      "@[<v 2>{@,parameter %a;@,storage %a;@,code @[<v 0>%a@]@,}" print_miche
      sc.parameter print_miche sc.storage pp_print_node_l sc.node_l ;
    Format.pp_print_flush format ()
end
