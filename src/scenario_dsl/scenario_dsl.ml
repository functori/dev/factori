(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module defines an embedded DSL for writing scenarios in Factori,
including various data types and functions for creating and manipulating AST
objects, handling forks, and updating scenario states. These functions are used
to build up the AST for a scenario, which can then be compiled to OCaml and
TypeScript. Warning: this short summary was automatically generated and could be
incomplete or misleading. If you feel this is the case, please open an issue. *)

open Dsl_exceptions
open Tzfunc.Proto
open Format

(* This module is the embedded DSL through which scenarios can be
   written *)
module AstInterface = struct
  open Ast
  open Scenario_value

  (* type ('a, 'b) ff = F : ('a -> 'b) -> ('a, 'b) ff
   *
   * let convert (_x : ('a,'b) ff)  (x : 'a astobj) : 'b astobj =
   *   ({id = x.id;ast_value = x.ast_value} : 'b astobj ) *)

  let convert (type a b) (x : a astobj) : b astobj =
    { id = x.id; ast_value = x.ast_value }

  (* Generator of fresh id generator, parametered by a string *)
  let fresh_id (_obj_type : string) =
    let count = ref 0 in
    fun () ->
      let res = !count in
      count := !count + 1 ;
      res

  let fresh_address = fresh_id "address"

  let fresh_balance = fresh_id "balance"

  let fresh_storage = fresh_id "storage"

  let fresh_print = fresh_id "print"

  let fresh_key = fresh_id "key"

  let fresh_native_type = fresh_id "native_type"

  let fresh_value = fresh_id "value"

  let fresh_amount = fresh_id "amount"

  let fresh_kt1 = fresh_id "kt1"

  let fresh_network = fresh_id "network"

  let fresh_micheline = fresh_id "micheline"

  let fresh_abstract_value = fresh_id "abstract_value"

  let fresh_deploy = fresh_id "deploy"

  let fresh_call = fresh_id "call"

  let fresh_failed_call = fresh_id "failed_call"

  let fresh_fork = fresh_id "fork"

  let fresh_default_identity = fresh_id "default_identity"

  type scenario_id = int

  let fresh_scenario_id =
    let x = ref 0 in
    function
    | () ->
      let res = !x in
      x := !x + 1 ;
      res

  type scenario = { get_scenario_id : unit -> scenario_id }

  type scenario_state = {
    finished : bool;
    open_forks : int list;
    ast : ast_context;
  }

  let scenarios_table = Hashtbl.create 10

  let add_scenario (id : int) (ast : scenario_state) =
    if Hashtbl.mem scenarios_table id then
      raise Scenario_key_already_used
    else
      Hashtbl.add scenarios_table id ast

  let replace_scenario scenario_id ast =
    Hashtbl.replace scenarios_table scenario_id ast

  let find_scenario (id : int) =
    match Hashtbl.find_opt scenarios_table id with
    | None -> raise Scenario_key_not_found
    | Some s -> s

  let new_scenario () =
    let ast = empty_ast in
    let id = fresh_scenario_id () in
    add_scenario id { ast; finished = false; open_forks = [] } ;
    { get_scenario_id = (fun () -> id) }

  type fork_id = int

  let finish_scenario scenario =
    let scenario_id = scenario.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    match scenario_state.open_forks with
    | [] ->
      Hashtbl.replace scenarios_table scenario_id
        { scenario_state with finished = true }
    | f :: _ -> raise (OpenForkRemaining f)

  let generate_agent (s : scenario) (seed : int) : identity astobj =
    let id = fresh_key () in
    let name = sprintf "agent%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (Generate_key seed) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let get_address (s : scenario) (identity : identity astobj) : address astobj =
    let id = fresh_address () in
    let name = sprintf "address%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (FunctionCall (GetAddress, [identity.id])) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let get_unary_unsafe v =
    match v with
    | Unary u -> u
    | _ ->
      failwith
        "[get_unary_unsafe] Please report: trying to get unary value from a \
         non unary one"

  let print_address (s : scenario) ?(msg = "") (address : address astobj) :
      unit astobj =
    let id = fresh_print () in
    let name = sprintf "print_address%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = PrintUnary (msg, get_unary_unsafe address.ast_value) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let get_balance (s : scenario) (address : address astobj) : Z.t astobj =
    let id = fresh_balance () in
    let name = sprintf "balance%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (FunctionCall (GetBalance, [address.id])) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let print_balance (s : scenario) ?(msg = "") (balance : Z.t astobj) :
      unit astobj =
    let id = fresh_print () in
    let name = sprintf "print_balance%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = PrintUnary (msg, get_unary_unsafe balance.ast_value) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let get_storage (s : scenario) (kt1 : string astobj) : 'a astobj =
    let id = fresh_storage () in
    let name = sprintf "storage%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (FunctionCall (GetStorage, [kt1.id])) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  (* let make_random (s : scenario) ~contract_name ~type_name : 'a astobj =
   *   let id = fresh_native_type () in
   *   let name = sprintf "random%d" id in
   *   let scenario_id = s.get_scenario_id () in
   *   let scenario_state = find_scenario scenario_id in
   *   let value = failwith "TODO: random"(\* unary (NativeType(RandomB(type_name),contract_name,Unchanged)) *\) in
   *   let new_ast = insert_at_end name value scenario_state.ast in
   *   replace_scenario scenario_id { scenario_state with ast = new_ast } ;
   *   { id = name; ast_value = value } *)

  (* let make_initial_blockchain_storage (s : scenario) ~contract_name : 'a astobj =
   *   let id = fresh_native_type () in
   *   let name = sprintf "ibstorage%d" id in
   *   let scenario_id = s.get_scenario_id () in
   *   let scenario_state = find_scenario scenario_id in
   *   let value = unary (NativeType(InitialBlockchainStorage,contract_name,Unchanged)) in
   *   let new_ast = insert_at_end name value scenario_state.ast in
   *   replace_scenario scenario_id { scenario_state with ast = new_ast } ;
   *   { id = name; ast_value = value } *)

  (* Helper function to make a substitution inside a record, possibly
     nested *)
  (* let make_subst (s : scenario) (b : baseNative) ~contract_name (field_list : string list) (value_to_substitute : 'a astobj) =
   *   let id = fresh_native_type () in
   *   let name = sprintf "subst%d" id in
   *   let scenario_id = s.get_scenario_id () in
   *   let scenario_state = find_scenario scenario_id in
   *   let rec aux = function
   *     | [] -> failwith "make_subst cannot be given an empty list"
   *     | [field1] -> Subst(value_to_substitute.id,field1)
   *     | field1::fields -> Subfield(field1,aux fields) in
   *   let value = unary @@ NativeType(b,contract_name,aux field_list) in
   * 
   *   let new_ast = insert_at_end name value scenario_state.ast in
   *   replace_scenario scenario_id { scenario_state with ast = new_ast } ;
   *   { id = name; ast_value = value } *)

  let make_kt1 (s : scenario) (kt1 : 'id kt1) : address astobj =
    let id = fresh_kt1 () in
    let name = sprintf "kt1%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (Kt1_uv kt1) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_static_kt1 (s : scenario) (kt1 : address) : address astobj =
    make_kt1 s (StaticKt1 kt1)

  let make_amount (s : scenario) (amount : amount) : amount astobj =
    let id = fresh_amount () in
    let name = sprintf "amount%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (Amount_uv amount) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_network (s : scenario) (n : network) : network astobj =
    let id = fresh_network () in
    let name = sprintf "network%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (Network_uv n) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_value (s : scenario) (v : 'a Abstract_value.value) : 'b astobj =
    let id = fresh_abstract_value () in
    let name = sprintf "abstract%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (NativeAbstractValue (AValue v)) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_micheline (s : scenario) (m : micheline) : micheline astobj =
    let id = fresh_micheline () in
    let name = sprintf "micheline%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value = unary (Micheline_uv m) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_deploy (s : scenario) (from : identity astobj)
      (amount : amount astobj) (network : network astobj) (storage : 'a astobj)
      contract_name : string astobj =
    let id = fresh_deploy () in
    let name = sprintf "kt1_deploy%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in

    (* let ast = find_scenario scenario_id in *)
    let value =
      Deploy
        {
          from = from.id;
          amount = amount.id;
          network = network.id;
          storage = storage.id;
          contract_name;
        } in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_call ?(assert_success = false) ?(msg = "") (s : scenario) from amount
      entrypoint kt1 network contract_name (param : 'a astobj) : string astobj =
    let id = fresh_call () in
    let name = sprintf "call%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in

    (* let ast = find_scenario scenario_id in *)
    let value =
      Call
        {
          from = from.id;
          amount = amount.id;
          network = network.id;
          entrypoint;
          contract = kt1.id;
          param = param.id;
          contract_name;
          assert_success;
          msg;
        } in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_failed_call (s : scenario) from amount entrypoint kt1 network
      contract_name (expected : string) (msg : string) (param : 'a astobj) :
      string astobj =
    let id = fresh_failed_call () in
    let name = sprintf "failed_call%d" id in
    let scenario_id = s.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let value =
      FailedCall
        (Failed
           ( expected,
             msg,
             {
               from = from.id;
               amount = amount.id;
               network = network.id;
               entrypoint;
               contract = kt1.id;
               param = param.id;
               contract_name;
               assert_success = false;
               msg;
             } )) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  let make_default_identity (s : scenario) (n : network) : address astobj =
    let id = fresh_default_identity () in
    let scenario_id = s.get_scenario_id () in
    let name = sprintf "default_identity%d" id in
    let scenario_state = find_scenario scenario_id in
    let value = unary (DefaultIdentity n) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    { id = name; ast_value = value }

  type 'a obj = 'a astobj

  let get_ast scenario =
    let scenario_state = find_scenario (scenario.get_scenario_id ()) in
    if not scenario_state.finished then (
      try
        finish_scenario scenario ;
        scenario_state.ast
      with OpenForkRemaining f ->
        eprintf "Could not seal scenario as some forks were open@." ;
        raise (OpenForkRemaining f)
    ) else
      scenario_state.ast

  let gen_agent scenario seed = generate_agent scenario seed

  let mk_amount scenario amount = make_amount scenario amount

  let mk_network scenario network = make_network scenario network

  let mk_micheline scenario m = make_micheline scenario m

  let mk_default_identity scenario network =
    make_default_identity scenario network

  let mk_deploy ~scenario ~from ~amount ~network ~storage ~contract_name =
    make_deploy scenario from amount network storage contract_name

  let mk_call ~assert_success ~msg ~scenario ~from ~amount ~entrypoint ~kt1
      ~network ~contract_name ~param =
    make_call ~assert_success ~msg scenario from amount entrypoint kt1 network
      contract_name param

  let mk_failed_call ~scenario ~from ~amount ~entrypoint ~kt1 ~network
      ~contract_name ~expected ~msg param =
    make_failed_call scenario from amount entrypoint kt1 network contract_name
      expected msg param

  let fork scenario (l : ast_context list) : unit =
    let id = fresh_fork () in
    let scenario_id = scenario.get_scenario_id () in
    let name = sprintf "fork%d" id in
    let scenario_state = find_scenario scenario_id in
    let fresh_obj = fresh_id (sprintf "%s" name) in
    let value =
      Fork
        (List.map
           (fun (li : ast_context) ->
             (Format.sprintf "fork%d" (fresh_obj ()), Seq li.ast_values))
           l) in
    let new_ast = insert_at_end name value scenario_state.ast in
    replace_scenario scenario_id { scenario_state with ast = new_ast } ;
    ()

  (* Forks *)

  let fresh_fork_id =
    let counter = ref 0 in
    function
    | () ->
      let res = !counter in
      incr counter ;
      res

  let forks_table = Hashtbl.create 10

  let open_fork scenario =
    let fork_id = fresh_fork_id () in
    (* Register that a new fork has been opened *)
    let scenario_id = scenario.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    Hashtbl.replace scenarios_table scenario_id
      { scenario_state with open_forks = fork_id :: scenario_state.open_forks } ;
    Hashtbl.add forks_table fork_id (scenario, []) ;
    fork_id

  let add_branch (fid : fork_id) =
    let scenario = new_scenario () in
    let s, l = Hashtbl.find forks_table fid in
    let branch_closing_function = function
      | () -> get_ast scenario in
    Hashtbl.replace forks_table fid (s, branch_closing_function :: l) ;
    scenario

  let close_fork (fid : fork_id) =
    let scenario, l = Hashtbl.find forks_table fid in
    (* Register that this fork is closing *)
    let scenario_id = scenario.get_scenario_id () in
    let scenario_state = find_scenario scenario_id in
    let new_open_forks =
      List.filter (fun x -> x <> fid) scenario_state.open_forks in
    Hashtbl.replace scenarios_table scenario_id
      { scenario_state with open_forks = new_open_forks } ;
    let l = List.rev l in
    fork scenario (List.map (fun f -> f ()) l)
end
