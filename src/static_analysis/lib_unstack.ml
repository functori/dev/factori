(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions to modify control flow graphs and their
nodes in the Michelson language for the Tezos blockchain. It defines a `mapper`
type, a `MapName` module, and a `NameSet` module, and includes functions to
change and replace elements using maps and lists. Warning: this short summary
was automatically generated and could be incomplete or misleading. If you feel
this is the case, please open an issue. *)

open Unstack_micheline
open Unstack_mapper
open Factori_representation
module MapName = Map.Make (Name_arg)
module NameSet = Set.Make (Name_arg)

let change env n =
  match MapName.find_opt n env with
  | None -> n
  | Some e -> e

let change_all env l = List.map (fun n -> change n env) l

let change_mapper = { default_mapper with apply_use_arg = change }

let replace_ident (node_l : name_arg node list) (old : name_arg list)
    (new_ones : name_arg list) : name_arg node list =
  let map =
    List.fold_left2
      (fun acc old young -> MapName.add old young acc)
      MapName.empty old new_ones in
  change_mapper.apply_node_l change_mapper map node_l

let label_of_node (node : 'a node) =
  match node with
  | N_SIMPLE (_, _, _, _, label)
  | N_START (_, _, label)
  | FAIL_CONTROL (_, label)
  | N_END (_, label)
  | N_CONCAT (_, _, _, label) (* MULTIPLE DEF *)
  | N_CONCAT_LIST (_, _, label)
  | N_SELF (_, _, label)
  | N_CONTRACT (_, _, _, label)
  | N_CREATE_CONTRACT (_, _, _, _, _, _, label)
  | N_NEVER (_, label)
  | N_FAILWITH (_, label)
  | N_CALL_VIEW (_, _, _, _, label) -> [label]
  | N_LAMBDA (_, _, _, _, l1, l2) (* TODO *) | N_EXEC (_, _, _, l1, l2) ->
    [l1; l2]

(*let node_list_of_ctrl (node : 'a control_node) =
  match node with
   | N_IF(_,_,node_l1,node_l2)
  | N_IF_NONE(_,_,node_l1,node_l2,_,_)
  | N_IF_CONS(_,_,node_l1,node_l2,_,_,_,_)
  | N_IF_LEFT of
      'a * label * ('a * label * 'a node list) * ('a * label * 'a node list)
  | N_ITER of {
      arg_iter : 'a;
      acc_iter : 'a list;
      pat_acc_iter : 'a list;
      pat : 'a * label;
      body_iter : 'a node list;
      label_iter : label;
    }
  | N_LOOP of {
      arg : 'a;
      acc : 'a list;
      pat_acc : 'a list;
      body : 'a node list;
      label_pat_l : label;
      label_loop : label;
    }
  | N_LOOP_LEFT of {
      arg_lf : 'a;
      pat_lf : 'a * label;
      acc_lf : 'a list;
      pat_acc_lf : 'a list;
      node_loop : 'a node list;
      pat_res_lf : 'a * label;
      label_lf : label;
    }
  | N_MAP of 'a * label * 'a * 'a node list * label
  | N_FOLD_MAP of {
      arg_fold_map : 'a;
      acc_fold_map : 'a list;
      pat_acc_fold_map : 'a list;
      pat_fold_map : 'a * label;
      body_fold_map : 'a node list;
      label_map : label;
    }*)
