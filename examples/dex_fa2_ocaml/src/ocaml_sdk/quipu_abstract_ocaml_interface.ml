open Factori_abstract_types.Abstract
module Interface = Quipu_ocaml_interface

type set_position_pause = unit'

type abstract_set_position_pause = set_position_pause

let value_of_set_position_pause : set_position_pause -> 'a = value_of_unit'

let value_of_abstract_set_position_pause = value_of_set_position_pause

let lift_set_position_pause : 'a -> abstract_set_position_pause = lift_unit'

type type0 =
  | Set_position_pause
  | Update_position_pause
  | X_to_y_pause
  | Y_to_x_pause

type abstract_type0 = type0 Factori_abstract_types.Abstract.id_or_concrete

let value_of_type0 (arg : type0) =
  match arg with
  | Set_position_pause ->
    Abstract_value.VSumtype
      (SumOrL
         (SumOrL
            (LeafOr
               (Factori_utils.sanitized_of_str "Set_position_pause", VUnit ()))))
  | Update_position_pause ->
    Abstract_value.VSumtype
      (SumOrL
         (SumOrR
            (LeafOr
               (Factori_utils.sanitized_of_str "Update_position_pause", VUnit ()))))
  | X_to_y_pause ->
    Abstract_value.VSumtype
      (SumOrR
         (SumOrL
            (LeafOr (Factori_utils.sanitized_of_str "X_to_y_pause", VUnit ()))))
  | Y_to_x_pause ->
    Abstract_value.VSumtype
      (SumOrR
         (SumOrR
            (LeafOr (Factori_utils.sanitized_of_str "Y_to_x_pause", VUnit ()))))

let value_of_abstract_type0 (x : abstract_type0) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_type0 c

let lift_type0 (arg : Interface.type0) : abstract_type0 =
  match arg with
  | Set_position_pause -> Concrete Set_position_pause
  | Update_position_pause -> Concrete Update_position_pause
  | X_to_y_pause -> Concrete X_to_y_pause
  | Y_to_x_pause -> Concrete Y_to_x_pause

type set_pause = abstract_type0 set

type abstract_set_pause = set_pause

let value_of_set_pause (arg : set_pause) : 'a =
  (value_of_set value_of_abstract_type0) arg

let value_of_abstract_set_pause = value_of_set_pause

let lift_set_pause (arg : Interface.set_pause) : set_pause =
  (lift_set lift_type0) arg

let mk_call_set_pause ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_pause param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_pause"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:param_value

let assert_failwith_str_set_pause ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_pause param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"set_pause"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"quipu"
    param_value

type set_owner = address

type abstract_set_owner = set_owner

let value_of_set_owner : set_owner -> 'a = value_of_address

let value_of_abstract_set_owner = value_of_set_owner

let lift_set_owner : 'a -> abstract_set_owner = lift_address

let mk_call_set_owner ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_owner param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_owner"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:param_value

let assert_failwith_str_set_owner ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_owner param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"set_owner"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"quipu"
    param_value

type set_dev_fee = nat

type abstract_set_dev_fee = set_dev_fee

let value_of_set_dev_fee : set_dev_fee -> 'a = value_of_nat

let value_of_abstract_set_dev_fee = value_of_set_dev_fee

let lift_set_dev_fee : 'a -> abstract_set_dev_fee = lift_nat

let mk_call_set_dev_fee ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_dev_fee param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_dev_fee"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:param_value

let assert_failwith_str_set_dev_fee ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_dev_fee param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"set_dev_fee"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"quipu"
    param_value

type fa2 = {
  token_id : abstract_set_dev_fee;
  token_address : abstract_set_owner;
}

type abstract_fa2 = fa2 Factori_abstract_types.Abstract.id_or_concrete

let value_of_fa2 (arg : fa2) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "token_id",
             value_of_abstract_set_dev_fee arg.token_id ),
         Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "token_address",
             value_of_abstract_set_owner arg.token_address ) ))

let value_of_abstract_fa2 (x : abstract_fa2) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_fa2 c

let lift_fa2 (arg : Interface.fa2) : abstract_fa2 =
  Concrete
    {
      token_id = lift_set_dev_fee arg.token_id;
      token_address = lift_set_owner arg.token_address;
    }

type token_y =
  | Fa12 of abstract_set_owner
  | Fa2 of abstract_fa2

type abstract_token_y = token_y Factori_abstract_types.Abstract.id_or_concrete

let value_of_token_y (arg : token_y) =
  match arg with
  | Fa12 x ->
    Abstract_value.VSumtype
      (SumOrL
         (LeafOr
            ( Factori_utils.sanitized_of_str "Fa12",
              value_of_abstract_set_owner x )))
  | Fa2 x ->
    Abstract_value.VSumtype
      (SumOrR
         (LeafOr (Factori_utils.sanitized_of_str "Fa2", value_of_abstract_fa2 x)))

let value_of_abstract_token_y (x : abstract_token_y) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_token_y c

let lift_token_y (arg : Interface.token_y) : abstract_token_y =
  match arg with
  | Fa12 x -> Concrete (Fa12 (lift_set_owner x))
  | Fa2 x -> Concrete (Fa2 (lift_fa2 x))

type cur_tick_index = int_michelson

type abstract_cur_tick_index = cur_tick_index

let value_of_cur_tick_index : cur_tick_index -> 'a = value_of_int_michelson

let value_of_abstract_cur_tick_index = value_of_cur_tick_index

let lift_cur_tick_index : 'a -> abstract_cur_tick_index = lift_int_michelson

type deploy_pool = {
  cur_tick_index : abstract_cur_tick_index;
  token_x : abstract_token_y;
  token_y : abstract_token_y;
  fee_bps : abstract_set_dev_fee;
  tick_spacing : abstract_set_dev_fee;
  extra_slots : abstract_set_dev_fee;
}

type abstract_deploy_pool =
  deploy_pool Factori_abstract_types.Abstract.id_or_concrete

let value_of_deploy_pool (arg : deploy_pool) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "cur_tick_index",
             value_of_abstract_cur_tick_index arg.cur_tick_index ),
         Abstract_value.PairP
           ( Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_x",
                 value_of_abstract_token_y arg.token_x ),
             Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "token_y",
                     value_of_abstract_token_y arg.token_y ),
                 Abstract_value.PairP
                   ( Abstract_value.LeafP
                       ( Factori_utils.sanitized_of_str "fee_bps",
                         value_of_abstract_set_dev_fee arg.fee_bps ),
                     Abstract_value.PairP
                       ( Abstract_value.LeafP
                           ( Factori_utils.sanitized_of_str "tick_spacing",
                             value_of_abstract_set_dev_fee arg.tick_spacing ),
                         Abstract_value.LeafP
                           ( Factori_utils.sanitized_of_str "extra_slots",
                             value_of_abstract_set_dev_fee arg.extra_slots ) )
                   ) ) ) ))

let value_of_abstract_deploy_pool (x : abstract_deploy_pool) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_deploy_pool c

let lift_deploy_pool (arg : Interface.deploy_pool) : abstract_deploy_pool =
  Concrete
    {
      cur_tick_index = lift_cur_tick_index arg.cur_tick_index;
      token_x = lift_token_y arg.token_x;
      token_y = lift_token_y arg.token_y;
      fee_bps = lift_set_dev_fee arg.fee_bps;
      tick_spacing = lift_set_dev_fee arg.tick_spacing;
      extra_slots = lift_set_dev_fee arg.extra_slots;
    }

let mk_call_deploy_pool ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_deploy_pool param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"deploy_pool"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:param_value

let assert_failwith_str_deploy_pool ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_deploy_pool param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"deploy_pool"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"quipu"
    param_value

type confirm_owner = set_position_pause

type abstract_confirm_owner = abstract_set_position_pause

let value_of_confirm_owner : confirm_owner -> 'a = value_of_set_position_pause

let value_of_abstract_confirm_owner = value_of_abstract_set_position_pause

let lift_confirm_owner : 'a -> abstract_confirm_owner = lift_set_position_pause

let mk_call_confirm_owner ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_confirm_owner param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"confirm_owner"
    ~kt1
    ~network
    ~contract_name:"quipu"
    ~param:param_value

let assert_failwith_str_confirm_owner ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_confirm_owner param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"confirm_owner"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"quipu"
    param_value

type type4 = {
  fee_bps : abstract_set_dev_fee;
  token_x : abstract_token_y;
  token_y : abstract_token_y;
}

type abstract_type4 = type4 Factori_abstract_types.Abstract.id_or_concrete

let value_of_type4 (arg : type4) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "fee_bps",
             value_of_abstract_set_dev_fee arg.fee_bps ),
         Abstract_value.PairP
           ( Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_x",
                 value_of_abstract_token_y arg.token_x ),
             Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_y",
                 value_of_abstract_token_y arg.token_y ) ) ))

let value_of_abstract_type4 (x : abstract_type4) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_type4 c

let lift_type4 (arg : Interface.type4) : abstract_type4 =
  Concrete
    {
      fee_bps = lift_set_dev_fee arg.fee_bps;
      token_x = lift_token_y arg.token_x;
      token_y = lift_token_y arg.token_y;
    }

type _storage = {
  pool_ids : (abstract_type4, abstract_set_dev_fee) big_map;
  pool_count : abstract_set_dev_fee;
  owner : abstract_set_owner;
  dev_fee_bps : abstract_set_dev_fee;
  pause_state : abstract_type0 set;
  pending_owner : abstract_set_owner option';
  pools : (abstract_set_dev_fee, abstract_set_owner) big_map;
}

type abstract__storage = _storage Factori_abstract_types.Abstract.id_or_concrete

let value_of__storage (arg : _storage) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.PairP
           ( Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "dev_fee_bps",
                     value_of_abstract_set_dev_fee arg.dev_fee_bps ),
                 Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "owner",
                     value_of_abstract_set_owner arg.owner ) ),
             Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "pause_state",
                     (value_of_set value_of_abstract_type0) arg.pause_state ),
                 Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "pending_owner",
                     (value_of_option' value_of_abstract_set_owner)
                       arg.pending_owner ) ) ),
         Abstract_value.PairP
           ( Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "pool_count",
                     value_of_abstract_set_dev_fee arg.pool_count ),
                 Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "pool_ids",
                     (value_of_big_map
                        value_of_abstract_type4
                        value_of_abstract_set_dev_fee)
                       arg.pool_ids ) ),
             Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "pools",
                 (value_of_big_map
                    value_of_abstract_set_dev_fee
                    value_of_abstract_set_owner)
                   arg.pools ) ) ))

let value_of_abstract__storage (x : abstract__storage) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of__storage c

let lift__storage (arg : Interface._storage) : abstract__storage =
  Concrete
    {
      dev_fee_bps = lift_set_dev_fee arg.dev_fee_bps;
      owner = lift_set_owner arg.owner;
      pause_state = (lift_set lift_type0) arg.pause_state;
      pending_owner = (lift_option' lift_set_owner) arg.pending_owner;
      pool_count = lift_set_dev_fee arg.pool_count;
      pool_ids = (lift_big_map lift_type4 lift_set_dev_fee) arg.pool_ids;
      pools = (lift_big_map lift_set_dev_fee lift_set_owner) arg.pools;
    }

(** Deploy*)
let mk_deploy ~scenario ~from ~amount ~network ~storage =
  let open Scenario_dsl.AstInterface in
  let storage_value = make_value scenario (value_of_abstract__storage storage) in

  mk_deploy
    ~scenario
    ~from
    ~amount
    ~network
    ~storage:storage_value
    ~contract_name:"quipu"

let abstract_initial_blockchain_storage : abstract__storage =
  lift__storage Interface.initial_blockchain_storage

let initial_blockchain_storage : _storage =
  match abstract_initial_blockchain_storage with
  | Concrete x -> x
  | _ ->
    failwith
      "[initial_blockchain_storage] initial blockchain storage should not be \
       an Id"
