#!/bin/sh

check_status () {
    if [ "$?" -ne "0" ]; then
       echo "Failed with ${0}"
       exit 1
    fi
}
# Check if ocamlformat exists
if ! command -v ocamlformat
then
   echo "Command 'ocamlformat' could not be found"
   exit 1
fi

# Check if git exists
if ! command -v git
then
   echo "Command 'git' could not be found"
   exit 1
fi

before_formatting=`git diff`
check_status
make format
check_status
after_formatting=`git diff`
check_status
fileonly=`git diff --name-only`
check_status

if [ "$after_formatting" = "$before_formatting" ] ; then
    echo "Success: all files are well indented"
    exit 0
else
    echo "Failure: following files are not well indented:"
    echo "$fileonly"
    exit 1
fi
