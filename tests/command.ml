open Factori_config
open Factori_network
open Format

module Cmd : sig
  type t

  val run : ?pre:bool -> ?timeout:int -> ?log:string -> t -> int

  val merge : t list -> t

  val make : string -> t
end = struct
  type t = string

  let run ?(pre = false) ?timeout ?log cmd =
    let cmd =
      match timeout with
      | None -> cmd
      | Some timeout -> sprintf "timeout --preserve-status %ds %s" timeout cmd
    in
    let cmd =
      match log with
      | None -> cmd
      | Some log ->
        let cmd = sprintf "(%s) 1>> %s 2>> %s " cmd log log in
        if pre then
          sprintf "echo '<pre>' >> %s && %s && echo '</pre>' >> %s" log cmd log
        else
          cmd in
    Sys.command cmd

  let merge =
    let and_sep ppf () = pp_print_string ppf " && " in
    asprintf "%a" @@ pp_print_list ~pp_sep:and_sep pp_print_string

  let make cmd = cmd
end

let make ?dir tag =
  let dir =
    match dir with
    | None -> ""
    | Some dir -> sprintf "-C %s" dir in
  Cmd.make @@ sprintf "make %s %s" dir tag

let mkdir dir = Cmd.make @@ sprintf "mkdir %s" dir

let rm targets =
  Cmd.make
  @@ asprintf "rm -rf %a"
       (pp_print_list ~pp_sep:(fun ppf () -> fprintf ppf " ") pp_print_string)
       targets

let cp targets src =
  Cmd.make
  @@ asprintf "cp -r %a %s"
       (pp_print_list ~pp_sep:pp_print_space pp_print_string)
       targets src

let replace target src = Cmd.merge [rm [target]; cp [src] target]

let hard_clean target = Cmd.merge [rm [target]; mkdir target]

let factori ~root = root // "_bin" // "factori.asm"

module Import = struct
  type option = interface_language

  let option_to_string = function
    | Csharp -> "csharp"
    | OCaml -> "ocaml"
    | Python -> "python"
    | Typescript -> "typescript"

  let options_to_string options =
    String.concat "_" @@ List.map option_to_string options

  let pp_option ppf option =
    let option = option_to_string option in
    fprintf ppf "--%s" option

  let pp_options ppf options =
    pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
      pp_option ppf options

  let empty_project ?(options = []) ~root ~dir () =
    Cmd.make
    @@ asprintf " %s empty project %s %a" (factori ~root) dir pp_options options

  let import_kt1 ?name ?(options = []) ~root ~dir ~network kt1 =
    let name =
      match name with
      | None -> ""
      | Some name -> sprintf "--name %s" name in
    Cmd.make
    @@ asprintf "%s import kt1 %s %s --network %s %s %a" (factori ~root) dir kt1
         (string_of_network network)
         name pp_options options
end

module OCamlCmd = struct
  let scenario ~dir = OCaml_SDK.scenarios_path ~dir ()

  let make_deps ~dir () =
    Cmd.merge [make ~dir "_opam"; Cmd.make "opam update"; make ~dir "deps"]

  let format ~dir () = make ~dir "format-ocaml"

  let compile ~dir () = make ~dir "ocaml"

  let run ~dir () = make ~dir "run_scenario_ocaml"
end

module TypescriptCmd = struct
  let scenario ~dir name =
    Typescript_SDK.interface_dir ~dir () // sprintf "%s.ts" name

  let scenario_dist ~dir name =
    Typescript_SDK.base_dir ~dir ()
    // "dist" // Typescript_SDK.interface_dir_name // sprintf "%s.js" name

  let make_deps ~dir () = make ~dir "ts-deps"

  let format ~dir () = make ~dir "format-ts"

  let compile ~dir () = make ~dir "ts"

  let run ~dir name = Cmd.make @@ sprintf "node %s" @@ scenario_dist ~dir name
end

module PythonCmd = struct
  let scenario ~dir name = Python_SDK.base_dir ~dir () // sprintf "%s.py" name

  let make_deps () = Cmd.make @@ "pip install pytezos mypy pyright black"

  let run ~dir name = Cmd.make @@ sprintf "python3 %s" @@ scenario ~dir name
end

module CsharpCmd = struct
  let scenario ~dir name =
    Csharp_SDK.scenarios_dir ~dir () // sprintf "%s.cs" name

  let init ~dir () = make ~dir "csharp-init"

  let build ~dir () = make ~dir "csharp-build"

  let format ~dir () = make ~dir "format-csharp"

  let run ~dir () =
    Cmd.make @@ sprintf "dotnet run --project %s" @@ Csharp_SDK.base_dir ~dir ()
end

module Analyze = struct
  let analyze_kt1 ~root ~network kt1 =
    Cmd.make
    @@ asprintf "%s analyze kt1 --network %s %s" (factori ~root)
         (string_of_network network)
         kt1
end

module Lint = struct
  let lint_kt1 ~root ~network kt1 =
    Cmd.make
    @@ asprintf "%s lint kt1 --network %s %s" (factori ~root)
         (string_of_network network)
         kt1
end
