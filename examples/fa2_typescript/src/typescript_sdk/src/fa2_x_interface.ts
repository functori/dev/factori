
import {
    TezosToolkit,
    BigMapAbstraction,
    MichelsonMap,
    OriginationOperation,
    OpKind,
    createTransferOperation,
    TransferParams,
    RPCOperation,
    createRevealOperation
} from "@taquito/taquito"
import { MichelsonV1Expression } from "@taquito/rpc"
import { RpcClient } from '@taquito/rpc';
import { encodeOpHash } from '@taquito/utils';
import { Parser } from '@taquito/michel-codec';
import { stringify } from "querystring"
import { JSONbig } from "./functolib"
import * as functolib from "./functolib";

let fa2_x_code = require('./fa2_x_code.json')


export type token_id = functolib.nat
export function token_id_encode(arg: token_id): MichelsonV1Expression { return functolib.nat_encode(arg) }
export function token_id_decode(m: MichelsonV1Expression): token_id { return (functolib.nat_decode(m)); }
export let token_id_micheline = functolib.nat_micheline
export let token_id_generator = function(): token_id { return functolib.nat_generator() }

export type _operator = functolib.address
export function _operator_encode(arg: _operator): MichelsonV1Expression { return functolib.address_encode(arg) }
export function _operator_decode(m: MichelsonV1Expression): _operator { return (functolib.address_decode(m)); }
export let _operator_micheline = functolib.address_micheline
export let _operator_generator = function(): _operator { return functolib.address_generator() }

export type add_operator = { token_id: token_id, _operator: _operator, owner: _operator }
export function add_operator_encode(arg: add_operator): MichelsonV1Expression {
    return { prim: 'Pair', args: [(_operator_encode(arg.owner)), { prim: 'Pair', args: [(_operator_encode(arg._operator)), (token_id_encode(arg.token_id))] }] }
}
export function add_operator_decode(arg: MichelsonV1Expression): add_operator {
    let before_projection = (functolib.tuple2_decode(_operator_decode, (functolib.tuple2_decode(_operator_decode, token_id_decode))))(arg);
    return {
        owner: (before_projection[0]),
        _operator: (before_projection[1][0]),
        token_id: (before_projection[1][1])
    }

}
let add_operator_micheline = { prim: 'Pair', args: [(_operator_micheline), { prim: 'Pair', args: [(_operator_micheline), (token_id_micheline)] }] }
export function add_operator_generator(): add_operator { return ({ token_id: token_id_generator(), _operator: _operator_generator(), owner: _operator_generator() }) }


type type0_Remove_operator_constructor_subtype = { kind: "Remove_operator_constructor"; Remove_operator_element: add_operator }
type type0_Add_operator_constructor_subtype = { kind: "Add_operator_constructor"; Add_operator_element: add_operator }
export type type0 =
    type0_Remove_operator_constructor_subtype |
    type0_Add_operator_constructor_subtype

export function type0_encode(arg: type0): MichelsonV1Expression {

    switch (arg.kind) {
        case ("Add_operator_constructor"):
            return {
                prim: 'Left',
                args: [add_operator_encode(arg.Add_operator_element)]
            }

        case ("Remove_operator_constructor"):
            return {
                prim: 'Right',
                args: [add_operator_encode(arg.Remove_operator_element)]
            }

    }
}
export function type0_decode(arg: MichelsonV1Expression): type0 {
    let p = functolib.retrieve_path_from_sumtype_typescript(arg);


    if (functolib.arrayEquals(p[0], [true])) {
        return { kind: "Add_operator_constructor", Add_operator_element: (add_operator_decode(p[1])) }
    }

    if (functolib.arrayEquals(p[0], [false])) {
        return { kind: "Remove_operator_constructor", Remove_operator_element: (add_operator_decode(p[1])) }
    }
    throw "unknown primitive in output_sumtype_decode"
}
let type0_micheline = {
    prim: "or",
    args: [(add_operator_micheline), (add_operator_micheline)]
}

export function type0_generator(): type0 {
    return
    functolib.chooseFrom
        ([({ kind: "Add_operator_constructor", value: (add_operator_generator()) }), ({ kind: "Remove_operator_constructor", value: (add_operator_generator()) })])
}

export type update_operators = type0[]
export function update_operators_encode(arg: update_operators): MichelsonV1Expression { return functolib.list_encode(type0_encode)(arg) }
export function update_operators_decode(arg: MichelsonV1Expression): update_operators { return (functolib.list_decode(type0_decode))(arg) }
export let update_operators_micheline = (functolib.list_micheline(type0_micheline))
export let update_operators_generator = function(): update_operators { return (functolib.list_generator(type0_generator))() }

export async function call_update_operators(tk: TezosToolkit,
    update_operators_kt1: string,
    param: update_operators, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = update_operators_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, update_operators_kt1, 'update_operators', res, amount, debug);
}
export async function assert_failwith_str_update_operators(tk: TezosToolkit,
    update_operators_kt1: string,
    param: update_operators, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = update_operators_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, update_operators_kt1, 'update_operators', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type type1 = { amount: token_id, token_id: token_id, to_: _operator }
export function type1_encode(arg: type1): MichelsonV1Expression {
    return { prim: 'Pair', args: [(_operator_encode(arg.to_)), { prim: 'Pair', args: [(token_id_encode(arg.token_id)), (token_id_encode(arg.amount))] }] }
}
export function type1_decode(arg: MichelsonV1Expression): type1 {
    let before_projection = (functolib.tuple2_decode(_operator_decode, (functolib.tuple2_decode(token_id_decode, token_id_decode))))(arg);
    return {
        to_: (before_projection[0]),
        token_id: (before_projection[1][0]),
        amount: (before_projection[1][1])
    }

}
let type1_micheline = { prim: 'Pair', args: [(_operator_micheline), { prim: 'Pair', args: [(token_id_micheline), (token_id_micheline)] }] }
export function type1_generator(): type1 { return ({ amount: token_id_generator(), token_id: token_id_generator(), to_: _operator_generator() }) }

export type type2 = { from_: _operator, txs: type1[] }
export function type2_encode(arg: type2): MichelsonV1Expression {
    return { prim: 'Pair', args: [(_operator_encode(arg.from_)), (functolib.list_encode(type1_encode)(arg.txs))] }
}
export function type2_decode(arg: MichelsonV1Expression): type2 {
    let before_projection = (functolib.tuple2_decode(_operator_decode, functolib.list_decode(type1_decode)))(arg);
    return {
        from_: (before_projection[0]),
        txs: (before_projection[1])
    }

}
let type2_micheline = { prim: 'Pair', args: [(_operator_micheline), ((functolib.list_micheline(type1_micheline)))] }
export function type2_generator(): type2 { return ({ from_: _operator_generator(), txs: (functolib.list_generator(type1_generator))() }) }

export type transfer = type2[]
export function transfer_encode(arg: transfer): MichelsonV1Expression { return functolib.list_encode(type2_encode)(arg) }
export function transfer_decode(arg: MichelsonV1Expression): transfer { return (functolib.list_decode(type2_decode))(arg) }
export let transfer_micheline = (functolib.list_micheline(type2_micheline))
export let transfer_generator = function(): transfer { return (functolib.list_generator(type2_generator))() }

export async function call_transfer(tk: TezosToolkit,
    transfer_kt1: string,
    param: transfer, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = transfer_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, transfer_kt1, 'transfer', res, amount, debug);
}
export async function assert_failwith_str_transfer(tk: TezosToolkit,
    transfer_kt1: string,
    param: transfer, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = transfer_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, transfer_kt1, 'transfer', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type type3 = { token_id: token_id, token_info: functolib.map<string, functolib.bytes> }
export function type3_encode(arg: type3): MichelsonV1Expression {
    return { prim: 'Pair', args: [(token_id_encode(arg.token_id)), (functolib.map_encode(functolib.string_encode, functolib.bytes_encode)(arg.token_info))] }
}
export function type3_decode(arg: MichelsonV1Expression): type3 {
    let before_projection = (functolib.tuple2_decode(token_id_decode, functolib.map_decode(functolib.string_decode, functolib.bytes_decode)))(arg);
    return {
        token_id: (before_projection[0]),
        token_info: (before_projection[1])
    }

}
let type3_micheline = { prim: 'Pair', args: [(token_id_micheline), ((functolib.map_micheline(functolib.string_micheline, functolib.bytes_micheline)))] }
export function type3_generator(): type3 { return ({ token_id: token_id_generator(), token_info: (functolib.map_generator(functolib.string_generator, functolib.bytes_generator))() }) }

export type token_metadata = { token_ids: token_id[], handler: functolib.lambda<type3[], functolib.unit> }
export function token_metadata_encode(arg: token_metadata): MichelsonV1Expression {
    return { prim: 'Pair', args: [(functolib.list_encode(token_id_encode)(arg.token_ids)), (functolib.lambda_encode(functolib.list_encode(type3_encode), functolib.unit_encode)(arg.handler))] }
}
export function token_metadata_decode(arg: MichelsonV1Expression): token_metadata {
    let before_projection = (functolib.tuple2_decode(functolib.list_decode(token_id_decode), functolib.lambda_decode(functolib.list_decode(type3_decode), functolib.unit_decode)))(arg);
    return {
        token_ids: (before_projection[0]),
        handler: (before_projection[1])
    }

}
let token_metadata_micheline = { prim: 'Pair', args: [((functolib.list_micheline(token_id_micheline))), ((functolib.lambda_micheline((functolib.list_micheline(type3_micheline)), functolib.unit_micheline)))] }
export function token_metadata_generator(): token_metadata { return ({ token_ids: (functolib.list_generator(token_id_generator))(), handler: (functolib.lambda_generator((functolib.list_generator(type3_generator)), functolib.unit_generator))() }) }

export async function call_token_metadata(tk: TezosToolkit,
    token_metadata_kt1: string,
    param: token_metadata, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = token_metadata_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, token_metadata_kt1, 'token_metadata', res, amount, debug);
}
export async function assert_failwith_str_token_metadata(tk: TezosToolkit,
    token_metadata_kt1: string,
    param: token_metadata, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = token_metadata_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, token_metadata_kt1, 'token_metadata', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type set_pause = functolib.bool
export function set_pause_encode(arg: set_pause): MichelsonV1Expression { return functolib.bool_encode(arg) }
export function set_pause_decode(m: MichelsonV1Expression): set_pause { return (functolib.bool_decode(m)); }
export let set_pause_micheline = functolib.bool_micheline
export let set_pause_generator = function(): set_pause { return functolib.bool_generator() }

export async function call_set_pause(tk: TezosToolkit,
    set_pause_kt1: string,
    param: set_pause, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = set_pause_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, set_pause_kt1, 'set_pause', res, amount, debug);
}
export async function assert_failwith_str_set_pause(tk: TezosToolkit,
    set_pause_kt1: string,
    param: set_pause, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = set_pause_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, set_pause_kt1, 'set_pause', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type set_administrator = _operator

export function set_administrator_encode(arg: set_administrator): MichelsonV1Expression { return _operator_encode(arg) }

export function set_administrator_decode(arg: MichelsonV1Expression): set_administrator { return _operator_decode(arg) }

export let set_administrator_micheline = _operator_micheline

export let set_administrator_generator = function(): set_administrator { return _operator_generator() }

export async function call_set_administrator(tk: TezosToolkit,
    set_administrator_kt1: string,
    param: set_administrator, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = set_administrator_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, set_administrator_kt1, 'set_administrator', res, amount, debug);
}
export async function assert_failwith_str_set_administrator(tk: TezosToolkit,
    set_administrator_kt1: string,
    param: set_administrator, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = set_administrator_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, set_administrator_kt1, 'set_administrator', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type payout_balance = functolib.unit
export function payout_balance_encode(arg: payout_balance): MichelsonV1Expression { return functolib.unit_encode(arg) }
export function payout_balance_decode(m: MichelsonV1Expression): payout_balance { return (functolib.unit_decode(m)); }
export let payout_balance_micheline = functolib.unit_micheline
export let payout_balance_generator = function(): payout_balance { return functolib.unit_generator() }

export async function call_payout_balance(tk: TezosToolkit,
    payout_balance_kt1: string,
    param: payout_balance, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = payout_balance_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, payout_balance_kt1, 'payout_balance', res, amount, debug);
}
export async function assert_failwith_str_payout_balance(tk: TezosToolkit,
    payout_balance_kt1: string,
    param: payout_balance, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = payout_balance_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, payout_balance_kt1, 'payout_balance', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type mint = { token_info: functolib.map<string, functolib.bytes>, token_id: token_id, address: _operator, amount: token_id }
export function mint_encode(arg: mint): MichelsonV1Expression {
    return { prim: 'Pair', args: [{ prim: 'Pair', args: [(_operator_encode(arg.address)), (token_id_encode(arg.amount))] }, { prim: 'Pair', args: [(token_id_encode(arg.token_id)), (functolib.map_encode(functolib.string_encode, functolib.bytes_encode)(arg.token_info))] }] }
}
export function mint_decode(arg: MichelsonV1Expression): mint {
    let before_projection = (functolib.tuple2_decode((functolib.tuple2_decode(_operator_decode, token_id_decode)), (functolib.tuple2_decode(token_id_decode, functolib.map_decode(functolib.string_decode, functolib.bytes_decode)))))(arg);
    return {
        address: (before_projection[0][0]),
        amount: (before_projection[0][1]),
        token_id: (before_projection[1][0]),
        token_info: (before_projection[1][1])
    }

}
let mint_micheline = { prim: 'Pair', args: [{ prim: 'Pair', args: [(_operator_micheline), (token_id_micheline)] }, { prim: 'Pair', args: [(token_id_micheline), ((functolib.map_micheline(functolib.string_micheline, functolib.bytes_micheline)))] }] }
export function mint_generator(): mint { return ({ token_info: (functolib.map_generator(functolib.string_generator, functolib.bytes_generator))(), token_id: token_id_generator(), address: _operator_generator(), amount: token_id_generator() }) }

export async function call_mint(tk: TezosToolkit,
    mint_kt1: string,
    param: mint, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = mint_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, mint_kt1, 'mint', res, amount, debug);
}
export async function assert_failwith_str_mint(tk: TezosToolkit,
    mint_kt1: string,
    param: mint, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = mint_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, mint_kt1, 'mint', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type burn = { token_id: token_id, amount: token_id, address: _operator }
export function burn_encode(arg: burn): MichelsonV1Expression {
    return { prim: 'Pair', args: [(_operator_encode(arg.address)), { prim: 'Pair', args: [(token_id_encode(arg.amount)), (token_id_encode(arg.token_id))] }] }
}
export function burn_decode(arg: MichelsonV1Expression): burn {
    let before_projection = (functolib.tuple2_decode(_operator_decode, (functolib.tuple2_decode(token_id_decode, token_id_decode))))(arg);
    return {
        address: (before_projection[0]),
        amount: (before_projection[1][0]),
        token_id: (before_projection[1][1])
    }

}
let burn_micheline = { prim: 'Pair', args: [(_operator_micheline), { prim: 'Pair', args: [(token_id_micheline), (token_id_micheline)] }] }
export function burn_generator(): burn { return ({ token_id: token_id_generator(), amount: token_id_generator(), address: _operator_generator() }) }

export async function call_burn(tk: TezosToolkit,
    burn_kt1: string,
    param: burn, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = burn_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, burn_kt1, 'burn', res, amount, debug);
}
export async function assert_failwith_str_burn(tk: TezosToolkit,
    burn_kt1: string,
    param: burn, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = burn_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, burn_kt1, 'burn', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type type9 = { owner: _operator, token_id: token_id }
export function type9_encode(arg: type9): MichelsonV1Expression {
    return { prim: 'Pair', args: [(_operator_encode(arg.owner)), (token_id_encode(arg.token_id))] }
}
export function type9_decode(arg: MichelsonV1Expression): type9 {
    let before_projection = (functolib.tuple2_decode(_operator_decode, token_id_decode))(arg);
    return {
        owner: (before_projection[0]),
        token_id: (before_projection[1])
    }

}
let type9_micheline = { prim: 'Pair', args: [(_operator_micheline), (token_id_micheline)] }
export function type9_generator(): type9 { return ({ owner: _operator_generator(), token_id: token_id_generator() }) }

export type balance_of = { requests: type9[], callback: functolib.contract }
export function balance_of_encode(arg: balance_of): MichelsonV1Expression {
    return { prim: 'Pair', args: [(functolib.list_encode(type9_encode)(arg.requests)), (functolib.contract_encode(new Parser().parseJSON({ "prim": "list", "args": [{ "prim": "pair", "args": [{ "prim": "pair", "args": [{ "prim": "address", "annots": ["%owner"] }, { "prim": "nat", "annots": ["%token_id"] }], "annots": ["%request"] }, { "prim": "nat", "annots": ["%balance"] }] }] }))(arg.callback))] }
}
export function balance_of_decode(arg: MichelsonV1Expression): balance_of {
    let before_projection = (functolib.tuple2_decode(functolib.list_decode(type9_decode), functolib.contract_decode(new Parser().parseJSON({ "prim": "list", "args": [{ "prim": "pair", "args": [{ "prim": "pair", "args": [{ "prim": "address", "annots": ["%owner"] }, { "prim": "nat", "annots": ["%token_id"] }], "annots": ["%request"] }, { "prim": "nat", "annots": ["%balance"] }] }] }))))(arg);
    return {
        requests: (before_projection[0]),
        callback: (before_projection[1])
    }

}
let balance_of_micheline = { prim: 'Pair', args: [((functolib.list_micheline(type9_micheline))), ((functolib.contract_micheline(new Parser().parseJSON({ "prim": "list", "args": [{ "prim": "pair", "args": [{ "prim": "pair", "args": [{ "prim": "address", "annots": ["%owner"] }, { "prim": "nat", "annots": ["%token_id"] }], "annots": ["%request"] }, { "prim": "nat", "annots": ["%balance"] }] }] }))))] }
export function balance_of_generator(): balance_of { return ({ requests: (functolib.list_generator(type9_generator))(), callback: functolib.contract_generator() }) }

export async function call_balance_of(tk: TezosToolkit,
    balance_of_kt1: string,
    param: balance_of, amount?: number, prefix = "", debug = false): Promise<functolib.operation_result> {
    let res = balance_of_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    return functolib.send(tk, balance_of_kt1, 'balance_of', res, amount, debug);
}
export async function assert_failwith_str_balance_of(tk: TezosToolkit,
    balance_of_kt1: string,
    param: balance_of, expected: string, prefix: string, msg: string, amount?: number): Promise<functolib.operation_result> {
    let param1 = balance_of_encode(param);
    //console.log(`res: ${JSONbig.stringify(res,null,2)}`);
    var res = await functolib.send(tk, balance_of_kt1, 'balance_of', param1, amount);
    //console.log(JSONbig.stringify(res))
    if (res.hash == null) {
        //console.log("hash is null!")
        res.error.errors.forEach(element => {
            //console.log(`element is ${JSONbig.stringify(element)}`)
            if (element.with != null) {
                var actual_fail = element.with.string
                if (actual_fail == expected) {
                    console.log(`${prefix}[assert_failwith] got ${expected} as expected`.padEnd(80) + "Test Success!")
                    return res
                }
                else {
                    console.log(`${prefix}[assert_failwith]${msg} got ${JSONbig.stringify(actual_fail)} instead of ${expected}`.padEnd(80) + "Test Failed!")
                    return res
                }
            }
            //console.log(`${prefix}element ${element} is null!`)
        });
    }
    return res
}

export type _storage = { paused: set_pause, operators: functolib.big_map<add_operator, payout_balance>, metadata: functolib.big_map<string, functolib.bytes>, ledger: functolib.big_map<[_operator, token_id], token_id>, administrator: _operator, all_tokens: token_id, payout: _operator, token_metadata: functolib.big_map<token_id, type3> }
export function _storage_encode(arg: _storage): MichelsonV1Expression {
    return { prim: 'Pair', args: [{ prim: 'Pair', args: [{ prim: 'Pair', args: [(_operator_encode(arg.administrator)), (token_id_encode(arg.all_tokens))] }, { prim: 'Pair', args: [(functolib.big_map_encode(functolib.tuple2_encode(_operator_encode, token_id_encode), token_id_encode)(arg.ledger)), (functolib.big_map_encode(functolib.string_encode, functolib.bytes_encode)(arg.metadata))] }] }, { prim: 'Pair', args: [{ prim: 'Pair', args: [(functolib.big_map_encode(add_operator_encode, payout_balance_encode)(arg.operators)), (set_pause_encode(arg.paused))] }, { prim: 'Pair', args: [(_operator_encode(arg.payout)), (functolib.big_map_encode(token_id_encode, type3_encode)(arg.token_metadata))] }] }] }
}
export function _storage_decode(arg: MichelsonV1Expression): _storage {
    let before_projection = (functolib.tuple2_decode((functolib.tuple2_decode((functolib.tuple2_decode(_operator_decode, token_id_decode)), (functolib.tuple2_decode(functolib.big_map_decode(functolib.tuple2_decode(_operator_decode, token_id_decode), token_id_decode), functolib.big_map_decode(functolib.string_decode, functolib.bytes_decode))))), (functolib.tuple2_decode((functolib.tuple2_decode(functolib.big_map_decode(add_operator_decode, payout_balance_decode), set_pause_decode)), (functolib.tuple2_decode(_operator_decode, functolib.big_map_decode(token_id_decode, type3_decode)))))))(arg);
    return {
        administrator: (before_projection[0][0][0]),
        all_tokens: (before_projection[0][0][1]),
        ledger: (before_projection[0][1][0]),
        metadata: (before_projection[0][1][1]),
        operators: (before_projection[1][0][0]),
        paused: (before_projection[1][0][1]),
        payout: (before_projection[1][1][0]),
        token_metadata: (before_projection[1][1][1])
    }

}
let _storage_micheline = {
    prim: 'Pair', args: [{
        prim: 'Pair', args: [{ prim: 'Pair', args: [(_operator_micheline), (token_id_micheline)] }, {
            prim: 'Pair', args: [(functolib.big_map_micheline(functolib.tuple2_micheline(_operator_micheline,
                token_id_micheline), token_id_micheline)), (functolib.big_map_micheline(functolib.string_micheline, functolib.bytes_micheline))]
        }]
    }, { prim: 'Pair', args: [{ prim: 'Pair', args: [(functolib.big_map_micheline(add_operator_micheline, payout_balance_micheline)), (set_pause_micheline)] }, { prim: 'Pair', args: [(_operator_micheline), (functolib.big_map_micheline(token_id_micheline, type3_micheline))] }] }]
}
export function _storage_generator(): _storage {
    return ({
        paused: set_pause_generator(), operators: (functolib.big_map_generator(add_operator_generator, payout_balance_generator))(), metadata: (functolib.big_map_generator(functolib.string_generator, functolib.bytes_generator))(), ledger: (functolib.big_map_generator((functolib.tuple2_generator(_operator_generator,
            token_id_generator)), token_id_generator))(), administrator: _operator_generator(), all_tokens: token_id_generator(), payout: _operator_generator(), token_metadata: (functolib.big_map_generator(token_id_generator, type3_generator))()
    })
}



async function deploy_fa2_x_raw(
    tezosKit: TezosToolkit,
    storage: MichelsonV1Expression,
    config: any,
    amount = 0,
    prefix = "",
    debug = false): Promise<string> {
    //console.log("[deploy_fa2_x_raw] Deploying new fa2_x smart contract");
    try {
        if (debug) { console.log(`fa2_x initial storage ${JSON.stringify(storage)}`) }
        const client = new RpcClient(config.node_addr);
        var b = await client.getBlock();
        let origination_op = await tezosKit.contract
            .originate({
                code: fa2_x_code,
                init: storage,
                balance: amount.toString(), // will probably change at some point
                mutez: true
            })
        //console.log(`Waiting for confirmation of origination for ${origination_op.contractAddress}...`);
        var contract = await origination_op.contract();
        var deploy_operation_result: functolib.operation_result = { level: origination_op.includedInBlock, hash: origination_op.hash, error: origination_op.errors }
        await functolib.wait_inclusion(deploy_operation_result, config, debug)
        //console.log(`Origination completed.`);
        return contract.address

    } catch (error) {
        console.log(`${prefix}ERROR in deploy _storage: ${JSON.stringify(error)}`)
        throw error
    }
}


export async function deploy_fa2_x(
    tezosKit: TezosToolkit,
    storage: _storage,
    config: any,
    amount = 0,
    prefix = "",
    debug = false): Promise<string> {
    let kt1_address = await deploy_fa2_x_raw(tezosKit, _storage_encode(storage), config, amount, prefix, debug);
    return kt1_address;
}

export type Storage_type = _storage
export let Storage_type_encode = _storage_encode
export let Storage_type_decode = _storage_decode




const p = new Parser();
export let initial_blockchain_storage: _storage = {
    administrator: "KT1CK9RnWZGnejBeT6gJfgvf4p7f1NwhP9wS",
    all_tokens: BigInt("9964"),
    ledger: { kind: 'abstract', value: BigInt("196857") },
    metadata: { kind: 'abstract', value: BigInt("196858") },
    operators: { kind: 'abstract', value: BigInt("196859") },
    paused: false,
    payout: "tz1aqMiWgnFddGZSTsEMSe8qbXkVGn7C4cg5",
    token_metadata: { kind: 'abstract', value: BigInt("196860") }
}
/*
 {"prim":"Pair","args":[{"prim":"Pair","args":[{"prim":"Pair","args":[{"string":"KT1CK9RnWZGnejBeT6gJfgvf4p7f1NwhP9wS"},{"int":"9964"}]},{"int":"196857"},{"int":"196858"}]},{"prim":"Pair","args":[{"int":"196859"},{"prim":"False"}]},{"string":"tz1aqMiWgnFddGZSTsEMSe8qbXkVGn7C4cg5"},{"int":"196860"}]}
*/
