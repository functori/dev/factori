open Fa2_y_code
open Factori_types
open Tzfunc.Proto

type token_id = nat

let token_id_encode : token_id -> micheline = nat_encode

let token_id_decode (m : micheline) : token_id = nat_decode m

let token_id_micheline = nat_micheline

let token_id_generator () = nat_generator ()

type _operator = address

let _operator_encode : _operator -> micheline = address_encode

let _operator_decode (m : micheline) : _operator = address_decode m

let _operator_micheline = address_micheline

let _operator_generator () = address_generator ()

type add_operator = {
  token_id : token_id;
  _operator : _operator;
  owner : _operator;
}

let add_operator_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          _operator_encode arg.owner;
          Mprim
            {
              prim = `Pair;
              args =
                [_operator_encode arg._operator; token_id_encode arg.token_id];
              annots = [];
            };
        ];
      annots = [];
    }

let add_operator_decode (m : micheline) : add_operator =
  let owner, (_operator, token_id) =
    (tuple2_decode
       _operator_decode
       (tuple2_decode _operator_decode token_id_decode))
      m in
  {token_id : token_id; _operator : _operator; owner : _operator}

let add_operator_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          _operator_micheline;
          Mprim
            {
              prim = `pair;
              args = [_operator_micheline; token_id_micheline];
              annots = [];
            };
        ];
      annots = [];
    }

let add_operator_generator () =
  {
    token_id = token_id_generator ();
    _operator = _operator_generator ();
    owner = _operator_generator ();
  }

type type0 =
  | Add_operator of add_operator
  | Remove_operator of add_operator

let type0_encode arg =
  match arg with
  | Add_operator x ->
    Mprim {prim = `Left; args = [add_operator_encode x]; annots = []}
  | Remove_operator x ->
    Mprim {prim = `Right; args = [add_operator_encode x]; annots = []}

let type0_decode = function
  | Mprim {prim = `Left; args = [x]; annots = []} when is_leaf x ->
    Add_operator (add_operator_decode x)
  | Mprim {prim = `Right; args = [x]; annots = []} when is_leaf x ->
    Remove_operator (add_operator_decode x)
  | _ -> failwith "unknown primitive in output_sumtype_decode"

let type0_micheline =
  Mprim
    {
      prim = `or_;
      args =
        [
          mich_annot ~annot:"add_operator" add_operator_micheline;
          mich_annot ~annot:"remove_operator" add_operator_micheline;
        ];
      annots = [];
    }

let type0_generator =
  chooseFrom
    [
      (fun () -> Add_operator (add_operator_generator ()));
      (fun () -> Remove_operator (add_operator_generator ()));
    ]

type update_operators = type0 list

let update_operators_encode (arg : update_operators) : micheline =
  (list_encode type0_encode) arg

let update_operators_decode = list_decode type0_decode

let update_operators_micheline = list_micheline type0_micheline

let update_operators_generator () = (list_generator type0_generator) ()

let call_update_operators ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : update_operators) =
  let param =
    {
      entrypoint = EPnamed "update_operators";
      value = Micheline (update_operators_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_update_operators ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : update_operators) =
  let param =
    {
      entrypoint = EPnamed "update_operators";
      value = Micheline (update_operators_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_update_operators ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_update_operators ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_update_operators ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_update_operators ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_update_operators ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_update_operators ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_update_operators ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"update_operators"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (update_operators_encode param))

type type1 = {
  amount : token_id;
  token_id : token_id;
  to_ : _operator;
}

let type1_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          _operator_encode arg.to_;
          Mprim
            {
              prim = `Pair;
              args = [token_id_encode arg.token_id; token_id_encode arg.amount];
              annots = [];
            };
        ];
      annots = [];
    }

let type1_decode (m : micheline) : type1 =
  let to_, (token_id, amount) =
    (tuple2_decode
       _operator_decode
       (tuple2_decode token_id_decode token_id_decode))
      m in
  {amount : token_id; token_id : token_id; to_ : _operator}

let type1_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          _operator_micheline;
          Mprim
            {
              prim = `pair;
              args = [token_id_micheline; token_id_micheline];
              annots = [];
            };
        ];
      annots = [];
    }

let type1_generator () =
  {
    amount = token_id_generator ();
    token_id = token_id_generator ();
    to_ = _operator_generator ();
  }

type type2 = {
  from_ : _operator;
  txs : type1 list;
}

let type2_encode arg =
  Mprim
    {
      prim = `Pair;
      args = [_operator_encode arg.from_; (list_encode type1_encode) arg.txs];
      annots = [];
    }

let type2_decode (m : micheline) : type2 =
  let from_, txs =
    (tuple2_decode _operator_decode (list_decode type1_decode)) m in
  {from_ : _operator; txs : type1 list}

let type2_micheline =
  Mprim
    {
      prim = `pair;
      args = [_operator_micheline; list_micheline type1_micheline];
      annots = [];
    }

let type2_generator () =
  {from_ = _operator_generator (); txs = (list_generator type1_generator) ()}

type transfer = type2 list

let transfer_encode (arg : transfer) : micheline =
  (list_encode type2_encode) arg

let transfer_decode = list_decode type2_decode

let transfer_micheline = list_micheline type2_micheline

let transfer_generator () = (list_generator type2_generator) ()

let call_transfer ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : transfer) =
  let param =
    {entrypoint = EPnamed "transfer"; value = Micheline (transfer_encode param)}
  in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_transfer ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : transfer) =
  let param =
    {entrypoint = EPnamed "transfer"; value = Micheline (transfer_encode param)}
  in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_transfer ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_transfer ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_transfer ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_transfer ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_transfer ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_transfer ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_transfer ~assert_success ~msg ~scenario ~from ~amount ~kt1 ~network
    ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"transfer"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (transfer_encode param))

type type3 = {
  token_id : token_id;
  token_info : (string, bytes) map;
}

let type3_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          token_id_encode arg.token_id;
          (map_encode string_encode bytes_encode) arg.token_info;
        ];
      annots = [];
    }

let type3_decode (m : micheline) : type3 =
  let token_id, token_info =
    (tuple2_decode token_id_decode (map_decode string_decode bytes_decode)) m
  in
  {token_id : token_id; token_info : (string, bytes) map}

let type3_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [token_id_micheline; map_micheline string_micheline bytes_micheline];
      annots = [];
    }

let type3_generator () =
  {
    token_id = token_id_generator ();
    token_info = (map_generator string_generator bytes_generator) ();
  }

type token_metadata = {
  token_ids : token_id list;
  handler : Factori_types.lambda;
}

let token_metadata_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          (list_encode token_id_encode) arg.token_ids;
          (lambda_encode (list_encode type3_encode) unit_encode) arg.handler;
        ];
      annots = [];
    }

let token_metadata_decode (m : micheline) : token_metadata =
  let token_ids, handler =
    (tuple2_decode
       (list_decode token_id_decode)
       (lambda_decode (list_decode type3_decode) unit_decode))
      m in
  {token_ids : token_id list; handler : Factori_types.lambda}

let token_metadata_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          list_micheline token_id_micheline;
          lambda_micheline (list_micheline type3_micheline) unit_micheline;
        ];
      annots = [];
    }

let token_metadata_generator () =
  {
    token_ids = (list_generator token_id_generator) ();
    handler =
      (lambda_generator (list_generator type3_generator) unit_generator) ();
  }

let call_token_metadata ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : token_metadata) =
  let param =
    {
      entrypoint = EPnamed "token_metadata";
      value = Micheline (token_metadata_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_token_metadata ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : token_metadata) =
  let param =
    {
      entrypoint = EPnamed "token_metadata";
      value = Micheline (token_metadata_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_token_metadata ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_token_metadata ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_token_metadata ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_token_metadata ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_token_metadata ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_token_metadata ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_token_metadata ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"token_metadata"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (token_metadata_encode param))

type set_pause = bool

let set_pause_encode : set_pause -> micheline = bool_encode

let set_pause_decode (m : micheline) : set_pause = bool_decode m

let set_pause_micheline = bool_micheline

let set_pause_generator () = bool_generator ()

let call_set_pause ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_pause) =
  let param =
    {
      entrypoint = EPnamed "set_pause";
      value = Micheline (set_pause_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_set_pause ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_pause) =
  let param =
    {
      entrypoint = EPnamed "set_pause";
      value = Micheline (set_pause_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_set_pause ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_set_pause ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_set_pause ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_set_pause ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_set_pause ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_set_pause ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_set_pause ~assert_success ~msg ~scenario ~from ~amount ~kt1 ~network
    ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_pause"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (set_pause_encode param))

type set_administrator = _operator

let set_administrator_encode : set_administrator -> micheline = _operator_encode

let set_administrator_decode = _operator_decode

let set_administrator_micheline = _operator_micheline

let set_administrator_generator () = _operator_generator ()

let call_set_administrator ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_administrator) =
  let param =
    {
      entrypoint = EPnamed "set_administrator";
      value = Micheline (set_administrator_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_set_administrator ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : set_administrator) =
  let param =
    {
      entrypoint = EPnamed "set_administrator";
      value = Micheline (set_administrator_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_set_administrator ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_set_administrator ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_set_administrator ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_set_administrator ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_set_administrator ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_set_administrator ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_set_administrator ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_administrator"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (set_administrator_encode param))

type payout_balance = unit

let payout_balance_encode : payout_balance -> micheline = unit_encode

let payout_balance_decode (m : micheline) : payout_balance = unit_decode m

let payout_balance_micheline = unit_micheline

let payout_balance_generator () = unit_generator ()

let call_payout_balance ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : payout_balance) =
  let param =
    {
      entrypoint = EPnamed "payout_balance";
      value = Micheline (payout_balance_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_payout_balance ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : payout_balance) =
  let param =
    {
      entrypoint = EPnamed "payout_balance";
      value = Micheline (payout_balance_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_payout_balance ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_payout_balance ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_payout_balance ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_payout_balance ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_payout_balance ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_payout_balance ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_payout_balance ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"payout_balance"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (payout_balance_encode param))

type mint = {
  token_info : (string, bytes) map;
  token_id : token_id;
  address : _operator;
  amount : token_id;
}

let mint_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          Mprim
            {
              prim = `Pair;
              args = [_operator_encode arg.address; token_id_encode arg.amount];
              annots = [];
            };
          Mprim
            {
              prim = `Pair;
              args =
                [
                  token_id_encode arg.token_id;
                  (map_encode string_encode bytes_encode) arg.token_info;
                ];
              annots = [];
            };
        ];
      annots = [];
    }

let mint_decode (m : micheline) : mint =
  let (address, amount), (token_id, token_info) =
    (tuple2_decode
       (tuple2_decode _operator_decode token_id_decode)
       (tuple2_decode token_id_decode (map_decode string_decode bytes_decode)))
      m in
  {
    token_info : (string, bytes) map;
    token_id : token_id;
    address : _operator;
    amount : token_id;
  }

let mint_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          Mprim
            {
              prim = `pair;
              args = [_operator_micheline; token_id_micheline];
              annots = [];
            };
          Mprim
            {
              prim = `pair;
              args =
                [
                  token_id_micheline;
                  map_micheline string_micheline bytes_micheline;
                ];
              annots = [];
            };
        ];
      annots = [];
    }

let mint_generator () =
  {
    token_info = (map_generator string_generator bytes_generator) ();
    token_id = token_id_generator ();
    address = _operator_generator ();
    amount = token_id_generator ();
  }

let call_mint ?(node = Blockchain.default_node) ?(debug = false) ?(amount = 0L)
    ?(fee = -1L) ?(gas_limit = Z.minus_one) ?(storage_limit = Z.minus_one) ~from
    ~kt1 (param : mint) =
  let param =
    {entrypoint = EPnamed "mint"; value = Micheline (mint_encode param)} in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_mint ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : mint) =
  let param =
    {entrypoint = EPnamed "mint"; value = Micheline (mint_encode param)} in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_mint ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_mint ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_mint ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_mint ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_mint ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_mint ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_mint ~assert_success ~msg ~scenario ~from ~amount ~kt1 ~network
    ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"mint"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (mint_encode param))

type burn = {
  token_id : token_id;
  amount : token_id;
  address : _operator;
}

let burn_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          _operator_encode arg.address;
          Mprim
            {
              prim = `Pair;
              args = [token_id_encode arg.amount; token_id_encode arg.token_id];
              annots = [];
            };
        ];
      annots = [];
    }

let burn_decode (m : micheline) : burn =
  let address, (amount, token_id) =
    (tuple2_decode
       _operator_decode
       (tuple2_decode token_id_decode token_id_decode))
      m in
  {token_id : token_id; amount : token_id; address : _operator}

let burn_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          _operator_micheline;
          Mprim
            {
              prim = `pair;
              args = [token_id_micheline; token_id_micheline];
              annots = [];
            };
        ];
      annots = [];
    }

let burn_generator () =
  {
    token_id = token_id_generator ();
    amount = token_id_generator ();
    address = _operator_generator ();
  }

let call_burn ?(node = Blockchain.default_node) ?(debug = false) ?(amount = 0L)
    ?(fee = -1L) ?(gas_limit = Z.minus_one) ?(storage_limit = Z.minus_one) ~from
    ~kt1 (param : burn) =
  let param =
    {entrypoint = EPnamed "burn"; value = Micheline (burn_encode param)} in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_burn ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : burn) =
  let param =
    {entrypoint = EPnamed "burn"; value = Micheline (burn_encode param)} in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_burn ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_burn ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_burn ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_burn ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_burn ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_burn ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_burn ~assert_success ~msg ~scenario ~from ~amount ~kt1 ~network
    ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"burn"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (burn_encode param))

type type9 = {
  owner : _operator;
  token_id : token_id;
}

let type9_encode arg =
  Mprim
    {
      prim = `Pair;
      args = [_operator_encode arg.owner; token_id_encode arg.token_id];
      annots = [];
    }

let type9_decode (m : micheline) : type9 =
  let owner, token_id = (tuple2_decode _operator_decode token_id_decode) m in
  {owner : _operator; token_id : token_id}

let type9_micheline =
  Mprim
    {
      prim = `pair;
      args = [_operator_micheline; token_id_micheline];
      annots = [];
    }

let type9_generator () =
  {owner = _operator_generator (); token_id = token_id_generator ()}

type balance_of = {
  requests : type9 list;
  callback : contract;
}

let balance_of_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          (list_encode type9_encode) arg.requests;
          (contract_encode
             (EzEncoding.destruct
                micheline_enc.json
                {|{"prim":"list","args":[{"prim":"pair","args":[{"prim":"pair","args":[{"prim":"address","annots":["%owner"]},{"prim":"nat","annots":["%token_id"]}],"annots":["%request"]},{"prim":"nat","annots":["%balance"]}]}]}|}))
            arg.callback;
        ];
      annots = [];
    }

let balance_of_decode (m : micheline) : balance_of =
  let requests, callback =
    (tuple2_decode
       (list_decode type9_decode)
       (contract_decode
          (EzEncoding.destruct
             micheline_enc.json
             {|{"prim":"list","args":[{"prim":"pair","args":[{"prim":"pair","args":[{"prim":"address","annots":["%owner"]},{"prim":"nat","annots":["%token_id"]}],"annots":["%request"]},{"prim":"nat","annots":["%balance"]}]}]}|})))
      m in
  {requests : type9 list; callback : contract}

let balance_of_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          list_micheline type9_micheline;
          contract_micheline
            (EzEncoding.destruct
               micheline_enc.json
               {|{"prim":"list","args":[{"prim":"pair","args":[{"prim":"pair","args":[{"prim":"address","annots":["%owner"]},{"prim":"nat","annots":["%token_id"]}],"annots":["%request"]},{"prim":"nat","annots":["%balance"]}]}]}|});
        ];
      annots = [];
    }

let balance_of_generator () =
  {
    requests = (list_generator type9_generator) ();
    callback = contract_generator ();
  }

let call_balance_of ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : balance_of) =
  let param =
    {
      entrypoint = EPnamed "balance_of";
      value = Micheline (balance_of_encode param);
    } in
  Blockchain.call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let forge_call_balance_of ?(node = Blockchain.default_node) ?(debug = false)
    ?(amount = 0L) ?(fee = -1L) ?(gas_limit = Z.minus_one)
    ?(storage_limit = Z.minus_one) ~from ~kt1 (param : balance_of) =
  let param =
    {
      entrypoint = EPnamed "balance_of";
      value = Micheline (balance_of_encode param);
    } in
  Blockchain.forge_call_entrypoint
    ~debug
    ~node
    ~amount
    ~fee
    ~gas_limit
    ~storage_limit
    ~from
    ~dst:kt1
    param

let assert_failwith_str_balance_of ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str
    ~expected
    ~prefix
    ~msg
    (forge_call_balance_of ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_str_list_balance_of ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~from ~kt1 ~expected ~prefix ~msg param =
  Blockchain.assert_failwith_str_list
    ~expected
    ~prefix
    ~msg
    (forge_call_balance_of ~debug ~node ~amount ~from ~kt1)
    param

let assert_failwith_generic_balance_of ?(node = Blockchain.default_node)
    ?(debug = false) ?(amount = 0L) ~enc ~from ~kt1 ~expected ~prefix ~msg param
    =
  Blockchain.assert_failwith_generic
    ~enc
    ~expected
    ~prefix
    ~msg
    (forge_call_balance_of ~debug ~node ~amount ~from ~kt1)
    param

let mk_call_balance_of ~assert_success ~msg ~scenario ~from ~amount ~kt1
    ~network ~param =
  let open Scenario_dsl.AstInterface in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"balance_of"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:(mk_micheline scenario (balance_of_encode param))

type _storage = {
  paused : set_pause;
  operators : (add_operator, payout_balance) big_map;
  metadata : (string, bytes) big_map;
  ledger : (_operator * token_id, token_id) big_map;
  administrator : _operator;
  all_tokens : token_id;
  payout : _operator;
  token_metadata : (token_id, type3) big_map;
}

let _storage_encode arg =
  Mprim
    {
      prim = `Pair;
      args =
        [
          Mprim
            {
              prim = `Pair;
              args =
                [
                  Mprim
                    {
                      prim = `Pair;
                      args =
                        [
                          _operator_encode arg.administrator;
                          token_id_encode arg.all_tokens;
                        ];
                      annots = [];
                    };
                  Mprim
                    {
                      prim = `Pair;
                      args =
                        [
                          (big_map_encode
                             (tuple2_encode _operator_encode token_id_encode)
                             token_id_encode)
                            arg.ledger;
                          (big_map_encode string_encode bytes_encode)
                            arg.metadata;
                        ];
                      annots = [];
                    };
                ];
              annots = [];
            };
          Mprim
            {
              prim = `Pair;
              args =
                [
                  Mprim
                    {
                      prim = `Pair;
                      args =
                        [
                          (big_map_encode
                             add_operator_encode
                             payout_balance_encode)
                            arg.operators;
                          set_pause_encode arg.paused;
                        ];
                      annots = [];
                    };
                  Mprim
                    {
                      prim = `Pair;
                      args =
                        [
                          _operator_encode arg.payout;
                          (big_map_encode token_id_encode type3_encode)
                            arg.token_metadata;
                        ];
                      annots = [];
                    };
                ];
              annots = [];
            };
        ];
      annots = [];
    }

let _storage_decode (m : micheline) : _storage =
  let ( ((administrator, all_tokens), (ledger, metadata)),
        ((operators, paused), (payout, token_metadata)) ) =
    (tuple2_decode
       (tuple2_decode
          (tuple2_decode _operator_decode token_id_decode)
          (tuple2_decode
             (big_map_decode
                (tuple2_decode _operator_decode token_id_decode)
                token_id_decode)
             (big_map_decode string_decode bytes_decode)))
       (tuple2_decode
          (tuple2_decode
             (big_map_decode add_operator_decode payout_balance_decode)
             set_pause_decode)
          (tuple2_decode
             _operator_decode
             (big_map_decode token_id_decode type3_decode))))
      m in
  {
    paused : set_pause;
    operators : (add_operator, payout_balance) big_map;
    metadata : (string, bytes) big_map;
    ledger : (_operator * token_id, token_id) big_map;
    administrator : _operator;
    all_tokens : token_id;
    payout : _operator;
    token_metadata : (token_id, type3) big_map;
  }

let _storage_micheline =
  Mprim
    {
      prim = `pair;
      args =
        [
          Mprim
            {
              prim = `pair;
              args =
                [
                  Mprim
                    {
                      prim = `pair;
                      args = [_operator_micheline; token_id_micheline];
                      annots = [];
                    };
                  Mprim
                    {
                      prim = `pair;
                      args =
                        [
                          big_map_micheline
                            (tuple2_micheline
                               _operator_micheline
                               token_id_micheline)
                            token_id_micheline;
                          big_map_micheline string_micheline bytes_micheline;
                        ];
                      annots = [];
                    };
                ];
              annots = [];
            };
          Mprim
            {
              prim = `pair;
              args =
                [
                  Mprim
                    {
                      prim = `pair;
                      args =
                        [
                          big_map_micheline
                            add_operator_micheline
                            payout_balance_micheline;
                          set_pause_micheline;
                        ];
                      annots = [];
                    };
                  Mprim
                    {
                      prim = `pair;
                      args =
                        [
                          _operator_micheline;
                          big_map_micheline token_id_micheline type3_micheline;
                        ];
                      annots = [];
                    };
                ];
              annots = [];
            };
        ];
      annots = [];
    }

let _storage_generator () =
  {
    paused = set_pause_generator ();
    operators =
      (big_map_generator add_operator_generator payout_balance_generator) ();
    metadata = (big_map_generator string_generator bytes_generator) ();
    ledger =
      (big_map_generator
         (tuple2_generator _operator_generator token_id_generator)
         token_id_generator)
        ();
    administrator = _operator_generator ();
    all_tokens = token_id_generator ();
    payout = _operator_generator ();
    token_metadata = (big_map_generator token_id_generator type3_generator) ();
  }

let deploy ?(amount = 0L) ?(node = "https://tz.functori.com")
    ?(name = "No name provided") ?(from = Blockchain.bootstrap1) storage =
  let storage = _storage_encode storage in
  Blockchain.deploy ~amount ~node ~name ~from ~code (Micheline storage)

let mk_deploy scenario ~from ~amount ~network ~storage =
  let open Scenario_dsl.AstInterface in
  mk_deploy
    ~scenario
    ~from
    ~amount
    ~network
    ~storage:(mk_micheline scenario (_storage_encode storage))
    ~contract_name:"fa2_y"

let test_storage_download ~kt1 ~node () =
  let open Tzfunc.Rp in
  let open Blockchain in
  Lwt_main.run
  @@ let>? storage =
       get_storage ~node ~debug:(!Factori_types.debug > 0) kt1 _storage_decode
     in
     let storage_reencoded = _storage_encode storage in
     Lwt.return_ok @@ Factori_types.output_debug
     @@ Format.asprintf
          "Done downloading storage: %s."
          (Ezjsonm_interface.to_string
             (Json_encoding.construct micheline_enc.json storage_reencoded))

let initial_blockchain_storage =
  {
    administrator = "KT1CK9RnWZGnejBeT6gJfgvf4p7f1NwhP9wS";
    all_tokens = Z.of_string "9964";
    ledger = Abstract (Z.of_string "196857");
    metadata = Abstract (Z.of_string "196858");
    operators = Abstract (Z.of_string "196859");
    paused = false;
    payout = "tz1aqMiWgnFddGZSTsEMSe8qbXkVGn7C4cg5";
    token_metadata = Abstract (Z.of_string "196860");
  }
(*
 {"prim":"Pair","args":[{"prim":"Pair","args":[{"prim":"Pair","args":[{"string":"KT1CK9RnWZGnejBeT6gJfgvf4p7f1NwhP9wS"},{"int":"9964"}]},{"int":"196857"},{"int":"196858"}]},{"prim":"Pair","args":[{"int":"196859"},{"prim":"False"}]},{"string":"tz1aqMiWgnFddGZSTsEMSe8qbXkVGn7C4cg5"},{"int":"196860"}]}
*)
