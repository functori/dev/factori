(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains various modules and functions that assist with the
static analysis of Tezos smart contracts, including sets of chains, labels,
flows, and blocks, as well as functions for analyzing control nodes, Michelson
nodes, and lambdas in the code. There are also utility functions for
manipulating and accessing analysis data. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Unstack_micheline
open Tzfunc.Proto
open Unstack_iterator
open Factori_representation
open Factori_micheline_lib
(*
open Factori_errors*)

type chain_call = label list

module ChainSet = Set.Make (struct
  type t = chain_call

  let compare = compare
end)

module LabelSet = Set.Make (struct
  type t = label

  let compare = compare
end)

let label_in_node acc node =
  match node with
  | N_SIMPLE (_, _, _, _, label)
  | N_END (_, label)
  | N_CONCAT (_, _, _, label) (* MULTIPLE DEF *)
  | N_CONCAT_LIST (_, _, label)
  | N_SELF (_, _, label)
  | N_CONTRACT (_, _, _, label)
  | N_CREATE_CONTRACT (_, _, _, _, _, _, label)
  | N_NEVER (_, label)
  | N_FAILWITH (_, label)
  | N_CALL_VIEW (_, _, _, _, label)
  | FAIL_CONTROL (_, label)
  | N_START (_, _, label) -> LabelSet.add label acc
  | N_EXEC (_, _, _, lc, lr) -> LabelSet.add lc (LabelSet.add lr acc)
  | N_LAMBDA (_, label, _, _, lin, lend) ->
    LabelSet.add label (LabelSet.add lin (LabelSet.add lend acc))
(* TODO *)

let label_in_ctrl_node acc ctrl_node =
  match ctrl_node with
  | N_IF (_, label_then, label_else, _, _) ->
    LabelSet.add label_then (LabelSet.add label_else acc)
  | N_IF_NONE (_, label_then, label_else, _, _, _, lab_pat) ->
    LabelSet.add label_then (LabelSet.add label_else (LabelSet.add lab_pat acc))
  | N_LOOP { label_pat_l = lab_pat; label_loop = label; _ }
  | N_LOOP_LEFT
      {
        (* skipping label in pat_res if we use it in info_node change this function too *)
        pat_lf = _, lab_pat;
        label_lf = label;
        _;
      }
  | N_MAP (_, lab_pat, _, _, label)
  | N_ITER { pat = _, lab_pat; label_iter = label; _ }
  | N_FOLD_MAP { pat_fold_map = _, lab_pat; label_map = label; _ } ->
    LabelSet.add label (LabelSet.add lab_pat acc)
  | N_IF_CONS (_, label_then, label_else, _, _, _, l2, _, l3)
  | N_IF_LEFT (_, label_then, label_else, (_, l2, _), (_, l3, _)) ->
    LabelSet.add label_then
      (LabelSet.add label_else (LabelSet.add l2 (LabelSet.add l3 acc)))

let lab_folder =
  let node_fold fold () acc node =
    let acc = label_in_node acc node in
    default_folder.node_fold fold () acc node in
  let control_node_fold fold () acc node =
    let acc = label_in_ctrl_node acc node in
    default_folder.control_node_fold fold () acc node in
  { default_folder with node_fold; control_node_fold }

(* From a type t we should be able to construct a type t from the micheline type *)
module type T = sig
  type t

  val construct : (?build_name:int -> string) -> micheline -> t

  val deconstruct : t -> micheline

  val name : t -> string option

  val to_string : t -> string

  val name_string_list : t list -> name_arg list

  val match_or : t -> (t * t) option

  val make_or : t -> t -> t

  val simple : name_arg -> t
end

module FlowSet = Set.Make (struct
  type t = label * label

  let compare = compare
end)

module InterFlow = Set.Make (struct
  type t = label * label * label * label

  let compare = compare
end)

let print_flow f =
  FlowSet.iter (fun (l1, l2) -> Format.eprintf "%d -> %d@." l1 l2) f

module type Analysis = sig
  type arg

  type analysis_tools

  val info_smart : arg sc_node -> analysis_tools

  val block_of_label : label -> analysis_tools -> arg block_unstack

  val print_block : arg block_unstack -> unit

  val list_of_blocks : analysis_tools -> arg block_unstack list

  val get_final : analysis_tools -> arg * label

  val get_init : analysis_tools -> arg * label

  val get_flow : analysis_tools -> FlowSet.t

  val get_inter_flow : analysis_tools -> InterFlow.t

  val view_labels : analysis_tools -> (label * label * arg) list

  val find_labels : label -> analysis_tools -> LabelSet.t

  val find_lambda :
    analysis_tools -> chain_call -> name_arg -> arg complete_lambda list

  val get_original : analysis_tools -> arg sc_node

  val all_fail : analysis_tools -> (int list * arg * int) list
end

module Aux : Analysis with type arg = Nested_arg.t = struct
  module A = Nested_arg

  type arg = A.t

  module BlockSet = Set.Make (struct
    type t = A.t block_unstack

    let compare = compare
  end)

  module LambdaChain = Map.Make (struct
    type t = name_arg * label list

    let compare = compare
  end)

  module LambdaName = Map.Make (struct
    type t = A.t

    let compare = compare
  end)

  type analysis_tools = {
    initial : label;
    name_init : A.t;
    final : label;
    name_final : A.t;
    blocks : BlockSet.t;
    flow : FlowSet.t;
    inter_flow : InterFlow.t;
    lambda_map : A.t complete_lambda list LambdaName.t;
    view_labels : (label * label * arg) list;
    original : A.t sc_node;
  }

  let build_flow_exec lab_s lab_e (flow, inter_flow) l =
    match l.lambda_code with
    | None -> (flow, inter_flow)
    | Some f ->
      let flow = FlowSet.add (lab_s, f.label_start) flow in
      let flow = FlowSet.add (f.label_end, lab_e) flow in
      let inter_flow =
        InterFlow.add (lab_s, f.label_start, f.label_end, lab_e) inter_flow
      in
      (flow, inter_flow)

  let annon_lambda left base =
    let miche = A.deconstruct left in
    match miche with
    | Mprim { prim = `lambda; _ } -> (
      let l = { lambda_name = left; lambda_code = None; in_between = [] } in
      match base with
      | None -> Some [l]
      | Some e -> Some (l :: e))
    | _ -> base

  (* In case this is an optionnal, this function choose_block will take advantage of the fact that the function
     name_string_list of Nested_arg return the same result for Option(var_1,var_2) and for (var_1,var_2). It will
     just return all the variables that appears *)
  let choose_block_map ~result_map ~result_body ~end_lab ~ty =
    match Type_primitive.is_option ty with
    | None -> BAnnonymous (result_map, result_body, end_lab)
    | _ -> BReturnAssign (result_map, result_body, end_lab)

  let build_name cpt = Format.sprintf "return_%d" cpt

  let rec info_control_node return_cpt end_lab result lambda_map label_start
      block_set flow (inter_flow : InterFlow.t) node =
    match node with
    | N_IF (arg_bool, label_then, label_else, node_then, node_else) ->
      block_of_if return_cpt end_lab lambda_map result label_start label_then
        label_else arg_bool node_then node_else block_set flow inter_flow
    | N_IF_NONE
        (arg_opt, label_then, label_else, node_none, node_some, _pat, _lab_pat)
      ->
      block_of_if return_cpt end_lab lambda_map result label_start label_then
        label_else arg_opt node_none node_some block_set flow inter_flow
    | N_IF_CONS
        ( arg_list,
          label_then,
          label_else,
          node_cons,
          node_nil,
          _pat_x,
          _lab_x,
          _pat_xs,
          _lab_xs ) ->
      block_of_if return_cpt end_lab lambda_map result label_start label_then
        label_else arg_list node_cons node_nil block_set flow inter_flow
    | N_IF_LEFT
        ( arg_or,
          label_then,
          label_else,
          (_pat_left, _lab_left, node_left),
          (_pat_right, _lab_right, node_right) ) ->
      block_of_if return_cpt end_lab lambda_map result label_start label_then
        label_else arg_or node_left node_right block_set flow inter_flow
    | N_ITER
        {
          arg_iter;
          acc_iter;
          pat_acc_iter;
          pat = pat, label_pat;
          body_iter;
          label_iter;
        } ->
      let flow = FlowSet.add (label_start, label_pat) flow in
      let ty = A.deconstruct arg_iter in
      let block_pat =
        choose_block_map ~result_map:[pat] ~result_body:[arg_iter]
          ~end_lab:label_pat ~ty in
      let block_acc = BReturnAssign (pat_acc_iter, acc_iter, label_iter) in
      let flow = FlowSet.add (label_pat, label_iter) flow in
      let block = BlockSet.add block_pat block_set in
      let block = BlockSet.add block_acc block in
      let new_return_cpt, lambda_map, final_lab, block, flow, inter_flow =
        info_node return_cpt lambda_map result label_iter block flow inter_flow
          body_iter in
      (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
      let block = BlockSet.add (BEnd end_lab) block in
      let flow = FlowSet.add (final_lab, end_lab) flow in
      (new_return_cpt, lambda_map, end_lab, block, flow, inter_flow)
    | N_MAP (arg, label_pat, pat, node, _label) -> (
      match result with
      | [result_map] ->
        let ty_coll = A.deconstruct result_map in
        let ty =
          match Type_primitive.type_result_map ty_coll with
          | Error e -> raise e
          | Ok res -> res in
        let new_return_cpt, new_name = A.construct ~build_name return_cpt ty in
        let ty_arg_coll = A.deconstruct arg in
        let block_pat =
          choose_block_map ~result_map:[pat] ~result_body:[arg]
            ~end_lab:label_pat ~ty:ty_arg_coll in
        let block = BlockSet.add block_pat block_set in
        let flow = FlowSet.add (label_start, label_pat) flow in
        let new_return_cpt, lambda_map, final_lab, block, flow, inter_flow =
          info_node new_return_cpt lambda_map [new_name] label_pat block flow
            inter_flow node in
        (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
        let block_end =
          choose_block_map ~result_map:result ~result_body:[new_name] ~end_lab
            ~ty:ty_coll in
        let block = BlockSet.add block_end block in
        let flow = FlowSet.add (final_lab, end_lab) flow in
        (new_return_cpt, lambda_map, end_lab, block, flow, inter_flow)
      | _ -> failwith "A MAP instruction canno't have a failwith in it's body")
    | N_FOLD_MAP
        {
          arg_fold_map;
          acc_fold_map;
          pat_acc_fold_map = pat_acc_fold_map, label_pat_acc;
          pat_fold_map = pat_fold_map, label_pat;
          body_fold_map;
          label_map = _;
        } -> (
      match result with
      | [] -> failwith "A MAP instruction canno't have a failwith in it's body"
      | new_coll :: acc ->
        let ty_coll = A.deconstruct new_coll in
        let ty =
          match Type_primitive.type_arg_map ty_coll with
          | Error e -> raise e
          | Ok res -> res in
        let new_return_cpt, new_name = A.construct ~build_name return_cpt ty in
        let ty_arg_coll = A.deconstruct arg_fold_map in
        let block_pat =
          choose_block_map ~result_map:[pat_fold_map]
            ~result_body:[arg_fold_map] ~end_lab:label_pat ~ty:ty_arg_coll in
        let block = BlockSet.add block_pat block_set in
        let block =
          BlockSet.add
            (BReturnAssign (pat_acc_fold_map, acc_fold_map, label_pat_acc))
            block in
        let flow = FlowSet.add (label_start, label_pat) flow in
        let flow = FlowSet.add (label_pat, label_pat_acc) flow in
        let new_return_cpt, lambda_map, final_lab, block, flow, inter_flow =
          info_node new_return_cpt lambda_map (new_name :: acc) label_pat_acc
            block flow inter_flow body_fold_map in
        (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
        let block_end =
          choose_block_map ~result_map:[new_coll] ~result_body:[new_name]
            ~end_lab ~ty:ty_coll in
        let block = BlockSet.add block_end block in
        let flow = FlowSet.add (final_lab, end_lab) flow in
        (new_return_cpt, lambda_map, end_lab, block, flow, inter_flow))
    | N_LOOP { arg; acc; pat_acc; body; label_loop; label_pat_l = label_pat } ->
      let block_start = BLoop (arg, result, acc, label_loop) in
      let block = BlockSet.add block_start block_set in
      let flow = FlowSet.add (label_start, label_loop) flow in
      let block = BlockSet.add (BPattern (pat_acc, acc, label_pat)) block in
      let flow = FlowSet.add (label_loop, label_pat) flow in
      let new_return_cpt, lambda_map, final_lab, block, flow, inter_flow =
        info_node return_cpt lambda_map (arg :: result) label_pat block flow
          inter_flow body in
      (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
      let block = BlockSet.add (BEnd end_lab) block in
      let flow = FlowSet.add (final_lab, end_lab) flow in
      (new_return_cpt, lambda_map, end_lab, block, flow, inter_flow)
    | N_LOOP_LEFT
        {
          arg_lf;
          pat_lf = pat_lf, label_pat;
          acc_lf;
          pat_acc_lf;
          node_loop;
          pat_res_lf = _res, _lab;
          label_lf;
        } ->
      let l, r =
        match A.match_or arg_lf with
        | Some res -> res
        | None -> failwith "Arg of Loop left is not a Or type" in
      let return_node =
        match result with
        | right_res :: result -> A.make_or pat_lf right_res :: result
        | _ -> result in
      let flow = FlowSet.add (label_start, label_lf) flow in
      let block_set =
        BlockSet.add (BLoop (arg_lf, result, r :: acc_lf, label_lf)) block_set
      in
      let block =
        BlockSet.add
          (BPattern (pat_lf :: pat_acc_lf, l :: acc_lf, label_pat))
          block_set in
      let flow = FlowSet.add (label_lf, label_pat) flow in
      let new_return_cpt, lambda_map, final_lab, block, flow, inter_flow =
        info_node return_cpt lambda_map return_node label_pat block flow
          inter_flow node_loop in
      (*let flow = FlowSet.add (final_lab, label_pat) flow in*)
      let block = BlockSet.add (BEnd end_lab) block in
      let flow = FlowSet.add (final_lab, end_lab) flow in
      (new_return_cpt, lambda_map, end_lab, block, flow, inter_flow)

  and info_node return_cpt lambda_map name_l previous_stm block_set flow_set
      (inter_flow : InterFlow.t) sub_node_l =
    match sub_node_l with
    | [] ->
      (return_cpt, lambda_map, previous_stm, block_set, flow_set, inter_flow)
    | [N_END (exp_l, label)] ->
      let all_name_l = A.name_string_list name_l in
      let all_exp_l = A.name_string_list exp_l in
      let all_name_l_t = List.map A.simple all_name_l in
      let all_exp_l_t = List.map A.simple all_exp_l in
      let lambda_map =
        try
          List.fold_left2
            (fun acc left right ->
              match LambdaName.find_opt right lambda_map with
              | None -> LambdaName.update left (annon_lambda left) acc
              | Some e ->
                LambdaName.update left
                  (function
                    | None -> Some e
                    | Some arg -> Some (e @ arg))
                  acc)
            lambda_map all_name_l_t all_exp_l_t
        with Invalid_argument _ ->
          raise
            (Factori_errors.GenericError
               ( "Analysis",
                 "Fail analysis due to wrong return in Michelson Unstack" ))
      in
      let flow = FlowSet.add (previous_stm, label) flow_set in
      let block =
        BlockSet.add (BReturnAssign (name_l, exp_l, label)) block_set in
      (return_cpt, lambda_map, label, block, flow, inter_flow)
    | [N_FAILWITH (arg, label)] ->
      let flow = FlowSet.add (previous_stm, label) flow_set in
      let block =
        BlockSet.add
          (BAssign (N_SIMPLE (`FAILWITH, [], [arg], [], label), label))
          block_set in
      (return_cpt, lambda_map, label, block, flow, inter_flow)
    | [N_NEVER (arg, label)] ->
      let flow = FlowSet.add (previous_stm, label) flow_set in
      let block =
        BlockSet.add
          (BAssign (N_SIMPLE (`NEVER, [], [arg], [], label), label))
          block_set in
      (return_cpt, lambda_map, label, block, flow, inter_flow)
    | [FAIL_CONTROL (ctrl_node, label_top)] ->
      info_control_node return_cpt label_top [] lambda_map previous_stm
        block_set flow_set inter_flow ctrl_node
    | x :: xs -> (
      match x with
      | N_LAMBDA (arg, _label_start, node_l, lambda, label_in, label_out) ->
        let new_return_cpt, lambda_map, block, flow, inter_flow =
          lambda_stacking return_cpt label_in label_out arg node_l lambda
            lambda_map block_set flow_set inter_flow in
        info_node new_return_cpt lambda_map name_l previous_stm block flow
          inter_flow xs
      | N_EXEC (arg, lambda, res, label_call, label_ret) -> (
        let flow = FlowSet.add (previous_stm, label_call) flow_set in
        match LambdaName.find_opt lambda lambda_map with
        | None ->
          let block =
            BlockSet.add
              (BAnnonymous ([res], [arg; lambda], label_call))
              block_set in
          info_node return_cpt lambda_map name_l label_call block flow
            inter_flow xs
        | Some lambdas
          when List.for_all (fun l -> Option.is_none l.lambda_code) lambdas ->
          let block =
            BlockSet.add
              (BAnnonymous ([res], [arg; lambda], label_call))
              block_set in
          info_node return_cpt lambda_map name_l label_call block flow
            inter_flow xs
        | Some lambdas ->
          let block =
            BlockSet.add (BStartExec (arg, lambda, label_call)) block_set in
          let block = BlockSet.add (BEndExec (res, lambda, label_ret)) block in
          let some_are_annon =
            List.exists (fun l -> Option.is_none l.lambda_code) lambdas in
          let flow =
            if some_are_annon then
              let flow = FlowSet.add (label_call, label_ret) flow in
              flow
            else
              flow in
          let flow, inter_flow =
            List.fold_left
              (build_flow_exec label_call label_ret)
              (flow, inter_flow) lambdas in
          info_node return_cpt lambda_map name_l label_ret block flow inter_flow
            xs)
      | N_SIMPLE (`APPLY, [], [arg; lambda], [res], label) ->
        (* Peut etre en faire un block dans l'analyse
                     arg : 'a;
           label_start : label;
           label_end : label;
           res : 'a option;
        *)
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block = BlockSet.add (BAssign (x, label)) block_set in
        let lambda_map =
          match LambdaName.find_opt lambda lambda_map with
          | None ->
            let lambda =
              { lambda_name = lambda; lambda_code = None; in_between = [arg] }
            in
            let lambda_map = LambdaName.add res [lambda] lambda_map in
            lambda_map
          | Some e ->
            let lambda_element =
              List.map
                (fun l ->
                  let in_between = arg :: l.in_between in
                  { l with in_between })
                e in
            let lambda_map = LambdaName.add res lambda_element lambda_map in
            lambda_map in
        info_node return_cpt lambda_map name_l previous_stm block flow
          inter_flow xs
      | N_CONCAT (arg1, arg2, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`CONCAT, [], [arg1; arg2], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_CONCAT_LIST (arg, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`CONCAT, [], [arg], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_SELF (_str_opt, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`SELF, [], [], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_CALL_VIEW (_name, arg, addr, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let simple_node = N_SIMPLE (`VIEW, [], [arg; addr], [res], label) in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_SIMPLE (_, _, _, _, label) as simple_node ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block = BlockSet.add (BAssign (simple_node, label)) block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_CONTRACT (_, addr, res, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block =
          BlockSet.add
            (BAssign (N_SIMPLE (`CONTRACT, [], [addr], [res], label), label))
            block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_FAILWITH (arg, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block =
          BlockSet.add
            (BAssign (N_SIMPLE (`FAILWITH, [], [arg], [], label), label))
            block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_NEVER (arg, label) ->
        let flow = FlowSet.add (previous_stm, label) flow_set in
        let block =
          BlockSet.add
            (BAssign (N_SIMPLE (`FAILWITH, [], [arg], [], label), label))
            block_set in
        info_node return_cpt lambda_map name_l label block flow inter_flow xs
      | N_START (result, ctrl_node, end_label) ->
        let new_return_cpt, lambda_map, end_lab, block, flow, inter_flow =
          info_control_node return_cpt end_label result lambda_map previous_stm
            block_set flow_set inter_flow ctrl_node in
        info_node new_return_cpt lambda_map name_l end_lab block flow inter_flow
          xs
      | FAIL_CONTROL (ctrl_node, end_label) ->
        let new_return_cpt, lambda_map, end_lab, block, flow, inter_flow =
          info_control_node return_cpt end_label [] lambda_map previous_stm
            block_set flow_set inter_flow ctrl_node in
        info_node new_return_cpt lambda_map name_l end_lab block flow inter_flow
          xs
      | N_CREATE_CONTRACT (opt_k, mutz, ty2, _contract, ope, addr, lab) ->
        let flow = FlowSet.add (previous_stm, lab) flow_set in
        let block =
          BlockSet.add
            (BAssign
               ( N_SIMPLE
                   (`CREATE_CONTRACT, [], [opt_k; mutz; ty2], [ope; addr], lab),
                 lab ))
            block_set in
        info_node return_cpt lambda_map name_l lab block flow inter_flow xs
      | _ -> failwith "TODO control node")

  and block_of_if return_cpt final_label lambda_map (result_if : A.t list)
      (label_start : label) (label_then : label) (label_else : label)
      (arg_if : A.t) (node_l : A.t node list) (node_r : A.t node list)
      (block : BlockSet.t) (flow : FlowSet.t) (inter_flow : InterFlow.t) =
    let flow = FlowSet.add (label_start, label_then) flow in
    let flow = FlowSet.add (label_start, label_else) flow in
    let block = BlockSet.add (BThen (arg_if, label_then)) block in
    let block = BlockSet.add (BElse (arg_if, label_else)) block in
    let new_return_cpt, lambda_map, final_left, block, flow, inter_flow =
      info_node return_cpt lambda_map result_if label_then block flow inter_flow
        node_l in
    let new_return_cpt, lambda_map, final_right, block, flow, inter_flow =
      info_node new_return_cpt lambda_map result_if label_else block flow
        inter_flow node_r in
    let flow =
      FlowSet.add (final_left, final_label)
        (FlowSet.add (final_right, final_label) flow) in
    let block = BlockSet.add (BEnd final_label) block in
    (new_return_cpt, lambda_map, final_label, block, flow, inter_flow)

  and lambda_stacking return_cpt label_start label_end arg node_l lambda acc
      block flow inter_flow =
    let block = BlockSet.add (BIs label_start) block in
    let ret =
      match A.deconstruct lambda with
      | Mprim { prim = `lambda; args = [_; ret]; _ } -> ret
      | _ -> failwith "The result of a lambda instruction is not a lambda" in
    match List.rev node_l with
    | N_END ([res], _abel) :: _ ->
      let new_return_cpt, final_lambda =
        A.construct ~build_name return_cpt ret in
      let new_return_cpt, acc, final_label, block, flow, inter_flow =
        info_node new_return_cpt acc [final_lambda] label_start block flow
          inter_flow node_l in
      let block = BlockSet.add (BEndLambda label_end) block in
      let flow = FlowSet.add (final_label, label_end) flow in
      let lambda_code =
        Some { arg; label_start; lcode = node_l; label_end; res = Some res }
      in
      let lambda_element =
        { lambda_name = lambda; lambda_code; in_between = [] } in
      let lambda_map =
        LambdaName.update lambda
          (function
            | None -> Some [lambda_element]
            | Some arg -> Some (lambda_element :: arg))
          acc in
      (new_return_cpt, lambda_map, block, flow, inter_flow)
    | N_FAILWITH _ :: _ | N_NEVER _ :: _ ->
      let new_return_cpt, acc, final_label, block, flow, inter_flow =
        info_node return_cpt acc [] label_start block flow inter_flow node_l
      in
      let lambda_code =
        Some { arg; label_start; lcode = node_l; label_end; res = None } in
      let block = BlockSet.add (BEndLambda label_end) block in
      let flow = FlowSet.add (final_label, label_end) flow in
      let lambda_element =
        { lambda_name = lambda; lambda_code; in_between = [] } in
      let lambda_map =
        LambdaName.update lambda
          (function
            | None -> Some [lambda_element]
            | Some arg -> Some (lambda_element :: arg))
          acc in
      (new_return_cpt, lambda_map, block, flow, inter_flow)
    | _ -> failwith "Not possible for a lambda to finish"

  and info_view (return_cpt, lambda_map, blocks, flow, inter_flow) view =
    let new_return_cpt, final_view =
      A.construct ~build_name return_cpt view.view_result in
    let blocks =
      BlockSet.add (BAnnoAssign ([view.view_stack], view.view_label)) blocks
    in
    let new_return_cpt, lambda_map, final, blocks, flow, inter_flow =
      info_node new_return_cpt lambda_map [final_view] view.view_label blocks
        flow inter_flow view.node_code_v in
    ( (new_return_cpt, lambda_map, blocks, flow, inter_flow),
      (view.view_label, final, final_view) )

  and info_smart (sc : arg sc_node) =
    let acc =
      (0, LambdaName.empty, BlockSet.empty, FlowSet.empty, InterFlow.empty)
    in
    let (new_return_cpt, lambda_map, blocks, flow, inter_flow), view_labels =
      List.fold_left_map info_view acc sc.views_node in
    let block =
      BlockSet.add (BAnnoAssign ([sc.begin_stack], sc.begin_label)) blocks in
    let op = prim `operation in
    let op_l = prim `list ~args:[op] in
    let ty = prim `pair ~args:[op_l; sc.storage] in
    let new_return_cpt, name_final = A.construct ~build_name new_return_cpt ty in
    let _new_return_cpt, lambda_map, final, blocks, flow, inter_flow =
      info_node new_return_cpt lambda_map [name_final] sc.begin_label block flow
        inter_flow sc.node_l in
    {
      final;
      name_init = sc.begin_stack;
      name_final;
      initial = sc.begin_label;
      blocks;
      inter_flow;
      flow;
      lambda_map;
      view_labels;
      original = sc;
    }

  let lab_in_block (lab : label) (b : _ block_unstack) =
    match b with
    | BAssign (_, l)
    | BAnnoAssign (_, l)
    | BReturnAssign (_, _, l)
    | BStartExec (_, _, l)
    | BEndExec (_, _, l)
    | BEnd l
    | BIs l
    | BLoop (_, _, _, l)
    | BPattern (_, _, l)
    | BThen (_, l)
    | BElse (_, l)
    | BEndLambda l
    | BAnnonymous (_, _, l) -> l = lab

  (* I am assuming that every variable is unique *)
  let print_block b =
    Format.pp_print_newline Format.err_formatter () ;
    match b with
    | BAssign (N_SIMPLE (prim, miche_l, args, res, _), l) ->
      let miche_l = List.map Micheline_lib.string_of_micheline miche_l in
      let args = List.map A.to_string args in
      let res = List.map A.to_string res in
      Format.eprintf "%s = %s %s (%s) [%d]@." (String.concat ", " res)
        (Micheline_lib.prim_name ~prim)
        (String.concat " " miche_l)
        (String.concat " ," args) l
    | BAnnoAssign (name_l, l) | BPattern (name_l, _, l) ->
      let name_l = List.map A.to_string name_l in
      Format.eprintf "Pattern(%s) [%d]@." (String.concat ", " name_l) l
    | BReturnAssign (name_assign, name_use, l) ->
      let name_assign = List.map A.to_string name_assign in
      let name_use = List.map A.to_string name_use in
      Format.eprintf "%s = %s [%d]@."
        (String.concat " ," name_assign)
        (String.concat " ," name_use)
        l
    | BEnd l -> Format.eprintf "END [%d]@." l
    | BStartExec (_arg, _lambda, _label) ->
      Format.eprintf "START [%d]@." _label
      (*Format.pp_print_list ~pp_sep:Format.pp_print_newline
        (fun ppf elt ->
          match elt.lambda_code with
          | None -> Format.fprintf ppf "Noi lambda"
          | Some m ->
            Format.fprintf ppf
              "arg : %s, start lambda : %d, end lambda : %d, res : %a"
              (A.to_string m.arg) m.label_start m.label_end
              (fun ppf -> function
                | None -> Format.fprintf ppf "No result"
                | Some s -> Format.fprintf ppf "%s" (A.to_string s))
              m.res)
        Format.err_formatter lambda*)
    | BEndLambda l -> Format.eprintf "End Lambda [%d]@." l
    | BEndExec _ -> Format.eprintf "End exec@."
    | BAssign _ -> failwith "Not simple"
    | BIs l -> Format.eprintf "Start in lambda [%d]" l
    | BThen (_arg, _) -> Format.eprintf "Start then"
    | BElse (_arg, _) -> Format.eprintf "Start else"
    | _ -> ()

  let block_of_label lab analysis =
    let blocs = BlockSet.filter (lab_in_block lab) analysis.blocks in
    match BlockSet.choose_opt blocs with
    | Some b when BlockSet.cardinal blocs = 1 -> b
    | _ ->
      BlockSet.iter print_block blocs ;
      failwith
        (Format.sprintf "%d block for label %d" (BlockSet.cardinal blocs) lab)

  let list_of_blocks (tool : analysis_tools) : A.t block_unstack list =
    BlockSet.fold (fun elt acc -> elt :: acc) tool.blocks []

  let get_final (tool : analysis_tools) : arg * label =
    (tool.name_final, tool.final)

  let get_init (tool : analysis_tools) : arg * label =
    (tool.name_init, tool.initial)

  let get_flow (tool : analysis_tools) : FlowSet.t = tool.flow

  let get_inter_flow (tool : analysis_tools) : InterFlow.t = tool.inter_flow

  let find_labels (label : label) (tool : analysis_tools) =
    let block = block_of_label label tool in
    match block with
    | BStartExec (_, lambda, _) -> (
      match LambdaName.find_opt lambda tool.lambda_map with
      | None -> LabelSet.empty
      | Some lambdas ->
        List.fold_left
          (fun acc -> function
            | { lambda_code = Some l; _ } ->
              let lab_set = lab_folder.node_seq lab_folder () acc l.lcode in
              LabelSet.add l.label_start (LabelSet.add l.label_end lab_set)
            | _ -> acc)
          LabelSet.empty lambdas)
    | _ -> LabelSet.empty

  let find_lambda (tool : analysis_tools) (chain : chain_call) (name : name_arg)
      : arg complete_lambda list =
    let lambda_chain =
      LambdaName.fold
        (fun arg l acc ->
          let name = Option.get @@ A.to_name_arg arg in
          LambdaChain.add (name, []) l acc)
        tool.lambda_map LambdaChain.empty in
    match LambdaChain.find_opt (name, chain) lambda_chain with
    | None ->
      (* it may be a lambda that was top level so it chain is []*)
      Option.value (LambdaChain.find_opt (name, []) lambda_chain) ~default:[]
    | Some l -> l

  let get_original (tool : analysis_tools) : arg sc_node = tool.original

  let view_labels (tool : analysis_tools) : (label * label * arg) list =
    tool.view_labels

  let all_fail (tool : analysis_tools) : (int list * arg * int) list =
    let flows = FlowSet.elements tool.flow in
    BlockSet.fold
      (fun block acc ->
        match block with
        | BAssign (N_SIMPLE (`FAILWITH, [], [arg], [], _lo), lo) ->
          let l =
            List.filter_map
              (fun (l1, l2) ->
                if l2 = lo then
                  Some l1
                else
                  None)
              flows in
          (l, arg, lo) :: acc
        | _ -> acc)
      tool.blocks []
end

let print_inter_flow (ppf : Format.formatter) (inter : InterFlow.t) : unit =
  let inter_elems = InterFlow.elements inter in
  Format.pp_print_list ~pp_sep:Format.pp_print_newline
    (fun ppf (lc, li, le, lr) ->
      Format.fprintf ppf "%d -> %d -> ... -> %d -> %d" lc li le lr)
    ppf inter_elems

module SetInt = Set.Make (Int)
module SetNested = Set.Make (Nested_arg.Ordered_nested)
module MapString = Map.Make (String)

let handle_pat folder env acc node_l (pat : Nested_arg.t) =
  let annots = Nested_arg.get_annot pat in
  let acc =
    match Factori_utils.get_name_annots annots with
    | None -> acc
    | Some s ->
      (match List.rev node_l with
      | [] -> failwith "Cant be empty"
      | x :: _ ->
        let lab_l = Lib_unstack.label_of_node x in
        MapString.update s (function
          | None -> Some (SetInt.of_list lab_l)
          | Some elt ->
            let res =
              List.fold_left (fun acc lab -> SetInt.add lab acc) elt lab_l in
            Some res))
        acc in
  let env_left = SetNested.add pat env in
  let acc_l = folder.node_seq folder env_left acc node_l in
  acc_l

let check_if_left folder env acc ctrl_node =
  match ctrl_node with
  | N_IF_LEFT (arg, _, _, (pat_l, _lab_l, node_l), (pat_r, _lab_r, node_r)) -> (
    match SetNested.mem arg env with
    | false ->
      (* just skip this if left has nothing to do with the parameter (maybe it can be in a lambda but for now let's not take this case*)
      acc
    | true ->
      let acc_l = handle_pat folder env acc node_l pat_l in
      let acc_r = handle_pat folder env acc_l node_r pat_r in
      acc_r)
  | _ -> default_folder.control_node_fold folder env acc ctrl_node

let last_entry_point (sc_node : Nested_arg.t sc_node) : SetInt.t MapString.t =
  let param_storage = sc_node.begin_stack in
  let parameter = Nested_arg.recup_parameter param_storage in
  let folder = { default_folder with control_node_fold = check_if_left } in
  let env = SetNested.singleton parameter in
  folder.node_seq folder env MapString.empty sc_node.node_l
