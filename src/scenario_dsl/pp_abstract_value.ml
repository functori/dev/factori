(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module contains functions for pretty-printing abstract values to
OCaml, TypeScript, and shell syntax. It exports six functions:
`pp_abstract_paired_type_to_ocaml`, `pp_tuple_to_ocaml`, `pp_value_to_ocaml`,
`pp_abstract_paired_type_to_typescript`, `pp_tuple_to_typescript`, and
`pp_value_to_typescript` for OCaml and TypeScript syntax, and
`pp_value_to_shell` for shell syntax. The module defines the
`abstract_paired_type`, `value tuple`, and `value` types, and provides examples
of how to use each function. Warning: this short summary was automatically
generated and could be incomplete or misleading. If you feel this is the case,
please open an issue. *)

open Format
open Factori_utils
open Abstract_value

let rec pp_abstract_paired_type_to_ocaml :
          'a.
          string Context.Context.Names.t ->
          Universes.universe ->
          formatter ->
          'a abstract_paired_type ->
          unit =
  fun (type a) names universe (ppf : formatter) (t : a abstract_paired_type) ->
   match t with
   | LeafP (field_name, v) ->
     fprintf ppf "%s = %a"
       (show_sanitized_name field_name)
       (pp_value_to_ocaml names universe)
       v
   | PairP (t1, t2) ->
     fprintf ppf "%a;\n%a"
       (pp_abstract_paired_type_to_ocaml names universe)
       t1
       (pp_abstract_paired_type_to_ocaml names universe)
       t2

and pp_tuple_to_ocaml :
      'a.
      string Context.Context.Names.t ->
      Universes.universe ->
      formatter ->
      'a value tuple ->
      unit =
  fun (type a) names universe (ppf : formatter) (t : a value tuple) ->
   match t with
   | Single v -> (pp_value_to_ocaml names universe) ppf v
   | Multi (v1, vrest) ->
     fprintf ppf "%a,%a"
       (pp_value_to_ocaml names universe)
       v1
       (pp_tuple_to_ocaml names universe)
       vrest

and pp_value_to_ocaml :
      'a.
      string Context.Context.Names.t ->
      Universes.universe ->
      formatter ->
      'a value ->
      unit =
  fun (type a) names universe (ppf : formatter) (v : a value) ->
   fprintf ppf "(" ;
   let res =
     match v with
     | VAbstract (UniId id) ->
       fprintf ppf "_%a" (Context.Context.print_name names universe) id
     | VAbstract (ScenId id) -> fprintf ppf "_%s" id
     | VSaplingState s -> fprintf ppf "{|%s|}" s
     | VTicket (Ticket s) -> fprintf ppf "{|%s|}" s
     | VUnit _ -> fprintf ppf "()"
     | VKeyHash kh -> fprintf ppf "\"%s\"" kh
     | VKey k -> fprintf ppf "\"%s\"" k
     | VChain_id s -> fprintf ppf "\"%s\"" s
     | VSignature s -> fprintf ppf "\"%s\"" s
     | VOperation o -> fprintf ppf "\"%s\"" o
     | VOption None -> fprintf ppf "None"
     | VOption (Some x) ->
       fprintf ppf "Some (%a)" (pp_value_to_ocaml names universe) x
     | VInt z -> print_z_int_ocaml ppf z
     | VTez z -> print_z_int_ocaml ppf z
     | VString s -> fprintf ppf "{|%s|}" s
     | VBytes s -> fprintf ppf "Crypto.H.mk {|%s|}" (s :> string)
     | VTimestamp s -> fprintf ppf "\"%s\"" s
     | VBool b -> fprintf ppf "%b" b
     | VAddress addr -> fprintf ppf "\"%s\"" addr
     | VSumtype (LeafOr (constructor, VUnit _)) ->
       fprintf ppf "%s" (show_sanitized_name constructor)
     | VSumtype (LeafOr (constructor, v)) ->
       fprintf ppf "%s (%a)"
         (show_sanitized_name constructor)
         (pp_value_to_ocaml names universe)
         v
     | VSumtype (SumOrL v) ->
       fprintf ppf "%a" (pp_value_to_ocaml names universe) (VSumtype v)
     | VSumtype (SumOrR v) ->
       fprintf ppf "%a" (pp_value_to_ocaml names universe) (VSumtype v)
     | VMap m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ";") (fun ppf xy ->
              match xy with
              | SP xy -> fprintf ppf "%a" (pp_value_to_ocaml names universe) xy
              | SP2 (x, y) ->
                fprintf ppf "%a,%a"
                  (pp_value_to_ocaml names universe)
                  x
                  (pp_value_to_ocaml names universe)
                  y))
         m
     | VSet m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ";") (pp_value_to_ocaml names universe))
         m
     | VList m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ";") (pp_value_to_ocaml names universe))
         m
     | VOnChainBigMap i -> fprintf ppf "Abstract (%a)" print_z_int_ocaml i
     | VLiteralBigMap bm ->
       fprintf ppf "Literal [%a]"
         (pp_print_list ~pp_sep:(tag ";") (fun ppf xy ->
              match xy with
              | SP xy -> fprintf ppf "%a" (pp_value_to_ocaml names universe) xy
              | SP2 (x, y) ->
                fprintf ppf "%a,%a"
                  (pp_value_to_ocaml names universe)
                  x
                  (pp_value_to_ocaml names universe)
                  y))
         bm
     | VTuple vl -> fprintf ppf "(%a)" (pp_tuple_to_ocaml names universe) vl
     | VLambda (Lambda l) ->
       fprintf ppf
         "Lambda {from = EzEncoding.destruct Tzfunc.Proto.micheline_enc.json \
          {|%s|};\n\
          to_ = EzEncoding.destruct Tzfunc.Proto.micheline_enc.json {|%s|};\n\
          body = EzEncoding.destruct Tzfunc.Proto.micheline_enc.json {|%s|}}"
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.from)
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.to_)
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.body)
     | VLambda (SeqLambda m) ->
       fprintf ppf
         "SeqLambda (EzEncoding.destruct Tzfunc.Proto.micheline_enc.json \
          {|%s|})"
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json m)
     | VRecord t ->
       fprintf ppf "{%a}" (pp_abstract_paired_type_to_ocaml names universe) t
   in
   fprintf ppf ")" ;
   res

(* Typescript version *)
let rec pp_abstract_paired_type_to_typescript :
          'a.
          string Context.Context.Names.t ->
          Universes.universe ->
          formatter ->
          'a abstract_paired_type ->
          unit =
  fun (type a) names universe (ppf : formatter) (t : a abstract_paired_type) ->
   match t with
   | LeafP (field_name, v) ->
     fprintf ppf "%s : %a"
       (show_sanitized_name field_name)
       (pp_value_to_typescript names universe)
       v
   | PairP (t1, t2) ->
     fprintf ppf "%a,\n%a"
       (pp_abstract_paired_type_to_typescript names universe)
       t1
       (pp_abstract_paired_type_to_typescript names universe)
       t2

and pp_tuple_to_typescript :
      'a.
      string Context.Context.Names.t ->
      Universes.universe ->
      formatter ->
      'a value tuple ->
      unit =
  fun (type a) names universe (ppf : formatter) (t : a value tuple) ->
   match t with
   | Single v -> pp_value_to_typescript names universe ppf v
   | Multi (v1, vrest) ->
     fprintf ppf "%a,%a"
       (pp_value_to_typescript names universe)
       v1
       (pp_tuple_to_typescript names universe)
       vrest

and pp_value_to_typescript :
      'a.
      string Context.Context.Names.t ->
      Universes.universe ->
      formatter ->
      'a value ->
      unit =
  fun (type a) names universe (ppf : formatter) (v : a value) ->
   fprintf ppf "" ;
   let res =
     match v with
     | VAbstract (UniId id) ->
       fprintf ppf "%a" (Context.Context.print_name names universe) id
     | VAbstract (ScenId id) -> fprintf ppf "%s" id
     | VChain_id s -> fprintf ppf "\"%s\"" s
     | VUnit _ -> fprintf ppf "null"
     | VKeyHash kh -> fprintf ppf "\"%s\"" kh
     | VKey k -> fprintf ppf "\"%s\"" k
     | VSignature s -> fprintf ppf "\"%s\"" s
     | VOperation o -> fprintf ppf "\"%s\"" o
     | VOption None -> fprintf ppf "null"
     | VOption (Some x) ->
       fprintf ppf "%a" (pp_value_to_typescript names universe) x
     | VInt z -> print_z_int_typescript ppf z
     | VTez z -> print_z_int_typescript ppf z
     | VString s -> fprintf ppf "\"%s\"" s
     | VBytes s -> fprintf ppf "\"%s\"" (s :> string)
     | VTimestamp s -> fprintf ppf "(\"%s\")" s
     | VBool b -> fprintf ppf "%b" b
     | VAddress addr -> fprintf ppf "\"%s\"" addr
     | VSumtype (LeafOr (constructor, VUnit _)) ->
       let constructor = show_sanitized_name constructor in
       fprintf ppf "/* TODO: fix constructors */{ kind : \"%s_constructor\" }"
         constructor
     | VSumtype (LeafOr (constructor, v)) ->
       let constructor = show_sanitized_name constructor in
       fprintf ppf
         "/* TODO: fix constructors */{ kind : \"%s_constructor\", %s_element \
          : (%a)}"
         constructor constructor
         (pp_value_to_typescript names universe)
         v
     | VSumtype (SumOrL v) ->
       fprintf ppf "%a" (pp_value_to_typescript names universe) (VSumtype v)
     | VSumtype (SumOrR v) ->
       fprintf ppf "%a" (pp_value_to_typescript names universe) (VSumtype v)
     | VMap m ->
       fprintf ppf "make_literal_map([%a])"
         (pp_print_list ~pp_sep:(tag ",") (fun ppf xy ->
              match xy with
              | SP xy ->
                fprintf ppf "%a" (pp_value_to_typescript names universe) xy
              | SP2 (x, y) ->
                fprintf ppf "[%a,%a]"
                  (pp_value_to_typescript names universe)
                  x
                  (pp_value_to_typescript names universe)
                  y))
         m
     | VSet m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ",")
            (pp_value_to_typescript names universe))
         m
     | VList m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ",")
            (pp_value_to_typescript names universe))
         m
     | VOnChainBigMap i ->
       fprintf ppf "make_abstract_bm(%a)" print_z_int_typescript i
     | VLiteralBigMap bm ->
       fprintf ppf "make_literal_bm([%a])"
         (pp_print_list ~pp_sep:(tag ",") (fun ppf xy ->
              match xy with
              | SP xy ->
                fprintf ppf "%a" (pp_value_to_typescript names universe) xy
              | SP2 (x, y) ->
                fprintf ppf "[%a,%a]"
                  (pp_value_to_typescript names universe)
                  x
                  (pp_value_to_typescript names universe)
                  y))
         bm
     | VTuple vl ->
       fprintf ppf "(%a)" (pp_tuple_to_typescript names universe) vl
     | VLambda (Lambda l) ->
       fprintf ppf
         "{from : new Parser().parseJSON(%s), to_ : new \
          Parser().parseJSON(%s), body : new Parser().parseJSON(%s)}"
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.from)
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.to_)
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.body)
     | VLambda (SeqLambda m) ->
       fprintf ppf "{ seqlambda : new Parser().parseJSON(%s) }"
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json m)
     | VRecord t ->
       fprintf ppf "{%a}"
         (pp_abstract_paired_type_to_typescript names universe)
         t
     | VTicket (Ticket s) -> fprintf ppf "\"%s\"" s
     | VSaplingState s -> fprintf ppf "\"%s\"" s in
   fprintf ppf "" ;
   res

(* Shell version *)
let rec pp_abstract_paired_type_to_shell :
          'a.
          string Context.Context.Names.t ->
          Universes.universe ->
          formatter ->
          'a abstract_paired_type ->
          unit =
  fun (type a) names universe (ppf : formatter) (t : a abstract_paired_type) ->
   match t with
   | LeafP (_field_name, v) ->
     fprintf ppf "%a" (pp_value_to_shell names universe) v
   | PairP (t1, t2) ->
     fprintf ppf "(Pair %a %a)"
       (pp_abstract_paired_type_to_shell names universe)
       t1
       (pp_abstract_paired_type_to_shell names universe)
       t2

and pp_tuple_to_shell :
      'a.
      string Context.Context.Names.t ->
      Universes.universe ->
      formatter ->
      'a value tuple ->
      unit =
  fun (type a) names universe (ppf : formatter) (t : a value tuple) ->
   match t with
   | Single v -> pp_value_to_shell names universe ppf v
   | Multi (v1, vrest) ->
     fprintf ppf "%a,%a"
       (pp_value_to_shell names universe)
       v1
       (pp_tuple_to_shell names universe)
       vrest

and pp_value_to_shell :
      'a.
      string Context.Context.Names.t ->
      Universes.universe ->
      formatter ->
      'a value ->
      unit =
  fun (type a) names universe (ppf : formatter) (v : a value) ->
   fprintf ppf "" ;
   let res =
     match v with
     | VAbstract (UniId id) ->
       fprintf ppf "%a" (Context.Context.print_name names universe) id
     | VAbstract (ScenId id) -> fprintf ppf "%s" id
     | VChain_id s -> fprintf ppf "\"%s\"" s
     | VUnit _ -> fprintf ppf "null"
     | VKeyHash kh -> fprintf ppf "\"%s\"" kh
     | VKey k -> fprintf ppf "\"%s\"" k
     | VSignature s -> fprintf ppf "\"%s\"" s
     | VOperation o -> fprintf ppf "\"%s\"" o
     | VOption None -> fprintf ppf "null"
     | VOption (Some x) -> fprintf ppf "%a" (pp_value_to_shell names universe) x
     | VInt z -> print_z_int_shell ppf z
     | VTez z -> print_z_int_shell ppf z
     | VString s -> fprintf ppf "\"%s\"" s
     | VBytes s -> fprintf ppf "\"%s\"" (s :> string)
     | VTimestamp s -> fprintf ppf "(\"%s\")" s
     | VBool b -> fprintf ppf "%b" b
     | VAddress addr -> fprintf ppf "\"%s\"" addr
     | VSumtype (LeafOr (constructor, VUnit _)) ->
       let constructor = show_sanitized_name constructor in
       fprintf ppf "/* TODO: fix constructors */{ kind : \"%s_constructor\" }"
         constructor
     | VSumtype (LeafOr (constructor, v)) ->
       let constructor = show_sanitized_name constructor in
       fprintf ppf
         "/* TODO: fix constructors */{ kind : \"%s_constructor\", %s_element \
          : (%a)}"
         constructor constructor
         (pp_value_to_shell names universe)
         v
     | VSumtype (SumOrL v) ->
       fprintf ppf "(Left %a)" (pp_value_to_shell names universe) (VSumtype v)
     | VSumtype (SumOrR v) ->
       fprintf ppf "(Right %a)" (pp_value_to_shell names universe) (VSumtype v)
     | VMap m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ",") (fun ppf xy ->
              match xy with
              | SP xy -> fprintf ppf "%a" (pp_value_to_shell names universe) xy
              | SP2 (x, y) ->
                fprintf ppf "[%a,%a]"
                  (pp_value_to_shell names universe)
                  x
                  (pp_value_to_shell names universe)
                  y))
         m
     | VSet m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ",") (pp_value_to_shell names universe))
         m
     | VList m ->
       fprintf ppf "[%a]"
         (pp_print_list ~pp_sep:(tag ",") (pp_value_to_shell names universe))
         m
     | VOnChainBigMap i ->
       fprintf ppf "make_abstract_bm(%a)" print_z_int_shell i
     | VLiteralBigMap bm ->
       fprintf ppf "({%a})"
         (pp_print_list ~pp_sep:(tag ",") (fun ppf xy ->
              match xy with
              | SP xy -> fprintf ppf "%a" (pp_value_to_shell names universe) xy
              | SP2 (x, y) ->
                fprintf ppf "[%a,%a]"
                  (pp_value_to_shell names universe)
                  x
                  (pp_value_to_shell names universe)
                  y))
         bm
     | VTuple vl -> fprintf ppf "(%a)" (pp_tuple_to_shell names universe) vl
     | VLambda (Lambda l) ->
       fprintf ppf
         "{from : new Parser().parseJSON(%s), to_ : new \
          Parser().parseJSON(%s), body : new Parser().parseJSON(%s)}"
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.from)
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.to_)
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json l.body)
     | VLambda (SeqLambda m) ->
       fprintf ppf "{ seqlambda : new Parser().parseJSON(%s)}"
         (EzEncoding.construct Tzfunc.Proto.micheline_enc.json m)
     | VRecord t ->
       fprintf ppf "{%a}" (pp_abstract_paired_type_to_shell names universe) t
     | VTicket (Ticket s) -> fprintf ppf "\"%s\"" s
     | VSaplingState s -> fprintf ppf "\"%s\"" s in
   fprintf ppf "" ;
   res
