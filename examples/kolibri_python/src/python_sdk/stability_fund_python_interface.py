from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

SetSavingsPoolContract = factori_types.address

def setSavingsPoolContract_generator() -> SetSavingsPoolContract:
	return factori_types.address_generator ()

def setSavingsPoolContract_encode (arg: SetSavingsPoolContract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in setSavingsPoolContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setSavingsPoolContract_decode (arg) -> SetSavingsPoolContract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in setSavingsPoolContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setSavingsPoolContract():
	temp=setSavingsPoolContract_generator()
	temp_encoded=setSavingsPoolContract_encode(temp)
	try:
		print('++++ setSavingsPoolContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setSavingsPoolContract: " + str(temp))
	temp1=setSavingsPoolContract_decode(temp_encoded)
	if temp1 == temp:
		print('setSavingsPoolContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setSavingsPoolContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setSavingsPoolContract()
#except Exception as e:
#	print('Error in assert_identity_setSavingsPoolContract' + str(e))




def callSetSavingsPoolContract(kt1 : str, _from : blockchain.identity, param : SetSavingsPoolContract, networkName = "ghostnet", debug=False):
	input = setSavingsPoolContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setSavingsPoolContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetOvenRegistryContract = SetSavingsPoolContract


def setOvenRegistryContract_generator() -> SetOvenRegistryContract:
	return setSavingsPoolContract_generator ()


def setOvenRegistryContract_encode (arg: SetOvenRegistryContract):
	try:
		return setSavingsPoolContract_encode(arg)
	except Exception as e:
		print('Error in setOvenRegistryContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setOvenRegistryContract_decode (arg) -> SetOvenRegistryContract:
	try:
		return setSavingsPoolContract_decode(arg)
	except Exception as e:
		print('Error in setOvenRegistryContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setOvenRegistryContract():
	temp=setOvenRegistryContract_generator()
	temp_encoded=setOvenRegistryContract_encode(temp)
	try:
		print('++++ setOvenRegistryContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setOvenRegistryContract: " + str(temp))
	temp1=setOvenRegistryContract_decode(temp_encoded)
	if temp1 == temp:
		print('setOvenRegistryContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setOvenRegistryContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setOvenRegistryContract()
#except Exception as e:
#	print('Error in assert_identity_setOvenRegistryContract' + str(e))




def callSetOvenRegistryContract(kt1 : str, _from : blockchain.identity, param : SetOvenRegistryContract, networkName = "ghostnet", debug=False):
	input = setOvenRegistryContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setOvenRegistryContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = SetSavingsPoolContract


def setGovernorContract_generator() -> SetGovernorContract:
	return setSavingsPoolContract_generator ()


def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return setSavingsPoolContract_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return setSavingsPoolContract_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetDelegate = Optional[factori_types.key_hash]

def setDelegate_generator() -> SetDelegate:
	return (factori_types.option_generator (factori_types.key_hash_generator)) ()

def setDelegate_encode (arg: SetDelegate):
	try:
		return factori_types.option_encode(factori_types.key_hash_encode)(arg)
	except Exception as e:
		print('Error in setDelegate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setDelegate_decode (arg) -> SetDelegate:
	try:
		return factori_types.option_decode(factori_types.key_hash_decode)(arg)
	except Exception as e:
		print('Error in setDelegate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setDelegate():
	temp=setDelegate_generator()
	temp_encoded=setDelegate_encode(temp)
	try:
		print('++++ setDelegate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setDelegate: " + str(temp))
	temp1=setDelegate_decode(temp_encoded)
	if temp1 == temp:
		print('setDelegate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setDelegate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setDelegate()
#except Exception as e:
#	print('Error in assert_identity_setDelegate' + str(e))




def callSetDelegate(kt1 : str, _from : blockchain.identity, param : SetDelegate, networkName = "ghostnet", debug=False):
	input = setDelegate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setDelegate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetAdministratorContract = SetSavingsPoolContract


def setAdministratorContract_generator() -> SetAdministratorContract:
	return setSavingsPoolContract_generator ()


def setAdministratorContract_encode (arg: SetAdministratorContract):
	try:
		return setSavingsPoolContract_encode(arg)
	except Exception as e:
		print('Error in setAdministratorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setAdministratorContract_decode (arg) -> SetAdministratorContract:
	try:
		return setSavingsPoolContract_decode(arg)
	except Exception as e:
		print('Error in setAdministratorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setAdministratorContract():
	temp=setAdministratorContract_generator()
	temp_encoded=setAdministratorContract_encode(temp)
	try:
		print('++++ setAdministratorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setAdministratorContract: " + str(temp))
	temp1=setAdministratorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setAdministratorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setAdministratorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setAdministratorContract()
#except Exception as e:
#	print('Error in assert_identity_setAdministratorContract' + str(e))




def callSetAdministratorContract(kt1 : str, _from : blockchain.identity, param : SetAdministratorContract, networkName = "ghostnet", debug=False):
	input = setAdministratorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setAdministratorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendTokens = Tuple[factori_types.nat,SetSavingsPoolContract]

def sendTokens_generator() -> SendTokens:
	return (factori_types.tuple2_generator(factori_types.nat_generator,
setSavingsPoolContract_generator))()

def sendTokens_encode(arg : SendTokens) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.nat_encode,setSavingsPoolContract_encode)(arg) 
	except Exception as e:
		print('Error in sendTokens_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def sendTokens_decode(arg) -> SendTokens:
	try:
		return factori_types.tuple2_decode(factori_types.nat_decode,setSavingsPoolContract_decode)(arg) 
	except Exception as e:
		print('Error in sendTokens_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendTokens():
	temp=sendTokens_generator()
	temp_encoded=sendTokens_encode(temp)
	try:
		print('++++ sendTokens: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendTokens: " + str(temp))
	temp1=sendTokens_decode(temp_encoded)
	if temp1 == temp:
		print('sendTokens check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendTokens check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendTokens()
#except Exception as e:
#	print('Error in assert_identity_sendTokens' + str(e))




def callSendTokens(kt1 : str, _from : blockchain.identity, param : SendTokens, networkName = "ghostnet", debug=False):
	input = sendTokens_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendTokens", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendAllTokens_callback = factori_types.nat

def sendAllTokens_callback_generator() -> SendAllTokens_callback:
	return factori_types.nat_generator ()

def sendAllTokens_callback_encode (arg: SendAllTokens_callback):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in sendAllTokens_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def sendAllTokens_callback_decode (arg) -> SendAllTokens_callback:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in sendAllTokens_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendAllTokens_callback():
	temp=sendAllTokens_callback_generator()
	temp_encoded=sendAllTokens_callback_encode(temp)
	try:
		print('++++ sendAllTokens_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendAllTokens_callback: " + str(temp))
	temp1=sendAllTokens_callback_decode(temp_encoded)
	if temp1 == temp:
		print('sendAllTokens_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendAllTokens_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendAllTokens_callback()
#except Exception as e:
#	print('Error in assert_identity_sendAllTokens_callback' + str(e))




def callSendAllTokens_callback(kt1 : str, _from : blockchain.identity, param : SendAllTokens_callback, networkName = "ghostnet", debug=False):
	input = sendAllTokens_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendAllTokens_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendAllTokens = SetSavingsPoolContract


def sendAllTokens_generator() -> SendAllTokens:
	return setSavingsPoolContract_generator ()


def sendAllTokens_encode (arg: SendAllTokens):
	try:
		return setSavingsPoolContract_encode(arg)
	except Exception as e:
		print('Error in sendAllTokens_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def sendAllTokens_decode (arg) -> SendAllTokens:
	try:
		return setSavingsPoolContract_decode(arg)
	except Exception as e:
		print('Error in sendAllTokens_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendAllTokens():
	temp=sendAllTokens_generator()
	temp_encoded=sendAllTokens_encode(temp)
	try:
		print('++++ sendAllTokens: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendAllTokens: " + str(temp))
	temp1=sendAllTokens_decode(temp_encoded)
	if temp1 == temp:
		print('sendAllTokens check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendAllTokens check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendAllTokens()
#except Exception as e:
#	print('Error in assert_identity_sendAllTokens' + str(e))




def callSendAllTokens(kt1 : str, _from : blockchain.identity, param : SendAllTokens, networkName = "ghostnet", debug=False):
	input = sendAllTokens_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendAllTokens", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendAll = SetSavingsPoolContract


def sendAll_generator() -> SendAll:
	return setSavingsPoolContract_generator ()


def sendAll_encode (arg: SendAll):
	try:
		return setSavingsPoolContract_encode(arg)
	except Exception as e:
		print('Error in sendAll_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def sendAll_decode (arg) -> SendAll:
	try:
		return setSavingsPoolContract_decode(arg)
	except Exception as e:
		print('Error in sendAll_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendAll():
	temp=sendAll_generator()
	temp_encoded=sendAll_encode(temp)
	try:
		print('++++ sendAll: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendAll: " + str(temp))
	temp1=sendAll_decode(temp_encoded)
	if temp1 == temp:
		print('sendAll check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendAll check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendAll()
#except Exception as e:
#	print('Error in assert_identity_sendAll' + str(e))




def callSendAll(kt1 : str, _from : blockchain.identity, param : SendAll, networkName = "ghostnet", debug=False):
	input = sendAll_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendAll", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Send = Tuple[factori_types.tez,SetSavingsPoolContract]

def send_generator() -> Send:
	return (factori_types.tuple2_generator(factori_types.tez_generator,
setSavingsPoolContract_generator))()

def send_encode(arg : Send) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.tez_encode,setSavingsPoolContract_encode)(arg) 
	except Exception as e:
		print('Error in send_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def send_decode(arg) -> Send:
	try:
		return factori_types.tuple2_decode(factori_types.tez_decode,setSavingsPoolContract_decode)(arg) 
	except Exception as e:
		print('Error in send_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_send():
	temp=send_generator()
	temp_encoded=send_encode(temp)
	try:
		print('++++ send: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of send: " + str(temp))
	temp1=send_decode(temp_encoded)
	if temp1 == temp:
		print('send check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('send check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_send()
#except Exception as e:
#	print('Error in assert_identity_send' + str(e))




def callSend(kt1 : str, _from : blockchain.identity, param : Send, networkName = "ghostnet", debug=False):
	input = send_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "send", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class RescueFA2(TypedDict):
	tokenContractAddress: SetSavingsPoolContract
	tokenId: SendAllTokens_callback
	amount: SendAllTokens_callback
	destination: SetSavingsPoolContract

def rescueFA2_generator() -> RescueFA2:
	return {'tokenContractAddress' : setSavingsPoolContract_generator (),'tokenId' : sendAllTokens_callback_generator (),'amount' : sendAllTokens_callback_generator (),'destination' : setSavingsPoolContract_generator ()}

def rescueFA2_encode(arg : RescueFA2):
	try:
		return PairType.from_comb([(setSavingsPoolContract_encode(arg['tokenContractAddress'])), PairType.from_comb([(sendAllTokens_callback_encode(arg['tokenId'])), PairType.from_comb([(sendAllTokens_callback_encode(arg['amount'])), (setSavingsPoolContract_encode(arg['destination']))])])])
	except Exception as e:
		print('Error in rescueFA2_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def rescueFA2_decode(arg) -> RescueFA2:
	try:
		before_projection=(factori_types.tuple2_decode (setSavingsPoolContract_decode,(factori_types.tuple2_decode (sendAllTokens_callback_decode,(factori_types.tuple2_decode (sendAllTokens_callback_decode,setSavingsPoolContract_decode))))))(arg)
		return {'tokenContractAddress' : (before_projection[0]),
'tokenId' : (before_projection[1][0]),
'amount' : (before_projection[1][1][0]),
'destination' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in rescueFA2_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_rescueFA2():
	temp=rescueFA2_generator()
	temp_encoded=rescueFA2_encode(temp)
	try:
		print('++++ rescueFA2: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of rescueFA2: " + str(temp))
	temp1=rescueFA2_decode(temp_encoded)
	if temp1 == temp:
		print('rescueFA2 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('rescueFA2 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_rescueFA2()
#except Exception as e:
#	print('Error in assert_identity_rescueFA2' + str(e))




def callRescueFA2(kt1 : str, _from : blockchain.identity, param : RescueFA2, networkName = "ghostnet", debug=False):
	input = rescueFA2_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "rescueFA2", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class RescueFA12(TypedDict):
	destination: SetSavingsPoolContract
	amount: SendAllTokens_callback
	tokenContractAddress: SetSavingsPoolContract

def rescueFA12_generator() -> RescueFA12:
	return {'destination' : setSavingsPoolContract_generator (),'amount' : sendAllTokens_callback_generator (),'tokenContractAddress' : setSavingsPoolContract_generator ()}

def rescueFA12_encode(arg : RescueFA12):
	try:
		return PairType.from_comb([(setSavingsPoolContract_encode(arg['tokenContractAddress'])), PairType.from_comb([(sendAllTokens_callback_encode(arg['amount'])), (setSavingsPoolContract_encode(arg['destination']))])])
	except Exception as e:
		print('Error in rescueFA12_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def rescueFA12_decode(arg) -> RescueFA12:
	try:
		before_projection=(factori_types.tuple2_decode (setSavingsPoolContract_decode,(factori_types.tuple2_decode (sendAllTokens_callback_decode,setSavingsPoolContract_decode))))(arg)
		return {'tokenContractAddress' : (before_projection[0]),
'amount' : (before_projection[1][0]),
'destination' : (before_projection[1][1])}


	except Exception as e:
		print('Error in rescueFA12_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_rescueFA12():
	temp=rescueFA12_generator()
	temp_encoded=rescueFA12_encode(temp)
	try:
		print('++++ rescueFA12: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of rescueFA12: " + str(temp))
	temp1=rescueFA12_decode(temp_encoded)
	if temp1 == temp:
		print('rescueFA12 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('rescueFA12 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_rescueFA12()
#except Exception as e:
#	print('Error in assert_identity_rescueFA12' + str(e))




def callRescueFA12(kt1 : str, _from : blockchain.identity, param : RescueFA12, networkName = "ghostnet", debug=False):
	input = rescueFA12_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "rescueFA12", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Liquidate = SetSavingsPoolContract


def liquidate_generator() -> Liquidate:
	return setSavingsPoolContract_generator ()


def liquidate_encode (arg: Liquidate):
	try:
		return setSavingsPoolContract_encode(arg)
	except Exception as e:
		print('Error in liquidate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def liquidate_decode (arg) -> Liquidate:
	try:
		return setSavingsPoolContract_decode(arg)
	except Exception as e:
		print('Error in liquidate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_liquidate():
	temp=liquidate_generator()
	temp_encoded=liquidate_encode(temp)
	try:
		print('++++ liquidate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of liquidate: " + str(temp))
	temp1=liquidate_decode(temp_encoded)
	if temp1 == temp:
		print('liquidate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('liquidate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_liquidate()
#except Exception as e:
#	print('Error in assert_identity_liquidate' + str(e))




def callLiquidate(kt1 : str, _from : blockchain.identity, param : Liquidate, networkName = "ghostnet", debug=False):
	input = liquidate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "liquidate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = factori_types.unit

def _default_generator() -> Constructor_default:
	return factori_types.unit_generator ()

def _default_encode (arg: Constructor_default):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _default_decode (arg) -> Constructor_default:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

AccrueInterest = SendAllTokens_callback


def accrueInterest_generator() -> AccrueInterest:
	return sendAllTokens_callback_generator ()


def accrueInterest_encode (arg: AccrueInterest):
	try:
		return sendAllTokens_callback_encode(arg)
	except Exception as e:
		print('Error in accrueInterest_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def accrueInterest_decode (arg) -> AccrueInterest:
	try:
		return sendAllTokens_callback_decode(arg)
	except Exception as e:
		print('Error in accrueInterest_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_accrueInterest():
	temp=accrueInterest_generator()
	temp_encoded=accrueInterest_encode(temp)
	try:
		print('++++ accrueInterest: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of accrueInterest: " + str(temp))
	temp1=accrueInterest_decode(temp_encoded)
	if temp1 == temp:
		print('accrueInterest check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('accrueInterest check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_accrueInterest()
#except Exception as e:
#	print('Error in assert_identity_accrueInterest' + str(e))




def callAccrueInterest(kt1 : str, _from : blockchain.identity, param : AccrueInterest, networkName = "ghostnet", debug=False):
	input = accrueInterest_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "accrueInterest", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

State = factori_types.int_michelson

def state_generator() -> State:
	return factori_types.int_generator ()

def state_encode (arg: State):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in state_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def state_decode (arg) -> State:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in state_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_state():
	temp=state_generator()
	temp_encoded=state_encode(temp)
	try:
		print('++++ state: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of state: " + str(temp))
	temp1=state_decode(temp_encoded)
	if temp1 == temp:
		print('state check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('state check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_state()
#except Exception as e:
#	print('Error in assert_identity_state' + str(e))




class Constructor_storage(TypedDict):
	sendAllTokens_destination: Optional[SetSavingsPoolContract]
	savingsPoolContractAddress: SetSavingsPoolContract
	ovenRegistryContractAddress: SetSavingsPoolContract
	governorContractAddress: SetSavingsPoolContract
	administratorContractAddress: SetSavingsPoolContract
	state: State
	tokenContractAddress: SetSavingsPoolContract

def _storage_generator() -> Constructor_storage:
	return {'sendAllTokens_destination' : (factori_types.option_generator(setSavingsPoolContract_generator)) (),'savingsPoolContractAddress' : setSavingsPoolContract_generator (),'ovenRegistryContractAddress' : setSavingsPoolContract_generator (),'governorContractAddress' : setSavingsPoolContract_generator (),'administratorContractAddress' : setSavingsPoolContract_generator (),'state' : state_generator (),'tokenContractAddress' : setSavingsPoolContract_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([(setSavingsPoolContract_encode(arg['administratorContractAddress'])), PairType.from_comb([(setSavingsPoolContract_encode(arg['governorContractAddress'])), (setSavingsPoolContract_encode(arg['ovenRegistryContractAddress']))])]), PairType.from_comb([PairType.from_comb([(setSavingsPoolContract_encode(arg['savingsPoolContractAddress'])), (factori_types.option_encode(setSavingsPoolContract_encode)(arg['sendAllTokens_destination']))]), PairType.from_comb([(state_encode(arg['state'])), (setSavingsPoolContract_encode(arg['tokenContractAddress']))])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode (setSavingsPoolContract_decode,(factori_types.tuple2_decode (setSavingsPoolContract_decode,setSavingsPoolContract_decode)))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (setSavingsPoolContract_decode,factori_types.option_decode(setSavingsPoolContract_decode))),(factori_types.tuple2_decode (state_decode,setSavingsPoolContract_decode))))))(arg)
		return {'administratorContractAddress' : (before_projection[0][0]),
'governorContractAddress' : (before_projection[0][1][0]),
'ovenRegistryContractAddress' : (before_projection[0][1][1]),
'savingsPoolContractAddress' : (before_projection[1][0][0]),
'sendAllTokens_destination' : (before_projection[1][0][1]),
'state' : (before_projection[1][1][0]),
'tokenContractAddress' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract stability_fund")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/stability_fund_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1