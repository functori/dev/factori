open Tzfunc.Proto
open Factori_types

(*Type definition for token_id *)
type token_id = nat

(** Encode elements of type token_id into micheline *)
val token_id_encode : token_id -> micheline

(** Decode elements of type micheline as token_id *)
val token_id_decode : micheline -> token_id

(** Generate random elements of type token_id*)
val token_id_generator : unit -> token_id

(** The micheline type corresponding to type token_id*)
val token_id_micheline : micheline

(*Type definition for _operator *)
type _operator = address

(** Encode elements of type _operator into micheline *)
val _operator_encode : _operator -> micheline

(** Decode elements of type micheline as _operator *)
val _operator_decode : micheline -> _operator

(** Generate random elements of type _operator*)
val _operator_generator : unit -> _operator

(** The micheline type corresponding to type _operator*)
val _operator_micheline : micheline

(*Type definition for add_operator *)

type add_operator = {
  token_id : token_id;
  _operator : _operator;
  owner : _operator;
}

(** Encode elements of type add_operator into micheline *)
val add_operator_encode : add_operator -> micheline

(** Decode elements of type micheline as add_operator *)
val add_operator_decode : micheline -> add_operator

(** Generate random elements of type add_operator*)
val add_operator_generator : unit -> add_operator

(** The micheline type corresponding to type add_operator*)
val add_operator_micheline : micheline

(*Type definition for type0 *)

type type0 =
  | Add_operator of add_operator
  | Remove_operator of add_operator

(** Encode elements of type type0 into micheline *)
val type0_encode : type0 -> micheline

(** Decode elements of type micheline as type0 *)
val type0_decode : micheline -> type0

(** Generate random elements of type type0*)
val type0_generator : unit -> type0

(** The micheline type corresponding to type type0*)
val type0_micheline : micheline

(*Type definition for update_operators *)
type update_operators = type0 list

(** Encode elements of type update_operators into micheline *)
val update_operators_encode : update_operators -> micheline

(** Decode elements of type micheline as update_operators *)
val update_operators_decode : micheline -> update_operators

(** Generate random elements of type update_operators*)
val update_operators_generator : unit -> update_operators

(** The micheline type corresponding to type update_operators*)
val update_operators_micheline : micheline

(** call entrypoint update_operators of the smart contract. *)
val call_update_operators :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  update_operators ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint update_operators of the smart contract. *)
val forge_call_update_operators :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  update_operators ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_update_operators :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  update_operators ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_update_operators :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  update_operators ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_update_operators :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  update_operators ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_update_operators :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:update_operators ->
  string Ast.astobj

(*Type definition for type1 *)

type type1 = {
  amount : token_id;
  token_id : token_id;
  to_ : _operator;
}

(** Encode elements of type type1 into micheline *)
val type1_encode : type1 -> micheline

(** Decode elements of type micheline as type1 *)
val type1_decode : micheline -> type1

(** Generate random elements of type type1*)
val type1_generator : unit -> type1

(** The micheline type corresponding to type type1*)
val type1_micheline : micheline

(*Type definition for type2 *)

type type2 = {
  from_ : _operator;
  txs : type1 list;
}

(** Encode elements of type type2 into micheline *)
val type2_encode : type2 -> micheline

(** Decode elements of type micheline as type2 *)
val type2_decode : micheline -> type2

(** Generate random elements of type type2*)
val type2_generator : unit -> type2

(** The micheline type corresponding to type type2*)
val type2_micheline : micheline

(*Type definition for transfer *)
type transfer = type2 list

(** Encode elements of type transfer into micheline *)
val transfer_encode : transfer -> micheline

(** Decode elements of type micheline as transfer *)
val transfer_decode : micheline -> transfer

(** Generate random elements of type transfer*)
val transfer_generator : unit -> transfer

(** The micheline type corresponding to type transfer*)
val transfer_micheline : micheline

(** call entrypoint transfer of the smart contract. *)
val call_transfer :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  transfer ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint transfer of the smart contract. *)
val forge_call_transfer :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  transfer ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_transfer :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  transfer ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_transfer :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  transfer ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_transfer :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  transfer ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_transfer :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:transfer ->
  string Ast.astobj

(*Type definition for type3 *)

type type3 = {
  token_id : token_id;
  token_info : (string, bytes) map;
}

(** Encode elements of type type3 into micheline *)
val type3_encode : type3 -> micheline

(** Decode elements of type micheline as type3 *)
val type3_decode : micheline -> type3

(** Generate random elements of type type3*)
val type3_generator : unit -> type3

(** The micheline type corresponding to type type3*)
val type3_micheline : micheline

(*Type definition for token_metadata *)

type token_metadata = {
  token_ids : token_id list;
  handler : Factori_types.lambda;
}

(** Encode elements of type token_metadata into micheline *)
val token_metadata_encode : token_metadata -> micheline

(** Decode elements of type micheline as token_metadata *)
val token_metadata_decode : micheline -> token_metadata

(** Generate random elements of type token_metadata*)
val token_metadata_generator : unit -> token_metadata

(** The micheline type corresponding to type token_metadata*)
val token_metadata_micheline : micheline

(** call entrypoint token_metadata of the smart contract. *)
val call_token_metadata :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  token_metadata ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint token_metadata of the smart contract. *)
val forge_call_token_metadata :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  token_metadata ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_token_metadata :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  token_metadata ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_token_metadata :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  token_metadata ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_token_metadata :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  token_metadata ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_token_metadata :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:token_metadata ->
  string Ast.astobj

(*Type definition for set_pause *)
type set_pause = bool

(** Encode elements of type set_pause into micheline *)
val set_pause_encode : set_pause -> micheline

(** Decode elements of type micheline as set_pause *)
val set_pause_decode : micheline -> set_pause

(** Generate random elements of type set_pause*)
val set_pause_generator : unit -> set_pause

(** The micheline type corresponding to type set_pause*)
val set_pause_micheline : micheline

(** call entrypoint set_pause of the smart contract. *)
val call_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_pause ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint set_pause of the smart contract. *)
val forge_call_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_pause ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  set_pause ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  set_pause ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  set_pause ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_set_pause :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:set_pause ->
  string Ast.astobj

(*Type definition for set_administrator *)
type set_administrator = _operator

(** Encode elements of type set_administrator into micheline *)
val set_administrator_encode : set_administrator -> micheline

(** Decode elements of type micheline as set_administrator *)
val set_administrator_decode : micheline -> set_administrator

(** Generate random elements of type set_administrator*)
val set_administrator_generator : unit -> set_administrator

(** The micheline type corresponding to type set_administrator*)
val set_administrator_micheline : micheline

(** call entrypoint set_administrator of the smart contract. *)
val call_set_administrator :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_administrator ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint set_administrator of the smart contract. *)
val forge_call_set_administrator :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_administrator ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_set_administrator :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  set_administrator ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_set_administrator :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  set_administrator ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_set_administrator :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  set_administrator ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_set_administrator :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:set_administrator ->
  string Ast.astobj

(*Type definition for payout_balance *)
type payout_balance = unit

(** Encode elements of type payout_balance into micheline *)
val payout_balance_encode : payout_balance -> micheline

(** Decode elements of type micheline as payout_balance *)
val payout_balance_decode : micheline -> payout_balance

(** Generate random elements of type payout_balance*)
val payout_balance_generator : unit -> payout_balance

(** The micheline type corresponding to type payout_balance*)
val payout_balance_micheline : micheline

(** call entrypoint payout_balance of the smart contract. *)
val call_payout_balance :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  payout_balance ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint payout_balance of the smart contract. *)
val forge_call_payout_balance :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  payout_balance ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_payout_balance :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  payout_balance ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_payout_balance :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  payout_balance ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_payout_balance :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  payout_balance ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_payout_balance :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:payout_balance ->
  string Ast.astobj

(*Type definition for mint *)

type mint = {
  token_info : (string, bytes) map;
  token_id : token_id;
  address : _operator;
  amount : token_id;
}

(** Encode elements of type mint into micheline *)
val mint_encode : mint -> micheline

(** Decode elements of type micheline as mint *)
val mint_decode : micheline -> mint

(** Generate random elements of type mint*)
val mint_generator : unit -> mint

(** The micheline type corresponding to type mint*)
val mint_micheline : micheline

(** call entrypoint mint of the smart contract. *)
val call_mint :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  mint ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint mint of the smart contract. *)
val forge_call_mint :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  mint ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_mint :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  mint ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_mint :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  mint ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_mint :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  mint ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_mint :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:mint ->
  string Ast.astobj

(*Type definition for burn *)

type burn = {
  token_id : token_id;
  amount : token_id;
  address : _operator;
}

(** Encode elements of type burn into micheline *)
val burn_encode : burn -> micheline

(** Decode elements of type micheline as burn *)
val burn_decode : micheline -> burn

(** Generate random elements of type burn*)
val burn_generator : unit -> burn

(** The micheline type corresponding to type burn*)
val burn_micheline : micheline

(** call entrypoint burn of the smart contract. *)
val call_burn :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  burn ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint burn of the smart contract. *)
val forge_call_burn :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  burn ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_burn :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  burn ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_burn :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  burn ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_burn :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  burn ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_burn :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:burn ->
  string Ast.astobj

(*Type definition for type9 *)

type type9 = {
  owner : _operator;
  token_id : token_id;
}

(** Encode elements of type type9 into micheline *)
val type9_encode : type9 -> micheline

(** Decode elements of type micheline as type9 *)
val type9_decode : micheline -> type9

(** Generate random elements of type type9*)
val type9_generator : unit -> type9

(** The micheline type corresponding to type type9*)
val type9_micheline : micheline

(*Type definition for balance_of *)

type balance_of = {
  requests : type9 list;
  callback : contract;
}

(** Encode elements of type balance_of into micheline *)
val balance_of_encode : balance_of -> micheline

(** Decode elements of type micheline as balance_of *)
val balance_of_decode : micheline -> balance_of

(** Generate random elements of type balance_of*)
val balance_of_generator : unit -> balance_of

(** The micheline type corresponding to type balance_of*)
val balance_of_micheline : micheline

(** call entrypoint balance_of of the smart contract. *)
val call_balance_of :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  balance_of ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint balance_of of the smart contract. *)
val forge_call_balance_of :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  balance_of ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_balance_of :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  balance_of ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_balance_of :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  balance_of ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_balance_of :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  balance_of ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_balance_of :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:balance_of ->
  string Ast.astobj

(*Type definition for _storage *)

type _storage = {
  paused : set_pause;
  operators : (add_operator, payout_balance) big_map;
  metadata : (string, bytes) big_map;
  ledger : (_operator * token_id, token_id) big_map;
  administrator : _operator;
  all_tokens : token_id;
  payout : _operator;
  token_metadata : (token_id, type3) big_map;
}

(** Encode elements of type _storage into micheline *)
val _storage_encode : _storage -> micheline

(** Decode elements of type micheline as _storage *)
val _storage_decode : micheline -> _storage

(** Generate random elements of type _storage*)
val _storage_generator : unit -> _storage

(** The micheline type corresponding to type _storage*)
val _storage_micheline : micheline

(** A function to deploy the smart contract.
           - amount is the initial balance of the contract
           - node allows to choose on which chain we are deploying
           - name allows to choose a name for the contract you are deploying
           - from is the account which will originate the contract (and pay for its origination)
           The function returns a pair (kt1,op_hash) where kt1 is the address of the contract
           and op_hash is the hash of the origination operation
       *)
val deploy :
  ?amount:int64 ->
  ?node:string ->
  ?name:string ->
  ?from:Blockchain.identity ->
  _storage ->
  (string * string, Tzfunc__.Rp.error) result Lwt.t

val mk_deploy :
  Scenario_dsl.AstInterface.scenario ->
  from:Scenario_value.identity Ast.astobj ->
  amount:Z.t Ast.astobj ->
  network:Scenario_value.network Ast.astobj ->
  storage:_storage ->
  string Ast.astobj

(** Downloads and decodes the storage, and then reencodes it.
Allows to check the robustness of the encoding and decoding functions. *)
val test_storage_download :
  kt1:Proto.A.contract ->
  node:string ->
  unit ->
  (unit, Tzfunc__.Rp.error) result

(** Initial storage as seen on the blockchain at the time of import. One possible use of this value is to copy it and modify it easily. *)
val initial_blockchain_storage : _storage
