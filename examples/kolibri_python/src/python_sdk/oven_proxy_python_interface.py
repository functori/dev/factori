from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

Withdraw_callback = factori_types.nat

def withdraw_callback_generator() -> Withdraw_callback:
	return factori_types.nat_generator ()

def withdraw_callback_encode (arg: Withdraw_callback):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in withdraw_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def withdraw_callback_decode (arg) -> Withdraw_callback:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in withdraw_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_withdraw_callback():
	temp=withdraw_callback_generator()
	temp_encoded=withdraw_callback_encode(temp)
	try:
		print('++++ withdraw_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of withdraw_callback: " + str(temp))
	temp1=withdraw_callback_decode(temp_encoded)
	if temp1 == temp:
		print('withdraw_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('withdraw_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_withdraw_callback()
#except Exception as e:
#	print('Error in assert_identity_withdraw_callback' + str(e))




def callWithdraw_callback(kt1 : str, _from : blockchain.identity, param : Withdraw_callback, networkName = "ghostnet", debug=False):
	input = withdraw_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "withdraw_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Withdraw = Tuple[factori_types.address,Tuple[factori_types.address,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,factori_types.tez]]]]]]]

def withdraw_generator() -> Withdraw:
	return (factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
factori_types.tez_generator))))))))))))))()

def withdraw_encode(arg : Withdraw) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tez_encode)))))))(arg) 
	except Exception as e:
		print('Error in withdraw_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def withdraw_decode(arg) -> Withdraw:
	try:
		return factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tez_decode)))))))(arg) 
	except Exception as e:
		print('Error in withdraw_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_withdraw():
	temp=withdraw_generator()
	temp_encoded=withdraw_encode(temp)
	try:
		print('++++ withdraw: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of withdraw: " + str(temp))
	temp1=withdraw_decode(temp_encoded)
	if temp1 == temp:
		print('withdraw check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('withdraw check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_withdraw()
#except Exception as e:
#	print('Error in assert_identity_withdraw' + str(e))




def callWithdraw(kt1 : str, _from : blockchain.identity, param : Withdraw, networkName = "ghostnet", debug=False):
	input = withdraw_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "withdraw", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateState = Tuple[factori_types.address,Tuple[Withdraw_callback,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,bool]]]]

def updateState_generator() -> UpdateState:
	return (factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
factori_types.bool_generator))))))))()

def updateState_encode(arg : UpdateState) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.bool_encode))))(arg) 
	except Exception as e:
		print('Error in updateState_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateState_decode(arg) -> UpdateState:
	try:
		return factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.bool_decode))))(arg) 
	except Exception as e:
		print('Error in updateState_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateState():
	temp=updateState_generator()
	temp_encoded=updateState_encode(temp)
	try:
		print('++++ updateState: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateState: " + str(temp))
	temp1=updateState_decode(temp_encoded)
	if temp1 == temp:
		print('updateState check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateState check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateState()
#except Exception as e:
#	print('Error in assert_identity_updateState' + str(e))




def callUpdateState(kt1 : str, _from : blockchain.identity, param : UpdateState, networkName = "ghostnet", debug=False):
	input = updateState_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateState", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Unpause = factori_types.unit

def unpause_generator() -> Unpause:
	return factori_types.unit_generator ()

def unpause_encode (arg: Unpause):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in unpause_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def unpause_decode (arg) -> Unpause:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in unpause_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_unpause():
	temp=unpause_generator()
	temp_encoded=unpause_encode(temp)
	try:
		print('++++ unpause: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of unpause: " + str(temp))
	temp1=unpause_decode(temp_encoded)
	if temp1 == temp:
		print('unpause check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('unpause check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_unpause()
#except Exception as e:
#	print('Error in assert_identity_unpause' + str(e))




def callUnpause(kt1 : str, _from : blockchain.identity, param : Unpause, networkName = "ghostnet", debug=False):
	input = unpause_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "unpause", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetPauseGuardianContract = factori_types.address

def setPauseGuardianContract_generator() -> SetPauseGuardianContract:
	return factori_types.address_generator ()

def setPauseGuardianContract_encode (arg: SetPauseGuardianContract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in setPauseGuardianContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setPauseGuardianContract_decode (arg) -> SetPauseGuardianContract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in setPauseGuardianContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setPauseGuardianContract():
	temp=setPauseGuardianContract_generator()
	temp_encoded=setPauseGuardianContract_encode(temp)
	try:
		print('++++ setPauseGuardianContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setPauseGuardianContract: " + str(temp))
	temp1=setPauseGuardianContract_decode(temp_encoded)
	if temp1 == temp:
		print('setPauseGuardianContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setPauseGuardianContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setPauseGuardianContract()
#except Exception as e:
#	print('Error in assert_identity_setPauseGuardianContract' + str(e))




def callSetPauseGuardianContract(kt1 : str, _from : blockchain.identity, param : SetPauseGuardianContract, networkName = "ghostnet", debug=False):
	input = setPauseGuardianContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setPauseGuardianContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetOvenRegistryContract = SetPauseGuardianContract


def setOvenRegistryContract_generator() -> SetOvenRegistryContract:
	return setPauseGuardianContract_generator ()


def setOvenRegistryContract_encode (arg: SetOvenRegistryContract):
	try:
		return setPauseGuardianContract_encode(arg)
	except Exception as e:
		print('Error in setOvenRegistryContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setOvenRegistryContract_decode (arg) -> SetOvenRegistryContract:
	try:
		return setPauseGuardianContract_decode(arg)
	except Exception as e:
		print('Error in setOvenRegistryContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setOvenRegistryContract():
	temp=setOvenRegistryContract_generator()
	temp_encoded=setOvenRegistryContract_encode(temp)
	try:
		print('++++ setOvenRegistryContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setOvenRegistryContract: " + str(temp))
	temp1=setOvenRegistryContract_decode(temp_encoded)
	if temp1 == temp:
		print('setOvenRegistryContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setOvenRegistryContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setOvenRegistryContract()
#except Exception as e:
#	print('Error in assert_identity_setOvenRegistryContract' + str(e))




def callSetOvenRegistryContract(kt1 : str, _from : blockchain.identity, param : SetOvenRegistryContract, networkName = "ghostnet", debug=False):
	input = setOvenRegistryContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setOvenRegistryContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetOracleContract = SetPauseGuardianContract


def setOracleContract_generator() -> SetOracleContract:
	return setPauseGuardianContract_generator ()


def setOracleContract_encode (arg: SetOracleContract):
	try:
		return setPauseGuardianContract_encode(arg)
	except Exception as e:
		print('Error in setOracleContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setOracleContract_decode (arg) -> SetOracleContract:
	try:
		return setPauseGuardianContract_decode(arg)
	except Exception as e:
		print('Error in setOracleContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setOracleContract():
	temp=setOracleContract_generator()
	temp_encoded=setOracleContract_encode(temp)
	try:
		print('++++ setOracleContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setOracleContract: " + str(temp))
	temp1=setOracleContract_decode(temp_encoded)
	if temp1 == temp:
		print('setOracleContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setOracleContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setOracleContract()
#except Exception as e:
#	print('Error in assert_identity_setOracleContract' + str(e))




def callSetOracleContract(kt1 : str, _from : blockchain.identity, param : SetOracleContract, networkName = "ghostnet", debug=False):
	input = setOracleContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setOracleContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetMinterContract = SetPauseGuardianContract


def setMinterContract_generator() -> SetMinterContract:
	return setPauseGuardianContract_generator ()


def setMinterContract_encode (arg: SetMinterContract):
	try:
		return setPauseGuardianContract_encode(arg)
	except Exception as e:
		print('Error in setMinterContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setMinterContract_decode (arg) -> SetMinterContract:
	try:
		return setPauseGuardianContract_decode(arg)
	except Exception as e:
		print('Error in setMinterContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setMinterContract():
	temp=setMinterContract_generator()
	temp_encoded=setMinterContract_encode(temp)
	try:
		print('++++ setMinterContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setMinterContract: " + str(temp))
	temp1=setMinterContract_decode(temp_encoded)
	if temp1 == temp:
		print('setMinterContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setMinterContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setMinterContract()
#except Exception as e:
#	print('Error in assert_identity_setMinterContract' + str(e))




def callSetMinterContract(kt1 : str, _from : blockchain.identity, param : SetMinterContract, networkName = "ghostnet", debug=False):
	input = setMinterContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setMinterContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = SetPauseGuardianContract


def setGovernorContract_generator() -> SetGovernorContract:
	return setPauseGuardianContract_generator ()


def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return setPauseGuardianContract_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return setPauseGuardianContract_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Repay = Tuple[SetPauseGuardianContract,Tuple[SetPauseGuardianContract,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,Withdraw_callback]]]]]]]

def repay_generator() -> Repay:
	return (factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
withdraw_callback_generator))))))))))))))()

def repay_encode(arg : Repay) -> PairType:
	try:
		return factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,withdraw_callback_encode)))))))(arg) 
	except Exception as e:
		print('Error in repay_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def repay_decode(arg) -> Repay:
	try:
		return factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,withdraw_callback_decode)))))))(arg) 
	except Exception as e:
		print('Error in repay_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_repay():
	temp=repay_generator()
	temp_encoded=repay_encode(temp)
	try:
		print('++++ repay: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of repay: " + str(temp))
	temp1=repay_decode(temp_encoded)
	if temp1 == temp:
		print('repay check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('repay check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_repay()
#except Exception as e:
#	print('Error in assert_identity_repay' + str(e))




def callRepay(kt1 : str, _from : blockchain.identity, param : Repay, networkName = "ghostnet", debug=False):
	input = repay_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "repay", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Pause = Unpause


def pause_generator() -> Pause:
	return unpause_generator ()


def pause_encode (arg: Pause):
	try:
		return unpause_encode(arg)
	except Exception as e:
		print('Error in pause_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def pause_decode (arg) -> Pause:
	try:
		return unpause_decode(arg)
	except Exception as e:
		print('Error in pause_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_pause():
	temp=pause_generator()
	temp_encoded=pause_encode(temp)
	try:
		print('++++ pause: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of pause: " + str(temp))
	temp1=pause_decode(temp_encoded)
	if temp1 == temp:
		print('pause check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('pause check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_pause()
#except Exception as e:
#	print('Error in assert_identity_pause' + str(e))




def callPause(kt1 : str, _from : blockchain.identity, param : Pause, networkName = "ghostnet", debug=False):
	input = pause_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "pause", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Liquidate_callback = Withdraw_callback


def liquidate_callback_generator() -> Liquidate_callback:
	return withdraw_callback_generator ()


def liquidate_callback_encode (arg: Liquidate_callback):
	try:
		return withdraw_callback_encode(arg)
	except Exception as e:
		print('Error in liquidate_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def liquidate_callback_decode (arg) -> Liquidate_callback:
	try:
		return withdraw_callback_decode(arg)
	except Exception as e:
		print('Error in liquidate_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_liquidate_callback():
	temp=liquidate_callback_generator()
	temp_encoded=liquidate_callback_encode(temp)
	try:
		print('++++ liquidate_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of liquidate_callback: " + str(temp))
	temp1=liquidate_callback_decode(temp_encoded)
	if temp1 == temp:
		print('liquidate_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('liquidate_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_liquidate_callback()
#except Exception as e:
#	print('Error in assert_identity_liquidate_callback' + str(e))




def callLiquidate_callback(kt1 : str, _from : blockchain.identity, param : Liquidate_callback, networkName = "ghostnet", debug=False):
	input = liquidate_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "liquidate_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Liquidate = Tuple[SetPauseGuardianContract,Tuple[SetPauseGuardianContract,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,SetPauseGuardianContract]]]]]]]

def liquidate_generator() -> Liquidate:
	return (factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
setPauseGuardianContract_generator))))))))))))))()

def liquidate_encode(arg : Liquidate) -> PairType:
	try:
		return factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,setPauseGuardianContract_encode)))))))(arg) 
	except Exception as e:
		print('Error in liquidate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def liquidate_decode(arg) -> Liquidate:
	try:
		return factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,setPauseGuardianContract_decode)))))))(arg) 
	except Exception as e:
		print('Error in liquidate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_liquidate():
	temp=liquidate_generator()
	temp_encoded=liquidate_encode(temp)
	try:
		print('++++ liquidate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of liquidate: " + str(temp))
	temp1=liquidate_decode(temp_encoded)
	if temp1 == temp:
		print('liquidate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('liquidate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_liquidate()
#except Exception as e:
#	print('Error in assert_identity_liquidate' + str(e))




def callLiquidate(kt1 : str, _from : blockchain.identity, param : Liquidate, networkName = "ghostnet", debug=False):
	input = liquidate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "liquidate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Deposit = Tuple[SetPauseGuardianContract,Tuple[SetPauseGuardianContract,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[bool,Tuple[factori_types.int_michelson,factori_types.int_michelson]]]]]]

def deposit_generator() -> Deposit:
	return (factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
factori_types.int_generator))))))))))))()

def deposit_encode(arg : Deposit) -> PairType:
	try:
		return factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.int_michelson_encode))))))(arg) 
	except Exception as e:
		print('Error in deposit_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def deposit_decode(arg) -> Deposit:
	try:
		return factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.int_michelson_decode))))))(arg) 
	except Exception as e:
		print('Error in deposit_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_deposit():
	temp=deposit_generator()
	temp_encoded=deposit_encode(temp)
	try:
		print('++++ deposit: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of deposit: " + str(temp))
	temp1=deposit_decode(temp_encoded)
	if temp1 == temp:
		print('deposit check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('deposit check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_deposit()
#except Exception as e:
#	print('Error in assert_identity_deposit' + str(e))




def callDeposit(kt1 : str, _from : blockchain.identity, param : Deposit, networkName = "ghostnet", debug=False):
	input = deposit_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "deposit", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = Unpause


def _default_generator() -> Constructor_default:
	return unpause_generator ()


def _default_encode (arg: Constructor_default):
	try:
		return unpause_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def _default_decode (arg) -> Constructor_default:
	try:
		return unpause_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Borrow_callback = Withdraw_callback


def borrow_callback_generator() -> Borrow_callback:
	return withdraw_callback_generator ()


def borrow_callback_encode (arg: Borrow_callback):
	try:
		return withdraw_callback_encode(arg)
	except Exception as e:
		print('Error in borrow_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def borrow_callback_decode (arg) -> Borrow_callback:
	try:
		return withdraw_callback_decode(arg)
	except Exception as e:
		print('Error in borrow_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_borrow_callback():
	temp=borrow_callback_generator()
	temp_encoded=borrow_callback_encode(temp)
	try:
		print('++++ borrow_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of borrow_callback: " + str(temp))
	temp1=borrow_callback_decode(temp_encoded)
	if temp1 == temp:
		print('borrow_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('borrow_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_borrow_callback()
#except Exception as e:
#	print('Error in assert_identity_borrow_callback' + str(e))




def callBorrow_callback(kt1 : str, _from : blockchain.identity, param : Borrow_callback, networkName = "ghostnet", debug=False):
	input = borrow_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "borrow_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Borrow = Tuple[SetPauseGuardianContract,Tuple[SetPauseGuardianContract,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,Withdraw_callback]]]]]]]

def borrow_generator() -> Borrow:
	return (factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
withdraw_callback_generator))))))))))))))()

def borrow_encode(arg : Borrow) -> PairType:
	try:
		return factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,withdraw_callback_encode)))))))(arg) 
	except Exception as e:
		print('Error in borrow_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def borrow_decode(arg) -> Borrow:
	try:
		return factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,withdraw_callback_decode)))))))(arg) 
	except Exception as e:
		print('Error in borrow_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_borrow():
	temp=borrow_generator()
	temp_encoded=borrow_encode(temp)
	try:
		print('++++ borrow: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of borrow: " + str(temp))
	temp1=borrow_decode(temp_encoded)
	if temp1 == temp:
		print('borrow check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('borrow check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_borrow()
#except Exception as e:
#	print('Error in assert_identity_borrow' + str(e))




def callBorrow(kt1 : str, _from : blockchain.identity, param : Borrow, networkName = "ghostnet", debug=False):
	input = borrow_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "borrow", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

State = factori_types.int_michelson

def state_generator() -> State:
	return factori_types.int_generator ()

def state_encode (arg: State):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in state_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def state_decode (arg) -> State:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in state_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_state():
	temp=state_generator()
	temp_encoded=state_encode(temp)
	try:
		print('++++ state: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of state: " + str(temp))
	temp1=state_decode(temp_encoded)
	if temp1 == temp:
		print('state check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('state check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_state()
#except Exception as e:
#	print('Error in assert_identity_state' + str(e))




Paused = bool

def paused_generator() -> Paused:
	return factori_types.bool_generator ()

def paused_encode (arg: Paused):
	try:
		return factori_types.bool_encode(arg)
	except Exception as e:
		print('Error in paused_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def paused_decode (arg) -> Paused:
	try:
		return factori_types.bool_decode(arg)
	except Exception as e:
		print('Error in paused_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_paused():
	temp=paused_generator()
	temp_encoded=paused_encode(temp)
	try:
		print('++++ paused: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of paused: " + str(temp))
	temp1=paused_decode(temp_encoded)
	if temp1 == temp:
		print('paused check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('paused check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_paused()
#except Exception as e:
#	print('Error in assert_identity_paused' + str(e))




class Constructor_storage(TypedDict):
	withdrawParams: Optional[Tuple[SetPauseGuardianContract,Tuple[SetPauseGuardianContract,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,factori_types.tez]]]]]]]]
	state: State
	paused: Paused
	oracleContractAddress: SetPauseGuardianContract
	minterContractAddress: SetPauseGuardianContract
	liquidateParams: Optional[Tuple[SetPauseGuardianContract,Tuple[SetPauseGuardianContract,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[Paused,Tuple[State,Tuple[State,SetPauseGuardianContract]]]]]]]]
	borrowParams: Optional[Tuple[SetPauseGuardianContract,Tuple[SetPauseGuardianContract,Tuple[Withdraw_callback,Tuple[Withdraw_callback,Tuple[Paused,Tuple[State,Tuple[State,Withdraw_callback]]]]]]]]
	governorContractAddress: SetPauseGuardianContract
	ovenRegistryContractAddress: SetPauseGuardianContract
	pauseGuardianContractAddress: SetPauseGuardianContract

def _storage_generator() -> Constructor_storage:
	return {'withdrawParams' : (factori_types.option_generator((factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
factori_types.tez_generator)))))))))))))))) (),'state' : state_generator (),'paused' : paused_generator (),'oracleContractAddress' : setPauseGuardianContract_generator (),'minterContractAddress' : setPauseGuardianContract_generator (),'liquidateParams' : (factori_types.option_generator((factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(paused_generator,
(factori_types.tuple2_generator(state_generator,
(factori_types.tuple2_generator(state_generator,
setPauseGuardianContract_generator)))))))))))))))) (),'borrowParams' : (factori_types.option_generator((factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(setPauseGuardianContract_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(withdraw_callback_generator,
(factori_types.tuple2_generator(paused_generator,
(factori_types.tuple2_generator(state_generator,
(factori_types.tuple2_generator(state_generator,
withdraw_callback_generator)))))))))))))))) (),'governorContractAddress' : setPauseGuardianContract_generator (),'ovenRegistryContractAddress' : setPauseGuardianContract_generator (),'pauseGuardianContractAddress' : setPauseGuardianContract_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([PairType.from_comb([(factori_types.option_encode(factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(paused_encode,factori_types.tuple2_encode(state_encode,factori_types.tuple2_encode(state_encode,withdraw_callback_encode))))))))(arg['borrowParams'])), (setPauseGuardianContract_encode(arg['governorContractAddress']))]), PairType.from_comb([(factori_types.option_encode(factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(paused_encode,factori_types.tuple2_encode(state_encode,factori_types.tuple2_encode(state_encode,setPauseGuardianContract_encode))))))))(arg['liquidateParams'])), PairType.from_comb([(setPauseGuardianContract_encode(arg['minterContractAddress'])), (setPauseGuardianContract_encode(arg['oracleContractAddress']))])])]), PairType.from_comb([PairType.from_comb([(setPauseGuardianContract_encode(arg['ovenRegistryContractAddress'])), (setPauseGuardianContract_encode(arg['pauseGuardianContractAddress']))]), PairType.from_comb([(paused_encode(arg['paused'])), PairType.from_comb([(state_encode(arg['state'])), (factori_types.option_encode(factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(setPauseGuardianContract_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(withdraw_callback_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tez_encode))))))))(arg['withdrawParams']))])])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.option_decode(factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(paused_decode,factori_types.tuple2_decode(state_decode,factori_types.tuple2_decode(state_decode,withdraw_callback_decode)))))))),setPauseGuardianContract_decode)),(factori_types.tuple2_decode (factori_types.option_decode(factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(paused_decode,factori_types.tuple2_decode(state_decode,factori_types.tuple2_decode(state_decode,setPauseGuardianContract_decode)))))))),(factori_types.tuple2_decode (setPauseGuardianContract_decode,setPauseGuardianContract_decode)))))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (setPauseGuardianContract_decode,setPauseGuardianContract_decode)),(factori_types.tuple2_decode (paused_decode,(factori_types.tuple2_decode (state_decode,factori_types.option_decode(factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(setPauseGuardianContract_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(withdraw_callback_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tez_decode))))))))))))))))(arg)
		return {'borrowParams' : (before_projection[0][0][0]),
'governorContractAddress' : (before_projection[0][0][1]),
'liquidateParams' : (before_projection[0][1][0]),
'minterContractAddress' : (before_projection[0][1][1][0]),
'oracleContractAddress' : (before_projection[0][1][1][1]),
'ovenRegistryContractAddress' : (before_projection[1][0][0]),
'pauseGuardianContractAddress' : (before_projection[1][0][1]),
'paused' : (before_projection[1][1][0]),
'state' : (before_projection[1][1][1][0]),
'withdrawParams' : (before_projection[1][1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract oven_proxy")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/oven_proxy_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1