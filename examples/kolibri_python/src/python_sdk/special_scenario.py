from importlib.metadata import metadata
import minter_python_interface
import test_python_interface
import deploy_scenario_minter
import deploy_scenario_test
import time
import blockchain
import factori_types


def main():
    kt1_minter= deploy_scenario_minter.exec()
    time.sleep(2)
    kt1_test= deploy_scenario_test.exec()
    time.sleep(2)
    op_hash=test_python_interface.callSet_contract(kt1_test,blockchain.alice,kt1_minter,networkName="flextesa")
    time.sleep(2)
    op_hash=test_python_interface.callMakecall(kt1_test,blockchain.alice,test_python_interface.makecall_generator(),networkName="flextesa")
main()
