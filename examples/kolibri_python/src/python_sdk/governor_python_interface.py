from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

RunLambda = factori_types.Lambda

def runLambda_generator() -> RunLambda:
	return (factori_types.lambda_generator (factori_types.unit_generator,(factori_types.list_generator(factori_types.operation_generator)))) ()

def runLambda_encode (arg: RunLambda):
	try:
		return factori_types.lambda_encode(factori_types.unit_encode,factori_types.list_encode(factori_types.operation_encode))(arg)		
	except Exception as e:
		print('Error in runLambda_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def runLambda_decode (arg) -> RunLambda:
	try:
		return factori_types.lambda_decode(factori_types.unit_decode,factori_types.list_decode(factori_types.operation_decode))(arg)
	except Exception as e:
		print('Error in runLambda_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_runLambda():
	temp=runLambda_generator()
	temp_encoded=runLambda_encode(temp)
	try:
		print('++++ runLambda: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of runLambda: " + str(temp))
	temp1=runLambda_decode(temp_encoded)
	if temp1 == temp:
		print('runLambda check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('runLambda check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_runLambda()
#except Exception as e:
#	print('Error in assert_identity_runLambda' + str(e))




def callRunLambda(kt1 : str, _from : blockchain.identity, param : RunLambda, networkName = "ghostnet", debug=False):
	input = runLambda_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "runLambda", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

BreakGlass = factori_types.address

def breakGlass_generator() -> BreakGlass:
	return factori_types.address_generator ()

def breakGlass_encode (arg: BreakGlass):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in breakGlass_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def breakGlass_decode (arg) -> BreakGlass:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in breakGlass_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_breakGlass():
	temp=breakGlass_generator()
	temp_encoded=breakGlass_encode(temp)
	try:
		print('++++ breakGlass: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of breakGlass: " + str(temp))
	temp1=breakGlass_decode(temp_encoded)
	if temp1 == temp:
		print('breakGlass check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('breakGlass check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_breakGlass()
#except Exception as e:
#	print('Error in assert_identity_breakGlass' + str(e))




def callBreakGlass(kt1 : str, _from : blockchain.identity, param : BreakGlass, networkName = "ghostnet", debug=False):
	input = breakGlass_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "breakGlass", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Constructor_storage(TypedDict):
	targetAddress: BreakGlass
	multisigAddress: BreakGlass
	daoAddress: BreakGlass

def _storage_generator() -> Constructor_storage:
	return {'targetAddress' : breakGlass_generator (),'multisigAddress' : breakGlass_generator (),'daoAddress' : breakGlass_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([(breakGlass_encode(arg['daoAddress'])), PairType.from_comb([(breakGlass_encode(arg['multisigAddress'])), (breakGlass_encode(arg['targetAddress']))])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode (breakGlass_decode,(factori_types.tuple2_decode (breakGlass_decode,breakGlass_decode))))(arg)
		return {'daoAddress' : (before_projection[0]),
'multisigAddress' : (before_projection[1][0]),
'targetAddress' : (before_projection[1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract governor")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/governor_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1