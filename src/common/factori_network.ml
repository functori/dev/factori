(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines a type called `network` and provides functions for
working with it, including converting the network to strings, getting the nodes
for a network, and handling errors related to networks. It also includes
functions for getting the base and tzKT API URL for a network, as well as a
function for getting the datasource name and URL for a network. Warning: this
short summary was automatically generated and could be incomplete or misleading.
If you feel this is the case, please open an issue. *)

type network =
  | Mainnet
  | Ghostnet
  | Limanet
  | Mumbainet
  | Kathmandunet
  | Nairobinet
  | Flextesa
  | Custom of string
[@@deriving encoding]

let string_of_network (n : network) =
  match n with
  | Mainnet -> "mainnet"
  | Ghostnet -> "ghostnet"
  | Limanet -> "limanet"
  | Mumbainet -> "mumbainet"
  | Kathmandunet -> "kathmandunet"
  | Nairobinet -> "nairobinet"
  | Flextesa -> "flextesa"
  | Custom s -> s

let mainnet_nodes =
  [
    "https://tz.functori.com";
    "https://mainnet.api.tez.ie/";
    "https://mainnet.smartpy.io";
    "https://rpc.tzbeta.net/";
    "https://mainnet.tezos.marigold.dev/";
    "https://eu01-node.teztools.net/";
  ]

let ghostnet_nodes =
  ["https://ghostnet.ecadinfra.com"; "https://ghostnet.tezos.marigold.dev/"]

let limanet_nodes =
  ["https://limanet.ecadinfra.com"; "https://rpczero.tzbeta.net/"]

let kathmandunet_nodes =
  [
    "https://kathmandunet.ecadinfra.com";
    "https://kathmandunet.smartpy.io/";
    "https://kathmandunet.tezos.marigold.dev/";
  ]

let mumbainet_nodes = []

let nairobinet_nodes =
  ["https://nairobinet.tezos.marigold.dev/"; "https://nairobinet.ecadinfra.com"]

let flextesa_nodes = ["http://localhost:20000"]

(** Says whether pre is a prefix of str, of length at least three  *)
let is_3_prefix ~str pre =
  String.length pre >= 3 && String.starts_with ~prefix:pre str

(** Liberal interpretation of network strings as networks, allowing
   for three letter prefixes such as mai for mainnet or gho for
   ghostnet *)
let network_of_string (s : string) =
  match s with
  | "mainnet" | "main" -> Mainnet
  | s when is_3_prefix ~str:"mainnet" s -> Mainnet
  | "ghostnet" | "ghost" -> Ghostnet
  | s when is_3_prefix ~str:"ghostnet" s -> Ghostnet
  | "limanet" | "lima" -> Limanet
  | s when is_3_prefix ~str:"limanet" s -> Limanet
  | "mumbainet" | "mumbai" -> Mumbainet
  | s when is_3_prefix ~str:"mumbainet" s -> Mumbainet
  | "kathmandunnet" | "kathmandu" -> Kathmandunet
  | s when is_3_prefix ~str:"kathmandunet" s -> Kathmandunet
  | "flextesa" -> Flextesa
  | s when is_3_prefix ~str:"flextesa" s -> Flextesa
  | "nairobinet" -> Nairobinet
  | s when is_3_prefix ~str:"nairobinet" s -> Nairobinet
  | s -> Custom s

let nodes_of_network = function
  | Mainnet -> mainnet_nodes
  | Ghostnet -> ghostnet_nodes
  | Kathmandunet -> kathmandunet_nodes
  | Limanet -> limanet_nodes
  | Nairobinet -> nairobinet_nodes
  | Mumbainet -> mumbainet_nodes
  | Flextesa -> flextesa_nodes
  | Custom s -> [s]

exception NoKnownNodes of network

exception AllNodesFailed of network * string list

let _ =
  let open Format in
  Factori_errors.add_exception_handler (function
    | NoKnownNodes n ->
      eprintf "Warning: there are 0 known nodes for network %s"
        (string_of_network n)
    | e -> raise e)

let _ =
  let open Format in
  Factori_errors.add_exception_handler (function
    | AllNodesFailed (n, failed) ->
      eprintf
        "All nodes failed for network %s; Here is the list of nodes which were \
         attempted:\n\
         - %a\n"
        (string_of_network n)
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "\n- ")
           (fun ppf node -> fprintf ppf "\"%s\"" node))
        failed
    | e -> raise e)

let get_raw_network ?(failed = []) ?(sort = fun x -> x) (n : network) =
  let nodes = sort @@ nodes_of_network n in
  if nodes = [] then
    raise (NoKnownNodes n)
  else
    match List.find_opt (fun x -> not (List.mem x failed)) nodes with
    | None -> raise (AllNodesFailed (n, failed))
    | Some node -> node

let get_base_of_network ~failed ?(sort = fun x -> x) x =
  EzAPI.BASE (get_raw_network ~failed ~sort x)

let get_tzkt_api = function
  | Mainnet -> "https://api.tzkt.io/"
  | Ghostnet -> "https://api.ghostnet.tzkt.io/"
  | _ -> "<CUSTOM_URL>"

let get_tzkt_datasource_url network =
  match network with
  | Mainnet -> ("tzkt_mainnet", get_tzkt_api network)
  | Ghostnet -> ("tzkt_testnet", get_tzkt_api network)
  | _ -> ("tzkt_custom", get_tzkt_api network)
