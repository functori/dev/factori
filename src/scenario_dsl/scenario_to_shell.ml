(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file `scenario_to_shell.ml` provides functions for converting a
scenario into shell scripts, including functions for pretty-printing abstract
values to different syntax formats and utility functions. The
`pp_sequence_to_typescript` function is particularly important, as it takes the
core scenario instructions and formats them in a way that can be executed by the
TezosToolkit. The `pp_scenario_from_sequences_to_shell` function pretty-prints a
list of `sequence` objects to shell syntax. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Context
open Format
open Tzfunc.Proto
open Context
open Scenario_value

let show_code c = asprintf "(%s)" (EzEncoding.construct script_expr_enc.json c)

let show_micheline m =
  Format.asprintf "new Parser().parseJSON(%s)"
    (EzEncoding.construct micheline_enc m)

let show_generate_identity universe i =
  Format.asprintf "get_identity (\"%d_%a\")" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ";")
       (fun ppf d -> fprintf ppf "%d" d))
    universe

let show_raw_identity universe i =
  Format.asprintf "%d_%a" i
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ";")
       (fun ppf d -> fprintf ppf "%d" d))
    universe

let fresh_native =
  let counter = ref 0 in
  fun () ->
    let res = !counter in
    incr counter ;
    res

let show_kt1_typescript names universe = function
  | StaticKt1 kt1 -> sprintf "%s" kt1
  | IdKt1 id -> asprintf "%a" (print_name ~underscore:true names universe) id

let pp_use_uv_typescript ?(static = false) ?(alter = fun x -> x) names universe
    ppf id =
  fprintf ppf "%a" (print_name ~static ~alter names universe) id

let print_id_typescript names universe ppf idpointer =
  fprintf ppf "%a" (pp_use_uv_typescript names universe) idpointer

let show_uv_typescript names universe = function
  (* | NamedValue vl -> show_named_value vl *)
  | DefaultIdentity ParameterNetwork -> sprintf "(get_default_identity network)"
  | DefaultIdentity (SpecificNetwork n) ->
    sprintf "(functolib.get_node %s)" (Factori_utils.get_default_identity n)
  | Code_uv c -> show_code c
  | Amount_uv amount -> show_amount amount
  | Kt1_uv kt1 -> show_kt1_typescript names universe kt1
  | Network_uv ParameterNetwork -> "network"
  | Network_uv network -> show_network network
  | String_uv s -> sprintf "\"%s\"" s
  | Entrypoint_name_uv s -> sprintf "\"%s\"" s
  | Micheline_uv c -> show_micheline c
  | Address_uv address -> show_address address
  | Generate_key seed -> show_generate_identity universe seed
  | NativeAbstractValue (AValue v) ->
    asprintf "%a" (Pp_abstract_value.pp_value_to_shell names universe) v
  | Id id_pointer ->
    asprintf "%a" (print_id_typescript names universe) id_pointer
  | FunctionCall (GetAddress, [id]) ->
    asprintf "(%a).pkh" (print_id_typescript names universe) id
  | FunctionCall (GetBalance, [id]) ->
    asprintf "TODOget_balance(%a) ()" (print_id_typescript names universe) id
  | FunctionCall (GetStorage, [id]) ->
    asprintf "TODOget_storage(%a)" (print_id_typescript names universe) id
  | FunctionCall (((GetAddress | GetStorage | GetBalance) as f), _) ->
    failwith
      (sprintf "Wrong number of arguments: %s has one argument" (show_func f))

let pp_definition_uv_typescript names universe id ppf uv =
  match uv with
  | uv ->
    fprintf ppf "let %a = %s;@."
      (print_name names universe)
      id
      (show_uv_typescript names universe uv)

let print_network_typescript ppf network =
  match network with
  | SpecificNetwork n -> fprintf ppf "\"%s\"" n
  | ParameterNetwork -> fprintf ppf "network"

(* ~/tezos/tezos-client -E http://localhost:20000 originate contract fex_fa2_1 transferring 0 from alice running ~/fa2-smart-contracts/single_asset/ligo/out/fa2_single_asset.tz  --init 'Pair (Pair (Pair (Pair "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" False) None) (Pair {} {}) (Pair {} 0)) {}' --burn-cap 1.0205 --force *)

(* id will be used to create a name to store the KT1 *)
let print_deploy_shell names universe ppf _id (d : u_id deploy) =
  fprintf ppf
    "octez-client originate --from %a.sk;\n\
     --network %a running TODO;\n\
     %s --init %a"
    (pp_use_uv_typescript names universe)
    d.from
    (pp_use_uv_typescript names universe)
    d.network d.contract_name
    (pp_use_uv_typescript names universe)
    d.storage

let print_call_typescript names universe id ppf (c : u_id call) =
  let contract_interface =
    sprintf "%s_interface" (String.capitalize_ascii c.contract_name) in
  fprintf ppf
    "setSigner(tezosKit,%a.sk);\n\
     let config_%a = {node_addr : get_node(%a)};\n\
     let %a = await %s.call_%s(tezosKit,%a,%a,%a) ;@."
    (pp_use_uv_typescript names universe)
    c.from
    (pp_use_uv_typescript names universe)
    id
    (pp_use_uv_typescript names universe)
    c.network
    (pp_use_uv_typescript names universe)
    id contract_interface c.entrypoint
    (pp_use_uv_typescript names universe)
    c.contract
    (pp_use_uv_typescript names universe)
    c.param
    (pp_use_uv_typescript names universe)
    c.amount

let print_failed_call_typescript names universe id ppf
    (expected_bc_error : string) (msg : string) (c : u_id call) =
  let contract_interface =
    sprintf "%s_interface" (String.capitalize_ascii c.contract_name) in
  fprintf ppf
    "setSigner(tezosKit,%a.sk);\n\
     let config_%a = {node_addr : get_node(%a)};\n\
     let %a = await \
     %s.assert_failwith_str_%s(tezosKit,%a,%a,\"%s\",\"[scenario%a]\",\"%s\",%a) \
     ;@."
    (pp_use_uv_typescript names universe)
    c.from
    (pp_use_uv_typescript names universe)
    id
    (pp_use_uv_typescript names universe)
    c.network
    (pp_use_uv_typescript names universe)
    id contract_interface c.entrypoint
    (pp_use_uv_typescript names universe)
    c.contract
    (pp_use_uv_typescript names universe)
    c.param expected_bc_error Universes.print_universe universe msg
    (pp_use_uv_typescript names universe)
    c.amount

let print_print_shell names universe id ppf (u : u_id uv) =
  match u with
  | FunctionCall (GetAddress, [addr]) ->
    fprintf ppf {|TODO SHELLlet %a = console.log(`[address] ${%a}`)|}
      (pp_use_uv_typescript names universe)
      id
      (pp_use_uv_typescript names universe)
      addr
  | Code_uv _
  | Amount_uv _
  | Kt1_uv _
  | Network_uv _
  | String_uv _
  | Entrypoint_name_uv _
  | Micheline_uv _
  | Address_uv _
  | Generate_key _
  | DefaultIdentity _
  | Id _
  | NativeAbstractValue _
  | FunctionCall ((GetBalance | GetStorage | GetAddress), _) ->
    fprintf ppf "#%s"
      (asprintf "[print_print_shell] TODO: %s"
         (show_uv (fun ppf _ -> fprintf ppf "<>") u))

let pp_seq_instruction_to_typescript names universe ppf
    ((id : u_id), (v : u_id value)) =
  match v with
  | Unary u -> pp_definition_uv_typescript names universe id ppf u
  | Checkpoint -> (* fprintf ppf "(\* Checkpoint before fork *\)@." *) ()
  | Deploy deploy -> print_deploy_shell names universe ppf id deploy
  | Call call -> print_call_typescript names universe id ppf call
  | FailedCall (Failed (s, msg, call)) ->
    print_failed_call_typescript names universe id ppf s msg call
  | PrintUnary (_msg, u) -> print_print_shell names universe id ppf u
  | Fork _ | Seq _ -> ()

(* Print a linear sequence of instructions to a Typescript scenario *)
let pp_sequence_to_typescript ppf (s : sequence) =
  List.iter
    (fprintf ppf
       "/* Universe %a */\n\n\
        let scenario%a = async function(network){\n\
        console.log('[scenario%a]Entering scenario%a')\n\
        let node_addr = get_node(network);\n\
        const tezosKit = new TezosToolkit(node_addr)@." Universes.print_universe
       s.seq_universe Universes.print_universe s.seq_universe
       Universes.print_universe s.seq_universe Universes.print_universe
       s.seq_universe ;
     pp_seq_instruction_to_typescript s.seq_names s.seq_universe ppf)
    s.seq ;
  let n = List.length s.seq in
  if n = 0 then
    fprintf ppf
      "return; (* Warning: there was an empty list of instructions *)@.}\n"
  else
    let last_element = List.nth s.seq (n - 1) in
    let last_name =
      Format.asprintf "%a"
        (print_name s.seq_names s.seq_universe)
        (fst last_element) in
    fprintf ppf "return (%s);@.}\n" last_name

let prelude _contract_names ppf () = fprintf ppf {||}

(* Copied from Blockchain *)
let get_identity (seed : string) : identity =
  let open Crypto in
  let better_seed =
    String.init 32 (fun i -> String.get seed (i mod String.length seed)) in
  let sk = Tzfunc.Crypto.Sk.T.mk @@ Raw.mk better_seed in
  let pk = Crypto.Sk.to_public_key sk in
  let curve = `ed25519 in
  let pkh = Pkh.b58enc ~curve @@ Pk.hash pk in
  let sk = Sk.b58enc ~curve sk in
  let pk = Pk.b58enc ~curve pk in
  { pkh; sk; pk }

let pp_funding ppf (ss : sequence list) =
  let show _names universe = function
    | Generate_key seed -> show_raw_identity universe seed
    | _ ->
      failwith
        "[pp_funding][show] This function should only find Generate_key \
         elements" in
  let addr_amount_map = Compilation_utils.get_addr_balances ~show_uv:show ss in
  fprintf ppf
    "\n\n\
     function get_identity(seed : string) : wallet{\n\
     switch(seed){%a\n\
     default:\n\
     throw (\"Unknown seed: \" + seed);\n\
     }\n\
     }@."
    (pp_print_seq ~pp_sep:(Factori_utils.tag "") (fun ppf (k, _) ->
         let ident = get_identity k in
         fprintf ppf
           "\ncase \"%s\":\nreturn {sk : \"%s\",pk: \"%s\",pkh : \"%s\" }" k
           ident.sk ident.pk ident.pkh))
    (AddrBalances.to_seq addr_amount_map) ;
  let addr_amount_map =
    Compilation_utils.get_addr_balances ~show_uv:show_uv_typescript ss in
  fprintf ppf
    "\n\n\
     fund() = {\n\
     #let node_addr = get_node(network);\n\
     const batch = await tezosKit.wallet.batch()\n\
     .withTransfer(%a\n\
     const batchOp = await batch.send();\n\
     await batchOp.confirmation();@.\n\
     }\n\
    \              "
    (pp_print_seq ~pp_sep:(Factori_utils.tag "\n.withTransfer(")
       (fun ppf (k, v) ->
         fprintf ppf "{ to: %s.pkh, amount: %s, mutez:true })" k v))
    (AddrBalances.to_seq
       (AddrBalances.filter (fun _k v -> v <> "0") addr_amount_map))

let pp_scenario_from_sequences_to_shell ?(funding = true) ppf
    (ss : sequence list) =
  let contract_names = extract_contract_names_from_sequence_list ss in
  prelude contract_names ppf () ;
  fprintf ppf "%s"
    (if funding then
       asprintf "%a" pp_funding ss
     else
       "") ;
  List.iter (pp_sequence_to_typescript ppf) ss ;
  fprintf ppf
    "\n\n\
     main () {\n\
     network = $1\n\
     # let node_addr = get_node(network);\n\
     fund network\n\
     #https://stackoverflow.com/questions/17307800/how-to-run-given-function-in-bash-in-parallel\n\
     for i in \"${list[@@]}\"\n\
     do\n\
     sem %a\n\
     done\n\
     }\n\n\
     main 'flextesa'"
    (pp_print_list ~pp_sep:(Factori_utils.tag ",") (fun ppf s ->
         fprintf ppf "scenario%a(network)" Universes.print_universe
           s.seq_universe))
    ss
