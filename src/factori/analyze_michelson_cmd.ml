(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains the implementation of the `analyze
   michelson` subcommand in Factori, which is used to lint a Tezos
   smart contract using its Michelson compiled form. It depends on
   several other modules and defines a single function,
   `analyze_michelson`, which takes no arguments and returns unit, but
   can raise exceptions if no Michelson file has been provided or if
   there is an error parsing the code. Warning: this short summary was
   automatically generated and could be incomplete or misleading. If
   you feel this is the case, please open an issue. *)

open Factori_command
open Factori_options
open Factori_errors
open Factori_analyze
open Factori_analyze_types.Analyze_types

let action () =
  add_exception_handler stack_overflow_handle ;
  try
    (* First initialize directory structure if it is not already there *)
    Factori_utils.try_overflow ~msg:"analyze_michelson" (fun () ->
        match !michelson_file with
        | None -> raise NoMichelsonFileProvided
        | Some michelson_file -> Analyze.run (MichelsonFile michelson_file))
  with e ->
    handle_exception e ;
    exit 1

let analyze_michelson =
  subcommand ~name:"analyze michelson"
    ~args:[michelson_file_anonymous0]
    ~doc:
      "Lint a contract using its Michelson compiled form. It can be in either \
       a Json or normal Michelson format."
    action

(* merged with `analyze michelson`, but we keep the command for backward compatibility
   reasons *)
let lint_michelson =
  subcommand ~name:"lint michelson"
    ~args:[michelson_file_anonymous0]
    ~doc:
      "Lint a contract using its Michelson compiled form. It can be in either \
       a Json or normal Michelson format."
    action
