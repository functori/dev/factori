(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines various type aliases, variables and functions for
lifting concrete values to their abstract counterparts in Factori. It includes
aliases for types such as `nat`, `bool'`, `unit'`, `string'`, `int_michelson`,
`address`, `bytes`, `map`, `big_map`, `lambda`, `timestamp`, `contract`,
`operation`, `signature`, `key`, `tez`, `key_hash`, `michelson_pair`, `set`,
`never_type`, `chain_id`, and `ticket`. The file also includes functions for
lifting concrete values to their abstract counterparts. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Format

let elem_types =
  [
    "nat";
    "bool'";
    "unit'";
    "string'";
    "int_michelson";
    "address";
    "bytes";
    "timestamp";
    "contract";
    "operation";
    "signature";
    "key";
    "tez";
    "key_hash";
    "never_type";
    "chain_id";
  ]

let make_elementary_lift typename =
  sprintf "let lift_%s (x) : %s = lift x" typename typename

let elem_lifts =
  asprintf "%a"
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n\n") (fun ppf s ->
         fprintf ppf "%s" (make_elementary_lift s)))
    elem_types

let abstract_types_ml =
  let type_definitions =
    {|
  type 'a astobj = 'a Ast.astobj

  type 'a id_or_concrete =
    | Concrete of 'a
    | Id of Scenario_id.id

  let lift x = Concrete x

  type nat = FT.nat id_or_concrete
  type bool' = bool id_or_concrete
  type unit' = unit id_or_concrete
  type string' = string id_or_concrete
  type 'a option' = 'a option id_or_concrete
  type 'a list' = 'a list id_or_concrete
  type int_michelson = FT.int_michelson id_or_concrete
  type address = FT.address id_or_concrete
  type bytes = FT.bytes id_or_concrete

  type ('a,'b) map =
    ('a * 'b) id_or_concrete list id_or_concrete

  type ('a, 'b) pre_big_map =
      Literal of ('a * 'b) id_or_concrete list
    | Abstract of A.zarith

  type ('a, 'b) big_map =
    ('a, 'b)  pre_big_map id_or_concrete

  type lambda = FT.lambda id_or_concrete

  type timestamp = FT.timestamp id_or_concrete

  type contract = FT.contract id_or_concrete

  type operation = FT.operation id_or_concrete

  type signature = FT.signature id_or_concrete

  type key = FT.key id_or_concrete

  type tez = FT.int_michelson id_or_concrete

  type key_hash = FT.key_hash id_or_concrete

  type ('a, 'b) michelson_pair = ('a, 'b) FT.michelson_pair id_or_concrete

  type 'a set = 'a FT.set id_or_concrete

  type never_type = FT.never_type id_or_concrete

  type chain_id = FT.chain_id id_or_concrete

  type 'a ticket = 'a FT.ticket id_or_concrete

|}
  in
  let functions_definitions =
    {|
let bind_abstract_value : 'a 'b. ('a -> 'b value) -> 'a id_or_concrete -> 'b value =
  fun (type b) (f: 'a -> b value) x -> match x with
    | Id id -> VAbstract (ScenId id)
    | Concrete x -> f x

  let value_of_nat : nat -> FT.nat value = bind_abstract_value (fun n -> VInt n)
  let value_of_unit' : unit' -> unit value =  bind_abstract_value (fun () -> VUnit ())
  let value_of_bool' : bool' -> bool value = bind_abstract_value (fun b -> VBool b)
  let value_of_string' : string' -> string value = bind_abstract_value (fun s -> VString s)

  let value_of_never_type : never_type -> 'a = fun _ -> failwith "never say never"

  let value_of_option' (f : 'a -> 'b value) =
    bind_abstract_value
      (fun (o : 'a option) -> match o with
         | None -> VOption None
         | Some a -> VOption (Some (f a)))

  let value_of_list' (f : 'a -> 'b value) =
    bind_abstract_value
      (fun (l : 'a list) -> VList (List.map f l))

  let value_of_int_michelson : int_michelson -> FT.int_michelson value =
    bind_abstract_value (fun x -> VInt x)

  let value_of_address : address -> FT.address value =
    bind_abstract_value (fun a -> VAddress a)

  let value_of_timestamp : address -> FT.timestamp value =
    bind_abstract_value (fun a -> VTimestamp a)

  let value_of_bytes : bytes -> FT.bytes value =
    bind_abstract_value (fun b -> VBytes b)

  let value_of_map f g (m : ('a,'b) map) =
    bind_abstract_value
      (fun m -> VMap (List.map (fun xy ->
           match xy with
           | Concrete (x,y) ->
           SP2 (f x,g y)
           | Id i -> SP (VAbstract (ScenId i)))
           m))
      m

  let value_of_big_map f g (x : ('a,'b) big_map) =
    bind_abstract_value
      (fun (m : ('a,'b) pre_big_map) ->
         match m with
         | Literal m ->
           VLiteralBigMap
             ((List.map (fun xy ->
                  match xy with
                  | Concrete (x,y) ->
                    SP2 (f x,g y)
                  | Id i -> SP (VAbstract (ScenId i))) m))
         | Abstract i -> VOnChainBigMap i) x

  let value_of_contract : contract -> FT.contract value =
    bind_abstract_value
      (fun (_c : FT.contract) -> (failwith "TODO: value_of_contract" : FT.contract value))

  let value_of_operation : operation -> FT.operation value =
    bind_abstract_value
      (fun op -> VOperation op)

  let value_of_signature : signature -> FT.signature value =
    bind_abstract_value
      (fun signature -> VSignature signature)

  let value_of_key : key -> FT.key value =
    bind_abstract_value
      (fun key -> VKey key)

  let value_of_tez : tez -> FT.tez value =
    bind_abstract_value
      (fun tez -> VTez tez)

  let value_of_key_hash : key_hash -> FT.key_hash value =
    bind_abstract_value
      (fun kh -> VKeyHash kh)

  (* let value_of_michelson_pair _ _ _ = failwith "TODO: michelson_pair" *)

  let value_of_set f =
    bind_abstract_value
      (fun s -> VSet (List.map f s))

  (* let value_of_never _ =
   *   bind_abstract_value
   *     (fun _ -> failwith "TODO: value_of_never") *)

  let value_of_chain_id : chain_id -> FT.chain_id value =
    bind_abstract_value
      (fun ci -> VChain_id ci)

  let value_of_ticket (type a) (type b) (_f : a -> b) =
    let open Factori_utils.Ticket in
    (bind_abstract_value
      (function (Ticket t) -> VTicket (Ticket t)) : a FT.ticket id_or_concrete -> a ticket value)

  let value_of_lambda (_f1) (_f2) : Lambda.lambda id_or_concrete -> Lambda.lambda value =
    bind_abstract_value
      (function | Lambda.Lambda {from;to_;body} -> VLambda (Lambda {from;to_;body})
                | Lambda.SeqLambda m -> VLambda (SeqLambda m))

|}
  in

  let composite_lifts =
    {|
let lift_ticket (type a) (type b) (_lift_type : a -> b) =
  let open Factori_utils.Ticket in
  (function (Ticket i) -> Concrete (Ticket i) : a FT.ticket -> b FT.ticket id_or_concrete)

let lift_list' lift_type l = Concrete (List.map lift_type l)

let lift_map l1 l2 l : ('a,'b) map = Concrete (List.map (fun (x,y) -> Concrete (l1 x,l2 y)) l)

let lift_lambda (_l1) (_l2) (l : FT.lambda) : lambda = match l with
  | Lambda {from;to_;body} -> Concrete (Lambda {from;to_;body})
  | SeqLambda m -> Concrete (SeqLambda m)

let lift_set f l : 'a set = Concrete (List.map (fun x -> f x) l)

let lift_option' f (x : 'a option) : 'b option' =
  match x with None -> Concrete None
             | Some x -> Concrete (Some (f x))

let lift_big_map l1 l2 (l : ('a,'b) FT.big_map) : ('c,'d) big_map =
  match l with
  | Literal l ->
    Concrete (Literal (List.map (fun (x,y) -> Concrete (l1 x,l2 y)) l))
  | Abstract z -> Concrete (Abstract z)
|}
  in

  let n_tuples = 25 in

  Format.sprintf
    {|module FT = Factori_types

module Abstract = struct
  open Tzfunc.Proto
%s
%s
open Abstract_value
%s
%s
%s
%s
%s
end

|}
    type_definitions
    (Ocaml_abstract_tuple_boilerplate.abstract_tuple_boilerplate n_tuples)
    elem_lifts composite_lifts
    (Ocaml_abstract_tuple_boilerplate.abstract_tuple_lifts_boilerplate n_tuples)
    functions_definitions
    (Ocaml_abstract_tuple_boilerplate.abstract_tuple_values_boilerplate n_tuples)

(* let lift_tuple2 lift1 lift2 ((x,y) : ('a * 'b)) = Concrete (lift1 x,lift2 y) *)
