(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions that perform unstacking and type checking
of Michelson expressions in the Tezos blockchain. The module depends on
`Unstack_micheline` and `Nested_arg` and includes functions such as `transform`,
`control_structure`, and `transform_sc`. No examples are provided. Warning: this
short summary was automatically generated and could be incomplete or misleading.
If you feel this is the case, please open an issue. *)

open Unstack_micheline
open Factori_representation
open Nested_arg
open Factori_micheline_lib
open Factori_errors

(*
module Transmo = Map.Make (struct
  type t = name_arg

  let compare n n2 = compare (fst n) (fst n2)
                   end)*)

module Transmo = Map.Make (String)

let transform cpt env (name, miche) =
  match Transmo.find_opt name env with
  | None ->
    let new_cpt, m = Nested_arg.construct cpt miche in
    let env = Transmo.add name m env in
    ((new_cpt, env), m)
  | Some e -> ((cpt, env), e)

let transform_fold (cpt, env) = transform cpt env

let rec control_structure cpt env node =
  match node with
  | N_IF (arg, label_then, label_else, node_b1, node_b2) ->
    let (new_cpt, env), arg = transform cpt env arg in
    let new_cpt, env, node_b1 = propagate_construction new_cpt env [] node_b1 in
    let new_cpt, env, node_b2 = propagate_construction new_cpt env [] node_b2 in
    (N_IF (arg, label_then, label_else, node_b1, node_b2), env, new_cpt)
  | N_IF_NONE
      ( arg_opt,
        label_then,
        label_else,
        node_b1,
        node_b2,
        (pat_some, _),
        label_pat ) ->
    let (new_cpt, env), nested_arg_opt = transform cpt env arg_opt in
    let new_cpt, env, node_b1 = propagate_construction new_cpt env [] node_b1 in
    let pat_some_arg =
      match nested_arg_opt with
      | Option (arg, _) -> arg
      | _ ->
        raise
          (GenericError
             ( "propagate_opt",
               Format.asprintf "Not an option %a"
                 (Micheline_lib.print_micheline ~simple:true)
                 (snd arg_opt) )) in
    let env = Transmo.add pat_some pat_some_arg env in
    let new_cpt, env, node_b2 = propagate_construction new_cpt env [] node_b2 in
    let env = Transmo.remove pat_some env in
    ( N_IF_NONE
        ( nested_arg_opt,
          label_then,
          label_else,
          node_b1,
          node_b2,
          pat_some_arg,
          label_pat ),
      env,
      new_cpt )
  | N_IF_CONS
      ( arg_list,
        label_then,
        label_else,
        node_b1,
        node_b2,
        pat_x,
        lab_x,
        pat_xs,
        lab_xs ) ->
    let (new_cpt, env), arg_list = transform cpt env arg_list in
    let new_cpt, env, node_b1 = propagate_construction new_cpt env [] node_b1 in
    let (new_cpt, pat_env), arg_pat_x = transform new_cpt env pat_x in
    let (new_cpt, pat_env), arg_pat_xs = transform new_cpt pat_env pat_xs in
    let new_cpt, env, node_b2 =
      propagate_construction new_cpt pat_env [] node_b2 in
    ( N_IF_CONS
        ( arg_list,
          label_then,
          label_else,
          node_b1,
          node_b2,
          arg_pat_x,
          lab_x,
          arg_pat_xs,
          lab_xs ),
      env,
      new_cpt )
  | N_IF_LEFT
      ( arg_or,
        label_then,
        label_else,
        ((pat_left, _), lab_left, node_b1),
        ((pat_right, _), lab_right, node_b2) ) ->
    let (new_cpt, env), arg_or = transform cpt env arg_or in
    let arg_pat_left, arg_pat_right =
      match arg_or with
      | Or (arg_l, arg_r, _) -> (arg_l, arg_r)
      | _ -> raise (GenericError ("propagate_opt", "Not a or")) in
    let env = Transmo.add pat_left arg_pat_left env in
    let new_cpt, env, node_b1 = propagate_construction new_cpt env [] node_b1 in
    let env = Transmo.remove pat_left env in
    let env = Transmo.add pat_right arg_pat_right env in
    let new_cpt, env, node_b2 = propagate_construction new_cpt env [] node_b2 in
    ( N_IF_LEFT
        ( arg_or,
          label_then,
          label_else,
          (arg_pat_left, lab_left, node_b1),
          (arg_pat_right, lab_right, node_b2) ),
      env,
      new_cpt )
  | N_ITER
      {
        arg_iter;
        acc_iter;
        pat_acc_iter;
        pat = pat, label_pat;
        body_iter;
        label_iter;
      } ->
    let (new_cpt, env), arg_iter = transform cpt env arg_iter in
    let (new_cpt, env), acc_iter =
      List.fold_left_map transform_fold (new_cpt, env) acc_iter in
    let (new_cpt, pat_env), pat_acc_iter =
      List.fold_left_map transform_fold (new_cpt, env) pat_acc_iter in
    let (new_cpt, pat_env), pat = transform new_cpt pat_env pat in
    let new_cpt, env, body_iter =
      propagate_construction new_cpt pat_env [] body_iter in
    ( N_ITER
        {
          arg_iter;
          acc_iter;
          pat = (pat, label_pat);
          pat_acc_iter;
          body_iter;
          label_iter;
        },
      env,
      new_cpt )
  | N_MAP (arg, label, pat, node_l, label_map) ->
    let (new_cpt, env), arg = transform cpt env arg in
    let (new_cpt, pat_env), pat = transform new_cpt env pat in
    let new_cpt, env, node_l =
      propagate_construction new_cpt pat_env [] node_l in
    (N_MAP (arg, label, pat, node_l, label_map), env, new_cpt)
  | N_LOOP_LEFT
      {
        arg_lf;
        pat_lf = pat_lf, lab_lf;
        acc_lf;
        pat_acc_lf;
        node_loop;
        pat_res_lf = pat_res_lf, lab_res;
        label_lf;
      } ->
    let (new_cpt, env), arg_lf = transform cpt env arg_lf in
    let (new_cpt, env), acc_lf =
      List.fold_left_map transform_fold (new_cpt, env) acc_lf in
    let (new_cpt, pat_env), pat_lf = transform new_cpt env pat_lf in
    let (new_cpt, pat_env), pat_res_lf = transform new_cpt pat_env pat_res_lf in
    let (new_cpt, pat_env), pat_acc_lf =
      List.fold_left_map transform_fold (new_cpt, pat_env) pat_acc_lf in
    let new_cpt, env, node_loop =
      propagate_construction new_cpt pat_env [] node_loop in
    ( N_LOOP_LEFT
        {
          arg_lf;
          pat_lf = (pat_lf, lab_lf);
          acc_lf;
          pat_acc_lf;
          node_loop;
          pat_res_lf = (pat_res_lf, lab_res);
          label_lf;
        },
      env,
      new_cpt )
  | N_FOLD_MAP
      {
        arg_fold_map;
        acc_fold_map;
        pat_acc_fold_map = pat_acc_fold_map, label_pat_acc;
        pat_fold_map = pat_fold_map, label_fold_map;
        body_fold_map;
        label_map;
      } ->
    let (new_cpt, env), arg_fold_map = transform cpt env arg_fold_map in
    let (new_cpt, pat_env), pat_fold_map = transform new_cpt env pat_fold_map in
    let (new_cpt, acc_env), acc_fold_map =
      List.fold_left_map transform_fold (new_cpt, env) acc_fold_map in
    let (new_cpt, pat_env), pat_acc_fold_map =
      List.fold_left_map transform_fold (new_cpt, pat_env) pat_acc_fold_map
    in
    let new_cpt, env, body_fold_map =
      propagate_construction new_cpt pat_env [] body_fold_map in
    ( N_FOLD_MAP
        {
          arg_fold_map;
          acc_fold_map;
          pat_acc_fold_map = (pat_acc_fold_map, label_pat_acc);
          pat_fold_map = (pat_fold_map, label_fold_map);
          body_fold_map;
          label_map;
        },
      Transmo.union (fun _name_arg _left right -> Some right) acc_env env,
      new_cpt )
  | N_LOOP { arg; body; pat_acc; acc; label_loop; label_pat_l } ->
    let (new_cpt, env), arg = transform cpt env arg in
    let (new_cpt, env), acc =
      List.fold_left_map transform_fold (new_cpt, env) acc in
    let (new_cpt, env), pat_acc =
      List.fold_left_map transform_fold (new_cpt, env) pat_acc in
    let new_cpt, env, body = propagate_construction new_cpt env [] body in
    (N_LOOP { body; pat_acc; arg; acc; label_loop; label_pat_l }, env, new_cpt)

and propagate_construction cpt env acc node_l =
  match node_l with
  | [] -> failwith "TODO impossible"
  | [N_END (arg_l, label_end)] ->
    let (new_cpt, env), arg_l =
      List.fold_left_map transform_fold (cpt, env) arg_l in
    let node = N_END (arg_l, label_end) in
    (new_cpt, env, List.rev (node :: acc))
  | [N_FAILWITH (arg, label_fail)] ->
    let (new_cpt, env), arg_l = transform cpt env arg in
    let node = N_FAILWITH (arg_l, label_fail) in
    (new_cpt, env, List.rev (node :: acc))
  | [N_NEVER (arg, label_never)] ->
    let (new_cpt, env), arg = transform cpt env arg in
    let node = N_NEVER (arg, label_never) in
    (new_cpt, env, List.rev (node :: acc))
  | [FAIL_CONTROL (ctrl, lab_ctrl)] ->
    let node, env, new_cpt = control_structure cpt env ctrl in
    (new_cpt, env, List.rev (FAIL_CONTROL (node, lab_ctrl) :: acc))
  | N_SIMPLE (`CDR, [_], [arg], [res], _) :: next_nodes ->
    let (new_cpt, env), arg = transform cpt env arg in
    let new_res =
      match Nested_arg.gets_pair (Z.of_int 2) arg with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`CAR, [_], [arg], [res], _) :: next_nodes ->
    let (new_cpt, env), arg = transform cpt env arg in
    let new_res =
      match Nested_arg.gets_pair (Z.of_int 1) arg with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`GET, [Mint n], [arg], [res], _) :: next_nodes ->
    let (new_cpt, env), arg = transform cpt env arg in
    let new_res =
      match Nested_arg.gets_pair n arg with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`UPDATE, [Mint n], [arg1; arg2], [res], _) :: next_nodes ->
    let (new_cpt, env), arg1 = transform cpt env arg1 in
    let (new_cpt, env), arg2 = transform new_cpt env arg2 in
    let new_res =
      match Nested_arg.updates_pair n arg1 arg2 with
      | Ok e -> e
      | Error e -> raise e in
    let env = Transmo.add (fst res) new_res env in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`PAIR, [], arg_l, [res], _) :: next_nodes ->
    let (new_cpt, env), arg_l =
      List.fold_left_map transform_fold (cpt, env) arg_l in
    let arg = Pair (arg_l, []) in
    let env = Transmo.add (fst res) arg env in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`UNPAIR, [], [arg], res_l, _) :: next_nodes ->
    let (new_cpt, env), arg = transform cpt env arg in
    let n = Z.of_int (List.length res_l) in
    let new_res_l = Nested_arg.unpair_arg n arg in
    let env =
      try
        List.fold_left2
          (fun env res new_res -> Transmo.add (fst res) new_res env)
          env res_l new_res_l
      with Invalid_argument _ ->
        raise
          (GenericError
             ( "Analysis",
               "Fail analysis due to wrong return in Michelson Unstack" )) in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`SOME, [], [arg], [res], _) :: next_nodes ->
    let (new_cpt, env), arg = transform cpt env arg in
    let env = Transmo.add (fst res) (Option (arg, [])) env in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`LEFT, [right], [arg], [res], _) :: next_nodes ->
    let (new_cpt, env), arg = transform cpt env arg in
    let new_cpt, r = Nested_arg.construct new_cpt right in
    let env = Transmo.add (fst res) (Or (arg, r, [])) env in
    propagate_construction new_cpt env acc next_nodes
  | N_SIMPLE (`RIGHT, [left], [arg], [res], _) :: next_nodes ->
    let (new_cpt, env), arg = transform cpt env arg in
    let new_cpt, l = Nested_arg.construct new_cpt left in
    let env = Transmo.add (fst res) (Or (l, arg, [])) env in
    propagate_construction new_cpt env acc next_nodes
  | node :: next_nodes ->
    let new_node, env, new_cpt =
      match node with
      | N_START (name_l, n, lab_start) ->
        let new_n, env, new_cpt = control_structure cpt env n in
        let (new_cpt, env), name_l =
          List.fold_left_map transform_fold (new_cpt, env) name_l in
        (N_START (name_l, new_n, lab_start), env, new_cpt)
      | N_SIMPLE (prim, args, arg_l, res_l, lab_simple) ->
        let (new_cpt, env), arg_l =
          List.fold_left_map transform_fold (cpt, env) arg_l in
        let (new_cpt, env), res_l =
          List.fold_left_map transform_fold (new_cpt, env) res_l in
        (N_SIMPLE (prim, args, arg_l, res_l, lab_simple), env, new_cpt)
      | N_CONCAT (arg1, arg2, res, label) ->
        let (new_cpt, env), arg1 = transform cpt env arg1 in
        let (new_cpt, env), arg2 = transform new_cpt env arg2 in
        let (new_cpt, env), res = transform new_cpt env res in
        (N_CONCAT (arg1, arg2, res, label), env, new_cpt)
      | N_CONCAT_LIST (arg, res, label) ->
        let (new_cpt, env), arg = transform cpt env arg in
        let (new_cpt, env), res = transform new_cpt env res in
        (N_CONCAT_LIST (arg, res, label), env, new_cpt)
      | N_SELF (str_opt, arg, label) ->
        let (new_cpt, env), arg = transform cpt env arg in
        (N_SELF (str_opt, arg, label), env, new_cpt)
      | N_CONTRACT (str_opt, arg, res, label) ->
        let (new_cpt, env), arg = transform cpt env arg in
        let (new_cpt, env), res = transform new_cpt env res in
        (N_CONTRACT (str_opt, arg, res, label), env, new_cpt)
      | N_CALL_VIEW (str, arg, view, res, label) ->
        let (new_cpt, env), arg = transform cpt env arg in
        let (new_cpt, env), view = transform new_cpt env view in
        let (new_cpt, env), res = transform new_cpt env res in
        (N_CALL_VIEW (str, arg, view, res, label), env, new_cpt)
      | N_LAMBDA (pat, lab_pat, node_l, res, lab_in, lab_end) ->
        let (new_cpt, env), pat = transform cpt env pat in
        let new_cpt, env, node_l =
          propagate_construction new_cpt env [] node_l in
        let (new_cpt, env), res = transform new_cpt env res in
        (N_LAMBDA (pat, lab_pat, node_l, res, lab_in, lab_end), env, new_cpt)
      | N_FAILWITH _ -> failwith "FAILWITH Instruction"
      | N_NEVER _ -> failwith "NEVER instruction"
      | N_CREATE_CONTRACT (opt_k, mutz, ty2, contract, ope, addr, label) ->
        let (new_cpt, env), opt_k = transform cpt env opt_k in
        let (new_cpt, env), mutz = transform new_cpt env mutz in
        let (new_cpt, env), ty2 = transform new_cpt env ty2 in
        let (new_cpt, env), ope = transform new_cpt env ope in
        let (new_cpt, env), addr = transform new_cpt env addr in
        let new_contract = transform_sc contract in
        ( N_CREATE_CONTRACT (opt_k, mutz, ty2, new_contract, ope, addr, label),
          env,
          new_cpt )
      | FAIL_CONTROL (node, label_fail) ->
        let node, env, new_cpt = control_structure cpt env node in
        (FAIL_CONTROL (node, label_fail), env, new_cpt)
      | N_EXEC (arg, lambda, res, label_call, label_ret) ->
        let (new_cpt, env), arg = transform cpt env arg in
        let (new_cpt, env), lambda = transform new_cpt env lambda in
        let (new_cpt, env), res = transform new_cpt env res in
        (N_EXEC (arg, lambda, res, label_call, label_ret), env, new_cpt)
      | N_END _ -> failwith "N_END node should be at the end of a node sequence"
    in
    propagate_construction new_cpt env (new_node :: acc) next_nodes

and transform_view (storage_arg : Nested_arg.t) (cpt : int)
    (view : name_arg v_node) =
  let new_cpt, param = Nested_arg.construct cpt view.view_parameter in
  let view_stack = Pair ([param; storage_arg], []) in
  let env = Transmo.singleton (fst view.view_stack) view_stack in
  let new_cpt, _, node_code_v =
    propagate_construction new_cpt env [] view.node_code_v in
  ( new_cpt,
    {
      view_label = view.view_label;
      node_name = view.node_name;
      view_stack;
      view_parameter = view.view_parameter;
      view_result = view.view_result;
      node_code_v;
    } )

and transform_sc ?(start = 0) (sc : name_arg sc_node) =
  let (new_cpt, env), begin_stack =
    transform start Transmo.empty sc.begin_stack in
  let storage =
    match begin_stack with
    | Pair (_ :: storage, _) -> Nested_arg.pair_simple storage
    | _ -> Error (UnstackMicheline "Error with storage") in
  let result =
    Result.bind storage (fun storage ->
        let new_cpt, _, node_l =
          propagate_construction new_cpt env [] sc.node_l in
        let _, views_node =
          List.fold_left_map (transform_view storage) new_cpt sc.views_node
        in
        Ok
          {
            begin_label = sc.begin_label;
            begin_stack;
            parameter = sc.parameter;
            storage = sc.storage;
            node_l;
            views_node;
          }) in
  match result with
  | Error exn -> raise exn
  | Ok res -> res
