{%%template|
<div>
  <v-home
    v-if="page.Home != undefined &&
          runs != undefined"
    :indexes="runs"
    />
  <v-run
    v-if="page.Run != undefined &&
          run != undefined"
    :run="run"
    />
</div>
|}

let () =
  EzLwtSys.run @@ fun () ->
  let indexes = Common.Index.get () in
  Lwt.bind indexes @@ fun indexes ->
  let page = Page.get ~indexes () in
  Page.set page ;
  let run =
    match page with
    | Page.Home -> Lwt.return_none
    | Page.Run date -> Lwt.map Option.some @@ Common.Run.get date in
  let runs =
    match page with
    | Page.Home -> Lwt.map Option.some @@ Lwt_list.map_p Common.Run.get indexes
    | Page.Run _ -> Lwt.return_none in
  Lwt.bind run @@ fun run ->
  Lwt.bind runs @@ fun runs ->
  let%data page : Page.t = page
  and run : Common.Run.t option = run
  and runs : Common.Run.t list option = runs in
  let _app = [%app { mount; conv; components = [Home; Run] }] in
  Lwt.return_unit
