(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains the main function for the Factori application,
which allows users to manage and interact with Tezos smart contracts. Warning:
this short summary was automatically generated and could be incomplete or
misleading. If you feel this is the case, please open an issue. *)

open Ezcmd.V2
open Factori_options

let ( // ) = concat

exception Error of string

module FACTORI = struct
  let command = "factori"

  let about = "factori COMMAND COMMAND-OPTIONS"

  let set_verbosity n = verbosity := n

  let get_verbosity () = !verbosity

  let backtrace_var = Some "FACTORI_BACKTRACE"

  let usage =
    "Manage and interact with Tezos smart contracts in multiple programming \
     languages"

  let version =
    let open Version in
    Format.sprintf "%s (%s)" version commit

  exception Error = Error
end

module MAIN = EZCMD.MAKE (FACTORI)

(** Options common to all commands  *)
let common_args = [overwrite_option]

let commands =
  [
    Init_cmd.cmd_init;
    Import_kt1_cmd.import_kt1;
    Import_michelson_cmd.import_michelson;
    Rename_variables_cmd.rename_variables;
    Remove_contract_cmd.remove_contract;
    Sandbox_start_cmd.sandbox_start;
    Sandbox_stop_cmd.sandbox_stop;
    Deploy_cmd.deploy_command;
    Deploy_clean_cmd.deploy_clean_command;
    Analyze_michelson_cmd.analyze_michelson;
    (* merged with `analyze michelson`, but we keep the command for backward compatibility
       reasons *)
    Analyze_michelson_cmd.lint_michelson;
    Analyze_kt1_cmd.analyze_kt1;
    (* merged with `analyze kt1`, but we keep the command for backward compatibility
       reasons *)
    Analyze_kt1_cmd.lint_kt1;
  ]

let main () =
  Tzfunc.Node.set_silent true ;
  MAIN.main commands ~common_args ~argv:Sys.argv

let _ = main ()
