(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file `compilation_utils.ml` contains a single function
`get_addr_balances` which takes a list of sequences as input and returns a map
of addresses to their corresponding balances represented as strings. The
function extracts paying addresses and their corresponding values from each
sequence's `value_balances` field and aggregates them into a single balance.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Context
open Format
open Context
open Scenario_value

let get_addr_balances ~show_uv (ss : sequence list) =
  let addr_amount_map =
    let addrmap = AddrBalances.empty in
    List.fold_right
      (fun s addrmap ->
        ValueBalances.fold
          (fun k v newmap ->
            let k =
              asprintf "%a"
                (fun ppf u ->
                  match u with
                  | Unary u ->
                    fprintf ppf "%s" (show_uv s.seq_names s.seq_universe u)
                  | _ -> ())
                k in
            let vnew =
              asprintf "%s"
                (Z.to_string
                   (List.fold_right
                      (fun u total ->
                        match u with
                        | Unary (Amount_uv amount) -> Z.add amount total
                        | _ -> total)
                      v Z.zero)) in

            match AddrBalances.find_opt k newmap with
            | None -> AddrBalances.add k vnew newmap
            | Some v -> AddrBalances.add k (sprintf "%s + %s" v vnew) newmap)
          (Context.collect_paying_addresses s)
          addrmap)
      ss addrmap in
  addr_amount_map
