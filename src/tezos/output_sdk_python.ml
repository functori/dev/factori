(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines functions for converting Michelson values to Python
counterparts, generating and encoding/decoding Python code for Michelson types,
and calling entrypoints and deploying smart contracts. It also extracts a smart
contract's interface and generates a Python script that defines a Pytezos
contract object with the necessary entrypoints and storage. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Format
open Infer_entrypoints
open Pair_type
open Value
open Or_type
open Types
open Factori_utils
open Factori_errors
open Factori_config

let rec pp_value_to_python ppf = function
  | VChain_id s -> fprintf ppf "\"%s\"" s
  | VUnit _ -> fprintf ppf "factori_types.unit"
  | VKeyHash kh -> fprintf ppf "\"%s\"" kh
  | VKey k -> fprintf ppf "\"%s\"" k
  | VSignature s -> fprintf ppf "\"%s\"" s
  | VOperation o -> fprintf ppf "\"%s\"" o
  | VOption (_, None) -> fprintf ppf "None"
  | VOption (_, Some x) -> fprintf ppf "%a" pp_value_to_python x
  | VInt z -> print_z_int_python ppf z
  | VTez z -> print_z_int_python ppf z
  | VString s -> fprintf ppf "\"%s\"" s
  | VBytes s -> fprintf ppf "bytes.fromhex(\"%s\")" (s :> string)
  | VTimestamp s -> fprintf ppf "(\"%s\")" s
  | VBool b ->
    fprintf ppf "%s"
      (if b then
         "True"
       else
         "False")
  | VAddress addr -> fprintf ppf "\"%s\"" addr
  | VSumtype (constructor, _, _, v) -> (
    match v with
    | VUnit () -> fprintf ppf "{ 'kind' : \"%s_constructor\" }" constructor
    | _ ->
      fprintf ppf "{ 'kind' : \"%s_constructor\", '%s_element' : (%a)}"
        constructor constructor pp_value_to_python v)
  | VMap (_, _, m) ->
    fprintf ppf "factori_types.Map(dict([%a]))"
      (pp_print_list ~pp_sep:(tag ",") (fun ppf (x, y) ->
           fprintf ppf "[%a,%a]" pp_value_to_python x pp_value_to_python y))
      m
  | VSet (_, m) ->
    fprintf ppf "factori_types.Set([%a])"
      (pp_print_list ~pp_sep:(tag ",") pp_value_to_python)
      m
  | VList (_, m) ->
    fprintf ppf "[%a]" (pp_print_list ~pp_sep:(tag ",") pp_value_to_python) m
  | VBigMap (_, _, i) ->
    fprintf ppf "factori_types.BigMap(int(%a))" print_z_int_python i
  | VTuple (_, vl) ->
    fprintf ppf "[%a]" (pp_print_list ~pp_sep:(tag ",") pp_value_to_python) vl
  | VLambda l ->
    fprintf ppf "{'from' : 0, 'to_' : 0, 'body' : '%s'}"
      (EzEncoding.construct Tzfunc.Proto.script_expr_enc.json l)
  | VRecord (LeafP (field_name, (_, v))) ->
    fprintf ppf "dict([(%s,%a)])"
      (show_sanitized_name field_name)
      pp_value_to_python v
  | VRecord (PairP _ as p) ->
    fprintf ppf "dict([%a])"
      (pp_pairedtype_typescript ~pairparens:false (* TODO *)
         ~f_leaf:(fun name ppf (_, x) ->
           fprintf ppf "(\"%s\",%a)" (show_sanitized_name name)
             pp_value_to_python x)
         ~f_pair:(fun ppf _d -> fprintf ppf "")
         ~delimiters:Nothing ~sep:",\n")
      p
  | VTicket s -> fprintf ppf "\"%s\"" s
  | VSaplingState s -> fprintf ppf "\"%s\"" s

let pp_sumtype ~prefix f l ppf x =
  let rec aux ppf = function
    | true :: a -> fprintf ppf "%s.make_left(%a)\n" prefix aux a
    | false :: b -> fprintf ppf "%s.make_right(%a)\n" prefix aux b
    | [] -> fprintf ppf "%a" f x in
  aux ppf l

let rec output_sumtype_encode ~prefix argname ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a\t\telse:\n\t\t\traise(ValueError('Unknown kind'))\n"
    (pp_print_list ~pp_sep:(tag "\n") (fun ppf (k, path) ->
         match k with
         | Annot (t, name) ->
           let name = show_sanitized_name @@ bind_sanitize_capitalize name in
           fprintf ppf
             "\t\tif %s['kind'] == \"%s_constructor\":\n\t\t\treturn %a" argname
             name
             (pp_sumtype ~prefix
                (fun ppf at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "%s.unit_encode(%s.unit)" prefix prefix
                  | _ ->
                    fprintf ppf "%a(%s['%s_element'])"
                      (output_type_encode ~prefix)
                      at argname name)
                path)
             t
         | t_local ->
           let name = constructor_from_path path in
           fprintf ppf
             "\t\tif %s['kind'] == \"%s_constructor\":\n\t\t\treturn %a" argname
             name
             (pp_sumtype ~prefix
                (fun ppf _at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "%s.unit_encode(%s.unit)" prefix prefix
                  | _ ->
                    fprintf ppf "%a(%s['%s_element'])"
                      (output_type_encode ~prefix)
                      t_local argname name)
                path)
             t))
    lst

and discriminate_path ~prefix argname path ppf t_ =
  let print_subscript argname ppf (subscript : int list) =
    fprintf ppf "%s%a" argname
      (pp_print_list ~pp_sep:(tag "") (fun ppf x -> fprintf ppf ".items[%d]" x))
      (List.rev subscript) in
  let rec aux offset subscript ppf = function
    | [] -> begin
      match t_ with
      | Annot (u_, name) when is_unit u_ ->
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf "%sreturn {'kind': \"%s_constructor\"}" offset name
      | Annot (t_, name) ->
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf "%sreturn {'kind': \"%s_constructor\",'%s_element': %a(%a)}"
          offset name name
          (output_type_decode ~prefix)
          t_ (print_subscript argname) subscript
      | t_ ->
        let name = sanitized_of_str @@ constructor_from_path path in
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf "%sreturn {'kind': \"%s_constructor\",'%s_element': %a(%a)}"
          offset name name
          (output_type_decode ~prefix)
          t_ (print_subscript name) subscript
    end
    | true :: path ->
      (* true is left *)
      let new_offset = sprintf "%s\t" offset in
      fprintf ppf "\n%sif %a.is_left():\n%s%a\n" offset
        (print_subscript argname) subscript new_offset
        (aux new_offset (0 :: subscript))
        path
    | false :: path ->
      (* false is right *)
      let new_offset = sprintf "%s\t" offset in
      fprintf ppf "\n%sif %a.is_right():\n%s%a\n" offset
        (print_subscript argname) subscript new_offset
        (aux new_offset (1 :: subscript))
        path in

  aux "\t\t" [] ppf path

and output_sumtype_decode ~prefix argname ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "")
       (fun ppf (t_, path) -> discriminate_path ~prefix argname path ppf t_))
    lst

and output_type_pretty ~prefix ppf =
  let output_type_pretty = output_type_pretty in
  function
  | Base String -> fprintf ppf "str"
  | Base b ->
    fprintf ppf "%a"
      (pp_base_type ~language:Factori_config.Python ~abstract:false ~prefix)
      b
  | Annot (ep, _name) -> fprintf ppf "%a" (output_type_pretty ~prefix) ep
  | Pair al ->
    fprintf ppf "Tuple[%a]"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn ->
    fprintf ppf "%s" (show_sanitized_name @@ bind_sanitize_capitalize (fst tn))
  | Unary (List, a) -> fprintf ppf "List[%a]" (output_type_pretty ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.Set[%a]" prefix (output_type_pretty ~prefix) a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.Ticket" prefix (* (output_type_pretty ~prefix) t *)
  | Contract _c ->
    fprintf ppf "%s.contract" prefix (* (output_type_pretty ~prefix) c *)
  | Binary (BigMap, (_k, _v)) -> fprintf ppf "%s.BigMap" prefix
  | Binary (Lambda, (_k, _v)) -> fprintf ppf "%s.Lambda" prefix
  | Binary (Map, (_k, _v)) -> fprintf ppf "%s.Map" prefix
  | Unary (Option, a) ->
    fprintf ppf "Optional[%a]" (output_type_pretty ~prefix) a

(* Some day, on some contract, this will get more complicated *)
and get_constructor ~prefix b ppf type_ =
  Format.fprintf ppf "%s of (%a)"
    (if b then
       "Left"
     else
       "Right")
    (output_type_content ~prefix)
    type_

and output_type_content ~prefix ppf =
  let output_type_pretty = output_type_pretty in
  let output_type_content = output_type_content in
  function
  | Base _ as t -> (output_type_pretty ~prefix) ppf t
  | Annot (_, _) as t -> (output_type_pretty ~prefix) ppf t
  | Pair al ->
    fprintf ppf "Tuple[%a]"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_content ~prefix))
      al
  | Record (LeafP (_n, x)) -> fprintf ppf "%a" (output_type_pretty ~prefix) x
  | Record (PairP l) ->
    let l = flattenP [] l in
    fprintf ppf "%a"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (n, x) ->
           fprintf ppf "%s: %a" (show_sanitized_name n)
             (output_type_pretty ~prefix)
             x))
      l
  | Or (OrAbs (a1, a2)) ->
    fprintf ppf "| %a | %a"
      (get_constructor ~prefix true)
      (Or a1)
      (get_constructor ~prefix false)
      (Or a2)
  | Or (LeafOr _) -> fprintf ppf "LeafOr TODO"
  | TVar tn ->
    fprintf ppf "%s" (show_sanitized_name @@ bind_sanitize_capitalize (fst tn))
  | Unary (List, a) -> fprintf ppf "List[%a]" (output_type_content ~prefix) a
  | Unary (Set, a) -> fprintf ppf "Set[%a]" (output_type_content ~prefix) a
  | Contract _c -> fprintf ppf "%s.contract" prefix (* pp_micheline_ c *)
  | Binary (BigMap, (_k, _v)) -> fprintf ppf "%s.BigMap" prefix
  | Binary (Lambda, (_k, _v)) -> fprintf ppf "%s.Lambda" prefix
  | Binary (Map, (_k, _v)) -> fprintf ppf "%s.Map" prefix
  | Unary (Option, a) ->
    fprintf ppf "Optional[%a]" (output_type_content ~prefix) a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.Ticket" prefix (* (output_type_content ~prefix) t *)

and declare_sumtype ~(* ~prefix *) typename ppf t =
  let lst = sumtype_to_list t in
  let rec aux res = function
    | [] -> res
    | (Annot (_t, name), _) :: xs ->
      let name = show_sanitized_name @@ bind_sanitize_capitalize name in
      let new_res =
        let cur_elem =
          Format.asprintf "%s_%s_constructor_subtype"
            (show_sanitized_name typename)
            name in
        cur_elem :: res in
      aux new_res xs
    | (_t, path) :: xs ->
      let name = constructor_from_path path in
      let new_res =
        let cur_elem =
          Format.asprintf "%s_%s_subtype" (show_sanitized_name typename) name
        in
        cur_elem :: res in
      aux new_res xs in
  fprintf ppf "Union[%s]\n" (String.concat ", " (aux [] lst))

and define_sumtype_subtypes ~typename ~prefix ppf t =
  let lst = sumtype_to_list t in
  let rec aux res = function
    | [] -> res
    | (t_, path) :: xs ->
      let new_res =
        let cur_elem =
          match t_ with
          | Annot (u_, name) when is_unit u_ ->
            let name = show_sanitized_name @@ bind_sanitize_capitalize name in
            Format.asprintf
              "class %s_%s_constructor_subtype(TypedDict):\n\
               \tkind: str  # \"%s_constructor\""
              (show_sanitized_name typename)
              name name
          | Annot (t, name) ->
            let name = show_sanitized_name @@ bind_sanitize_capitalize name in
            Format.asprintf
              "class %s_%s_constructor_subtype(TypedDict):\n\
               \tkind: str  # \"%s_constructor\"\n\
               \t%s_element: %a"
              (show_sanitized_name typename)
              name name name
              (output_type_pretty ~prefix)
              t
          | t ->
            let name = constructor_from_path path in
            Format.asprintf
              "class %s_%s_subtype(TypedDict):\n\
               \tkind: str  # \"%s_constructor\"\n\
               \t%s_element: %a"
              (show_sanitized_name typename)
              name name name
              (output_type_pretty ~prefix)
              t in
        cur_elem :: res in
      aux new_res xs in
  fprintf ppf "\n%s\n\n" (String.concat "\n" (aux [] lst))

and declare_type_ ~prefix (m : type_) ppf (name : sanitized_name) =
  output_verbose ~level:1
  @@ sprintf "[python][declare_type_] Entering with name %s"
       (show_sanitized_name name) ;
  let mk_init (name : sanitized_name) = name in
  let output_type_pretty = output_type_pretty ~prefix in
  let output_type_content = output_type_content in
  match m with
  | Or _ ->
    let name = bind_sanitize_capitalize name in
    let sumtype_content =
      Format.asprintf "%a" (declare_sumtype ~typename:name) m in
    let d =
      if not (is_one_field_record m) then
        Curly
      else
        Nothing in
    fprintf ppf "%a%s = %s%s%s"
      (define_sumtype_subtypes ~prefix ~typename:name)
      m
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      (opening d) sumtype_content (closing d)
  | Base String ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "%s = str" typename
  | Base (Bool as b) | Base (Byte as b) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "%s = %a" typename
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b
  | Base b ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "%s = %a" typename
      (pp_base_type ~language:Factori_config.Python ~abstract:false ~prefix)
      b
  | TVar (vname, Base _b) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "%s = %s" typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
  | TVar (vname, _) ->
    let typename =
      show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name in
    fprintf ppf "class %s(%s):\n\tpass" typename
      (show_sanitized_name @@ bind_sanitize_capitalize vname)
  | Binary (BigMap, (_k, _v)) ->
    fprintf ppf "%s = %s.BigMap"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix (* output_type_pretty k output_type_pretty v *)
  | Binary (Lambda, (_k, _v)) ->
    fprintf ppf "%s = %s.Lambda"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix (* output_type_pretty k output_type_pretty v *)
  | Binary (Map, (_k, _v)) ->
    fprintf ppf "%s = %s.Map"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix (* output_type_pretty k output_type_pretty v *)
  | Unary (List, t) ->
    fprintf ppf "%s = List[%a]"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      output_type_pretty t
  | Unary (Set, t) ->
    fprintf ppf "%s = List[%a]"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      output_type_pretty t
  | Unary (Ticket, _t) ->
    fprintf ppf "%s = %s.Ticket"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix (* output_type_pretty t *)
  | Unary (Option, t) ->
    fprintf ppf "%s = Optional[%a]"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      output_type_pretty t
  | Contract _t ->
    fprintf ppf "%s = %s.contract"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      prefix
  (* output_type_pretty t *)
  | Annot (ep, _) -> declare_type_ ~prefix ep ppf (mk_init name)
  | Pair _ ->
    fprintf ppf "%s = %a"
      (show_sanitized_name @@ bind_sanitize_capitalize @@ mk_init name)
      (output_type_content ~prefix)
      m
  | Record r ->
    let name = bind_sanitize_capitalize name in
    let l =
      match r with
      | PairP l -> l
      | x -> [x] in
    let l = flattenP [] l in
    fprintf ppf "class %s(TypedDict):\n\t%a"
      (show_sanitized_name @@ mk_init name)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf "\n\t")
         (fun ppf (n, x) ->
           fprintf ppf "%s: %a" (show_sanitized_name n) output_type_pretty x))
      l

and output_type_encode ~prefix ppf = function
  | Base b ->
    fprintf ppf "%s.%a_encode" prefix
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (output_type_encode ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.tuple%d_encode(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_encode ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s_encode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s_encode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s_encode" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.list_encode(%a,%a)" prefix
      (output_type_encode ~prefix)
      a
      (output_type_class ~prefix)
      a
  | Unary (Set, a) ->
    fprintf ppf "%s.set_encode(%a,%a)" prefix
      (output_type_encode ~prefix)
      a
      (output_type_class ~prefix)
      a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.ticket_encode" prefix (* (output_type_encode ~prefix) t *)
  | Contract _c -> fprintf ppf "%s.contract_encode" prefix
  (* pp_micheline_
     * c *)
  (* (output_type_encode ~prefix) c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.big_map_encode(%a,%a,%a,%a)" prefix
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
      (output_type_class ~prefix)
      k
      (output_type_class ~prefix)
      v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s.lambda_encode(%a,%a)" prefix
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.map_encode(%a,%a,%a,%a)" prefix
      (output_type_encode ~prefix)
      k
      (output_type_encode ~prefix)
      v
      (output_type_class ~prefix)
      k
      (output_type_class ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.option_encode(%a,%a)" prefix
      (output_type_encode ~prefix)
      a
      (output_type_class ~prefix)
      a

and output_sumtype_class ~prefix ppf or_t =
  pp_ortype_python
    (fun ppf t_ ->
      match t_ with
      (*| Annot (u_, annot) when is_unit u_ ->
        fprintf ppf "unit_type() \"%s\"" (show_sanitized_name annot)*)
      | t_ -> fprintf ppf "%a" (output_type_class ~prefix) t_)
    ppf or_t

and output_type_class ~prefix ppf = function
  | Base b ->
    fprintf ppf "%s.%a_type()" prefix
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b
  | Annot (ep, _annot) -> fprintf ppf "%a" (output_type_class ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.tuple%d_type(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ", ")
         (output_type_class ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s_type()" (show_sanitized_name (Naming.get_or_create_name t))
  | Or (_ as t) -> output_sumtype_class ~prefix ppf t
  | TVar tn -> fprintf ppf "%s_type()" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s.%s_type(%a))" prefix (str_of_unary u)
      (output_type_class ~prefix)
      a
  | Contract _c -> fprintf ppf "%s.contract_type(MichelsonType)" prefix
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s.%s_type(%a,%a))" prefix (str_of_binary b)
      (output_type_class ~prefix)
      k
      (output_type_class ~prefix)
      v

and encode_type_decl ~prefix (m : type_) ppf (name : sanitized_name) =
  let output_type_encode = output_type_encode ~prefix in
  let prelude =
    asprintf "def %s_encode (arg: %s):\n\ttry:\n" (show_sanitized_name name)
      (show_sanitized_name @@ bind_sanitize_capitalize @@ name) in
  begin
    match m with
    | Base String ->
      (* this is a special case *)
      fprintf ppf "%s\t\treturn %s.string_encode(arg)" prelude prefix
    | Base b ->
      fprintf ppf "%s\t\treturn %s.%a_encode(arg)" prelude prefix
        (pp_base_type ~language:Factori_config.Python ~abstract:false
           ?prefix:None)
        b
    | Annot (ep, _) -> fprintf ppf "%a" (encode_type_decl ~prefix ep) name
    | TVar tn ->
      fprintf ppf "\n%s\t\treturn %s_encode(arg)" prelude
        (show_sanitized_name (fst tn))
    | Unary (List, a) ->
      fprintf ppf "%s\t\treturn %s.list_encode(%a,%a)(arg)" prelude prefix
        output_type_encode a
        (output_type_class ~prefix)
        a
    | Unary (Set, a) ->
      fprintf ppf "%s\t\treturn %s.list_encode(%a,%a)(arg)" prelude prefix
        output_type_encode a
        (output_type_class ~prefix)
        a
    | Unary (Ticket, _t) ->
      fprintf ppf "%s\t\treturn %s.ticket_encode(arg)" prelude prefix
    (* output_type_encode t *)
    | Contract _c ->
      fprintf ppf "%s\t\treturn %s.contract_encode(arg)" prelude prefix
    (* output_type_encode *)
    (* pp_micheline_ c *)
    | Binary (BigMap, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.big_map_encode(%a,%a,%a,%a)(arg)" prelude
        prefix output_type_encode k output_type_encode v
        (output_type_class ~prefix)
        k
        (output_type_class ~prefix)
        v
    | Binary (Lambda, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.lambda_encode(%a,%a)(arg)\t\t" prelude prefix
        output_type_encode k output_type_encode v
    | Binary (Map, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.map_encode(%a,%a,%a,%a)(arg)" prelude prefix
        output_type_encode k output_type_encode v
        (output_type_class ~prefix)
        k
        (output_type_class ~prefix)
        v
    | Unary (Option, a) ->
      fprintf ppf "%s\t\treturn %s.option_encode(%a,%a)(arg)" prelude prefix
        output_type_encode a
        (output_type_class ~prefix)
        a
    | Or _ as t ->
      fprintf ppf "def %s_encode (arg) -> OrType:\n\ttry:\n%a"
        (show_sanitized_name name)
        (output_sumtype_encode ~prefix "arg")
        t
    | Pair _ as t ->
      fprintf ppf
        "def %s_encode(arg : %s) -> PairType:\n\ttry:\n\t\treturn %a(arg) "
        (show_sanitized_name name)
        (show_sanitized_name @@ bind_sanitize_capitalize @@ name)
        output_type_encode t
    | Record r ->
      fprintf ppf "def %s_encode(arg : %s):\n\ttry:\n\t\treturn %a"
        (show_sanitized_name name)
        (show_sanitized_name @@ bind_sanitize_capitalize @@ name)
        (encode_record ~prefix) r
  end ;
  fprintf ppf
    "\n\
     \texcept Exception as e:\n\
     \t\tprint('Error in %s_encode, with input:' + str(arg) + 'Error:' + str(e))\n\
     \t\traise e" (show_sanitized_name name)

and encode_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_encode ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_python_build
        (fun s ppf t ->
          fprintf ppf "(%a(arg['%s']))"
            (output_type_encode ~prefix)
            t (show_sanitized_name s))
        ppf p in
  aux ep

and class_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_class ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_python_build ~mich_type:true
        (fun _s ppf t -> fprintf ppf "(%a)" (output_type_class ~prefix) t)
        ppf p in
  aux ep

and decode_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_decode ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_typescript ~pairparens:true
        ~f_leaf:(fun _name ppf x -> output_type_decode ~prefix ppf x)
        ~f_pair:(fun ppf d -> fprintf ppf "%s.tuple%d_decode " prefix d)
        ~delimiters:Parens ~sep:"," ppf p in
  aux ep

and output_type_decode ~prefix ppf = function
  | Base b ->
    fprintf ppf "%s.%a_decode" prefix
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (output_type_decode ~prefix) ep
  | Pair al ->
    fprintf ppf "%s.tuple%d_decode(%a)" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_decode ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s_decode" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s_decode" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s_decode" (show_sanitized_name (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.list_decode(%a)" prefix (output_type_decode ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.set_decode(%a)" prefix (output_type_decode ~prefix) a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.ticket_decode" prefix (* (output_type_decode ~prefix) t *)
  | Contract _c -> fprintf ppf "%s.contract_decode" prefix
  (* pp_micheline_
     * c *)
  (* (output_type_decode ~prefix) c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.big_map_decode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s.lambda_decode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.map_decode(%a,%a)" prefix
      (output_type_decode ~prefix)
      k
      (output_type_decode ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.option_decode(%a)" prefix (output_type_decode ~prefix) a

let rec iterate_over_fields ppf (lst : (sanitized_name * _) list)
    (f : int -> sanitized_name -> 'a -> string) (d : delimiter) (sep : string) =
  let rec aux res count = function
    | [] -> List.rev res
    | (k, v) :: xs ->
      let new_res =
        let cur_elem = f count k v in
        cur_elem :: res in
      aux new_res (count + 1) xs in
  fprintf ppf "%s%s%s\n" (opening d)
    (String.concat sep (aux [] 0 lst))
    (closing d)

and record_extract_decoded_values ?(is_one_field_record = false) m ppf
    record_name =
  let sep =
    if is_one_field_record then
      Nothing
    else
      Curly in
  let f _ (k : sanitized_name) (path, _) =
    if is_one_field_record then
      Format.asprintf "(%s%a)" record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path
    else
      Format.asprintf "'%s' : (%s%a)" (show_sanitized_name k) record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path in
  fprintf ppf "return %a" (fun ppf -> iterate_over_fields ppf m f sep) ",\n"

and record_to_list ep =
  (* path is how we get to the element in the tuple *)
  let rec aux ~path = function
    | LeafP (field_name, t) -> [(field_name, (path, t))]
    | PairP l -> List.concat (List.mapi (fun i x -> aux ~path:(path @ [i]) x) l)
  in
  aux ~path:[] ep

and decode_type_decl ~prefix (m : type_) ppf (name : sanitized_name) =
  let output_type_decode = output_type_decode ~prefix in
  let prelude =
    asprintf "def %s_decode (arg) -> %s:\n\ttry:\n" (show_sanitized_name name)
      (show_sanitized_name @@ bind_sanitize_capitalize @@ name) in
  begin
    match m with
    | Base String ->
      (* this is a special case *)
      fprintf ppf "%s\t\treturn %s.string_decode(arg)" prelude prefix
    | Base b ->
      fprintf ppf "%s\t\treturn %s.%a_decode(arg)" prelude prefix
        (pp_base_type ~language:Factori_config.Python ~abstract:false
           ?prefix:None)
        b
    | Annot (ep, _) -> fprintf ppf "%a" (decode_type_decl ~prefix ep) name
    | TVar tn ->
      fprintf ppf "\n%s\t\treturn %s_decode(arg)" prelude
        (show_sanitized_name (fst tn))
    | Unary (List, a) ->
      fprintf ppf "%s\t\treturn %s.list_decode(%a)(arg)" prelude prefix
        output_type_decode a
    | Unary (Set, a) ->
      fprintf ppf "%s\t\treturn %s.list_decode(%a)(arg)" prelude prefix
        output_type_decode a
    | Unary (Ticket, _t) ->
      fprintf ppf "%s\t\treturn %s.ticket_decode(arg)" prelude prefix
      (* output_type_decode t *)
    | Contract _c ->
      fprintf ppf "%s\t\treturn %s.contract_decode(arg)" prelude prefix
      (* output_type_decode *)
      (* pp_micheline_ c *)
    | Binary (BigMap, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.big_map_decode(%a,%a)(arg)" prelude prefix
        output_type_decode k output_type_decode v
    | Binary (Lambda, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.lambda_decode(%a,%a)(arg)" prelude prefix
        output_type_decode k output_type_decode v
    | Binary (Map, (k, v)) ->
      fprintf ppf "%s\t\treturn %s.map_decode(%a,%a)(arg)" prelude prefix
        output_type_decode k output_type_decode v
    | Unary (Option, a) ->
      fprintf ppf "%s\t\treturn %s.option_decode(%a)(arg)" prelude prefix
        output_type_decode a
    | Or _ as t ->
      fprintf ppf
        "def %s_decode (arg : MichelsonType) -> %s:\n\
         \ttry:\n\
         %a\n\
         \t\traise(ValueError('Value is neither left nor right'))"
        (show_sanitized_name name)
        (show_sanitized_name @@ bind_sanitize_capitalize @@ name)
        (output_sumtype_decode ~prefix "arg")
        t
    | Pair _ as t ->
      fprintf ppf "def %s_decode(arg) -> %s:\n\ttry:\n\t\treturn %a(arg) "
        (show_sanitized_name name)
        (show_sanitized_name @@ bind_sanitize_capitalize @@ name)
        output_type_decode t
    | Record r ->
      let is_one_field_record = is_one_field_record m in
      let lst = record_to_list r in
      fprintf ppf
        "def %s_decode(arg) -> %s:\n\
         \ttry:\n\
         \t\tbefore_projection=%a(arg)\n\
         \t\t%a\n"
        (show_sanitized_name name)
        (show_sanitized_name @@ bind_sanitize_capitalize @@ name)
        (decode_record ~prefix) r
        (record_extract_decoded_values ~is_one_field_record lst)
        "before_projection"
  end ;
  fprintf ppf
    "\n\
     \texcept Exception as e:\n\
     \t\tprint('Error in %s_decode, with input: ' + \
     str(arg.to_micheline_value(lazy_diff=None)) + ', and error is:' + str(e))\n\
     \t\traise e" (show_sanitized_name name)

and micheline_type_decl ~prefix (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  let prelude = asprintf "def %s_type():\n" san_name in
  match m with
  | Base b ->
    fprintf ppf "%s\treturn %s.%a_type()" prelude prefix
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (micheline_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s\treturn %s_type()" prelude (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s\treturn (%s.%s_type(%a))" prelude prefix (str_of_unary u)
      (output_type_class ~prefix)
      a
  | Contract c ->
    fprintf ppf "%s\treturn %a" prelude (output_type_class ~prefix) (Contract c)
  | Binary (b, (k, v)) ->
    fprintf ppf "%s\treturn (%s.%s_type(%a,%a))" prelude prefix
      (str_of_binary b)
      (output_type_class ~prefix)
      k
      (output_type_class ~prefix)
      v
  | Or (_ as t) ->
    fprintf ppf "def %s_type():\n\treturn %a" san_name
      (output_sumtype_class ~prefix)
      t
  | Pair _ as t ->
    fprintf ppf "def %s_type():\n\treturn %a" san_name
      (output_type_class ~prefix)
      t
  | Record r ->
    fprintf ppf "def %s_type():\n\treturn %a" san_name (class_record ~prefix) r

and decode_from_micheline_decl ~prefix (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  let prelude =
    asprintf "def %s_decode_from_micheline(arg : Micheline):\n" san_name in
  match m with
  | Base b ->
    fprintf ppf "%s\treturn %s.%a_decode_from_micheline(arg)" prelude prefix
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) ->
    fprintf ppf "%a" (decode_from_micheline_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s\treturn %s_decode_from_micheline(arg)" prelude
      (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s\treturn (%s.%s_decode_from_micheline(%a)(arg))" prelude
      prefix (str_of_unary u)
      (output_type_class ~prefix)
      a
  | Contract _c ->
    fprintf ppf "%s\treturn %s.contract_decode_from_micheline(arg)" prelude
      prefix
  | Binary (b, (k, v)) ->
    fprintf ppf "%s\treturn (%s.%s_decode_from_micheline(%a,%a)(arg))" prelude
      prefix (str_of_binary b)
      (output_type_class ~prefix)
      k
      (output_type_class ~prefix)
      v
  | Or (_ as t) ->
    fprintf ppf
      "def %s_decode_from_micheline(arg):\n\
       \tcls = %a\n\
       \treturn cls.from_micheline_value(arg)" san_name
      (output_sumtype_class ~prefix)
      t
  | Pair _ as t ->
    fprintf ppf
      "def %s_decode_from_micheline(arg):\n\
       \tcls = %a\n\
       \treturn cls.from_micheline_value(arg)" san_name
      (output_type_class ~prefix)
      t
  | Record r ->
    fprintf ppf
      "def %s_decode_from_micheline(arg):\n\
       \tcls = %a\n\
       \treturn cls.from_micheline_value(arg)" san_name (class_record ~prefix) r

and decode_full_decl ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  fprintf ppf
    "def %s_decode_full(arg : Micheline):\n\
     \treturn %s_decode(%s_decode_from_micheline(arg))" san_name san_name
    san_name

and encode_full_decl ppf name =
  let name = bind_sanitize_uncapitalize name in
  let san_name = show_sanitized_name name in
  fprintf ppf
    "def %s_encode_full(arg : Micheline):\n\
     \treturn %s_encode(arg).to_micheline_value()" san_name san_name

and output_sumtype_generator ~prefix ppf t_ =
  let lst = sumtype_to_list t_ in
  fprintf ppf "%a"
    (pp_print_list ~pp_sep:(tag ",") (fun ppf (t_, path) ->
         fprintf ppf "(%a)"
           (fun ppf at ->
             match at with
             | Annot (u_, name) when is_unit u_ ->
               fprintf ppf "{ 'kind' : \"%s_constructor\"}"
                 (show_sanitized_name (bind_sanitize_capitalize name))
             | Annot (t_, name) ->
               let name = show_sanitized_name @@ bind_sanitize_capitalize name in
               fprintf ppf
                 "{ 'kind' : \"%s_constructor\", '%s_element':  (%a ())}" name
                 name
                 (output_type_generator ~prefix)
                 t_
             | t ->
               let name = constructor_from_path path in
               fprintf ppf "{ 'kind' : \"%s\", '%s' : (%a ())}"
                 (sprintf "%s_constructor" (constructor_from_path path))
                 (show_sanitized_name @@ bind_sanitize_capitalize
                @@ sanitized_of_str name)
                 (output_type_generator ~prefix)
                 t)
           t_))
    lst

and output_type_generator ~prefix ppf t =
  let output_type_generator = output_type_generator ~prefix in
  match t with
  | Base b ->
    fprintf ppf "%s.%a_generator" prefix
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b
  | Annot (ep, _) -> fprintf ppf "%a" output_type_generator ep
  | Pair al ->
    fprintf ppf "(%s.tuple%d_generator(%a))" prefix (List.length al)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",\n")
         output_type_generator)
      al
  | Record _ as t ->
    fprintf ppf "%s_generator"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t -> output_sumtype_generator ~prefix ppf t
  | TVar tn -> fprintf ppf "%s_generator" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s.%s_generator(%a))" prefix (str_of_unary u)
      output_type_generator a
  | Contract _c -> fprintf ppf "%s.contract_generator" prefix
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s.%s_generator(%a,%a))" prefix (str_of_binary b)
      output_type_generator k output_type_generator v

and generator_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, x) -> fprintf ppf "%a ()" (output_type_generator ~prefix) x
    | PairP l ->
      let l = flattenP [] l in
      fprintf ppf "{%a}"
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf (n, x) ->
             fprintf ppf "'%s' : %a ()" (show_sanitized_name n)
               (output_type_generator ~prefix)
               x))
        l in
  aux ep

and generator_type_decl ~prefix (m : type_) ppf name =
  let name = bind_sanitize_uncapitalize name in
  let typename = bind_sanitize_capitalize name in
  let prelude =
    asprintf "def %s_generator() -> %s:\n\treturn " (show_sanitized_name name)
      (show_sanitized_name typename) in
  let closing = sprintf "" in
  match m with
  | Base b ->
    fprintf ppf "%s%s.%a_generator ()%s" prelude prefix
      (pp_base_type ~language:Factori_config.Python ~abstract:false ?prefix:None)
      b closing
  | Annot (ep, _) -> fprintf ppf "%a" (generator_type_decl ~prefix ep) name
  | TVar tn ->
    fprintf ppf "\n%s%s_generator ()%s" prelude
      (show_sanitized_name (fst tn))
      closing
  | Unary (u, a) ->
    fprintf ppf "%s(%s.%s_generator (%a)) ()%s" prelude prefix (str_of_unary u)
      (output_type_generator ~prefix)
      a closing
  | Contract _c ->
    fprintf ppf "%s%s.contract_generator ()%s" prelude prefix closing
  | Binary (b, (k, v)) ->
    fprintf ppf "%s(%s.%s_generator (%a,%a)) ()%s" prelude prefix
      (str_of_binary b)
      (output_type_generator ~prefix)
      k
      (output_type_generator ~prefix)
      v closing
  | Or _ as t ->
    fprintf ppf
      "def %s_generator() -> %s:\n\
       \tl : list[%s] = [%a];\n\
       \treturn %s.chooseFrom(l)" (show_sanitized_name name)
      (show_sanitized_name typename)
      (show_sanitized_name typename)
      (output_sumtype_generator ~prefix)
      t prefix
  | Pair _ as t ->
    fprintf ppf "def %s_generator() -> %s:\n\treturn %a()"
      (show_sanitized_name name)
      (show_sanitized_name typename)
      (output_type_generator ~prefix)
      t
  | Record r ->
    fprintf ppf "def %s_generator() -> %s:\n\treturn %a"
      (show_sanitized_name name)
      (show_sanitized_name typename)
      (generator_record ~prefix) r

and assert_encode_decode ppf name =
  let name = show_sanitized_name name in
  fprintf ppf
    "def assert_identity_%s():\n\
     \ttemp=%s_generator()\n\
     \ttemp_encoded=%s_encode(temp)\n\
     \ttry:\n\
     \t\tprint('++++ %s: ' + \
     str(temp_encoded.to_micheline_value(lazy_diff=None)))\n\
     \texcept Exception as e:\n\
     \t\tprint(\"---- couldn't print encoded version of %s: \" + str(temp))\n\
     \ttemp1=%s_decode(temp_encoded)\n\
     \tif temp1 == temp:\n\
     \t\tprint('%s check passed')\n\
     \telse:\n\
     \t\tif isinstance(temp, dict) and isinstance(temp1, dict):\n\
     \t\t\ttemp = dict(sorted(temp.items()))\n\
     \t\t\ttemp1 = dict(sorted(temp1.items()))\n\
     \t\t\tdiff_string = \
     '\\n'.join(difflib.Differ().compare(str(temp1).splitlines(), \
     str(temp).splitlines()))\n\
     \t\t\tprint('%s check failed: temp = '+str(temp)+' is not equal to \
     '+str(temp1)+'\\ndiff:\\n'+diff_string)\n\n\
     #Use this for debugging:\n\
     #try:\n\
     #\tassert_identity_%s()\n\
     #except Exception as e:\n\
     #\tprint('Error in assert_identity_%s' + str(e))\n"
    name name name name name name name name name name

and call_entrypoint ~contract_name ppf name =
  let _ = contract_name in
  let capitalized_ep_name =
    show_sanitized_name @@ bind_sanitize_capitalize name in
  let ep_name = show_sanitized_name name in
  fprintf ppf
    "def call%s(kt1 : str, _from : blockchain.identity, param : %s, \
     networkName = \"ghostnet\", debug=False):\n\
     \tinput = %s_encode(param)\n\
     \tophash = blockchain.callEntrypoint(_from, kt1, \"%s\", \
     input.to_micheline_value(lazy_diff=None), \
     0,network=networkName,debug=debug)\n\
     \treturn ophash" capitalized_ep_name capitalized_ep_name ep_name
    (get_original name)

and deploy ~storage_name ~contract_name ~network ppf () =
  let _ = (storage_name, network, ppf) in
  let contract_name = sanitized_of_str contract_name in
  fprintf ppf
    "def deploy(_from : blockchain.identity, initial_storage : %s, amount, \
     network, debug = False):\n\
     \tif(debug):\n\
     \t\tprint(\"Deploying contract %s\")\n\
     \tstorage_encoded = %s_encode(initial_storage)\n\
     \tif(debug):\n\
     \t\tprint(initial_storage)\n\
     \tkt1 : str = blockchain.deployContract(\"%s\",_from, \
     storage_encoded.to_micheline_value(lazy_diff=None), amount, network, \
     debug)\n\
     \treturn kt1"
    (show_sanitized_name @@ bind_sanitize_capitalize storage_name)
    (show_sanitized_name contract_name)
    (show_sanitized_name @@ storage_name)
    (Python_SDK.code_path ~dir:(Factori_config.get_dir ()) ~contract_name ())

let print_original_blockchain_storage ~final_storage_type_ ~original_storage ppf
    () =
  let open Tzfunc.Proto in
  try
    let value = value_of_typed_micheline original_storage final_storage_type_ in
    fprintf ppf "\ninitial_blockchain_storage=%a#%s\n\n" pp_value_to_python
      value
      (EzEncoding.construct micheline_enc.json original_storage)
  with e ->
    handle_exception e ;
    fprintf ppf
      "\n\
       initial_blockchain_storage = raise \"Could not download initial \
       storage, this is a bug\"\n\
       (*%s*)\n"
      (EzEncoding.construct micheline_enc.json original_storage)

let pp_define_big_map_access ~store_name ~encode_key ~decode_value
    ~encoded_big_map ~network ~pretty_name ppf f =
  Format.fprintf ppf
    "def get_%s_value(key,%s,network='%s'):\n\
     \t%a\n\
     \treturn blockchain.find_big_map(key,%s,%s,%s,network)\n\
     @]@."
    (show_sanitized_name pretty_name)
    (show_sanitized_name store_name)
    (Factori_network.string_of_network network)
    f ()
    (show_sanitized_name encoded_big_map)
    (show_sanitized_name encode_key)
    (show_sanitized_name decode_value)

let pp_pattern_position previous_f position ppf () =
  match position with
  | In_pair (i, Pair _) -> Format.fprintf ppf "%a[%d]" previous_f () i
  | In_record ((name_field, _), Record _) ->
    Format.fprintf ppf "%a['%s']" previous_f () (show_sanitized_name name_field)
  | _ ->
    raise (Factori_errors.GenericError ("Output_Python", "Incoherent types"))

let pp_big_map_access ~prefix ~key ~value ~position ~bm ~network ppf =
  let store_name = sanitized_of_str "storage" in
  let big_map_name = sanitized_of_str "bigMap" in
  let key_encoding = sanitized_of_str "encode_key" in
  let value_decoding = sanitized_of_str "decode_value" in
  let value_encoding = sanitized_of_str "encode_value" in
  let key_type = sanitized_of_str "key_type" in
  let value_type = sanitized_of_str "value_type" in
  let big_map_encoding = sanitized_of_str "big_map_encode" in
  let output_type_class = output_type_class ~prefix in
  let output_type_encode = output_type_encode ~prefix in
  let output_type_decode = output_type_decode ~prefix in
  let print_key_type ppf () =
    Format.fprintf ppf "%s = %a"
      (show_sanitized_name key_type)
      output_type_class key in
  let print_value_type ppf () =
    Format.fprintf ppf "%s = %a"
      (show_sanitized_name value_type)
      output_type_class value in
  let print_value_decode ppf () =
    Format.fprintf ppf "%s = %a"
      (show_sanitized_name value_decoding)
      output_type_decode value in
  let print_value_encode ppf () =
    Format.fprintf ppf "%s = %a"
      (show_sanitized_name value_encoding)
      output_type_encode value in
  let print_key_encode ppf () =
    Format.fprintf ppf "%s = %a"
      (show_sanitized_name key_encoding)
      output_type_encode key in
  let print_big_map_encode ppf () =
    Format.fprintf ppf "%s = %s.big_map_encode(%s,%s,%s,%s)"
      (show_sanitized_name big_map_encoding)
      prefix
      (show_sanitized_name key_encoding)
      (show_sanitized_name value_encoding)
      (show_sanitized_name key_type)
      (show_sanitized_name value_type) in
  let retrieve_big_map =
    List.fold_left pp_pattern_position
      (fun ppf () ->
        Format.fprintf ppf "%s = %s"
          (show_sanitized_name big_map_name)
          (show_sanitized_name store_name))
      position in
  let mich_bm = sanitized_of_str "encoded_big_map" in
  let print_big_map_encoded ppf () =
    Format.fprintf ppf "%s = %s(%s)"
      (show_sanitized_name mich_bm)
      (show_sanitized_name big_map_encoding)
      (show_sanitized_name big_map_name) in
  let prelude ppf () =
    Format.fprintf ppf "%a\n\t%a\n\t%a\n\t%a\n\t%a\n\t%a\n\t%a\n\t%a"
      retrieve_big_map () print_key_type () print_value_type () print_key_encode
      () print_value_encode () print_big_map_encode () print_value_decode ()
      print_big_map_encoded () in
  pp_define_big_map_access ~store_name ~encode_key:key_encoding
    ~decode_value:value_decoding ~encoded_big_map:mich_bm ~network
    ~pretty_name:bm ppf prelude

let rec pp_interface_instruction_python ~prefix ~contract_name ~storage_name
    ~network ppf ii =
  match ii with
  | Define_type (name, type_) ->
    fprintf ppf "%a\n\n%a\n\n%a\n\n%a\n\n%a\n\n%a\n\n%a\n\n%a\n\n%a"
      (micheline_type_decl ~prefix type_)
      name
      (declare_type_ ~prefix type_)
      name
      (generator_type_decl ~prefix type_)
      name
      (encode_type_decl ~prefix type_)
      name
      (decode_type_decl type_ ~prefix)
      name
      (decode_from_micheline_decl type_ ~prefix)
      name encode_full_decl name decode_full_decl name assert_encode_decode name
  | Deploy -> deploy ~storage_name ~contract_name ~network ppf ()
  | Seq ii ->
    (pp_interface_python ~prefix ~contract_name ~storage_name ~network) ppf ii
  | Entrypoint (name, _) -> call_entrypoint ~contract_name ppf name
  | Big_map_access { key; bm; value; position; storage = _ } ->
    pp_big_map_access ~prefix ~key ~value ~position ~bm ~network ppf

and pp_interface_python ~prefix ~network ~contract_name ~storage_name
    (ppf : formatter) (i : interface) =
  output_verbose ~level:3
    (asprintf "Entering pp_interface_python with interface %a" pp_interface i) ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_python ~prefix ~contract_name ~storage_name
       ~network)
    ppf i

let download_and_decode_storage ~storage_name ~network ppf () =
  fprintf ppf
    {|def storage_download(kt1,network='%s'):
     return blockchain.storage_download_decode(kt1,%s_decode,network)@.@.|}
    (Factori_network.string_of_network network)
    (show_sanitized_name storage_name) ;
  fprintf ppf
    {|def test_storage_download(kt1,network='%s'):
     storage = storage_download(kt1,network)
     return %s_decode_full(%s_encode_full(storage))
     |}
    (Factori_network.string_of_network network)
    (show_sanitized_name storage_name)
    (show_sanitized_name storage_name)

let process_entrypoints ~prefix ~contract_name ~storage_name
    ~final_storage_type_ ~network ?(original_storage = None) ppf interface =
  let contract_name = show_sanitized_name contract_name in
  output_verbose ~level:2
    (asprintf
       "[python] Entering process_entrypoints with final storage: %a\n\
       \ and interface %a" pp_type_ final_storage_type_ pp_interface interface) ;
  let interface =
    try_overflow ~msg:"extract interface ocaml" (fun () -> interface) in
  let _ = check_interface_for_doubles interface in
  let original_blockchain_storage =
    match original_storage with
    | None -> "#No KT1"
    | Some original_storage ->
      Format.asprintf "\n%a"
        (print_original_blockchain_storage ~final_storage_type_
           ~original_storage)
        () in
  fprintf ppf
    "from pytezos import pytezos\n\
     #from pytezos import ContractInterface\n\
     from pytezos.michelson.types.pair import PairType\n\
     from pytezos.michelson.types.sum import OrType\n\
     from typing import TypedDict, Optional, List, Tuple, Union, Set\n\
     from pytezos.michelson.types.base import MichelsonType\n\
     from pytezos.michelson.micheline import Micheline\n\
     import difflib\n\
     from %s import blockchain, %s\n\
     %a\n\n\
     %a%s"
    Python_SDK.libraries_dir_name prefix
    (pp_interface_python ~prefix ~contract_name ~storage_name ~network)
    interface
    (download_and_decode_storage ~storage_name ~network)
    () original_blockchain_storage
