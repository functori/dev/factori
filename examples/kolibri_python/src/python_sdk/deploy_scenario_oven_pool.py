
from importlib.metadata import metadata
import oven_pool_python_interface
import blockchain

def exec():
    kt1 = oven_pool_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= oven_pool_python_interface.initial_blockchain_storage,
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 oven_pool " + str(kt1) + " sent to a node.")
    return kt1
