(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file `factori_config.ml` provides types and functions for
configuring contracts and generating code interfaces in the Factori codebase. It
defines several types and provides various functions for getting configuration
and file names for different programming languages, such as Python, C#, and
TypeScript. The functions help generate file paths for different programming
languages, such as Python, C#, and TypeScript. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Factori_utils
open Factori_file
open Factori_network
open Factori_errors

let ( // ) = Factori_options.concat

type factori_config = unit [@@deriving encoding]

type address = string [@@deriving encoding]

type interface_language =
  | OCaml [@key "ocaml"]
  | Typescript [@key "typescript"]
  | Python [@key "python"]
  | Csharp [@key "csharp"]
(* Not done yet *)
[@@deriving show, encoding]

(** WARNING: fill this out if new languages are added  *)
let all_languages = [OCaml; Typescript; Python; Csharp]

type crawler =
  | Crawlori [@key "crawlori"]
  | Dipdup [@key "dipdup"]
[@@deriving show, encoding]

(** WARNING: fill this out if new crawlers are added  *)
let all_crawlers = [Crawlori; Dipdup]

type contract_format =
  | Michelson
  | MichelsonJson
  | KT1
[@@deriving show, encoding]

type factori_option =
  | Language of interface_language
  | Crawler of crawler
  | Webinterface
[@@deriving show, encoding]

(** WARNING: fill this out if new options are added  *)
let all_options =
  [Webinterface]
  @ List.map (fun c -> Crawler c) all_crawlers
  @ List.map (fun l -> Language l) all_languages

type contract = {
  contract_name : string;
  original_format : contract_format;
  original_kt1 : (address * network) option;
  import_date : string;
  options : factori_option list;
}
[@@deriving encoding]

let has_language (i : interface_language) (c : contract) =
  List.mem (Language i) c.options

let get_default_contract_name ~kt1 =
  String.uncapitalize_ascii (String.sub kt1 0 7)

type red_flags = (string * int) list [@@deriving encoding]

type local_config = {
  bad_nodes : red_flags;
  contracts : contract list;
}
[@@deriving encoding]

type version_config = { version : string } [@@deriving encoding]

(* The main directory of the project, either specified from arguments
   or the current dir *)
let get_dir () =
  match !Factori_options.dir with
  | None -> Sys.getcwd ()
  | Some dir ->
    if Filename.is_relative dir then
      Sys.getcwd () // dir
    else
      dir

let default_get_dir = function
  | None -> get_dir ()
  | Some path -> path

let get_contracts_dir ~dir = dir // "src" // "contracts"

let get_typenaming_file ~dir ~contract_name =
  get_contracts_dir ~dir // "type_naming"
  // (get_sanitized contract_name ^ ".json")

let get_local_config_name ~dir = Factori_options.concat dir "contracts.json"

let get_version_file_name ~dir = Factori_options.concat dir "project.json"

let get_contract_signature_filename ~contract_name ~dir =
  get_contracts_dir ~dir
  // Format.sprintf "%s_signature.json" (show_sanitized_name contract_name)

let verify_factori_repo ~dir () =
  let config = get_local_config_name ~dir in
  if not @@ Sys.file_exists config then raise (NotAFactoriRepo dir)

type contract_signature = Proto.micheline * Tzfunc.Node.entrypoints

let contract_signature_enc =
  let open Json_encoding in
  obj2
    (req "storage" Proto.micheline_enc.json)
    (req "entrypoints" Tzfunc.Node.entrypoints_enc.json)

let marshal_contract_signature ~dir ~contract_name storage
    (entrypoints : Tzfunc.Node.entrypoints) =
  let filename = get_contract_signature_filename ~contract_name ~dir in
  serialize_file ~filename ~enc:contract_signature_enc (storage, entrypoints)

let unmarshal_contract_signature ~dir ~contract_name =
  let filename = get_contract_signature_filename ~contract_name ~dir in
  deserialize_file ~silent:false ~filename ~enc:contract_signature_enc

(* Dir name for sdks we did this like with the include so that in the future
   if we want different name for sdk's we will just have to change this file*)

module Dir_name = struct
  let interface_dir_name = "interface"

  let deploy_dir_name = "deploy"

  let scenarios_dir_name = "scenarios"

  let libraries_dir_name = "libraries"
end
(* OCaml file names and directories *)

module OCaml_SDK = struct
  include Dir_name

  let base_dir ?(relative = false) ~dir () =
    let tail = "src" // "ocaml_sdk" in
    if relative then
      tail
    else
      dir // tail

  let interface_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // interface_dir_name

  let deploy_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // deploy_dir_name

  let libraries_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // libraries_dir_name

  let scenarios_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // scenarios_dir_name

  let interface_basename ~contract_name =
    show_sanitized_name contract_name ^ "_ocaml_interface"

  let abstract_interface_basename ~contract_name =
    show_sanitized_name contract_name ^ "_abstract_ocaml_interface"

  let crawlori_tables_basename ~contract_name =
    show_sanitized_name contract_name ^ "_tables"

  let crawlori_plugin_basename ~contract_name =
    show_sanitized_name contract_name ^ "_plugin"

  let abstract_interface_filename ~contract_name =
    abstract_interface_basename ~contract_name ^ ".ml"

  let abstract_interface_mli_filename ~contract_name =
    abstract_interface_basename ~contract_name ^ ".mli"

  let interface_filename ~contract_name =
    interface_basename ~contract_name ^ ".ml"

  let interface_mli_filename ~contract_name =
    interface_basename ~contract_name ^ ".mli"

  let crawlori_tables_filename ~contract_name =
    crawlori_tables_basename ~contract_name ^ ".ml"

  let crawlori_plugin_filename ~contract_name =
    crawlori_plugin_basename ~contract_name ^ ".ml"

  let factori_abstract_types_path ?(relative = false) ~dir () =
    libraries_dir ~relative ~dir () // "factori_abstract_types.ml"

  let factori_types_path ?(relative = false) ~dir () =
    libraries_dir ~relative ~dir () // "factori_types.ml"

  let blockchain_path ?(relative = false) ~dir () =
    libraries_dir ~relative ~dir () // "blockchain.ml"

  let crawlori_dir_name = "crawlori"

  let crawlori_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // crawlori_dir_name

  (* OCaml dirs and names for factori deploy *)

  (* OCaml dirs and names for scenarios *)
  let scenarios_filename = "scenario.ml"

  let scenarios_example_filename = "scenario.ml.example"

  let scenarios_path ?(relative = false) ~dir () =
    scenarios_dir ~relative ~dir () // scenarios_filename

  let scenarios_example_path ?(relative = false) ~dir () =
    scenarios_dir ~relative ~dir () // scenarios_example_filename

  let abstract_interface_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir ()
    // abstract_interface_filename ~contract_name

  let abstract_interface_mli_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir ()
    // abstract_interface_mli_filename ~contract_name

  let interface_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // interface_filename ~contract_name

  let interface_mli_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // interface_mli_filename ~contract_name

  let code_basename ~contract_name () =
    show_sanitized_name contract_name ^ "_code"

  let crawlori_tables_path ?(relative = false) ~dir ~contract_name () =
    crawlori_dir ~relative ~dir () // crawlori_tables_filename ~contract_name

  let crawlori_plugin_path ?(relative = false) ~dir ~contract_name () =
    crawlori_dir ~relative ~dir () // crawlori_plugin_filename ~contract_name

  let crawlori_bm_utils_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "bm_utils.ml"

  let crawlori_info_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "info.ml"

  let crawlori_register_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "register.ml"

  let crawlori_common_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "common.ml"

  let crawlori_converters_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "converters.sexp"

  let crawlori_crawler_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "crawler.ml"

  let crawlori_static_tables_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "static_tables.ml"

  let crawlori_contracts_table_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "contracts_table.ml"

  let crawlori_contracts_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "contracts.ml"

  let crawlori_update_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "update.ml"

  let crawlori_config_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "config.json"

  let crawlori_readme_path ?(relative = false) ~dir () =
    crawlori_dir ~relative ~dir () // "README"

  let code_filename_mli ~contract_name () =
    code_basename ~contract_name () ^ ".mli"

  let code_filename ~contract_name = code_basename ~contract_name () ^ ".ml"

  let code_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // code_filename ~contract_name

  let code_mli_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // code_filename_mli ~contract_name ()

  let generate_sdk ~dir () =
    make_dir (base_dir ~dir ()) ;
    make_dir (libraries_dir ~dir ()) ;
    make_dir (interface_dir ~dir ()) ;
    make_dir (scenarios_dir ~dir ())
end

module Python_SDK = struct
  include Dir_name
  (* Python file names and directories *)

  let base_dir ?(relative = false) ~dir () =
    let tail = "src" // "python_sdk" in
    if relative then
      tail
    else
      dir // tail

  let interface_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // interface_dir_name

  let interface_basename ~contract_name =
    show_sanitized_name contract_name ^ "_python_interface"

  let code_basename ~contract_name () =
    show_sanitized_name contract_name ^ "_code"

  let interface_filename ~contract_name =
    interface_basename ~contract_name ^ ".py"

  let code_filename ~contract_name = code_basename ~contract_name () ^ ".json"

  let interface_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // interface_filename ~contract_name

  let libraries_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // libraries_dir_name

  let factori_types_path ~dir = libraries_dir ~dir () // "factori_types.py"

  let code_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // code_filename ~contract_name

  let blockchain_path ~dir = libraries_dir ~dir () // "blockchain.py"

  (* Python dirs and names for factori deploy *)

  let deploy_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // deploy_dir_name

  (* Python dirs and names for scenarios *)
  let scenarios_filename = "scenario.py"

  let scenarios_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // scenarios_dir_name

  let scenarios_path ?(relative = false) ~dir () =
    scenarios_dir ~relative ~dir () // scenarios_filename

  let generate_deploy ~dir () =
    let deploy = deploy_dir ~dir () in
    let init = deploy // "__init__.py" in
    make_dir deploy ;
    if not @@ Sys.file_exists init then write_file ~filename:init ""

  let generate_sdk ~dir () =
    let lib = libraries_dir ~dir () in
    let inter = interface_dir ~dir () in
    let scenario = scenarios_dir ~dir () in
    make_dir (base_dir ~dir ()) ;
    make_dir lib ;
    write_file ~filename:(lib // "__init__.py") "" ;
    make_dir inter ;
    write_file ~filename:(inter // "__init__.py") "" ;
    make_dir scenario ;
    write_file ~filename:(scenario // "__init__.py") ""
end

module Csharp_SDK = struct
  include Dir_name
  (* Csharp file names and directories *)

  let base_dir ?(relative = false) ~dir () =
    let tail = "src" // "csharp_sdk" in
    if relative then
      tail
    else
      dir // tail

  let interface_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // interface_dir_name

  let interface_basename ~contract_name =
    show_sanitized_name contract_name ^ "_csharp_interface"

  let code_basename ~contract_name = show_sanitized_name contract_name ^ "_code"

  let interface_filename ~contract_name =
    interface_basename ~contract_name ^ ".cs"

  let code_filename ~contract_name = code_basename ~contract_name ^ ".json"

  let interface_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // interface_filename ~contract_name

  let code_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // code_filename ~contract_name

  let libraries_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // libraries_dir_name

  let factori_types_path ~dir = libraries_dir ~dir () // "factori_types.cs"

  let project_file_path ~dir = base_dir ~dir () // "project_file.csproj"

  let sdk_file_path ~dir = base_dir ~dir () // "csharp_sdk.csproj"

  let blockchain_path ~dir = libraries_dir ~dir () // "blockchain.cs"

  (* Csharp dirs and names for factori deploy *)

  let deploy_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // deploy_dir_name

  (* Csharp dirs and names for scenarios *)
  let scenarios_filename = "Scenario.cs"

  let scenarios_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // scenarios_dir_name

  let scenarios_path ?(relative = false) ~dir () =
    scenarios_dir ~relative ~dir () // scenarios_filename

  let generate_sdk ~dir () =
    make_dir (base_dir ~dir ()) ;
    make_dir (libraries_dir ~dir ()) ;
    make_dir (interface_dir ~dir ()) ;
    make_dir (deploy_dir ~dir ()) ;
    make_dir (scenarios_dir ~dir ())
end

module Typescript_SDK = struct
  include Dir_name
  (* Typescript file names and directories *)

  let base_dir ?(relative = false) ~dir () =
    let tail = "src" // "typescript_sdk" in
    if relative then
      tail
    else
      dir // tail

  let interface_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // interface_dir_name

  let interface_dist_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // "dist" // interface_dir_name

  let interface_filename ~contract_name =
    show_sanitized_name contract_name ^ "_interface.ts"

  let code_basename ~contract_name =
    (* let contract_name = sanitize_basename contract_name in *)
    Format.sprintf "%s_code" (show_sanitized_name contract_name)

  let libraries_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // libraries_dir_name

  let code_filename ~contract_name =
    Format.sprintf "%s.json" (code_basename ~contract_name)

  let code_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // code_filename ~contract_name

  let interface_path ?(relative = false) ~dir ~contract_name () =
    interface_dir ~relative ~dir () // interface_filename ~contract_name

  let scenario_basename ~contract_name =
    let contract_name = sanitize_basename contract_name in
    Format.sprintf "scenario_%s" (show_sanitized_name contract_name)

  (* Typescript dirs and names for factori deploy *)

  let deploy_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // deploy_dir_name

  let web_dir_name = "web_src"

  let web_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // web_dir_name

  (* Typescript dirs and names for scenarios *)
  let scenarios_filename = "scenario.ts"

  let scenarios_dir ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // scenarios_dir_name

  let scenarios_path ?(relative = false) ~dir () =
    scenarios_dir ~relative ~dir () // scenarios_filename

  let functolib_path ?(relative = false) ~dir () =
    libraries_dir ~relative ~dir () // "functolib.ts"

  let tsconfig_path ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // "tsconfig.json"

  let package_dot_json_path ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // "package.json"

  let webpack_config_js_path ?(relative = false) ~dir () =
    base_dir ~relative ~dir () // "webpack.config.js"

  let public_dir ?(relative = false) ~dir () =
    web_dir ~relative ~dir () // "public"

  let index_html_path ?(relative = false) ~dir () =
    public_dir ~relative ~dir () // "index.html"

  let index_ts_path ?(relative = false) ~dir () =
    web_dir ~relative ~dir () // "index.ts"

  let app_vue_path ?(relative = false) ~dir () =
    web_dir ~relative ~dir () // "App.vue"

  let get_vue_components_dir ?(relative = false) ~dir () =
    web_dir ~relative ~dir () // "components"

  let storage_vue_path ?(relative = false) ~dir () =
    get_vue_components_dir ~relative ~dir () // "Storage.vue"

  let vue_shim_ts_path ?(relative = false) ~dir () =
    web_dir ~relative ~dir () // "vue-shim.d.ts"

  let generate_sdk ~dir () =
    make_dir (base_dir ~dir ()) ;
    make_dir (libraries_dir ~dir ()) ;
    make_dir (interface_dir ~dir ()) ;
    make_dir (scenarios_dir ~dir ()) ;
    make_dir (public_dir ~dir ())
end

let get_dipdup_dir ~dir = dir // "src" // "dipdup"

let get_dipdup_handlers_dir ~dir = get_dipdup_dir ~dir // "handlers"

let get_dipdup_models_path ~dir () = get_dipdup_dir ~dir // "models.py"

let get_dipdup_configuration_path ~dir () = get_dipdup_dir ~dir // "dipdup.yml"

let get_dipdup_storage_handler_path ~dir ~contract_name =
  let filename = Format.sprintf "on_%s_new_storage.py" contract_name in
  get_dipdup_handlers_dir ~dir // filename

let get_dipdup_ep_handler_path ~dir ~table_name =
  let filename = Format.sprintf "on_%s.py" table_name in
  get_dipdup_handlers_dir ~dir // filename

let get_dipdup_bm_handler_path ~dir ~contract_name ~bm_name =
  let filename = Format.sprintf "on_%s_%s_updates.py" contract_name bm_name in
  get_dipdup_handlers_dir ~dir // filename

let get_contracts ~dir =
  let local_config_file = get_local_config_name ~dir in
  let lc =
    Factori_utils.deserialize_file ~silent:false ~filename:local_config_file
      ~enc:local_config_enc in
  lc.contracts

let get_contract_signature ~dir ~contract_name =
  let filename =
    get_contract_signature_filename
      ~contract_name:(sanitized_of_str contract_name)
      ~dir in
  let signature =
    deserialize_file ~silent:false ~filename ~enc:contract_signature_enc in
  signature

let get_all_contract_signatures ~dir =
  let contracts = get_contracts ~dir in
  let contract_names = List.map (fun c -> c.contract_name) contracts in
  let signatures : (contract_signature * string) list =
    List.map
      (fun n -> (get_contract_signature ~dir ~contract_name:n, n))
      contract_names in
  signatures

let find_same_signature_opt ~contract_name ~dir (signature : contract_signature)
    =
  let signatures =
    List.filter (fun (_, name) -> name <> contract_name.original)
    @@ get_all_contract_signatures ~dir in
  List.assoc_opt signature signatures

let get_contract_opt name =
  let cl = get_contracts ~dir:(get_dir ()) in
  List.find_opt (fun x -> x.contract_name = show_sanitized_name name) cl

let get_contract name =
  let ctrt_opt = get_contract_opt name in
  match ctrt_opt with
  | None ->
    raise
      (UnknownContract
         (show_sanitized_name name, get_local_config_name ~dir:(get_dir ())))
  | Some ctrt -> ctrt

let get_contract_original_kt1 name =
  match get_contract_opt name with
  | None -> None
  | Some c -> c.original_kt1

let contract_exists name =
  match get_contract_opt name with
  | None -> false
  | Some _c -> true

let get_format_of_contract name =
  match get_contract_opt name with
  | None -> None
  | Some c -> Some c.original_format

let parameter_of_option =
  let open Factori_options in
  function
  | Crawler Crawlori -> crawlori
  | Crawler Dipdup -> dipdup
  | Language OCaml -> ocaml
  | Language Typescript -> typescript
  | Language Csharp -> csharp
  | Language Python -> python
  | Webinterface -> web_mode

(** Builds the list of options for the current parameters in an
   importation. It will read from the `bool ref` values in the
   Factori_options module. *)
let options () =
  let options =
    List.map
      (fun foption -> (!(parameter_of_option foption), foption))
      all_options in
  let enabled = List.filter (fun (o, _) -> o) options in
  snd @@ List.split enabled

(** Gets the current time as a timestamp string for use in the
   contracts.json file *)
let current_time () =
  let current_time = Unix.localtime @@ Unix.time () in
  Format.asprintf "%d/%d/%d %d:%d:%d" current_time.tm_mday
    (current_time.tm_mon + 1)
    (1900 + current_time.tm_year)
    current_time.tm_hour current_time.tm_min current_time.tm_sec

(** Remove a contract from the contracts.json file  *)
let remove_contract_from_config_file ~dir ~name =
  if not @@ contract_exists name then
    failwith "No such contract in config file%!\n"
  else
    let local_config_file = get_local_config_name ~dir in
    let lc =
      deserialize_file ~silent:false ~filename:local_config_file
        ~enc:local_config_enc in
    let new_lc =
      {
        bad_nodes = lc.bad_nodes;
        contracts =
          List.filter
            (fun c -> c.contract_name <> show_sanitized_name name)
            lc.contracts;
      } in
    serialize_file ~filename:local_config_file ~enc:local_config_enc new_lc

(** Checks whether, for a given option (of type factori_option above),
   there exists at least one contract which has that option *)
let exists_option ~dir option =
  let local_config_file = get_local_config_name ~dir in
  let lc =
    try
      deserialize_file ~silent:true ~filename:local_config_file
        ~enc:local_config_enc
    with _ -> { bad_nodes = []; contracts = [] } in
  List.exists
    (fun c -> List.exists (fun o -> o = option) c.options)
    lc.contracts

let add_bad_node ~dir node =
  let local_config_file = get_local_config_name ~dir in
  let lc =
    deserialize_file ~silent:true ~filename:local_config_file
      ~enc:local_config_enc in
  let new_bad_nodes =
    match List.assoc_opt node lc.bad_nodes with
    | None -> (node, 1) :: lc.bad_nodes
    | Some k -> (node, k + 1) :: List.remove_assoc node lc.bad_nodes in
  let new_lc = { lc with bad_nodes = new_bad_nodes } in
  serialize_file ~filename:local_config_file ~enc:local_config_enc new_lc

let get_bad_nodes ~dir =
  let local_config_file = get_local_config_name ~dir in
  let lc =
    deserialize_file ~silent:true ~filename:local_config_file
      ~enc:local_config_enc in
  lc.bad_nodes

(** Sort nodes such that the ones with red flags are put last  *)
let sort_nodes ~dir node_list =
  let bad_nodes = get_bad_nodes ~dir in
  let get_flag_count node =
    match List.assoc_opt node bad_nodes with
    | None -> 0
    | Some k -> k in
  let node_list = List.map (fun x -> (x, get_flag_count x)) node_list in
  let node_list =
    List.sort
      (fun (_, flag_count1) (_, flag_count2) ->
        Stdlib.compare flag_count1 flag_count2)
      node_list in
  List.map fst node_list

(** Adds a contract to the contracts.json file  *)
let add_contract ?(original_kt1 = None) ~dir ~format ~name () =
  let local_config_file = get_local_config_name ~dir in
  let lc =
    deserialize_file ~silent:false ~filename:local_config_file
      ~enc:local_config_enc in
  if contract_exists name then begin
    Console.info "Contract already exists@." ;
    let c = get_contract name in
    if c.original_format = format then begin
      Console.info "Contract has the same format@." ;
      if !Factori_options.overwrite then begin
        Console.info "[--force is enabled] Overwriting the contract.@." ;
        let merged_options =
          List.sort_uniq Stdlib.compare (c.options @ options ()) in
        let new_contract = { c with options = merged_options; original_kt1 } in
        remove_contract_from_config_file ~dir ~name ;
        let lc =
          deserialize_file ~silent:false ~filename:local_config_file
            ~enc:local_config_enc in
        let new_lc =
          { bad_nodes = lc.bad_nodes; contracts = new_contract :: lc.contracts }
        in
        serialize_file ~filename:local_config_file ~enc:local_config_enc new_lc
      end else begin
        Console.error
          "Enable the option --force if you want to force the import of the \
           same contract@." ;
        exit 1
      end
    end else begin
      Console.error
        "A contract with the same name (%s) but a different original format \
         (%s <> %s) already exists in your project@."
        (show_sanitized_name name)
        (show_contract_format c.original_format)
        (show_contract_format format) ;
      exit 1
    end
  end else
    let contract =
      {
        contract_name = show_sanitized_name name;
        original_format = format;
        original_kt1;
        import_date = current_time ();
        options = options ();
      } in
    let lc =
      { bad_nodes = lc.bad_nodes; contracts = contract :: lc.contracts } in
    serialize_file ~filename:local_config_file ~enc:local_config_enc lc
