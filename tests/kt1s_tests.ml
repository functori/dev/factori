open Factori_network
open Command
open Kt1s_tests_types

module Results : sig
  type 'step t

  val empty : 'step t

  val return : ?step:'step -> Result.t Lazy.t -> 'step t

  val merge : 'step t -> 'step t -> 'step t

  val bind : 'step t -> (unit -> 'step t) -> 'step t

  val safe : ?step:'step -> (unit -> unit) -> 'step t

  val get : 'step t -> Result.t * ('step * (Result.t * Time.t)) list

  module Syntax : sig
    val return : ?step:'step -> Result.t Lazy.t -> 'step t

    val ( let* ) : 'step t -> (unit -> 'step t) -> 'step t
  end
end = struct
  type 'step t = ('step option * (Result.t * Time.t) Lazy.t) list

  let empty = []

  let return ?step res =
    let res () =
      let t = Unix.time () in
      let res = Lazy.force res in
      let time = Unix.time () -. t in
      (res, time) in
    [(step, Lazy.from_fun res)]

  let merge = ( @ )

  let bind l f = merge l (f ())

  let safe ?step f =
    let safe () =
      try
        f () ;
        Result.Succeeded
      with _ -> Result.Failed 1 in
    return ?step @@ Lazy.from_fun safe

  let get l =
    let open Result in
    let final, acc =
      List.fold_left
        (fun (prev_res, acc) (step, res) ->
          let next_res, res =
            match prev_res with
            | Failed _ | Passed -> (prev_res, (Passed, Time.none))
            | Succeeded ->
              let res_info = Lazy.force res in
              (fst res_info, res_info) in
          match step with
          | None -> (next_res, acc)
          | Some step -> (next_res, (step, res) :: acc))
        (Succeeded, []) l in
    (final, List.rev acc)

  module Syntax = struct
    let return = return

    let ( let* ) = bind
  end
end

let safe_print str = Results.safe @@ fun () -> Format.printf "%s@." str

let simple_run ?timeout ?log cmd =
  Results.safe @@ fun () -> ignore @@ Cmd.run ?timeout ?log cmd

let get_results ?timeout ?log ?step cmd =
  let open Results in
  return ?step @@ lazy (Result.make @@ Cmd.run ~pre:true ?timeout ?log cmd)

let ( // ) = Filename.concat

let root = try Sys.getenv "ROOT" with Not_found -> Sys.getcwd ()

let global_test_dir = root // "test"

let mk_log kt1 log_dir log_file =
  log_dir // Format.sprintf "%s_%s.html" kt1 log_file

module type Test = sig
  val name : string

  val prelude : Step.t Results.t

  val test : log_dir:string -> network:network -> kt1:string -> Step.t Results.t

  val finalize : Step.t Results.t
end

module type InterfaceLanguageTest = sig
  val name : string

  val option : Factori_config.interface_language

  (* Builds dependencies which are independent of kt1 *)
  val make_deps : dir:string -> Step.t Results.t

  val test_content : network:network -> kt1_name:string -> kt1:string -> string

  val test :
    dir:string ->
    log:string ->
    network:network ->
    kt1_name:string ->
    kt1:string ->
    unit ->
    Step.t Results.t
end

let clean_global_test_dir = simple_run @@ hard_clean global_test_dir

let interface_language_test_to_test (module Test : InterfaceLanguageTest) =
  let module Test = struct
    open Test

    let name = name

    let bkp_dir = "/tmp" // "test.bkp" // name

    let prelude =
      let open Results.Syntax in
      let* () = clean_global_test_dir in
      let* () = safe_print @@ Format.sprintf "Import empty %s project@." name in
      let* () = simple_run @@ rm [bkp_dir] in
      let* () =
        get_results
        @@ Import.empty_project ~options:[option] ~root ~dir:bkp_dir () in
      let* () = safe_print @@ Format.sprintf "Make %s deps@." name in
      make_deps ~dir:bkp_dir

    let dir = global_test_dir // name

    let test ~log_dir ~network ~kt1 =
      let open Results.Syntax in
      let kt1_name = "contract_" ^ String.lowercase_ascii kt1 in
      let* () = prelude in
      let test_content =
        Format.sprintf "<pre>%s</pre>" @@ test_content ~network ~kt1_name ~kt1
      in
      Factori_utils.write_file
        ~filename:(mk_log kt1 log_dir (name ^ "_test"))
        test_content ;
      let log = mk_log kt1 log_dir name in
      let* () = simple_run @@ replace dir bkp_dir in
      let* () =
        get_results ~timeout:150 ~log ~step:Step.Import
        @@ Import.import_kt1 kt1 ~options:[option] ~name:kt1_name ~root ~dir
             ~network in
      test ~dir ~log ~network ~kt1_name ~kt1 ()

    let finalize =
      let open Results.Syntax in
      let* () = simple_run @@ rm [bkp_dir] in
      clean_global_test_dir
  end in
  (module Test : Test)

module OCamlTest : InterfaceLanguageTest = struct
  open OCamlCmd

  let name = "ocaml"

  let option = Factori_config.OCaml

  let make_deps ~dir = get_results @@ make_deps ~dir ()

  let test_content = Tests.OCaml.get_storage

  let test ~dir ~log ~network ~kt1_name ~kt1 () =
    let open Results.Syntax in
    let* () =
      get_results ~timeout:150 ~log ~step:Step.Compilation @@ compile ~dir ()
    in
    let* () =
      Results.safe @@ fun () ->
      Factori_utils.write_file ~filename:(scenario ~dir)
        (test_content ~network ~kt1_name ~kt1) in
    get_results ~timeout:40 ~log ~step:Step.Test @@ run ~dir ()
end

module TypescriptTest : InterfaceLanguageTest = struct
  open TypescriptCmd

  let name = "typescript"

  let option = Factori_config.Typescript

  let make_deps ~dir = get_results @@ make_deps ~dir ()

  let test_content = Tests.Typescript.get_storage

  let test ~dir ~log ~network ~kt1_name ~kt1 () =
    let open Results.Syntax in
    let test = "test" in
    let* () =
      get_results ~timeout:150 ~log ~step:Step.Compilation @@ compile ~dir ()
    in
    let* () =
      Results.safe @@ fun () ->
      Factori_utils.write_file ~filename:(scenario ~dir test)
        (test_content ~network ~kt1_name ~kt1) in
    get_results ~timeout:40 ~log ~step:Step.Test
    @@ Cmd.merge [compile ~dir (); run ~dir test]
end

module PythonTest : InterfaceLanguageTest = struct
  open PythonCmd

  let name = "python"

  let option = Factori_config.Python

  let make_deps ~dir:_ = get_results @@ make_deps ()

  let test_content ~network:_ = Tests.Python.get_storage

  let test ~dir ~log ~network ~kt1_name ~kt1 () =
    let open Results.Syntax in
    let test = "test" in
    let* () =
      Results.safe @@ fun () ->
      Factori_utils.write_file ~filename:(scenario ~dir test)
        (test_content ~network ~kt1_name ~kt1) in
    get_results ~timeout:40 ~log ~step:Step.Test @@ run ~dir test
end

module CsharpTest : InterfaceLanguageTest = struct
  open CsharpCmd

  let name = "csharp"

  let option = Factori_config.Csharp

  let make_deps ~dir:_ = Results.empty

  let test_content = Tests.Csharp.get_storage

  let test ~dir ~log ~network ~kt1_name ~kt1 () =
    let open Results.Syntax in
    let test = "Program" in
    let* () =
      get_results ~timeout:150 ~log ~step:Step.Compilation
      @@ Cmd.merge [init ~dir (); build ~dir ()] in
    let* () =
      Results.safe @@ fun () ->
      Factori_utils.write_file ~filename:(scenario ~dir test)
        (test_content ~network ~kt1_name ~kt1) in
    get_results ~timeout:40 ~log ~step:Step.Test @@ run ~dir ()
end

module AnalyzeTest : Test = struct
  let name = "analyze"

  let prelude = Results.empty

  let test ~log_dir ~network ~kt1 =
    let log = mk_log kt1 log_dir name in
    get_results ~timeout:20 ~log ~step:Step.Test
    @@ Analyze.analyze_kt1 kt1 ~root ~network

  let finalize = Results.empty
end

module LintTest : Test = struct
  let name = "lint"

  let prelude = Results.empty

  let test ~log_dir ~network ~kt1 =
    let log = mk_log kt1 log_dir name in
    get_results ~timeout:40 ~log ~step:Step.Test
    @@ Lint.lint_kt1 kt1 ~root ~network

  let finalize = Results.empty
end

let tests =
  let open TestMap in
  let open Test in
  empty
  |> add OCaml (interface_language_test_to_test (module OCamlTest))
  |> add Typescript (interface_language_test_to_test (module TypescriptTest))
  |> add Python (interface_language_test_to_test (module PythonTest))
  |> add Csharp (interface_language_test_to_test (module CsharpTest))
  |> add Analyze (module AnalyzeTest : Test)
  |> add Lint (module LintTest : Test)

let output_results ~nb_runs ~synopsis_filename ~results_filename =
  let outputs : Outputs.t ref = ref Outputs.empty in
  let synopsis : Synopsis.t ref = ref (Synopsis.empty ~nb_runs) in
  Factori_utils.write_file ~filename:results_filename
    (Outputs.to_string !outputs) ;
  Factori_utils.write_file ~filename:synopsis_filename
    (Synopsis.to_string !synopsis) ;
  fun ~kt1_entry ~results ->
    outputs := Outputs.add kt1_entry results !outputs ;
    Factori_utils.write_file ~filename:results_filename
      (Outputs.to_string !outputs) ;
    synopsis := Synopsis.add results !synopsis ;
    Factori_utils.write_file ~filename:synopsis_filename
      (Synopsis.to_string !synopsis)

let run_test ~log_dir ~network ~kt1 (module Test : Test) =
  let final_result, steps = Results.get @@ Test.test ~log_dir ~network ~kt1 in
  let steps =
    List.map (fun (kind, (result, time)) -> { kind; time; result }) steps in
  let results = { final_result; steps } in
  Format.printf "%s: %a\n@."
    (String.capitalize_ascii Test.name)
    Result.pp final_result ;
  results

exception MaxRunReached

let run_tests_on_all_kt1s ?max_run_threshold ~handle_results ~log_dir ~tests
    kt1s =
  let assert_max_run_threshold_not_reached i =
    Option.iter
      (fun threshold -> if threshold <= i then raise MaxRunReached)
      max_run_threshold in
  let _set_max_threads = Lwt_preemptive.set_bounds (0, 4) in
  let run_all_projects i Kt1Entry.({ kt1; network; _ } as kt1_entry) =
    let network = Factori_network.network_of_string network in
    assert_max_run_threshold_not_reached i ;
    Format.printf "%s:\n@." kt1 ;
    let results =
      TestMap.Lwt.map_p
        (Lwt_preemptive.detach (run_test ~log_dir ~network ~kt1))
        tests in
    let results = Lwt_main.run results in
    handle_results ~kt1_entry ~results in
  List.iteri run_all_projects kt1s

let read_kt1s filename =
  let ic = open_in filename in
  let buf = really_input_string ic (in_channel_length ic) in
  close_in ic ;
  EzEncoding.destruct (Json_encoding.list Kt1Entry.enc) buf
  |> List.sort Kt1Entry.compare_by_occurrence

let () =
  match Sys.argv with
  | [|
   _;
   mainnet_kt1_json;
   synopsis_filename;
   results_filename;
   log_dir;
   max_run_threshold;
  |] ->
    let max_run_threshold =
      try Some (int_of_string max_run_threshold) with _ -> None in
    let kt1s = read_kt1s mainnet_kt1_json in
    let nb_runs =
      match max_run_threshold with
      | None -> List.length kt1s
      | Some threshold -> threshold in
    let handle_results =
      output_results ~nb_runs ~synopsis_filename ~results_filename in
    Format.printf
      "Prelude: creating and saving default compilation environment@." ;
    TestMap.iter
      (fun _ (module Test : Test) -> ignore @@ Results.get Test.prelude)
      tests ;
    Format.printf "End of prelude\n@." ;
    let () =
      try
        run_tests_on_all_kt1s ?max_run_threshold ~handle_results ~log_dir ~tests
          kt1s
      with MaxRunReached ->
        Format.printf "MaxRunReached : %s@."
        @@ Option.fold ~none:"none" ~some:Int.to_string max_run_threshold in
    TestMap.iter
      (fun _ (module Test : Test) -> ignore @@ Results.get Test.finalize)
      tests
  | _ ->
    Format.eprintf "Wrong argument@." ;
    exit 1
