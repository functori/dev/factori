(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains several exception definitions and functions to
handle them for the Factori program. The exceptions include
"Scenario_key_already_used", "Scenario_key_not_found", "OpenForkRemaining of
int". The file also includes functions such as "hookable_handle_exception",
"handle_exception", and "add_exception_handler" that can be used to handle
exceptions. Warning: this short summary was automatically generated and could be
incomplete or misleading. If you feel this is the case, please open an issue. *)

open Factori_errors

exception Scenario_key_already_used

exception Scenario_key_not_found

(* TODO: add a function to handle all these new errors *)
exception OpenForkRemaining of int

let _ =
  add_exception_handler (function
    | Scenario_key_already_used ->
      Format.eprintf "Error: Scenario key already used"
    | Scenario_key_not_found -> Format.eprintf "Error: Scenario key not found"
    | OpenForkRemaining f ->
      Format.eprintf "You forgot to close an open fork (#%d) in your scenario" f
    | e -> raise e)
