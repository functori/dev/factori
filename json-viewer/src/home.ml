open Common

module Run = struct
  include Run

  {%%template|
  <tr>
    <td>
      <v-version :version="run.synopsis.version" style="color: inherit" />
    </td>
    <td>
      <a :href="run_link"> {{ run_date }} </a>
    </td>
    <td>
      <v-progress-bar
        :value="run.synopsis.nb_processed"
        :max="run.synopsis.nb_runs"
        />
    </td>
    <td v-for="test in tests">
      <v-progress-bar
        :value="test_success(test)"
        :max="run.synopsis.nb_processed"
        success
        />
    </td>
  </tr>
  |}

  let%prop run : t = { req }

  let%data tests : Test.t list = Test.all

  let%comp run_link this : string =
    let { index; _ } = of_jsoo this##.run in
    Page.(to_string @@ Run index)

  and run_date this : string =
    Date.(to_string @@ of_jsoo this##.run##.index##.date)

  let%meth test_success this test : int =
    number_of_test_success ~test:(Test.of_jsoo test) (of_jsoo this##.run)

  [%%comp { name = "v-run"; conv; components = [V_version; V_progress_bar] }]
end

{%%template|
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand mr-2" href="https://functori.com">
    <img src="https://www.functori.com/assets/img/brand/favicon.png"
	 alt="Explorus"
	 class="mr-2"
	 style="height: 40px" >
    Functori
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/home">All Runs</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="https://functori.com/docs/factori">Factori Docs</a>
      </li>
    </ul>
    <form class="form-inline my-4 my-lg-0">
      <input
        type="text"
        class="form-control"
        placeholder="Search..."
        v-model="searching" />
    </form>

  </div>
</nav>

<div class="mx-4">
  <h4 class="ml-5 mb-5 mt-4"> Factori's Historical Runs </h4>
  <table class="table table-striped table-sm" style="table-layout: fixed;">
    <thead class="thead-dark" style="opacity: 0.9">
      <tr>
	<th>Version</th>
	<th style="width: 7%">Date</th>
	<th>Smart Contracts Processed</th>
	<th v-for="test in tests"> {{ pretty_test(test) }}</th>
      </tr>
    </thead>
    <tbody>
      <v-run v-for="run in historical_runs" :run="run" />
    </tbody>
  </table>
</div>
|}

let%prop indexes : Run.t list = { req }

let%data historical_runs : Run.t list = []

and tests : Test.t list = Test.all

let%meth pretty_test _this test = Test.(pretty @@ of_jsoo test)

let sort_indexes this =
  this##.historical_runs_ :=
    this##.indexes
    |> Ezjs_min.to_listf Run.of_jsoo
    |> List.sort Run.compare
    |> Ezjs_min.of_listf Run.to_jsoo

[%%beforeMount fun this -> sort_indexes this]

[%%comp { name = "v-home"; conv; components = [Run] }]
