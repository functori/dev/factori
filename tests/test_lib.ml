module Map = struct
  include Map

  module type S = sig
    include S

    module Lwt : sig
      val iter_s : (key -> 'a -> unit Lwt.t) -> 'a t -> unit Lwt.t

      val iter_p : (key -> 'a -> unit Lwt.t) -> 'a t -> unit Lwt.t

      val fold_s : (key -> 'a -> 'b -> 'b Lwt.t) -> 'a t -> 'b -> 'b Lwt.t

      val for_all_s : (key -> 'a -> bool Lwt.t) -> 'a t -> bool Lwt.t

      val for_all_p : (key -> 'a -> bool Lwt.t) -> 'a t -> bool Lwt.t

      val exists_s : (key -> 'a -> bool Lwt.t) -> 'a t -> bool Lwt.t

      val exists_p : (key -> 'a -> bool Lwt.t) -> 'a t -> bool Lwt.t

      val filter_s : (key -> 'a -> bool Lwt.t) -> 'a t -> 'a t Lwt.t

      val filter_p : (key -> 'a -> bool Lwt.t) -> 'a t -> 'a t Lwt.t

      val filter_map_s : (key -> 'a -> 'b option Lwt.t) -> 'a t -> 'b t Lwt.t

      val filter_map_p : (key -> 'a -> 'b option Lwt.t) -> 'a t -> 'b t Lwt.t

      val map_s : ('a -> 'b Lwt.t) -> 'a t -> 'b t Lwt.t

      val map_p : ('a -> 'b Lwt.t) -> 'a t -> 'b t Lwt.t

      val mapi_s : (key -> 'a -> 'b Lwt.t) -> 'a t -> 'b t Lwt.t

      val mapi_p : (key -> 'a -> 'b Lwt.t) -> 'a t -> 'b t Lwt.t

      val find_opt_s : (key -> 'a -> bool Lwt.t) -> 'a t -> 'a option Lwt.t
    end
  end

  module Make (Ord : OrderedType) : S with type key = Ord.t = struct
    include Make (Ord)

    module Lwt = struct
      let fold_s f m acc =
        fold (fun k v acc -> Lwt.bind acc (f k v)) m (Lwt.return acc)

      let filter_map_s f m =
        let open Lwt.Syntax in
        fold_s
          (fun k v m ->
            let* v = f k v in
            match v with
            | None -> Lwt.return m
            | Some v -> Lwt.return (add k v m))
          m empty

      let filter_s f =
        let open Lwt.Syntax in
        filter_map_s @@ fun k v ->
        let* b = f k v in
        if b then
          Lwt.return_some v
        else
          Lwt.return_none

      let mapi_s f = filter_map_s @@ fun k v -> Lwt.bind (f k v) Lwt.return_some

      let map_s f = mapi_s @@ fun _ -> f

      let iter_s f m = fold_s (fun k v () -> f k v) m ()

      let for_all_s f m =
        fold_s
          (fun k v -> function
            | true -> f k v
            | false -> Lwt.return_false)
          m true

      let exists_s f m =
        fold_s
          (fun k v -> function
            | false -> f k v
            | true -> Lwt.return_true)
          m false

      let find_map_opt_s f m =
        fold_s
          (fun k v -> function
            | None -> f k v
            | Some v -> Lwt.return_some v)
          m None

      let find_opt_s f =
        let open Lwt.Syntax in
        find_map_opt_s @@ fun k v ->
        let* b = f k v in
        if b then
          Lwt.return_some v
        else
          Lwt.return_none

      let mapi_p f m =
        let open Lwt.Syntax in
        let+ bindings =
          Lwt_list.map_p
            (fun (k, v) ->
              let+ v = f k v in
              (k, v))
            (bindings m) in
        List.fold_left (fun m (k, v) -> add k v m) empty bindings

      let map_p f = mapi_p @@ fun _ -> f

      let iter_p f m = Lwt.map (iter (fun _ () -> ())) @@ mapi_p f m

      let for_all_p f m = Lwt.map (for_all (fun _ b -> b)) @@ mapi_p f m

      let exists_p f m = Lwt.map (exists (fun _ b -> b)) @@ mapi_p f m

      let filter_map_p f m = Lwt.map (filter_map (fun _ b -> b)) @@ mapi_p f m

      let filter_p f =
        let open Lwt.Syntax in
        filter_map_p @@ fun k v ->
        let* b = f k v in
        if b then
          Lwt.return_some v
        else
          Lwt.return_none
    end
  end
end
