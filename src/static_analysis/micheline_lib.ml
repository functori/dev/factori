(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tzfunc.Proto
open Factori_errors

let prim_macro =
  List.map (fun (s, p) -> ((p : primitive :> primitive_or_macro), s)) primitives

let prim_name ~prim = List.assoc prim prim_macro

let new_item_cpt cpt miche =
  match miche with
  | Mprim { prim; _ } ->
    let s = List.assoc prim prim_macro in
    Ok (cpt + 1, (Format.sprintf "%s_%d" s cpt, miche))
  | _ -> Error (GenericError ("Michelson var", "Not a micheline prim"))

let comparison prim =
  match prim with
  | `EQ | `GT | `GE | `LE | `LT | `NEQ -> true
  | _ -> false

let inverse_prim = function
  | `EQ -> Some `NEQ
  | `NEQ -> Some `EQ
  | `GT -> Some `LE
  | `LE -> Some `GT
  | `LT -> Some `GE
  | `GE -> Some `LT
  | _ -> None

let pretty_prim = function
  | `EQ -> "="
  | `GT -> ">"
  | `GE -> ">="
  | `LE -> "<="
  | `LT -> "<"
  | `MUL -> "*"
  | `ADD -> "+"
  | `SUB -> "-"
  | `SUB_MUTEZ -> "-"
  | `EDIV -> "/"
  | `NEQ -> "<>"
  | `AND -> "/\\"
  | `OR -> "\\/"
  | `CONTRACT -> "GET_CONTRACT"
  | prim -> List.assoc prim prim_macro

let associative_prim = function
  | `EQ
  | `GT
  | `GE
  | `LE
  | `LT
  | `MUL
  | `ADD
  | `SUB
  | `SUB_MUTEZ
  | `EDIV
  | `NEQ
  | `AND
  | `OR -> true
  | _ -> false

let rec print_micheline (ppf : Format.formatter) (m : micheline) : unit =
  let print_arg ppf = function
    | [] -> ()
    | arg_l ->
      let pp_l =
        Format.pp_print_list ~pp_sep:Format.pp_print_cut (fun ppf arg ->
            Format.fprintf ppf "%a" print_micheline arg) in
      Format.fprintf ppf " @[<v 0>%a@]" pp_l arg_l in
  match m with
  | Mprim { prim = n; args = []; annots = [] } ->
    Format.fprintf ppf "%s" (List.assoc n prim_macro)
  | Mprim { prim = n; args; annots } ->
    let s = List.assoc n prim_macro in
    Format.fprintf ppf "(%s%a%a)" s
      (Format.pp_print_list
         ~pp_sep:(fun ppf () -> Format.fprintf ppf " ")
         (fun ppf -> Format.fprintf ppf "%s"))
      annots print_arg args
  | Mstring s -> Format.fprintf ppf "\"%s\"" s
  | Mint i -> Format.fprintf ppf "%s" (Z.to_string i)
  | Mbytes b ->
    let raw = Crypto.hex_to_raw b in
    let str = Crypto.coerce raw in
    Format.fprintf ppf "%s" str
  | Mseq miche_l ->
    Format.fprintf ppf "{@,%a@,}"
      (Format.pp_print_list (fun ppf elt ->
           Format.fprintf ppf "%a" print_micheline elt))
      miche_l

let rec print_micheline_single_line (ppf : Format.formatter) (m : micheline) :
    unit =
  let sub_print ppf m =
    let print_arg ppf = function
      | [] -> ()
      | arg_l ->
        Format.pp_print_list ~pp_sep:Format.pp_print_space
          (fun ppf arg ->
            Format.fprintf ppf "%a" print_micheline_single_line arg)
          ppf arg_l in
    let print_annot ppf = function
      | [] -> Format.pp_print_space ppf ()
      | annot_l ->
        Format.pp_print_list ~pp_sep:Format.pp_print_space
          (fun ppf -> Format.fprintf ppf "%s")
          ppf annot_l in
    match m with
    | Mprim { prim = n; args = []; annots = [] } ->
      Format.fprintf ppf "%s" (List.assoc n prim_macro)
    | Mprim { prim = n; args; annots } ->
      let s = List.assoc n prim_macro in
      Format.fprintf ppf "(%s%a%a)" s print_annot annots print_arg args
    | Mstring s -> Format.fprintf ppf "%S" s
    | Mint i -> Format.fprintf ppf "%s" (Z.to_string i)
    | Mbytes b ->
      let raw = Crypto.hex_to_raw b in
      let str = Crypto.coerce raw in
      Format.fprintf ppf "%s" str
    | Mseq miche_l ->
      Format.fprintf ppf "{@,%a@,}"
        (Format.pp_print_list (fun ppf elt ->
             Format.fprintf ppf "%a" print_micheline_single_line elt))
        miche_l in
  Format.fprintf ppf "@[<h>%a@]" sub_print m

let print_micheline ?(simple = true) =
  if simple then
    print_micheline_single_line
  else
    print_micheline

let string_of_micheline ?(simple = true) (m : micheline) : string =
  Format.asprintf "%a" (print_micheline ~simple) m
