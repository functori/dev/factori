from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

Unpause = factori_types.unit

def unpause_generator() -> Unpause:
	return factori_types.unit_generator ()

def unpause_encode (arg: Unpause):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in unpause_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def unpause_decode (arg) -> Unpause:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in unpause_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_unpause():
	temp=unpause_generator()
	temp_encoded=unpause_encode(temp)
	try:
		print('++++ unpause: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of unpause: " + str(temp))
	temp1=unpause_decode(temp_encoded)
	if temp1 == temp:
		print('unpause check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('unpause check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_unpause()
#except Exception as e:
#	print('Error in assert_identity_unpause' + str(e))




def callUnpause(kt1 : str, _from : blockchain.identity, param : Unpause, networkName = "ghostnet", debug=False):
	input = unpause_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "unpause", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Value = factori_types.nat

def value_generator() -> Value:
	return factori_types.nat_generator ()

def value_encode (arg: Value):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in value_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def value_decode (arg) -> Value:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in value_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_value():
	temp=value_generator()
	temp_encoded=value_encode(temp)
	try:
		print('++++ value: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of value: " + str(temp))
	temp1=value_decode(temp_encoded)
	if temp1 == temp:
		print('value check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('value check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_value()
#except Exception as e:
#	print('Error in assert_identity_value' + str(e))




Constructor_to = factori_types.address

def _to_generator() -> Constructor_to:
	return factori_types.address_generator ()

def _to_encode (arg: Constructor_to):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in _to_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _to_decode (arg) -> Constructor_to:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in _to_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__to():
	temp=_to_generator()
	temp_encoded=_to_encode(temp)
	try:
		print('++++ _to: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _to: " + str(temp))
	temp1=_to_decode(temp_encoded)
	if temp1 == temp:
		print('_to check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_to check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__to()
#except Exception as e:
#	print('Error in assert_identity__to' + str(e))




class Transfer(TypedDict):
	value: Value
	_to: Constructor_to
	_from: Constructor_to

def transfer_generator() -> Transfer:
	return {'value' : value_generator (),'_to' : _to_generator (),'_from' : _to_generator ()}

def transfer_encode(arg : Transfer):
	try:
		return PairType.from_comb([(_to_encode(arg['_from'])), PairType.from_comb([(_to_encode(arg['_to'])), (value_encode(arg['value']))])])
	except Exception as e:
		print('Error in transfer_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def transfer_decode(arg) -> Transfer:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,(factori_types.tuple2_decode (_to_decode,value_decode))))(arg)
		return {'_from' : (before_projection[0]),
'_to' : (before_projection[1][0]),
'value' : (before_projection[1][1])}


	except Exception as e:
		print('Error in transfer_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_transfer():
	temp=transfer_generator()
	temp_encoded=transfer_encode(temp)
	try:
		print('++++ transfer: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of transfer: " + str(temp))
	temp1=transfer_decode(temp_encoded)
	if temp1 == temp:
		print('transfer check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('transfer check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_transfer()
#except Exception as e:
#	print('Error in assert_identity_transfer' + str(e))




def callTransfer(kt1 : str, _from : blockchain.identity, param : Transfer, networkName = "ghostnet", debug=False):
	input = transfer_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "transfer", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class SetTokenMetadata(TypedDict):
	token_id: Value
	token_info: factori_types.Map

def setTokenMetadata_generator() -> SetTokenMetadata:
	return {'token_id' : value_generator (),'token_info' : (factori_types.map_generator(factori_types.string_generator,factori_types.bytes_generator)) ()}

def setTokenMetadata_encode(arg : SetTokenMetadata):
	try:
		return PairType.from_comb([(value_encode(arg['token_id'])), (factori_types.map_encode(factori_types.string_encode,factori_types.bytes_encode)(arg['token_info']))])
	except Exception as e:
		print('Error in setTokenMetadata_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setTokenMetadata_decode(arg) -> SetTokenMetadata:
	try:
		before_projection=(factori_types.tuple2_decode (value_decode,factori_types.map_decode(factori_types.string_decode,factori_types.bytes_decode)))(arg)
		return {'token_id' : (before_projection[0]),
'token_info' : (before_projection[1])}


	except Exception as e:
		print('Error in setTokenMetadata_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setTokenMetadata():
	temp=setTokenMetadata_generator()
	temp_encoded=setTokenMetadata_encode(temp)
	try:
		print('++++ setTokenMetadata: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setTokenMetadata: " + str(temp))
	temp1=setTokenMetadata_decode(temp_encoded)
	if temp1 == temp:
		print('setTokenMetadata check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setTokenMetadata check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setTokenMetadata()
#except Exception as e:
#	print('Error in assert_identity_setTokenMetadata' + str(e))




def callSetTokenMetadata(kt1 : str, _from : blockchain.identity, param : SetTokenMetadata, networkName = "ghostnet", debug=False):
	input = setTokenMetadata_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setTokenMetadata", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetStabilityFundContract = Constructor_to


def setStabilityFundContract_generator() -> SetStabilityFundContract:
	return _to_generator ()


def setStabilityFundContract_encode (arg: SetStabilityFundContract):
	try:
		return _to_encode(arg)
	except Exception as e:
		print('Error in setStabilityFundContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setStabilityFundContract_decode (arg) -> SetStabilityFundContract:
	try:
		return _to_decode(arg)
	except Exception as e:
		print('Error in setStabilityFundContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setStabilityFundContract():
	temp=setStabilityFundContract_generator()
	temp_encoded=setStabilityFundContract_encode(temp)
	try:
		print('++++ setStabilityFundContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setStabilityFundContract: " + str(temp))
	temp1=setStabilityFundContract_decode(temp_encoded)
	if temp1 == temp:
		print('setStabilityFundContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setStabilityFundContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setStabilityFundContract()
#except Exception as e:
#	print('Error in assert_identity_setStabilityFundContract' + str(e))




def callSetStabilityFundContract(kt1 : str, _from : blockchain.identity, param : SetStabilityFundContract, networkName = "ghostnet", debug=False):
	input = setStabilityFundContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setStabilityFundContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetPauseGuardianContract = Constructor_to


def setPauseGuardianContract_generator() -> SetPauseGuardianContract:
	return _to_generator ()


def setPauseGuardianContract_encode (arg: SetPauseGuardianContract):
	try:
		return _to_encode(arg)
	except Exception as e:
		print('Error in setPauseGuardianContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setPauseGuardianContract_decode (arg) -> SetPauseGuardianContract:
	try:
		return _to_decode(arg)
	except Exception as e:
		print('Error in setPauseGuardianContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setPauseGuardianContract():
	temp=setPauseGuardianContract_generator()
	temp_encoded=setPauseGuardianContract_encode(temp)
	try:
		print('++++ setPauseGuardianContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setPauseGuardianContract: " + str(temp))
	temp1=setPauseGuardianContract_decode(temp_encoded)
	if temp1 == temp:
		print('setPauseGuardianContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setPauseGuardianContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setPauseGuardianContract()
#except Exception as e:
#	print('Error in assert_identity_setPauseGuardianContract' + str(e))




def callSetPauseGuardianContract(kt1 : str, _from : blockchain.identity, param : SetPauseGuardianContract, networkName = "ghostnet", debug=False):
	input = setPauseGuardianContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setPauseGuardianContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetInterestRate = Value


def setInterestRate_generator() -> SetInterestRate:
	return value_generator ()


def setInterestRate_encode (arg: SetInterestRate):
	try:
		return value_encode(arg)
	except Exception as e:
		print('Error in setInterestRate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setInterestRate_decode (arg) -> SetInterestRate:
	try:
		return value_decode(arg)
	except Exception as e:
		print('Error in setInterestRate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setInterestRate():
	temp=setInterestRate_generator()
	temp_encoded=setInterestRate_encode(temp)
	try:
		print('++++ setInterestRate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setInterestRate: " + str(temp))
	temp1=setInterestRate_decode(temp_encoded)
	if temp1 == temp:
		print('setInterestRate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setInterestRate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setInterestRate()
#except Exception as e:
#	print('Error in assert_identity_setInterestRate' + str(e))




def callSetInterestRate(kt1 : str, _from : blockchain.identity, param : SetInterestRate, networkName = "ghostnet", debug=False):
	input = setInterestRate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setInterestRate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = Constructor_to


def setGovernorContract_generator() -> SetGovernorContract:
	return _to_generator ()


def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return _to_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return _to_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetContractMetadata = Tuple[str,bytes]

def setContractMetadata_generator() -> SetContractMetadata:
	return (factori_types.tuple2_generator(factori_types.string_generator,
factori_types.bytes_generator))()

def setContractMetadata_encode(arg : SetContractMetadata) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.string_encode,factori_types.bytes_encode)(arg) 
	except Exception as e:
		print('Error in setContractMetadata_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setContractMetadata_decode(arg) -> SetContractMetadata:
	try:
		return factori_types.tuple2_decode(factori_types.string_decode,factori_types.bytes_decode)(arg) 
	except Exception as e:
		print('Error in setContractMetadata_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setContractMetadata():
	temp=setContractMetadata_generator()
	temp_encoded=setContractMetadata_encode(temp)
	try:
		print('++++ setContractMetadata: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setContractMetadata: " + str(temp))
	temp1=setContractMetadata_decode(temp_encoded)
	if temp1 == temp:
		print('setContractMetadata check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setContractMetadata check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setContractMetadata()
#except Exception as e:
#	print('Error in assert_identity_setContractMetadata' + str(e))




def callSetContractMetadata(kt1 : str, _from : blockchain.identity, param : SetContractMetadata, networkName = "ghostnet", debug=False):
	input = setContractMetadata_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setContractMetadata", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

RescueXTZ = Constructor_to


def rescueXTZ_generator() -> RescueXTZ:
	return _to_generator ()


def rescueXTZ_encode (arg: RescueXTZ):
	try:
		return _to_encode(arg)
	except Exception as e:
		print('Error in rescueXTZ_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def rescueXTZ_decode (arg) -> RescueXTZ:
	try:
		return _to_decode(arg)
	except Exception as e:
		print('Error in rescueXTZ_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_rescueXTZ():
	temp=rescueXTZ_generator()
	temp_encoded=rescueXTZ_encode(temp)
	try:
		print('++++ rescueXTZ: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of rescueXTZ: " + str(temp))
	temp1=rescueXTZ_decode(temp_encoded)
	if temp1 == temp:
		print('rescueXTZ check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('rescueXTZ check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_rescueXTZ()
#except Exception as e:
#	print('Error in assert_identity_rescueXTZ' + str(e))




def callRescueXTZ(kt1 : str, _from : blockchain.identity, param : RescueXTZ, networkName = "ghostnet", debug=False):
	input = rescueXTZ_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "rescueXTZ", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Redeem_callback = Value


def redeem_callback_generator() -> Redeem_callback:
	return value_generator ()


def redeem_callback_encode (arg: Redeem_callback):
	try:
		return value_encode(arg)
	except Exception as e:
		print('Error in redeem_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def redeem_callback_decode (arg) -> Redeem_callback:
	try:
		return value_decode(arg)
	except Exception as e:
		print('Error in redeem_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_redeem_callback():
	temp=redeem_callback_generator()
	temp_encoded=redeem_callback_encode(temp)
	try:
		print('++++ redeem_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of redeem_callback: " + str(temp))
	temp1=redeem_callback_decode(temp_encoded)
	if temp1 == temp:
		print('redeem_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('redeem_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_redeem_callback()
#except Exception as e:
#	print('Error in assert_identity_redeem_callback' + str(e))




def callRedeem_callback(kt1 : str, _from : blockchain.identity, param : Redeem_callback, networkName = "ghostnet", debug=False):
	input = redeem_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "redeem_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Redeem = Value


def redeem_generator() -> Redeem:
	return value_generator ()


def redeem_encode (arg: Redeem):
	try:
		return value_encode(arg)
	except Exception as e:
		print('Error in redeem_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def redeem_decode (arg) -> Redeem:
	try:
		return value_decode(arg)
	except Exception as e:
		print('Error in redeem_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_redeem():
	temp=redeem_generator()
	temp_encoded=redeem_encode(temp)
	try:
		print('++++ redeem: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of redeem: " + str(temp))
	temp1=redeem_decode(temp_encoded)
	if temp1 == temp:
		print('redeem check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('redeem check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_redeem()
#except Exception as e:
#	print('Error in assert_identity_redeem' + str(e))




def callRedeem(kt1 : str, _from : blockchain.identity, param : Redeem, networkName = "ghostnet", debug=False):
	input = redeem_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "redeem", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Pause = Unpause


def pause_generator() -> Pause:
	return unpause_generator ()


def pause_encode (arg: Pause):
	try:
		return unpause_encode(arg)
	except Exception as e:
		print('Error in pause_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def pause_decode (arg) -> Pause:
	try:
		return unpause_decode(arg)
	except Exception as e:
		print('Error in pause_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_pause():
	temp=pause_generator()
	temp_encoded=pause_encode(temp)
	try:
		print('++++ pause: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of pause: " + str(temp))
	temp1=pause_decode(temp_encoded)
	if temp1 == temp:
		print('pause check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('pause check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_pause()
#except Exception as e:
#	print('Error in assert_identity_pause' + str(e))




def callPause(kt1 : str, _from : blockchain.identity, param : Pause, networkName = "ghostnet", debug=False):
	input = pause_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "pause", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Mint(TypedDict):
	address: Constructor_to
	value: Value

def mint_generator() -> Mint:
	return {'address' : _to_generator (),'value' : value_generator ()}

def mint_encode(arg : Mint):
	try:
		return PairType.from_comb([(_to_encode(arg['address'])), (value_encode(arg['value']))])
	except Exception as e:
		print('Error in mint_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def mint_decode(arg) -> Mint:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,value_decode))(arg)
		return {'address' : (before_projection[0]),
'value' : (before_projection[1])}


	except Exception as e:
		print('Error in mint_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_mint():
	temp=mint_generator()
	temp_encoded=mint_encode(temp)
	try:
		print('++++ mint: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of mint: " + str(temp))
	temp1=mint_decode(temp_encoded)
	if temp1 == temp:
		print('mint check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('mint check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_mint()
#except Exception as e:
#	print('Error in assert_identity_mint' + str(e))




def callMint(kt1 : str, _from : blockchain.identity, param : Mint, networkName = "ghostnet", debug=False):
	input = mint_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "mint", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetTotalSupply = Tuple[Unpause,factori_types.contract]

def getTotalSupply_generator() -> GetTotalSupply:
	return (factori_types.tuple2_generator(unpause_generator,
factori_types.contract_generator))()

def getTotalSupply_encode(arg : GetTotalSupply) -> PairType:
	try:
		return factori_types.tuple2_encode(unpause_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getTotalSupply_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getTotalSupply_decode(arg) -> GetTotalSupply:
	try:
		return factori_types.tuple2_decode(unpause_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getTotalSupply_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getTotalSupply():
	temp=getTotalSupply_generator()
	temp_encoded=getTotalSupply_encode(temp)
	try:
		print('++++ getTotalSupply: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getTotalSupply: " + str(temp))
	temp1=getTotalSupply_decode(temp_encoded)
	if temp1 == temp:
		print('getTotalSupply check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getTotalSupply check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getTotalSupply()
#except Exception as e:
#	print('Error in assert_identity_getTotalSupply' + str(e))




def callGetTotalSupply(kt1 : str, _from : blockchain.identity, param : GetTotalSupply, networkName = "ghostnet", debug=False):
	input = getTotalSupply_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getTotalSupply", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetBalance = Tuple[Constructor_to,factori_types.contract]

def getBalance_generator() -> GetBalance:
	return (factori_types.tuple2_generator(_to_generator,
factori_types.contract_generator))()

def getBalance_encode(arg : GetBalance) -> PairType:
	try:
		return factori_types.tuple2_encode(_to_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getBalance_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getBalance_decode(arg) -> GetBalance:
	try:
		return factori_types.tuple2_decode(_to_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getBalance_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getBalance():
	temp=getBalance_generator()
	temp_encoded=getBalance_encode(temp)
	try:
		print('++++ getBalance: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getBalance: " + str(temp))
	temp1=getBalance_decode(temp_encoded)
	if temp1 == temp:
		print('getBalance check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getBalance check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getBalance()
#except Exception as e:
#	print('Error in assert_identity_getBalance' + str(e))




def callGetBalance(kt1 : str, _from : blockchain.identity, param : GetBalance, networkName = "ghostnet", debug=False):
	input = getBalance_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getBalance", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Type4(TypedDict):
	owner: Constructor_to
	spender: Constructor_to

def type4_generator() -> Type4:
	return {'owner' : _to_generator (),'spender' : _to_generator ()}

def type4_encode(arg : Type4):
	try:
		return PairType.from_comb([(_to_encode(arg['owner'])), (_to_encode(arg['spender']))])
	except Exception as e:
		print('Error in type4_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def type4_decode(arg) -> Type4:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,_to_decode))(arg)
		return {'owner' : (before_projection[0]),
'spender' : (before_projection[1])}


	except Exception as e:
		print('Error in type4_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_type4():
	temp=type4_generator()
	temp_encoded=type4_encode(temp)
	try:
		print('++++ type4: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of type4: " + str(temp))
	temp1=type4_decode(temp_encoded)
	if temp1 == temp:
		print('type4 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('type4 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_type4()
#except Exception as e:
#	print('Error in assert_identity_type4' + str(e))




GetAllowance = Tuple[Type4,factori_types.contract]

def getAllowance_generator() -> GetAllowance:
	return (factori_types.tuple2_generator(type4_generator,
factori_types.contract_generator))()

def getAllowance_encode(arg : GetAllowance) -> PairType:
	try:
		return factori_types.tuple2_encode(type4_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getAllowance_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getAllowance_decode(arg) -> GetAllowance:
	try:
		return factori_types.tuple2_decode(type4_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getAllowance_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getAllowance():
	temp=getAllowance_generator()
	temp_encoded=getAllowance_encode(temp)
	try:
		print('++++ getAllowance: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getAllowance: " + str(temp))
	temp1=getAllowance_decode(temp_encoded)
	if temp1 == temp:
		print('getAllowance check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getAllowance check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getAllowance()
#except Exception as e:
#	print('Error in assert_identity_getAllowance' + str(e))




def callGetAllowance(kt1 : str, _from : blockchain.identity, param : GetAllowance, networkName = "ghostnet", debug=False):
	input = getAllowance_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getAllowance", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Deposit_callback = Value


def deposit_callback_generator() -> Deposit_callback:
	return value_generator ()


def deposit_callback_encode (arg: Deposit_callback):
	try:
		return value_encode(arg)
	except Exception as e:
		print('Error in deposit_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def deposit_callback_decode (arg) -> Deposit_callback:
	try:
		return value_decode(arg)
	except Exception as e:
		print('Error in deposit_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_deposit_callback():
	temp=deposit_callback_generator()
	temp_encoded=deposit_callback_encode(temp)
	try:
		print('++++ deposit_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of deposit_callback: " + str(temp))
	temp1=deposit_callback_decode(temp_encoded)
	if temp1 == temp:
		print('deposit_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('deposit_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_deposit_callback()
#except Exception as e:
#	print('Error in assert_identity_deposit_callback' + str(e))




def callDeposit_callback(kt1 : str, _from : blockchain.identity, param : Deposit_callback, networkName = "ghostnet", debug=False):
	input = deposit_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "deposit_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Deposit = Value


def deposit_generator() -> Deposit:
	return value_generator ()


def deposit_encode (arg: Deposit):
	try:
		return value_encode(arg)
	except Exception as e:
		print('Error in deposit_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def deposit_decode (arg) -> Deposit:
	try:
		return value_decode(arg)
	except Exception as e:
		print('Error in deposit_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_deposit():
	temp=deposit_generator()
	temp_encoded=deposit_encode(temp)
	try:
		print('++++ deposit: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of deposit: " + str(temp))
	temp1=deposit_decode(temp_encoded)
	if temp1 == temp:
		print('deposit check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('deposit check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_deposit()
#except Exception as e:
#	print('Error in assert_identity_deposit' + str(e))




def callDeposit(kt1 : str, _from : blockchain.identity, param : Deposit, networkName = "ghostnet", debug=False):
	input = deposit_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "deposit", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Burn(Mint):
	pass


def burn_generator() -> Burn:
	return mint_generator ()


def burn_encode (arg: Burn):
	try:
		return mint_encode(arg)
	except Exception as e:
		print('Error in burn_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def burn_decode (arg) -> Burn:
	try:
		return mint_decode(arg)
	except Exception as e:
		print('Error in burn_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_burn():
	temp=burn_generator()
	temp_encoded=burn_encode(temp)
	try:
		print('++++ burn: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of burn: " + str(temp))
	temp1=burn_decode(temp_encoded)
	if temp1 == temp:
		print('burn check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('burn check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_burn()
#except Exception as e:
#	print('Error in assert_identity_burn' + str(e))




def callBurn(kt1 : str, _from : blockchain.identity, param : Burn, networkName = "ghostnet", debug=False):
	input = burn_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "burn", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Approve(TypedDict):
	spender: Constructor_to
	value: Value

def approve_generator() -> Approve:
	return {'spender' : _to_generator (),'value' : value_generator ()}

def approve_encode(arg : Approve):
	try:
		return PairType.from_comb([(_to_encode(arg['spender'])), (value_encode(arg['value']))])
	except Exception as e:
		print('Error in approve_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def approve_decode(arg) -> Approve:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,value_decode))(arg)
		return {'spender' : (before_projection[0]),
'value' : (before_projection[1])}


	except Exception as e:
		print('Error in approve_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_approve():
	temp=approve_generator()
	temp_encoded=approve_encode(temp)
	try:
		print('++++ approve: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of approve: " + str(temp))
	temp1=approve_decode(temp_encoded)
	if temp1 == temp:
		print('approve check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('approve check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_approve()
#except Exception as e:
#	print('Error in assert_identity_approve' + str(e))




def callApprove(kt1 : str, _from : blockchain.identity, param : Approve, networkName = "ghostnet", debug=False):
	input = approve_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "approve", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UNSAFE_redeem = Value


def uNSAFE_redeem_generator() -> UNSAFE_redeem:
	return value_generator ()


def uNSAFE_redeem_encode (arg: UNSAFE_redeem):
	try:
		return value_encode(arg)
	except Exception as e:
		print('Error in uNSAFE_redeem_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def uNSAFE_redeem_decode (arg) -> UNSAFE_redeem:
	try:
		return value_decode(arg)
	except Exception as e:
		print('Error in uNSAFE_redeem_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_uNSAFE_redeem():
	temp=uNSAFE_redeem_generator()
	temp_encoded=uNSAFE_redeem_encode(temp)
	try:
		print('++++ uNSAFE_redeem: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of uNSAFE_redeem: " + str(temp))
	temp1=uNSAFE_redeem_decode(temp_encoded)
	if temp1 == temp:
		print('uNSAFE_redeem check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('uNSAFE_redeem check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_uNSAFE_redeem()
#except Exception as e:
#	print('Error in assert_identity_uNSAFE_redeem' + str(e))




def callUNSAFE_redeem(kt1 : str, _from : blockchain.identity, param : UNSAFE_redeem, networkName = "ghostnet", debug=False):
	input = uNSAFE_redeem_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "uNSAFE_redeem", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

State = factori_types.int_michelson

def state_generator() -> State:
	return factori_types.int_generator ()

def state_encode (arg: State):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in state_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def state_decode (arg) -> State:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in state_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_state():
	temp=state_generator()
	temp_encoded=state_encode(temp)
	try:
		print('++++ state: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of state: " + str(temp))
	temp1=state_decode(temp_encoded)
	if temp1 == temp:
		print('state check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('state check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_state()
#except Exception as e:
#	print('Error in assert_identity_state' + str(e))




Paused = bool

def paused_generator() -> Paused:
	return factori_types.bool_generator ()

def paused_encode (arg: Paused):
	try:
		return factori_types.bool_encode(arg)
	except Exception as e:
		print('Error in paused_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def paused_decode (arg) -> Paused:
	try:
		return factori_types.bool_decode(arg)
	except Exception as e:
		print('Error in paused_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_paused():
	temp=paused_generator()
	temp_encoded=paused_encode(temp)
	try:
		print('++++ paused: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of paused: " + str(temp))
	temp1=paused_decode(temp_encoded)
	if temp1 == temp:
		print('paused check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('paused check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_paused()
#except Exception as e:
#	print('Error in assert_identity_paused' + str(e))




LastInterestCompoundTime = factori_types.timestamp

def lastInterestCompoundTime_generator() -> LastInterestCompoundTime:
	return factori_types.timestamp_generator ()

def lastInterestCompoundTime_encode (arg: LastInterestCompoundTime):
	try:
		return factori_types.timestamp_encode(arg)
	except Exception as e:
		print('Error in lastInterestCompoundTime_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def lastInterestCompoundTime_decode (arg) -> LastInterestCompoundTime:
	try:
		return factori_types.timestamp_decode(arg)
	except Exception as e:
		print('Error in lastInterestCompoundTime_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_lastInterestCompoundTime():
	temp=lastInterestCompoundTime_generator()
	temp_encoded=lastInterestCompoundTime_encode(temp)
	try:
		print('++++ lastInterestCompoundTime: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of lastInterestCompoundTime: " + str(temp))
	temp1=lastInterestCompoundTime_decode(temp_encoded)
	if temp1 == temp:
		print('lastInterestCompoundTime check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('lastInterestCompoundTime check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_lastInterestCompoundTime()
#except Exception as e:
#	print('Error in assert_identity_lastInterestCompoundTime' + str(e))




class Type6(TypedDict):
	approvals: factori_types.Map
	balance: Value

def type6_generator() -> Type6:
	return {'approvals' : (factori_types.map_generator(_to_generator,value_generator)) (),'balance' : value_generator ()}

def type6_encode(arg : Type6):
	try:
		return PairType.from_comb([(factori_types.map_encode(_to_encode,value_encode)(arg['approvals'])), (value_encode(arg['balance']))])
	except Exception as e:
		print('Error in type6_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def type6_decode(arg) -> Type6:
	try:
		before_projection=(factori_types.tuple2_decode (factori_types.map_decode(_to_decode,value_decode),value_decode))(arg)
		return {'approvals' : (before_projection[0]),
'balance' : (before_projection[1])}


	except Exception as e:
		print('Error in type6_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_type6():
	temp=type6_generator()
	temp_encoded=type6_encode(temp)
	try:
		print('++++ type6: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of type6: " + str(temp))
	temp1=type6_decode(temp_encoded)
	if temp1 == temp:
		print('type6 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('type6 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_type6()
#except Exception as e:
#	print('Error in assert_identity_type6' + str(e))




class Constructor_storage(TypedDict):
	tokenContractAddress: Constructor_to
	state: State
	stabilityFundContractAddress: Constructor_to
	savedState_tokensToRedeem: Optional[Value]
	savedState_depositor: Optional[Constructor_to]
	paused: Paused
	governorContractAddress: Constructor_to
	balances: factori_types.BigMap
	interestRate: Value
	lastInterestCompoundTime: LastInterestCompoundTime
	metadata: factori_types.BigMap
	pauseGuardianContractAddress: Constructor_to
	savedState_redeemer: Optional[Constructor_to]
	savedState_tokensToDeposit: Optional[Value]
	token_metadata: factori_types.BigMap
	totalSupply: Value
	underlyingBalance: Value

def _storage_generator() -> Constructor_storage:
	return {'tokenContractAddress' : _to_generator (),'state' : state_generator (),'stabilityFundContractAddress' : _to_generator (),'savedState_tokensToRedeem' : (factori_types.option_generator(value_generator)) (),'savedState_depositor' : (factori_types.option_generator(_to_generator)) (),'paused' : paused_generator (),'governorContractAddress' : _to_generator (),'balances' : (factori_types.big_map_generator(_to_generator,type6_generator)) (),'interestRate' : value_generator (),'lastInterestCompoundTime' : lastInterestCompoundTime_generator (),'metadata' : (factori_types.big_map_generator(factori_types.string_generator,factori_types.bytes_generator)) (),'pauseGuardianContractAddress' : _to_generator (),'savedState_redeemer' : (factori_types.option_generator(_to_generator)) (),'savedState_tokensToDeposit' : (factori_types.option_generator(value_generator)) (),'token_metadata' : (factori_types.big_map_generator(value_generator,setTokenMetadata_generator)) (),'totalSupply' : value_generator (),'underlyingBalance' : value_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([PairType.from_comb([PairType.from_comb([(factori_types.big_map_encode(_to_encode,type6_encode)(arg['balances'])), (_to_encode(arg['governorContractAddress']))]), PairType.from_comb([(value_encode(arg['interestRate'])), (lastInterestCompoundTime_encode(arg['lastInterestCompoundTime']))])]), PairType.from_comb([PairType.from_comb([(factori_types.big_map_encode(factori_types.string_encode,factori_types.bytes_encode)(arg['metadata'])), (_to_encode(arg['pauseGuardianContractAddress']))]), PairType.from_comb([(paused_encode(arg['paused'])), (factori_types.option_encode(_to_encode)(arg['savedState_depositor']))])])]), PairType.from_comb([PairType.from_comb([PairType.from_comb([(factori_types.option_encode(_to_encode)(arg['savedState_redeemer'])), (factori_types.option_encode(value_encode)(arg['savedState_tokensToDeposit']))]), PairType.from_comb([(factori_types.option_encode(value_encode)(arg['savedState_tokensToRedeem'])), (_to_encode(arg['stabilityFundContractAddress']))])]), PairType.from_comb([PairType.from_comb([(state_encode(arg['state'])), (_to_encode(arg['tokenContractAddress']))]), PairType.from_comb([(factori_types.big_map_encode(value_encode,setTokenMetadata_encode)(arg['token_metadata'])), PairType.from_comb([(value_encode(arg['totalSupply'])), (value_encode(arg['underlyingBalance']))])])])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.big_map_decode(_to_decode,type6_decode),_to_decode)),(factori_types.tuple2_decode (value_decode,lastInterestCompoundTime_decode)))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.big_map_decode(factori_types.string_decode,factori_types.bytes_decode),_to_decode)),(factori_types.tuple2_decode (paused_decode,factori_types.option_decode(_to_decode))))))),(factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.option_decode(_to_decode),factori_types.option_decode(value_decode))),(factori_types.tuple2_decode (factori_types.option_decode(value_decode),_to_decode)))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (state_decode,_to_decode)),(factori_types.tuple2_decode (factori_types.big_map_decode(value_decode,setTokenMetadata_decode),(factori_types.tuple2_decode (value_decode,value_decode))))))))))(arg)
		return {'balances' : (before_projection[0][0][0][0]),
'governorContractAddress' : (before_projection[0][0][0][1]),
'interestRate' : (before_projection[0][0][1][0]),
'lastInterestCompoundTime' : (before_projection[0][0][1][1]),
'metadata' : (before_projection[0][1][0][0]),
'pauseGuardianContractAddress' : (before_projection[0][1][0][1]),
'paused' : (before_projection[0][1][1][0]),
'savedState_depositor' : (before_projection[0][1][1][1]),
'savedState_redeemer' : (before_projection[1][0][0][0]),
'savedState_tokensToDeposit' : (before_projection[1][0][0][1]),
'savedState_tokensToRedeem' : (before_projection[1][0][1][0]),
'stabilityFundContractAddress' : (before_projection[1][0][1][1]),
'state' : (before_projection[1][1][0][0]),
'tokenContractAddress' : (before_projection[1][1][0][1]),
'token_metadata' : (before_projection[1][1][1][0]),
'totalSupply' : (before_projection[1][1][1][1][0]),
'underlyingBalance' : (before_projection[1][1][1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract savings_pool")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/savings_pool_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1