(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains utility functions for reading and processing
Michelson code files used in the OCaml generation process of the Tezos
blockchain, including functions for reading from input channels, reading file
contents, and packing Micheline values. There are no monad binders in this file.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

let utils_ml =
  {|open Tzfunc.Proto

let rec read ic buffer bytes =
  let nbytes = input ic bytes 0 1024 in
  if nbytes > 0 then
    begin
      Buffer.add_subbytes buffer bytes 0 nbytes ;
      read ic buffer bytes
    end

let read_file filename =
  let ic = open_in filename in
  let bytes = Bytes.create 1024 in
  let buf = Buffer.create 1024 in
  read ic buf bytes ;
  close_in ic ;
  Buffer.contents buf

let get_code filename : script_expr =
  if not (Sys.file_exists filename) then (
    Format.eprintf "Could not find file: %s\n" filename ;
    raise Not_found)
  else
    let s = read_file filename in
    Micheline (EzEncoding.destruct Tzfunc.Proto.(micheline_enc.Encoding.json) s)

let get_code_from_path path name : script_expr =
  let ( // ) x y = Filename.concat x y in
  let filename = path // (name ^ ".json") in
  get_code filename

let pack ?(print = false) michtyp michval =
  let open Format in
  if print then
    eprintf
      "Calling pack on\ntype ==> %s\nand value ==> %s\n\n%!"
      (EzEncoding.construct Tzfunc.Proto.micheline_enc.json michtyp)
      (EzEncoding.construct Tzfunc.Proto.micheline_enc.json michval) ;
  match Tzfunc.Forge.pack michtyp michval with
  | Ok packed -> Result.Ok packed
  | Error e ->
      Format.eprintf "%s" (Tzfunc.Rp.string_of_error e) ;
      Result.error e|}
