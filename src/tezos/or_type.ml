(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines an algebraic data type for values that can take on
one of two possible types, and includes functions for comparing, pretty-
printing, folding, and mapping over values of this type. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Format
open Factori_utils

type 'a or_type =
  | LeafOr of 'a
  | OrAbs of 'a or_type * 'a or_type
[@@deriving show, encoding { recursive }]

let rec eq_or_type ~eq p1 p2 =
  match (p1, p2) with
  | LeafOr x1, LeafOr x2 -> eq x1 x2
  | OrAbs (l1, l2), OrAbs (l3, l4) ->
    eq_or_type ~eq l1 l3 && eq_or_type ~eq l2 l4
  | _, _ -> false

let pp_ortype_ocaml (f : formatter -> 'a -> unit) (ppf : formatter) x : unit =
  let rec aux ppf = function
    | LeafOr l -> fprintf ppf "%a" f l
    | OrAbs (l1, l2) ->
      fprintf ppf "Mprim {prim = `or_;\nargs = [%a];\nannots=[]}\n"
        (pp_print_list ~pp_sep:(tag ";") aux)
        [l1; l2] in
  aux ppf x

let pp_ortype_typescript (f : formatter -> 'a -> unit) (ppf : formatter) x :
    unit =
  let rec aux ppf = function
    | LeafOr l -> fprintf ppf "%a" f l
    | OrAbs (l1, l2) ->
      fprintf ppf "{prim : \"or\",\nargs : [%a]}\n"
        (pp_print_list ~pp_sep:(tag ",") aux)
        [l1; l2] in
  aux ppf x

let pp_ortype_csharp (f : formatter -> 'a -> unit) (ppf : formatter) x : unit =
  let rec aux ppf = function
    | LeafOr l -> fprintf ppf "%a" f l
    | OrAbs (l1, l2) ->
      fprintf ppf
        "new MichelinePrim {Prim = PrimType.or,Args = new \
         List<IMicheline>(){%a}}"
        (pp_print_list ~pp_sep:(tag ",") aux)
        [l1; l2] in
  aux ppf x

let pp_ortype_python (f : formatter -> 'a -> unit) (ppf : formatter) x : unit =
  let rec aux ppf = function
    | LeafOr l -> fprintf ppf "%a" f l
    | OrAbs (l1, l2) ->
      fprintf ppf "OrType.create_type([%a])"
        (pp_print_list ~pp_sep:(tag ",") aux)
        [l1; l2] in
  aux ppf x

let fold_or_type (f : 'a -> 'b -> 'b) (init : 'b) (x : 'a or_type) =
  let rec aux res = function
    | LeafOr x -> f x res
    | OrAbs (x, y) -> aux (aux res y) x in
  aux init x

let fold_and_replace_or_type ~f ~init x =
  let rec aux res = function
    | LeafOr x -> f x res
    | OrAbs (x, y) ->
      let t1, res1 = aux res x in
      let t2, res2 = aux res1 y in
      (OrAbs (t1, t2), res2) in
  aux init x

let fold_and_replace_or_type_combine ~f combine x =
  let rec aux = function
    | LeafOr x -> f x
    | OrAbs (x, y) ->
      let t1, res1 = aux x in
      let t2, res2 = aux y in
      (OrAbs (t1, t2), combine res1 res2) in
  aux x

let rec mapOr_pure ~f = function
  | LeafOr t_ -> LeafOr (f t_)
  | OrAbs (x, y) -> OrAbs (mapOr_pure ~f x, mapOr_pure ~f y)

(** Can't remember why this is called mapOr, it does something
     slightly different *)
let rec mapOr (f : 'a -> 'b -> 'b * 'a) (init : 'a) = function
  | LeafOr t_ ->
    let t_, res = f init t_ in
    (LeafOr t_, res)
  | OrAbs (x, y) ->
    let t_x, res_x = mapOr f init x in
    let t_y, res_y = mapOr f res_x y in
    (OrAbs (t_x, t_y), res_y)

let constructor_from_path path =
  String.concat "_"
    (List.map
       (fun b ->
         if b then
           "Left"
         else
           "Right")
       path)
