(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Csharp *)
open Xml
open Format
open Factori_utils
open Factori_config

module Generate = struct
  let blockchain_csharp_storage interface =
    sprintf "%s.initialBlockchainStorage.initial_blockchain_storage()" interface

  let random_csharp_storage interface = sprintf "new %s.Storage()" interface

  let storage_csharp ~storage_type =
    match storage_type with
    | Random -> random_csharp_storage
    | Blockchain -> blockchain_csharp_storage

  let deploy_csharp ~deploy_name_class ~contract_name ~network ~from
      ~storage_type ppf () =
    let get_from from = sprintf "%sFlextesa" from in
    fprintf ppf
      {|
using static Blockchain.Identities;

class %s {
public async static Task<string> Deploy()
{
    var storage = %s;
    var kt1 = await %s.Functions.Deploy(%s, storage, 1000000, 100000, 30000, 1000000, %S, false);
    if (kt1 == null)
    {
        throw new Exception("Deployment seems to have failed, no KT1 in output.");
    }
    Console.WriteLine($"Deploy %s contract at {kt1} on %s.");
    return kt1;
}
}|}
      (String.capitalize_ascii deploy_name_class)
      (storage_csharp ~storage_type contract_name)
      contract_name (get_from from) network contract_name network

  let scenario_csharp ~deploy_name_class ~scenario_name_class =
    Format.sprintf
      {|
class %s
{
    static async Task Main(string[] args)
    {
       string kt1 = await %s.Deploy(); 
    }
}|}
      (String.capitalize_ascii scenario_name_class)
      (String.capitalize_ascii deploy_name_class)
end

module Config = struct
  exception Unexpected_csproj_format of string

  (* [error_message] returns an error message depending on the path of the
     configuration file and the path of the generated file.

     At this point, Factori generated the deploy file but didn't find the
     configuration file. The cause might be that the user didn't use `make
     csharp-init` like in the tutorial, or the name of the configuration file
     changed. The error message has a field that the user can copy and paste into
     the configuration file. *)
  let error_message ~path_csproj ~scenario_name_class =
    Format.sprintf
      "Don't forget to run `make csharp-init` at the root of the project.@.The \
       file %s to deploy your smart contract has been generated,@.but Factori \
       could not find a valid configuration file at %s.@.This file needs to be \
       edited to prevent multiple entrypoint (if you want to know why: \
       https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-options/advanced#mainentrypoint-or-startupobject)@.You \
       can edit your csproj file by adding at the end of the project \
       field:@.@[<v 2><PropertyGroup>@,\
       @[<v 2><StartupObject>@,\
       Deploy_scenario_my_contract@]@,\
       </StartupObject>@]@,\
       </PropertyGroup>@.This will define the Deploy program as the main \
       entrypoint@.and you'll just have to execute `dotnet run --project` \
       PATH/TO/PROJECT"
      scenario_name_class path_csproj

  (* Adding exception handler to have a better print of the errors like xml ones or an unexpected format *)
  let csharp_handler ~scenario_name_class exn =
    Format.eprintf "%s%s%sError on factori deploy%s@." enable_red enable_bold
      enable_underline reset_format ;
    match exn with
    | Xml_light_errors.File_not_found path_csproj ->
      Format.eprintf "%s" (error_message ~path_csproj ~scenario_name_class)
    | Xml_light_errors.Xml_error _ ->
      Format.eprintf
        "The configuration file of your Factori project is not a correct xml, \
         maybe try to run `make csharp-build` to see if there is a problem"
    | Unexpected_csproj_format str ->
      Format.eprintf
        "The csproj of the Factori project has an unexpected format: %s" str
    | e -> raise e

  (* Returns true if the last StartupObject has the same name as the deploy scenario name *)
  (* StartupObject must be in a field PropertyGroup in a csharp configuration file *)
  let valid_configuration ~scenario_name_class ~xml =
    Xml.fold
      (fun acc elt ->
        match elt with
        | Element ("PropertyGroup", _, children) ->
          List.fold_left
            (fun acc elt ->
              match elt with
              | Element ("StartupObject", _, [PCData startup]) ->
                startup = scenario_name_class
              | _ -> acc)
            acc children
        | _ -> acc)
      false xml

  (* This function returns the field factori should add in the configuration file *)
  (* to define the deploy scenario as the new main *)
  let xml_deploy ~scenario_name_class =
    let startup = PCData scenario_name_class in
    let entrypoint = Element ("StartupObject", [], [startup]) in
    Element ("PropertyGroup", [], [entrypoint])

  (* Take the config xml and the name of the deploy class in entry *)
  (* and return the same xml with the new field at the end *)
  let generate_new_config ~xml ~scenario_name_class =
    match xml with
    | Element ("Project", str_l, child_l) ->
      let child = xml_deploy ~scenario_name_class in
      let child_l = List.rev (child :: List.rev child_l) in
      let xml = Element ("Project", str_l, child_l) in
      xml
    | _ ->
      raise
        (Unexpected_csproj_format
           "Configuration file doesn't start with a Project field")
end

(* Use all the above functions to write a deploy scenario in csharp *)
(* make this scenario the new main of the project and `launch dotnet` run *)
let deploy ~storage_type ~contract_name ~network ~from () =
  let dir = get_dir () in
  let deploy_name_class = sprintf "%s" (String.capitalize_ascii contract_name) in
  let deploy_file =
    asprintf "%a"
      (Generate.deploy_csharp ~deploy_name_class ~contract_name ~network ~from
         ~storage_type)
      () in
  let deploy_dir = Csharp_SDK.deploy_dir ~dir () in
  let deploy_name_cs = sprintf "%s.cs" deploy_name_class in
  let full_deploy_path = deploy_dir // deploy_name_cs in
  write_file ~filename:full_deploy_path deploy_file ;
  let scenario_name_class = Format.sprintf "Deploy_scenario_%s" contract_name in
  let scenario_file =
    Generate.scenario_csharp ~scenario_name_class ~deploy_name_class in
  let full_path_scenario =
    Csharp_SDK.scenarios_dir ~dir ()
    // Format.sprintf "%s.cs" scenario_name_class in
  write_file ~filename:full_path_scenario scenario_file ;
  let config_file = Csharp_SDK.sdk_file_path ~dir in
  Factori_errors.add_exception_handler
    (Config.csharp_handler ~scenario_name_class) ;
  let xml = Xml.parse_file config_file in
  let new_xml =
    if Config.valid_configuration ~scenario_name_class ~xml then
      xml
    else
      Config.generate_new_config ~xml ~scenario_name_class in
  let str = Xml.to_string_fmt new_xml in
  write_file ~filename:config_file str ;
  (* Here is a little message for the user because Factori set the deploy scenario *)
  (* as the new main of the project. The user should edit it or use factori deploy clean *)
  (* to clean what factori generates with a deploy *)
  Format.eprintf
    "Factori modified your configuration file at %s and changed the entrypoint \
     of your project.@.If you don't edit your configuration file, the next \
     `dotnet run` will take as main the file %s@."
    config_file scenario_name_class ;
  sprintf "dotnet run --project %s" (Csharp_SDK.base_dir ~dir ())
