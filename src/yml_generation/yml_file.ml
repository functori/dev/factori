(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module defines data types and functions to represent and print
DipDup configuration files in YAML format, as well as read and write backup
files. It includes various data types, utility functions, and a main function
`write_yml_file` to write the YML file to a specified path. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Format
open Factori_errors

type yml_dipdup_db = {
  yml_dipdup_db_kind : string;
  yml_dipdup_db_path : string;
}
[@@deriving encoding]

type yml_dipdup_contract = {
  yml_dipdup_contract_name : string;
  yml_dipdup_contract_address : string;
  yml_dipdup_contract_typename : string;
}
[@@deriving encoding]

type dipdup_metadata = {
  dipdup_metadata_url : string;
  dipdup_metadata_network : string;
}
[@@deriving encoding]

type yml_dipdup_ds =
  | Tzkt of string [@kind "tzkt"] [@kind_label "kind"] [@wrap "Tzkt"]
  | Coinbase [@kind "coinbase"] [@kind_label "kind"] [@wrap "Coinbase"]
  | Metadata of dipdup_metadata [@kind "metadata"] [@kind_label "kind"]
      [@wrap "Metadata"]
  | Ipfs of string [@kind "ipfs"] [@kind_label "kind"] [@wrap "Ipfs"]
[@@deriving encoding]

type dipdup_index_kind =
  | DDOperation of string [@kind "operation"] [@kind_label "kind"]
      [@wrap "Operation"]
  | DDBig_map [@kind "big_map"] [@kind_label "kind"] [@wrap "Big_map"]
[@@deriving encoding]

type dipdup_pattern = {
  dipdup_pattern_destination : string;
  dipdup_pattern_entrypoint : string;
}
[@@deriving encoding]

type dipdup_handler = {
  dipdup_handler_callback : string;
  dipdup_handler_pattern : dipdup_pattern option;
  dipdup_handler_contract : string option;
  dipdup_handler_path : string option;
}
[@@deriving encoding]

type yml_dipdup_index = {
  yml_dipdup_index_name : string;
  yml_dipdup_index_kind : dipdup_index_kind;
  yml_dipdup_index_datasource : string;
  yml_dipdup_index_handlers : dipdup_handler list;
}
[@@deriving encoding]

type yml_dipdup_reindex_kind =
  | Exception
  | Wipe
  | Ignore
[@@deriving encoding]

type yml_dipdup_reindex = {
  yml_dipdup_reindex_manual : yml_dipdup_reindex_kind option;
  yml_dipdup_reindex_migration : yml_dipdup_reindex_kind option;
  yml_dipdup_reindex_rollback : yml_dipdup_reindex_kind option;
  yml_dipdup_reindex_config_modified : yml_dipdup_reindex_kind option;
  yml_dipdup_reindex_schema_modified : yml_dipdup_reindex_kind option;
}
[@@deriving encoding]

type yml_dipdup_advanced_feature = Reindex of yml_dipdup_reindex
[@@deriving encoding]

type yml_dipdup_file = {
  yml_dipdup_spec : string option;
  yml_dipdup_package : string option;
  yml_dipdup_database : yml_dipdup_db option;
  yml_dipdup_contracts : yml_dipdup_contract list;
  yml_dipdup_datasources : (string * yml_dipdup_ds) list;
  yml_dipdup_indexes : yml_dipdup_index list;
  yml_dipdup_advanced : yml_dipdup_advanced_feature list;
}
[@@deriving encoding]

let empty_yml_file =
  {
    yml_dipdup_spec = None;
    yml_dipdup_package = None;
    yml_dipdup_database = None;
    yml_dipdup_contracts = [];
    yml_dipdup_datasources = [];
    yml_dipdup_indexes = [];
    yml_dipdup_advanced = [];
  }

let print_yml_dipdup_spec ppf = function
  | None -> fprintf ppf "spec_version: 1.2"
  | Some s -> fprintf ppf "spec_version: %s" s

let print_yml_dipdup_package ppf = function
  | None -> ()
  | Some p -> fprintf ppf "package: %s" p

let print_yml_dipdup_database ppf = function
  | None -> ()
  | Some db ->
    fprintf ppf "database:\n  kind: %s\n  path: %s" db.yml_dipdup_db_kind
      db.yml_dipdup_db_path

let print_contract ppf contract =
  fprintf ppf "  %s:\n    address: %s\n    typename: %s"
    contract.yml_dipdup_contract_name contract.yml_dipdup_contract_address
    contract.yml_dipdup_contract_typename

let print_yml_dipdup_contracts ppf contracts =
  fprintf ppf "contracts:\n%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
       print_contract)
    contracts

let print_datasource ppf (name, kind) =
  match kind with
  | Tzkt url -> fprintf ppf "  %s:\n    kind: tzkt\n    url: %s" name url
  | Coinbase -> fprintf ppf "  %s:\n    kind: coinbase" name
  | Metadata dm ->
    fprintf ppf "  %s:\n    kind: tzkt\n    url: %s\n    network: %s" name
      dm.dipdup_metadata_url dm.dipdup_metadata_network
  | Ipfs url -> fprintf ppf "  %s:\n    kind: ipfs\n    url: %s" name url

let print_yml_dipdup_datasources ppf ds =
  fprintf ppf "datasources:\n%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
       print_datasource)
    ds

let print_index_kind ppf = function
  | DDOperation c ->
    fprintf ppf "    kind: operation\n    contracts:\n      - %s" c
  | DDBig_map -> fprintf ppf "    kind: big_map\n    skip_history: never"

let pp_print_handler_pattern ppf = function
  | None -> ()
  | Some p ->
    fprintf ppf
      "        pattern:\n\
      \          - destination: %s\n\
      \            entrypoint: %s\n"
      p.dipdup_pattern_destination p.dipdup_pattern_entrypoint

let pp_print_handler_contract ppf = function
  | None -> ()
  | Some s -> fprintf ppf "        contract: %s\n" s

let pp_print_handler_path ppf = function
  | None -> ()
  | Some "0" -> fprintf ppf "        path: big_map\n"
  | Some s -> fprintf ppf "        path: %s\n" s

let print_index_handler ppf handler =
  fprintf ppf "      - callback: %s\n%a%a%a" handler.dipdup_handler_callback
    pp_print_handler_pattern handler.dipdup_handler_pattern
    pp_print_handler_contract handler.dipdup_handler_contract
    pp_print_handler_path handler.dipdup_handler_path

let print_index_handlers ppf handlers =
  fprintf ppf "    handlers:\n%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
       print_index_handler)
    handlers

let print_index ppf index =
  match index.yml_dipdup_index_handlers with
  | [] -> ()
  | _ ->
    fprintf ppf "  %s:\n%a\n    datasource: %s\n%a" index.yml_dipdup_index_name
      print_index_kind index.yml_dipdup_index_kind
      index.yml_dipdup_index_datasource print_index_handlers
      index.yml_dipdup_index_handlers

let print_yml_dipdup_indexes ppf indexes =
  fprintf ppf "indexes:\n%a"
    (pp_print_list ~pp_sep:(fun ppf _ -> pp_print_newline ppf ()) print_index)
    indexes

let print_reindex_kind ppf = function
  | Exception -> fprintf ppf "exception"
  | Wipe -> fprintf ppf "wipe"
  | Ignore -> fprintf ppf "ignore"

let print_reindex_manual ppf reind =
  match reind.yml_dipdup_reindex_manual with
  | None -> ()
  | Some k -> fprintf ppf "    manual: %a\n" print_reindex_kind k

let print_reindex_migration ppf reind =
  match reind.yml_dipdup_reindex_migration with
  | None -> ()
  | Some k -> fprintf ppf "    migration: %a\n" print_reindex_kind k

let print_reindex_rollback ppf reind =
  match reind.yml_dipdup_reindex_rollback with
  | None -> ()
  | Some k -> fprintf ppf "    rollback: %a\n" print_reindex_kind k

let print_reindex_config_modified ppf reind =
  match reind.yml_dipdup_reindex_config_modified with
  | None -> ()
  | Some k -> fprintf ppf "    config_modified: %a\n" print_reindex_kind k

let print_reindex_schema_modified ppf reind =
  match reind.yml_dipdup_reindex_schema_modified with
  | None -> ()
  | Some k -> fprintf ppf "    schema_modified: %a\n" print_reindex_kind k

let print_reind ppf reind =
  fprintf ppf "  reindex:\n%a%a%a%a%a" print_reindex_manual reind
    print_reindex_migration reind print_reindex_rollback reind
    print_reindex_config_modified reind print_reindex_schema_modified reind

let print_adv ppf = function
  | Reindex reind -> print_reind ppf reind

let print_yml_dipdup_advanced ppf = function
  | [] -> ()
  | adv ->
    fprintf ppf "advanced:\n%a"
      (pp_print_list ~pp_sep:(fun ppf _ -> pp_print_newline ppf ()) print_adv)
      adv

let print_yml_dipdup_file ppf dipdup_file =
  fprintf ppf "%a\n%a\n\n%a\n\n%a\n\n%a\n\n%a\n\n%a" print_yml_dipdup_spec
    dipdup_file.yml_dipdup_spec print_yml_dipdup_package
    dipdup_file.yml_dipdup_package print_yml_dipdup_database
    dipdup_file.yml_dipdup_database print_yml_dipdup_contracts
    dipdup_file.yml_dipdup_contracts print_yml_dipdup_datasources
    dipdup_file.yml_dipdup_datasources print_yml_dipdup_indexes
    dipdup_file.yml_dipdup_indexes print_yml_dipdup_advanced
    dipdup_file.yml_dipdup_advanced

let read_backup_file ?(check_if_already_there = true) ~filename () =
  let backup_file = Factori_utils.get_backup_file_name ~filename in
  if not (Sys.file_exists backup_file) then
    if check_if_already_there then
      failwith
      @@ sprintf
           "[decode_yml_file] trying to decode a non-existing backup file %s"
           backup_file
    else
      empty_yml_file
  else
    try
      Factori_utils.deserialize_file ~silent:false ~filename:backup_file
        ~enc:yml_dipdup_file_enc
    with CouldNotDestruct _ | CouldNotReadAsJson _ ->
      Factori_utils.output_verbose
        "Could not deserialize file, will behave as if it were not present" ;
      empty_yml_file

let read_enc_backup_file ?(check_if_already_there = true) ~filename ~enc () =
  let backup_file = Factori_utils.get_backup_file_name ~filename in
  if not (Sys.file_exists backup_file) then
    if check_if_already_there then
      failwith
      @@ sprintf
           "[decode_yml_file] trying to decode a non-existing backup file %s"
           backup_file
    else
      None
  else
    try Some (Factori_utils.deserialize_file ~filename:backup_file ~enc)
    with CouldNotDestruct _ | CouldNotReadAsJson _ ->
      Factori_utils.output_verbose
        "Could not deserialize file, will behave as if it were not present" ;
      None

(** Take the filename of the yml file, and write the backup file by
   its side *)
let write_backup_file ~yml_file ~filename =
  let backup_filename = Factori_utils.get_backup_file_name ~filename in
  Factori_utils.serialize_file ~filename:backup_filename
    ~enc:yml_dipdup_file_enc yml_file

(** Write an Yml file <path/filename.py> and, by its side, write
   <path/filename.json> which contains a json encoded version of the Yml
   file, making it easy to read and update it. If with_readable_backup
   is set to false, then no json file is written *)
let write_yml_file ?(with_readable_backup = true) ~yml_file ~path () =
  Factori_utils.write_file ~filename:path
    (asprintf "%a" print_yml_dipdup_file yml_file) ;
  if with_readable_backup then write_backup_file ~yml_file ~filename:path
