(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file `deploy_cmd.ml` contains a module that provides a subcommand
named `deploy`, which is used to deploy contracts with various options such as
network, language, and storage type. Additionally, the module includes helper
functions for generating dune files and scenarios, as well as deploying the
contract using either the OCaml or TypeScript interface. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Factori_command
open Factori_options
open Factori_errors
open Factori_config
open Factori_utils
open Factori_deploy
open Format

let check_language_selection () =
  let ocaml_int = Bool.to_int !ocaml in
  let typescript_int = Bool.to_int !typescript in
  let python_int = Bool.to_int !python in
  let csharp_int = Bool.to_int !csharp in
  match ocaml_int + typescript_int + python_int + csharp_int with
  | 0 -> raise NoLanguageProvided
  | 1 -> (
    match (!ocaml, !typescript, !python, !csharp) with
    | true, _, _, _ -> OCaml
    | _, true, _, _ -> Typescript
    | _, _, true, _ -> Python
    | _ -> Csharp)
  | _ -> raise TooManyLanguages

let message_contract ppf (original, san) =
  Format.fprintf ppf
    "Note that %s is not the name of the contract in factori, it's %s" original
    san

let deploy_command =
  subcommand ~name:"deploy"
    ~args:
      [
        contract_name_anonymous;
        network_option;
        ocaml_language_option;
        typescript_language_option;
        python_language_option;
        csharp_language_option;
        storage_option;
      ] ~doc:"Quickly deploy one of your imported contracts." (fun () ->
      try
        Factori_config.verify_factori_repo ~dir:(Factori_config.get_dir ()) () ;
        let language = check_language_selection () in
        let network = !network in
        let from = !sender in
        let contract_name =
          match !contract_name with
          | None -> raise NoContractNameProvided
          | Some c ->
            let sanitized = sanitize_basename c in
            show_sanitized_difference ~message:message_contract ~sanitized ;
            show_sanitized_name sanitized in
        let storage_type = storage_type_of_str !storage_type in
        let cmd =
          match language with
          | OCaml ->
            Deploy_ocaml.deploy ~storage_type ~network ~from ~contract_name ()
          | Typescript ->
            Deploy_typescript.deploy ~storage_type ~network ~from ~contract_name
              ()
          | Python ->
            Deploy_python.deploy ~storage_type ~network ~from ~contract_name ()
          | Csharp ->
            Deploy_csharp.deploy ~storage_type ~contract_name ~network ~from ()
        in
        eprintf "[cmd] %s@." cmd ;
        let _ = Sys.command cmd in
        ()
      with e -> handle_exception e)
