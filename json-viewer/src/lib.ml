module Pretty = struct
  (* start..endding *)
  let shortened_by_the_middle ~max_len ~front_len str =
    try
      let front = String.sub str 0 front_len in
      let middle = ".." in
      let back_len = max_len - (front_len + String.length middle) in
      let back_start = String.length str - back_len in
      let back = String.sub str back_start back_len in
      Format.sprintf "%s%s%s" front middle back
    with _ -> str
end

module IdGenerator = struct
  type id = int [@@deriving jsoo]

  type t = int ref

  let create () = ref 0

  let gen last_id =
    let id = !last_id in
    incr last_id ;
    id
end

module Color = struct
  type t = string [@@deriving jsoo]

  let pale_pink = "#D3B6C6"

  let black = "black"

  let darkgreen = "darkgreen"
end

module Request = struct
  let get ~__FUNCTION__ url encoding =
    let result = EzReq_lwt.get (EzAPI.URL url) in
    Lwt.bind result @@ fun result ->
    try Lwt.return @@ EzEncoding.destruct encoding @@ Result.get_ok result
    with _ ->
      let err =
        Format.sprintf "%s: cannot get result from %s" __FUNCTION__ url in
      Lwt.fail_with err
end
