(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions and types for analyzing permissions in
Michelson code. It defines several types, including `prop`, `Constant`,
`Result`, and `Perm`, as well as functions for manipulating them. The file also
includes functions for performing permission analysis and analyzing entry and
return points of lambda functions. Warning: this short summary was automatically
generated and could be incomplete or misleading. If you feel this is the case,
please open an issue. *)

open Auxilary
open Unstack_micheline
open Factori_permission_lib
open Factori_representation
open Permission_tool
open Factori_errors
open Permission_ast
module NameArg = Name_arg

let alone_arg arg =
  match Nested_arg.name_string_list arg with
  | [res] -> Some res
  | _ -> None

module Prop = struct
  type t = prop

  let compare = equal_prop
end

module Constant = Factori_representation.Nested_arg.Constant

let find_or_simple elt constant =
  match Constant.find_opt elt constant with
  | None -> Ident elt
  | Some res -> res
(* map that will contains prop*)

module OrderedPerm = Permission_tool.Ordered_permission (Prop)
module MapTest = Map.Make (OrderedPerm)

module Result = Map.Make (struct
  type t = label * chain_call

  let compare = compare
end)

type formula_logic = prop Permission_tool.michelson_logic

let transform_formula (f : formula_logic) =
  let open Tzfunc.Proto in
  let aux f =
    match f with
    | Is_true (Propagate (`NOT, [], [x])) -> Logic_not (P (Is_true x))
    | Is_true (Propagate (`PUSH, [_; Mprim { prim = `False; _ }], [])) -> False
    | Is_true (Propagate (`PUSH, [_; Mprim { prim = `True; _ }], [])) -> True
    | Is_none (Propagate (`NONE, _, []))
    | Is_none (Propagate (`PUSH, [_; Mprim { prim = `None; _ }], [])) -> True
    | Is_true
        (Propagate
          ( `EQ,
            [],
            [arg; Propagate (`PUSH, [_; Mprim { prim = `False; _ }], [])] ))
    | Is_true
        (Propagate
          ( `EQ,
            [],
            [Propagate (`PUSH, [_; Mprim { prim = `False; _ }], []); arg] ))
    | Is_true
        (Propagate
          ( `NEQ,
            [],
            [arg; Propagate (`PUSH, [_; Mprim { prim = `True; _ }], [])] ))
    | Is_true
        (Propagate
          ( `NEQ,
            [],
            [Propagate (`PUSH, [_; Mprim { prim = `True; _ }], []); arg] )) ->
      Logic_not (P (Is_true arg))
    | Is_true
        (Propagate
          (`EQ, [], [arg; Propagate (`PUSH, [_; Mprim { prim = `True; _ }], [])]))
    | Is_true
        (Propagate
          (`EQ, [], [Propagate (`PUSH, [_; Mprim { prim = `True; _ }], []); arg]))
    | Is_true
        (Propagate
          ( `NEQ,
            [],
            [arg; Propagate (`PUSH, [_; Mprim { prim = `False; _ }], [])] ))
    | Is_true
        (Propagate
          ( `NEQ,
            [],
            [Propagate (`PUSH, [_; Mprim { prim = `False; _ }], []); arg] )) ->
      P (Is_true arg)
    | f -> P f in
  Permission_tool.map_in_logic ~f:aux f

module Entry_point = Path

let make_logic_entry_point (t : Nested_arg.t) :
    (bool MapTest.t * Nested_arg.t) Entry_point.t =
  let open Nested_arg in
  let rec aux_logic_entry map acc t =
    match t with
    | Or (left, right, _) ->
      let actual = Is_left (Ident t) in
      let l_acc = MapTest.add actual true acc in
      let r_acc = MapTest.add actual false acc in
      let map = aux_logic_entry map l_acc left in
      aux_logic_entry map r_acc right
    | _ ->
      let map =
        match Factori_utils.get_name_annots @@ Nested_arg.get_annot t with
        | None -> map
        | Some entry -> Entry_point.add entry (acc, t) map in
      map in
  let result = aux_logic_entry Entry_point.empty MapTest.empty t in
  if Entry_point.is_empty result then
    Entry_point.singleton "default" (MapTest.empty, t)
  else
    result

let formula_simplification f =
  let f = transform_formula f in
  Permission_tool.simplify OrderedPerm.compare f

(* this functions map the logic formule building a constant contexte when seeing an assignation node *)
let map_assignations ?(verbose = false) f =
  let assignation = Sys.time () in
  let rec map_test acc f =
    match f with
    | Logic_and l ->
      let l = List.rev l in
      let acc, l = List.fold_left_map map_test acc l in
      (acc, Logic_and (List.rev l))
    | Logic_or l ->
      let l = List.map (fun l -> snd @@ map_test acc l) l in
      (acc, Logic_or l)
    | Logic_not l ->
      (* SOMETHING TO DO WITH THIS NOT *)
      let acc, l = map_test acc l in
      (acc, Logic_not l)
    | True -> (acc, True)
    | False -> (acc, False)
    | P (Is_true (Equal (nested, prop))) ->
      let f_prop = function
        | Ident n -> find_or_simple n acc
        | prop -> prop in
      let prop = map_prop ~f_args:(fun a -> a) ~f_prop prop in
      let logic =
        if verbose then
          P (Is_true (Equal (nested, prop)))
        else
          True in
      (Constant.add nested prop acc, logic)
    | P l ->
      let f_prop = function
        | Ident n -> find_or_simple n acc
        | prop -> prop in
      let p = Permission_tool.map_permission f_prop l in
      (acc, P p) in
  let res = map_test Constant.empty f in
  let end_assignation = Sys.time () in
  let time = end_assignation -. assignation in
  Format.eprintf "Assignation time : %f@." time ;
  res
(* potentiellement là où on va avoir un equal *)

let replace_entry_point map f =
  let open Permission_tool in
  Entry_point.map
    (fun (logic, t) ->
      let res =
        Result.fold
          (fun lab (env, formule, elt) acc ->
            let no_simp_formule =
              Permission_tool.replace_true
                (fun perm -> MapTest.find_opt perm logic)
                formule in
            let formule = formula_simplification no_simp_formule in
            match formule with
            | False -> acc
            | _ ->
              let formule = formula_simplification formule in
              Result.add lab (env, formule, elt) acc)
          f Result.empty in
      (res, t))
    map

module Perm = struct
  module SA = Auxilary.Aux

  let map_formula (f : prop -> prop) (logic : formula_logic) =
    map_logic (map_permission f) logic

  let compare_formula a b =
    (* we suppose that both formule are simplified*)
    compare_logic OrderedPerm.compare a b

  module Formule_map = Map.Make (struct
    type t = formula_logic

    let compare = compare_formula
  end)

  type indirection =
    | Indirect of formula_logic
    | Direct of prop

  type cst_propagation = prop Formule_map.t Constant.t

  module Regle = Map.Make (Nested_arg.Ordered_nested)

  type t = cst_propagation * formula_logic

  let print_constant ?(verbose = false) ppf cst =
    let cst_elems = Constant.bindings cst in
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
      (fun ppf (l, s) ->
        Format.fprintf ppf "%a -> %a"
          (Nested_arg.print ~verbose)
          l
          (print_prop ~verbose ~nested_printer:Nested_arg.print)
          s)
      ppf cst_elems

  let print_formula print_prop ppf f =
    Format.fprintf ppf "%a"
      (Permission_tool.print_logic
         (Permission_tool.print_permission print_prop))
      f

  let print_env ?(verbose = false) ~nested_printer ppf env =
    let bindings = Constant.bindings env in
    let print_prop = print_prop ~verbose ~nested_printer in
    Format.pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf "@.@.")
      (fun ppf (arg, elt) ->
        Format.fprintf ppf "%a -> {%a}"
          (Nested_arg.print ~verbose)
          arg
          (Format.pp_print_list
             ~pp_sep:(fun ppf () -> Format.fprintf ppf ";@.")
             (fun ppf (formule, prop) ->
               Format.fprintf ppf "(%a,%a)" (print_formula print_prop) formule
                 print_prop prop))
          (Formule_map.bindings elt))
      ppf bindings

  let print ppf (_env, f) =
    (*print_env ~verbose:false ~nested_printer:Nested_arg.print ppf env*)
    print_formula (print_prop ~nested_printer:Nested_arg.print) ppf f

  let lub_env env1 env2 =
    Constant.union
      (fun _ map1 map2 ->
        let map =
          Formule_map.union
            (fun _ arg1 arg2 ->
              if prof_prop arg1 > prof_prop arg2 then
                Some arg1
              else
                Some arg2)
            map1 map2 in
        Some map)
      env1 env2

  let lub ((env_l, logic_l) : t) ((env_r, logic_r) : t) =
    (* for now f is not used but is should be some evaluation between c and f*)
    (* For example lub between Is_left x and Is_right x should be a or Is_left x or Is_right x that evaluates to true this is how
       we come back on the previous environment*)
    (* Other example with multiple if_left we could have Is_left x and Is_left y OR Is_left x and Is_right y this simplifies to
                     (Is_left x or Is_left x) and (Is_left x or Is_right y) and (Is_left y or Is_left x) and (Is_left y or Is_right y)
              -------------------------------------------------------------------------------------------------------------------------
                     Is_left x and (Is_left x or Is_right y) and (Is_left y or Is_left x) and true
                     here we can simplify with Is_left x

       a -> (true, l)
    *)
    let logic =
      match (logic_l, logic_r) with
      | Logic_and l, other | other, Logic_and l ->
        if List.exists (fun elt -> compare_formula elt other = 0) l then
          other
        else
          fact_or OrderedPerm.compare logic_l logic_r
      | _ -> Permission_tool.make_or_logic logic_l logic_r in
    let env = lub_env env_l env_r in
    let logic = formula_simplification logic in
    (env, logic)

  (* in this analyze c and f should always be the same *)
  let leq_formula actual_l actual_r =
    match (actual_l, actual_r) with
    | False, _ -> true
    | _, True -> true
    | _, False | True, _ -> false
    | a, Logic_or l when List.exists (fun elt -> compare_formula a elt = 0) l ->
      true
    | Logic_or l, a when List.exists (fun elt -> compare_formula a elt = 0) l ->
      false
    | a, Logic_and l when List.exists (fun elt -> compare_formula a elt = 0) l
      -> false
    | Logic_and l, a when List.exists (fun elt -> compare_formula a elt = 0) l
      -> true
    | Logic_and l, Logic_or l2
      when List.exists
             (fun l -> List.exists (fun l2 -> compare_formula l l2 = 0) l2)
             l -> true
    | Logic_and l, Logic_and l2
      when List.for_all
             (fun l2 -> List.exists (fun l -> compare_formula l l2 = 0) l)
             l2 -> true
    | _ -> compare_formula actual_l actual_r = 0

  let leq ((_, actual_l) : t) ((_, actual_r) : t) =
    (* naive case everyone is lower than another one this case should do nothing really right (in reality
       it depends if the arc have been sorted) *)
    leq_formula actual_l actual_r

  let find_cst_formula_opt elt actual constant =
    match Constant.find_opt elt constant with
    | None -> None
    | Some res -> (
      match Formule_map.find_opt actual res with
      | None -> (
        match
          Formule_map.find_first_opt (fun f -> leq_formula actual f) res
        with
        | None -> None
        | Some (_, res) -> Some res)
      | res -> res)

  let find_cst_formula elt actual constant =
    match find_cst_formula_opt elt actual constant with
    | None -> Ident elt
    | Some prop -> prop

  let bot : t = (Constant.empty, False)

  let union (a : t) (b : t) : t = lub a b

  let diff ((_b, _c) : t) ((_e, _f) : t) : t = failwith "TODO"
  (* in this analyze b and d should always be the same *)

  let iota_view (_tool : SA.analysis_tools) (_lab : label) : t =
    (Constant.empty, True)

  let ext_view (tool : SA.analysis_tools) : label list =
    List.map (fun (_, l_end, _) -> l_end) (SA.view_labels tool)

  let iota (_tool : SA.analysis_tools) : t = (Constant.empty, True)

  let link_return ~actual ~assign_l ~name_l ~env =
    let assign_nested = Nested_arg.name_string_list assign_l in
    let name_nested = Nested_arg.name_string_list name_l in
    try
      List.fold_left2
        (fun acc assign name ->
          let simple_assign = Nested_arg.simple assign in
          let simple_name = Nested_arg.simple name in
          Constant.update simple_assign
            (fun before ->
              let before =
                match before with
                | None -> Formule_map.empty
                | Some res -> res in
              let formula_map =
                Formule_map.add actual (Ident simple_name) before in
              Some formula_map)
            acc)
        env assign_nested name_nested
    with Invalid_argument _ ->
      raise
        (GenericError
           ( "permissions analysis",
             "Fail analysis due to wrong return in Michelson Unstack" ))

  let assign_in_formule ~paths ~actual ~nested_list ~simple =
    let list_logic =
      List.map
        (fun elt ->
          let nested_elt = Nested_arg.simple elt in
          match Path.find (fst elt) paths with
          | [] -> P (Is_true (Equal (nested_elt, simple)))
          | path -> P (Is_true (Equal (nested_elt, In_Nested (path, simple)))))
        nested_list in
    let l = and_or_simple list_logic in
    let actual = make_and_logic l actual in
    actual

  let add_in_env ~nested_elt ~actual ~env ~prop =
    Constant.update nested_elt
      (function
        | None -> Some (Formule_map.singleton actual prop)
        | Some map ->
          let map =
            Formule_map.update actual
              (function
                | None -> Some prop
                | Some prop -> Some prop)
              map in
          Some map)
      env

  let assign_in_env ~paths ~actual ~elements ~prop ~env =
    List.fold_left
      (fun acc elt ->
        let nested_elt = Nested_arg.simple elt in
        match Path.find (fst elt) paths with
        | [] -> add_in_env ~nested_elt ~actual ~prop ~env:acc
        | path ->
          let new_prop = In_Nested (path, prop) in
          add_in_env ~nested_elt ~actual ~prop:new_prop ~env:acc)
      env elements

  let link_in_formule assign_l name_l formule =
    let assign_nested = Nested_arg.name_string_list assign_l in
    let name_nested = Nested_arg.name_string_list name_l in
    List.fold_left2
      (fun acc assign name ->
        let right_f =
          P
            (Is_true
               (Equal (Nested_arg.simple assign, Ident (Nested_arg.simple name))))
        in
        make_and_logic acc right_f)
      formule assign_nested name_nested

  let make_simple prim args arg_l =
    let arg_l = List.map (fun arg -> Ident arg) arg_l in
    Propagate (prim, args, arg_l)

  let print_map f ppf map =
    let map_b = MapTest.bindings map in
    Format.pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf ";")
      (fun ppf (b, elt) ->
        Format.fprintf ppf "%a -> %b" (print_permission f) b elt)
      ppf map_b

  let determine_perm arg miche =
    let open Tzfunc.Proto in
    let res =
      match miche with
      | Mprim { prim = `or_; _ } -> Is_left arg
      | Mprim { prim = `list; _ } -> Is_cons arg
      | Mprim { prim = `option; _ } -> Is_none arg
      | Mprim { prim = `bool; _ } -> Is_true arg
      | _ -> failwith "Not possible for a type if" in
    P res

  let gen ((env, actual) : t) (lab : label) (tool : SA.analysis_tools) : t =
    let block = SA.block_of_label lab tool in
    let env, actual =
      match block with
      | BAssign (N_SIMPLE (`FAILWITH, _, [_arg], [], _lo), _l) ->
        (env, Permission_tool.False)
      | BAssign (N_SIMPLE (prim, args, used, [elt], _), _) -> (
        (* Other than unpair that was already remove with the inline_stack module, there are not much instruction that pushes
           multiple element on the stack. So for now we are going to ignore it *)
        let used_nested = List.rev @@ Nested_arg.name_string_list used in
        let paths = path_for_nested elt in
        match paths with
        | Error exn -> raise exn
        | Ok paths ->
          (* First we get all the paths like Pair x1 x2 would return {x1 -> First; x2 -> Second}*)
          (* it's like going backward with the nested_arg *)
          (* we can now take its nested list *)
          let nested_list = Nested_arg.name_string_list [elt] in
          let used_prop =
            List.map (fun elt -> Ident (Nested_arg.simple elt)) used_nested
          in
          let simple = Propagate (prim, args, used_prop) in
          let env, actual =
            let env =
              Constant.update elt
                (fun elt ->
                  let e =
                    match elt with
                    | None -> Formule_map.empty
                    | Some e -> e in
                  Some (Formule_map.add actual simple e))
                env in
            ( assign_in_env ~paths ~env ~prop:simple ~elements:nested_list
                ~actual,
              actual ) in
          (env, actual))
      | BReturnAssign (name_assign, name_l, _) ->
        let env, actual =
          (link_return ~actual ~assign_l:name_assign ~name_l ~env, actual) in
        (env, actual)
      | BThen (arg, _lab) ->
        let branch_formule =
          let ident = Ident arg in
          let miche = Nested_arg.deconstruct arg in
          determine_perm ident miche in
        let new_actual = make_and_logic actual branch_formule in
        let simp_actual = formula_simplification new_actual in
        (env, simp_actual)
      | BElse (arg, _lab) ->
        let branch_formule =
          let ident = Ident arg in
          let miche = Nested_arg.deconstruct arg in
          Logic_not (determine_perm ident miche) in
        let new_actual = make_and_logic actual branch_formule in
        let simp_actual = formula_simplification new_actual in
        (env, simp_actual)
      | BLoop _ -> (env, actual)
      | _ -> (env, actual) in
    (env, actual)

  let f_entry_analyze (tool : SA.analysis_tools) (chain : chain_call)
      (li : label) (le : label) (lab : label) (base_analyze : t) : t =
    (* link le resultat avec les potentiels fail imaginons que l'on fail sur le resultat de cette lambda
            alors préciser genre si x1 = EXEC l arg IF x1 then fail else continue dans notre map on sait que x1 a provoquer
       le fail donc link x1 avec res et dire que sur cette chain d'appel c'est res qui a fait fail le point d'entrée*)
    let block = SA.block_of_label lab tool in
    match block with
    | BStartExec (arg, name, _label) -> (
      let complete_lambdas =
        SA.find_lambda tool chain (Option.get @@ Nested_arg.to_name_arg name)
      in
      let opt =
        List.find_opt
          (fun lambda ->
            match lambda.lambda_code with
            | None -> false
            | Some l -> l.label_start = li && l.label_end = le)
          complete_lambdas in
      match opt with
      | Some { lambda_code = Some code; in_between; lambda_name = _ } ->
        let env, actual = base_analyze in
        let env, f =
          ( link_return ~actual ~assign_l:[code.arg] ~name_l:(arg :: in_between)
              ~env,
            actual ) in
        (*let cst =
          List.fold_left2
            (fun constant arg arg_lambda ->
              Constant.add
                (Nested_arg.simple arg_lambda)
                (find_or_simple (Nested_arg.simple arg) env)
                constant)
            Constant.empty arg_exec arg_lambda in*)
        (env, f)
      | _ ->
        let _ = failwith "Anonymous functions" in
        base_analyze)
    | _ -> failwith "Should be a start exec block"

  let f_return_analyze (tool : SA.analysis_tools) (chain : chain_call)
      (li : label) (le : label) (lab : label)
      ((before_call, after_call) : t * t) : t =
    let block = SA.block_of_label lab tool in
    match block with
    | BEndExec (res, name, _label) -> (
      let env, actual = after_call in
      let _old_env, _ = before_call in
      let complete_lambdas =
        SA.find_lambda tool chain (Option.get @@ Nested_arg.to_name_arg name)
      in
      let opt =
        List.find_opt
          (fun lambda ->
            match lambda.lambda_code with
            | None -> false
            | Some l -> l.label_start = li && l.label_end = le)
          complete_lambdas in
      match opt with
      | Some { lambda_code = Some code; _ } ->
        let env =
          Option.fold ~none:env
            ~some:(fun res_in_lambda ->
              let env =
                Constant.update res
                  (fun _ -> Constant.find_opt res_in_lambda env)
                  env in
              link_return ~actual ~assign_l:[res] ~name_l:[res_in_lambda] ~env)
            code.res in
        (env, actual)
      | _ -> after_call)
    | _ -> failwith "Should be an end exec block"

  let kill (lab : label) (tool : SA.analysis_tools) : t =
    let _block = SA.block_of_label lab tool in
    (Constant.empty, False)

  let lambda_return (_, _, le, lret) = (lret, le)

  let lambda_entrance (lc, li, _, _) = (lc, li)

  let ext (tool : SA.analysis_tools) : label list = [snd (SA.get_init tool)]

  let flow (tool : SA.analysis_tools) : FlowSet.t = SA.get_flow tool

  let f (tool : SA.analysis_tools) (lab : label) (k : t) : t =
    match k with
    | _, False -> k
    | _ -> gen k lab tool
end

include Perm
