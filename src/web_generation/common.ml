(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file provides functions for generating web interfaces for smart
contracts on the Tezos blockchain, including getting entrypoints, deriving
constructors, getting all constructors, and printing types and arguments in HTML
format. Warning: this short summary was automatically generated and could be
incomplete or misleading. If you feel this is the case, please open an issue. *)

open Types
open Or_type
open Infer_entrypoints
open Factori_file
open Factori_utils

let rec get_entrypoints interface =
  match interface with
  | [] -> []
  | Entrypoint (n, t) :: l' -> (n, t) :: get_entrypoints l'
  | _ :: l' -> get_entrypoints l'

let rec derive_constructor ~acc path (ort : type_ or_type) =
  match (path, ort) with
  | true :: l, OrAbs (t1, _) -> derive_constructor ~acc:("Left" :: acc) l t1
  | false :: l, OrAbs (_, t2) -> derive_constructor ~acc:("Right" :: acc) l t2
  | [], LeafOr (Annot (t, name)) ->
    (show_sanitized_name @@ bind_sanitize_capitalize name, t)
  | [], LeafOr t ->
    ( show_sanitized_name @@ sanitized_of_str @@ String.concat "_" (List.rev acc),
      t )
  | _ ->
    raise
      (Factori_errors.GenericError
         ("[derive_constructor]", EzEncoding.construct type__enc (Or ort)))

let get_all_constructor (ort : type_ or_type) =
  let l = sumtype_to_list (Or ort) in
  List.map (fun (_, path) -> derive_constructor ~acc:[] path ort) l

let print_type_base fmt b =
  match b with
  | Never -> File.write fmt "never"
  | String -> File.write fmt "string"
  | Nat -> File.write fmt "nat"
  | Int -> File.write fmt "int"
  | Byte -> File.write fmt "byte"
  | Address -> File.write fmt "address"
  | Signature -> File.write fmt "signature"
  | Unit -> File.write fmt "unit"
  | Bool -> File.write fmt "bool"
  | Timestamp -> File.write fmt "timestamp"
  | Keyhash -> File.write fmt "keyhash"
  | Key -> File.write fmt "key"
  | Mutez -> File.write fmt "mutez"
  | Operation -> Format.printf "operation"
  | Chain_id -> File.write fmt "chain_id"
  | Sapling_state -> File.write fmt "sapling_state"
  | Sapling_transaction_deprecated ->
    File.write fmt "sapling_transaction_deprecated"

let print_base fmt b =
  File.write fmt "<a-typography-text type='warning' code>%a</a-typography-text>"
    print_type_base b

let print_arg fmt name =
  match name with
  | None -> ()
  | Some s ->
    File.write fmt "<a-typography-text strong>%s</a-typography-text>" s
