from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

UpdateTokenMetadata = Tuple[factori_types.nat,factori_types.Map]

def updateTokenMetadata_generator() -> UpdateTokenMetadata:
	return (factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.map_generator(factori_types.string_generator,factori_types.bytes_generator))))()

def updateTokenMetadata_encode(arg : UpdateTokenMetadata) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.nat_encode,factori_types.map_encode(factori_types.string_encode,factori_types.bytes_encode))(arg) 
	except Exception as e:
		print('Error in updateTokenMetadata_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateTokenMetadata_decode(arg) -> UpdateTokenMetadata:
	try:
		return factori_types.tuple2_decode(factori_types.nat_decode,factori_types.map_decode(factori_types.string_decode,factori_types.bytes_decode))(arg) 
	except Exception as e:
		print('Error in updateTokenMetadata_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateTokenMetadata():
	temp=updateTokenMetadata_generator()
	temp_encoded=updateTokenMetadata_encode(temp)
	try:
		print('++++ updateTokenMetadata: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateTokenMetadata: " + str(temp))
	temp1=updateTokenMetadata_decode(temp_encoded)
	if temp1 == temp:
		print('updateTokenMetadata check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateTokenMetadata check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateTokenMetadata()
#except Exception as e:
#	print('Error in assert_identity_updateTokenMetadata' + str(e))




def callUpdateTokenMetadata(kt1 : str, _from : blockchain.identity, param : UpdateTokenMetadata, networkName = "ghostnet", debug=False):
	input = updateTokenMetadata_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateTokenMetadata", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateRewardPercent = factori_types.nat

def updateRewardPercent_generator() -> UpdateRewardPercent:
	return factori_types.nat_generator ()

def updateRewardPercent_encode (arg: UpdateRewardPercent):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in updateRewardPercent_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateRewardPercent_decode (arg) -> UpdateRewardPercent:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in updateRewardPercent_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateRewardPercent():
	temp=updateRewardPercent_generator()
	temp_encoded=updateRewardPercent_encode(temp)
	try:
		print('++++ updateRewardPercent: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateRewardPercent: " + str(temp))
	temp1=updateRewardPercent_decode(temp_encoded)
	if temp1 == temp:
		print('updateRewardPercent check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateRewardPercent check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateRewardPercent()
#except Exception as e:
#	print('Error in assert_identity_updateRewardPercent' + str(e))




def callUpdateRewardPercent(kt1 : str, _from : blockchain.identity, param : UpdateRewardPercent, networkName = "ghostnet", debug=False):
	input = updateRewardPercent_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateRewardPercent", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateQuipuswapAddress = factori_types.address

def updateQuipuswapAddress_generator() -> UpdateQuipuswapAddress:
	return factori_types.address_generator ()

def updateQuipuswapAddress_encode (arg: UpdateQuipuswapAddress):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in updateQuipuswapAddress_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateQuipuswapAddress_decode (arg) -> UpdateQuipuswapAddress:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in updateQuipuswapAddress_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateQuipuswapAddress():
	temp=updateQuipuswapAddress_generator()
	temp_encoded=updateQuipuswapAddress_encode(temp)
	try:
		print('++++ updateQuipuswapAddress: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateQuipuswapAddress: " + str(temp))
	temp1=updateQuipuswapAddress_decode(temp_encoded)
	if temp1 == temp:
		print('updateQuipuswapAddress check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateQuipuswapAddress check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateQuipuswapAddress()
#except Exception as e:
#	print('Error in assert_identity_updateQuipuswapAddress' + str(e))




def callUpdateQuipuswapAddress(kt1 : str, _from : blockchain.identity, param : UpdateQuipuswapAddress, networkName = "ghostnet", debug=False):
	input = updateQuipuswapAddress_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateQuipuswapAddress", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateOvenRegistryAddress = UpdateQuipuswapAddress


def updateOvenRegistryAddress_generator() -> UpdateOvenRegistryAddress:
	return updateQuipuswapAddress_generator ()


def updateOvenRegistryAddress_encode (arg: UpdateOvenRegistryAddress):
	try:
		return updateQuipuswapAddress_encode(arg)
	except Exception as e:
		print('Error in updateOvenRegistryAddress_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def updateOvenRegistryAddress_decode (arg) -> UpdateOvenRegistryAddress:
	try:
		return updateQuipuswapAddress_decode(arg)
	except Exception as e:
		print('Error in updateOvenRegistryAddress_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateOvenRegistryAddress():
	temp=updateOvenRegistryAddress_generator()
	temp_encoded=updateOvenRegistryAddress_encode(temp)
	try:
		print('++++ updateOvenRegistryAddress: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateOvenRegistryAddress: " + str(temp))
	temp1=updateOvenRegistryAddress_decode(temp_encoded)
	if temp1 == temp:
		print('updateOvenRegistryAddress check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateOvenRegistryAddress check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateOvenRegistryAddress()
#except Exception as e:
#	print('Error in assert_identity_updateOvenRegistryAddress' + str(e))




def callUpdateOvenRegistryAddress(kt1 : str, _from : blockchain.identity, param : UpdateOvenRegistryAddress, networkName = "ghostnet", debug=False):
	input = updateOvenRegistryAddress_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateOvenRegistryAddress", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateGovernorAddress = UpdateQuipuswapAddress


def updateGovernorAddress_generator() -> UpdateGovernorAddress:
	return updateQuipuswapAddress_generator ()


def updateGovernorAddress_encode (arg: UpdateGovernorAddress):
	try:
		return updateQuipuswapAddress_encode(arg)
	except Exception as e:
		print('Error in updateGovernorAddress_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def updateGovernorAddress_decode (arg) -> UpdateGovernorAddress:
	try:
		return updateQuipuswapAddress_decode(arg)
	except Exception as e:
		print('Error in updateGovernorAddress_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateGovernorAddress():
	temp=updateGovernorAddress_generator()
	temp_encoded=updateGovernorAddress_encode(temp)
	try:
		print('++++ updateGovernorAddress: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateGovernorAddress: " + str(temp))
	temp1=updateGovernorAddress_decode(temp_encoded)
	if temp1 == temp:
		print('updateGovernorAddress check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateGovernorAddress check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateGovernorAddress()
#except Exception as e:
#	print('Error in assert_identity_updateGovernorAddress' + str(e))




def callUpdateGovernorAddress(kt1 : str, _from : blockchain.identity, param : UpdateGovernorAddress, networkName = "ghostnet", debug=False):
	input = updateGovernorAddress_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateGovernorAddress", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateContractMetadata = Tuple[str,bytes]

def updateContractMetadata_generator() -> UpdateContractMetadata:
	return (factori_types.tuple2_generator(factori_types.string_generator,
factori_types.bytes_generator))()

def updateContractMetadata_encode(arg : UpdateContractMetadata) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.string_encode,factori_types.bytes_encode)(arg) 
	except Exception as e:
		print('Error in updateContractMetadata_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateContractMetadata_decode(arg) -> UpdateContractMetadata:
	try:
		return factori_types.tuple2_decode(factori_types.string_decode,factori_types.bytes_decode)(arg) 
	except Exception as e:
		print('Error in updateContractMetadata_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateContractMetadata():
	temp=updateContractMetadata_generator()
	temp_encoded=updateContractMetadata_encode(temp)
	try:
		print('++++ updateContractMetadata: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateContractMetadata: " + str(temp))
	temp1=updateContractMetadata_decode(temp_encoded)
	if temp1 == temp:
		print('updateContractMetadata check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateContractMetadata check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateContractMetadata()
#except Exception as e:
#	print('Error in assert_identity_updateContractMetadata' + str(e))




def callUpdateContractMetadata(kt1 : str, _from : blockchain.identity, param : UpdateContractMetadata, networkName = "ghostnet", debug=False):
	input = updateContractMetadata_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateContractMetadata", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Transfer(TypedDict):
	value: UpdateRewardPercent
	_to: UpdateQuipuswapAddress
	_from: UpdateQuipuswapAddress

def transfer_generator() -> Transfer:
	return {'value' : updateRewardPercent_generator (),'_to' : updateQuipuswapAddress_generator (),'_from' : updateQuipuswapAddress_generator ()}

def transfer_encode(arg : Transfer):
	try:
		return PairType.from_comb([(updateQuipuswapAddress_encode(arg['_from'])), PairType.from_comb([(updateQuipuswapAddress_encode(arg['_to'])), (updateRewardPercent_encode(arg['value']))])])
	except Exception as e:
		print('Error in transfer_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def transfer_decode(arg) -> Transfer:
	try:
		before_projection=(factori_types.tuple2_decode (updateQuipuswapAddress_decode,(factori_types.tuple2_decode (updateQuipuswapAddress_decode,updateRewardPercent_decode))))(arg)
		return {'_from' : (before_projection[0]),
'_to' : (before_projection[1][0]),
'value' : (before_projection[1][1])}


	except Exception as e:
		print('Error in transfer_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_transfer():
	temp=transfer_generator()
	temp_encoded=transfer_encode(temp)
	try:
		print('++++ transfer: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of transfer: " + str(temp))
	temp1=transfer_decode(temp_encoded)
	if temp1 == temp:
		print('transfer check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('transfer check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_transfer()
#except Exception as e:
#	print('Error in assert_identity_transfer' + str(e))




def callTransfer(kt1 : str, _from : blockchain.identity, param : Transfer, networkName = "ghostnet", debug=False):
	input = transfer_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "transfer", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Redeem_callback = UpdateRewardPercent


def redeem_callback_generator() -> Redeem_callback:
	return updateRewardPercent_generator ()


def redeem_callback_encode (arg: Redeem_callback):
	try:
		return updateRewardPercent_encode(arg)
	except Exception as e:
		print('Error in redeem_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def redeem_callback_decode (arg) -> Redeem_callback:
	try:
		return updateRewardPercent_decode(arg)
	except Exception as e:
		print('Error in redeem_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_redeem_callback():
	temp=redeem_callback_generator()
	temp_encoded=redeem_callback_encode(temp)
	try:
		print('++++ redeem_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of redeem_callback: " + str(temp))
	temp1=redeem_callback_decode(temp_encoded)
	if temp1 == temp:
		print('redeem_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('redeem_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_redeem_callback()
#except Exception as e:
#	print('Error in assert_identity_redeem_callback' + str(e))




def callRedeem_callback(kt1 : str, _from : blockchain.identity, param : Redeem_callback, networkName = "ghostnet", debug=False):
	input = redeem_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "redeem_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Redeem = UpdateRewardPercent


def redeem_generator() -> Redeem:
	return updateRewardPercent_generator ()


def redeem_encode (arg: Redeem):
	try:
		return updateRewardPercent_encode(arg)
	except Exception as e:
		print('Error in redeem_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def redeem_decode (arg) -> Redeem:
	try:
		return updateRewardPercent_decode(arg)
	except Exception as e:
		print('Error in redeem_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_redeem():
	temp=redeem_generator()
	temp_encoded=redeem_encode(temp)
	try:
		print('++++ redeem: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of redeem: " + str(temp))
	temp1=redeem_decode(temp_encoded)
	if temp1 == temp:
		print('redeem check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('redeem check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_redeem()
#except Exception as e:
#	print('Error in assert_identity_redeem' + str(e))




def callRedeem(kt1 : str, _from : blockchain.identity, param : Redeem, networkName = "ghostnet", debug=False):
	input = redeem_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "redeem", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Mint(TypedDict):
	address: UpdateQuipuswapAddress
	value: UpdateRewardPercent

def mint_generator() -> Mint:
	return {'address' : updateQuipuswapAddress_generator (),'value' : updateRewardPercent_generator ()}

def mint_encode(arg : Mint):
	try:
		return PairType.from_comb([(updateQuipuswapAddress_encode(arg['address'])), (updateRewardPercent_encode(arg['value']))])
	except Exception as e:
		print('Error in mint_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def mint_decode(arg) -> Mint:
	try:
		before_projection=(factori_types.tuple2_decode (updateQuipuswapAddress_decode,updateRewardPercent_decode))(arg)
		return {'address' : (before_projection[0]),
'value' : (before_projection[1])}


	except Exception as e:
		print('Error in mint_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_mint():
	temp=mint_generator()
	temp_encoded=mint_encode(temp)
	try:
		print('++++ mint: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of mint: " + str(temp))
	temp1=mint_decode(temp_encoded)
	if temp1 == temp:
		print('mint check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('mint check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_mint()
#except Exception as e:
#	print('Error in assert_identity_mint' + str(e))




def callMint(kt1 : str, _from : blockchain.identity, param : Mint, networkName = "ghostnet", debug=False):
	input = mint_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "mint", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Liquidate = UpdateQuipuswapAddress


def liquidate_generator() -> Liquidate:
	return updateQuipuswapAddress_generator ()


def liquidate_encode (arg: Liquidate):
	try:
		return updateQuipuswapAddress_encode(arg)
	except Exception as e:
		print('Error in liquidate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def liquidate_decode (arg) -> Liquidate:
	try:
		return updateQuipuswapAddress_decode(arg)
	except Exception as e:
		print('Error in liquidate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_liquidate():
	temp=liquidate_generator()
	temp_encoded=liquidate_encode(temp)
	try:
		print('++++ liquidate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of liquidate: " + str(temp))
	temp1=liquidate_decode(temp_encoded)
	if temp1 == temp:
		print('liquidate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('liquidate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_liquidate()
#except Exception as e:
#	print('Error in assert_identity_liquidate' + str(e))




def callLiquidate(kt1 : str, _from : blockchain.identity, param : Liquidate, networkName = "ghostnet", debug=False):
	input = liquidate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "liquidate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetTotalSupply = Tuple[factori_types.unit,factori_types.contract]

def getTotalSupply_generator() -> GetTotalSupply:
	return (factori_types.tuple2_generator(factori_types.unit_generator,
factori_types.contract_generator))()

def getTotalSupply_encode(arg : GetTotalSupply) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.unit_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getTotalSupply_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getTotalSupply_decode(arg) -> GetTotalSupply:
	try:
		return factori_types.tuple2_decode(factori_types.unit_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getTotalSupply_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getTotalSupply():
	temp=getTotalSupply_generator()
	temp_encoded=getTotalSupply_encode(temp)
	try:
		print('++++ getTotalSupply: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getTotalSupply: " + str(temp))
	temp1=getTotalSupply_decode(temp_encoded)
	if temp1 == temp:
		print('getTotalSupply check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getTotalSupply check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getTotalSupply()
#except Exception as e:
#	print('Error in assert_identity_getTotalSupply' + str(e))




def callGetTotalSupply(kt1 : str, _from : blockchain.identity, param : GetTotalSupply, networkName = "ghostnet", debug=False):
	input = getTotalSupply_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getTotalSupply", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetBalance = Tuple[UpdateQuipuswapAddress,factori_types.contract]

def getBalance_generator() -> GetBalance:
	return (factori_types.tuple2_generator(updateQuipuswapAddress_generator,
factori_types.contract_generator))()

def getBalance_encode(arg : GetBalance) -> PairType:
	try:
		return factori_types.tuple2_encode(updateQuipuswapAddress_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getBalance_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getBalance_decode(arg) -> GetBalance:
	try:
		return factori_types.tuple2_decode(updateQuipuswapAddress_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getBalance_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getBalance():
	temp=getBalance_generator()
	temp_encoded=getBalance_encode(temp)
	try:
		print('++++ getBalance: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getBalance: " + str(temp))
	temp1=getBalance_decode(temp_encoded)
	if temp1 == temp:
		print('getBalance check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getBalance check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getBalance()
#except Exception as e:
#	print('Error in assert_identity_getBalance' + str(e))




def callGetBalance(kt1 : str, _from : blockchain.identity, param : GetBalance, networkName = "ghostnet", debug=False):
	input = getBalance_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getBalance", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Type4(TypedDict):
	owner: UpdateQuipuswapAddress
	spender: UpdateQuipuswapAddress

def type4_generator() -> Type4:
	return {'owner' : updateQuipuswapAddress_generator (),'spender' : updateQuipuswapAddress_generator ()}

def type4_encode(arg : Type4):
	try:
		return PairType.from_comb([(updateQuipuswapAddress_encode(arg['owner'])), (updateQuipuswapAddress_encode(arg['spender']))])
	except Exception as e:
		print('Error in type4_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def type4_decode(arg) -> Type4:
	try:
		before_projection=(factori_types.tuple2_decode (updateQuipuswapAddress_decode,updateQuipuswapAddress_decode))(arg)
		return {'owner' : (before_projection[0]),
'spender' : (before_projection[1])}


	except Exception as e:
		print('Error in type4_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_type4():
	temp=type4_generator()
	temp_encoded=type4_encode(temp)
	try:
		print('++++ type4: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of type4: " + str(temp))
	temp1=type4_decode(temp_encoded)
	if temp1 == temp:
		print('type4 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('type4 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_type4()
#except Exception as e:
#	print('Error in assert_identity_type4' + str(e))




GetAllowance = Tuple[Type4,factori_types.contract]

def getAllowance_generator() -> GetAllowance:
	return (factori_types.tuple2_generator(type4_generator,
factori_types.contract_generator))()

def getAllowance_encode(arg : GetAllowance) -> PairType:
	try:
		return factori_types.tuple2_encode(type4_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getAllowance_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getAllowance_decode(arg) -> GetAllowance:
	try:
		return factori_types.tuple2_decode(type4_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getAllowance_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getAllowance():
	temp=getAllowance_generator()
	temp_encoded=getAllowance_encode(temp)
	try:
		print('++++ getAllowance: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getAllowance: " + str(temp))
	temp1=getAllowance_decode(temp_encoded)
	if temp1 == temp:
		print('getAllowance check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getAllowance check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getAllowance()
#except Exception as e:
#	print('Error in assert_identity_getAllowance' + str(e))




def callGetAllowance(kt1 : str, _from : blockchain.identity, param : GetAllowance, networkName = "ghostnet", debug=False):
	input = getAllowance_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getAllowance", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Deposit_callback = UpdateRewardPercent


def deposit_callback_generator() -> Deposit_callback:
	return updateRewardPercent_generator ()


def deposit_callback_encode (arg: Deposit_callback):
	try:
		return updateRewardPercent_encode(arg)
	except Exception as e:
		print('Error in deposit_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def deposit_callback_decode (arg) -> Deposit_callback:
	try:
		return updateRewardPercent_decode(arg)
	except Exception as e:
		print('Error in deposit_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_deposit_callback():
	temp=deposit_callback_generator()
	temp_encoded=deposit_callback_encode(temp)
	try:
		print('++++ deposit_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of deposit_callback: " + str(temp))
	temp1=deposit_callback_decode(temp_encoded)
	if temp1 == temp:
		print('deposit_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('deposit_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_deposit_callback()
#except Exception as e:
#	print('Error in assert_identity_deposit_callback' + str(e))




def callDeposit_callback(kt1 : str, _from : blockchain.identity, param : Deposit_callback, networkName = "ghostnet", debug=False):
	input = deposit_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "deposit_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Deposit = UpdateRewardPercent


def deposit_generator() -> Deposit:
	return updateRewardPercent_generator ()


def deposit_encode (arg: Deposit):
	try:
		return updateRewardPercent_encode(arg)
	except Exception as e:
		print('Error in deposit_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def deposit_decode (arg) -> Deposit:
	try:
		return updateRewardPercent_decode(arg)
	except Exception as e:
		print('Error in deposit_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_deposit():
	temp=deposit_generator()
	temp_encoded=deposit_encode(temp)
	try:
		print('++++ deposit: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of deposit: " + str(temp))
	temp1=deposit_decode(temp_encoded)
	if temp1 == temp:
		print('deposit check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('deposit check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_deposit()
#except Exception as e:
#	print('Error in assert_identity_deposit' + str(e))




def callDeposit(kt1 : str, _from : blockchain.identity, param : Deposit, networkName = "ghostnet", debug=False):
	input = deposit_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "deposit", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = factori_types.unit

def _default_generator() -> Constructor_default:
	return factori_types.unit_generator ()

def _default_encode (arg: Constructor_default):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _default_decode (arg) -> Constructor_default:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Burn(Mint):
	pass


def burn_generator() -> Burn:
	return mint_generator ()


def burn_encode (arg: Burn):
	try:
		return mint_encode(arg)
	except Exception as e:
		print('Error in burn_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def burn_decode (arg) -> Burn:
	try:
		return mint_decode(arg)
	except Exception as e:
		print('Error in burn_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_burn():
	temp=burn_generator()
	temp_encoded=burn_encode(temp)
	try:
		print('++++ burn: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of burn: " + str(temp))
	temp1=burn_decode(temp_encoded)
	if temp1 == temp:
		print('burn check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('burn check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_burn()
#except Exception as e:
#	print('Error in assert_identity_burn' + str(e))




def callBurn(kt1 : str, _from : blockchain.identity, param : Burn, networkName = "ghostnet", debug=False):
	input = burn_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "burn", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Approve(TypedDict):
	spender: UpdateQuipuswapAddress
	value: UpdateRewardPercent

def approve_generator() -> Approve:
	return {'spender' : updateQuipuswapAddress_generator (),'value' : updateRewardPercent_generator ()}

def approve_encode(arg : Approve):
	try:
		return PairType.from_comb([(updateQuipuswapAddress_encode(arg['spender'])), (updateRewardPercent_encode(arg['value']))])
	except Exception as e:
		print('Error in approve_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def approve_decode(arg) -> Approve:
	try:
		before_projection=(factori_types.tuple2_decode (updateQuipuswapAddress_decode,updateRewardPercent_decode))(arg)
		return {'spender' : (before_projection[0]),
'value' : (before_projection[1])}


	except Exception as e:
		print('Error in approve_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_approve():
	temp=approve_generator()
	temp_encoded=approve_encode(temp)
	try:
		print('++++ approve: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of approve: " + str(temp))
	temp1=approve_decode(temp_encoded)
	if temp1 == temp:
		print('approve check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('approve check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_approve()
#except Exception as e:
#	print('Error in assert_identity_approve' + str(e))




def callApprove(kt1 : str, _from : blockchain.identity, param : Approve, networkName = "ghostnet", debug=False):
	input = approve_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "approve", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

State = factori_types.int_michelson

def state_generator() -> State:
	return factori_types.int_generator ()

def state_encode (arg: State):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in state_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def state_decode (arg) -> State:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in state_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_state():
	temp=state_generator()
	temp_encoded=state_encode(temp)
	try:
		print('++++ state: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of state: " + str(temp))
	temp1=state_decode(temp_encoded)
	if temp1 == temp:
		print('state check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('state check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_state()
#except Exception as e:
#	print('Error in assert_identity_state' + str(e))




class Type7(TypedDict):
	approvals: factori_types.Map
	balance: UpdateRewardPercent

def type7_generator() -> Type7:
	return {'approvals' : (factori_types.map_generator(updateQuipuswapAddress_generator,updateRewardPercent_generator)) (),'balance' : updateRewardPercent_generator ()}

def type7_encode(arg : Type7):
	try:
		return PairType.from_comb([(factori_types.map_encode(updateQuipuswapAddress_encode,updateRewardPercent_encode)(arg['approvals'])), (updateRewardPercent_encode(arg['balance']))])
	except Exception as e:
		print('Error in type7_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def type7_decode(arg) -> Type7:
	try:
		before_projection=(factori_types.tuple2_decode (factori_types.map_decode(updateQuipuswapAddress_decode,updateRewardPercent_decode),updateRewardPercent_decode))(arg)
		return {'approvals' : (before_projection[0]),
'balance' : (before_projection[1])}


	except Exception as e:
		print('Error in type7_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_type7():
	temp=type7_generator()
	temp_encoded=type7_encode(temp)
	try:
		print('++++ type7: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of type7: " + str(temp))
	temp1=type7_decode(temp_encoded)
	if temp1 == temp:
		print('type7 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('type7 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_type7()
#except Exception as e:
#	print('Error in assert_identity_type7' + str(e))




class Constructor_storage(TypedDict):
	totalSupply: UpdateRewardPercent
	token_metadata: factori_types.BigMap
	savedState_redeemer: Optional[UpdateQuipuswapAddress]
	savedState_depositor: Optional[UpdateQuipuswapAddress]
	quipuswapAddress: UpdateQuipuswapAddress
	ovenRegistryAddress: UpdateQuipuswapAddress
	metadata: factori_types.BigMap
	governorAddress: UpdateQuipuswapAddress
	balances: factori_types.BigMap
	rewardChangeAllowedLevel: UpdateRewardPercent
	rewardPercent: UpdateRewardPercent
	savedState_tokensToDeposit: Optional[UpdateRewardPercent]
	savedState_tokensToRedeem: Optional[UpdateRewardPercent]
	state: State
	tokenAddress: UpdateQuipuswapAddress

def _storage_generator() -> Constructor_storage:
	return {'totalSupply' : updateRewardPercent_generator (),'token_metadata' : (factori_types.big_map_generator(updateRewardPercent_generator,(factori_types.tuple2_generator(updateRewardPercent_generator,
(factori_types.map_generator(factori_types.string_generator,factori_types.bytes_generator)))))) (),'savedState_redeemer' : (factori_types.option_generator(updateQuipuswapAddress_generator)) (),'savedState_depositor' : (factori_types.option_generator(updateQuipuswapAddress_generator)) (),'quipuswapAddress' : updateQuipuswapAddress_generator (),'ovenRegistryAddress' : updateQuipuswapAddress_generator (),'metadata' : (factori_types.big_map_generator(factori_types.string_generator,factori_types.bytes_generator)) (),'governorAddress' : updateQuipuswapAddress_generator (),'balances' : (factori_types.big_map_generator(updateQuipuswapAddress_generator,type7_generator)) (),'rewardChangeAllowedLevel' : updateRewardPercent_generator (),'rewardPercent' : updateRewardPercent_generator (),'savedState_tokensToDeposit' : (factori_types.option_generator(updateRewardPercent_generator)) (),'savedState_tokensToRedeem' : (factori_types.option_generator(updateRewardPercent_generator)) (),'state' : state_generator (),'tokenAddress' : updateQuipuswapAddress_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([PairType.from_comb([(factori_types.big_map_encode(updateQuipuswapAddress_encode,type7_encode)(arg['balances'])), PairType.from_comb([(updateQuipuswapAddress_encode(arg['governorAddress'])), (factori_types.big_map_encode(factori_types.string_encode,factori_types.bytes_encode)(arg['metadata']))])]), PairType.from_comb([PairType.from_comb([(updateQuipuswapAddress_encode(arg['ovenRegistryAddress'])), (updateQuipuswapAddress_encode(arg['quipuswapAddress']))]), PairType.from_comb([(updateRewardPercent_encode(arg['rewardChangeAllowedLevel'])), (updateRewardPercent_encode(arg['rewardPercent']))])])]), PairType.from_comb([PairType.from_comb([PairType.from_comb([(factori_types.option_encode(updateQuipuswapAddress_encode)(arg['savedState_depositor'])), (factori_types.option_encode(updateQuipuswapAddress_encode)(arg['savedState_redeemer']))]), PairType.from_comb([(factori_types.option_encode(updateRewardPercent_encode)(arg['savedState_tokensToDeposit'])), (factori_types.option_encode(updateRewardPercent_encode)(arg['savedState_tokensToRedeem']))])]), PairType.from_comb([PairType.from_comb([(state_encode(arg['state'])), (updateQuipuswapAddress_encode(arg['tokenAddress']))]), PairType.from_comb([(factori_types.big_map_encode(updateRewardPercent_encode,factori_types.tuple2_encode(updateRewardPercent_encode,factori_types.map_encode(factori_types.string_encode,factori_types.bytes_encode)))(arg['token_metadata'])), (updateRewardPercent_encode(arg['totalSupply']))])])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.big_map_decode(updateQuipuswapAddress_decode,type7_decode),(factori_types.tuple2_decode (updateQuipuswapAddress_decode,factori_types.big_map_decode(factori_types.string_decode,factori_types.bytes_decode))))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (updateQuipuswapAddress_decode,updateQuipuswapAddress_decode)),(factori_types.tuple2_decode (updateRewardPercent_decode,updateRewardPercent_decode)))))),(factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.option_decode(updateQuipuswapAddress_decode),factori_types.option_decode(updateQuipuswapAddress_decode))),(factori_types.tuple2_decode (factori_types.option_decode(updateRewardPercent_decode),factori_types.option_decode(updateRewardPercent_decode))))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (state_decode,updateQuipuswapAddress_decode)),(factori_types.tuple2_decode (factori_types.big_map_decode(updateRewardPercent_decode,factori_types.tuple2_decode(updateRewardPercent_decode,factori_types.map_decode(factori_types.string_decode,factori_types.bytes_decode))),updateRewardPercent_decode))))))))(arg)
		return {'balances' : (before_projection[0][0][0]),
'governorAddress' : (before_projection[0][0][1][0]),
'metadata' : (before_projection[0][0][1][1]),
'ovenRegistryAddress' : (before_projection[0][1][0][0]),
'quipuswapAddress' : (before_projection[0][1][0][1]),
'rewardChangeAllowedLevel' : (before_projection[0][1][1][0]),
'rewardPercent' : (before_projection[0][1][1][1]),
'savedState_depositor' : (before_projection[1][0][0][0]),
'savedState_redeemer' : (before_projection[1][0][0][1]),
'savedState_tokensToDeposit' : (before_projection[1][0][1][0]),
'savedState_tokensToRedeem' : (before_projection[1][0][1][1]),
'state' : (before_projection[1][1][0][0]),
'tokenAddress' : (before_projection[1][1][0][1]),
'token_metadata' : (before_projection[1][1][1][0]),
'totalSupply' : (before_projection[1][1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract oven_pool")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/oven_pool_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c

initial_blockchain_storage=dict([("balances",factori_types.BigMap(int(1600))),
("governorAddress","KT1FC288PPN87gtmTKRuHVUhoJ1Z8T4KBUAD"),
("metadata",factori_types.BigMap(int(1601))),
("ovenRegistryAddress","KT1Ldn1XWQmk7J4pYgGFjjwV57Ew8NYvcNtJ"),
("quipuswapAddress","tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"),
("rewardChangeAllowedLevel",1522673),
("rewardPercent",10),
("savedState_depositor",None),
("savedState_redeemer",None),
("savedState_tokensToDeposit",None),
("savedState_tokensToRedeem",None),
("state",0),
("tokenAddress","KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV"),
("token_metadata",factori_types.BigMap(int(1602))),
("totalSupply",22256683567651269911333617468703311423250)])#{"prim":"Pair","args":[{"prim":"Pair","args":[{"prim":"Pair","args":[{"int":"1600"},{"string":"KT1FC288PPN87gtmTKRuHVUhoJ1Z8T4KBUAD"},{"int":"1601"}]},{"prim":"Pair","args":[{"string":"KT1Ldn1XWQmk7J4pYgGFjjwV57Ew8NYvcNtJ"},{"string":"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU"}]},{"int":"1522673"},{"int":"10"}]},{"prim":"Pair","args":[{"prim":"Pair","args":[{"prim":"None"},{"prim":"None"}]},{"prim":"None"},{"prim":"None"}]},{"prim":"Pair","args":[{"int":"0"},{"string":"KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV"}]},{"int":"1602"},{"int":"22256683567651269911333617468703311423250"}]}

