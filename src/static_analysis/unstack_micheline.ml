(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file defines data types and functions related to control flow
graphs and nodes in the Michelson language, which is used on the Tezos
blockchain. The file includes types such as Label, Args, control_node, node,
Name_arg, Stack, v_node, sc_node, lambdas, complete_lambda, and block_unstack,
as well as a function called string_of_micheline that converts Micheline
expressions to strings. Warning: this short summary was automatically generated
and could be incomplete or misleading. If you feel this is the case, please open
an issue. *)

open Tzfunc.Proto
open Factori_representation

type label = int

type args = micheline list

type name_arg = Name_arg.t
(* for node 'a is a representation of the variable *)

type 'a control_node =
  | N_IF of 'a * label * label * 'a node list * 'a node list
  | N_IF_NONE of 'a * label * label * 'a node list * 'a node list * 'a * label
  | N_IF_CONS of
      'a * label * label * 'a node list * 'a node list * 'a * label * 'a * label
  | N_IF_LEFT of
      'a
      * label
      * label
      * ('a * label * 'a node list)
      * ('a * label * 'a node list)
  | N_ITER of {
      arg_iter : 'a;
      acc_iter : 'a list;
      pat_acc_iter : 'a list;
      pat : 'a * label;
      body_iter : 'a node list;
      label_iter : label;
    }
  | N_LOOP of {
      arg : 'a;
      acc : 'a list;
      pat_acc : 'a list;
      body : 'a node list;
      label_pat_l : label;
      label_loop : label;
    }
  | N_LOOP_LEFT of {
      arg_lf : 'a;
      pat_lf : 'a * label;
      acc_lf : 'a list;
      pat_acc_lf : 'a list;
      node_loop : 'a node list;
      pat_res_lf : 'a * label;
      label_lf : label;
    }
  | N_MAP of 'a * label * 'a * 'a node list * label
  | N_FOLD_MAP of {
      arg_fold_map : 'a;
      acc_fold_map : 'a list;
      pat_acc_fold_map : 'a list * label;
      pat_fold_map : 'a * label;
      body_fold_map : 'a node list;
      label_map : label;
    }

and 'a node =
  | N_SIMPLE of primitive_or_macro * args * 'a list * 'a list * label
  | N_EXEC of 'a * 'a * 'a * label * label
  | N_START of 'a list * 'a control_node * label
  | FAIL_CONTROL of 'a control_node * label
  | N_END of 'a list * label
  | N_LAMBDA of 'a * label * 'a node list * 'a * label * label (* TODO *)
  | N_CONCAT of 'a * 'a * 'a * label (* MULTIPLE DEF *)
  | N_CONCAT_LIST of 'a * 'a * label
  | N_SELF of string option * 'a * label
  | N_CONTRACT of string option * 'a * 'a * label
  | N_CREATE_CONTRACT of 'a * 'a * 'a * 'a sc_node * 'a * 'a * label
  | N_NEVER of 'a * label
  | N_FAILWITH of 'a * label
  | N_CALL_VIEW of string * 'a * 'a * 'a * label

and 'a v_node = {
  node_name : string;
  view_label : label;
  view_stack : 'a;
  view_parameter : micheline;
  view_result : micheline;
  node_code_v : 'a node list;
}

and 'a sc_node = {
  begin_label : label;
  begin_stack : 'a;
  parameter : micheline;
  storage : micheline;
  node_l : 'a node list;
  views_node : 'a v_node list;
}

type stack = name_arg list

type 'a lambdas = {
  arg : 'a;
  lcode : 'a node list;
  label_start : label;
  label_end : label;
  res : 'a option;
}

type 'a complete_lambda = {
  lambda_name : 'a;
  lambda_code : 'a lambdas option;
  in_between : 'a list;
}

type 'a block_unstack =
  | BAssign of 'a node * label
  | BAnnoAssign of 'a list * label
  | BReturnAssign of 'a list * 'a list * label
  | BThen of 'a * label
  | BElse of 'a * label
  | BLoop of 'a * 'a list * 'a list * label
  | BStartExec of 'a (* arg *) * 'a * label
  | BEndExec of 'a (* res *) * 'a * label
  (*| BLoopL of 'a * 'a list * 'a list * label*)
  | BPattern of 'a list * 'a list * label
  | BIs of label
  | BEnd of label
  | BAnnonymous of 'a list * 'a list * label
  | BEndLambda of label
