(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* OCaml *)

open Factori_config
open Factori_utils
open Format

let main_deploy ~contract_name =
  Format.sprintf "deploy_%s" (show_sanitized_name contract_name)

module Generate = struct
  (* The ocaml file that will deploy the KT1 by using the generated interface *)

  let blockchain_ocaml_storage = sprintf "initial_blockchain_storage"

  (* TODO: get storage_name in case storage isn't called storage *)
  let random_ocaml_storage = sprintf "(storage_generator ())"

  let storage_ocaml ~storage_type =
    match storage_type with
    | Random -> random_ocaml_storage
    | Blockchain -> blockchain_ocaml_storage

  let deploy_ocaml ~contract_name ~network ~from ~storage_type ppf () =
    let get_network_node network = sprintf "Blockchain.%s_node" network in
    let get_from from = sprintf "Blockchain.%s_flextesa" from in
    fprintf ppf
      {|
    open %s_ocaml_interface
    open Tzfunc.Rp
|}
      (String.capitalize_ascii contract_name) ;

    fprintf ppf
      {|let deploy () =
       let>? kt1,op =
       deploy
         ~node:%s
         ~name:"%s"
         ~from:%s
         ~amount:100000L
         %s in
       Format.printf "Deploy %s contract at %%s on %s" kt1;
       Lwt.return_ok (kt1,op)
     |}
      (get_network_node network) contract_name (get_from from)
      (storage_ocaml ~storage_type)
      contract_name network

  let scenario_ocaml module_name contract_name ppf () =
    Format.fprintf ppf
      {|
   open %s
    open Tzfunc.Rp
    let _ = Tzfunc.Node.set_silent true
   let deploy_%s () =
       let>? _kt1,_op_hash =
           %s.deploy ()
       in
       Lwt.return_ok ()
   
   let _ =
      Lwt_main.run (deploy_%s ())@.                
|}
      (String.capitalize_ascii module_name)
      contract_name
      (String.capitalize_ascii contract_name)
      contract_name
end

module Dune_helper = struct
  type file =
    | Deploy
    | Scenario

  let dune_file_deploy ~stanza_name contract_name =
    let contract = get_contract contract_name in
    Artifacts.deploy_dunefile ~stanza_name [contract]

  let dune_file_scenarios ~stanza_name contract_name =
    ignore stanza_name ;
    let module_name = main_deploy ~contract_name in
    Artifacts.scenario_deploy_file ~module_name stanza_name

  let dune_stanza_scenarios ~stanza_name contract_name =
    ignore stanza_name ;
    let module_name = main_deploy ~contract_name in
    Artifacts.scenarios_deploy_stanza ~module_name stanza_name

  let add_library =
    Ocaml_dune.add_modules_to_stanza_in_dune_file ?file_must_exist:None
      ?stanza_must_exist:None

  let add_modules ~path ~stanza_name contract_name =
    let contract_name = sanitized_of_str contract_name in
    Ocaml_dune.add_stanza_to_dune_file ?file_must_exist:None
      ?stanza_must_not_exist:None ~path
      (dune_stanza_scenarios ~stanza_name contract_name)

  let write_dune build_dune add_stanza ~stanza_name ~dune_path contract_name =
    if not (Sys.file_exists dune_path) then
      Ocaml_dune.write_dune_file ~with_readable_backup:true
        ~dune_file:(build_dune ~stanza_name (sanitized_of_str contract_name))
        ~path:dune_path ()
    else
      add_stanza ~path:dune_path ~stanza_name contract_name

  let generate ~dir ~stanza_name contract_name file =
    match file with
    | Deploy ->
      let dune_path = OCaml_SDK.deploy_dir ~dir () // "dune" in
      write_dune dune_file_deploy add_library ~stanza_name ~dune_path
        contract_name
    | Scenario ->
      let dune_path = OCaml_SDK.scenarios_dir ~dir () // "dune" in
      write_dune dune_file_scenarios add_modules ~stanza_name ~dune_path
        contract_name

  let generate_scenario ~dir ~stanza_name contract_name =
    generate ~dir ~stanza_name contract_name Scenario

  let generate_deploy ~dir ~stanza_name contract_name =
    generate ~dir ~stanza_name contract_name Deploy
end

let deploy ~storage_type ~contract_name ~network ~from () =
  let dir = get_dir () in
  let stanza_name = OCaml_SDK.deploy_dir_name in
  let ocaml_scenario_dir = OCaml_SDK.scenarios_dir ~dir () in
  let ocaml_deploy_dir = OCaml_SDK.deploy_dir ~dir () in
  make_dir ocaml_deploy_dir ;
  let deploy_name =
    main_deploy ~contract_name:(sanitized_of_str contract_name) in
  let deploy_file =
    asprintf "%a"
      (Generate.deploy_ocaml ~network ~from ~contract_name ~storage_type)
      () in
  let scenario_file =
    asprintf "%a" (Generate.scenario_ocaml stanza_name contract_name) () in
  (* Write the dune file allowing for the scenario to be seen by the compiler *)
  Dune_helper.generate_deploy ~dir ~stanza_name contract_name ;
  Dune_helper.generate_scenario ~dir ~stanza_name contract_name ;
  (* Write the actual scenario file *)
  let deploy_path = ocaml_deploy_dir // sprintf "%s.ml" contract_name in
  write_file ~filename:deploy_path deploy_file ;
  let scenario_path = ocaml_scenario_dir // sprintf "%s.ml" deploy_name in
  write_file ~filename:scenario_path scenario_file ;
  sprintf "cd %s && make ocaml && dune exec ./%s/%s.exe" dir
    (OCaml_SDK.scenarios_dir ~relative:true ~dir ())
    deploy_name
