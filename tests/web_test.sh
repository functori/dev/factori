#!/bin/bash

NBJOBS=26

CONTRACTS=(
  "KT1CpMKxKFoHvS7zxSd2M6SHU8BD4gBmtw2N dogami_NFT_sale mainnet"
  "KT1NVvPsNDChrLRH5K2cy6Sc9r1uuUwdiZQd dogami_NFT mainnet"
  "KT1NrAnSjrjp3Bycaw8YKPrDq2aXUTHhq8MF hic_et_nunc_DAO mainnet"
  "KT1KVfQzyjxoKx2WjcVAatknW3YsR37cCA6s arago mainnet"
  "KT1TwzD6zV3WeJ39ukuqxcfK2fJCnhvrdN1X smlk mainnet"
  "KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b plenty mainnet"
  "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb quipuswap mainnet"
  "KT1Su3fNrnABFSYMtWVDZnMbH3DzJeysAd6t fILMCrew_NFT mainnet"
  "KT1ERjU9W6jzZkq8v6caxu65pQkamv1ptn35 kT1ERjUtn35 mainnet"
  "KT1Ha4yFVeyzw6KRAdkzq6TxDHB97KG4pZe8 some_fa12 mainnet"
  "KT1D6XTy8oAHkUWdzuQrzySECCDMnANEchQq juster mainnet"
  "KT1Lz5S39TMHEA7izhQn8Z1mQoddm6v1jTwH youves mainnet"
  "KT1Xobej4mc6XgEjDoJoHtTKgbD1ELMvcQuL youves_bis mainnet"
  "KT1VG2WtYdSWz5E7chTeAdDPZNy2MpP8pTfL atomex mainnet"
  "KT1Ap287P1NzsnToSJdA4aqSNjPomRaHBZSr atomex_tzBTC mainnet"
  "KT1EpQVwqLGSH7vMCWKJnq6Uxi851sEDbhWL atomex_kUSD mainnet"
  "KT18pVpRXKPY2c4U2yFEGSH3ZnhB2kL8kwXS rarible mainnet"
  "KT1HSYQ9NLTQufuvNUwMhLY7B9TX8LDUfgsr plenty-farm-std mainnet"
  "KT1CSZ4ywhbUZwuXKMw3udoYSbisQv44sxsB chain_id_example mainnet"
  "KT19h3TpAWUKtrbdSw2MhQXYj95nnxwwLsWG ticket_example mainnet"
  "KT1HXXYrbhyY76afTGJ75UKts8C6j7P6tqXi byte_example mainnet"
  "KT19SUzs8XMJnVx6XrMrYiLRvFnSspgRf1Vt decode_error_example mainnet"
  "KT1EQLxRRhgwPd9GRisfE3rrXD24P12WBLNb unbound_storage_error mainnet"
  "KT1JMi5Jg3GX237rgpcsvCNftHtMJpwxuASD escape_bytes_example mainnet"
  "KT1Grni14amJ2K6FkmwGznSXqfjckgUBSJ4A underscore_in_constructor_names mainnet"
  "KT1KD9oNXFJHC8auPit74XwYEY89GxzBcJgj set_error mainnet"
  "KT1VdCrmZsQfuYgbQsezAHT1pXvs6zKF8xHB endlesswaysfa2_sumtype mainnet"
)
function call_factori() {
  KT1=$1
  NAME=$2
  NETWORK=$3
  factori import kt1 testweb $KT1 --force --network=$NETWORK --name=$NAME --project_name=web_test --typescript --web -q
}

export -f call_factori

printf "%s\n" "${CONTRACTS[@]}" | xargs -P $NBJOBS -I {} bash -c "call_factori {}"
