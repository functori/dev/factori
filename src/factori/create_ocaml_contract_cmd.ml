(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines a subcommand for the `Factori` application to create
a new OCaml contract using `mligo`. It provides a function
`create_ocaml_contract` that takes the directory and contract name as arguments
and creates a new OCaml contract in the specified directory. An example is given
to show how to add the `dir` and `contract_name` arguments using
`Factori_command.add_arg`. Warning: this short summary was automatically
generated and could be incomplete or misleading. If you feel this is the case,
please open an issue. *)

open Factori_command
open Options
open Factori_errors

let ( // ) = Filename.concat

(** This helps the user create and develop a contract using
   https://gitlab.com/functori/dev/mligo. This feature will probably
   be moved to a separate package eventually, as Factori aims to
   manipulate Michelson contracts. *)
let create_ocaml_contract =
  subcommand ~name:"create ocaml contract"
    ~args:[dir_anonymous_option; contract_name_option]
    ~doc:
      "Create a new OCaml contract using \
       https://gitlab.com/functori/dev/mligo. It will be compiled to mligo and \
       then to Michelson." (fun () ->
      try
        match !contract_name with
        | None -> raise NoContractNameProvided
        | Some contract_name ->
          File_structure.create_ml_contract ~dir:!dir ~overwrite:!overwrite
            ~contract_name
      with e -> handle_exception e)
