from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

SetGovernorContract = factori_types.address

def setGovernorContract_generator() -> SetGovernorContract:
	return factori_types.address_generator ()

def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetDelegate = Optional[factori_types.key_hash]

def setDelegate_generator() -> SetDelegate:
	return (factori_types.option_generator (factori_types.key_hash_generator)) ()

def setDelegate_encode (arg: SetDelegate):
	try:
		return factori_types.option_encode(factori_types.key_hash_encode)(arg)
	except Exception as e:
		print('Error in setDelegate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setDelegate_decode (arg) -> SetDelegate:
	try:
		return factori_types.option_decode(factori_types.key_hash_decode)(arg)
	except Exception as e:
		print('Error in setDelegate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setDelegate():
	temp=setDelegate_generator()
	temp_encoded=setDelegate_encode(temp)
	try:
		print('++++ setDelegate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setDelegate: " + str(temp))
	temp1=setDelegate_decode(temp_encoded)
	if temp1 == temp:
		print('setDelegate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setDelegate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setDelegate()
#except Exception as e:
#	print('Error in assert_identity_setDelegate' + str(e))




def callSetDelegate(kt1 : str, _from : blockchain.identity, param : SetDelegate, networkName = "ghostnet", debug=False):
	input = setDelegate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setDelegate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetAdministratorContract = SetGovernorContract


def setAdministratorContract_generator() -> SetAdministratorContract:
	return setGovernorContract_generator ()


def setAdministratorContract_encode (arg: SetAdministratorContract):
	try:
		return setGovernorContract_encode(arg)
	except Exception as e:
		print('Error in setAdministratorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setAdministratorContract_decode (arg) -> SetAdministratorContract:
	try:
		return setGovernorContract_decode(arg)
	except Exception as e:
		print('Error in setAdministratorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setAdministratorContract():
	temp=setAdministratorContract_generator()
	temp_encoded=setAdministratorContract_encode(temp)
	try:
		print('++++ setAdministratorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setAdministratorContract: " + str(temp))
	temp1=setAdministratorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setAdministratorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setAdministratorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setAdministratorContract()
#except Exception as e:
#	print('Error in assert_identity_setAdministratorContract' + str(e))




def callSetAdministratorContract(kt1 : str, _from : blockchain.identity, param : SetAdministratorContract, networkName = "ghostnet", debug=False):
	input = setAdministratorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setAdministratorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendTokens = Tuple[factori_types.nat,SetGovernorContract]

def sendTokens_generator() -> SendTokens:
	return (factori_types.tuple2_generator(factori_types.nat_generator,
setGovernorContract_generator))()

def sendTokens_encode(arg : SendTokens) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.nat_encode,setGovernorContract_encode)(arg) 
	except Exception as e:
		print('Error in sendTokens_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def sendTokens_decode(arg) -> SendTokens:
	try:
		return factori_types.tuple2_decode(factori_types.nat_decode,setGovernorContract_decode)(arg) 
	except Exception as e:
		print('Error in sendTokens_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendTokens():
	temp=sendTokens_generator()
	temp_encoded=sendTokens_encode(temp)
	try:
		print('++++ sendTokens: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendTokens: " + str(temp))
	temp1=sendTokens_decode(temp_encoded)
	if temp1 == temp:
		print('sendTokens check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendTokens check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendTokens()
#except Exception as e:
#	print('Error in assert_identity_sendTokens' + str(e))




def callSendTokens(kt1 : str, _from : blockchain.identity, param : SendTokens, networkName = "ghostnet", debug=False):
	input = sendTokens_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendTokens", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendAllTokens_callback = factori_types.nat

def sendAllTokens_callback_generator() -> SendAllTokens_callback:
	return factori_types.nat_generator ()

def sendAllTokens_callback_encode (arg: SendAllTokens_callback):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in sendAllTokens_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def sendAllTokens_callback_decode (arg) -> SendAllTokens_callback:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in sendAllTokens_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendAllTokens_callback():
	temp=sendAllTokens_callback_generator()
	temp_encoded=sendAllTokens_callback_encode(temp)
	try:
		print('++++ sendAllTokens_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendAllTokens_callback: " + str(temp))
	temp1=sendAllTokens_callback_decode(temp_encoded)
	if temp1 == temp:
		print('sendAllTokens_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendAllTokens_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendAllTokens_callback()
#except Exception as e:
#	print('Error in assert_identity_sendAllTokens_callback' + str(e))




def callSendAllTokens_callback(kt1 : str, _from : blockchain.identity, param : SendAllTokens_callback, networkName = "ghostnet", debug=False):
	input = sendAllTokens_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendAllTokens_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendAllTokens = SetGovernorContract


def sendAllTokens_generator() -> SendAllTokens:
	return setGovernorContract_generator ()


def sendAllTokens_encode (arg: SendAllTokens):
	try:
		return setGovernorContract_encode(arg)
	except Exception as e:
		print('Error in sendAllTokens_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def sendAllTokens_decode (arg) -> SendAllTokens:
	try:
		return setGovernorContract_decode(arg)
	except Exception as e:
		print('Error in sendAllTokens_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendAllTokens():
	temp=sendAllTokens_generator()
	temp_encoded=sendAllTokens_encode(temp)
	try:
		print('++++ sendAllTokens: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendAllTokens: " + str(temp))
	temp1=sendAllTokens_decode(temp_encoded)
	if temp1 == temp:
		print('sendAllTokens check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendAllTokens check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendAllTokens()
#except Exception as e:
#	print('Error in assert_identity_sendAllTokens' + str(e))




def callSendAllTokens(kt1 : str, _from : blockchain.identity, param : SendAllTokens, networkName = "ghostnet", debug=False):
	input = sendAllTokens_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendAllTokens", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SendAll = SetGovernorContract


def sendAll_generator() -> SendAll:
	return setGovernorContract_generator ()


def sendAll_encode (arg: SendAll):
	try:
		return setGovernorContract_encode(arg)
	except Exception as e:
		print('Error in sendAll_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def sendAll_decode (arg) -> SendAll:
	try:
		return setGovernorContract_decode(arg)
	except Exception as e:
		print('Error in sendAll_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_sendAll():
	temp=sendAll_generator()
	temp_encoded=sendAll_encode(temp)
	try:
		print('++++ sendAll: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of sendAll: " + str(temp))
	temp1=sendAll_decode(temp_encoded)
	if temp1 == temp:
		print('sendAll check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('sendAll check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_sendAll()
#except Exception as e:
#	print('Error in assert_identity_sendAll' + str(e))




def callSendAll(kt1 : str, _from : blockchain.identity, param : SendAll, networkName = "ghostnet", debug=False):
	input = sendAll_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "sendAll", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Send = Tuple[factori_types.tez,SetGovernorContract]

def send_generator() -> Send:
	return (factori_types.tuple2_generator(factori_types.tez_generator,
setGovernorContract_generator))()

def send_encode(arg : Send) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.tez_encode,setGovernorContract_encode)(arg) 
	except Exception as e:
		print('Error in send_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def send_decode(arg) -> Send:
	try:
		return factori_types.tuple2_decode(factori_types.tez_decode,setGovernorContract_decode)(arg) 
	except Exception as e:
		print('Error in send_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_send():
	temp=send_generator()
	temp_encoded=send_encode(temp)
	try:
		print('++++ send: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of send: " + str(temp))
	temp1=send_decode(temp_encoded)
	if temp1 == temp:
		print('send check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('send check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_send()
#except Exception as e:
#	print('Error in assert_identity_send' + str(e))




def callSend(kt1 : str, _from : blockchain.identity, param : Send, networkName = "ghostnet", debug=False):
	input = send_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "send", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class RescueFA2(TypedDict):
	tokenContractAddress: SetGovernorContract
	tokenId: SendAllTokens_callback
	amount: SendAllTokens_callback
	destination: SetGovernorContract

def rescueFA2_generator() -> RescueFA2:
	return {'tokenContractAddress' : setGovernorContract_generator (),'tokenId' : sendAllTokens_callback_generator (),'amount' : sendAllTokens_callback_generator (),'destination' : setGovernorContract_generator ()}

def rescueFA2_encode(arg : RescueFA2):
	try:
		return PairType.from_comb([(setGovernorContract_encode(arg['tokenContractAddress'])), PairType.from_comb([(sendAllTokens_callback_encode(arg['tokenId'])), PairType.from_comb([(sendAllTokens_callback_encode(arg['amount'])), (setGovernorContract_encode(arg['destination']))])])])
	except Exception as e:
		print('Error in rescueFA2_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def rescueFA2_decode(arg) -> RescueFA2:
	try:
		before_projection=(factori_types.tuple2_decode (setGovernorContract_decode,(factori_types.tuple2_decode (sendAllTokens_callback_decode,(factori_types.tuple2_decode (sendAllTokens_callback_decode,setGovernorContract_decode))))))(arg)
		return {'tokenContractAddress' : (before_projection[0]),
'tokenId' : (before_projection[1][0]),
'amount' : (before_projection[1][1][0]),
'destination' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in rescueFA2_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_rescueFA2():
	temp=rescueFA2_generator()
	temp_encoded=rescueFA2_encode(temp)
	try:
		print('++++ rescueFA2: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of rescueFA2: " + str(temp))
	temp1=rescueFA2_decode(temp_encoded)
	if temp1 == temp:
		print('rescueFA2 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('rescueFA2 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_rescueFA2()
#except Exception as e:
#	print('Error in assert_identity_rescueFA2' + str(e))




def callRescueFA2(kt1 : str, _from : blockchain.identity, param : RescueFA2, networkName = "ghostnet", debug=False):
	input = rescueFA2_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "rescueFA2", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class RescueFA12(TypedDict):
	destination: SetGovernorContract
	amount: SendAllTokens_callback
	tokenContractAddress: SetGovernorContract

def rescueFA12_generator() -> RescueFA12:
	return {'destination' : setGovernorContract_generator (),'amount' : sendAllTokens_callback_generator (),'tokenContractAddress' : setGovernorContract_generator ()}

def rescueFA12_encode(arg : RescueFA12):
	try:
		return PairType.from_comb([(setGovernorContract_encode(arg['tokenContractAddress'])), PairType.from_comb([(sendAllTokens_callback_encode(arg['amount'])), (setGovernorContract_encode(arg['destination']))])])
	except Exception as e:
		print('Error in rescueFA12_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def rescueFA12_decode(arg) -> RescueFA12:
	try:
		before_projection=(factori_types.tuple2_decode (setGovernorContract_decode,(factori_types.tuple2_decode (sendAllTokens_callback_decode,setGovernorContract_decode))))(arg)
		return {'tokenContractAddress' : (before_projection[0]),
'amount' : (before_projection[1][0]),
'destination' : (before_projection[1][1])}


	except Exception as e:
		print('Error in rescueFA12_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_rescueFA12():
	temp=rescueFA12_generator()
	temp_encoded=rescueFA12_encode(temp)
	try:
		print('++++ rescueFA12: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of rescueFA12: " + str(temp))
	temp1=rescueFA12_decode(temp_encoded)
	if temp1 == temp:
		print('rescueFA12 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('rescueFA12 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_rescueFA12()
#except Exception as e:
#	print('Error in assert_identity_rescueFA12' + str(e))




def callRescueFA12(kt1 : str, _from : blockchain.identity, param : RescueFA12, networkName = "ghostnet", debug=False):
	input = rescueFA12_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "rescueFA12", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = factori_types.unit

def _default_generator() -> Constructor_default:
	return factori_types.unit_generator ()

def _default_encode (arg: Constructor_default):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _default_decode (arg) -> Constructor_default:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

State = factori_types.int_michelson

def state_generator() -> State:
	return factori_types.int_generator ()

def state_encode (arg: State):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in state_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def state_decode (arg) -> State:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in state_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_state():
	temp=state_generator()
	temp_encoded=state_encode(temp)
	try:
		print('++++ state: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of state: " + str(temp))
	temp1=state_decode(temp_encoded)
	if temp1 == temp:
		print('state check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('state check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_state()
#except Exception as e:
#	print('Error in assert_identity_state' + str(e))




class Constructor_storage(TypedDict):
	governorContractAddress: SetGovernorContract
	administratorContractAddress: SetGovernorContract
	sendAllTokens_destination: Optional[SetGovernorContract]
	state: State
	tokenContractAddress: SetGovernorContract

def _storage_generator() -> Constructor_storage:
	return {'governorContractAddress' : setGovernorContract_generator (),'administratorContractAddress' : setGovernorContract_generator (),'sendAllTokens_destination' : (factori_types.option_generator(setGovernorContract_generator)) (),'state' : state_generator (),'tokenContractAddress' : setGovernorContract_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([(setGovernorContract_encode(arg['administratorContractAddress'])), (setGovernorContract_encode(arg['governorContractAddress']))]), PairType.from_comb([(factori_types.option_encode(setGovernorContract_encode)(arg['sendAllTokens_destination'])), PairType.from_comb([(state_encode(arg['state'])), (setGovernorContract_encode(arg['tokenContractAddress']))])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode (setGovernorContract_decode,setGovernorContract_decode)),(factori_types.tuple2_decode (factori_types.option_decode(setGovernorContract_decode),(factori_types.tuple2_decode (state_decode,setGovernorContract_decode))))))(arg)
		return {'administratorContractAddress' : (before_projection[0][0]),
'governorContractAddress' : (before_projection[0][1]),
'sendAllTokens_destination' : (before_projection[1][0]),
'state' : (before_projection[1][1][0]),
'tokenContractAddress' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract dev_fund")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/dev_fund_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1