open Tezos_micheline
open Factori_permission_lib
open Factori_representation

type prop = Permission_ast.prop

type 'a perm = 'a Permission_tool.permission

type 'a logic = 'a Permission_tool.logic

module Constant = Nested_arg.Constant

type result_perm =
  ((prop perm logic * prop)
  * (string * (prop perm logic * prop) list) Constant.t)
  list

type permission = {
  entrypoint : string;
  parameter : Nested_arg.t;
  result : result_perm;
}

type warnings =
  | MultipleTypeForAnnotation of {
      origin : Micheline_parser.location;
      annotation : string;
      location : Micheline_parser.location;
      expected : Micheline_printer.node;
      actual : Micheline_printer.node;
    }
  | MissingAnnotation of {
      location : Micheline_parser.location;
      on : Micheline_printer.node;
    }
  | ForbiddenAnnotation of {
      location : Micheline_parser.location;
      annotation : string;
      actual : Micheline_printer.node;
    }
  | WrongOrder of string option * string * string
  (* From lint *)
  | AbsCheck
  | StorageCheck of {
      ty : string;
      annot : string option;
      nb : int;
    }
  | ReentrancyCheck
  | UnusedFieldCheck of {
      var_name : string;
      code : string;
    }
  | PermissionCheck of permission list

module Warnings = Map.Make (struct
  type t = warnings

  let compare = compare
end)

type description = {
  number : int;
  names : string list;
  description : string;
  since : string option;
}

type to_lint =
  | MichelsonFile of string
  | KT1 of string

let number = function
  | MultipleTypeForAnnotation _ -> 1
  | MissingAnnotation _ -> 2
  | ForbiddenAnnotation _ -> 3
  | WrongOrder (_, _, _) -> 4
  | AbsCheck -> 5
  | StorageCheck _ -> 6
  | ReentrancyCheck -> 7
  | UnusedFieldCheck _ -> 8
  | PermissionCheck _ -> 9

let descriptions =
  [
    {
      number = 1;
      names = ["multiple-types-for-annotation"];
      description =
        "It is ill-advised to use the same type name for different types, as \
         it may cause confusion for users (including programs interacting with \
         your contract).";
      since = Some "v0.6.0";
    };
    {
      number = 2;
      names = ["missing-annotation"];
      description = "Some annotations are missing.";
      since = Some "v0.6.0";
    };
    {
      number = 3;
      names = ["forbidden-annotation"];
      description =
        "Some keywords from Michelson are reserved, we advise not using them \
         as type annotations. For example, you probably should not use as \
         annotations the words 'address', 'parameter', 'Left', 'Right', etc.";
      since = Some "v0.6.0";
    };
    {
      number = 4;
      names = ["wrong-order"];
      description = "Wrong order of Michelson primitives";
      since = Some "v0.6.0";
    };
    {
      number = 5;
      names = ["abs-check"];
      description =
        "The `ABS` instruction can break a contract's logic if it is (mis)used \
         to convert integers to natural numbers without checking if the \
         integer is negative. IS_NAT should be preferred to handle the failing \
         case explicitly.";
      since = Some "v0.6.0";
    };
    {
      number = 6;
      names = ["storage-check"];
      description =
        "The use of `map`, `set` or `list` in the storage can be dangerous. \
         These structures will be deserialized at every call and might become \
         too large and make the contract unusable.";
      since = Some "v0.6.0";
    };
    {
      number = 7;
      names = ["reentrancy-check"];
      description =
        "In some cases, outbound calls may cause reentrancy, meaning that this \
         call may trigger an external, inbound call which may take advantage \
         of the internal state.";
      since = Some "v0.6.0";
    };
    {
      number = 8;
      names = ["unusef-field"];
      description =
        "Unused fields in the storage inflate the cost of using the contract. \
         Note that it may be that the field is used as part of a specification \
         (e.g. `metadata`) even if not used, or it may be that the field is \
         used off-chain; don't remove it before checking whether it breaks \
         invariants.";
      since = Some "v0.6.0";
    };
    {
      number = 9;
      names = ["permission-check"];
      description =
        "For each entrypoint, failure modes and conditions triggering these \
         failures are described. Please use due diligence: such analysis is \
         not an audit and may be incomplete and/or incorrect.";
      since = Some "v0.6.0";
    };
  ]

let description number =
  match List.find_opt (fun d -> number = d.number) descriptions with
  | None -> String.empty
  | Some d -> d.description

let add_warnings ?(initial_value = 1) warnings w =
  Warnings.update w
    (function
      | None -> Some initial_value
      | Some nb -> Some (nb + 1))
    warnings
