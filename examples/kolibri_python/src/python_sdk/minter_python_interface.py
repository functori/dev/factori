from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

Withdraw = Tuple[factori_types.nat,Tuple[factori_types.address,Tuple[factori_types.address,Tuple[factori_types.nat,Tuple[factori_types.nat,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,factori_types.tez]]]]]]]]

def withdraw_generator() -> Withdraw:
	return (factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
factori_types.tez_generator))))))))))))))))()

def withdraw_encode(arg : Withdraw) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.nat_encode,factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(factori_types.nat_encode,factori_types.tuple2_encode(factori_types.nat_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tez_encode))))))))(arg) 
	except Exception as e:
		print('Error in withdraw_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def withdraw_decode(arg) -> Withdraw:
	try:
		return factori_types.tuple2_decode(factori_types.nat_decode,factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(factori_types.nat_decode,factori_types.tuple2_decode(factori_types.nat_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tez_decode))))))))(arg) 
	except Exception as e:
		print('Error in withdraw_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_withdraw():
	temp=withdraw_generator()
	temp_encoded=withdraw_encode(temp)
	try:
		print('++++ withdraw: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of withdraw: " + str(temp))
	temp1=withdraw_decode(temp_encoded)
	if temp1 == temp:
		print('withdraw check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('withdraw check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_withdraw()
#except Exception as e:
#	print('Error in assert_identity_withdraw' + str(e))




def callWithdraw(kt1 : str, _from : blockchain.identity, param : Withdraw, networkName = "ghostnet", debug=False):
	input = withdraw_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "withdraw", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateParams = Tuple[factori_types.nat,Tuple[factori_types.nat,Tuple[factori_types.nat,Optional[factori_types.tez]]]]

def updateParams_generator() -> UpdateParams:
	return (factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.option_generator(factori_types.tez_generator))))))))()

def updateParams_encode(arg : UpdateParams) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.nat_encode,factori_types.tuple2_encode(factori_types.nat_encode,factori_types.tuple2_encode(factori_types.nat_encode,factori_types.option_encode(factori_types.tez_encode))))(arg) 
	except Exception as e:
		print('Error in updateParams_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateParams_decode(arg) -> UpdateParams:
	try:
		return factori_types.tuple2_decode(factori_types.nat_decode,factori_types.tuple2_decode(factori_types.nat_decode,factori_types.tuple2_decode(factori_types.nat_decode,factori_types.option_decode(factori_types.tez_decode))))(arg) 
	except Exception as e:
		print('Error in updateParams_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateParams():
	temp=updateParams_generator()
	temp_encoded=updateParams_encode(temp)
	try:
		print('++++ updateParams: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateParams: " + str(temp))
	temp1=updateParams_decode(temp_encoded)
	if temp1 == temp:
		print('updateParams check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateParams check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateParams()
#except Exception as e:
#	print('Error in assert_identity_updateParams' + str(e))




def callUpdateParams(kt1 : str, _from : blockchain.identity, param : UpdateParams, networkName = "ghostnet", debug=False):
	input = updateParams_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateParams", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

StabilityFundSplit = factori_types.nat

def stabilityFundSplit_generator() -> StabilityFundSplit:
	return factori_types.nat_generator ()

def stabilityFundSplit_encode (arg: StabilityFundSplit):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in stabilityFundSplit_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def stabilityFundSplit_decode (arg) -> StabilityFundSplit:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in stabilityFundSplit_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_stabilityFundSplit():
	temp=stabilityFundSplit_generator()
	temp_encoded=stabilityFundSplit_encode(temp)
	try:
		print('++++ stabilityFundSplit: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of stabilityFundSplit: " + str(temp))
	temp1=stabilityFundSplit_decode(temp_encoded)
	if temp1 == temp:
		print('stabilityFundSplit check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('stabilityFundSplit check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_stabilityFundSplit()
#except Exception as e:
#	print('Error in assert_identity_stabilityFundSplit' + str(e))




class UpdateFundSplits(TypedDict):
	developerFundSplit: StabilityFundSplit
	stabilityFundSplit: StabilityFundSplit

def updateFundSplits_generator() -> UpdateFundSplits:
	return {'developerFundSplit' : stabilityFundSplit_generator (),'stabilityFundSplit' : stabilityFundSplit_generator ()}

def updateFundSplits_encode(arg : UpdateFundSplits):
	try:
		return PairType.from_comb([(stabilityFundSplit_encode(arg['developerFundSplit'])), (stabilityFundSplit_encode(arg['stabilityFundSplit']))])
	except Exception as e:
		print('Error in updateFundSplits_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateFundSplits_decode(arg) -> UpdateFundSplits:
	try:
		before_projection=(factori_types.tuple2_decode (stabilityFundSplit_decode,stabilityFundSplit_decode))(arg)
		return {'developerFundSplit' : (before_projection[0]),
'stabilityFundSplit' : (before_projection[1])}


	except Exception as e:
		print('Error in updateFundSplits_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateFundSplits():
	temp=updateFundSplits_generator()
	temp_encoded=updateFundSplits_encode(temp)
	try:
		print('++++ updateFundSplits: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateFundSplits: " + str(temp))
	temp1=updateFundSplits_decode(temp_encoded)
	if temp1 == temp:
		print('updateFundSplits check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateFundSplits check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateFundSplits()
#except Exception as e:
#	print('Error in assert_identity_updateFundSplits' + str(e))




def callUpdateFundSplits(kt1 : str, _from : blockchain.identity, param : UpdateFundSplits, networkName = "ghostnet", debug=False):
	input = updateFundSplits_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateFundSplits", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateContracts = Tuple[factori_types.address,Tuple[factori_types.address,Tuple[factori_types.address,Tuple[factori_types.address,factori_types.address]]]]

def updateContracts_generator() -> UpdateContracts:
	return (factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(factori_types.address_generator,
(factori_types.tuple2_generator(factori_types.address_generator,
factori_types.address_generator))))))))()

def updateContracts_encode(arg : UpdateContracts) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(factori_types.address_encode,factori_types.tuple2_encode(factori_types.address_encode,factori_types.address_encode))))(arg) 
	except Exception as e:
		print('Error in updateContracts_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateContracts_decode(arg) -> UpdateContracts:
	try:
		return factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(factori_types.address_decode,factori_types.tuple2_decode(factori_types.address_decode,factori_types.address_decode))))(arg) 
	except Exception as e:
		print('Error in updateContracts_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateContracts():
	temp=updateContracts_generator()
	temp_encoded=updateContracts_encode(temp)
	try:
		print('++++ updateContracts: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateContracts: " + str(temp))
	temp1=updateContracts_decode(temp_encoded)
	if temp1 == temp:
		print('updateContracts check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateContracts check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateContracts()
#except Exception as e:
#	print('Error in assert_identity_updateContracts' + str(e))




def callUpdateContracts(kt1 : str, _from : blockchain.identity, param : UpdateContracts, networkName = "ghostnet", debug=False):
	input = updateContracts_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateContracts", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetTokenContract = factori_types.address

def setTokenContract_generator() -> SetTokenContract:
	return factori_types.address_generator ()

def setTokenContract_encode (arg: SetTokenContract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in setTokenContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setTokenContract_decode (arg) -> SetTokenContract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in setTokenContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setTokenContract():
	temp=setTokenContract_generator()
	temp_encoded=setTokenContract_encode(temp)
	try:
		print('++++ setTokenContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setTokenContract: " + str(temp))
	temp1=setTokenContract_decode(temp_encoded)
	if temp1 == temp:
		print('setTokenContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setTokenContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setTokenContract()
#except Exception as e:
#	print('Error in assert_identity_setTokenContract' + str(e))




def callSetTokenContract(kt1 : str, _from : blockchain.identity, param : SetTokenContract, networkName = "ghostnet", debug=False):
	input = setTokenContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setTokenContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetStabilityFundContract = SetTokenContract


def setStabilityFundContract_generator() -> SetStabilityFundContract:
	return setTokenContract_generator ()


def setStabilityFundContract_encode (arg: SetStabilityFundContract):
	try:
		return setTokenContract_encode(arg)
	except Exception as e:
		print('Error in setStabilityFundContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setStabilityFundContract_decode (arg) -> SetStabilityFundContract:
	try:
		return setTokenContract_decode(arg)
	except Exception as e:
		print('Error in setStabilityFundContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setStabilityFundContract():
	temp=setStabilityFundContract_generator()
	temp_encoded=setStabilityFundContract_encode(temp)
	try:
		print('++++ setStabilityFundContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setStabilityFundContract: " + str(temp))
	temp1=setStabilityFundContract_decode(temp_encoded)
	if temp1 == temp:
		print('setStabilityFundContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setStabilityFundContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setStabilityFundContract()
#except Exception as e:
#	print('Error in assert_identity_setStabilityFundContract' + str(e))




def callSetStabilityFundContract(kt1 : str, _from : blockchain.identity, param : SetStabilityFundContract, networkName = "ghostnet", debug=False):
	input = setStabilityFundContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setStabilityFundContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetStabilityFee = StabilityFundSplit


def setStabilityFee_generator() -> SetStabilityFee:
	return stabilityFundSplit_generator ()


def setStabilityFee_encode (arg: SetStabilityFee):
	try:
		return stabilityFundSplit_encode(arg)
	except Exception as e:
		print('Error in setStabilityFee_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setStabilityFee_decode (arg) -> SetStabilityFee:
	try:
		return stabilityFundSplit_decode(arg)
	except Exception as e:
		print('Error in setStabilityFee_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setStabilityFee():
	temp=setStabilityFee_generator()
	temp_encoded=setStabilityFee_encode(temp)
	try:
		print('++++ setStabilityFee: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setStabilityFee: " + str(temp))
	temp1=setStabilityFee_decode(temp_encoded)
	if temp1 == temp:
		print('setStabilityFee check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setStabilityFee check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setStabilityFee()
#except Exception as e:
#	print('Error in assert_identity_setStabilityFee' + str(e))




def callSetStabilityFee(kt1 : str, _from : blockchain.identity, param : SetStabilityFee, networkName = "ghostnet", debug=False):
	input = setStabilityFee_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setStabilityFee", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetPrivateLiquidationThreshold = StabilityFundSplit


def setPrivateLiquidationThreshold_generator() -> SetPrivateLiquidationThreshold:
	return stabilityFundSplit_generator ()


def setPrivateLiquidationThreshold_encode (arg: SetPrivateLiquidationThreshold):
	try:
		return stabilityFundSplit_encode(arg)
	except Exception as e:
		print('Error in setPrivateLiquidationThreshold_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setPrivateLiquidationThreshold_decode (arg) -> SetPrivateLiquidationThreshold:
	try:
		return stabilityFundSplit_decode(arg)
	except Exception as e:
		print('Error in setPrivateLiquidationThreshold_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setPrivateLiquidationThreshold():
	temp=setPrivateLiquidationThreshold_generator()
	temp_encoded=setPrivateLiquidationThreshold_encode(temp)
	try:
		print('++++ setPrivateLiquidationThreshold: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setPrivateLiquidationThreshold: " + str(temp))
	temp1=setPrivateLiquidationThreshold_decode(temp_encoded)
	if temp1 == temp:
		print('setPrivateLiquidationThreshold check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setPrivateLiquidationThreshold check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setPrivateLiquidationThreshold()
#except Exception as e:
#	print('Error in assert_identity_setPrivateLiquidationThreshold' + str(e))




def callSetPrivateLiquidationThreshold(kt1 : str, _from : blockchain.identity, param : SetPrivateLiquidationThreshold, networkName = "ghostnet", debug=False):
	input = setPrivateLiquidationThreshold_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setPrivateLiquidationThreshold", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetOvenProxyContract = SetTokenContract


def setOvenProxyContract_generator() -> SetOvenProxyContract:
	return setTokenContract_generator ()


def setOvenProxyContract_encode (arg: SetOvenProxyContract):
	try:
		return setTokenContract_encode(arg)
	except Exception as e:
		print('Error in setOvenProxyContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setOvenProxyContract_decode (arg) -> SetOvenProxyContract:
	try:
		return setTokenContract_decode(arg)
	except Exception as e:
		print('Error in setOvenProxyContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setOvenProxyContract():
	temp=setOvenProxyContract_generator()
	temp_encoded=setOvenProxyContract_encode(temp)
	try:
		print('++++ setOvenProxyContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setOvenProxyContract: " + str(temp))
	temp1=setOvenProxyContract_decode(temp_encoded)
	if temp1 == temp:
		print('setOvenProxyContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setOvenProxyContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setOvenProxyContract()
#except Exception as e:
#	print('Error in assert_identity_setOvenProxyContract' + str(e))




def callSetOvenProxyContract(kt1 : str, _from : blockchain.identity, param : SetOvenProxyContract, networkName = "ghostnet", debug=False):
	input = setOvenProxyContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setOvenProxyContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetLiquidityPoolContract = SetTokenContract


def setLiquidityPoolContract_generator() -> SetLiquidityPoolContract:
	return setTokenContract_generator ()


def setLiquidityPoolContract_encode (arg: SetLiquidityPoolContract):
	try:
		return setTokenContract_encode(arg)
	except Exception as e:
		print('Error in setLiquidityPoolContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setLiquidityPoolContract_decode (arg) -> SetLiquidityPoolContract:
	try:
		return setTokenContract_decode(arg)
	except Exception as e:
		print('Error in setLiquidityPoolContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setLiquidityPoolContract():
	temp=setLiquidityPoolContract_generator()
	temp_encoded=setLiquidityPoolContract_encode(temp)
	try:
		print('++++ setLiquidityPoolContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setLiquidityPoolContract: " + str(temp))
	temp1=setLiquidityPoolContract_decode(temp_encoded)
	if temp1 == temp:
		print('setLiquidityPoolContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setLiquidityPoolContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setLiquidityPoolContract()
#except Exception as e:
#	print('Error in assert_identity_setLiquidityPoolContract' + str(e))




def callSetLiquidityPoolContract(kt1 : str, _from : blockchain.identity, param : SetLiquidityPoolContract, networkName = "ghostnet", debug=False):
	input = setLiquidityPoolContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setLiquidityPoolContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetLiquidationFeePercent = StabilityFundSplit


def setLiquidationFeePercent_generator() -> SetLiquidationFeePercent:
	return stabilityFundSplit_generator ()


def setLiquidationFeePercent_encode (arg: SetLiquidationFeePercent):
	try:
		return stabilityFundSplit_encode(arg)
	except Exception as e:
		print('Error in setLiquidationFeePercent_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setLiquidationFeePercent_decode (arg) -> SetLiquidationFeePercent:
	try:
		return stabilityFundSplit_decode(arg)
	except Exception as e:
		print('Error in setLiquidationFeePercent_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setLiquidationFeePercent():
	temp=setLiquidationFeePercent_generator()
	temp_encoded=setLiquidationFeePercent_encode(temp)
	try:
		print('++++ setLiquidationFeePercent: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setLiquidationFeePercent: " + str(temp))
	temp1=setLiquidationFeePercent_decode(temp_encoded)
	if temp1 == temp:
		print('setLiquidationFeePercent check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setLiquidationFeePercent check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setLiquidationFeePercent()
#except Exception as e:
#	print('Error in assert_identity_setLiquidationFeePercent' + str(e))




def callSetLiquidationFeePercent(kt1 : str, _from : blockchain.identity, param : SetLiquidationFeePercent, networkName = "ghostnet", debug=False):
	input = setLiquidationFeePercent_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setLiquidationFeePercent", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = SetTokenContract


def setGovernorContract_generator() -> SetGovernorContract:
	return setTokenContract_generator ()


def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return setTokenContract_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return setTokenContract_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetDeveloperFundContract = SetTokenContract


def setDeveloperFundContract_generator() -> SetDeveloperFundContract:
	return setTokenContract_generator ()


def setDeveloperFundContract_encode (arg: SetDeveloperFundContract):
	try:
		return setTokenContract_encode(arg)
	except Exception as e:
		print('Error in setDeveloperFundContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setDeveloperFundContract_decode (arg) -> SetDeveloperFundContract:
	try:
		return setTokenContract_decode(arg)
	except Exception as e:
		print('Error in setDeveloperFundContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setDeveloperFundContract():
	temp=setDeveloperFundContract_generator()
	temp_encoded=setDeveloperFundContract_encode(temp)
	try:
		print('++++ setDeveloperFundContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setDeveloperFundContract: " + str(temp))
	temp1=setDeveloperFundContract_decode(temp_encoded)
	if temp1 == temp:
		print('setDeveloperFundContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setDeveloperFundContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setDeveloperFundContract()
#except Exception as e:
#	print('Error in assert_identity_setDeveloperFundContract' + str(e))




def callSetDeveloperFundContract(kt1 : str, _from : blockchain.identity, param : SetDeveloperFundContract, networkName = "ghostnet", debug=False):
	input = setDeveloperFundContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setDeveloperFundContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetCollateralizationPercentage = StabilityFundSplit


def setCollateralizationPercentage_generator() -> SetCollateralizationPercentage:
	return stabilityFundSplit_generator ()


def setCollateralizationPercentage_encode (arg: SetCollateralizationPercentage):
	try:
		return stabilityFundSplit_encode(arg)
	except Exception as e:
		print('Error in setCollateralizationPercentage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setCollateralizationPercentage_decode (arg) -> SetCollateralizationPercentage:
	try:
		return stabilityFundSplit_decode(arg)
	except Exception as e:
		print('Error in setCollateralizationPercentage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setCollateralizationPercentage():
	temp=setCollateralizationPercentage_generator()
	temp_encoded=setCollateralizationPercentage_encode(temp)
	try:
		print('++++ setCollateralizationPercentage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setCollateralizationPercentage: " + str(temp))
	temp1=setCollateralizationPercentage_decode(temp_encoded)
	if temp1 == temp:
		print('setCollateralizationPercentage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setCollateralizationPercentage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setCollateralizationPercentage()
#except Exception as e:
#	print('Error in assert_identity_setCollateralizationPercentage' + str(e))




def callSetCollateralizationPercentage(kt1 : str, _from : blockchain.identity, param : SetCollateralizationPercentage, networkName = "ghostnet", debug=False):
	input = setCollateralizationPercentage_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setCollateralizationPercentage", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Repay = Tuple[SetTokenContract,Tuple[SetTokenContract,Tuple[StabilityFundSplit,Tuple[StabilityFundSplit,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,StabilityFundSplit]]]]]]]

def repay_generator() -> Repay:
	return (factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
stabilityFundSplit_generator))))))))))))))()

def repay_encode(arg : Repay) -> PairType:
	try:
		return factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,stabilityFundSplit_encode)))))))(arg) 
	except Exception as e:
		print('Error in repay_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def repay_decode(arg) -> Repay:
	try:
		return factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,stabilityFundSplit_decode)))))))(arg) 
	except Exception as e:
		print('Error in repay_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_repay():
	temp=repay_generator()
	temp_encoded=repay_encode(temp)
	try:
		print('++++ repay: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of repay: " + str(temp))
	temp1=repay_decode(temp_encoded)
	if temp1 == temp:
		print('repay check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('repay check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_repay()
#except Exception as e:
#	print('Error in assert_identity_repay' + str(e))




def callRepay(kt1 : str, _from : blockchain.identity, param : Repay, networkName = "ghostnet", debug=False):
	input = repay_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "repay", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Liquidate = Tuple[StabilityFundSplit,Tuple[SetTokenContract,Tuple[SetTokenContract,Tuple[StabilityFundSplit,Tuple[StabilityFundSplit,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,SetTokenContract]]]]]]]]

def liquidate_generator() -> Liquidate:
	return (factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
setTokenContract_generator))))))))))))))))()

def liquidate_encode(arg : Liquidate) -> PairType:
	try:
		return factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,setTokenContract_encode))))))))(arg) 
	except Exception as e:
		print('Error in liquidate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def liquidate_decode(arg) -> Liquidate:
	try:
		return factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,setTokenContract_decode))))))))(arg) 
	except Exception as e:
		print('Error in liquidate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_liquidate():
	temp=liquidate_generator()
	temp_encoded=liquidate_encode(temp)
	try:
		print('++++ liquidate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of liquidate: " + str(temp))
	temp1=liquidate_decode(temp_encoded)
	if temp1 == temp:
		print('liquidate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('liquidate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_liquidate()
#except Exception as e:
#	print('Error in assert_identity_liquidate' + str(e))




def callLiquidate(kt1 : str, _from : blockchain.identity, param : Liquidate, networkName = "ghostnet", debug=False):
	input = liquidate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "liquidate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetInterestIndex = factori_types.contract

def getInterestIndex_generator() -> GetInterestIndex:
	return factori_types.contract_generator ()

def getInterestIndex_encode (arg: GetInterestIndex):
	try:
		return factori_types.contract_encode(arg)
	except Exception as e:
		print('Error in getInterestIndex_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getInterestIndex_decode (arg) -> GetInterestIndex:
	try:
		return factori_types.contract_decode(arg)
	except Exception as e:
		print('Error in getInterestIndex_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getInterestIndex():
	temp=getInterestIndex_generator()
	temp_encoded=getInterestIndex_encode(temp)
	try:
		print('++++ getInterestIndex: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getInterestIndex: " + str(temp))
	temp1=getInterestIndex_decode(temp_encoded)
	if temp1 == temp:
		print('getInterestIndex check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getInterestIndex check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getInterestIndex()
#except Exception as e:
#	print('Error in assert_identity_getInterestIndex' + str(e))




def callGetInterestIndex(kt1 : str, _from : blockchain.identity, param : GetInterestIndex, networkName = "ghostnet", debug=False):
	input = getInterestIndex_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getInterestIndex", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Deposit = Tuple[SetTokenContract,Tuple[SetTokenContract,Tuple[StabilityFundSplit,Tuple[StabilityFundSplit,Tuple[bool,Tuple[factori_types.int_michelson,factori_types.int_michelson]]]]]]

def deposit_generator() -> Deposit:
	return (factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
factori_types.int_generator))))))))))))()

def deposit_encode(arg : Deposit) -> PairType:
	try:
		return factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.int_michelson_encode))))))(arg) 
	except Exception as e:
		print('Error in deposit_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def deposit_decode(arg) -> Deposit:
	try:
		return factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.int_michelson_decode))))))(arg) 
	except Exception as e:
		print('Error in deposit_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_deposit():
	temp=deposit_generator()
	temp_encoded=deposit_encode(temp)
	try:
		print('++++ deposit: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of deposit: " + str(temp))
	temp1=deposit_decode(temp_encoded)
	if temp1 == temp:
		print('deposit check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('deposit check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_deposit()
#except Exception as e:
#	print('Error in assert_identity_deposit' + str(e))




def callDeposit(kt1 : str, _from : blockchain.identity, param : Deposit, networkName = "ghostnet", debug=False):
	input = deposit_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "deposit", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Borrow = Tuple[StabilityFundSplit,Tuple[SetTokenContract,Tuple[SetTokenContract,Tuple[StabilityFundSplit,Tuple[StabilityFundSplit,Tuple[bool,Tuple[factori_types.int_michelson,Tuple[factori_types.int_michelson,StabilityFundSplit]]]]]]]]

def borrow_generator() -> Borrow:
	return (factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(setTokenContract_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(stabilityFundSplit_generator,
(factori_types.tuple2_generator(factori_types.bool_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
(factori_types.tuple2_generator(factori_types.int_generator,
stabilityFundSplit_generator))))))))))))))))()

def borrow_encode(arg : Borrow) -> PairType:
	try:
		return factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(setTokenContract_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(stabilityFundSplit_encode,factori_types.tuple2_encode(factori_types.bool_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,factori_types.tuple2_encode(factori_types.int_michelson_encode,stabilityFundSplit_encode))))))))(arg) 
	except Exception as e:
		print('Error in borrow_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def borrow_decode(arg) -> Borrow:
	try:
		return factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(setTokenContract_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(stabilityFundSplit_decode,factori_types.tuple2_decode(factori_types.bool_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,factori_types.tuple2_decode(factori_types.int_michelson_decode,stabilityFundSplit_decode))))))))(arg) 
	except Exception as e:
		print('Error in borrow_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_borrow():
	temp=borrow_generator()
	temp_encoded=borrow_encode(temp)
	try:
		print('++++ borrow: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of borrow: " + str(temp))
	temp1=borrow_decode(temp_encoded)
	if temp1 == temp:
		print('borrow check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('borrow check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_borrow()
#except Exception as e:
#	print('Error in assert_identity_borrow' + str(e))




def callBorrow(kt1 : str, _from : blockchain.identity, param : Borrow, networkName = "ghostnet", debug=False):
	input = borrow_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "borrow", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

LastInterestIndexUpdateTime = factori_types.timestamp

def lastInterestIndexUpdateTime_generator() -> LastInterestIndexUpdateTime:
	return factori_types.timestamp_generator ()

def lastInterestIndexUpdateTime_encode (arg: LastInterestIndexUpdateTime):
	try:
		return factori_types.timestamp_encode(arg)
	except Exception as e:
		print('Error in lastInterestIndexUpdateTime_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def lastInterestIndexUpdateTime_decode (arg) -> LastInterestIndexUpdateTime:
	try:
		return factori_types.timestamp_decode(arg)
	except Exception as e:
		print('Error in lastInterestIndexUpdateTime_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_lastInterestIndexUpdateTime():
	temp=lastInterestIndexUpdateTime_generator()
	temp_encoded=lastInterestIndexUpdateTime_encode(temp)
	try:
		print('++++ lastInterestIndexUpdateTime: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of lastInterestIndexUpdateTime: " + str(temp))
	temp1=lastInterestIndexUpdateTime_decode(temp_encoded)
	if temp1 == temp:
		print('lastInterestIndexUpdateTime check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('lastInterestIndexUpdateTime check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_lastInterestIndexUpdateTime()
#except Exception as e:
#	print('Error in assert_identity_lastInterestIndexUpdateTime' + str(e))




class Constructor_storage(TypedDict):
	tokenContractAddress: SetTokenContract
	stabilityFundContractAddress: SetTokenContract
	lastInterestIndexUpdateTime: LastInterestIndexUpdateTime
	interestIndex: StabilityFundSplit
	governorContractAddress: SetTokenContract
	developerFundContractAddress: SetTokenContract
	devFundSplit: StabilityFundSplit
	collateralizationPercentage: StabilityFundSplit
	liquidationFeePercent: StabilityFundSplit
	liquidityPoolContractAddress: SetTokenContract
	ovenProxyContractAddress: SetTokenContract
	privateOwnerLiquidationThreshold: StabilityFundSplit
	stabilityFee: StabilityFundSplit

def _storage_generator() -> Constructor_storage:
	return {'tokenContractAddress' : setTokenContract_generator (),'stabilityFundContractAddress' : setTokenContract_generator (),'lastInterestIndexUpdateTime' : lastInterestIndexUpdateTime_generator (),'interestIndex' : stabilityFundSplit_generator (),'governorContractAddress' : setTokenContract_generator (),'developerFundContractAddress' : setTokenContract_generator (),'devFundSplit' : stabilityFundSplit_generator (),'collateralizationPercentage' : stabilityFundSplit_generator (),'liquidationFeePercent' : stabilityFundSplit_generator (),'liquidityPoolContractAddress' : setTokenContract_generator (),'ovenProxyContractAddress' : setTokenContract_generator (),'privateOwnerLiquidationThreshold' : stabilityFundSplit_generator (),'stabilityFee' : stabilityFundSplit_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([PairType.from_comb([(stabilityFundSplit_encode(arg['collateralizationPercentage'])), PairType.from_comb([(stabilityFundSplit_encode(arg['devFundSplit'])), (setTokenContract_encode(arg['developerFundContractAddress']))])]), PairType.from_comb([(setTokenContract_encode(arg['governorContractAddress'])), PairType.from_comb([(stabilityFundSplit_encode(arg['interestIndex'])), (lastInterestIndexUpdateTime_encode(arg['lastInterestIndexUpdateTime']))])])]), PairType.from_comb([PairType.from_comb([(stabilityFundSplit_encode(arg['liquidationFeePercent'])), PairType.from_comb([(setTokenContract_encode(arg['liquidityPoolContractAddress'])), (setTokenContract_encode(arg['ovenProxyContractAddress']))])]), PairType.from_comb([PairType.from_comb([(stabilityFundSplit_encode(arg['privateOwnerLiquidationThreshold'])), (stabilityFundSplit_encode(arg['stabilityFee']))]), PairType.from_comb([(setTokenContract_encode(arg['stabilityFundContractAddress'])), (setTokenContract_encode(arg['tokenContractAddress']))])])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode (stabilityFundSplit_decode,(factori_types.tuple2_decode (stabilityFundSplit_decode,setTokenContract_decode)))),(factori_types.tuple2_decode (setTokenContract_decode,(factori_types.tuple2_decode (stabilityFundSplit_decode,lastInterestIndexUpdateTime_decode)))))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (stabilityFundSplit_decode,(factori_types.tuple2_decode (setTokenContract_decode,setTokenContract_decode)))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (stabilityFundSplit_decode,stabilityFundSplit_decode)),(factori_types.tuple2_decode (setTokenContract_decode,setTokenContract_decode))))))))(arg)
		return {'collateralizationPercentage' : (before_projection[0][0][0]),
'devFundSplit' : (before_projection[0][0][1][0]),
'developerFundContractAddress' : (before_projection[0][0][1][1]),
'governorContractAddress' : (before_projection[0][1][0]),
'interestIndex' : (before_projection[0][1][1][0]),
'lastInterestIndexUpdateTime' : (before_projection[0][1][1][1]),
'liquidationFeePercent' : (before_projection[1][0][0]),
'liquidityPoolContractAddress' : (before_projection[1][0][1][0]),
'ovenProxyContractAddress' : (before_projection[1][0][1][1]),
'privateOwnerLiquidationThreshold' : (before_projection[1][1][0][0]),
'stabilityFee' : (before_projection[1][1][0][1]),
'stabilityFundContractAddress' : (before_projection[1][1][1][0]),
'tokenContractAddress' : (before_projection[1][1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract minter")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/minter_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1