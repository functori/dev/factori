
import {
    TezosToolkit,
    MichelsonMap,
} from "@taquito/taquito";
import { MichelsonV1Expression } from "@taquito/rpc"
import * as fa2_x from "./fa2_x_interface";
import * as functolib from "./functolib";
const config = functolib.flextesa_config;
const tezosKit = new TezosToolkit(config.node_addr);

async function main(tezosKit: TezosToolkit) {
    functolib.setSigner(tezosKit, functolib.alice_flextesa.sk);
    let storage = fa2_x._storage_generator();
    let kt1 = await fa2_x.deploy_fa2_x(tezosKit, storage, config);
    console.log("KT1: %s", kt1);
}

main(tezosKit)
