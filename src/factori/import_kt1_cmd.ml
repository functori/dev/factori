(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `import_kt1` subcommand of this OCaml file is used to import an on-chain
contract using its KT1 and generate code in multiple languages. The subcommand
takes several optional arguments to specify options such as the network,
language, and whether or not to use a library. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Factori_command
open Factori_options
open Factori_errors

let import_kt1 =
  subcommand ~name:"import kt1"
    ~args:
      [
        dir_anonymous_option;
        kt1_anonymous1;
        contract_name_option;
        ocaml_language_option;
        typescript_language_option;
        python_language_option;
        csharp_language_option;
        field_prefixes_option;
        network_option;
        library_option;
        crawlori_option;
        crawlori_db_name_option;
        web_option;
        dipdup_option;
        project_name_option;
        do_not_regroup_option;
      ]
    ~doc:
      "Import an on-chain contract using its KT1. You may specify from which \
       network it should be pulled." (fun () ->
      language_shenanigans () ;
      try
        (* First initialize directory structure if it is not already there
            *)
        if (not (File_structure.is_initialized_dir ())) || !overwrite then
          Init_cmd.init_fun () ;
        let network =
          Factori_network.network_of_string !Factori_options.network in
        let db_name = db_name () in
        Import_kt1.import_kt1 ~library:!library_mode ~crawlori:!crawlori
          ~dipdup:!dipdup ~network ~db_name () ;
        Format.eprintf "Successfully imported KT1.\n%!"
      with e -> handle_exception e)
