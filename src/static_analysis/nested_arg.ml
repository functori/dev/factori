(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines types and functions for nested arguments in the
Michelson language used in the Tezos blockchain. The types include `Name`,
`Pair`, `Or`, and `Option`, and the functions include `construct`,
`deconstruct`, `name_string_list`, `to_name_arg`, `match_or`, `make_or`,
`to_string`, `pair_simple`, `unpair_arg`, `gets_pair`, `equal`,
`recup_parameter`, and `updates_pair`. No monad binders are used in this file.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Tzfunc.Proto
open Factori_errors
open Factori_micheline_lib

type name_arg = Name_arg.t

let pp_micheline = Micheline_lib.print_micheline ~simple:true

type t =
  | Name of string * micheline
  | Pair of t list * string list
  | Or of t * t * string list
  | Option of t * string list
[@@deriving show]

let construct ?build_name (id_start : int) (m : micheline) : int * t =
  let rec construct id_start m =
    match m with
    | Mprim { prim = `pair; args; annots } ->
      let acc, l = List.fold_left_map construct id_start args in
      (acc, Pair (l, annots))
    | Mprim { prim = `or_; args = [arg1; arg2]; annots } ->
      let acc, arg1 = construct id_start arg1 in
      let acc, arg2 = construct acc arg2 in
      (acc, Or (arg1, arg2, annots))
    | Mprim { prim = `option; args = [arg]; annots } ->
      let acc, new_arg = construct id_start arg in
      (acc, Option (new_arg, annots))
    | _ -> (
      match (Micheline_lib.new_item_cpt id_start m, build_name) with
      | Error e, _ -> raise e
      | Ok (cpt, (_, m)), Some f -> (cpt, Name (f cpt, m))
      | Ok (cpt, (name, m)), None -> (cpt, Name (name, m))) in
  construct id_start m

let name (arg : t) : string option =
  match arg with
  | Name (str, _) -> Some str
  | _ -> None

let rec deconstruct (arg : t) : micheline =
  match arg with
  | Name (_, m) -> m
  | Pair (ty_l, annots) ->
    let args = List.map deconstruct ty_l in
    Mprim { prim = `pair; args; annots }
  | Or (left, right, annots) ->
    let left = deconstruct left in
    let right = deconstruct right in
    Mprim { prim = `or_; args = [left; right]; annots }
  | Option (opt, annots) ->
    let opt = deconstruct opt in
    Mprim { prim = `option; args = [opt]; annots }

let name_string_list (arg : t list) : name_arg list =
  let rec bring_all (acc : name_arg list) (next_arg : t list) : name_arg list =
    match next_arg with
    | [] -> acc
    | arg :: next_arg -> (
      match arg with
      | Name (s, ty) -> bring_all ((s, ty) :: acc) next_arg
      | Pair (l, _) -> bring_all acc (List.rev_append l next_arg)
      | Or (l, r, _) -> bring_all acc (l :: r :: next_arg)
      | Option (opt, _) -> bring_all acc (opt :: next_arg)) in
  bring_all [] arg

(*let top_level_pair (arg : t) : t list =
  let rec bring_pair (acc : t list) (next_arg : t list) : t list =
    match next_arg with
    | [] -> acc
    | arg :: next_arg -> (
      match arg with
      | Name (s, ty) -> bring_all ((s, ty) :: acc) next_arg
      | Pair (l, _) -> bring_all acc (List.rev_append l next_arg)
      | Or (l, r, _) -> bring_all acc (l :: r :: next_arg)
      | Option (opt, _) -> bring_all acc (opt :: next_arg)) in
  bring_all [] arg*)

let to_name_arg (t : t) : name_arg option =
  match t with
  | Name (arg, ty) -> Some (arg, ty)
  | _ -> None

let match_or (l : t) =
  match l with
  | Or (l, r, _) -> Some (l, r)
  | _ -> None

let make_or ?(annots = []) (l : t) (r : t) = Or (l, r, annots)

let unconstruct_or (l : t) =
  match l with
  | Or (l, r, _) -> Some (l, r)
  | _ -> None

let simple ((n, m) : name_arg) : t = Name (n, m)

let rec to_string (arg : t) : string =
  match arg with
  | Name (s, _) -> s
  | Pair (l, _) ->
    let l = List.map to_string l in
    "(" ^ String.concat ", " l ^ ")"
  | Or (l, r, _) -> "Or (" ^ to_string l ^ ")  (" ^ to_string r ^ ")"
  | Option (opt, _) -> "Option (" ^ to_string opt ^ ")"

let pair_simple = function
  | [] -> Error (UnstackMicheline "Canno't pair 0 argument")
  | [x] -> Ok x
  | arg_l -> Ok (Pair (arg_l, []))

let unpair_arg n pair =
  let unpair_fun = function
    | Pair (l, _) -> Ok l
    | _ -> Error (UnstackMicheline "Arg is not of type pair") in
  let res = Factori_utils.split_pair_at_general n unpair_fun pair_simple pair in
  match res with
  | Ok res -> res
  | Error exn -> raise exn

let rec gets_pair n ty =
  if Z.equal n Z.zero then
    Ok ty
  else
    match ty with
    | Pair (x :: _, _) when Z.equal n Z.one -> Ok x
    | Pair (_ :: sub_miche_l, _) ->
      let new_n = Z.sub n (Z.of_int 2) in
      Result.bind (pair_simple sub_miche_l) (gets_pair new_n)
    | Name (s, _) ->
      Error (UnstackMicheline ("Empty" ^ s ^ " " ^ Z.to_string n))
    | _ ->
      Error
        (UnstackMicheline
           (Format.sprintf "Not good for a GET n instruction here %s"
              (to_string ty)))

let rec equal t1 t2 =
  match (t1, t2) with
  | Name (s1, _), Name (s2, _) -> Stdlib.compare s1 s2
  | Pair (ty_l1, _), Pair (ty_l2, _) ->
    let l = List.compare_lengths ty_l1 ty_l2 in
    if l = 0 then
      List.fold_left2
        (fun acc l1 l2 ->
          if acc = 0 then
            equal l1 l2
          else
            acc)
        0 ty_l1 ty_l2
    else
      l
  | Or (t_l1, t_r1, _), Or (t_l2, t_r2, _) ->
    let temp = equal t_l1 t_l2 in
    if temp = 0 then
      equal t_r1 t_r2
    else
      temp
  | Option (t1, _), Option (t2, _) -> equal t1 t2
  | Name _, _ -> -1
  | _, Name _ -> 1
  | Option _, _ -> -1
  | _, Option _ -> 1
  | Or _, _ -> -1
  | _, Or _ -> 1

let recup_parameter param_storage =
  match gets_pair (Z.of_int 1) param_storage with
  | Error exn -> raise exn
  | Ok res -> res

let updates_pair n new_ty ty =
  let rec aux_update acc n ty =
    if Z.equal n Z.zero then
      pair_simple (List.rev_append acc [ty])
    else
      match ty with
      | Pair (_ :: sub_miche_l, _) when Z.equal n Z.one ->
        pair_simple (List.rev_append acc (new_ty :: sub_miche_l))
      | Pair (x :: sub_miche_l, _) ->
        let new_n = Z.sub n (Z.of_int 2) in
        Result.bind (pair_simple sub_miche_l) (aux_update (x :: acc) new_n)
      | _ -> Error (UnstackMicheline "Not good for a UPDATE n instruction")
  in
  aux_update [] n ty

let get_annot (n : t) : string list =
  match n with
  | Name (_, m) -> (
    match m with
    | Mprim { annots; _ } -> annots
    | _ -> [] (* maybe fail this case should never happen*))
  | Option (_, annots) | Pair (_, annots) | Or (_, _, annots) -> annots

(*let map_nested_node (f :  (string * micheline) -> *)

module Ordered_nested = struct
  type nonrec t = t

  let compare = equal
end

let break_pair (nested : t) =
  let rec aux_break depth acc = function
    | Pair (t_l, []) -> List.fold_left (aux_break depth) acc t_l
    | Pair (t_l, annot) ->
      let depth =
        match Factori_utils.get_name_annots annot with
        | None -> depth
        | Some r -> r :: depth in
      List.fold_left (aux_break depth) acc t_l
    | res -> (List.rev depth, res) :: acc in
  aux_break [] [] nested

let string_field (prefix : string) (l : t) =
  let annots = get_annot l in
  let name =
    match Factori_utils.get_name_annots annots with
    | None -> Micheline_lib.string_of_micheline (deconstruct l)
    | Some name -> name in
  Format.sprintf "%s.%s" prefix name

let print ?(verbose = false) (ppf : Format.formatter) (l : t) =
  let default l =
    Format.fprintf ppf "%s" (Micheline_lib.string_of_micheline (deconstruct l))
  in
  if not verbose then
    match l with
    (* Concise printing of big_maps *)
    | Name (_n, Mprim { prim = `big_map; args = _; annots }) -> (
      match Factori_utils.get_name_annots annots with
      | None -> default l
      | Some name -> Format.fprintf ppf "%s" name)
    | _ -> default l
  else
    default l

module Constant = Map.Make (Ordered_nested)
