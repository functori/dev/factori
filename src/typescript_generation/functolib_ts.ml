(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file `functolib_ts.ml` contains utility functions and
configurations for generating TypeScript code to interact with Tezos smart
contracts. It includes functions for encoding and decoding Michelson
expressions, decoding different Tezos types, decoding tuples, and performing
transactions. Additionally, the file defines asynchronous functions for
transferring tokens and revealing public keys. However, there are some functions
that are not fully implemented, and some of the functions do not have example
usage provided. Warning: this short summary was automatically generated and
could be incomplete or misleading. If you feel this is the case, please open an
issue. *)

open Typescript_boilerplate_tuple_generation
open Typescript_generator_generation
open Format

let functolib_ts ppf () =
  fprintf ppf
    {|import {
  TezosToolkit,
  BigMapAbstraction,
  MichelsonMap,
  OriginationOperation,
  OpKind,
  createTransferOperation,
  TransferParams,
  RPCOperation,
  createRevealOperation
} from "@taquito/taquito"
import { MichelsonV1Expression, MichelsonV1ExpressionExtended, MichelsonV1ExpressionBase } from "@taquito/rpc"
import { encodeOpHash, encodeExpr } from '@taquito/utils';
import { RpcClient } from '@taquito/rpc';
import { BigMapKeyType, Schema } from '@taquito/michelson-encoder';
import { HttpResponseError, STATUS_CODE } from '@taquito/http-utils';
import { fail, notDeepEqual } from "assert";

import { InMemorySigner, importKey } from "@taquito/signer";

export var JSONbig = require('json-bigint')({ storeAsString: true, useNativeBigInt: true });;

export let debug = 0

export function set_debug(i : number){
  debug = i
}


export function output_debug(msg : string, level : number = 1){
if (debug >= level) {
  console.log(msg);
  return
} else {
  return;
}
}


/* Configurations for various networks */

export let config = {
  node_addr: 'http://127.0.0.1:20000',
  usleep: 1000, // 50 milliseconds
  wait_for_block_x_levels: 10
}

export let flextesa_config = {
  node_addr: 'http://127.0.0.1:20000',
  usleep: 1000, // 50 milliseconds
  wait_for_block_x_levels: 10
}

export let ithacanet_config =
{
  node_addr: 'https://ithacanet.tezos.marigold.dev',
  usleep: 1000,
  wait_for_block_x_levels: 10
}

export let ghostnet_config =
{
  node_addr: 'https://ghostnet.tezos.marigold.dev',
  usleep: 1000,
  wait_for_block_x_levels: 10
}

export let jakartanet_config =
{
  node_addr: 'https://jakartanet.tezos.marigold.dev',
  usleep: 1000,
  wait_for_block_x_levels: 10
}

export let mainnet_config =
{
  node_addr: 'https://tz.functori.com',
  usleep: 1000,
  wait_for_block_x_levels: 10
}

export function get_node(network : string){
  switch (network) {
    case "sandbox":
      return config.node_addr
    case "flextesa":
      return flextesa_config.node_addr
    case "jakarta":
      return jakartanet_config.node_addr
    case "jakartanet":
      return jakartanet_config.node_addr
    case "ithaca":
      return ithacanet_config.node_addr
    case "ithacanet":
      return ithacanet_config.node_addr
    case "ghostnet":
      return ghostnet_config.node_addr
    case "mainnet":
      return mainnet_config.node_addr
    default:
      throw ("Unknown Network: " + network)
  }
}

/* End : configurations for various networks */


/* Keys for Alice and Bob on Flextesa */

export let bob_flextesa : wallet =
  {
    pkh : "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6",
    pk : "edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4",
    sk : "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt",
  }

export let alice_flextesa : wallet =
  {
    pkh : "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb",
    pk : "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn",
    sk : "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq",
  }

export let bootstrap1 : wallet =
  {
    pkh : "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx",
    pk : "edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav",
    sk: "unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh"
  }

export function get_default_identity(network : string){
  switch (network) {
    case "ithacanet":
      return alice_flextesa
    case "jakartanet":
      return alice_flextesa
    case "flextesa":
      return alice_flextesa
    case "sandbox":
      return bootstrap1
    default:
      throw ("Unknown Network or no default identity for network: " + network)
  }
}

//export const client = new RpcClient(config.node_addr);

export type wallet = { sk: string; pk: string; pkh: string }

export function setSigner(tezosKit: TezosToolkit, sk: any) {
  tezosKit.setProvider({
      signer: new InMemorySigner(sk),
  });
}

type lambda_params = {from : MichelsonV1Expression; to_ : MichelsonV1Expression; body : MichelsonV1Expression }

export type nat = BigInt;
export type int = BigInt;
export type tez = int;
export type unit = any;
export type address = string;
export type bool = boolean;
export type timestamp = string;
export type bytes = string;
export type key = string;
export type key_hash = string
export type signature = string
export type lambda<T1,T2> = lambda_params

export type list<T> = T[]
export type option<T>=T|null
export type big_map<K,V> = {kind : "literal", value : [K,V][]} | {kind : "abstract", value : BigInt}
export type map<K,V> = [K,V][]
export type contract = MichelsonV1Expression
export type operation = MichelsonV1Expression // TODO
export type never_type = {kind : "never"}
export type chain_id = string
export type ticket<T> = { kind : "ticket", value : T}

export function make_abstract_bm<K,V>(i : BigInt){
  var res : big_map<K,V> = {kind : "abstract", value : i}
  return res
}

export function make_literal_map<K,V>(l : [K,V][]) : map<any,any>{
  var res : map<K,V> = l;
  return res
}

export function make_literal_bm<K,V>(l : [K,V][]) : big_map<any,any>{
  var res : big_map<K,V> = {kind : "literal", value : l};
  return res;
}

export function fail_on_micheline(msg : string,expr:MichelsonV1Expression) : void {
  console.log(`[fail_on_micheline][${msg}] : ${JSONbig.stringify(expr,null,2)}`);
  throw "fail_on_micheline"

}

export function isExtended(expr: MichelsonV1Expression): expr is MichelsonV1ExpressionExtended {
  return (expr as MichelsonV1ExpressionExtended).prim !== undefined;
}

function isList(expr: MichelsonV1Expression): expr is MichelsonV1Expression[] {
  output_debug("Entering isList with " + JSONbig.stringify(expr) + "\n")
  if (Array.isArray(expr)){
    if (expr.length == 0){
      return true
    }
    else{
      return expr.every((x) => {return (expr as MichelsonV1Expression) != undefined})
    }}
    else{
      return false
    }
  }



function isInt(expr: MichelsonV1Expression): expr is MichelsonV1ExpressionBase {
  return (expr as MichelsonV1ExpressionBase).int !== undefined;
}

function isBytes(expr: MichelsonV1Expression): expr is MichelsonV1ExpressionBase {
  output_debug("Entering isBytes with "+ JSONbig.stringify(expr))
  return (expr as MichelsonV1ExpressionBase).bytes !== undefined;
}

function isString(expr: MichelsonV1Expression): expr is MichelsonV1ExpressionBase {
  output_debug("Entering isString with "+ JSONbig.stringify(expr))
  return (expr as MichelsonV1ExpressionBase).string !== undefined;
}

function isBase(expr : MichelsonV1Expression) : expr is MichelsonV1ExpressionBase {
  return (isInt(expr) || isBytes(expr) || isString(expr));
}


export function extract_list_from_pair(expr : MichelsonV1Expression,msg : string ="record type") : MichelsonV1Expression[]{
  if(isExtended(expr)){
    switch (expr.prim) {
      case 'Pair':
        if (expr.args !== undefined){
        return expr.args}
        else{
        return [];}

      default:
        fail_on_micheline(msg,expr);
        return [];
    }
  }
  else{
    fail_on_micheline(msg,expr)
    return []
  }
}

export function is_left_or_right(expr : MichelsonV1Expression) : boolean{
  if(isExtended(expr)){
    switch(expr.prim){
      case('Left'):
        return true;
      case('Right'):
        return true;
      default:
        return false;
    }

  }
  else {
    return false;
  }
}

export function retrieve_field_index_from_sumtype_typescript(expr : MichelsonV1Expression): [number,MichelsonV1Expression]{
  const aux = function(res : number, e : MichelsonV1Expression) : [number,MichelsonV1Expression]{
    if(isExtended(e)){
      switch (e.prim) {
        case 'Left':
          if (e.args !== undefined){
            if(e.args.length == 1){
              return aux(res,e.args[0]);
            }
            else{
              return [res,e]
            }
          }
          else{
          return [res,e];}

        case 'Right':
          if (e.args !== undefined){
            if(e.args.length == 1){
              return aux(res+1,e.args[0]);
            }
            else{
              return [res,e]
            }
          }
          else{
          return [res,e];}

        default:
          return [res,e]
      }
    }
    else{
      return [res,e]
    }
    }
    return aux(0,expr);
  }


  export function arrayEquals(a : any, b : any) {
    return Array.isArray(a) &&
      Array.isArray(b) &&
      a.length === b.length &&
      a.every((val, index) => val === b[index]);
  }

  export function retrieve_path_from_sumtype_typescript(expr : MichelsonV1Expression): [boolean[],MichelsonV1Expression]{
    const aux = function(res : boolean[], e : MichelsonV1Expression) : [boolean[],MichelsonV1Expression]{
      if(isExtended(e)){
        switch (e.prim) {
          case 'Left':
            if (e.args !== undefined){
              if(e.args.length == 1){
                return aux(res.concat([true]),e.args[0]);
              }
              else{
                return [res.concat([true]),e]
              }
            }
            else{
            return [res,e];}

          case 'Right':
            if (e.args !== undefined){
              if(e.args.length == 1){
                return aux(res.concat([false]),e.args[0]);
              }
              else{
                return [res.concat([false]),e]
              }
            }
            else{
            return [res,e];}

          default:
            return [res,e]
        }
      }
      else{
        return [res,e]
      }
      }
      return aux([],expr);
    }


export function operation_encode(o : operation) : MichelsonV1Expression{
  return o
}

export function bytes_encode(b : bytes) : MichelsonV1Expression{
  return {bytes : b}
}

export function key_encode(k : key) : MichelsonV1Expression{
  return {string : k}
}

export function key_hash_encode(k : key_hash) : MichelsonV1Expression{
  return {string : k}
}

export function timestamp_encode(t : timestamp) : MichelsonV1Expression{
   return {string : t.toString()}
}

export function bool_encode(b : bool) : MichelsonV1Expression {
  return b? {prim : 'True'} : {prim: 'False'}
}

export function string_encode(s : string) : MichelsonV1Expression {
  return {string : s}
}

export function address_encode(a : address) : MichelsonV1Expression {
  return string_encode(a);
}

export function chain_id_encode(ci : chain_id) : MichelsonV1Expression {
  return string_encode(ci);
}

export function make_unit () : MichelsonV1Expression {return { prim : "Unit"}}

export let unit_micheline : MichelsonV1Expression = {prim : "unit"}

export function unit_micheline_with_annot(annot : string) : MichelsonV1Expression {
return {prim : "unit", annots : [annot]}
}

export let nat_micheline : MichelsonV1Expression = {prim : "nat"}

export let int_micheline : MichelsonV1Expression = {prim : "int"}

export let tez_micheline : MichelsonV1Expression = {prim : "mutez"}

export let address_micheline : MichelsonV1Expression = {prim : "address"}

export let never_type_micheline : MichelsonV1Expression = {prim : "never"}

export let timestamp_micheline : MichelsonV1Expression = {prim : "timestamp"}

export let bool_micheline : MichelsonV1Expression = {prim : "bool"}

export let operation_micheline : MichelsonV1Expression = {prim : "operation"}

export let key_hash_micheline : MichelsonV1Expression = {prim : "key_hash"}

export function make_or_micheline(t1 : MichelsonV1Expression,t2 : MichelsonV1Expression) : MichelsonV1Expression
  {return {prim : "OR",args : [t1,t2]}}

export function option_micheline(a_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "option", args : [a_micheline]}
}

export function ticket_micheline(a_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "TICKET", args : [a_micheline]}
}

export function list_micheline(a_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "list", args : [a_micheline]}
}

export function set_micheline(a_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "set", args : [a_micheline]}
}

export function map_micheline(a_micheline : MichelsonV1Expression, b_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "map", args : [a_micheline,b_micheline]}
}

export function big_map_micheline(a_micheline : MichelsonV1Expression, b_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "big_map", args : [a_micheline,b_micheline]}
}

export function lambda_micheline(a_micheline : MichelsonV1Expression, b_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "lambda", args : [a_micheline,b_micheline]}
}

export let string_micheline : MichelsonV1Expression = {prim : "string"}

export let chain_id_micheline : MichelsonV1Expression = string_micheline

export let signature_micheline = {prim : "signature"}

export let key_micheline = {prim : "key"}

export let bytes_micheline = {prim : "bytes"}

export function contract_micheline(a_micheline : MichelsonV1Expression): MichelsonV1Expression
{
return {prim : "CONTRACT", args : [a_micheline]}
}

export function never_type_encode(n : never_type) : MichelsonV1Expression {
  throw "Cannot build a Michelson value of type never"
}

export function nat_encode(n : nat): MichelsonV1Expression {
  return {int : n.toString()};
}

export function int_encode(n : int): MichelsonV1Expression {
  return {int : n.toString()};
}

export function tez_encode(n : tez): MichelsonV1Expression {
  return {int : n.toString()};
}

export function signature_encode(s : signature): MichelsonV1Expression {
  return {string : s}
}

export function unit_encode(x : unit) : any {
  return {prim : "Unit"};
}

export function unit_decode(x : MichelsonV1Expression) : unit {
  return null;
}

export function ticket_encode<T>(t_encode : (t : T) => MichelsonV1Expression) : (x : ticket<T>) => MichelsonV1Expression{
  return (x : ticket<T>) =>
  {
    return {prim : "Ticket", args : [t_encode(x.value)]}
  }
}

export function list_encode<Type>(t_encode : (t : Type) => MichelsonV1Expression) : (x : Type[]) => MichelsonV1Expression{
  return (x : Type[]) =>
  {
    return x.map(function(v) : MichelsonV1Expression{
      return (t_encode(v))
    })
  }
}

export function set_encode<Type>(t_encode : (t : Type) => MichelsonV1Expression) : (x : Type[]) => MichelsonV1Expression{
  return (x : Type[]) =>
  {
    return x.map(function(v) : MichelsonV1Expression{
      return (t_encode(v))
    })
  }
}
export function option_encode<Type>(t_encode : (t : Type) => MichelsonV1Expression) : (x : option<Type>) => MichelsonV1Expression{
  return (x : option<Type>) =>
  {
switch (x) {
  case null:
    return {prim : 'None'}

  default:
    return{prim : 'Some',args:[t_encode(x)]}
}
  }
}

export function map_encode<K,V>(k_encode : (k : K) => MichelsonV1Expression,v_encode : (v : V) => MichelsonV1Expression)
{return (bm : map<K,V>) =>
  {var res : MichelsonV1Expression[] = [];
    bm.forEach( (pair) => {
    let k : K = pair[0];
    let v : V = pair[1];
    let new_pair = {prim : 'Elt', args : [k_encode(k),v_encode(v)]}
    res.push(new_pair);
  })
return res;}
}

export function big_map_encode<K,V>(k_encode : (k : K) => MichelsonV1Expression,v_encode : (v : V) => MichelsonV1Expression)
{return (bm : big_map<K,V>) : MichelsonV1Expression =>{

  if((bm.kind == "abstract")){
    var res1 : MichelsonV1Expression[] = []/* {int : bm.value.toString()} */
    return(res1);
  }
  else{
    var res2 : MichelsonV1Expression[] = [];
    bm.value.forEach( (pair) => {
    let k : K = pair[0];
    let v : V = pair[1];
    let new_pair = {prim : 'Elt', args : [k_encode(k),v_encode(v)]}
    res2.push(new_pair);
  })
return res2;}}
}

export function lambda_encode<K,V>(k_encode : (k : K) => MichelsonV1Expression,v_encode : (v : V) => MichelsonV1Expression)
{
  return (l : lambda<K,V>) =>
  {return {prim : `LAMBDA`,args : [l.from,l.to_,l.body]}}
}


export function contract_encode(e : MichelsonV1Expression) : (x : contract) => MichelsonV1Expression
{
  return (x : contract) =>
  {
    return x // TODO
  }
}

export function general_int_decode(m : MichelsonV1Expression,specific_message : string) : nat{
  if(isInt(m)){
    if (m.int == undefined){
      fail_on_micheline(specific_message,m);
      throw specific_message;
    }
    else{
    return BigInt(m.int)
    }
  }
  else{
    fail_on_micheline(specific_message,m);
    throw specific_message;
  }
}

export function general_string_decode(m : MichelsonV1Expression,specific_message : string) : string{
  if(isString(m)){
    if(m.string !== undefined){
      return m.string;
    }else{
      fail_on_micheline(specific_message,m)
        throw specific_message;
    }
  }
  else{
    fail_on_micheline(specific_message,m);
    throw specific_message;
  }
}

export function string_decode(m : MichelsonV1Expression) : string {
  return general_string_decode(m,"string_decode")
}

export function address_decode(m : MichelsonV1Expression) : address{
  return general_string_decode(m,"address_decode")
}

export function chain_id_decode(m : MichelsonV1Expression) : address{
  return general_string_decode(m,"chain_id_decode")
}

export function never_type_decode(m : MichelsonV1Expression) : never_type {
  return {kind : "never"}
}

export function bytes_decode(m : MichelsonV1Expression) : bytes{
  if(isBytes(m) && m.bytes != undefined){
    return m.bytes
  }
  else{
    fail_on_micheline("bytes_decode",m)
    throw "bytes_decode"
  }
}

export function key_decode(m : MichelsonV1Expression) : bytes{
  return general_string_decode(m,"key_decode")
}

export function signature_decode(m : MichelsonV1Expression) : signature{
  return general_string_decode(m,"signature")
}

export function key_hash_decode(m : MichelsonV1Expression) : bytes{
  return general_string_decode(m,"key_hash_decode")
}

export function nat_decode(m : MichelsonV1Expression) : nat{
 return (general_int_decode(m,"nat_decode"))
}

export function int_decode(m : MichelsonV1Expression) : int{
  return (general_int_decode(m,"int_decode"))
 }

export function tez_decode(m : MichelsonV1Expression) : tez{
  return (general_int_decode(m,"tez_decode"))
}

export function timestamp_decode(m : MichelsonV1Expression) : timestamp {
 return (general_string_decode(m,"timestamp_decode"))
}

export function bool_decode(m : MichelsonV1Expression) : boolean{
  if(isExtended(m)){
    switch (m.prim) {
      case 'True':
        return true;

      case 'False':
        return false;

      default:
        fail_on_micheline("bool_decode",m)
        throw "bool_decode"
}}
else{
  fail_on_micheline("bool_decode",m)
  throw "bool_decode"
}}


export function option_decode<Type>(t_decode :  (m : MichelsonV1Expression) => Type) : (m : MichelsonV1Expression) => option<Type>{
  return (m : MichelsonV1Expression) =>
  {
    if (isExtended(m))
    {switch (m.prim) {
      case 'Some':
        if (m.args != undefined && m.args.length == 1)
        {return t_decode(m.args[0])}
        else
        {return null}
      case 'None':
        return null
      default:
        {fail_on_micheline("option_decode",m); throw "option decode"}
    }
  }
    else
    {fail_on_micheline("option_decode",m); throw "option decode"}
}}

export function ticket_decode<T>(t_decode : (m : MichelsonV1Expression) => T) : (x : MichelsonV1Expression) => ticket<T>{
  return (m : MichelsonV1Expression) =>
  {
    if (isExtended(m))
    {switch(m.prim) {
      case 'Ticket':
        if(m.args != undefined && m.args.length ==1)
        {return {kind : 'ticket', value : t_decode(m.args[0])}}
        else
        {fail_on_micheline("ticket_decode",m); throw "ticket decode"}
      default:
        {fail_on_micheline("ticket_decode",m); throw "ticket decode"}
    }}
    else
    {fail_on_micheline("ticket_decode",m); throw "ticket decode"}
  }
}

export function list_decode<Type>(t_decode :  (m : MichelsonV1Expression) => Type) : (x : MichelsonV1Expression) => list<Type>{
  return (x : MichelsonV1Expression) =>
  {
    if(isList(x))
    {
      return x.map(function(v : MichelsonV1Expression) : Type{
      return (t_decode(v))
    })
  }
    else
    {fail_on_micheline("list_decode",x)
    throw ""}
  }
}

export function set_decode<Type>(t_decode :  (m : MichelsonV1Expression) => Type) : (x : MichelsonV1Expression) => list<Type>{
  return (x : MichelsonV1Expression) =>
  {
    if(isList(x))
    {
      return x.map(function(v : MichelsonV1Expression) : Type{
      return (t_decode(v))
    })
  }
    else
    {fail_on_micheline("set_decode",x)
    throw ""}
  }
}

export function big_map_decode<K,V>(k_decode : (m : MichelsonV1Expression) => K,v_decode : (m : MichelsonV1Expression) => V) :
(m : MichelsonV1Expression) => big_map<K,V>
{return (m : MichelsonV1Expression) =>
  {if((isInt(m) && m.int !== undefined)) {
    return {kind : "abstract", value : BigInt (m.int)}
  }
  else
  {
    if(isList(m)){
    var value : [K,V][] = []
    m.forEach( (x : MichelsonV1Expression) =>
    {
      if(isExtended(x) && x.prim == 'Elt' && x.args !== undefined && x.args.length == 2){
        let x1 = k_decode(x.args[0])
        let x2 = v_decode(x.args[1])
        value.push([x1,x2])
      }
    }
    )
    var res : big_map<K,V> = {kind : "literal", value}
    return res;
    }
    else
    {
    fail_on_micheline("big_map_decode",m);
    throw "Could not decode big_map"
  }
}
}
}

export function lambda_decode<K,V>(k_decode : (m : MichelsonV1Expression) => K,v_decode : (m : MichelsonV1Expression) => V) :
(m : MichelsonV1Expression) => lambda<K,V>
{return (m : MichelsonV1Expression) =>
  {if(isExtended(m) && m.prim == 'LAMBDA' && m.args !== undefined && m.args.length == 3){
    let res : lambda<K,V> = {from : m.args[0],to_ : m.args[1], body : m.args[2]}
    return res
  }
  else{
    let res : lambda<K,V> = {from : {prim : "unit"}, to_ : {prim : "unit"}, body : m}
    return res}
}
}

export function map_decode<K,V>(k_decode : (m : MichelsonV1Expression) => K,v_decode : (m : MichelsonV1Expression) => V) :
(m : MichelsonV1Expression) => map<K,V>
{return (m : MichelsonV1Expression) =>
  {if(isList(m)){
    var value : [K,V][] = []
    m.forEach( (x : MichelsonV1Expression) =>
    {
      if(isExtended(x) && x.prim == 'Elt' && x.args != undefined && x.args.length == 2){
        let x1 = k_decode(x.args[0])
        let x2 = v_decode(x.args[1])
        value.push([x1,x2])
      }
    }
    )
    var res : map<K,V> = value
    return res;
    }
    else
    {
    fail_on_micheline("map_decode",m);
    throw "Could not decode big_map"
  }
}
}

export function contract_decode(m : MichelsonV1Expression) : (x : MichelsonV1Expression) => contract
{
  return (m : MichelsonV1Expression) =>
  {
   return m;
  }
}

%a

%a

%a

%a

/* This may seem superfluous but it is useful as a base case */
export function tuple1_decode<T1>(x_decode : (x : MichelsonV1Expression) => T1) : (x : MichelsonV1Expression) => T1{
  return (x : MichelsonV1Expression) =>
  {
    return x_decode(x)
  }
}

export function tuple2_decode<T1,T2>(x_decode : (x : MichelsonV1Expression) => T1,y_decode : (y : MichelsonV1Expression) => T2) :
(x : MichelsonV1Expression) => [T1,T2]
/* : (x : [T1,T2]) => MichelsonV1Expression */
{
  return (x : MichelsonV1Expression) =>
    {
      output_debug("Entering tuple2_decode with input" + JSONbig.stringify(x))
      if(isExtended(x)){
        output_debug("Entering the extended subcase with " + JSONbig.stringify(x))
        if(x.prim == 'Pair' && x.args !== undefined){
          if(x.args.length < 2){
            fail_on_micheline("tuple2 with less than 2 arguments",x)
            throw ("tuple2_decode")
          }
          else{
            if(x.args.length == 2){
              output_debug("Entering the extended subcase with |args| == 2 ")
              output_debug("First argument : " + JSONbig.stringify(x.args[0]))
              let x1 = x_decode(x.args[0])
              output_debug("Second argument : " + JSONbig.stringify(x.args[1]))
              let x2 = y_decode(x.args[1])
              return [x1,x2]
            }
            else{
              output_debug("Entering the extended subcase with |args| >= 2 ")
              let x1 = x_decode(x.args[0])
              let x2 = tuple1_decode(y_decode)(x.args.splice(1))
              return [x1,x2]
            }
          }
          //return [x_decode(x.args[0]),y_decode(x.args[1])]
        }
        else{
          fail_on_micheline("tuple2_decode",x)
          throw "tuple2_decode"
        }
      }
      else{
        if(isList(x)){
          output_debug("Entering the list subcase with " + JSONbig.stringify(x))
          if(x.length < 2){
            fail_on_micheline("tuple2 with less than 2 arguments",x)
            throw ("tuple2_decode")
          }
          else{
            if(x.length == 2){
              return([x_decode(x[0]),y_decode(x[1])])
            }
            else{
              return([x_decode(x[0]),y_decode(x.splice(1))])
            }
          }

        }
        else{
        fail_on_micheline("tuple2_decode",x)
        throw "tuple2_decode"
      }
    }
}
}

%a



export function operation_decode(m : MichelsonV1Expression) : operation{
  return m
}

function toStrRec(input: any): any {
  Object.keys(input).forEach(k => {
    let elt = input[k]
    if (elt === undefined || elt === null) {
      input[k] = undefined
    } else if (typeof elt === 'object') {
      input[k] = toStrRec(elt);
    } else {
      input[k] = elt.toString();
    }
  });
  return input;
}


export interface operation_result {
  hash: string | null;
  level: number;
  error: any;
}
/**
 * Generic auxiliary function for transfers and contracts calls
 * @param tk : Tezos toolkit
 * @param transferParams : the transfer's parameters
 * @returns injection result
 */
 async function make_transactions(tk: TezosToolkit, transfersParams: Array<TransferParams>,debug=false): Promise<operation_result> {
  var opHash = null;
  var level = 0;
  try {
    let source = await tk.signer.publicKeyHash();
    let contract = await tk.rpc.getContract(source);
    let counter = parseInt(contract.counter || '0', 10)
    let contents: Array<RPCOperation> = []
    await Promise.all(
      transfersParams.map(async function (transferParams) {
        try {let estimate = await tk.estimate.transfer(transferParams);
        if(debug){console.log(`[make_transactions] estimate : ${JSON.stringify(estimate)}`)}
        const rpcTransferOperation = await createTransferOperation({
          ...transferParams,
          fee: estimate.suggestedFeeMutez,
          gasLimit: estimate.gasLimit,
          storageLimit: estimate.storageLimit
        });
        counter++;
        let v = {
          ...rpcTransferOperation,
          source,
          counter: counter,
        };
        contents.push(v)
  } catch (error){
    console.log(`Estimate error: ${error}`)
  }
      }));
    let header = await tk.rpc.getBlockHeader();
    level = header.level;
    let op = toStrRec({
      branch: header.hash,
      contents: contents
    })
    if(debug)console.log(`[make_transactions] op : ${JSON.stringify(op)}`)
    let forgedOp = await tk.rpc.forgeOperations(op)
    let signOp = await tk.signer.sign(forgedOp, new Uint8Array([3]));
    opHash = encodeOpHash(signOp.sbytes);
    if(debug)console.log(`[make_transactions] opHash : ${JSON.stringify(opHash)}`)
    try {let injectedoOpHash = await tk.rpc.injectOperation(signOp.sbytes)
       console.assert(injectedoOpHash == opHash);
       let res = { hash: opHash, level: level, error: null }
       if(debug)console.log(`res : ${JSON.stringify(res)}`)
       config.node_addr = tk.rpc.getRpcUrl()
       await wait_inclusion(res,config,debug);
       return res
     }
     catch(error){
       console.log(`[make_transactions] error : ${error}`)
     }
  } catch (error) {
    return { hash: opHash, level: level, error: error }
  }
}

// drain from stack exchange https://tezos.stackexchange.com/questions/4531/draining-a-tz1-account/4532#4532
export async function drain(node_addr : string, id: wallet,debug) {
  try{
  const Tezos = new TezosToolkit(node_addr);
  const sender_pkh = id.pkh
  const receiver_pkh = alice_flextesa.pkh
  setSigner(Tezos,id.sk);
  const balance = await Tezos.tz.getBalance(sender_pkh);
  if(balance.toNumber() == 0){
    return
  }
  const estimate = await Tezos.estimate.transfer({
      to: receiver_pkh,
      amount: 0.5,
  });

  // // Emptying the account
  const totalFees = estimate.suggestedFeeMutez + estimate.burnFeeMutez;
  const maxAmount = balance.minus(totalFees).toNumber();
  // // Temporary fix, see https://gitlab.com/tezos/tezos/-/issues/1754
  // // we need to increase the gasLimit and fee returned by the estimation
  const gasBuffer = 500;
  const MINIMAL_FEE_PER_GAS_MUTEZ = 0.1;
  const increasedFee = (gasBuffer: number, opSize: number) => {
       return (gasBuffer) * MINIMAL_FEE_PER_GAS_MUTEZ + opSize
   }

  const opTransfer = await Tezos.contract.transfer({
       to: receiver_pkh,
       mutez: true,
       amount: maxAmount - increasedFee(gasBuffer, Number(estimate.opSize)),
       fee: estimate.suggestedFeeMutez + increasedFee(gasBuffer, Number(estimate.opSize)), // baker fees
       gasLimit: estimate.gasLimit + gasBuffer,
       storageLimit: estimate.storageLimit
   });

   await opTransfer.confirmation();
   const finalBalance = await Tezos.tz.getBalance(sender_pkh);
   if(debug)console.log(`[drain] finalBalance: ${finalBalance}`)}
   catch(error){
    console.log(`[drain] Error: ${error}`)
   }
}
export async function reveal(node_addr : string, identity : wallet,cfg=config,debug=false){
const tk = new TezosToolkit(node_addr);
let header = await tk.rpc.getBlockHeader();
let level = header.level;
setSigner(tk,identity.sk);
let source = identity.pkh;
if(debug)console.log(`[reveal] revealing ${source}`);
let contract = await tk.rpc.getContract(source);
let counter = parseInt(contract.counter || '0', 10)
let contents: Array<RPCOperation> = []; // This will contain the contents of the TX

let revealEstimate = await tk.estimate.reveal();
if(debug)console.log(`[reveal] Reveal estimate: ${JSON.stringify(revealEstimate)}`)
if(revealEstimate == undefined) {console.log(`[reveal] Revelation not needed for ${source}`); return}
if (revealEstimate !== undefined) { // Pk revelation needed. Put it at the beginning
  let publicKey = identity.pk;
  let revealParams = {
    fee: revealEstimate.suggestedFeeMutez,
    gasLimit: revealEstimate.gasLimit,
    storageLimit: revealEstimate.storageLimit
  };
  let rpcRevealOperation = await createRevealOperation(revealParams, source, publicKey);
  counter++;
  contents.push(({ ...rpcRevealOperation, source, counter: counter } as RPCOperation))
}
let op = toStrRec({
  branch: header.hash,
  contents: contents
})
if(debug)console.log(`[reveal] op : ${JSON.stringify(op)}`)
    try {
       let forgedOp = await tk.rpc.forgeOperations(op)
       let signOp = await tk.signer.sign(forgedOp, new Uint8Array([3]));
       let opHash = encodeOpHash(signOp.sbytes);
       let injectedoOpHash = await tk.rpc.injectOperation(signOp.sbytes)
       console.assert(injectedoOpHash == opHash);
       let res = { hash: opHash, level: level, error: null }
       if(debug)console.log(`res : ${JSON.stringify(res)}`)
       await wait_inclusion(res,cfg,debug)
       return res
     }
     catch(error){
       console.log(`[reveal] error : ${error}`)
       throw error
     }
}

export async function send(
  tk: TezosToolkit,
  kt1: string,
  entrypoint: string,
  value: MichelsonV1Expression,
  amount: number = 0,debug=false): Promise<operation_result> {
  try {
    return await make_transactions(tk, [{
      amount: amount,
      to: kt1,
      parameter: { entrypoint, value },
      mutez:true
    }],debug);
  } catch (error) {
    console.log(`[send]]: ${JSONbig.stringify(error, null, 2)}`);
    console.log(`[send]]: ${error}`);
    return { hash: null, level: -1, error: error };
  }
}


export function usleep(ms: number): Promise<void> {
  return new Promise(resolve => setTimeout(resolve, ms));
}


export async function wait_inclusion(
  tx: operation_result,
  cfg: any,
  debug = false) {
  const client = new RpcClient(cfg.node_addr);
  let start_lvl = tx.level;
  let opH = tx.hash;
  if (debug) {
      console.log(`[wait_inclusion] Waiting inclusion of operation ${opH} at level ${start_lvl}`);
      //console.log("[wait_inclusion] " + stringify(tx, null, 2));
  }
  var end_lvl = start_lvl + cfg.wait_for_block_x_levels;
  var authorized_failures = 10;
  let getBlock = async function (hash : any): Promise<any> {
      var b = null;
      while (b === null) {
          try {
              if (hash == null) {
                  b = await client.getBlock();
              } else {
                  b = await client.getBlock(hash);
              }
          } catch (error) {
              authorized_failures--;
              if (debug) {
                  console.log("[wait_inclusion/getBlock] " + error);
                  //console.log("[wait_inclusion/getBlock]" + stringify(error));
              }
              if (authorized_failures <= 0) {
                  process.exit(0);
              }
          }
      }
      return b;
  };
  let retro_inspection: any = async function (b: any) {
      var b = await getBlock(b.header.predecessor);
      if (debug) {
          console.log(`[wait_inclusion/retro_inspection] Retro inspect level ${b.header.level} for operation ${opH}`)
      };
      let found_op = b.operations[3].find((op : any) => op.hash === opH);
      if (found_op !== undefined) {
          return { block: b, included: true, op: found_op }
      }
      if (b.header.level < start_lvl) {
          throw "Operation not found"
      }
      return await retro_inspection(b);
  };
  let aux: any = async function () {
      var b = await getBlock(null);
      var level = b.header.level;
      if(debug){console.log(`Waiting inclusion ... block level is ${level}`);}
      let found_op = b.operations[3].find((op : any) => op.hash === opH);
      if (found_op !== undefined) {
          return { block: b, included: true, op: found_op }
      }
      if (end_lvl < level) {
          try {
              await retro_inspection(b);
          } catch (error) {
              console.log(error);
              return { block: b, included: false, op: null }
          }
      }
      while (level == b.header.level) {
          await usleep(cfg.usleep);
          try {
              let b2 = await client.getBlock();
              level = b2.header.level;
          } catch (error) {
              authorized_failures--;
              console.log(`[wait_inclusion] Error: ${JSONbig.stringify(error)}`);
              if (authorized_failures <= 0) {
                  process.exit(0);
              }
          }
      }
      var res = await aux();
      return res;
  }
  if (opH == null) {
      throw ('[wait_inclusion] Cannot monitor an operation whose hash is null');
  }
  return await aux()
               }
     
async function findBigMapTezos<T extends BigMapKeyType,A>(tezosKit : TezosToolkit,bigMapId : BigInt, bigMapKey : T, key_micheline : MichelsonV1Expression, value_micheline : MichelsonV1Expression, decoding : (value : MichelsonV1Expression) => A) : Promise<A>{
  const bigMapType = big_map_micheline(key_micheline,value_micheline);
  const bigMapSchema = new Schema(bigMapType);
  const { key, type }  = bigMapSchema.EncodeBigMapKey(bigMapKey);
  const { packed } = await tezosKit.rpc.packData({ data: key, type });
  const encodedExpr = encodeExpr(packed);
  try {
    let big_map_res = await tezosKit.rpc.getBigMapExpr(bigMapId.toString(),encodedExpr).then((value) => { return (decoding(value)); });
    return big_map_res;
  } catch (ex) {
    if (ex instanceof HttpResponseError && ex.status === STATUS_CODE.NOT_FOUND) {
      return;
    } else {
      throw ex;
    }
  }
}

export async function findBigMap<T extends BigMapKeyType,A>(tezosKit : TezosToolkit,bigMap : big_map<T,A>, bigMapKey : T, key_micheline : MichelsonV1Expression, value_micheline : MichelsonV1Expression, decoding : (value : MichelsonV1Expression) => A) : Promise<A>{
  if(bigMap.kind = "abstract"){
    return findBigMapTezos<T,A>(tezosKit,bigMap.value as BigInt,bigMapKey,key_micheline,value_micheline,decoding);
  }else{
    let l = bigMap.value as [T,A][];
    return (l.find((elt) => elt[0] == bigMapKey)[1]);
  }
}
|}
    auxiliary_functions ()
    (pp_print_list ~pp_sep:(Factori_utils.tag "\n\n") (fun ppf x ->
         fprintf ppf "export %a" build_generator x))
    (generate_all_kinds 2 50)
    (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n") encode)
    (k_to_n 2 50)
    (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n") micheline)
    (k_to_n 2 50)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
       (fun ppf d -> fprintf ppf "%a\n\n%a" concat d decode d))
    (k_to_n 3 50)
