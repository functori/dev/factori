open Factori_abstract_types.Abstract
module Interface = Fa2_y_ocaml_interface

type token_id = nat

type abstract_token_id = token_id

let value_of_token_id : token_id -> 'a = value_of_nat

let value_of_abstract_token_id = value_of_token_id

let lift_token_id : 'a -> abstract_token_id = lift_nat

type _operator = address

type abstract__operator = _operator

let value_of__operator : _operator -> 'a = value_of_address

let value_of_abstract__operator = value_of__operator

let lift__operator : 'a -> abstract__operator = lift_address

type add_operator = {
  token_id : abstract_token_id;
  _operator : abstract__operator;
  owner : abstract__operator;
}

type abstract_add_operator =
  add_operator Factori_abstract_types.Abstract.id_or_concrete

let value_of_add_operator (arg : add_operator) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "owner",
             value_of_abstract__operator arg.owner ),
         Abstract_value.PairP
           ( Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "_operator",
                 value_of_abstract__operator arg._operator ),
             Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_id",
                 value_of_abstract_token_id arg.token_id ) ) ))

let value_of_abstract_add_operator (x : abstract_add_operator) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_add_operator c

let lift_add_operator (arg : Interface.add_operator) : abstract_add_operator =
  Concrete
    {
      owner = lift__operator arg.owner;
      _operator = lift__operator arg._operator;
      token_id = lift_token_id arg.token_id;
    }

type type0 =
  | Add_operator of abstract_add_operator
  | Remove_operator of abstract_add_operator

type abstract_type0 = type0 Factori_abstract_types.Abstract.id_or_concrete

let value_of_type0 (arg : type0) =
  match arg with
  | Add_operator x ->
    Abstract_value.VSumtype
      (SumOrL
         (LeafOr
            ( Factori_utils.sanitized_of_str "Add_operator",
              value_of_abstract_add_operator x )))
  | Remove_operator x ->
    Abstract_value.VSumtype
      (SumOrR
         (LeafOr
            ( Factori_utils.sanitized_of_str "Remove_operator",
              value_of_abstract_add_operator x )))

let value_of_abstract_type0 (x : abstract_type0) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_type0 c

let lift_type0 (arg : Interface.type0) : abstract_type0 =
  match arg with
  | Add_operator x -> Concrete (Add_operator (lift_add_operator x))
  | Remove_operator x -> Concrete (Remove_operator (lift_add_operator x))

type update_operators = abstract_type0 list'

type abstract_update_operators = update_operators

let value_of_update_operators (arg : update_operators) : 'a =
  (value_of_list' value_of_abstract_type0) arg

let value_of_abstract_update_operators = value_of_update_operators

let lift_update_operators (arg : Interface.update_operators) : update_operators
    =
  (lift_list' lift_type0) arg

let mk_call_update_operators ?(assert_success = false) ?(msg = "") ~scenario
    ~from ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_update_operators param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"update_operators"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_update_operators ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_update_operators param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"update_operators"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type type1 = {
  amount : abstract_token_id;
  token_id : abstract_token_id;
  to_ : abstract__operator;
}

type abstract_type1 = type1 Factori_abstract_types.Abstract.id_or_concrete

let value_of_type1 (arg : type1) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "to_",
             value_of_abstract__operator arg.to_ ),
         Abstract_value.PairP
           ( Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_id",
                 value_of_abstract_token_id arg.token_id ),
             Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "amount",
                 value_of_abstract_token_id arg.amount ) ) ))

let value_of_abstract_type1 (x : abstract_type1) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_type1 c

let lift_type1 (arg : Interface.type1) : abstract_type1 =
  Concrete
    {
      to_ = lift__operator arg.to_;
      token_id = lift_token_id arg.token_id;
      amount = lift_token_id arg.amount;
    }

type type2 = {
  from_ : abstract__operator;
  txs : abstract_type1 list';
}

type abstract_type2 = type2 Factori_abstract_types.Abstract.id_or_concrete

let value_of_type2 (arg : type2) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "from_",
             value_of_abstract__operator arg.from_ ),
         Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "txs",
             (value_of_list' value_of_abstract_type1) arg.txs ) ))

let value_of_abstract_type2 (x : abstract_type2) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_type2 c

let lift_type2 (arg : Interface.type2) : abstract_type2 =
  Concrete
    {from_ = lift__operator arg.from_; txs = (lift_list' lift_type1) arg.txs}

type transfer = abstract_type2 list'

type abstract_transfer = transfer

let value_of_transfer (arg : transfer) : 'a =
  (value_of_list' value_of_abstract_type2) arg

let value_of_abstract_transfer = value_of_transfer

let lift_transfer (arg : Interface.transfer) : transfer =
  (lift_list' lift_type2) arg

let mk_call_transfer ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_transfer param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"transfer"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_transfer ~scenario ~network ~amount ~from ~kt1 ~expected
    ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_transfer param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"transfer"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type type3 = {
  token_id : abstract_token_id;
  token_info : (string', bytes) map;
}

type abstract_type3 = type3 Factori_abstract_types.Abstract.id_or_concrete

let value_of_type3 (arg : type3) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "token_id",
             value_of_abstract_token_id arg.token_id ),
         Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "token_info",
             (value_of_map value_of_string' value_of_bytes) arg.token_info ) ))

let value_of_abstract_type3 (x : abstract_type3) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_type3 c

let lift_type3 (arg : Interface.type3) : abstract_type3 =
  Concrete
    {
      token_id = lift_token_id arg.token_id;
      token_info = (lift_map lift_string' lift_bytes) arg.token_info;
    }

type token_metadata = {
  token_ids : abstract_token_id list';
  handler : lambda;
}

type abstract_token_metadata =
  token_metadata Factori_abstract_types.Abstract.id_or_concrete

let value_of_token_metadata (arg : token_metadata) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "token_ids",
             (value_of_list' value_of_abstract_token_id) arg.token_ids ),
         Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "handler",
             (value_of_lambda
                (value_of_list' value_of_abstract_type3)
                value_of_unit')
               arg.handler ) ))

let value_of_abstract_token_metadata (x : abstract_token_metadata) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_token_metadata c

let lift_token_metadata (arg : Interface.token_metadata) :
    abstract_token_metadata =
  Concrete
    {
      token_ids = (lift_list' lift_token_id) arg.token_ids;
      handler = (lift_lambda (lift_list' lift_type3) lift_unit') arg.handler;
    }

let mk_call_token_metadata ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_token_metadata param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"token_metadata"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_token_metadata ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_token_metadata param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"token_metadata"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type set_pause = bool'

type abstract_set_pause = set_pause

let value_of_set_pause : set_pause -> 'a = value_of_bool'

let value_of_abstract_set_pause = value_of_set_pause

let lift_set_pause : 'a -> abstract_set_pause = lift_bool'

let mk_call_set_pause ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_pause param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_pause"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_set_pause ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_set_pause param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"set_pause"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type set_administrator = _operator

type abstract_set_administrator = abstract__operator

let value_of_set_administrator : set_administrator -> 'a = value_of__operator

let value_of_abstract_set_administrator = value_of_abstract__operator

let lift_set_administrator : 'a -> abstract_set_administrator = lift__operator

let mk_call_set_administrator ?(assert_success = false) ?(msg = "") ~scenario
    ~from ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_set_administrator param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"set_administrator"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_set_administrator ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_set_administrator param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"set_administrator"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type payout_balance = unit'

type abstract_payout_balance = payout_balance

let value_of_payout_balance : payout_balance -> 'a = value_of_unit'

let value_of_abstract_payout_balance = value_of_payout_balance

let lift_payout_balance : 'a -> abstract_payout_balance = lift_unit'

let mk_call_payout_balance ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_payout_balance param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"payout_balance"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_payout_balance ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value =
    make_value scenario (value_of_abstract_payout_balance param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"payout_balance"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type mint = {
  token_info : (string', bytes) map;
  token_id : abstract_token_id;
  address : abstract__operator;
  amount : abstract_token_id;
}

type abstract_mint = mint Factori_abstract_types.Abstract.id_or_concrete

let value_of_mint (arg : mint) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.PairP
           ( Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "address",
                 value_of_abstract__operator arg.address ),
             Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "amount",
                 value_of_abstract_token_id arg.amount ) ),
         Abstract_value.PairP
           ( Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_id",
                 value_of_abstract_token_id arg.token_id ),
             Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_info",
                 (value_of_map value_of_string' value_of_bytes) arg.token_info
               ) ) ))

let value_of_abstract_mint (x : abstract_mint) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_mint c

let lift_mint (arg : Interface.mint) : abstract_mint =
  Concrete
    {
      address = lift__operator arg.address;
      amount = lift_token_id arg.amount;
      token_id = lift_token_id arg.token_id;
      token_info = (lift_map lift_string' lift_bytes) arg.token_info;
    }

let mk_call_mint ?(assert_success = false) ?(msg = "") ~scenario ~from ~amount
    ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_mint param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"mint"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_mint ~scenario ~network ~amount ~from ~kt1 ~expected
    ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_mint param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"mint"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type burn = {
  token_id : abstract_token_id;
  amount : abstract_token_id;
  address : abstract__operator;
}

type abstract_burn = burn Factori_abstract_types.Abstract.id_or_concrete

let value_of_burn (arg : burn) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "address",
             value_of_abstract__operator arg.address ),
         Abstract_value.PairP
           ( Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "amount",
                 value_of_abstract_token_id arg.amount ),
             Abstract_value.LeafP
               ( Factori_utils.sanitized_of_str "token_id",
                 value_of_abstract_token_id arg.token_id ) ) ))

let value_of_abstract_burn (x : abstract_burn) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_burn c

let lift_burn (arg : Interface.burn) : abstract_burn =
  Concrete
    {
      address = lift__operator arg.address;
      amount = lift_token_id arg.amount;
      token_id = lift_token_id arg.token_id;
    }

let mk_call_burn ?(assert_success = false) ?(msg = "") ~scenario ~from ~amount
    ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_burn param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"burn"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_burn ~scenario ~network ~amount ~from ~kt1 ~expected
    ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_burn param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"burn"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type type9 = {
  owner : abstract__operator;
  token_id : abstract_token_id;
}

type abstract_type9 = type9 Factori_abstract_types.Abstract.id_or_concrete

let value_of_type9 (arg : type9) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "owner",
             value_of_abstract__operator arg.owner ),
         Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "token_id",
             value_of_abstract_token_id arg.token_id ) ))

let value_of_abstract_type9 (x : abstract_type9) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_type9 c

let lift_type9 (arg : Interface.type9) : abstract_type9 =
  Concrete
    {owner = lift__operator arg.owner; token_id = lift_token_id arg.token_id}

type balance_of = {
  requests : abstract_type9 list';
  callback : contract;
}

type abstract_balance_of =
  balance_of Factori_abstract_types.Abstract.id_or_concrete

let value_of_balance_of (arg : balance_of) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "requests",
             (value_of_list' value_of_abstract_type9) arg.requests ),
         Abstract_value.LeafP
           ( Factori_utils.sanitized_of_str "callback",
             value_of_contract
               (*EzEncoding.destruct Tzfunc.Proto.micheline_enc.json {|{"prim":"list","args":[{"prim":"pair","args":[{"prim":"pair","args":[{"prim":"address","annots":["%owner"]},{"prim":"nat","annots":["%token_id"]}],"annots":["%request"]},{"prim":"nat","annots":["%balance"]}]}]}|}*)
               arg.callback ) ))

let value_of_abstract_balance_of (x : abstract_balance_of) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of_balance_of c

let lift_balance_of (arg : Interface.balance_of) : abstract_balance_of =
  Concrete
    {
      requests = (lift_list' lift_type9) arg.requests;
      callback =
        lift_contract
          (*EzEncoding.destruct Tzfunc.Proto.micheline_enc.json {|{"prim":"list","args":[{"prim":"pair","args":[{"prim":"pair","args":[{"prim":"address","annots":["%owner"]},{"prim":"nat","annots":["%token_id"]}],"annots":["%request"]},{"prim":"nat","annots":["%balance"]}]}]}|}*)
          arg.callback;
    }

let mk_call_balance_of ?(assert_success = false) ?(msg = "") ~scenario ~from
    ~amount ~kt1 ~network ~param () =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_balance_of param) in
  mk_call
    ~assert_success
    ~msg
    ~scenario
    ~from
    ~amount
    ~entrypoint:"balance_of"
    ~kt1
    ~network
    ~contract_name:"fa2_y"
    ~param:param_value

let assert_failwith_str_balance_of ~scenario ~network ~amount ~from ~kt1
    ~expected ~msg param =
  let open Scenario_dsl.AstInterface in
  let param_value = make_value scenario (value_of_abstract_balance_of param) in
  mk_failed_call
    ~scenario
    ~entrypoint:"balance_of"
    ~expected
    ~msg
    ~network
    ~amount
    ~from
    ~kt1
    ~contract_name:"fa2_y"
    param_value

type _storage = {
  paused : abstract_set_pause;
  operators : (abstract_add_operator, abstract_payout_balance) big_map;
  metadata : (string', bytes) big_map;
  ledger :
    ((abstract__operator, abstract_token_id) tuple2, abstract_token_id) big_map;
  administrator : abstract__operator;
  all_tokens : abstract_token_id;
  payout : abstract__operator;
  token_metadata : (abstract_token_id, abstract_type3) big_map;
}

type abstract__storage = _storage Factori_abstract_types.Abstract.id_or_concrete

let value_of__storage (arg : _storage) =
  Abstract_value.VRecord
    (Abstract_value.PairP
       ( Abstract_value.PairP
           ( Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "administrator",
                     value_of_abstract__operator arg.administrator ),
                 Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "all_tokens",
                     value_of_abstract_token_id arg.all_tokens ) ),
             Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "ledger",
                     (value_of_big_map
                        (value_of_tuple2
                           value_of_abstract__operator
                           value_of_abstract_token_id)
                        value_of_abstract_token_id)
                       arg.ledger ),
                 Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "metadata",
                     (value_of_big_map value_of_string' value_of_bytes)
                       arg.metadata ) ) ),
         Abstract_value.PairP
           ( Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "operators",
                     (value_of_big_map
                        value_of_abstract_add_operator
                        value_of_abstract_payout_balance)
                       arg.operators ),
                 Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "paused",
                     value_of_abstract_set_pause arg.paused ) ),
             Abstract_value.PairP
               ( Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "payout",
                     value_of_abstract__operator arg.payout ),
                 Abstract_value.LeafP
                   ( Factori_utils.sanitized_of_str "token_metadata",
                     (value_of_big_map
                        value_of_abstract_token_id
                        value_of_abstract_type3)
                       arg.token_metadata ) ) ) ))

let value_of_abstract__storage (x : abstract__storage) =
  match x with
  | Id id -> Abstract_value.VAbstract (ScenId id)
  | Concrete c -> value_of__storage c

let lift__storage (arg : Interface._storage) : abstract__storage =
  Concrete
    {
      administrator = lift__operator arg.administrator;
      all_tokens = lift_token_id arg.all_tokens;
      ledger =
        (lift_big_map (lift_tuple2 lift__operator lift_token_id) lift_token_id)
          arg.ledger;
      metadata = (lift_big_map lift_string' lift_bytes) arg.metadata;
      operators =
        (lift_big_map lift_add_operator lift_payout_balance) arg.operators;
      paused = lift_set_pause arg.paused;
      payout = lift__operator arg.payout;
      token_metadata =
        (lift_big_map lift_token_id lift_type3) arg.token_metadata;
    }

(** Deploy*)
let mk_deploy ~scenario ~from ~amount ~network ~storage =
  let open Scenario_dsl.AstInterface in
  let storage_value = make_value scenario (value_of_abstract__storage storage) in

  mk_deploy
    ~scenario
    ~from
    ~amount
    ~network
    ~storage:storage_value
    ~contract_name:"fa2_y"

let abstract_initial_blockchain_storage : abstract__storage =
  lift__storage Interface.initial_blockchain_storage

let initial_blockchain_storage : _storage =
  match abstract_initial_blockchain_storage with
  | Concrete x -> x
  | _ ->
    failwith
      "[initial_blockchain_storage] initial blockchain storage should not be \
       an Id"
