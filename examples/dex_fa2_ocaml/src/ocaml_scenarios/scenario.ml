(* This file is only generated once, but as long as it exists it
   will not be overwritten, you can safely write in it. To find
   examples for your scenarios, you can look at scenarios.example.ml *)
module FA2 = Fa2_y_ocaml_interface
module Q = Quipu_ocaml_interface
module B = Blockchain

let main () =
  let open Tzfunc.Rp in
  let from = B.alice_flextesa in
  let node = B.flextesa_node in
  let (storage : FA2._storage) =
    {
      paused = true;
      operators = Literal [];
      metadata = Literal [];
      ledger = Literal [];
      administrator = from.pkh;
      all_tokens = Z.of_string "0";
      payout = B.bob_flextesa.pkh;
      token_metadata = Literal [];
    } in
  let>? kt1, _op_hash = FA2.deploy ~amount:0L ~node ~name:"fa2" ~from storage in
  Format.printf "KT1 : %s@." kt1 ;
  let open FA2 in
  let param_transfer =
    [
      {
        from_ = from.pkh;
        txs =
          [
            {
              amount = Z.of_string "1";
              token_id = Z.of_string "0";
              to_ = B.bob_flextesa.pkh;
            };
          ];
      };
    ] in
  let>? () =
    FA2.assert_failwith_str_transfer
      ~node
      ~from
      ~expected:"WrongCondition: ~ self.data.paused"
      ~kt1
      ~prefix:"Test_transfer"
      ~msg:"Hello"
      param_transfer in
  let>? op_hash = FA2.call_set_pause ~node ~from ~kt1 false in
  Format.printf "Set_pause operation hash : %s@." op_hash ;
  let param_mint =
    {
      token_info = [];
      token_id = Z.of_string "0";
      address = from.pkh;
      amount = Z.of_string "45";
    } in
  let>? op_hash = FA2.call_mint ~node ~from ~kt1 param_mint in
  Format.printf "Mint operation hash : %s@." op_hash ;
  let>? op_hash = FA2.call_transfer ~node ~from ~kt1 param_transfer in
  Format.printf "Transfer operation hash : %s@." op_hash ;
  let param_mint_2 =
    {
      token_info = [];
      token_id = Z.of_string "1";
      address = from.pkh;
      amount = Z.of_string "100";
    } in
  let>? op_hash = FA2.call_mint ~node ~from ~kt1 param_mint_2 in
  Format.printf "Mint operation hash : %s@." op_hash ;
  (* Now we have 2 token for this fa2 we will deploy our new dex_pool *)
  let storage_quipu =
    Q.
      {
        dev_fee_bps = Z.of_string "3000";
        owner = from.pkh;
        pause_state = [];
        pending_owner = None;
        pool_count = Z.of_string "0";
        pool_ids = Literal [];
        pools = Literal [];
      } in
  let>? quipu_kt1, _op_hash =
    Q.deploy
      ~amount:0L
      ~node:B.flextesa_node
      ~name:"my_quipu"
      ~from
      storage_quipu in
  Format.printf "Quipuswap : %s@." quipu_kt1 ;
  let param_deploy =
    Q.
      {
        cur_tick_index = Z.of_string "-1325";
        token_x = Fa2 {token_id = Z.of_string "1"; token_address = kt1};
        token_y = Fa2 {token_id = Z.of_string "0"; token_address = kt1};
        fee_bps = Z.of_string "30";
        tick_spacing = Z.of_string "60";
        extra_slots = Z.of_string "100";
      } in
  let>? op_hash = Q.call_deploy_pool ~node ~from ~kt1:quipu_kt1 param_deploy in
  Format.printf "New pool operation hash : %s@." op_hash ;
  Lwt.return_ok ()

let _ = Tzfunc.Node.set_silent true

let _ = Lwt_main.run (main ())
