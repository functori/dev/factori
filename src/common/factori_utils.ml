(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module contains various utility functions and command line
options used throughout the Factori codebase, including functions for file I/O,
string manipulation, JSON encoding and decoding, and Micheline tree processing.
It also provides examples for some of the functions. Warning: this short summary
was automatically generated and could be incomplete or misleading. If you feel
this is the case, please open an issue. *)

open Factori_errors
open Format
open Tzfunc.Proto
open Tzfunc.Rp

type storage_type =
  | Random
  | Blockchain

let storage_type_of_str = function
  | "random" | "Random" -> Random
  | "blockchain" | "Blockchain" -> Blockchain
  | s -> raise (UnknownStorageType s)

let list_integers ?(from = 1) n =
  let rec aux res = function
    | n when n = from - 1 -> res
    | n -> aux (n :: res) (n - 1) in
  aux [] n

let add_indices (l : 'a list) : (int * 'a) list =
  List.mapi (fun i x -> (i, x)) l

(** shortcut for the separator in pp_print_list  *)
let tag s ppf _ = fprintf ppf s

(* Utilities for sanitizing names while still keeping around the
   original names of variables, entrypoint names, etc... *)

type accessors = string list [@@deriving show, encoding]

(** In Factori, we need to sanitize the names we find on the
   blockchain to make them into acceptable names for types,
   entrypoints, etc... But sometimes we need to remember what the
   original name was. This is the purpose of this type, which
   preserves the original. *)
type sanitized_name = {
  original : string;
  sanitized : string;
  accessors : accessors;
}
[@@deriving show, encoding]

let get_original sn = sn.original

let get_sanitized sn = sn.sanitized

let get_accessors sn = sn.accessors

let add_accessors sn l = { sn with accessors = sn.accessors @ l }

let add_accessor sn x = add_accessors sn [x]

let make_sanitized ~original ~sanitized ~accessors =
  { original; sanitized; accessors }

let sanitized_of_str ?(accessors = []) sn =
  { original = sn; sanitized = sn; accessors }

(** This function tells whether the sanitized values are equal, it
   does not concern itself with the original values (though in all
   logic they *should* be the same *)
let eq_sanitized sn1 sn2 =
  get_sanitized sn1 = get_sanitized sn2 && get_accessors sn1 = get_accessors sn2

let sanitize_aux s =
  let open Str in
  let temp = global_replace (regexp_string "-") "_" s in
  let temp = global_replace (regexp_string " ") "" temp in
  String.lowercase_ascii temp

let sanitize_basename contract_name =
  {
    original = contract_name;
    sanitized = sanitize_aux contract_name;
    accessors = [];
  }

let print_sanitized_name ppf sn =
  let open Format in
  fprintf ppf "%s%s" (get_sanitized sn)
    (match sn.accessors with
    | [] -> ""
    | l ->
      asprintf "%a"
        (pp_print_list ~pp_sep:(tag "_") (fun ppf x -> fprintf ppf "%s" x))
        l)

let show_sanitized_name sn = Format.asprintf "%a" print_sanitized_name sn

let show_sanitized_difference
    ~(message : Format.formatter -> string * string -> unit)
    ~(sanitized : sanitized_name) =
  let original = get_original sanitized in
  let san = get_sanitized sanitized in
  if original <> san then Format.eprintf "Info: %a@." message (original, san)

(* *)

(** remove an element from a list, as many times at it is present *)
let remove ~eq x l = List.filter (fun y -> not (eq y x)) l

let is_dir dir = try Sys.is_directory dir with _ -> false

let try_overflow ~msg f =
  try f ()
  with Stack_overflow ->
    Format.eprintf "Stack overflow in %s\n%!" msg ;
    raise Stack_overflow

let try_generic ~silent ~msg f =
  try f ()
  with e ->
    if silent then
      ()
    else
      Format.eprintf "Error in %s\n%!" msg ;
    raise e

let output_if_verbose ?(level = 1) ~verbose s =
  if verbose >= level then
    Format.eprintf "[verbosity %d>= (level) %d] %s\n" verbose level s

let get_fresh_id =
  let counter = ref 0 in
  fun () ->
    let res = !counter in
    counter := !counter + 1 ;
    res

(** Function for printing messages to the terminal. `id` is an
   optional identifier you can give the function if you want to make
   sure you're looking at a particular instance of your logging
   message, for example if there are many nested calls to the same
   function:

   [id 1] Entering f with 3
   [id 2] Entering f with 2
   [id 3] Entering f with 1
   [id 4] Entering f with 0
   [id 4] Leaving f with 1
   [id 4] Leaving f with 2
   [id 4] Leaving f with 6
   [id 4] Leaving f with 24

   `level` is the level of verbosity which will trigger this message.
    In the default mode, only messages with level 0 will be printed.
    In `-v` verbose mode, messages with level 1 and 0 will be printed.
    In `-vv` verbose mode, messages with levels 2, 1 and 0 will be printed.
    Etc...

 *)
let output_verbose ?(id = -1) ?(level = 1) s =
  let s =
    if id = -1 then
      s
    else
      sprintf "[id:%d]%s" id s in
  let verbose = !Factori_options.verbosity in
  output_if_verbose ~level ~verbose s

let write_file ~filename str =
  let oc = open_out filename in
  output_string oc str ;
  close_out oc

let write_file ~filename str =
  let dir = Filename.dirname filename in
  output_verbose @@ Format.sprintf "Writing to directory: %s@.\n" dir ;
  if is_dir dir then
    write_file ~filename str
  else
    raise (NotADirectory (dir, Format.sprintf "write_file(%s)" filename))

let remove_file filename =
  if Sys.file_exists filename then
    Sys.remove filename
  else
    ()

let contains s1 s2 =
  let re = Str.regexp_string s2 in
  try
    ignore (Str.search_forward re s1 0) ;
    true
  with Not_found -> false

let rec make_dir dir =
  if Sys.file_exists dir && Sys.is_directory dir then
    ()
  else if Sys.file_exists dir && not (Sys.is_directory dir) then
    raise (NotADirectory (dir, "make_dir"))
  else begin
    let dirname = Filename.dirname dir in
    make_dir dirname ;
    let basename = Filename.basename dir in
    match basename with
    | "." | ".." -> ()
    | _ -> (
      try Unix.mkdir dir 0o755 with Unix.Unix_error (EEXIST, _, _) -> ())
  end

let command_if_exists ?(error_msg = "command does not exist") ~cmd_name
    cmd_content =
  Format.sprintf
    {|
if ! command -v %s \&> /dev/null
then
    echo "%s"
    exit
else
    %s
fi
|}
    cmd_name error_msg cmd_content

let rec read ic buffer bytes =
  let nbytes = input ic bytes 0 1024 in
  if nbytes > 0 then begin
    Buffer.add_subbytes buffer bytes 0 nbytes ;
    read ic buffer bytes
  end

let read_file filename =
  let ic = open_in filename in
  let bytes = Bytes.create 1024 in
  let buf = Buffer.create 1024 in
  read ic buf bytes ;
  close_in ic ;
  Buffer.contents buf

let rec copy ic oc b =
  let read = input ic b 0 1024 in
  if read = 0 then
    ()
  else begin
    output oc b 0 read ;
    copy ic oc b
  end

let copy_file src dst =
  let ic = open_in_bin src in
  let oc = open_out_bin dst in
  try
    let b = Bytes.create 1024 in
    copy ic oc b
  with exn ->
    close_in ic ;
    close_out oc ;
    raise exn

let append_to_file ?(check_already_present = true) ~filepath ~content () =
  if not (Sys.file_exists filepath) then
    write_file ~filename:filepath content
  else
    let cur_file_content = read_file filepath in
    if check_already_present && contains cur_file_content content then
      ()
    else
      let result = Format.sprintf "%s%s" cur_file_content content in
      write_file ~filename:filepath result

let remove_from_file ~filepath ~content () =
  if not (Sys.file_exists filepath) then
    Format.eprintf
      "(Soft) Warning: file %s does not exist, it cannot be removed" filepath
  else
    let open Str in
    let new_file_content =
      global_replace (regexp_string content) "" (read_file filepath) in
    write_file ~filename:filepath new_file_content

(* Serialization/Deserialization *)
let serialize_value ?(pretty = true) ~enc v =
  Ezjsonm.value_to_string ~minify:(not pretty) @@ Json_encoding.construct enc v

let serialize_file ~filename ~enc v =
  write_file ~filename @@ serialize_value ~enc v

let deserialize_value ~enc s =
  try
    let js_value = Ezjsonm.value_from_string s in
    try Json_encoding.destruct enc js_value
    with _e ->
      raise (CouldNotDestruct (sprintf "%s" (Ezjsonm.value_to_string js_value)))
  with _e -> raise (CouldNotDestruct (sprintf "%s" s))

let deserialize_file ?(silent = true) ~filename ~enc =
  try_generic ~silent
    (fun () -> deserialize_value ~enc @@ read_file filename)
    ~msg:(Format.sprintf "deserialize filename:%s" filename)

type delimiter =
  | Brackets
  | Parens
  | Curly
  | Nothing

let opening = function
  | Brackets -> "["
  | Parens -> "("
  | Curly -> "{"
  | Nothing -> ""

let closing = function
  | Brackets -> "]"
  | Parens -> ")"
  | Curly -> "}"
  | Nothing -> ""

let surround d f ppf x = Format.fprintf ppf "%s%a%s" (opening d) f x (closing d)

let comment_ocaml x = Format.sprintf "(*%s*)" x

(* Some common blockchain stuff imported from import_kt1.ml  *)

let unexpected_michelson msg m =
  Format.eprintf "%s\n%!" msg ;
  raise @@ UnexpectedMichelson (EzEncoding.construct micheline_enc.json m)

let get_code_elt p = function
  | Some (Micheline (Mseq l)) ->
    List.find_map
      (function
        | Mprim { prim; args = [arg]; _ } when prim = p -> Some arg
        | _ -> None)
      l
  | _ -> None

let get_all_code_elt p = function
  | Some (Micheline (Mseq l)) ->
    List.filter_map
      (function
        | Mprim { prim; args; _ } when prim = p -> Some args
        | _ -> None)
      l
  | _ -> []

let get_storage_from_script_expr (se : script_expr) : micheline option =
  get_code_elt `storage (Some se)

let get_parameter_from_script_expr (se : script_expr) : micheline option =
  get_code_elt `parameter (Some se)

let get_code_from_script_expr (se : script_expr) : micheline option =
  get_code_elt `code (Some se)

let get_views_from_script_expr (se : script_expr) : micheline list list =
  get_all_code_elt `view (Some se)

let get_name_annots = function
  | [s] when String.length s > 0 && String.get s 0 = '%' ->
    Some (String.sub s 1 (String.length s - 1))
  | _ -> None

let get_entrypoints ?(sub_entrypoints = false) script =
  output_verbose ~level:2
    (sprintf "Entering get_entrypoints with %s"
       (EzEncoding.construct script_expr_enc.json script)) ;
  let get_acc_from_annot annots m acc =
    match get_name_annots annots with
    | None -> acc
    | Some name -> (sanitized_of_str name, m) :: acc in
  let rec aux acc = function
    | Mprim { prim = `or_; args = l; annots } as m -> (
      let arg1 = List.nth l 0 in
      let arg2 = List.nth l 1 in
      match aux acc arg1 with
      | Error e -> Error e
      | Ok acc ->
        let acc =
          if sub_entrypoints then
            get_acc_from_annot annots m acc
          else
            acc in
        aux acc arg2)
    | Mprim { annots; _ } as m -> Ok (get_acc_from_annot annots m acc)
    | m -> unexpected_michelson "toto\n\n\n" m in
  match get_parameter_from_script_expr script with
  | Some (Mprim { prim = `or_; _ } as m) -> aux [] m
  | Some m -> Ok [(sanitized_of_str "default", m)]
  | None ->
    Format.eprintf "couldn't get code\n%!" ;
    Result.error (Error `unexpected_michelson_value)

(* Get storage from the blockchain from a KT1. The network is chosen
   from the `kt1` ref value in options.ml, which is affected by the
   --network option *)

let get_default_identity = function
  | "ithacanet" | "ghostnet" | "flextesa" -> "alice_flextesa"
  | "sandbox" -> "bootstrap1"
  | n -> sprintf "No default identity for %s" n

let get_backup_file_name ~filename =
  Format.sprintf "%s.json" (Filename.remove_extension filename)

let camel_to_snake s =
  let n = String.length s in
  let b = Bytes.create (2 * n) in
  let rec aux i j =
    if i = n then
      j
    else
      let c = String.get s i in
      let code = Char.code c in
      if code >= 65 && code <= 90 then (
        Bytes.set b j '_' ;
        Bytes.set b (j + 1) (Char.chr (code + 32)) ;
        aux (i + 1) (j + 2)
      ) else (
        Bytes.set b j c ;
        aux (i + 1) (j + 1)
      ) in
  let m = aux 0 0 in
  Bytes.(to_string @@ sub b 0 m)

let snake_to_camel s =
  let n = String.length s in
  let b = Bytes.create n in
  let rec aux i j =
    if i = n then
      j
    else
      let c = String.get s i in
      if c = '_' && i <> n - 1 then (
        let c = String.get s (i + 1) in
        let cc = Char.code c in
        if cc >= 97 && cc <= 122 then
          Bytes.set b j Char.(chr @@ (code c - 32))
        else
          Bytes.set b j Char.(chr @@ code c) ;
        aux (i + 2) (j + 1)
      ) else (
        Bytes.set b j c ;
        aux (i + 1) (j + 1)
      ) in
  let m = aux 0 0 in
  Bytes.(to_string @@ sub b 0 m)

(* Two utility functions *)
let map3 ?(reference = "") f l1 l2 l3 =
  let l = List.length in
  let n1, n2, n3 = (l l1, l l2, l l3) in
  if not (n1 = n2 && n2 = n3) then
    failwith
      (Format.sprintf
         "[map3][ref %s] Error: lists have different lengths %d,%d and %d@."
         reference n1 n2 n3)
  else
    List.map
      (fun ((x, y), z) -> f x y z)
      (List.map2 (fun x y -> (x, y)) (List.map2 (fun x y -> (x, y)) l1 l2) l3)

let split4 l1234 =
  let l12, l34 = List.split l1234 in
  let l1, l2 = List.split l12 in
  let l3, l4 = List.split l34 in
  (l1, l2, l3, l4)

let print_z_int_ocaml ppf z = fprintf ppf "Z.of_string (\"%s\")" (Z.to_string z)

let print_z_int_typescript ppf z = fprintf ppf "BigInt (\"%s\")" (Z.to_string z)

let print_z_int_python ppf z = fprintf ppf "%s" (Z.to_string z)

let print_z_int_csharp ppf z =
  fprintf ppf "BigInteger.Parse(\"%s\")" (Z.to_string z)

let print_z_int_shell ppf z = fprintf ppf "%s" (Z.to_string z)

module Z = struct
  include Z

  let enc = Json_encoding.conv to_string of_string Json_encoding.string
end

module Hex = struct
  type hex = Proto.hex

  let hex_enc = hex_enc.json
end

module Ticket = struct
  type 'a ticket = Ticket of string
end

module Lambda = struct
  type lambda_params = {
    from : micheline;
    to_ : micheline;
    body : micheline;
  }

  type lambda =
    | Lambda of lambda_params
    | SeqLambda of micheline

  (* type ex_lambda = CLambda : ('a, 'b) lambda -> ex_lambda *)

  (* let lambda_enc (_aenc : 'a encoding) (_benc : 'b encoding) =
   *   let open Json_encoding in
   *   (conv
   *      (function
   *        | Lambda l -> (l.from, l.to_, l.body))
   *      (fun (from, to_, body) -> Lambda { from; to_; body })
   *      (obj3
   *         (req "from" micheline_enc.json)
   *         (req "to_" micheline_enc.json)
   *         (req "body" micheline_enc.json))
   *     : ('a, 'b) lambda encoding) *)

  (* let lambda_enc = fun (_aenc : 'a encoding) (_benc : 'b encoding) ->
   *   let open Json_encoding in
   *   (conv (function | Lambda l -> (l.from,l.to_,l.body)) (fun (from,to_,body) -> Lambda {from;to_;body}) (obj3 (req "from" micheline_enc.json) (req "to_" micheline_enc.json) (req "body" micheline_enc.json)) : ('a,'b) lambda encoding ) *)
end

(* For CSharp Tuples *)
let derive_correct_item i : string list =
  let soi i = Format.sprintf "Item%d" i in
  let rec aux res k =
    if k < 0 then
      List.rev res
    else if k <= 7 then
      List.rev (soi k :: res)
    else
      (* k>=8 *)
      aux ("Rest" :: res) (k - 7) in
  aux [] i

let print_item ppf i =
  let open Format in
  let l = derive_correct_item i in
  fprintf ppf "%a"
    (pp_print_list ~pp_sep:(tag ".") (fun ppf x -> fprintf ppf "%s" x))
    l

(* Special version of get_code_from_KT1 without using dir. Should
   probably be merged, with an option to distinguish both use cases
   (with or without a factori local repo with a contracts.json), with
   the one in src/tezos/import_kt1.ml *)
let get_code_from_KT1 kt1 ~network () : script_expr =
  let open Format in
  let open Tzfunc.Rp in
  match kt1 with
  | None -> raise NoKT1Provided
  | Some kt1 ->
    let rec aux ~failed =
      let sort x = x in
      let node = Factori_network.get_raw_network ~failed ~sort network in
      match
        Lwt_main.run
          (Tzfunc.Node.get_account_info
             ~base:(Factori_network.get_base_of_network ~sort ~failed network)
             kt1)
      with
      | Error (`http_error (_, _) as e) ->
        output_verbose ~level:0
          (sprintf
             "[get_code_from_KT1][analyze_kt1_cmd] Failed with node %s: %s" node
             (string_of_error e)) ;
        aux ~failed:(node :: failed)
      | Error e -> raise (TzfuncError e)
      | Ok { ac_script = None; _ } -> raise (NoScript kt1)
      | Ok { ac_script = Some { code = c; _ }; _ } -> Micheline c in
    aux ~failed:[]

let enable_red = "\x1B[31m"

let enable_green = "\x1B[32m"

let enable_blue = "\x1B[34m"

let enable_yellow = "\x1B[33m"

let enable_underline = "\x1B[4m"

let enable_bold = "\x1B[1m"

let enable_italic = "\x1B[3m"

let enable_lightbulb = "\xf0\x9f\x92\xa1"

let reset_format = "\x1B[0m"

let fold_map_left f acc l =
  let rec aux cps l =
    match l with
    | [] -> cps
    | x :: xs ->
      aux
        (fun (a, l) ->
          let acc, new_x = f a x in
          cps (acc, new_x :: l))
        xs in
  let cps = aux (fun a -> a) l in
  cps (acc, [])

module List = struct
  include Stdlib.List

  let fold_left_map_result f acc l =
    let$ acc, rev_l =
      List.fold_left
        (fun acc_l elt ->
          let$ acc, l = acc_l in
          let$ acc, elt = f acc elt in
          Ok (acc, elt :: l))
        (Ok (acc, []))
        l in
    Ok (acc, List.rev rev_l)

  let fold_left_i f acc l =
    let rec aux i acc = function
      | [] -> acc
      | elt :: xs -> aux (i + 1) (f i acc elt) xs in
    aux 0 acc l
end

let split_at z l =
  let rec aux_split acc cpt l =
    if Z.compare cpt Z.zero <= 0 then
      Some (List.rev acc, l)
    else
      match l with
      | [] -> None
      | x :: xs -> aux_split (x :: acc) (Z.pred cpt) xs in
  aux_split [] z l

let split_pair_at_general n unpair_fun pair_fun pair =
  let rec aux_split_pair acc n pair =
    Result.bind (unpair_fun pair) (fun ty_l ->
        if Z.geq (Z.of_int (List.length ty_l)) n then
          let l_opt = split_at (Z.pred n) ty_l in
          match l_opt with
          | None -> Error (GenericError ("split_pair_at", "Unreachable branch"))
          | Some (tops, bots) ->
            Result.bind (pair_fun bots) (fun pair ->
                Ok (List.rev_append acc tops @ [pair]))
        else
          match List.rev ty_l with
          | [] ->
            Error (GenericError ("split_pair_at", "Empty argument for pairing"))
          | x :: xs ->
            let new_n = Z.sub n (Z.of_int @@ List.length xs) in
            aux_split_pair (xs @ acc) new_n x) in
  aux_split_pair [] n pair

let string_to_print f ppf s = Format.fprintf ppf "%s" (f s)

let prim_macro =
  List.map (fun (s, p) -> ((p : primitive :> primitive_or_macro), s)) primitives

let prim_name ~prim = List.assoc prim prim_macro

let pp_print_list_2 ?pp_sep f ppf l1 l2 =
  let promise = List.map2 (fun a b ppf () -> f ppf a b) l1 l2 in
  pp_print_list ?pp_sep (fun ppf p -> p ppf ()) ppf promise
