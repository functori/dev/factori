#!/bin/bash

JSON=`mktemp /tmp/factori-snippet.XXXXXX`

FACTORI_SCRIPT="scripts/factori.sh"

FILE_PATH="factori.sh"

REQUEST=PUT

SNIPPET=2509259

API_GIT=https://gitlab.com/api/v4/projects/functori%2Fdev%2Ffactori

cp $FACTORI_SCRIPT $JSON

if [ -z "$SNIPPET_TOKEN" ]
then
    echo "SNIPPET_TOKEN variable is empty"
else
    echo "SNIPPET_TOKEN is set to $SNIPPET_TOKEN"
fi

sed -i 's|\\|\\\\|g' $JSON
sed -i 's|\"|\\"|g' $JSON
sed -i 's|\t|\\t|g' $JSON

CONTENT=$(sed ':a;N;$!ba;s/\n/\\n/g' $JSON) 

PARAM="{ \"files\": [ { \"action\": \"update\", \"file_path\":\"$FILE_PATH\", \"content\" : \"$CONTENT\"} ] }"

cat > $JSON <<EOF
$PARAM
EOF

CURL="curl --request ${REQUEST} ${API_GIT}/snippets/${SNIPPET} --header \"PRIVATE-TOKEN:${SNIPPET_TOKEN}\" --header \"Content-Type:application/json\" -d @$JSON"

eval $CURL

rm $JSON

