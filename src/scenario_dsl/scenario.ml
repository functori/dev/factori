(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module provides functions for creating complex actions in a
context, which is an abstract syntax tree (AST) with extra information. The
module exports functions for adding checkpoints, deployments, calls, failed
calls, and forks to the context, and it depends on the `Scenario_value` module
for defining the types of values used in a scenario. Warning: this short summary
was automatically generated and could be incomplete or misleading. If you feel
this is the case, please open an issue. *)

open Context

(* This module facilitates creating such complex actions as
   deployments, calls or forks *)
module Scenario (Ctxt : CONTEXT) = struct
  open Ctxt
  open Scenario_value

  let context = init_context ()

  let make_checkpoint context = set_value context Checkpoint

  let make_deploy ?(name = None) context deploy =
    let id, context = set_value ~name context (Deploy deploy) in
    (* storage has to exist before deployment event *)
    let context = add_dependency context deploy.storage id in
    let context = add_dependency context deploy.from id in
    let context = add_dependency context deploy.amount id in
    let context = add_dependency context deploy.network id in
    (id, context)

  let make_call ?(name = None) context call =
    let id, context = set_value ~name context (Call call) in
    let context = add_dependency context call.from id in
    let context = add_dependency context call.amount id in
    let context = add_dependency context call.contract id in
    let context = add_dependency context call.network id in
    let context = add_dependency context call.param id in
    (id, context)

  let make_failed_call ?(name = None) context (expected : string) (msg : string)
      call =
    let id, context =
      set_value ~name context (FailedCall (Failed (expected, msg, call))) in
    let context = add_dependency context call.from id in
    let context = add_dependency context call.amount id in
    let context = add_dependency context call.contract id in
    let context = add_dependency context call.network id in
    let context = add_dependency context call.param id in
    (id, context)

  (** after is the id after which the fork should happen  *)
  let make_fork ~(after : u_id) (context : context) fork =
    let open Ctxt in
    let id_checkpoint, context = make_checkpoint context in
    let id, context = set_value context (Fork fork) in
    let context = add_dependency context after id_checkpoint in
    let context = add_dependency context id_checkpoint id in
    (id, context)

  (* let make_print (context : context) u_id =
   *   let open Ctxt in
   *   let new_id,context = set_value context (PrintUnary u_id) in
   *   let context = add_dependency context id new_id in
   *   (new_id,context) *)
end
