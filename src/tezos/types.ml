(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file defines types and functions related to Tezos types, including
variants for unary, binary, and basetype, as well as variants for abs_type_ and
type_. Functions are provided to convert these variants to strings, and there
are also functions for converting paired_type to a list, checking whether a type
is Unit, and iterating over fields of a list. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Tzfunc.Proto
open Format
open Pair_type
open Or_type
open Factori_utils

type unary =
  | List
  | Set
  | Ticket
  | Option
[@@deriving show, encoding]

let str_of_unary ?(camelcase = false) ?(csharp = false) =
  if camelcase then
    function
  | List when csharp -> "MList"
  | List -> "List"
  | Set -> "Set"
  | Ticket -> "Ticket"
  | Option -> "Option"
  else
    function
  | List -> "list"
  | Set -> "set"
  | Ticket -> "ticket"
  | Option -> "option"

type binary =
  | Map
  | BigMap
  | Lambda
[@@deriving show, encoding]

let str_of_binary ?(camelcase = false) =
  if camelcase then
    function
  | Map -> "Map"
  | BigMap -> "BigMap"
  | Lambda -> "Lambda"
  else
    function
  | Map -> "map"
  | BigMap -> "big_map"
  | Lambda -> "lambda"

(** A reification of Michelson types for our inference purposes *)
type basetype =
  | Never
  | String
  | Nat
  | Int
  | Byte
  | Address
  | Signature
  | Unit
  | Bool
  | Timestamp
  | Keyhash
  | Key
  | Mutez
  | Operation
  | Chain_id
  | Sapling_state
  | Sapling_transaction_deprecated
[@@deriving show, encoding { enum }]

let str_of_base = function
  | Never -> "never"
  | String -> "string"
  | Nat -> "nat"
  | Int -> "int"
  | Byte -> "bytes"
  | Address -> "address"
  | Signature -> "signature"
  | Unit -> "unit"
  | Bool -> "bool"
  | Timestamp -> "timestamp"
  | Keyhash -> "key_hash"
  | Key -> "key"
  | Mutez -> "mutez"
  | Operation -> "operation"
  | Chain_id -> "chain_id"
  | Sapling_state -> "sapling_state"
  | Sapling_transaction_deprecated -> "sapling_transaction_deprecated"

(** An abstract type to represent types (only those with content and
   which mirror on-chain types) with their arities so as to generate
   generic functions on those types in various languages. *)
type abs_type_ =
  | ATuple of int
  | AContract
  | ABase of basetype
  | AUnary of unary
  | ABinary of binary

let arity = function
  | ATuple k -> k
  | AContract -> 0
  | ABase _ -> 0
  | AUnary _ -> 1
  | ABinary _ -> 2

type kind =
  | Cstring
  | Cint
  | Cbool
  | Cbytes
  | Cunknown

let base_literal_type s =
  match s with
  | String
  | Address
  | Keyhash
  | Key
  | Chain_id
  | Signature
  | Timestamp
  | Operation -> Cstring
  | Nat | Int | Mutez -> Cint
  | Bool -> Cbool
  | Byte -> Cbytes
  | _ -> Cunknown

type typename = string [@@deriving show, encoding]

type micheline_ = micheline

let micheline__enc = micheline_enc.json

let pp_micheline_ ppf m =
  fprintf ppf "%s" (EzEncoding.construct micheline__enc m)

type type_ =
  | Pair of (type_ list[@wrap "pair"])
  | Record of (type_ paired_type[@wrap "record"])
  | TVar of ((sanitized_name * type_)[@wrap "tvar"])
  | Annot of ((type_ * sanitized_name)[@wrap "annot"])
  | Or of (type_ or_type[@wrap "or"])
  | Contract of (micheline_[@wrap "contract"])
  | Base of (basetype[@wrap "base"])
  | Unary of ((unary * type_)[@wrap "unary"])
  | Binary of ((binary * (type_ * type_))[@wrap "binary"])
[@@deriving show, encoding { recursive }]

let rec is_unit t_ =
  match t_ with
  | Base Unit -> true
  | TVar (_, t_) -> is_unit t_
  | _ -> false

let record_to_list ep =
  (* path is how we get to the element in the tuple *)
  let rec aux ~path = function
    | LeafP (field_name, t) -> [(field_name, (path, t))]
    | PairP l -> List.concat (List.mapi (fun i x -> aux ~path:(path @ [i]) x) l)
  in
  aux ~path:[] (* ~name:name *) ep

let iterate_over_fields ppf (lst : (sanitized_name * _) list)
    (f : int -> sanitized_name -> 'a -> string) (d : delimiter) (sep : string) =
  let rec aux res count = function
    | [] -> List.rev res
    | (k, v) :: xs ->
      let new_res =
        let cur_elem = f count k v in
        cur_elem :: res in
      aux new_res (count + 1) xs in
  fprintf ppf "%s%s%s\n" (opening d)
    (String.concat sep (aux [] 0 lst))
    (closing d)

(** Print base types for abstract scenarios  *)
let pp_base_type_abstract ~prefix ppf b =
  let prefix =
    match prefix with
    | Some prefix -> sprintf "%s." prefix
    | None -> "" in
  match b with
  | Never -> fprintf ppf "%snever_type" prefix
  | Chain_id -> fprintf ppf "%schain_id" prefix
  | String -> fprintf ppf "%s%s" prefix "string'"
  | Nat -> fprintf ppf "%snat" prefix
  | Int -> fprintf ppf "%sint_michelson" prefix
  | Byte -> fprintf ppf "%sbytes" prefix
  | Address -> fprintf ppf "%saddress" prefix
  | Signature -> fprintf ppf "%ssignature" prefix
  | Unit -> fprintf ppf "%s%s" prefix "unit'"
  | Bool -> fprintf ppf "%s%s" prefix "bool'"
  | Keyhash -> fprintf ppf "key_hash"
  | b -> begin
    let typename =
      match b with
      | Timestamp -> "timestamp"
      | Mutez -> "tez"
      | Key -> "key"
      | Operation -> "operation"
      | Sapling_state -> "sapling_state"
      | Sapling_transaction_deprecated -> "sapling_transaction_deprecated"
      | b ->
        failwith
          (sprintf
             "[pp_base_type] %s is supposed to already have been taken care of \
              above"
             (str_of_base b)) in
    fprintf ppf "%s%s" prefix typename
  end

let pp_literal_base_type ppf b =
  let s = base_literal_type b in
  match s with
  | Cstring -> Format.fprintf ppf "string"
  | Cint -> Format.fprintf ppf "BigInteger"
  | Cbool -> Format.fprintf ppf "bool"
  | Cbytes -> Format.fprintf ppf "byte[]"
  | Cunknown -> ()

(** General function for printing the name of a base type in the
   generated SDKs. The code is factored out when possible, and
   separated by language when there are specific requirements. *)
let pp_base_type ?(language = Factori_config.OCaml) ?(abstract = false) ?prefix
    ppf b =
  let open Factori_config in
  if abstract then
    pp_base_type_abstract ~prefix ppf b
  else begin
    let prefix =
      match prefix with
      | Some prefix -> sprintf "%s." prefix
      | None -> "" in
    match b with
    | Never -> (
      match language with
      | Csharp -> fprintf ppf "%sNever" prefix
      | _ -> fprintf ppf "%snever_type" prefix)
    | Chain_id -> (
      match language with
      | Csharp -> fprintf ppf "%sChainId" prefix
      | _ -> fprintf ppf "%schain_id" prefix)
    | String -> (
      match language with
      | Csharp -> fprintf ppf "%sString" prefix
      | _ -> fprintf ppf "%sstring" prefix)
    | Nat -> (
      match language with
      | Csharp -> fprintf ppf "%sNat" prefix
      | _ -> fprintf ppf "%snat" prefix)
    | Int -> begin
      match language with
      | Typescript -> fprintf ppf "%sint" prefix
      | OCaml | Python -> fprintf ppf "%sint_michelson" prefix
      | Csharp -> fprintf ppf "%sInt" prefix
    end
    | Byte -> (
      match language with
      | Csharp -> fprintf ppf "%sBytes" prefix
      | Python -> fprintf ppf "bytes"
      | _ -> fprintf ppf "%sbytes" prefix)
    | Address -> (
      match language with
      | Csharp -> fprintf ppf "%sAddress" prefix
      | _ -> fprintf ppf "%saddress" prefix)
    | Signature -> (
      match language with
      | Csharp -> fprintf ppf "%sSignature" prefix
      | _ -> fprintf ppf "%ssignature" prefix)
    | Unit -> (
      match language with
      | Csharp -> fprintf ppf "%sUnit" prefix
      | _ -> fprintf ppf "%s%s" prefix "unit")
    | Bool -> (
      match language with
      | Python -> fprintf ppf "bool"
      | Csharp -> fprintf ppf "%sBool" prefix
      | _ -> fprintf ppf "%s%s" prefix "bool")
    | Keyhash -> (
      match language with
      | Csharp -> fprintf ppf "%sKeyHash" prefix
      | Typescript -> fprintf ppf "%skey_hash" prefix
      | Python -> fprintf ppf "%skey_hash" prefix
      | _ -> fprintf ppf "key_hash")
    | b -> begin
      let typename =
        match b with
        | Timestamp -> "timestamp"
        | Mutez -> "tez"
        | Key -> "key"
        | Operation -> "operation"
        | Sapling_state -> "sapling_state"
        | Sapling_transaction_deprecated -> "sapling_transaction_deprecated"
        | b ->
          failwith
            (sprintf
               "[pp_base_type] %s is supposed to already have been taken care \
                of above"
               (str_of_base b)) in
      fprintf ppf "%s%s" prefix
        (match language with
        | Csharp -> String.capitalize_ascii typename
        | _ -> typename)
    end
  end
