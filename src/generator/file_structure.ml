(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `file_structure.ml` file contains utility functions for lifting concrete
values to their abstract counterparts in Factori. It provides lifting functions
for elementary and non-elementary types, as well as functions for lifting
integers, floats, booleans, strings, tuples, and lists. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Factori_errors
open Factori_config
open Factori_utils
open Ocaml_dune

let ( // ) = Factori_options.concat

let create_local_config_file ~verbose ~dir =
  let local_config_file = Factori_config.get_local_config_name ~dir in
  if Sys.file_exists local_config_file then
    output_if_verbose ~verbose "contracts.json already exists\n"
  else (
    output_if_verbose ~verbose
      (Format.sprintf "Writing contracts.json to %s\n%!" local_config_file) ;
    serialize_file ~filename:local_config_file
      ~enc:Factori_config.local_config_enc
      { bad_nodes = []; contracts = [] }
  )

let create_version_file ~dir =
  let version_file = Factori_config.get_version_file_name ~dir in
  if Sys.file_exists version_file then begin
    let project =
      deserialize_file ~silent:false ~filename:version_file
        ~enc:Factori_config.version_config_enc in
    if Version.version <> project.version then begin
      Format.printf
        "The version of the project [%s] does not match with the version of \
         Factori [%s]@."
        project.version Version.version ;
      exit 1
    end
  end else
    serialize_file ~filename:version_file ~enc:Factori_config.version_config_enc
      { version = Version.version }

let check_typescript_infrastructure_present ~dir () =
  let tsconfig = Typescript_SDK.tsconfig_path ~dir () in
  let functolib = Typescript_SDK.functolib_path ~dir () in
  let package = Typescript_SDK.package_dot_json_path () ~dir in
  Sys.file_exists tsconfig && Sys.file_exists functolib
  && Sys.file_exists package

let check_ocaml_infrastructure_present ~dir () =
  let factori_types = OCaml_SDK.factori_types_path ~dir () in
  let blockchain_ml = OCaml_SDK.blockchain_path ~dir () in
  Sys.file_exists factori_types && Sys.file_exists blockchain_ml

let check_python_infrastructure_present ~dir () =
  let factori_types = Python_SDK.factori_types_path ~dir in
  let blockchain_py = Python_SDK.blockchain_path ~dir in
  Sys.file_exists factori_types && Sys.file_exists blockchain_py

let check_csharp_infrastructure_present ~dir () =
  let factori_types = Csharp_SDK.factori_types_path ~dir in
  let blockchain_cs = Csharp_SDK.blockchain_path ~dir in
  Sys.file_exists factori_types && Sys.file_exists blockchain_cs

(** Check initialized status of current directory *)
let is_initialized_dir () =
  let dir = get_dir () in
  if not (is_dir dir) then (
    make_dir dir ;
    false
  ) else if
      Sys.file_exists (get_local_config_name ~dir)
      && (if !Factori_options.typescript then
            check_typescript_infrastructure_present ~dir ()
          else
            true)
      && (if !Factori_options.ocaml then
            check_ocaml_infrastructure_present ~dir ()
          else
            true)
      && (if !Factori_options.python then
            check_python_infrastructure_present ~dir ()
          else
            true)
      &&
      if !Factori_options.csharp then
        check_csharp_infrastructure_present ~dir ()
      else
        true
    then
    true
  else if Sys.readdir dir <> [||] && not !Factori_options.overwrite then
    raise (DirectoryNotEmpty dir)
  else
    false

(** Create file structure for the new Factori project *)
let create ?(verbose = 0) ?(library = false) ?(crawlori = false)
    ?(typescript = false) ?(ocaml = false) ?(python = false) ?(csharp = false)
    ?(web_mode = false) ?(dipdup = false) ~overwrite ~name ~db_name () =
  let dir = get_dir () in
  make_dir dir ;
  let exists_crawlori =
    crawlori || Factori_config.exists_option ~dir (Crawler Crawlori) in
  let exists_python =
    python || Factori_config.exists_option ~dir (Language Python) in
  let exists_web_mode =
    web_mode || Factori_config.exists_option ~dir Webinterface in
  let exists_csharp =
    csharp || Factori_config.exists_option ~dir (Language Csharp) in
  let exists_ocaml =
    ocaml || Factori_config.exists_option ~dir (Language OCaml) in
  let exists_typescript =
    typescript || Factori_config.exists_option ~dir (Language Typescript) in
  if not (is_dir dir) then
    raise (NotADirectory (dir, "create"))
  else if Sys.readdir dir <> [||] && not overwrite then
    raise (DirectoryNotEmpty dir)
  else
    let _ = create_version_file ~dir in
    let _ = create_local_config_file ~verbose ~dir in
    let name = Option.value ~default:"my_factori_project" name in
    let src_dir = dir // "src" in
    let contracts_dir = src_dir // "contracts" in
    let ocaml_scenario_dir = OCaml_SDK.scenarios_dir ~dir () in
    let naming_dir = contracts_dir // "type_naming" in
    make_dir src_dir ;
    make_dir naming_dir ;

    if !Factori_options.python then begin
      Python_SDK.generate_sdk ~dir () ;
      write_file
        ~filename:(Python_SDK.factori_types_path ~dir)
        Python_types.python_types ;
      write_file
        ~filename:(Python_SDK.blockchain_path ~dir)
        Python_blockchain.blockchain_py
      (* TODO: dependencies when there will be some *)
    end ;

    if !Factori_options.csharp then begin
      Csharp_SDK.generate_sdk ~dir () ;
      write_file
        ~filename:(Csharp_SDK.factori_types_path ~dir)
        Csharp_types.csharp_types ;
      (* write_file
       *   (Csharp_SDK.project_file_path ~dir)
       *   Artifacts.csharp_project_file ; *)
      write_file
        ~filename:(Csharp_SDK.blockchain_path ~dir)
        Csharp_blockchain.blockchain_cs
      (* TODO: dependencies when there will be some *)
    end ;

    if !Factori_options.ocaml then begin
      let libraries_dir = OCaml_SDK.libraries_dir ~dir () in
      OCaml_SDK.generate_sdk ~dir () ;
      write_file
        ~filename:(OCaml_SDK.factori_types_path ~dir ())
        Ocaml_types.types_ml ;
      write_file
        ~filename:(OCaml_SDK.factori_abstract_types_path ~dir ())
        Ocaml_abstract_types.abstract_types_ml ;
      write_file
        ~filename:(OCaml_SDK.blockchain_path ~dir ())
        Ocaml_blockchain.blockchain_ml ;
      write_file ~filename:(libraries_dir // "utils.ml") Ocaml_utils.utils_ml ;
      write_dune_file ~with_readable_backup:true
        ~dune_file:Artifacts.libraries_dunefile ~path:(libraries_dir // "dune")
        ()
    end ;

    (* write_file (ocaml_scenario_dir // "libraries" // "dune") Artifacts.libraries_dunefile; *)
    if !Factori_options.ocaml && not library then begin
      make_dir ocaml_scenario_dir ;
      write_dune_file ~with_readable_backup:true
        ~dune_file:(Artifacts.scenarios_dunefile (get_contracts ~dir))
        ~path:(ocaml_scenario_dir // "dune")
        () ;
      let scenario_path = OCaml_SDK.scenarios_path ~dir () in
      if not (Sys.file_exists scenario_path) then
        write_file ~filename:scenario_path Artifacts.scenario_ml
    end ;
    (* write_file (ocaml_scenario_dir // "dune") (Artifacts.scenarios_dunefile []); *)
    if !Factori_options.typescript then begin
      Typescript_SDK.generate_sdk ~dir ()
    end ;
    if !Factori_options.typescript && not library then begin
      write_file
        ~filename:(Typescript_SDK.functolib_path ~dir ())
        (Format.asprintf "%a" Functolib_ts.functolib_ts ()) ;
      write_file
        ~filename:(Typescript_SDK.tsconfig_path ~dir ())
        Artifacts.ts_config ;
      write_file
        ~filename:(Typescript_SDK.package_dot_json_path ~dir ())
        Artifacts.package_json ;
      if !Factori_options.web_mode then begin
        make_dir (Typescript_SDK.get_vue_components_dir ~dir ()) ;
        write_file
          ~filename:(Typescript_SDK.webpack_config_js_path ~dir ())
          Artifacts.webpack_config_json ;
        write_file
          ~filename:(Typescript_SDK.index_html_path ~dir ())
          Artifacts.index_html ;
        write_file
          ~filename:(Typescript_SDK.vue_shim_ts_path ~dir ())
          Artifacts.vue_shim_ts
      end
    end ;
    if not library then begin
      write_file ~filename:(dir // "Makefile")
        (Artifacts.makefile ~db_name ~crawlori:exists_crawlori
           ~python:exists_python ~web_mode:exists_web_mode ~csharp:exists_csharp
           ~ocaml:exists_ocaml ~typescript:exists_typescript) ;
      write_file ~filename:(dir // "README.md")
        (Artifacts.readme ~crawlori ~dipdup) ;
      if !Factori_options.ocaml then begin
        write_file
          ~filename:
            (dir
            // Format.sprintf "%s.opam"
                 (Option.value ~default:"project" !Factori_options.project_name)
            )
          (Artifacts.factori_project_opam_file ~crawlori) ;
        write_file ~filename:(dir // "dune-project") "(lang dune 2.9)" ;
        write_file ~filename:(dir // ".ocamlformat") Artifacts.ocamlformat
      end ;
      write_file
        ~filename:(dir // "Makefile.generated")
        Artifacts.generated_makefile
    end ;
    if !Factori_options.ocaml then
      Format.eprintf "Project '%s' created.\n%!" name ;
    ()

let create_global_config_file ~filedir =
  let open Factori_config in
  Format.eprintf "parent %s%!\n" filedir ;
  make_dir filedir ;
  let filename = filedir // "config.json" in
  let config : factori_config = () in
  serialize_file ~filename ~enc:factori_config_enc config

(* let create_ml_contract ~dir ~overwrite ~contract_name =
   let dir = Option.value ~default:current_dir_name dir in
   let src_dir = dir // "src" in
   let contracts_dir = src_dir // "contracts" in
   let ml_dir = contracts_dir // "ml" in
   make_dir ~p:true ml_dir ;
   (* let contract_name = Option.value ~default:"unnamed_contract" contract_name in *)
   let filename = ml_dir // add_suffix contract_name ".ml" in
   if exists filename && not overwrite then
     (* Refuse to overwrite *)
     raise (NotOverwritingFile filename)
   else (
     write_file filename Artifacts.default_ml_contract ;
     let dune_content =
       Artifacts.dune_chunk_new_ml_contract ~name:contract_name in
     add_stanza_to_dune_file ~path:(ml_dir // "dune") ~file_must_exist:false
       Artifacts.dune_env_remove_warnings ;
     (* append_to_file ~check_already_present:true ~filepath:(ml_dir // "dune") ~content:Artifacts.dune_env_remove_warnings (); *)
     add_to_dune_file ~path:(ml_dir // "dune") dune_content ;
     (* append_to_file ~check_already_present:true ~filepath:(ml_dir // "dune") ~content:dune_content (); *)
     Factori_config.add_contract ~original_kt1:None ~dir ~format:OCaml
       ~name:contract_name () ;
     let ocaml_interface_file =
       Factori_config.OCaml_SDK.interface_filename ~contract_name in
     let ts_interface_file = contract_name ^ "_interface.ts" in
     write_file (OCaml_SDK.interface_dir ~dir // ocaml_interface_file) "" ;
     write_file
       (Factori_config.Typescript_SDK.interface_dir ~dir // ts_interface_file)
       "" ;
     add_to_dune_file
       ~path:(OCaml_SDK.interface_dir ~dir // "dune")
       (Artifacts.dune_chunk_new_ocaml_interface ~name:contract_name)
   ) ;
   (* Now we need to enable ocaml-interface-related stuff for this new contract *)
   let ocaml_scenario_dir = OCaml_SDK.scenarios_dir ~dir in
   add_library_to_stanza_in_dune_file
     ~path:(ocaml_scenario_dir // "dune")
     ~stanza_name:"scenario"
     (Factori_config.OCaml_SDK.interface_basename ~contract_name) *)
(* write_file (ocaml_scenario_dir // "dune") (Artifacts.scenarios_dunefile contracts) *)
