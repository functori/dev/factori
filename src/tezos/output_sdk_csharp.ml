(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions for generating C# code that interacts
with Michelson smart contracts. It includes functions for decoding and encoding
Michelson expressions, defining C# classes for Michelson types, and deploying
smart contracts. The functions are specialized for use in the Tezos output SDK
for C#. Example usage and detailed explanations are provided for each function.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Format
open Infer_entrypoints
open Pair_type
open Value
open Or_type
open Types
open Factori_utils
open Factori_errors
open Tzfunc.Proto
open Factori_config
(* C# *)

let get_csharp_basetype b =
  match Types.base_literal_type b with
  | Cbool -> Some "bool"
  | Cstring -> Some "string"
  | Cint -> Some "BigInteger"
  | Cbytes -> Some "byte[]"
  | Cunknown -> None

let rec output_micheline ~prefix ppf m =
  match m with
  | Mprim { prim; args = []; _ } ->
    Format.fprintf ppf "%s.%s" prefix
      (String.capitalize_ascii (Factori_utils.prim_name ~prim))
  | Mprim { prim = `list; args; _ } ->
    Format.fprintf ppf "%s.MList<%a>" prefix
      (pp_print_list ~pp_sep:(tag ",") (output_micheline ~prefix))
      args
  | Mprim { prim = `pair; args; _ } ->
    Format.fprintf ppf "%s.MyTuple<%a>" prefix
      (pp_print_list ~pp_sep:(tag ",") (output_micheline ~prefix))
      args
  | Mprim { prim = `big_map; args; _ } ->
    Format.fprintf ppf "%s.BigMap<%a>" prefix
      (pp_print_list ~pp_sep:(tag ",") (output_micheline ~prefix))
      args
  | Mprim { prim; args; _ } ->
    Format.fprintf ppf "%s.%s<%a>" prefix
      (String.capitalize_ascii (Factori_utils.prim_name ~prim))
      (pp_print_list ~pp_sep:(tag ",") (output_micheline ~prefix))
      args
  | _ -> raise (GenericError ("Contract", "Expected micheline primitive"))

(* Output functions on Michelson types *)
let rec output_type_pretty ~prefix ppf =
  let output_type_pretty = output_type_pretty in
  function
  | Base b ->
    fprintf ppf "%a"
      (pp_base_type ~language:Factori_config.Csharp ~abstract:false ~prefix)
      b
  | Annot (ep, _name) -> fprintf ppf "%a" (output_type_pretty ~prefix) ep
  | Pair _ as t when Naming.has_name t ->
    fprintf ppf "%s"
      (show_sanitized_name @@ bind_capitalize (Naming.get_name_unsafe t))
  | Pair al ->
    fprintf ppf "%s.MyTuple<%a>" prefix
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "%s" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "%s" (show_sanitized_name @@ bind_capitalize (fst tn))
  | Unary (u, a) ->
    fprintf ppf "%s.%s<%a>" prefix
      (str_of_unary ~camelcase:true ~csharp:true u)
      (output_type_pretty ~prefix)
      a
  | Contract c ->
    fprintf ppf "%s.Contract<%a>" prefix (output_micheline ~prefix)
      c (* (output_type_pretty ~prefix) c *)
  | Binary (bin, (k, v)) ->
    fprintf ppf "%s.%s<%a,%a>" prefix
      (str_of_binary ~camelcase:true bin)
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v

let rec output_type_content ~prefix ppf =
  let output_type_pretty = output_type_pretty in
  let output_type_content = output_type_content in
  function
  | Base _ as t -> (output_type_pretty ~prefix) ppf t
  | Annot (_, _) as t -> (output_type_pretty ~prefix) ppf t
  | Pair al ->
    fprintf ppf "%s.MyTuple<%a>" prefix
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_content ~prefix))
      al
  | Record (LeafP (_n, x)) -> fprintf ppf "%a" (output_type_pretty ~prefix) x
  | Record (PairP l) ->
    let l = flattenP [] l in
    fprintf ppf "%a"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (n, x) ->
           fprintf ppf "%s: %a" (show_sanitized_name n)
             (output_type_pretty ~prefix)
             x))
      l
  (* | Record _ -> fprintf ppf "<TODO: record type content>" *)
  | Or (OrAbs (a1, a2)) ->
    fprintf ppf "| %a | %a"
      (get_constructor ~prefix true)
      (Or a1)
      (get_constructor ~prefix false)
      (Or a2)
  | Or (LeafOr _) -> fprintf ppf "LeafOr TODO"
  | TVar tn -> fprintf ppf "%s" (show_sanitized_name @@ bind_capitalize (fst tn))
  | Unary (List, a) ->
    fprintf ppf "%s.MList<%a>" prefix (output_type_content ~prefix) a
  | Unary (Set, a) ->
    fprintf ppf "%s.Set<%a>" prefix (output_type_content ~prefix) a
  | Contract c ->
    fprintf ppf "%s.Contract<%a>" prefix (output_micheline ~prefix)
      c (* pp_micheline_ c *)
  | Binary (BigMap, (k, v)) ->
    fprintf ppf "%s.BigMap<%a,%a>" prefix
      (output_type_content ~prefix)
      k
      (output_type_content ~prefix)
      v
  | Binary (Lambda, (k, v)) ->
    fprintf ppf "%s.Lambda<%a,%a>" prefix
      (output_type_content ~prefix)
      k
      (output_type_content ~prefix)
      v
  | Binary (Map, (k, v)) ->
    fprintf ppf "%s.Map<%a,%a>" prefix
      (output_type_content ~prefix)
      k
      (output_type_content ~prefix)
      v
  | Unary (Option, a) ->
    fprintf ppf "%s.Option<%a>" prefix (output_type_content ~prefix) a
  | Unary (Ticket, _t) ->
    fprintf ppf "%s.Ticket" prefix (* (output_type_content ~prefix) t *)

(* Some day, on some contract, this will get more complicated *)
and get_constructor ~prefix b ppf type_ =
  Format.fprintf ppf "%s of (%a)"
    (if b then
       "Left"
     else
       "Right")
    (output_type_content ~prefix)
    type_

(* Literals *)
let rec pp_value_to_csharp ?(first_tuple_as_parameters = false) ?storage ~prefix
    types ppf value =
  let pp_value_to_csharp =
    pp_value_to_csharp ~first_tuple_as_parameters:false ~prefix in
  let normal_or_storage types storage =
    match storage with
    | Some name -> show_sanitized_name @@ bind_capitalize name
    | None -> Format.asprintf "%a" (output_type_pretty ~prefix) types in
  match value with
  | VChain_id s -> fprintf ppf "\"%s\"" s
  | VUnit _ -> fprintf ppf "new %s()" (normal_or_storage types storage)
  | VKeyHash kh -> fprintf ppf "\"%s\"" kh
  | VKey k -> fprintf ppf "\"%s\"" k
  | VSignature s -> fprintf ppf "\"%s\"" s
  | VOperation o -> fprintf ppf "\"%s\"" o
  | VOption (t, None) ->
    fprintf ppf "new %s(new %s.None<%a>())"
      (normal_or_storage types storage)
      prefix
      (output_type_pretty ~prefix)
      t
  | VOption (t, Some x) ->
    fprintf ppf "new %s(new %s.Some<%a>(%a))"
      (normal_or_storage types storage)
      prefix
      (output_type_pretty ~prefix)
      t (pp_value_to_csharp t) x
  | VInt z -> print_z_int_csharp ppf z
  | VTez z -> print_z_int_csharp ppf z
  | VString s -> fprintf ppf "\"%s\"" s
  | VBytes s -> fprintf ppf "Utils.StringToByteArray(\"%s\")" (s :> string)
  | VTimestamp s -> fprintf ppf "(\"%s\")" s
  | VBool b ->
    fprintf ppf "%s"
      (if b then
         "true"
       else
         "false")
  | VAddress addr -> fprintf ppf "\"%s\"" addr
  | VSumtype (constructor, typename, subtype, v) -> (
    match v with
    | VUnit () ->
      fprintf ppf "new %s_%s_constructor_subtype()"
        (show_sanitized_name @@ bind_sanitize_capitalize typename)
        constructor
    | _ ->
      (* TODO: something similar as above (not sure yet, waiting for a
         concrete example to try it out) *)
      fprintf ppf "new %s_%s_constructor_subtype{ %s_element = (%a)}"
        (show_sanitized_name @@ bind_sanitize_capitalize typename)
        constructor constructor
        (pp_value_to_csharp subtype)
        v)
  | VMap (t1, t2, m) ->
    fprintf ppf "new %s(){%a}"
      (normal_or_storage types storage)
      (pp_print_list ~pp_sep:(tag ",") (fun ppf (x, y) ->
           fprintf ppf "{%a,%a}" (pp_value_to_csharp t1) x
             (pp_value_to_csharp t2) y))
      m
  | VSet (t, m) ->
    fprintf ppf "new %s(){%a}"
      (normal_or_storage types storage)
      (pp_print_list ~pp_sep:(tag ",") (pp_value_to_csharp t))
      m
  | VList (t, m) ->
    fprintf ppf "new %s(new List<%a>(){%a})"
      (normal_or_storage types storage)
      (output_type_pretty ~prefix)
      t
      (pp_print_list ~pp_sep:(tag ",") (pp_value_to_csharp t))
      m
  | VBigMap (t1, t2, i) ->
    fprintf ppf "new %s(new %s.AbstractBigMap<%a,%a>(%a))"
      (normal_or_storage types storage)
      prefix
      (output_type_pretty ~prefix)
      t1
      (output_type_pretty ~prefix)
      t2 print_z_int_csharp i
  | VTuple (tl, vl) when Naming.has_name types ->
    Format.fprintf ppf "new %s(%a)"
      (show_sanitized_name @@ bind_capitalize (Naming.get_name_unsafe types))
      (fun ppf vl ->
        Factori_utils.pp_print_list_2 ~pp_sep:(tag ",")
          (fun ppf t v -> pp_value_to_csharp t ppf v)
          ppf tl vl)
      vl
  | VTuple (tl, vl) ->
    if first_tuple_as_parameters then
      fprintf ppf "%a"
        (fun ppf vl ->
          Factori_utils.pp_print_list_2 ~pp_sep:(tag ",")
            (fun ppf t v -> pp_value_to_csharp t ppf v)
            ppf tl vl)
        vl
    else
      fprintf ppf "new %s.MyTuple<%a>(%a)" prefix
        (pp_print_list ~pp_sep:(tag ",") (output_type_pretty ~prefix))
        tl
        (fun ppf vl ->
          Factori_utils.pp_print_list_2 ~pp_sep:(tag ",")
            (fun ppf t v -> pp_value_to_csharp t ppf v)
            ppf tl vl)
        vl
  | VLambda _l ->
    fprintf ppf
      "new %s (new MichelineArray() /* TODO: put the actual lambda from the \
       blockchain, some printing effort involved*/)"
      (normal_or_storage types storage)
  | VRecord (LeafP (field_name, (t, v))) ->
    fprintf ppf "%s = %a"
      (show_sanitized_name field_name)
      (pp_value_to_csharp t) v
  | VRecord (PairP _ as p) ->
    fprintf ppf "new %s(%a)"
      (match storage with
      | Some x -> show_sanitized_name @@ bind_capitalize x
      | None -> Format.asprintf "%a" (output_type_pretty ~prefix) types)
      (pp_pairedtype_typescript ~pairparens:false (* TODO *)
         ~f_leaf:(fun name ppf (t, x) ->
           match t with
           | TVar (n, Base b) -> (
             match Types.base_literal_type b with
             | Cunknown ->
               fprintf ppf "%s : new %s()" (show_sanitized_name name)
                 (show_sanitized_name @@ bind_capitalize n)
             | _ ->
               fprintf ppf "%s : new %s(%a)" (show_sanitized_name name)
                 (show_sanitized_name @@ bind_capitalize n)
                 (pp_value_to_csharp t) x)
           | _ ->
             fprintf ppf "%s : %a" (show_sanitized_name name)
               (pp_value_to_csharp t) x)
         ~f_pair:(fun ppf _d -> fprintf ppf "")
         ~delimiters:Nothing ~sep:",\n")
      p
  | VTicket s -> fprintf ppf "\"%s\"" s
  | VSaplingState s -> fprintf ppf "\"%s\"" s

(* Encoding *)

let encode_record ppf ep =
  match ep with
  | LeafP (n, _t) -> fprintf ppf "this.%s.Encode()" (show_sanitized_name n)
  | PairP _ as p ->
    pp_pairedtype_csharp_build
      (fun s ppf _t -> fprintf ppf "this.%s.Encode()" (show_sanitized_name s))
      ppf p

let pp_sumtype ~prefix f l ppf x =
  let rec aux ppf = function
    | true :: a -> fprintf ppf "%s.make_left(%a)" prefix aux a
    | false :: b -> fprintf ppf "%s.make_right(%a)" prefix aux b
    | [] -> fprintf ppf "%a" f x in
  aux ppf l

let output_sumtype_encode ~prefix ~typename ppf t =
  let lst = sumtype_to_list t in
  fprintf ppf "%a\n"
    (pp_print_list ~pp_sep:(tag "\n") (fun ppf (k, path) ->
         match k with
         | Annot (t, name) ->
           let name = show_sanitized_name @@ bind_sanitize_capitalize name in
           fprintf ppf
             "\t\tcase(%s_%s_constructor_subtype localarg):\n\t\t\treturn %a;"
             (show_sanitized_name typename)
             name
             (pp_sumtype ~prefix
                (fun ppf _at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "%s.UnitEncode(new %s.Unit ())" prefix prefix
                  | _ ->
                    fprintf ppf
                      "(localarg.%s_element == null ? throw new \
                       EncodeError(\"null\") : localarg.%s_element.Encode())"
                      name name)
                path)
             t
         | _t_local ->
           let name = constructor_from_path path in
           fprintf ppf
             "\t\tcase(%s_%s_constructor_subtype localarg):\n\t\t\treturn %a;"
             (show_sanitized_name typename)
             name
             (pp_sumtype ~prefix
                (fun ppf _at ->
                  match t with
                  | u_ when is_unit u_ ->
                    fprintf ppf "%s.UnitEncode(%s.unit)" prefix prefix
                  | _ ->
                    fprintf ppf
                      "(localarg.%s_element == null ? throw new \
                       EncodeError(\"null\") : localarg.%s_element.Encode)"
                      name name)
                path)
             t))
    lst

let encode_or ~has_element typename path =
  let path_as_michelson =
    List.rev_map
      (fun p ->
        if p then
          "Left"
        else
          "Right")
      path in
  List.fold_left
    (fun acc elt ppf () ->
      Format.fprintf ppf
        "new MichelinePrim{Prim=PrimType.%s, Args= new List<IMicheline>(){%a}}"
        elt acc ())
    (fun ppf () ->
      if has_element then
        Format.fprintf ppf "%s_element.Encode()" typename
      else
        Format.fprintf ppf "new MichelinePrim{Prim = PrimType.Unit}")
    path_as_michelson

let encode_constructor ppf real_type =
  match real_type with
  | Record r ->
    fprintf ppf "public IMicheline Encode() {\nreturn %a;\n}\n\n" encode_record
      r
  | Or _ ->
    fprintf ppf
      "public IMicheline Encode(){\nreturn _contentOr.Encode();\n\n}\n\n"
  | _ -> ()

(* Decoding *)

let decode_record ~prefix ppf ep =
  let rec aux_typename ppf = function
    | LeafP (_n, t) -> fprintf ppf "%a" (output_type_pretty ~prefix) t
    | PairP al ->
      fprintf ppf "%s.MyTuple<%a>" prefix
        (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ",") aux_typename)
        al in
  match ep with
  | LeafP (_n, t) -> fprintf ppf "%a" (output_type_pretty ~prefix) t
  | PairP al ->
    fprintf ppf "new %s.MyTuple<%a>" prefix
      (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ",") aux_typename)
      al

let filter_path b lst =
  List.filter_map
    (fun (t, path, acc_path) ->
      match path with
      | x :: xs when b = x -> Some (t, xs, x :: acc_path)
      | _ -> None)
    lst

let discriminate_path ~prefix ~typename argname ppf paths =
  let rec aux offset mpname ppf = function
    | [] -> failwith "TODO"
    | [(t_, [], rev_path)] -> begin
      match t_ with
      | Annot (u_, name) when is_unit u_ ->
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf
          "%s%s_%s_constructor_subtype res%s = new %s_%s_constructor_subtype();\n\
           _contentOr = res%s;\n"
          offset
          (show_sanitized_name typename)
          name name
          (show_sanitized_name typename)
          name name
      | Annot (t_, name) ->
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf
          "%s%s_%s_constructor_subtype res%s = new %s_%s_constructor_subtype();\n\
           res%s.%s_element = new %a(%s);\n\
           _contentOr = res%s;" offset
          (show_sanitized_name typename)
          name name
          (show_sanitized_name typename)
          name name name
          (output_type_pretty ~prefix)
          t_ argname name
      | t_ ->
        let name =
          sanitized_of_str @@ constructor_from_path (List.rev rev_path) in
        let name = show_sanitized_name @@ bind_sanitize_capitalize name in
        fprintf ppf
          "%s%s_%s_constructor_subtype res%s = new \
           %s_%s_constructor_subtype();\n\n\
          \           res%s.%s_element = new %a(%s);\n\
           _contentOr = res%s;" offset
          (show_sanitized_name typename)
          name name
          (show_sanitized_name typename)
          name name name
          (output_type_pretty ~prefix)
          t_ argname name
    end
    | lst ->
      fprintf ppf "\nswitch(%s){" argname ;
      begin
        let left_cons = filter_path true lst in
        let right_cons = filter_path false lst in
        let new_offset = sprintf "%s\t" offset in
        fprintf ppf
          "//Left case\n\
           case MichelinePrim %s:\n\
           switch(%s.Prim){\n\
           case PrimType.Left:\n\
           if(%s.Args == null){\n\
           throw new \
           DecodeError($\"{FactoriTypes.Utils.GetIMichelineString(%s)}PrimType.Left \
           should have an argument\");\n\
           }\n\
           %s=%s.Args[0];%s\n\
           %a\n\
           break;\n\n\
           case PrimType.Right:\n\
           if(%s.Args == null){\n\
           throw new \
           DecodeError($\"{FactoriTypes.Utils.GetIMichelineString(%s)}PrimType.Right \
           should have an argument\");\n\
           }\n\
           %s=%s.Args[0];%s\n\
           %a\n\
           break;\n\n\
          \           default:\n\
           throw new DecodeError($\"Could not decode {%s}, nor Left or \
           Right\");}\n\
           break;\n"
          mpname mpname mpname mpname argname mpname new_offset
          (aux new_offset (mpname ^ "l"))
          left_cons mpname mpname argname mpname new_offset
          (aux new_offset (mpname ^ "r"))
          right_cons argname
      end ;
      fprintf ppf
        "default:\n\
         throw new DecodeError($\"Could not decode {%s}, it should be a \
         Prim\");}\n"
        argname in

  aux "\t\t" "mp_" ppf paths

let output_sumtype_decode ~prefix ~typename argname ppf t =
  let lst = sumtype_to_list t in
  let lst = List.map (fun (t, path) -> (t, path, [])) lst in
  discriminate_path ~prefix ~typename argname ppf lst

let rec output_type_decode ~prefix ppf = function
  | Base b ->
    let typename =
      String.capitalize_ascii
        (asprintf "%a"
           (pp_base_type ~language:Factori_config.Csharp ~abstract:false
              ?prefix:None)
           b) in
    fprintf ppf "new %s.%s(%a)" prefix typename
  | Annot (ep, _) -> output_type_decode ~prefix ppf ep
  | Pair al ->
    fprintf ppf "new %s.MyTuple<%a>(%a)" prefix
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "new %s(%a)" (show_sanitized_name (Naming.get_or_create_name t))
  | Or _ as t ->
    fprintf ppf "new %s(%a)" (show_sanitized_name (Naming.get_or_create_name t))
  | TVar tn -> fprintf ppf "new %s(%a)" (show_sanitized_name (fst tn))
  | Unary (u, a) ->
    fprintf ppf "new %s.%s<%a>(%a)" prefix
      (str_of_unary ~camelcase:true ~csharp:true u)
      (output_type_pretty ~prefix)
      a
  | Contract m ->
    fprintf ppf "new %s.Contract<%a>(%a)" prefix (output_micheline ~prefix) m
  | Binary (bin, (k, v)) ->
    fprintf ppf "new %s.%s<%a,%a>(%a)" prefix
      (str_of_binary ~camelcase:true bin)
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v

let record_extract_decoded_values ?(is_one_field_record = false) m ppf
    record_name =
  let f _ (k : sanitized_name) (path, _) =
    if is_one_field_record then
      Format.asprintf "(%s%a)" record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf ".%a" print_item (x + 1)))
        path
    else
      Format.asprintf "%s = (%s%a)" (show_sanitized_name k) record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf ".%a" print_item (x + 1)))
        path in
  fprintf ppf "%a;" (fun ppf -> iterate_over_fields ppf m f Nothing) ";\n"

let decode_constructor ~prefix typename ppf m =
  match m with
  | Record r ->
    let is_one_field_record = is_one_field_record m in
    let lst = record_to_list r in
    fprintf ppf
      "public %s(IMicheline im) {\n\
       var before_projection = %a(im);\n\
       %a\n\
       %!}\n\n\
       public static %s.IFactoriT GenericDecode(IMicheline im){\n\
       return new %s(im);\n\
       }\n"
      typename (decode_record ~prefix) r
      (record_extract_decoded_values ~is_one_field_record lst)
      "before_projection" prefix
      typename (* (opening d) record_content (closing d) *)
  | Or _ ->
    fprintf ppf
      "public %s(IMicheline im){\n\
       %a\n\
       }\n\n\
       public static %s.IFactoriT GenericDecode(IMicheline im){\n\
       return new %s(im);\n\
       }\n"
      typename
      (output_sumtype_decode ~prefix ~typename:(sanitized_of_str typename) "im")
      m prefix typename
  | _ ->
    Format.fprintf ppf
      "public %s(IMicheline im) : base(im) {}\n\n\
       public static new %s.IFactoriT GenericDecode(IMicheline im){\n\
       return new %s(im);\n\
       }\n"
      typename prefix typename

(* Get Micheline *)

let rec output_sumtype_micheline ~prefix ppf or_t =
  pp_ortype_csharp
    (fun ppf t_ ->
      match t_ with
      | Annot (u_, _annot) when is_unit u_ ->
        (* later use annotation to rebuild micheline types and encoding *)
        fprintf ppf "(FactoriTypes.Types.Unit.GetMicheline())"
      | t_ -> fprintf ppf "%a" (output_type_micheline ~prefix) t_)
    ppf or_t

and micheline_record ~prefix ppf ep =
  let aux = function
    | LeafP (_n, t) -> fprintf ppf "(%a)" (output_type_micheline ~prefix) t
    | PairP _ as p ->
      pp_pairedtype_csharp_build ~mich_type:true
        (fun _s ppf t -> fprintf ppf "(%a)" (output_type_micheline ~prefix) t)
        ppf p in
  aux ep

and output_type_micheline ~prefix ppf = function
  | Base b ->
    fprintf ppf "%a.GetMicheline()"
      (pp_base_type ~language:Factori_config.Csharp ~abstract:false ~prefix)
      b
  | Annot (ep, _) -> fprintf ppf "%a" (output_type_micheline ~prefix) ep
  | TVar tn ->
    fprintf ppf "%s.GetMicheline()"
      (show_sanitized_name @@ bind_capitalize (fst tn))
  | Unary (u, a) ->
    fprintf ppf "(%s.%s<%a>.GetMicheline())" prefix
      (str_of_unary ~camelcase:true ~csharp:true u)
      (output_type_pretty ~prefix)
      a
  | Contract c ->
    fprintf ppf "%s.Contract<%a>.GetMicheline()" prefix
      (output_micheline ~prefix) c
  | Binary (b, (k, v)) ->
    fprintf ppf "(%s.%s<%a,%a>.GetMicheline())" prefix
      (String.capitalize_ascii (str_of_binary ~camelcase:true b))
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v
  | Or _ as t ->
    fprintf ppf "%s.GetMicheline()"
      (show_sanitized_name (Naming.get_or_create_name t))
  | Pair al ->
    fprintf ppf "(FactoriTypes.Types.MyTuple<%a>.GetMicheline())"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "%s.GetMicheline()"
      (show_sanitized_name (Naming.get_or_create_name t))

(* Micheline type decl *)
let micheline_type_decl ~prefix ppf (m : type_) =
  match m with
  | Or t ->
    Format.fprintf ppf
      "public static IMicheline GetMicheline(){\nreturn %a;\n}\n"
      (output_sumtype_micheline ~prefix)
      t
  | Record r ->
    Format.fprintf ppf
      "public static IMicheline GetMicheline(){\nreturn %a;\n}\n"
      (micheline_record ~prefix) r
  | _ -> ()

(* Generator *)

let rec output_sumtype_generator ~prefix ~typename ppf t_ =
  let lst = sumtype_to_list t_ in
  let lst_i = add_indices lst in
  fprintf ppf
    "%a_contentOr = Utils.chooseFrom<Content%s>(new List<Content%s>{\n%a\n});"
    (pp_print_list ~pp_sep:(tag ";\n\t") (fun ppf (i, (t_, path)) ->
         fprintf ppf "%a"
           (fun ppf at ->
             match at with
             | Annot (u_, name) when is_unit u_ ->
               let name = show_sanitized_name @@ bind_sanitize_capitalize name in
               fprintf ppf
                 "%s_%s_constructor_subtype res%d = new \
                  %s_%s_constructor_subtype();\n"
                 (show_sanitized_name @@ bind_capitalize typename)
                 name i
                 (show_sanitized_name @@ bind_capitalize typename)
                 name
             | Annot (t_, name) ->
               let name = show_sanitized_name @@ bind_sanitize_capitalize name in
               fprintf ppf
                 "%s_%s_constructor_subtype res%d = new \
                  %s_%s_constructor_subtype();\n\
                  res%d.%s_element=  %a;\n"
                 (show_sanitized_name @@ bind_capitalize typename)
                 name i
                 (show_sanitized_name @@ bind_capitalize typename)
                 name i name
                 (output_type_generator ~prefix)
                 t_
             | t ->
               let name = constructor_from_path path in
               fprintf ppf
                 "%s_%s_constructor_subtype res%d = new \
                  %s_%s_constructor_subtype();\n\
                  res%d.%s_element = %a;\n"
                 (show_sanitized_name @@ bind_capitalize typename)
                 name i
                 (show_sanitized_name @@ bind_capitalize typename)
                 name i
                 (show_sanitized_name @@ bind_sanitize_capitalize
                @@ sanitized_of_str name)
                 (output_type_generator ~prefix)
                 t)
           t_))
    lst_i
    (show_sanitized_name @@ bind_capitalize typename)
    (show_sanitized_name @@ bind_capitalize typename)
    (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, _) -> fprintf ppf "res%d" i))
    lst_i

and output_type_generator ~prefix ppf t =
  let output_type_generator = output_type_generator ~prefix in
  match t with
  | Base b ->
    let basename =
      String.capitalize_ascii
      @@ asprintf "%a"
           (pp_base_type ~language:Factori_config.Csharp ~abstract:false
              ?prefix:None)
           b in
    fprintf ppf "new %s.%s()" prefix basename
  | Annot (ep, _) -> fprintf ppf "%a" output_type_generator ep
  | Pair al ->
    fprintf ppf "(new %s.MyTuple<%a>())" prefix
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (output_type_pretty ~prefix))
      al
  | Record _ as t ->
    fprintf ppf "new %s()"
      (show_sanitized_name (bind_capitalize (Naming.get_or_create_name t)))
  | Or _ as t ->
    fprintf ppf "new %s()"
      (show_sanitized_name (bind_capitalize (Naming.get_or_create_name t)))
  | TVar tn ->
    fprintf ppf "new %s()" (show_sanitized_name (bind_capitalize (fst tn)))
  | Unary (u, a) ->
    fprintf ppf "new %s.%s<%a>()" prefix
      (str_of_unary ~camelcase:true ~csharp:true u)
      (output_type_pretty ~prefix)
      a
  | Contract c ->
    fprintf ppf "new %s.Contract<%a>()" prefix (output_micheline ~prefix) c
  | Binary (b, (k, v)) ->
    fprintf ppf "new %s.%s<%a,%a>()" prefix
      (str_of_binary ~camelcase:true b)
      (output_type_pretty ~prefix)
      k
      (output_type_pretty ~prefix)
      v

and generator_record ~prefix ~typename ppf ep =
  let aux = function
    | LeafP (_n, _x) ->
      fprintf ppf "new %s()" (show_sanitized_name @@ bind_capitalize typename)
    | PairP l ->
      let l = flattenP [] l in
      fprintf ppf "new %s(%a)"
        (show_sanitized_name @@ bind_capitalize typename)
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf (_n, x) -> output_type_generator ~prefix ppf x))
        l in
  aux ep

let generator_type_decl ~prefix ~typename ppf ep =
  match ep with
  | Record t ->
    let l =
      match t with
      | PairP l -> l
      | x -> [x] in
    let l = flattenP [] l in
    Format.fprintf ppf "public %s() : this(%a) {}\n\n"
      (show_sanitized_name @@ bind_capitalize typename)
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ",")
         (fun ppf (_n, x) ->
           fprintf ppf "new %a()" (output_type_pretty ~prefix) x))
      l
  | Or _ ->
    Format.fprintf ppf "public %s() {\n%a\n}\n\n"
      (show_sanitized_name @@ bind_capitalize typename)
      (output_sumtype_generator ~prefix ~typename)
      ep
  | _ ->
    Format.fprintf ppf "public %s() : base() {}\n\n"
      (show_sanitized_name @@ bind_capitalize typename)

(* Declare type *)

let make_converter_with_class typename classname ppf b =
  let basetypename = get_csharp_basetype b in
  match basetypename with
  | None -> ()
  | Some basetypename ->
    fprintf ppf
      "public static implicit operator %s(%s a) => (%s) (%a) a;\n\
       public static implicit operator %s(%s a) => (%s) (%a) a;" basetypename
      typename basetypename classname b typename basetypename typename classname
      b

let make_converter typename ppf b =
  let basetypename = get_csharp_basetype b in
  match basetypename with
  | None -> ()
  | Some basetypename ->
    fprintf ppf "public static implicit operator %s(%s a) => new %s(a);\n"
      typename basetypename typename

let rec define_sumtype_subtypes ~typename ~prefix ppf t =
  let lst = sumtype_to_list t in
  let rec aux res = function
    | [] -> res
    | (t_, path) :: xs ->
      let new_res =
        let cur_elem =
          match t_ with
          | Annot (u_, name) when is_unit u_ ->
            let name = show_sanitized_name @@ bind_sanitize_capitalize name in
            Format.asprintf
              "class %s_%s_constructor_subtype: Content%s{\n\
               \tpublic static string kind = \"%s_constructor\";\n\
               public override IMicheline Encode(){\n\
               return %a;\n\
               }\n\
               }\n"
              (show_sanitized_name typename)
              name
              (show_sanitized_name typename)
              name
              (encode_or ~has_element:false name path)
              ()
          | Annot (t, name) ->
            let name = show_sanitized_name @@ bind_sanitize_capitalize name in
            Format.asprintf
              "class %s_%s_constructor_subtype: Content%s{\n\
               \tpublic static string kind = \"%s_constructor\";\n\
               \tpublic %a %s_element {get; set; }\n\
               public override IMicheline Encode(){\n\
               return %a;\n\
               }\n\
              \   \n\
              \               public %s_%s_constructor_subtype() {\n\
               %s_element = new %a();\n\
               }\n\
               }\n"
              (show_sanitized_name typename)
              name
              (show_sanitized_name typename)
              name
              (output_type_pretty ~prefix)
              t name
              (encode_or ~has_element:true name path)
              ()
              (show_sanitized_name typename)
              name name
              (output_type_pretty ~prefix)
              t
          | t ->
            let name = constructor_from_path path in
            Format.asprintf
              "class %s_%s_constructor_subtype: Content%s{\n\
               \tpublic static string kind = \"%s_constructor\";\n\
               \tpublic %a %s_element {get; set; }\n\
               public override IMicheline Encode(){\n\
               return %a;\n\
               }\n\
               public %s_%s_constructor_subtype() {\n\
               %s_element = new %a();\n\
               }\n\
               }\n"
              (show_sanitized_name typename)
              name
              (show_sanitized_name typename)
              name
              (output_type_pretty ~prefix)
              t name
              (encode_or ~has_element:true name path)
              ()
              (show_sanitized_name typename)
              name name
              (output_type_pretty ~prefix)
              t in
        cur_elem :: res in
      aux new_res xs in
  fprintf ppf "\n%s\n\n" (String.concat "\n" (aux [] lst))

and declare_type_ ~prefix ~network (m : type_) ppf (name : sanitized_name) :
    unit =
  (* With inheritance, some types doesn't need getMicheline method *)
  output_verbose ~level:1
  @@ sprintf "[csharp][declare_type_] Entering with name %s"
       (show_sanitized_name name) ;
  let mk_init (name : sanitized_name) = name in
  let output_type_pretty = output_type_pretty ~prefix in
  let output_type_content = output_type_content in
  let typename = show_sanitized_name @@ bind_capitalize @@ mk_init name in
  let base_fun =
    match m with
    | Or _ ->
      let name = bind_capitalize name in
      (* let sumtype_content =
       *   Format.asprintf "%a" (declare_sumtype ~typename:name) m in *)
      fun f last ->
        fprintf ppf
          "public abstract class Content%s { public abstract IMicheline \
           Encode(); }\n\n\
           public class %s : %s.IFactoriT { \n\
           public Content%s _contentOr;\n\
           %a}"
          (show_sanitized_name name) (show_sanitized_name name) prefix
          (show_sanitized_name name) f last ;
        fprintf ppf "%a" (define_sumtype_subtypes ~prefix ~typename:name) m
    | Base b -> (
      let classname = pp_base_type ~language:Csharp ~abstract:false ~prefix in
      match Types.base_literal_type b with
      | Cunknown ->
        Format.fprintf ppf "public class %s : %a, %s.IFactoriT {\n%a}" typename
          classname b prefix
      | _ ->
        fprintf ppf
          "public class %s : %a, %s.IFactoriT{\n\
           public %s(%a v) : base(v) {}\n\
           %a\n\
           %a\n\
          \                         }" typename classname b prefix typename
          Types.pp_literal_base_type b (make_converter typename) b)
    | TVar (vname, Base b) -> (
      let classname = pp_base_type ~language:Csharp ~abstract:false ~prefix in
      match Types.base_literal_type b with
      | Cunknown ->
        Format.fprintf ppf "public class %s : %s, %s.IFactoriT {\n\n%a}"
          typename
          (show_sanitized_name @@ bind_capitalize vname)
          prefix
      | _ ->
        fprintf ppf
          "public class %s : %s, %s.IFactoriT {\n\
           public %s(%a v) : base(v) {}\n\
           %a\n\
           %a\n\
          \                 }" typename
          (show_sanitized_name @@ bind_capitalize vname)
          prefix typename Types.pp_literal_base_type b
          (make_converter_with_class typename classname)
          b)
    (* let typename =
     *   show_sanitized_name @@ bind_capitalize @@ mk_init name in
     * fprintf ppf "using %s = %s /*%s*/;" typename
     *   (show_sanitized_name @@ bind_capitalize vname) (show_basetype b) *)
    | TVar (vname, Record r) ->
      let l =
        match r with
        | PairP l -> l
        | x -> [x] in
      let l = flattenP [] l in
      fprintf ppf
        "public record %s : %s, %s.IFactoriT {\n\
         public %s(%a) : base(%a) {}\n\n\
         %a\n\n\
         }\n"
        typename
        (show_sanitized_name @@ bind_capitalize vname)
        prefix typename
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf (n, x) ->
             fprintf ppf "%a %s" output_type_pretty x (show_sanitized_name n)))
        l
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf (n, _x) -> fprintf ppf "%s" (show_sanitized_name n)))
        l
    | TVar (vname, _) ->
      fprintf ppf "public class %s : %s, %s.IFactoriT {%a\n\n}" typename
        (show_sanitized_name @@ bind_capitalize vname)
        prefix
    | Binary (BigMap, (k, v)) ->
      fprintf ppf
        "public class %s : %s.BigMap<%a,%a>, %s.IFactoriT  {\n\
         public %s(%s.BigMap<%a,%a> dict) : base(dict) {}\n\
         %a\n\
         }"
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty k output_type_pretty v prefix
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty k output_type_pretty v
    | Binary (Lambda, (k, v)) ->
      fprintf ppf
        "public class %s : %s.Lambda<%a,%a>, %s.IFactoriT  {\n\
         public %s(%s.Lambda<%a,%a> l) : base(l._body) { }\n\
         %a\n\
         }"
        typename prefix output_type_pretty k output_type_pretty v prefix
        typename prefix output_type_pretty k output_type_pretty v
      (* output_type_pretty k output_type_pretty v *)
    | Binary (Map, (k, v)) ->
      fprintf ppf
        "public class %s : %s.Map<%a,%a>, %s.IFactoriT  {\n\
         public %s(%s.Map<%a,%a> dict) : base(dict) { }\n\
         %a\n\
         }"
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty k output_type_pretty v prefix
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty k output_type_pretty v
    | Unary (List, t) ->
      fprintf ppf
        "public class %s : %s.MList<%a>, %s.IFactoriT  {\n\
         public %s(List<%a> l) : base(l) { }\n\
         %a\n\
         }"
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty t prefix
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        output_type_pretty t
    | Unary (Set, t) ->
      fprintf ppf
        "public class %s : %s.Set<%a>, %s.IFactoriT  {\n\
         public %s(%s.Set<%a> l) : base(l) { }\n\
         %a\n\
         } //maybe make this into an explicit set later?"
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty t prefix
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty t
    | Unary (Ticket, t) ->
      fprintf ppf
        "public class %s : %s.Ticket<%a>, %s.IFactoriT  {\n\
         public %s(%s.Ticket<%a> t) : base() {}\n\
         %a\n\
         }"
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty t prefix
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty t
    | Unary (Option, t) ->
      fprintf ppf
        "public class %s : %s.Option<%a>, %s.IFactoriT  {\n\
         public %s(%s.Option<%a> opt) : base(opt) { }\n\
         %a\n\
         }"
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty t prefix
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix output_type_pretty t
    | Contract t ->
      fprintf ppf
        "public class %s : %s.Contract<%a>, %s.IFactoriT  {\n\
         public %s(%s.Contract<%a> l) : base() {}\n\
         %a\n\
         }"
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix (output_micheline ~prefix) t prefix
        (show_sanitized_name @@ bind_capitalize @@ mk_init name)
        prefix (output_micheline ~prefix) t
    (* output_type_pretty t *)
    | Annot (ep, _) ->
      fun _ _ -> declare_type_ ~prefix ~network ep ppf (mk_init name)
    | Pair l ->
      let numbered_type_list = List.mapi (fun i x -> (i, x)) l in
      fprintf ppf
        "public class %s : %a, %s.IFactoriT  {\n\
         public %s(%a) : base(%a) {}\n\
         public %s(%a tuple) : base(%a) {}\n\n\
         %a\n\n\
        \       }" typename
        (output_type_content ~prefix)
        m prefix typename
        (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, t) ->
             fprintf ppf "%a x%d" (output_type_content ~prefix) t i))
        numbered_type_list
        (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, _) -> fprintf ppf "x%d" i))
        numbered_type_list typename
        (output_type_content ~prefix)
        m
        (pp_print_list ~pp_sep:(tag ",") (fun ppf (i, _) ->
             fprintf ppf "tuple.Item%d" (i + 1)))
        numbered_type_list
    | Record r ->
      let name = bind_capitalize name in
      let l =
        match r with
        | PairP l -> l
        | x -> [x] in
      let l = flattenP [] l in
      let classname = show_sanitized_name @@ bind_capitalize (mk_init name) in
      fprintf ppf
        "public record %s : Types.IFactoriT {\n\
         \t%a\n\
         public %s(%a){\n\
         \t\t%a}\n\n\
         %a\n\
         }"
        classname
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "\n\t")
           (fun ppf (n, x) ->
             fprintf ppf "public %a %s  { get; set;}" output_type_pretty x
               (show_sanitized_name n)))
        l classname
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf ",")
           (fun ppf (n, x) ->
             fprintf ppf "%a %s" output_type_pretty x (show_sanitized_name n)))
        l
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "\n\t\t")
           (fun ppf (n, _x) ->
             fprintf ppf "this.%s = %s;" (show_sanitized_name n)
               (show_sanitized_name n)))
        l in
  base_fun
    (fun ppf m ->
      let class_name = bind_capitalize (mk_init name) in
      let typename = show_sanitized_name class_name in
      Format.fprintf ppf "%a%a%a%a"
        (generator_type_decl ~prefix ~typename:(mk_init name))
        m
        (decode_constructor ~prefix typename)
        m encode_constructor m
        (micheline_type_decl ~prefix)
        m)
    m

let rec iterate_over_fields ppf (lst : (sanitized_name * _) list)
    (f : int -> sanitized_name -> 'a -> string) (d : delimiter) (sep : string) =
  let rec aux res count = function
    | [] -> List.rev res
    | (k, v) :: xs ->
      let new_res =
        let cur_elem = f count k v in
        cur_elem :: res in
      aux new_res (count + 1) xs in
  fprintf ppf "%s%s%s\n" (opening d)
    (String.concat sep (aux [] 0 lst))
    (closing d)

and record_extract_decoded_values ?(is_one_field_record = false) m ppf
    record_name =
  let sep =
    if is_one_field_record then
      Nothing
    else
      Curly in
  let f _ (k : sanitized_name) (path, _) =
    if is_one_field_record then
      Format.asprintf "(%s%a)" record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path
    else
      Format.asprintf "'%s' : (%s%a)" (show_sanitized_name k) record_name
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf "")
           (fun ppf x -> fprintf ppf "[%d]" x))
        path in
  fprintf ppf "return %a" (fun ppf -> iterate_over_fields ppf m f sep) ",\n"

and record_to_list ep =
  (* path is how we get to the element in the tuple *)
  let rec aux ~path = function
    | LeafP (field_name, t) -> [(field_name, (path, t))]
    | PairP l -> List.concat (List.mapi (fun i x -> aux ~path:(path @ [i]) x) l)
  in
  aux ~path:[] ep

let call_entrypoint ~contract_name ppf name =
  let _ = contract_name in
  let capitalized_ep_name = show_sanitized_name @@ bind_capitalize name in
  let ep_name = get_original name in
  fprintf ppf
    "public static async Task<string?> Call%s(Netezos.Keys.Key from, string \
     kt1, %s param, long fee, long gasLimit, long storageLimit, long amount = \
     0, string networkName = \"ghostnet\", bool debug = false){\n\
     var rpc = new TezosRpc(Blockchain.Functions.GetNode(networkName));\n\
     var encodedParam = param.Encode();\n\
     var opHash = await Blockchain.Functions.contractCall(from, kt1, \"%s\", \
     encodedParam, fee, gasLimit, storageLimit, amount,networkName, debug);\n\
     if(opHash != null) Console.WriteLine($\"Successful call to %s: {opHash}\");\n\
     else{ Console.WriteLine(\"Failed call to %s\");}\n\
     return opHash;\n\
     }"
    capitalized_ep_name capitalized_ep_name ep_name ep_name ep_name

let deploy ~storage_name ~contract_name ~network ppf () =
  let _ = (storage_name, network, ppf) in
  let contract_name = sanitized_of_str contract_name in
  let storage_type = show_sanitized_name @@ bind_capitalize storage_name in
  fprintf ppf
    {|public static async Task<string?> Deploy(Netezos.Keys.Key from, %s initial_storage, long fee, long gasLimit, long storageLimit, long amount = 0, string networkName = "ghostnet", bool debug=false){
  var encoded_initial_storage = initial_storage.Encode();
  var options = new System.Text.Json.JsonSerializerOptions();
  options.Converters.Add(new Netezos.Encoding.Serialization.MichelineConverter());
  string json1 = System.Text.Json.JsonSerializer.Serialize(encoded_initial_storage, options);
  if(debug){
    Console.WriteLine(json1);
  }
  string? kt1 = await Blockchain.Functions.DeployContract("%s",from, encoded_initial_storage,fee,gasLimit,storageLimit,amount,networkName,debug);
  return kt1;
}
|}
    storage_type
    (Csharp_SDK.code_path ~relative:true
       ~dir:(Factori_config.get_dir ())
       ~contract_name ())

let print_original_blockchain_storage ~prefix ~final_storage_type_ ~storage_name
    ~original_storage ppf () =
  let open Tzfunc.Proto in
  try
    let value =
      value_of_typed_micheline
        (* ~debug:(!Factori_options.verbosity > 0) *)
        original_storage final_storage_type_ in
    let return_storage_expression ppf value =
      match value with
      | VRecord _ ->
        fprintf ppf "return %a;"
          (pp_value_to_csharp ~first_tuple_as_parameters:true
             ~storage:storage_name ~prefix final_storage_type_)
          value
      | VTuple (_tl, _vl) ->
        fprintf ppf "return %a;"
          (pp_value_to_csharp ~first_tuple_as_parameters:true
             ~storage:storage_name ~prefix final_storage_type_)
          value
      | _ ->
        fprintf ppf "return %a;"
          (pp_value_to_csharp ~first_tuple_as_parameters:true
             ~storage:storage_name ~prefix final_storage_type_)
          value in
    fprintf ppf
      "\npublic static %s initial_blockchain_storage(){\n%a}\n/* %s\n%a*/\n\n"
      (show_sanitized_name @@ bind_capitalize storage_name)
      return_storage_expression value
      (EzEncoding.construct micheline_enc.json original_storage)
      show_value value
  with e ->
    handle_exception e ;
    fprintf ppf
      "\n\
       var initial_blockchain_storage = raise \"Could not download initial \
       storage, this is a bug\"\n\
       (*%s*)\n"
      (EzEncoding.construct micheline_enc.json original_storage)

let pp_interface_instruction_csharp_defs_only ~prefix ~network ppf ii =
  match ii with
  | Define_type (name, type_) ->
    fprintf ppf "%a\n\n" (declare_type_ ~prefix ~network type_) name
  | _ -> fprintf ppf ""

let pp_interface_instruction_csharp_calls_only ~contract_name ppf ii =
  match ii with
  | Entrypoint (name, _) -> call_entrypoint ~contract_name ppf name
  | _ -> fprintf ppf ""

let pp_big_map_access ~prefix ~value_class ~key_class ~name_big_map
    ~storage_class ~storage_name ~network ~last_name ppf f elt =
  let t_type_fun = output_type_pretty ~prefix in
  fprintf ppf
    {|public static async Task<%a?> GetBigMap%sElement(%a key,%a %s, string network = "%s"){
     %a@.
     return await %s.Get(key,network);
     }
|}
    t_type_fun value_class
    (show_sanitized_name @@ bind_sanitize_capitalize name_big_map)
    t_type_fun key_class t_type_fun storage_class
    (show_sanitized_name storage_name)
    (Factori_network.get_raw_network network)
    f elt
    (show_sanitized_name last_name)

let pp_pattern_position ~prefix position previous_name =
  (* Check for the problem of max tuple (for now just ignore it) *)
  let t_type_fun = output_type_pretty ~prefix in
  let previous_name = show_sanitized_name previous_name in
  match position with
  | In_pair (i, Pair t_l) ->
    let ty = List.nth t_l i in
    let new_name = sanitized_of_str (Format.sprintf "%s_%d" previous_name i) in
    let result_fun ppf =
      Format.fprintf ppf "%a %s = %s.%a;@.%a" t_type_fun ty
        (show_sanitized_name new_name)
        previous_name print_item (i + 1) in
    (result_fun, new_name)
  | In_record ((name_field, type_), Record _) ->
    let new_name =
      sanitized_of_str
        (Format.sprintf "%s_%s" previous_name (show_sanitized_name name_field))
    in
    let result_fun ppf =
      Format.fprintf ppf "%a %s = %s.%s;@.%a" t_type_fun type_
        (show_sanitized_name new_name)
        previous_name
        (show_sanitized_name name_field) in
    (result_fun, new_name)
  | _ ->
    raise (Factori_errors.GenericError ("Output_OCaml", "Incoherent types"))

let rec pp_interface_instruction_csharp ~prefix ~contract_name ~storage_name
    ~network ppf ii =
  match ii with
  | Define_type (_name, _type_) -> ()
  | Deploy -> deploy ~storage_name ~contract_name ~network ppf ()
  | Seq ii ->
    (pp_interface_csharp ~prefix ~contract_name ~storage_name ~network) ppf ii
  | Entrypoint (_, _) -> (* call_entrypoint ~contract_name ppf name *) ()
  | Big_map_access access ->
    let store_name = sanitized_of_str "storage" in
    let print_suite, big_map_name =
      List.fold_left
        (fun (f_acc, previous_name) pos ->
          let print_actual, new_one =
            pp_pattern_position ~prefix pos previous_name in
          ( (fun ppf next_f actual_elt ->
              f_acc ppf
                (fun ppf next_element -> print_actual ppf next_f next_element)
                actual_elt),
            new_one ))
        ((fun ppf f r -> Format.fprintf ppf "%a" f r), store_name)
        access.position in
    let pp_print_last =
      pp_big_map_access ~prefix ~value_class:access.value ~key_class:access.key
        ~storage_name:store_name ~name_big_map:access.bm
        ~storage_class:access.storage ~last_name:big_map_name ~network in
    pp_print_last ppf (fun ppf () -> print_suite ppf (fun _ _ -> ()) ()) ()

and pp_interface_csharp ~prefix ~network ~contract_name ~storage_name
    (ppf : formatter) (i : interface) =
  output_verbose ~level:3
    (asprintf "Entering pp_interface_csharp with interface %a" pp_interface i) ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_csharp_defs_only ~prefix ~network)
    ppf i ;
  (* In C#, functions have to be wrapped inside a class *)
  fprintf ppf "public class Functions{\n" ;
  pp_print_list
    (pp_interface_instruction_csharp ~prefix ~contract_name ~storage_name
       ~network)
    ppf i ;
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (pp_interface_instruction_csharp_calls_only ~contract_name)
    ppf i ;
  fprintf ppf "}"

let download_and_decode_storage ~storage_name ppf () =
  fprintf ppf
    {|
public static class Test{
    public async static Task test_storage_download(String kt1, String network)
{
    var storage_micheline = await Blockchain.Functions.getStorage(kt1, network);
    var storage = new %s(storage_micheline);
}
}
|}
    (show_sanitized_name @@ bind_capitalize storage_name)

let process_entrypoints ~prefix ~contract_name ~storage_name
    ~final_storage_type_ ~network ?(original_storage = None) ppf interface =
  let contract_name = show_sanitized_name contract_name in
  output_verbose ~level:2
    (asprintf
       "[csharp] Entering process_entrypoints with final storage: %a\n\
       \ and interface %a" pp_type_ final_storage_type_ pp_interface interface) ;
  let interface =
    try_overflow ~msg:"extract interface ocaml" (fun () -> interface) in
  let _ = check_interface_for_doubles interface in
  let original_blockchain_storage =
    match original_storage with
    | None -> "//No KT1"
    | Some original_storage ->
      Format.asprintf "\npublic class initialBlockchainStorage{\n%a\n}"
        (print_original_blockchain_storage ~prefix ~final_storage_type_
           ~storage_name ~original_storage)
        () in
  fprintf ppf
    "using FactoriTypes;\n\
     using System.Numerics;\n\
     using Netezos.Encoding;\n\
     using Netezos.Rpc;\n\
     using Netezos.Keys;\n\
     namespace %s{\n\
     %a\n\n\
     %a%s\n\
     }"
    contract_name
    (pp_interface_csharp ~prefix ~contract_name ~storage_name ~network)
    interface
    (download_and_decode_storage ~storage_name)
    () original_blockchain_storage
