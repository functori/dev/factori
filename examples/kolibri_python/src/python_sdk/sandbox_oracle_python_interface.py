from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

SetMaxDataDelaySec = factori_types.nat

def setMaxDataDelaySec_generator() -> SetMaxDataDelaySec:
	return factori_types.nat_generator ()

def setMaxDataDelaySec_encode (arg: SetMaxDataDelaySec):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in setMaxDataDelaySec_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setMaxDataDelaySec_decode (arg) -> SetMaxDataDelaySec:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in setMaxDataDelaySec_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setMaxDataDelaySec():
	temp=setMaxDataDelaySec_generator()
	temp_encoded=setMaxDataDelaySec_encode(temp)
	try:
		print('++++ setMaxDataDelaySec: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setMaxDataDelaySec: " + str(temp))
	temp1=setMaxDataDelaySec_decode(temp_encoded)
	if temp1 == temp:
		print('setMaxDataDelaySec check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setMaxDataDelaySec check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setMaxDataDelaySec()
#except Exception as e:
#	print('Error in assert_identity_setMaxDataDelaySec' + str(e))




def callSetMaxDataDelaySec(kt1 : str, _from : blockchain.identity, param : SetMaxDataDelaySec, networkName = "ghostnet", debug=False):
	input = setMaxDataDelaySec_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setMaxDataDelaySec", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = factori_types.address

def setGovernorContract_generator() -> SetGovernorContract:
	return factori_types.address_generator ()

def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetXtzUsdRate_callback = Tuple[str,Tuple[factori_types.timestamp,SetMaxDataDelaySec]]

def getXtzUsdRate_callback_generator() -> GetXtzUsdRate_callback:
	return (factori_types.tuple2_generator(factori_types.string_generator,
(factori_types.tuple2_generator(factori_types.timestamp_generator,
setMaxDataDelaySec_generator))))()

def getXtzUsdRate_callback_encode(arg : GetXtzUsdRate_callback) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.string_encode,factori_types.tuple2_encode(factori_types.timestamp_encode,setMaxDataDelaySec_encode))(arg) 
	except Exception as e:
		print('Error in getXtzUsdRate_callback_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getXtzUsdRate_callback_decode(arg) -> GetXtzUsdRate_callback:
	try:
		return factori_types.tuple2_decode(factori_types.string_decode,factori_types.tuple2_decode(factori_types.timestamp_decode,setMaxDataDelaySec_decode))(arg) 
	except Exception as e:
		print('Error in getXtzUsdRate_callback_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getXtzUsdRate_callback():
	temp=getXtzUsdRate_callback_generator()
	temp_encoded=getXtzUsdRate_callback_encode(temp)
	try:
		print('++++ getXtzUsdRate_callback: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getXtzUsdRate_callback: " + str(temp))
	temp1=getXtzUsdRate_callback_decode(temp_encoded)
	if temp1 == temp:
		print('getXtzUsdRate_callback check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getXtzUsdRate_callback check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getXtzUsdRate_callback()
#except Exception as e:
#	print('Error in assert_identity_getXtzUsdRate_callback' + str(e))




def callGetXtzUsdRate_callback(kt1 : str, _from : blockchain.identity, param : GetXtzUsdRate_callback, networkName = "ghostnet", debug=False):
	input = getXtzUsdRate_callback_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getXtzUsdRate_callback", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetXtzUsdRate = factori_types.contract

def getXtzUsdRate_generator() -> GetXtzUsdRate:
	return factori_types.contract_generator ()

def getXtzUsdRate_encode (arg: GetXtzUsdRate):
	try:
		return factori_types.contract_encode(arg)
	except Exception as e:
		print('Error in getXtzUsdRate_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getXtzUsdRate_decode (arg) -> GetXtzUsdRate:
	try:
		return factori_types.contract_decode(arg)
	except Exception as e:
		print('Error in getXtzUsdRate_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getXtzUsdRate():
	temp=getXtzUsdRate_generator()
	temp_encoded=getXtzUsdRate_encode(temp)
	try:
		print('++++ getXtzUsdRate: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getXtzUsdRate: " + str(temp))
	temp1=getXtzUsdRate_decode(temp_encoded)
	if temp1 == temp:
		print('getXtzUsdRate check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getXtzUsdRate check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getXtzUsdRate()
#except Exception as e:
#	print('Error in assert_identity_getXtzUsdRate' + str(e))




def callGetXtzUsdRate(kt1 : str, _from : blockchain.identity, param : GetXtzUsdRate, networkName = "ghostnet", debug=False):
	input = getXtzUsdRate_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getXtzUsdRate", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = factori_types.unit

def _default_generator() -> Constructor_default:
	return factori_types.unit_generator ()

def _default_encode (arg: Constructor_default):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _default_decode (arg) -> Constructor_default:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

State = factori_types.int_michelson

def state_generator() -> State:
	return factori_types.int_generator ()

def state_encode (arg: State):
	try:
		return factori_types.int_encode(arg)
	except Exception as e:
		print('Error in state_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def state_decode (arg) -> State:
	try:
		return factori_types.int_decode(arg)
	except Exception as e:
		print('Error in state_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_state():
	temp=state_generator()
	temp_encoded=state_encode(temp)
	try:
		print('++++ state: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of state: " + str(temp))
	temp1=state_decode(temp_encoded)
	if temp1 == temp:
		print('state check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('state check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_state()
#except Exception as e:
#	print('Error in assert_identity_state' + str(e))




class Constructor_storage(TypedDict):
	governorContractAddress: SetGovernorContract
	clientCallback: Optional[SetGovernorContract]
	harbingerContractAddress: SetGovernorContract
	maxDataDelaySec: SetMaxDataDelaySec
	state: State

def _storage_generator() -> Constructor_storage:
	return {'governorContractAddress' : setGovernorContract_generator (),'clientCallback' : (factori_types.option_generator(setGovernorContract_generator)) (),'harbingerContractAddress' : setGovernorContract_generator (),'maxDataDelaySec' : setMaxDataDelaySec_generator (),'state' : state_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([(factori_types.option_encode(setGovernorContract_encode)(arg['clientCallback'])), (setGovernorContract_encode(arg['governorContractAddress']))]), PairType.from_comb([(setGovernorContract_encode(arg['harbingerContractAddress'])), PairType.from_comb([(setMaxDataDelaySec_encode(arg['maxDataDelaySec'])), (state_encode(arg['state']))])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.option_decode(setGovernorContract_decode),setGovernorContract_decode)),(factori_types.tuple2_decode (setGovernorContract_decode,(factori_types.tuple2_decode (setMaxDataDelaySec_decode,state_decode))))))(arg)
		return {'clientCallback' : (before_projection[0][0]),
'governorContractAddress' : (before_projection[0][1]),
'harbingerContractAddress' : (before_projection[1][0]),
'maxDataDelaySec' : (before_projection[1][1][0]),
'state' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract sandbox_oracle")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/sandbox_oracle_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1