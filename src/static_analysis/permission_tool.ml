(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines several types including `permission`, `t`, `or_`, `a
v_node`, and `a sc_node`. It also provides functions for mapping, getting
argument, string conversion, and printing. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

type 'a permission =
  | Is_true of 'a
  | Is_none of 'a
  | Is_left of 'a
  | Is_cons of 'a
  | Not_empty of 'a
[@@deriving show]

let print_permission (f : Format.formatter -> 'a -> unit)
    (ppf : Format.formatter) = function
  | Is_true a -> Format.fprintf ppf "(%a)" f a
  | Is_none a -> Format.fprintf ppf "(%a is None)" f a
  | Is_left a -> Format.fprintf ppf "(%a is Left)" f a
  | Is_cons a -> Format.fprintf ppf "(%a is Cons)" f a
  | Not_empty a -> Format.fprintf ppf "(%a is Not_empty)" f a

let map_permission (f : 'a -> 'b) (p : 'a permission) : 'b permission =
  match p with
  | Is_true arg -> Is_true (f arg)
  | Is_none arg -> Is_none (f arg)
  | Is_left arg -> Is_left (f arg)
  | Is_cons arg -> Is_cons (f arg)
  | Not_empty arg -> Not_empty (f arg)

let fun_perm p =
  match p with
  | Is_true _ -> fun prop -> Is_true prop
  | Is_none _ -> fun prop -> Is_none prop
  | Is_left _ -> fun prop -> Is_left prop
  | Is_cons _ -> fun prop -> Is_cons prop
  | Not_empty _ -> fun prop -> Not_empty prop

type 'a logic =
  | Logic_or of 'a logic list
  | Logic_and of 'a logic list
  | Logic_not of 'a logic
  | True
  | False
  | P of 'a
[@@deriving show]

type 'a michelson_logic = 'a permission logic

let is_true (l : 'a logic) =
  match l with
  | True -> true
  | _ -> false

let is_false (l : 'a logic) =
  match l with
  | False -> true
  | _ -> false

(** Apply f : 'a -> 'a logic to all leaves of the logic formula l *)
let map_in_logic ~(f : 'a -> 'a logic) (l : 'a logic) : 'a logic =
  let rec map_in_logic l =
    match l with
    | Logic_or l ->
      let l = List.map map_in_logic l in
      Logic_or l
    | Logic_and l ->
      let l = List.map map_in_logic l in
      Logic_and l
    | Logic_not p ->
      let p = map_in_logic p in
      Logic_not p
    | True -> True
    | False -> False
    | P elt -> f elt in
  map_in_logic l

(** Apply f : 'a -> 'a logic to all leaves of the logic formula l and maintain an accumulator *)
let map_fold_in_logic ~(f : 'b -> 'a -> 'b * 'a logic) (acc : 'b) (l : 'a logic)
    : 'b * 'a logic =
  let rec map_in_logic acc l =
    match l with
    | Logic_or l ->
      let acc, l = List.fold_left_map map_in_logic acc l in
      (acc, Logic_or l)
    | Logic_and l ->
      let acc, l = List.fold_left_map map_in_logic acc l in
      (acc, Logic_and l)
    | Logic_not p ->
      let acc, p = map_in_logic acc p in
      (acc, Logic_not p)
    | True -> (acc, True)
    | False -> (acc, False)
    | P elt -> f acc elt in
  map_in_logic acc l

(** Apply f : 'a -> 'a logic to all leaves of the logic formula l and maintain an accumulator *)
let rec map_fold_in_logic_cps next
    (f : ('a logic -> 'a logic) -> 'b -> 'a -> 'b * 'a logic) (acc : 'b)
    (l : 'a logic) =
  match l with
  | Logic_or l ->
    let acc, l =
      Factori_utils.fold_map_left
        (map_fold_in_logic_cps (fun logic -> logic) f)
        acc l in
    (acc, next (Logic_or l))
  | Logic_and l ->
    let acc, l =
      Factori_utils.fold_map_left
        (map_fold_in_logic_cps (fun logic -> logic) f)
        acc l in
    (acc, next (Logic_and l))
  | Logic_not p ->
    let next p = next (Logic_not p) in
    map_fold_in_logic_cps next f acc p
  | True -> (acc, next True)
  | False -> (acc, next False)
  | P elt -> f next acc elt

(** Apply f : 'a -> 'b to all propositions inside the logic formula l  *)
let map_logic (f : 'a -> 'b) (l : 'a logic) : 'b logic =
  let rec map_logic l =
    match l with
    | Logic_or l ->
      let l = List.map map_logic l in
      Logic_or l
    | Logic_and l ->
      let l = List.map map_logic l in
      Logic_and l
    | Logic_not p ->
      let p = map_logic p in
      Logic_not p
    | True -> True
    | False -> False
    | P elt -> P (f elt) in
  map_logic l

let rec print_logic (f : Format.formatter -> 'a -> unit)
    (ppf : Format.formatter) = function
  | Logic_or l ->
    Format.fprintf ppf "(%a)"
      (Format.pp_print_list
         ~pp_sep:(fun ppf () -> Format.fprintf ppf " \\/ ")
         (print_logic f))
      l
  | Logic_and l ->
    Format.fprintf ppf "(%a)"
      (Format.pp_print_list
         ~pp_sep:(fun ppf () -> Format.fprintf ppf " /\\ ")
         (print_logic f))
      l
  | Logic_not p -> Format.fprintf ppf "(not %a)" (print_logic f) p
  | True -> Format.fprintf ppf "true"
  | False -> Format.fprintf ppf "false"
  | P p -> Format.fprintf ppf "%a" f p

(* Transform a formule to a nnf form *)
let rec nnf (logic : 'a logic) =
  match logic with
  | Logic_not (Logic_not p) -> nnf p
  | Logic_not (Logic_and l) ->
    let l = List.map (fun elt -> nnf (Logic_not elt)) l in
    Logic_or l
  | Logic_not (Logic_or l) ->
    let l = List.map (fun elt -> nnf (Logic_not elt)) l in
    Logic_and l
  | Logic_and l ->
    let l = List.map nnf l in
    Logic_and l
  | Logic_or l ->
    let l = List.map nnf l in
    Logic_or l
  | _ -> logic

(* Make smart or using concatenation *)
let make_or_logic a b =
  match (a, b) with
  | Logic_or l1, Logic_or l2 -> Logic_or (l1 @ l2)
  | a, Logic_or l | Logic_or l, a -> Logic_or (a :: l)
  | _ -> Logic_or [a; b]

(* Make smart and using concatenation *)
let make_and_logic a b =
  match (a, b) with
  | Logic_and l1, Logic_and l2 -> Logic_and (l1 @ l2)
  | a, Logic_and l | Logic_and l, a -> Logic_and (a :: l)
  | _ -> Logic_and [a; b]

(* Depending on the list in parameter, return an and logic, simple logic or the default
   value True *)
let and_or_simple l =
  match l with
  | [] -> True
  | [x] -> x
  | _ -> Logic_and l

(* Depending on the list in parameter, return an or logic, simple logic or the default
   value True *)
let or_simple l =
  match l with
  | [] -> True
  | [x] -> x
  | _ -> Logic_or l

(* Comparison between two formula using a compare function *)
let rec compare_logic (comp : 'a -> 'a -> int) logic_a logic_b =
  match (logic_a, logic_b) with
  | True, True | False, False -> 0
  | Logic_or l1, Logic_or l2 | Logic_and l1, Logic_and l2 -> (
    match List.compare_lengths l1 l2 with
    | 0 ->
      let l1 = List.sort (compare_logic comp) l1 in
      let l2 = List.sort (compare_logic comp) l2 in
      List.fold_left2
        (fun acc l_elt r_elt ->
          if acc = 0 then
            compare_logic comp l_elt r_elt
          else
            acc)
        0 l1 l2
    | res -> res)
  | P a, P b -> comp a b
  | Logic_not a, Logic_not b -> compare_logic comp a b
  | True, _ -> 1
  | _, True -> -1
  | Logic_and _, _ -> 1
  | _, Logic_and _ -> -1
  | Logic_not _, _ -> 1
  | _, Logic_not _ -> -1
  | Logic_or _, _ -> 1
  | _, Logic_or _ -> -1
  | P _, _ -> 1
  | _, P _ -> -1

(* Tells with a compare function if a and b are opposite,
   used for simplification *)
let find_opposite comp a b =
  match (a, b) with
  | a, Logic_not b | Logic_not b, a -> compare_logic comp a b = 0
  | _ -> false

(* This function make an or but tries to find some simplification by factoring *)
let fact_or comp a b =
  match (a, b) with
  | Logic_and la, Logic_and lb ->
    let factor, left, right =
      List.fold_left
        (fun (factor, acc_l, lb) (a : 'a logic) ->
          let factor, res, new_lb =
            List.fold_left
              (fun (acc, left, right) b ->
                match compare_logic comp a b with
                | 0 -> (make_and_logic a acc, None, right)
                | _ -> (acc, left, b :: right))
              (factor, Some a, []) lb in
          match res with
          | None -> (factor, acc_l, new_lb)
          | Some a -> (factor, a :: acc_l, new_lb))
        (True, [], lb) la in
    let left = and_or_simple left in
    let right = and_or_simple right in
    let new_one = make_or_logic left right in
    make_and_logic factor new_one
  (*| a, Logic_and l ->
     let new_l = List.filter (find_opposite comp a) l in
     make_or_logic a (Logic_and new_l)*)
  | _ -> make_or_logic a b

(* Sub function for the or simplification *)
let simplificate_or comp (l : 'a logic list) =
  let build = function
    | [] -> False
    | [x] -> x
    | res -> Logic_or (List.rev res) in
  let rec aux res = function
    | [] -> build res
    | True :: _ -> True
    | False :: xs -> aux res xs
    | Logic_or new_or :: xs -> aux res (new_or @ xs)
    | a :: xs -> (
      let e = List.exists (find_opposite comp a) res in
      match e with
      | true -> True
      | false ->
        let res = List.filter (fun b -> not (compare_logic comp a b = 0)) res in
        aux (a :: res) xs) in
  aux [] l

(* Sub function for the and simplification *)
let simplificate_and comp (l : 'a logic list) =
  let build = function
    | [] -> True
    | [x] -> x
    | res -> Logic_and (List.rev res) in
  let rec aux res = function
    | [] -> build res
    | False :: _ -> False
    | True :: xs -> aux res xs
    | Logic_and new_and :: xs -> aux res (new_and @ xs)
    | a :: xs -> (
      let e = List.exists (find_opposite comp a) res in
      match e with
      | true -> False
      | false ->
        let res = List.filter (fun b -> not (compare_logic comp a b = 0)) res in
        aux (a :: res) xs) in
  aux [] l

(* Main functions for formula simplification *)
let simplify (comp : 'a -> 'a -> int) (logic : 'a logic) =
  let rec aux_simp (logic : 'a logic) =
    match logic with
    | Logic_not True -> False
    | Logic_not False -> True
    | Logic_not (Logic_not p) -> aux_simp p
    | Logic_or l ->
      let l = List.map aux_simp l in
      simplificate_or comp l
    | Logic_and l ->
      let l = List.map aux_simp l in
      simplificate_and comp l
    | Logic_not a -> (
      let a = aux_simp a in
      match a with
      | True -> False
      | False -> True
      | Logic_not a -> a
      | _ -> Logic_not a)
    | _ -> logic in
  aux_simp logic

(* Evaluates a formula by using an environment hidden in the f function *)
let eval_logic (f : 'a -> bool) (logic : 'a logic) =
  let rec eval_logic = function
    | Logic_or l ->
      List.fold_left
        (fun acc elt ->
          let e = eval_logic elt in
          acc || e)
        true l
    | Logic_and l ->
      List.fold_left
        (fun acc elt ->
          let e = eval_logic elt in
          acc && e)
        true l
    | Logic_not a ->
      let eval_a = eval_logic a in
      not eval_a
    | True -> true
    | False -> false
    | P elt -> f elt in
  eval_logic logic

(* Simplifies a formula by using an environment hidden in the replace function *)
let replace_true (replace : 'a -> bool option) f =
  let rec aux_logic ffa =
    match ffa with
    | Logic_not a ->
      let fa = aux_logic a in
      Logic_not fa
    | Logic_and l ->
      let ffa = List.map aux_logic l in
      Logic_and ffa
    | Logic_or l ->
      let ffa = List.map aux_logic l in
      Logic_or ffa
    | P l -> (
      match replace l with
      | None -> P l
      | Some true -> True
      | Some false -> False)
    | _ -> ffa in
  aux_logic f

module Ordered_permission (A : Map.OrderedType) :
  Map.OrderedType with type t = A.t permission = struct
  type t = A.t permission

  let compare c1 c2 =
    match (c1, c2) with
    | Is_true a, Is_true b
    | Is_none a, Is_none b
    | Is_cons a, Is_cons b
    | Not_empty a, Not_empty b
    | Is_left a, Is_left b -> A.compare a b
    | Is_cons _, _ -> -1
    | _, Is_cons _ -> 1
    | Is_left _, _ -> -1
    | _, Is_left _ -> 1
    | Is_none _, _ -> -1
    | _, Is_none _ -> 1
    | Not_empty _, _ -> -1
    | _, Not_empty _ -> 1
end

let all_free_var comp f =
  let rec aux acc f =
    match f with
    | Logic_and list_f | Logic_or list_f -> List.fold_left aux acc list_f
    | Logic_not n_logic -> aux acc n_logic
    | P l ->
      if List.exists (fun elt -> comp elt l = 0) acc then
        acc
      else
        l :: acc
    | True -> acc
    | False -> acc in
  aux [] f

let rec fold_logic f acc l =
  match l with
  | Logic_not p -> fold_logic f acc p
  | Logic_or p_l | Logic_and p_l -> List.fold_left (fold_logic f) acc p_l
  | True -> acc
  | False -> acc
  | P p -> f acc p

module MapKey = Ordered_permission (String)
module MapPermission = Map.Make (Ordered_permission (String))

let print_map f ppf map =
  let map_b = MapPermission.bindings map in
  Format.pp_print_list
    ~pp_sep:(fun ppf () -> Format.fprintf ppf ";")
    (fun ppf (b, elt) ->
      Format.fprintf ppf "%a -> %b" (print_permission f) b elt)
    ppf map_b
