open Factori_permission_lib
open Permission_tool
module OrdPerm = Permission_tool.Ordered_permission (String)

let perm1 = Is_true "a"

let perm2 = Is_true "b"

let test_compare () =
  let comparison = OrdPerm.compare perm1 perm2 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:(-1)
    ~actual:comparison

let perm3 = Is_left "a"

let test_compare_2 () =
  let comparison = OrdPerm.compare perm1 perm3 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:1
    ~actual:comparison

let perm4 = (fun_perm perm1) "a"

let test_compare_3 () =
  let comparison = OrdPerm.compare perm1 perm4 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:0
    ~actual:comparison

let perm5 = map_permission (fun _ -> "b") perm1

let test_compare_4 () =
  let comparison = OrdPerm.compare perm2 perm5 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:0
    ~actual:comparison

(* At this point perm is considered as operational
   so we can directly pass it to alcotest *)
let alco_perm =
  let print_perm ppf perm =
    Permission_tool.print_permission
      (fun ppf s -> Format.fprintf ppf "%s" s)
      ppf perm in
  Alcotest.testable
    (fun fmt perm ->
      let str = Format.asprintf "%a" print_perm perm in
      Fmt.fmt "%s" fmt str)
    (fun perm1 perm2 -> OrdPerm.compare perm1 perm2 = 0)

let fo1 = P perm1

let fo2 = P perm2

let compare_logic = compare_logic OrdPerm.compare

let test_compare_f_1 () =
  let comparison = compare_logic fo1 fo2 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:(-1)
    ~actual:comparison

let fo3 = make_and_logic fo1 fo2

let fo4 = Logic_and [fo1; fo2]

let test_compare_f_2 () =
  let comparison = compare_logic fo3 fo4 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:0
    ~actual:comparison

let fo5 = make_and_logic fo3 fo4

let fo6 = Logic_and [fo1; fo2; fo1; fo2]

let test_compare_f_3 () =
  let comparison = compare_logic fo5 fo6 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:0
    ~actual:comparison

let fo5 = make_and_logic (P (Is_left "n")) fo4

let fo6 = make_and_logic fo4 (P (Is_left "n"))

let test_compare_f_4 () =
  let comparison = compare_logic fo5 fo6 in
  Alcotest.check' Alcotest.int ~msg:"Comparison two permission" ~expected:0
    ~actual:comparison

let alco_formula print_perm =
  Alcotest.testable
    (fun fmt formula ->
      let str =
        Format.asprintf "%a" (Permission_tool.print_logic print_perm) formula
      in
      Fmt.fmt "%s" fmt str)
    (fun warn1 warn2 -> compare_logic warn1 warn2 = 0)

let alco_formula =
  alco_formula (print_permission (fun ppf -> Format.fprintf ppf "%s"))

let simp1 = Logic_not True

let test_compare_simp () =
  let simpli = simplify OrdPerm.compare simp1 in
  Alcotest.check' alco_formula ~msg:"Comparison two permission" ~expected:False
    ~actual:simpli

let a = make_and_logic (P (Is_true "a")) (P (Is_true "b"))

let b = make_and_logic (P (Is_true "a")) (Logic_not (P (Is_true "b")))

let test_compare_factor () =
  let simpli = simplify OrdPerm.compare (fact_or OrdPerm.compare a b) in
  Alcotest.check' alco_formula ~msg:"Comparison two permission"
    ~expected:(P (Is_true "a")) ~actual:simpli

let a = make_and_logic (Logic_not (P (Is_true "a"))) (P (Is_true "b"))

let b = make_and_logic (P (Is_true "a")) (P (Is_true "b"))

let test_compare_factor_2 () =
  let simpli = simplify OrdPerm.compare (fact_or OrdPerm.compare a b) in
  Alcotest.check' alco_formula ~msg:"Comparison two permission"
    ~expected:(P (Is_true "b")) ~actual:simpli

(* Run it *)
let () =
  let open Alcotest in
  run "Permission_tool"
    [
      ( "permission",
        [
          test_case "Comparison test on permission 1" `Quick test_compare;
          test_case "Comparison test on permission 2" `Quick test_compare_2;
          test_case "Comparison test on permission 3" `Quick test_compare_3;
          test_case "Comparison test on permission 4" `Quick test_compare_4;
        ] );
      ( "formula",
        [
          test_case "Comparison test on formula 1" `Quick test_compare_f_1;
          test_case "Comparison test on formula 2" `Quick test_compare_f_2;
          test_case "Comparison test on formula 3" `Quick test_compare_f_3;
          test_case "Comparison test on formula 4" `Quick test_compare_f_4;
          test_case "Simplification test on formula" `Quick test_compare_simp;
          test_case "Factorisation test on formula 1" `Quick test_compare_factor;
          test_case "Factorisation test on formula 2" `Quick
            test_compare_factor_2;
        ] );
    ]
