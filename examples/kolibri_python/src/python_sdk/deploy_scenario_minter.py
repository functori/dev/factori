
from importlib.metadata import metadata
import minter_python_interface
import blockchain

def exec():
    storage = minter_python_interface._storage_generator()
    storage['lastInterestIndexUpdateTime'] = 0
    kt1 = minter_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= storage,
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 minter" + str(kt1) + " sent to a node.")
    return kt1
