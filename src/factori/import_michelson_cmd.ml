(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `import_michelson_cmd.ml` module provides the `import_michelson` command
to import a smart contract from its Michelson compiled form and generate code
for different languages and tools with various options. The command requires an
initialized directory structure, which can be created with the `init` command
provided by the `init_cmd.ml` module. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Factori_command
open Factori_options
open Factori_errors

let import_michelson =
  subcommand ~name:"import michelson"
    ~args:
      [
        dir_anonymous_option;
        contract_name_option;
        ocaml_language_option;
        typescript_language_option;
        python_language_option;
        csharp_language_option;
        field_prefixes_option;
        michelson_file_anonymous1;
        library_option;
        crawlori_option;
        crawlori_db_name_option;
        web_option;
        project_name_option;
        do_not_regroup_option;
      ]
    ~doc:
      "Import a contract using its Michelson compiled form. It can be in \
       either a Json or normal Michelson format." (fun () ->
      language_shenanigans () ;
      try
        (* First initialize directory structure if it is not already there
            *)
        if not (File_structure.is_initialized_dir ()) then Init_cmd.init_fun () ;
        let db_name = db_name () in
        let contract_name =
          match (!contract_name, !michelson_file) with
          | None, Some michelson_file ->
            Filename.basename (Filename.remove_extension michelson_file)
          | Some contract_name, Some _michelson_file -> contract_name
          | _, _ -> raise NoMichelsonFileProvided in
        Factori_utils.try_overflow ~msg:"import_from_michelson" (fun () ->
            let _ =
              Import_kt1.import_from_michelson ~library:!library_mode
                ~ctrct_name:contract_name ~crawlori:!crawlori ~db_name () in
            Format.eprintf "Successfully imported KT1.\n%!")
      with e -> handle_exception e)
