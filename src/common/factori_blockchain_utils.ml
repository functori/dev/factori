(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file provides functions for interacting with the Tezos
blockchain, including retrieving storage and entrypoints of smart contracts on a
specified network. Warning: this short summary was automatically generated and
could be incomplete or misleading. If you feel this is the case, please open an
issue. *)

open Factori_utils
open Factori_errors
open Format

let get_storage ~network kt1 =
  let open Proto in
  let open Factori_network in
  let open Tzfunc.Rp in
  let dir = Factori_config.get_dir () in
  let sort = Factori_config.sort_nodes ~dir in
  let rec aux ~failed =
    let node = get_raw_network ~failed ~sort network in
    match
      Lwt_main.run
        (Tzfunc.Node.get_account_info
           ~base:(get_base_of_network ~failed ~sort network)
           kt1)
    with
    | Error (`http_error (_, _) as e) ->
      output_verbose ~level:0
        (sprintf "[get_storage] Failed with node %s: %s\n%!" node
           (string_of_error e)) ;
      Factori_config.add_bad_node ~dir node ;
      aux ~failed:(node :: failed)
    | Error e ->
      let str_err = Tzfunc.Rp.string_of_error e in
      Format.eprintf "[Error in get_storage]: %s\n%!" str_err ;
      raise (GenericError ("get_storage", str_err))
    | Ok { ac_script = None; _ } ->
      Format.eprintf "[Error in get_storage]: No script for kt1: %s\n%!" kt1 ;
      raise (NoScript kt1)
    | Ok { ac_script = Some { code = se; _ }; _ } -> (
      let storage = get_storage_from_script_expr (Micheline se) in
      match storage with
      | None -> raise (GenericError ("get_storage", "No storage type"))
      | Some sto -> Lwt.return_ok sto) in
  aux ~failed:[]

let wrap_get_entrypoints_michelson_file se =
  match get_entrypoints se with
  | Error _ ->
    raise
      (GenericError
         ( "import_from_michelson",
           "Couldn't get entrypoints from Michelson file" ))
  | Ok entrypoints -> entrypoints

let wrap_get_all_entrypoints_michelson_file se =
  match get_entrypoints ~sub_entrypoints:true se with
  | Error _ ->
    raise
      (GenericError ("analyze", "Couldn't get entrypoints from Michelson file"))
  | Ok entrypoints -> entrypoints

let wrap_get_entrypoints_from_kt1 ?(failed = []) ~network ~kt1 () =
  let open Factori_network in
  let open Tzfunc.Rp in
  let dir = Factori_config.get_dir () in
  let sort = Factori_config.sort_nodes ~dir in
  let rec aux ~failed =
    let node = get_raw_network ~failed ~sort network in
    match
      Lwt_main.run
        (Tzfunc.Node.get_entrypoints
           ~base:(get_base_of_network ~failed ~sort network)
           kt1)
    with
    | Error (`http_error (_, _) as e) ->
      output_verbose ~level:0
        (sprintf "[wrap_get_entrypoints_from_kt1] Failed with node %s: %s" node
           (string_of_error e)) ;
      Factori_config.add_bad_node ~dir node ;
      aux ~failed:(node :: failed)
    | Error e ->
      raise
        (GenericError ("Error in get_entrypoints", Tzfunc.Rp.string_of_error e))
    | Ok entrypoints -> entrypoints in
  aux ~failed

(** TODO: debug why original storage need this 'kind' of `get_storage` and
   type__of_micheline needs the other one *)
let get_storage_alt ?(network = Factori_network.Mainnet) kt1 =
  let open Tzfunc.Rp in
  let open Factori_network in
  let dir = Factori_config.get_dir () in
  let sort = Factori_config.sort_nodes ~dir in
  let rec aux ~failed =
    let node = get_raw_network ~failed ~sort network in
    match
      Lwt_main.run
      @@ let>? storage =
           Tzfunc.Node.get_storage
             ~base:(Factori_network.get_base_of_network ~sort ~failed network)
             kt1 in
         Lwt.return_ok @@ storage
    with
    | Error (`http_error (_, _) as e) ->
      output_verbose ~level:0
        (sprintf "[get_storage] Failed with node %s: %s\n%!" node
           (string_of_error e)) ;
      Factori_config.add_bad_node ~dir node ;
      aux ~failed:(node :: failed)
    | Error e ->
      let str_err = Tzfunc.Rp.string_of_error e in
      Format.eprintf "[Error in get_storage]: %s\n%!" str_err ;
      raise (GenericError ("get_storage", str_err))
    | Ok m -> Lwt.return_ok m in
  aux ~failed:[]
