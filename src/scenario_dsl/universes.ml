(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines the concept of a universe in the context of
scenarios, which can be duplicated into subuniverses. It also includes functions
to print and create subuniverses, and utilizes the `deriving` plugin to
automatically derive `show` and `encoding` functions for the custom types
defined in the file. Warning: this short summary was automatically generated and
could be incomplete or misleading. If you feel this is the case, please open an
issue. *)

open Format

(* Every scenario happens inside a 'universe'. A traditional scenario,
   with no forks, lives in one boring universe U (also represented as
   U[], as a convention) and there isn't much to say about
   it. However, a universe can be duplicated into two subuniverses,
   say U[0] and U[1]. Every object gets a subscript so that these
   universes are (usually) disjoint. For example, if the 'mother'
   universe had a generated tz1 account Alice, then universe U[0] has
   a generated tz1 account Alice[0] and universe U[1] has a generated
   tz1 account Alice[1], and Alice[0] and Alice[1] are distinct. Of
   course, universes can be subdivided arbitrarily, so that U[0] can
   be split into U[0;0] and U[0;1] *)
type universe = int list [@@deriving show, encoding]

let print_universe ppf universe =
  fprintf ppf "%a"
    (pp_print_list ~pp_sep:(Factori_utils.tag "_") (fun ppf x ->
         fprintf ppf "%d" x))
    universe

let subuniverse universe i = universe @ [i]

type universe_id = {
  universe : universe;
  id : int;
}
[@@deriving show, encoding]
