open Common

module Path = struct
  let pp_print_args ppf =
    let pp_print_arg ppf (k, v) = Format.fprintf ppf "%s=%s" k v in
    let pp_print_arg_list ppf args =
      let pp_sep ppf () = Format.fprintf ppf "&" in
      Format.pp_print_list ~pp_sep pp_print_arg ppf args in
    function
    | [] -> Format.fprintf ppf ""
    | args -> Format.fprintf ppf "?%a" pp_print_arg_list args

  let pp_print ppf (path, args) =
    Format.fprintf ppf "/%s%a" path pp_print_args args

  let to_string = Format.asprintf "%a" pp_print

  let of_string path =
    let open Url in
    let paths =
      match url_of_string path with
      | None -> []
      | Some (Http http_url) | Some (Https http_url) -> http_url.hu_path
      | Some (File file_url) -> file_url.fu_path in
    match List.rev paths with
    | [] -> ""
    | path :: _ -> path

  let get () = of_string @@ Js.to_string Dom_html.window##.location##.href

  let set ?(args = []) path =
    Dom_html.window##.history##pushState
      (Js.string "") (Js.string "")
      (Js.some (Js.string @@ to_string (path, args)))
end

type t =
  | Home
  | Run of Index.t
[@@deriving jsoo]

let to_string = function
  | Home -> "home"
  | Run Index.{ name; _ } -> Format.sprintf "run-%s" name

let of_string ~indexes s =
  try
    Scanf.sscanf s "run-%s" @@ fun name' ->
    let index =
      List.find (fun Index.{ name; _ } -> String.equal name name') indexes in
    Run index
  with _ -> Home

let get ~indexes () = of_string ~indexes @@ Path.get ()

let set page = Path.set @@ to_string page
