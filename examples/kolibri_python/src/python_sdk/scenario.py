from importlib.metadata import metadata
import dev_fund_python_interface
import minter_python_interface
import oracle_python_interface
import oven_factory_python_interface
import oven_proxy_python_interface
import oven_registry_python_interface
import oven_python_interface
import oven_pool_python_interface
import sandbox_oracle_python_interface
import savings_pool_python_interface
import stability_fund_python_interface
import governor_python_interface
import token_python_interface
import deploy_scenario_dev_fund
import deploy_scenario_minter
import deploy_scenario_oracle
import deploy_scenario_oven_factory
import deploy_scenario_oven_proxy
import deploy_scenario_oven
import deploy_scenario_oven_registry
import deploy_scenario_sandbox_oracle
import deploy_scenario_savings_pool
import deploy_scenario_stability_fund
import deploy_scenario_token
import deploy_scenario_governor
import deploy_scenario_oven_pool
import time
import blockchain
import factori_types
from pytezos.michelson.types.domain import LambdaType
from pytezos.michelson.parse import michelson_to_micheline
from pytezos.operation.result import OperationResult

def call_governor_lambda(addr, entrypoint):
    michelson = '{ DROP; NIL operation; \
PUSH address ' + addr + ' ; CONTRACT%' + entrypoint['name'] + ' ' + entrypoint['types'] + '; IF_NONE { PUSH string "No Contract for this entrypoint"; FAILWITH } {}; PUSH mutez 0; \
PUSH ' + entrypoint['types'] + ' ' + entrypoint['value'] + '; TRANSFER_TOKENS; CONS; }'
    lmbd = LambdaType.match(michelson_to_micheline('lambda unit (list operation)')).from_micheline_value(michelson_to_micheline(michelson))
    return lmbd

def main():
    kt1_dev_fund=deploy_scenario_dev_fund.exec()
    time.sleep(2)
    kt1_minter=deploy_scenario_minter.exec()
    time.sleep(2)
    kt1_oracle=deploy_scenario_oracle.exec()
    time.sleep(2)
    kt1_oven_factory=deploy_scenario_oven_factory.exec()
    time.sleep(2)
    kt1_oven_proxy=deploy_scenario_oven_proxy.exec()
    time.sleep(2)
    kt1_oven=deploy_scenario_oven.exec()
    time.sleep(2)
    kt1_oven_registry=deploy_scenario_oven_registry.exec()
    time.sleep(2)
    kt1_sandbox_oracle=deploy_scenario_sandbox_oracle.exec()
    time.sleep(2)
    kt1_savings_pool=deploy_scenario_savings_pool.exec()
    time.sleep(2)
    kt1_stability_fund=deploy_scenario_stability_fund.exec()
    time.sleep(2)
    kt1_token=deploy_scenario_token.exec()
    time.sleep(2)
    kt1_governor=deploy_scenario_governor.exec(kt1_oven_registry)
    time.sleep(2)
    kt1_oven_pool=deploy_scenario_oven_pool.exec()
    time.sleep(2)
    print("Deploy operation all KT1 sent to a node.")
    op_hash= oven_registry_python_interface.callSetGovernorContract(kt1_oven_registry,blockchain.alice,kt1_governor,networkName="flextesa")
    print("Setting governor for oven_registry, check operation " + op_hash)
    time.sleep(2)
    lmbda = call_governor_lambda('"'+kt1_oven_registry+'"',{'name' : 'setOvenFactoryContract', 'types' : 'address', 'value' : '"'+kt1_oven_factory+'"'})
    op_hash = governor_python_interface.callRunLambda(kt1_governor,blockchain.bob,lmbda,networkName="flextesa")
    print("Setting factory address to oven registry, check operation " + op_hash)
    time.sleep(2)
    # For next we will let alice govern the contracts
    lmbda = call_governor_lambda('"'+kt1_oven_registry+'"', { 'name' : 'setGovernorContract', 'types' : 'address', 'value' : '"'+blockchain.alice['pkh']+'"'})
    op_hash = governor_python_interface.callRunLambda(kt1_governor,blockchain.bob,lmbda,networkName="flextesa")
    print("Alice is now the governor for the oven_registry contract, check operation " + op_hash)
    oven_proxy_python_interface.callSetOvenRegistryContract(kt1_oven_proxy,blockchain.alice,kt1_oven_registry,networkName="flextesa")
    time.sleep(2)
    oven_proxy_python_interface.callSetOracleContract(kt1_oven_proxy,blockchain.alice,kt1_oracle,networkName="flextesa")
    time.sleep(2)
    oven_proxy_python_interface.callSetMinterContract(kt1_oven_proxy,blockchain.alice,kt1_minter,networkName="flextesa")
    print("Alice set contracts for proxy")
    time.sleep(2)
    oven_factory_python_interface.callSetOvenRegistryContract(kt1_oven_factory,blockchain.alice,kt1_oven_registry,networkName="flextesa")
    time.sleep(2)
    oven_factory_python_interface.callSetOvenProxyContract(kt1_oven_factory,blockchain.alice,kt1_oven_proxy,networkName="flextesa")
    time.sleep(2)
    oven_factory_python_interface.callSetMinterContract(kt1_oven_factory,blockchain.alice,kt1_minter,networkName="flextesa")
    print("Alice set contracts for factory")
    time.sleep(2)
    minter_python_interface.callSetLiquidityPoolContract(kt1_minter,blockchain.alice,kt1_oven_pool,networkName="flextesa")
    time.sleep(2)
    minter_python_interface.callSetDeveloperFundContract(kt1_minter,blockchain.alice,kt1_dev_fund,networkName="flextesa")
    time.sleep(2)
    minter_python_interface.callSetOvenProxyContract(kt1_minter, blockchain.alice,kt1_oven_proxy,networkName="flextesa")
    time.sleep(2)
    minter_python_interface.callSetTokenContract(kt1_minter,blockchain.alice,kt1_token,networkName="flextesa")
    time.sleep(2)
    minter_python_interface.callSetStabilityFundContract(kt1_minter,blockchain.alice,kt1_stability_fund,networkName="flextesa")
    print("Alice set contracts for minter")
    time.sleep(2)
    stability_fund_python_interface.callSetOvenRegistryContract(kt1_stability_fund,blockchain.alice,kt1_oven_registry,networkName="flextesa")
    print("Alice set contracts for stability fund")
    print("KOLLIBRI SHOULD BE READY TO START")
    time.sleep(2)
    param=oven_factory_python_interface.makeOven_generator()
    op_hash=oven_factory_python_interface.callMakeOven(kt1_oven_factory,blockchain.bob,param,networkName="flextesa")
    print("Bob call makeOven of the factory")
    time.sleep(2)
    bob_oven=input("Please enter the oven you just created\n")
    print(bob_oven)
    #param=(blockchain.alice['pkh'], (blockchain.bob['pkh'],(0,(0,(True,(0,(0,0)))))))
    #oven_proxy_python_interface.callBorrow(kt1_oven_proxy,blockchain.alice, param, networkName = "flextesa",debug=True)
    
    
    
    
    
    
main()
