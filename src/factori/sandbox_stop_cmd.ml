(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `sandbox_stop_cmd.ml` module defines a subcommand for the `Factori`
application to stop the Flextesa sandbox. The `sandbox_stop` function takes no
arguments and returns `unit`. An example is provided on how to use this
subcommand along with other subcommands for the `Factori` application. Warning:
this short summary was automatically generated and could be incomplete or
misleading. If you feel this is the case, please open an issue. *)

open Factori_command

let sandbox_stop =
  subcommand ~name:"sandbox stop" ~args:[]
    ~doc:"Stop the Flextesa sandbox started with Factori." (fun () ->
      let cmd =
        Factori_utils.command_if_exists
          ~error_msg:"Please install docker first." ~cmd_name:"docker"
          "docker kill factori-sandbox" in
      let _ = Sys.command cmd in
      ())
