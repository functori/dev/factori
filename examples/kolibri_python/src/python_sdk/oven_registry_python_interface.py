from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

SetOvenFactoryContract = factori_types.address

def setOvenFactoryContract_generator() -> SetOvenFactoryContract:
	return factori_types.address_generator ()

def setOvenFactoryContract_encode (arg: SetOvenFactoryContract):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in setOvenFactoryContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setOvenFactoryContract_decode (arg) -> SetOvenFactoryContract:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in setOvenFactoryContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setOvenFactoryContract():
	temp=setOvenFactoryContract_generator()
	temp_encoded=setOvenFactoryContract_encode(temp)
	try:
		print('++++ setOvenFactoryContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setOvenFactoryContract: " + str(temp))
	temp1=setOvenFactoryContract_decode(temp_encoded)
	if temp1 == temp:
		print('setOvenFactoryContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setOvenFactoryContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setOvenFactoryContract()
#except Exception as e:
#	print('Error in assert_identity_setOvenFactoryContract' + str(e))




def callSetOvenFactoryContract(kt1 : str, _from : blockchain.identity, param : SetOvenFactoryContract, networkName = "ghostnet", debug=False):
	input = setOvenFactoryContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setOvenFactoryContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = SetOvenFactoryContract


def setGovernorContract_generator() -> SetGovernorContract:
	return setOvenFactoryContract_generator ()


def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return setOvenFactoryContract_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return setOvenFactoryContract_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

IsOven = SetOvenFactoryContract


def isOven_generator() -> IsOven:
	return setOvenFactoryContract_generator ()


def isOven_encode (arg: IsOven):
	try:
		return setOvenFactoryContract_encode(arg)
	except Exception as e:
		print('Error in isOven_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def isOven_decode (arg) -> IsOven:
	try:
		return setOvenFactoryContract_decode(arg)
	except Exception as e:
		print('Error in isOven_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_isOven():
	temp=isOven_generator()
	temp_encoded=isOven_encode(temp)
	try:
		print('++++ isOven: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of isOven: " + str(temp))
	temp1=isOven_decode(temp_encoded)
	if temp1 == temp:
		print('isOven check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('isOven check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_isOven()
#except Exception as e:
#	print('Error in assert_identity_isOven' + str(e))




def callIsOven(kt1 : str, _from : blockchain.identity, param : IsOven, networkName = "ghostnet", debug=False):
	input = isOven_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "isOven", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Constructor_default = factori_types.unit

def _default_generator() -> Constructor_default:
	return factori_types.unit_generator ()

def _default_encode (arg: Constructor_default):
	try:
		return factori_types.unit_encode(arg)
	except Exception as e:
		print('Error in _default_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _default_decode (arg) -> Constructor_default:
	try:
		return factori_types.unit_decode(arg)
	except Exception as e:
		print('Error in _default_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__default():
	temp=_default_generator()
	temp_encoded=_default_encode(temp)
	try:
		print('++++ _default: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _default: " + str(temp))
	temp1=_default_decode(temp_encoded)
	if temp1 == temp:
		print('_default check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_default check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__default()
#except Exception as e:
#	print('Error in assert_identity__default' + str(e))




def callConstructor_default(kt1 : str, _from : blockchain.identity, param : Constructor_default, networkName = "ghostnet", debug=False):
	input = _default_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "_default", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

AddOven = Tuple[SetOvenFactoryContract,SetOvenFactoryContract]

def addOven_generator() -> AddOven:
	return (factori_types.tuple2_generator(setOvenFactoryContract_generator,
setOvenFactoryContract_generator))()

def addOven_encode(arg : AddOven) -> PairType:
	try:
		return factori_types.tuple2_encode(setOvenFactoryContract_encode,setOvenFactoryContract_encode)(arg) 
	except Exception as e:
		print('Error in addOven_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def addOven_decode(arg) -> AddOven:
	try:
		return factori_types.tuple2_decode(setOvenFactoryContract_decode,setOvenFactoryContract_decode)(arg) 
	except Exception as e:
		print('Error in addOven_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_addOven():
	temp=addOven_generator()
	temp_encoded=addOven_encode(temp)
	try:
		print('++++ addOven: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of addOven: " + str(temp))
	temp1=addOven_decode(temp_encoded)
	if temp1 == temp:
		print('addOven check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('addOven check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_addOven()
#except Exception as e:
#	print('Error in assert_identity_addOven' + str(e))




def callAddOven(kt1 : str, _from : blockchain.identity, param : AddOven, networkName = "ghostnet", debug=False):
	input = addOven_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "addOven", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Constructor_storage(TypedDict):
	ovenMap: factori_types.BigMap
	ovenFactoryContractAddress: SetOvenFactoryContract
	governorContractAddress: SetOvenFactoryContract

def _storage_generator() -> Constructor_storage:
	return {'ovenMap' : (factori_types.big_map_generator(setOvenFactoryContract_generator,setOvenFactoryContract_generator)) (),'ovenFactoryContractAddress' : setOvenFactoryContract_generator (),'governorContractAddress' : setOvenFactoryContract_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([(setOvenFactoryContract_encode(arg['governorContractAddress'])), PairType.from_comb([(setOvenFactoryContract_encode(arg['ovenFactoryContractAddress'])), (factori_types.big_map_encode(setOvenFactoryContract_encode,setOvenFactoryContract_encode)(arg['ovenMap']))])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode (setOvenFactoryContract_decode,(factori_types.tuple2_decode (setOvenFactoryContract_decode,factori_types.big_map_decode(setOvenFactoryContract_decode,setOvenFactoryContract_decode)))))(arg)
		return {'governorContractAddress' : (before_projection[0]),
'ovenFactoryContractAddress' : (before_projection[1][0]),
'ovenMap' : (before_projection[1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract oven_registry")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/oven_registry_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1