(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Factori_representation
open Factori_micheline_lib
open Tzfunc.Proto

(* Ways to navigate inside values  *)
type path_elt =
  | Left
  | Right
  | First
  | Second
  | Get_opt
[@@deriving show]

let print_path ppf p =
  match p with
  | Left -> Format.fprintf ppf "LEFT"
  | Right -> Format.fprintf ppf "RIGHT"
  | First -> Format.fprintf ppf "CAR"
  | Second -> Format.fprintf ppf "CDR"
  | Get_opt -> Format.fprintf ppf "GET_OPT"

module Path = Map.Make (String)

(* maybe get back the annotation for now dropping them *)
let path_for_nested (n : Nested_arg.t) =
  let acc = Path.empty in
  let rec test acc (path_l : path_elt list) (n : Nested_arg.t) =
    match n with
    | Name (str, _) -> Result.ok @@ Path.add str path_l acc
    | Option (n, _) -> test acc (Get_opt :: path_l) n
    | Pair (f :: snd, _) ->
      let acc = test acc (First :: path_l) f in
      Result.bind acc @@ fun acc ->
      Result.bind (Nested_arg.pair_simple snd) @@ fun snd ->
      test acc (Second :: path_l) snd
    | Or (l, r, _) ->
      let acc = test acc (Left :: path_l) l in
      Result.bind acc @@ fun acc -> test acc (Right :: path_l) r
    | Pair _ ->
      Error
        (Factori_errors.UnstackMicheline "Pair of nested arg cannot be empty")
  in
  test acc [] n

type args = micheline list

(* Examples
   - Var_220
   - [PUSH,[int,5],[]] or [ADD,[],x;y]
   - In_Nested([Get_opt],option x_1) represents the value inside (option x_1)
*)

let pp_micheline = Micheline_lib.print_micheline ~simple:true

let pp_args = Format.pp_print_list ~pp_sep:Format.pp_print_space pp_micheline

let pp_primitive_or_macro ppf prim =
  Format.fprintf ppf "%s" (List.assoc prim Micheline_lib.prim_macro)

type prop =
  | Ident of Nested_arg.t
  | Propagate of
      primitive_or_macro
      * args
      * prop list (* (Instr,Instr implicit arguments,Instr stack arguments) *)
  | Real_name of string
  | In_Nested of path_elt list * prop
  | Equal of Nested_arg.t * prop
[@@deriving show]

let rec equal_prop (p : prop) (p2 : prop) =
  match (p, p2) with
  | Ident n, Ident n1 -> Nested_arg.equal n n1
  | Propagate (p, args_1, prop_l), Propagate (p2, args_2, prop_l2) ->
    let size_comp = List.compare equal_prop prop_l prop_l2 in
    let args_comp = List.compare compare args_1 args_2 in
    let prim_comp = compare p p2 in
    if size_comp = 0 && prim_comp = 0 && args_comp = 0 then
      0
    else if prim_comp <> 0 then
      prim_comp
    else if size_comp <> 0 then
      size_comp
    else
      args_comp
  | In_Nested (path_l, prop), In_Nested (path_l2, prop2) ->
    let path_comp = List.compare compare path_l path_l2 in
    if path_comp = 0 then
      equal_prop prop prop2
    else
      path_comp
  | Equal (nested1, prop1), Equal (nested2, prop2) -> (
    match Nested_arg.equal nested1 nested2 with
    | 0 -> equal_prop prop1 prop2
    | res -> res)
  | Real_name str1, Real_name str2 -> compare str1 str2
  | Equal _, _ -> 1
  | _, Equal _ -> -1
  | Ident _, _ -> 1
  | _, Ident _ -> -1
  | In_Nested _, _ -> 1
  | _, In_Nested _ -> -1
  | Propagate _, _ -> 1
  | _, Propagate _ -> -1

let rec inline_prop cp =
  match cp with
  | Propagate (p, args, [Propagate (`COMPARE, [], [cp1; cp2])]) -> (
    let cp1 = inline_prop cp1 in
    let cp2 = inline_prop cp2 in
    match Micheline_lib.comparison p with
    | false -> Propagate (p, args, [Propagate (`COMPARE, [], [cp1; cp2])])
    | true -> Propagate (p, args, [cp1; cp2]))
  | Propagate (p, args, prop_l) ->
    let prop_l = List.map inline_prop prop_l in
    Propagate (p, args, prop_l)
  | Ident n -> Ident n
  | In_Nested (path, elt) ->
    let elt = inline_prop elt in
    In_Nested (path, elt)
  | Equal (nested, elt) ->
    let elt = inline_prop elt in
    Equal (nested, elt)
  | Real_name x -> Real_name x

let map_prop ?(f_args = fun arg -> arg) ?(f_prop = fun prop -> prop) cp =
  let rec aux_map_prop cp =
    match cp with
    | Propagate (p, args, prop_l) ->
      let args = List.map f_args args in
      let prop_l =
        List.map
          (fun prop ->
            let p = f_prop prop in
            aux_map_prop p)
          prop_l in
      Propagate (p, args, prop_l)
    | In_Nested (path, elt) ->
      let p = f_prop elt in
      let prop = aux_map_prop p in
      In_Nested (path, prop)
    | Ident _ -> f_prop cp
    | Equal (nested, arg) ->
      let p = f_prop arg in
      let prop = aux_map_prop p in
      Equal (nested, prop)
    | Real_name _ ->
      let p = f_prop cp in
      p in
  aux_map_prop cp

let map_fold_prop f cps acc prop =
  let rec aux_prop cps acc prop =
    match prop with
    | Ident x ->
      let acc, x = f cps acc x in
      (acc, cps x)
    | In_Nested (path, prop) ->
      let cps new_res = cps (In_Nested (path, new_res)) in
      aux_prop cps acc prop
    | Propagate (prim, args, prop_l) ->
      let acc, prop_l =
        Factori_utils.fold_map_left (aux_prop (fun a -> a)) acc prop_l in
      (acc, cps (Propagate (prim, args, prop_l)))
    | Equal (nested, prop) ->
      let cps new_p = cps (Equal (nested, new_p)) in
      aux_prop cps acc prop
    | Real_name x -> (acc, cps (Real_name x)) in
  aux_prop cps acc prop

let depth_prop prop =
  let rec aux_depth_prop acc prop =
    match prop with
    | Propagate (_prim, _args, prop_l) ->
      List.fold_left
        (fun acc elt ->
          let d = aux_depth_prop (acc + 1) elt in
          if d > acc then
            d
          else
            acc)
        acc prop_l
    | In_Nested (_, prop) -> aux_depth_prop (acc + 1) prop
    | Equal (_, prop) -> aux_depth_prop (acc + 1) prop
    | Real_name _ -> acc
    | Ident _ -> acc in
  aux_depth_prop 0 prop

let print_prop ?(verbose = false) ?(nested_printer = Nested_arg.print)
    (ppf : Format.formatter) (prop : prop) =
  let prop = inline_prop prop in
  ignore verbose ;
  let rec print_prop (ppf : Format.formatter) (prop : prop) =
    let print_nested path elt =
      Format.fprintf ppf "[%a] (%a)"
        (Format.pp_print_list
           ~pp_sep:(fun ppf () -> Format.fprintf ppf ";")
           print_path)
        path print_prop elt in
    match prop with
    | Ident arg -> Format.fprintf ppf "%a" (nested_printer ~verbose) arg
    | Propagate (prim, _, [arg]) when Micheline_lib.comparison prim ->
      Format.fprintf ppf "%a %s 0" print_prop arg
        (Micheline_lib.pretty_prim prim)
    | Propagate (`MEM, _, [arg; (Ident _ as bm)]) ->
      Format.fprintf ppf "%a in %a" print_prop arg print_prop bm
    | Propagate (prim, [], [prop_l; prop_r])
      when Micheline_lib.associative_prim prim ->
      let pp_prim = Micheline_lib.pretty_prim prim in
      Format.fprintf ppf "(%a %s %a)" print_prop prop_l pp_prim print_prop
        prop_r
    | Propagate (prim, [], []) ->
      Format.fprintf ppf "%s" (List.assoc prim Micheline_lib.prim_macro)
    | Propagate (`PUSH, [_ty; data], []) ->
      Format.fprintf ppf "%s" (Micheline_lib.string_of_micheline data)
    (* | Propagate (`NOT, [], []) ->  *)
    | Propagate (prim, args, prop_l) ->
      let prim = Micheline_lib.pretty_prim prim in
      let print_l f =
        Format.pp_print_list
          ~pp_sep:(fun format () -> Format.fprintf format ", ")
          f in
      let print_args ppf args =
        match args with
        | [] -> Format.fprintf ppf " "
        | _ ->
          Format.fprintf ppf " args:[%a] "
            (print_l (fun ppf elt ->
                 Format.fprintf ppf "%s" (Micheline_lib.string_of_micheline elt)))
            args in
      Format.fprintf ppf "%s%a(%a)" prim print_args args (print_l print_prop)
        prop_l
    | In_Nested ([Get_opt], (Propagate (`GET, _, l) as elt)) -> (
      match List.rev l with
      | [(Ident _ as bm); arg] ->
        Format.fprintf ppf "%a[%a]" print_prop bm print_prop arg
      | (Ident _ as bm) :: xs ->
        Format.fprintf ppf "%a[(%a)]" print_prop bm
          (Format.pp_print_list
             ~pp_sep:(fun ppf () -> Format.fprintf ppf ",")
             print_prop)
          xs
      | _ -> print_nested [Get_opt] elt)
    | In_Nested (path, elt) -> print_nested path elt
    | Equal (nested, elt) ->
      Format.fprintf ppf "%a = %a" (nested_printer ~verbose) nested print_prop
        elt
    | Real_name x -> Format.fprintf ppf "%s" x in
  print_prop ppf prop

let prof_prop (prop : prop) =
  let break_prop acc = function
    | Ident _ -> acc
    | Propagate (_, _, p) -> List.rev_append p acc
    | In_Nested (_, p) -> [p]
    | Equal (_, p) -> [p]
    | Real_name _ -> acc in
  let rec aux_prof prof prop_l =
    let all = List.fold_left break_prop [] prop_l in
    match all with
    | [] -> prof
    | _ -> aux_prof (prof + 1) all in
  aux_prof 1 [prop]

let simple nested = Ident nested

let equal arg1 arg2 = Propagate (`EQ, [], [arg1; arg2])

let michelson_equal arg = Propagate (`EQ, [], [arg])

let sender = Propagate (`SENDER, [], [])

let prop_true = Propagate (`PUSH, [prim `bool; prim `True], [])

let prop_false = Propagate (`PUSH, [prim `bool; prim `False], [])

let prop_string arg = Propagate (`PUSH, [prim `string; Mstring arg], [])

let prop_compare arg1 arg2 = Propagate (`COMPARE, [], [arg1; arg2])
