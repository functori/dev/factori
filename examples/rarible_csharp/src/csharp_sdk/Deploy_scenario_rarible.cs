
using static Blockchain.Identities;
using static FactoriTypes.Types;
using System.Numerics;

class Deploy_scenario_rarible
{
    async static Task Main()
    {
        var storage = rarible.initialBlockchainStorage.initial_blockchain_storage();
        Bool b = new Bool(false);
        storage.paused = new rarible.Paused(b);
        storage.owner = new rarible.Constructor_operator(aliceFlextesa.Address);
        var kt1 = await rarible.Functions.Deploy(aliceFlextesa, storage, 1000000, 100000, 30000, 1000000, "flextesa", false);
        if (kt1 == null)
        {
            Console.WriteLine("Deployment seems to have failed, no KT1 in output.");
            return;
        }
        Console.WriteLine($"Deploy operation of KT1 {kt1} sent to a node.");
        Console.WriteLine("Waiting for its inclusion in a block ...");
        await Task.Delay(2000); // wait 2 seconds
        rarible.Constructor_operator account = new rarible.Constructor_operator(bobFlextesa.Address);
        rarible.Constructor_operator owner = new rarible.Constructor_operator(aliceFlextesa.Address);
        rarible.Token_id part = new rarible.Token_id(new BigInteger(50));
        rarible.Type7 onepart = new rarible.Type7(account, part);
        List<rarible.Type7> royal = new List<rarible.Type7> { onepart };
        rarible.Token_id amount = new rarible.Token_id(new BigInteger(200));
        rarible.Token_id token_id = new rarible.Token_id(new BigInteger(0));
        rarible.Mint mintParam = new rarible.Mint(iroyalties: royal,
            itokenMetadata: (MapGenerator<rarible.Ikey, rarible.Idata>(rarible.Functions.ikeyGenerator, rarible.Functions.idataGenerator))(),
            iamount: amount,
            iowner: owner,
            itokenid: token_id);
        var ophash = await rarible.Functions.CallMint(aliceFlextesa, kt1, mintParam, 1000000, 100000, 30000, 1000000, "flextesa", false);
        Console.WriteLine($"A mint operation is sent to a node. Its hash is: {ophash}");
    }
}