{%%template|
<div class="progress">
  <div
    :class="'progress-bar ' + (success ? 'bg-success' : '')"
    role="progressbar"
    :style="'width:' + percent + '%'"
    >
    <span> {{ pretty }}% </span>
  </div>
  <div
    v-if="success"
    class="progress-bar bg-danger"
    role="progressbar"
    :style="'width:'+ rev_percent +'%'"
    >
    <div v-if="percent === 0"> {{ pretty }}% </div>
  </div>
</div>
|}

let%prop max : int = { req }

and value : int = { req }

and success : bool = false

let number_to_string value =
  let isInterger value : bool =
    Js.Unsafe.fun_call
      (Js.Unsafe.js_expr "Number.isInteger")
      [|Js.Unsafe.inject value|] in
  let value = Ezjs_min.number_of_float value in
  let fixed =
    if isInterger value then
      0
    else
      2 in
  Js.to_string (value##toFixed fixed)

let get_percent ~max value =
  let value = Float.of_int value in
  let max = Float.of_int max in
  let percent = 100. *. value /. max in
  if Float.is_nan percent then
    0.
  else
    percent

let%comp percent this : float =
  let value = this##.value in
  let max = this##.max in
  get_percent ~max value

and pretty this : string =
  let value = this##.value in
  let max = this##.max in
  number_to_string @@ get_percent ~max value

and rev_percent this : float =
  let value = this##.value in
  let max = this##.max in
  get_percent ~max (max - value)

[%%comp { name = "v-progress-bar"; conv }]
