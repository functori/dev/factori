open Factori_analyze
open Factori_analyze_types
open Factori_analyze_print
open Factori_micheline_lib
open Analyze_types
open Tzfunc.Proto
open Factori_representation
open Factori_permission_lib

module OrdPerm = Permission_tool.Ordered_permission (struct
  type t = Permission_ast.prop

  let compare = Permission_ast.equal_prop
end)

let ( // ) = Factori_options.concat

module Constant = Nested_arg.Constant

let pwd = Sys.getcwd ()

let test_contracts = pwd // "test_contracts"

let get_new_sc kt1_or_michelson =
  let sc = Analyze.get_script kt1_or_michelson in
  Inline_unstack.transform_sc sc

let compare_logic = Permission_tool.compare_logic OrdPerm.compare

let check_constant_arg (form1, arg1) (form2, arg2) =
  let form = compare_logic form1 form2 in
  if form = 0 then
    Permission_ast.equal_prop arg1 arg2
  else
    form

let check_constant cst1 cst2 =
  Constant.compare
    (fun (var1, l1) (var2, l2) ->
      let var = compare var1 var2 in
      if var = 0 then
        List.compare check_constant_arg l1 l2
      else
        var)
    cst1 cst2

let compare_result ((princip_f1, arg_fail1), cst1)
    ((princip_f2, arg_fail2), cst2) =
  let princip = compare_logic princip_f1 princip_f2 in
  if princip = 0 then
    let arg_fail = Permission_ast.equal_prop arg_fail1 arg_fail2 in
    if arg_fail = 0 then
      check_constant cst1 cst2
    else
      arg_fail
  else
    princip

let result_alco =
  Alcotest.testable
    (fun fmt result ->
      Fmt.fmt "%s" fmt
        (Format.asprintf "%a"
           (Analyze_print.pp_permission ~verbose:false ?nested_printer:None)
           result))
    (fun res1 res2 ->
      match (res1, res2) with
      | ( { entrypoint = e1; parameter = param1; result = res1 },
          { entrypoint = e2; parameter = param2; result = res2 } )
        when e1 = e2 && Nested_arg.equal param1 param2 = 0 ->
        List.compare compare_result res1 res2 = 0
      | _ -> false)

let alco_warning =
  Alcotest.testable
    (fun fmt warning ->
      Fmt.fmt "%s" fmt
        (Format.asprintf "%a" Analyze_print.simple_warning_print warning))
    (fun warn1 warn2 ->
      match (warn1, warn2) with
      | (PermissionCheck perm_l1, 1), (PermissionCheck perm_l2, 1) ->
        let (module T) = result_alco in
        List.equal T.equal perm_l1 perm_l2
      | _ -> compare warn1 warn2 = 0)

let analyze name_file =
  let michelson = test_contracts // name_file in
  let script = Analyze.get_script (MichelsonFile michelson) in
  let new_sc = Inline_unstack.transform_sc script in
  let warning = Warnings.empty |> Analyze.check_unused_field new_sc in
  let warn_list = Warnings.bindings warning in
  warn_list

let analyze_perm name_file =
  let michelson = test_contracts // name_file in
  let script = Analyze.get_script (MichelsonFile michelson) in
  let new_sc = Inline_unstack.transform_sc script in
  let warning =
    Warnings.empty |> Analyze.PermissionCheck.permission_check new_sc in
  let warn_list = Warnings.bindings warning in
  warn_list

let no_warning name_file =
  let warn_list = analyze name_file in
  let warning_check = [] in
  Alcotest.check'
    (Alcotest.list alco_warning)
    ~msg:name_file ~expected:warning_check ~actual:warn_list

let analyze_unused_map_fail () =
  let name_file = "analyze_unused_map_fail.tz" in
  let warn_list = analyze name_file in
  let ty = prim `int in
  let ty_str = Micheline_lib.string_of_micheline ty in
  let warning_check =
    [(UnusedFieldCheck { var_name = "int_1"; code = ty_str }, 1)] in
  Alcotest.check'
    (Alcotest.list alco_warning)
    ~msg:name_file ~expected:warning_check ~actual:warn_list

let analyze_unused_foldmap () = no_warning "analyze_unused_foldmap.tz"

let analyze_unused_foldmap_fail () =
  let name_file = "analyze_unused_foldmap_fail.tz" in
  let warn_list = analyze name_file in
  let ty = prim `int in
  let ty_str = Micheline_lib.string_of_micheline ty in
  let warning_check =
    [(UnusedFieldCheck { var_name = "int_1"; code = ty_str }, 1)] in
  Alcotest.check'
    (Alcotest.list alco_warning)
    ~msg:name_file ~expected:warning_check ~actual:warn_list

let analyze_unused_iter_fail () =
  let name_file = "analyze_unused_iter_fail.tz" in
  let warn_list = analyze name_file in
  let ty = prim `list ~args:[prim `int] in
  let ty_str = Micheline_lib.string_of_micheline ty in
  let warning_check =
    [(UnusedFieldCheck { var_name = "list_2"; code = ty_str }, 1)] in
  Alcotest.check'
    (Alcotest.list alco_warning)
    ~msg:name_file ~expected:warning_check ~actual:warn_list

let analyze_perm_addr () =
  let name_file = "analyze_perm_addr.tz" in
  let warn_list = analyze name_file in
  let ty = prim `list ~args:[prim `int] in
  let ty_str = Micheline_lib.string_of_micheline ty in
  let warning_check =
    [(UnusedFieldCheck { var_name = "list_2"; code = ty_str }, 1)] in
  Alcotest.check'
    (Alcotest.list alco_warning)
    ~msg:name_file ~expected:warning_check ~actual:warn_list

let analyze_unused_iter () = no_warning "analyze_unused_iter.tz"

let analyze_unused_map () = no_warning "analyze_unused_map.tz"

let analyze_unused_foldmap_2 () = no_warning "analyze_unused_foldmap_2.tz"

let perm =
  let open Nested_arg in
  let open Permission_tool in
  let open Permission_ast in
  let storage = Name ("bool_1", prim `bool) in
  let name_var_0 = "var_0" in
  let var_0 = Real_name name_var_0 in
  let storage_proposition = P (Is_true (Ident storage)) in
  let principal_formula =
    make_and_logic storage_proposition (Logic_not (P (Is_true var_0))) in
  let iter_arg_name = "address_5" in
  (* this we need to see it in the return ast by printer*)
  let iter_arg = Name (iter_arg_name, prim `address) in
  let prop_right =
    Permission_ast.(michelson_equal (prop_compare sender (simple iter_arg)))
  in
  let false_scenario =
    make_and_logic storage_proposition (Logic_not (P (Is_true prop_right)))
  in
  let true_scenario =
    make_and_logic storage_proposition (P (Is_true prop_right)) in
  let sub_var_0 =
    (name_var_0, [(false_scenario, prop_false); (true_scenario, prop_true)])
  in
  let cst =
    Constant.singleton (Nested_arg.simple ("bool_10", prim `bool)) sub_var_0
  in
  {
    entrypoint = "default";
    parameter = Name ("set_0", prim `set ~args:[prim `address]);
    result = [((principal_formula, prop_string "FA2_INVALID_TRANSFERRER"), cst)];
  }

let check_perm () =
  let name_file = "perm_addr.tz" in
  let warn_list = analyze_perm name_file in
  let warning_check = [(PermissionCheck [perm], 1)] in
  Alcotest.check'
    (Alcotest.list alco_warning)
    ~msg:name_file ~expected:warning_check ~actual:warn_list

(* Run it *)
let () =
  let open Alcotest in
  run "Analyze"
    [
      ( "map-case",
        [
          test_case "Unused storage optional in MAP" `Quick
            analyze_unused_map_fail;
          test_case "Use of storage optional with MAP instruction" `Quick
            analyze_unused_map;
          test_case
            "MAP instruction on list adding to the accumulator and all elemtns \
             of the list"
            `Quick analyze_unused_foldmap;
          test_case
            "MAP instruction on list and adding to storage without connection"
            `Quick analyze_unused_foldmap_2;
          test_case
            "Use of storage list with MAP instruction without using one field \
             of the storage accumulator"
            `Quick analyze_unused_foldmap_fail;
        ] );
      ( "iter_case",
        [
          test_case "Use of storage optional with ITER instruction" `Quick
            analyze_unused_iter;
          test_case "Use of storage optional with ITER instruction" `Quick
            analyze_unused_iter_fail;
        ] );
      ("permission_case", [test_case "Test permission" `Quick check_perm]);
    ]
