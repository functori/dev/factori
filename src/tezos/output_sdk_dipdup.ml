(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file provides functions for generating Python code for defining
models that can interact with a smart contract on the Tezos blockchain. The file
includes functions for printing field types, additional field information,
fields, models, and a header for the models. The `process_models` function takes
a list of table information and generates the Python code for the models.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Format
open Infer_entrypoints

let pp_field_type ppf = function
  | CVarchar -> pp_print_string ppf "TextField"
  | CZarith -> pp_print_string ppf "BigIntField"
  | CInteger -> pp_print_string ppf "IntField"
  | CJson -> pp_print_string ppf "JsonField"
  | CTimestamp -> pp_print_string ppf "DateTimeField"
  | CBoolean -> pp_print_string ppf "BooleanFieldD"
  | CBigint -> pp_print_string ppf "BigInt"
  | CBytea -> failwith "TODO-dipdup : custom field for CBytea"
  | CArray _typ -> failwith "TODO-dipdup : custom field for CArray"

let pp_field_extra ppf field =
  fprintf ppf "%s%s"
    (if field.crawlori_column_option then
       "null=True"
     else
       "")
    (match field.crawlori_column_index with
    | Some _ -> "index=True"
    | _ -> "")

let pp_field ppf field =
  fprintf ppf "  %s = fields.%a(%a)" field.crawlori_column_name pp_field_type
    field.crawlori_column_type pp_field_extra field

let pp_fields ppf fields =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
    pp_field ppf fields

let pp_model ppf table =
  fprintf ppf "class %s(Model):\n%a"
    (String.capitalize_ascii table.crawlori_table_name)
    pp_fields table.crawlori_table_fields

let models_header ppf () =
  fprintf ppf
    {|from tortoise import Tortoise, fields
from dipdup.models import Model
|}

let process_models ppf tables =
  models_header ppf () ;
  pp_print_newline ppf () ;
  pp_print_list
    ~pp_sep:(fun ppf _ ->
      pp_print_newline ppf () ;
      pp_print_newline ppf ())
    pp_model ppf tables
