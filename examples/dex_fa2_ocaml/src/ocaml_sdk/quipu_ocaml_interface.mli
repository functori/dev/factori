open Tzfunc.Proto
open Factori_types

(*Type definition for set_position_pause *)
type set_position_pause = unit

(** Encode elements of type set_position_pause into micheline *)
val set_position_pause_encode : set_position_pause -> micheline

(** Decode elements of type micheline as set_position_pause *)
val set_position_pause_decode : micheline -> set_position_pause

(** Generate random elements of type set_position_pause*)
val set_position_pause_generator : unit -> set_position_pause

(** The micheline type corresponding to type set_position_pause*)
val set_position_pause_micheline : micheline

(*Type definition for type0 *)

type type0 =
  | Set_position_pause
  | Update_position_pause
  | X_to_y_pause
  | Y_to_x_pause

(** Encode elements of type type0 into micheline *)
val type0_encode : type0 -> micheline

(** Decode elements of type micheline as type0 *)
val type0_decode : micheline -> type0

(** Generate random elements of type type0*)
val type0_generator : unit -> type0

(** The micheline type corresponding to type type0*)
val type0_micheline : micheline

(*Type definition for set_pause *)
type set_pause = type0 set

(** Encode elements of type set_pause into micheline *)
val set_pause_encode : set_pause -> micheline

(** Decode elements of type micheline as set_pause *)
val set_pause_decode : micheline -> set_pause

(** Generate random elements of type set_pause*)
val set_pause_generator : unit -> set_pause

(** The micheline type corresponding to type set_pause*)
val set_pause_micheline : micheline

(** call entrypoint set_pause of the smart contract. *)
val call_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_pause ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint set_pause of the smart contract. *)
val forge_call_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_pause ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  set_pause ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  set_pause ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_set_pause :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  set_pause ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_set_pause :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:set_pause ->
  string Ast.astobj

(*Type definition for set_owner *)
type set_owner = address

(** Encode elements of type set_owner into micheline *)
val set_owner_encode : set_owner -> micheline

(** Decode elements of type micheline as set_owner *)
val set_owner_decode : micheline -> set_owner

(** Generate random elements of type set_owner*)
val set_owner_generator : unit -> set_owner

(** The micheline type corresponding to type set_owner*)
val set_owner_micheline : micheline

(** call entrypoint set_owner of the smart contract. *)
val call_set_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_owner ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint set_owner of the smart contract. *)
val forge_call_set_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_owner ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_set_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  set_owner ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_set_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  set_owner ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_set_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  set_owner ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_set_owner :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:set_owner ->
  string Ast.astobj

(*Type definition for set_dev_fee *)
type set_dev_fee = nat

(** Encode elements of type set_dev_fee into micheline *)
val set_dev_fee_encode : set_dev_fee -> micheline

(** Decode elements of type micheline as set_dev_fee *)
val set_dev_fee_decode : micheline -> set_dev_fee

(** Generate random elements of type set_dev_fee*)
val set_dev_fee_generator : unit -> set_dev_fee

(** The micheline type corresponding to type set_dev_fee*)
val set_dev_fee_micheline : micheline

(** call entrypoint set_dev_fee of the smart contract. *)
val call_set_dev_fee :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_dev_fee ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint set_dev_fee of the smart contract. *)
val forge_call_set_dev_fee :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  set_dev_fee ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_set_dev_fee :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  set_dev_fee ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_set_dev_fee :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  set_dev_fee ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_set_dev_fee :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  set_dev_fee ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_set_dev_fee :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:set_dev_fee ->
  string Ast.astobj

(*Type definition for fa2 *)

type fa2 = {
  token_id : set_dev_fee;
  token_address : set_owner;
}

(** Encode elements of type fa2 into micheline *)
val fa2_encode : fa2 -> micheline

(** Decode elements of type micheline as fa2 *)
val fa2_decode : micheline -> fa2

(** Generate random elements of type fa2*)
val fa2_generator : unit -> fa2

(** The micheline type corresponding to type fa2*)
val fa2_micheline : micheline

(*Type definition for token_y *)

type token_y =
  | Fa12 of set_owner
  | Fa2 of fa2

(** Encode elements of type token_y into micheline *)
val token_y_encode : token_y -> micheline

(** Decode elements of type micheline as token_y *)
val token_y_decode : micheline -> token_y

(** Generate random elements of type token_y*)
val token_y_generator : unit -> token_y

(** The micheline type corresponding to type token_y*)
val token_y_micheline : micheline

(*Type definition for cur_tick_index *)
type cur_tick_index = int_michelson

(** Encode elements of type cur_tick_index into micheline *)
val cur_tick_index_encode : cur_tick_index -> micheline

(** Decode elements of type micheline as cur_tick_index *)
val cur_tick_index_decode : micheline -> cur_tick_index

(** Generate random elements of type cur_tick_index*)
val cur_tick_index_generator : unit -> cur_tick_index

(** The micheline type corresponding to type cur_tick_index*)
val cur_tick_index_micheline : micheline

(*Type definition for deploy_pool *)

type deploy_pool = {
  cur_tick_index : cur_tick_index;
  token_x : token_y;
  token_y : token_y;
  fee_bps : set_dev_fee;
  tick_spacing : set_dev_fee;
  extra_slots : set_dev_fee;
}

(** Encode elements of type deploy_pool into micheline *)
val deploy_pool_encode : deploy_pool -> micheline

(** Decode elements of type micheline as deploy_pool *)
val deploy_pool_decode : micheline -> deploy_pool

(** Generate random elements of type deploy_pool*)
val deploy_pool_generator : unit -> deploy_pool

(** The micheline type corresponding to type deploy_pool*)
val deploy_pool_micheline : micheline

(** call entrypoint deploy_pool of the smart contract. *)
val call_deploy_pool :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  deploy_pool ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint deploy_pool of the smart contract. *)
val forge_call_deploy_pool :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  deploy_pool ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_deploy_pool :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  deploy_pool ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_deploy_pool :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  deploy_pool ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_deploy_pool :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  deploy_pool ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_deploy_pool :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:deploy_pool ->
  string Ast.astobj

(*Type definition for confirm_owner *)
type confirm_owner = set_position_pause

(** Encode elements of type confirm_owner into micheline *)
val confirm_owner_encode : confirm_owner -> micheline

(** Decode elements of type micheline as confirm_owner *)
val confirm_owner_decode : micheline -> confirm_owner

(** Generate random elements of type confirm_owner*)
val confirm_owner_generator : unit -> confirm_owner

(** The micheline type corresponding to type confirm_owner*)
val confirm_owner_micheline : micheline

(** call entrypoint confirm_owner of the smart contract. *)
val call_confirm_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  confirm_owner ->
  (string, Tzfunc__.Rp.error) result Lwt.t

(** forge_call entrypoint confirm_owner of the smart contract. *)
val forge_call_confirm_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  ?fee:int64 ->
  ?gas_limit:Z.t ->
  ?storage_limit:Z.t ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  confirm_owner ->
  ( Crypto.Raw.t
    * string
    * string
    * Proto.script_expr Proto.manager_operation list,
    Tzfunc__.Rp.error )
  result
  Lwt.t

val assert_failwith_str_confirm_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string ->
  prefix:string ->
  msg:string ->
  confirm_owner ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

(** Same as above, but the error may belong to a list of options *)
val assert_failwith_str_list_confirm_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:string list ->
  prefix:string ->
  msg:string ->
  confirm_owner ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val assert_failwith_generic_confirm_owner :
  ?node:string ->
  ?debug:bool ->
  ?amount:int64 ->
  enc:'a Json_encoding.encoding ->
  from:Blockchain.identity ->
  kt1:Tzfunc.Proto.A.contract ->
  expected:'a ->
  prefix:string ->
  msg:string ->
  confirm_owner ->
  (unit, Tzfunc__.Rp.error) result Lwt.t

val mk_call_confirm_owner :
  assert_success:bool ->
  msg:string ->
  scenario:Scenario_dsl.AstInterface.scenario ->
  from:'a Ast.astobj ->
  amount:'b Ast.astobj ->
  kt1:'c Ast.astobj ->
  network:'d Ast.astobj ->
  param:confirm_owner ->
  string Ast.astobj

(*Type definition for type4 *)

type type4 = {
  fee_bps : set_dev_fee;
  token_x : token_y;
  token_y : token_y;
}

(** Encode elements of type type4 into micheline *)
val type4_encode : type4 -> micheline

(** Decode elements of type micheline as type4 *)
val type4_decode : micheline -> type4

(** Generate random elements of type type4*)
val type4_generator : unit -> type4

(** The micheline type corresponding to type type4*)
val type4_micheline : micheline

(*Type definition for _storage *)

type _storage = {
  pool_ids : (type4, set_dev_fee) big_map;
  pool_count : set_dev_fee;
  owner : set_owner;
  dev_fee_bps : set_dev_fee;
  pause_state : type0 set;
  pending_owner : set_owner option;
  pools : (set_dev_fee, set_owner) big_map;
}

(** Encode elements of type _storage into micheline *)
val _storage_encode : _storage -> micheline

(** Decode elements of type micheline as _storage *)
val _storage_decode : micheline -> _storage

(** Generate random elements of type _storage*)
val _storage_generator : unit -> _storage

(** The micheline type corresponding to type _storage*)
val _storage_micheline : micheline

(** A function to deploy the smart contract.
           - amount is the initial balance of the contract
           - node allows to choose on which chain we are deploying
           - name allows to choose a name for the contract you are deploying
           - from is the account which will originate the contract (and pay for its origination)
           The function returns a pair (kt1,op_hash) where kt1 is the address of the contract
           and op_hash is the hash of the origination operation
       *)
val deploy :
  ?amount:int64 ->
  ?node:string ->
  ?name:string ->
  ?from:Blockchain.identity ->
  _storage ->
  (string * string, Tzfunc__.Rp.error) result Lwt.t

val mk_deploy :
  Scenario_dsl.AstInterface.scenario ->
  from:Scenario_value.identity Ast.astobj ->
  amount:Z.t Ast.astobj ->
  network:Scenario_value.network Ast.astobj ->
  storage:_storage ->
  string Ast.astobj

(** Downloads and decodes the storage, and then reencodes it.
Allows to check the robustness of the encoding and decoding functions. *)
val test_storage_download :
  kt1:Proto.A.contract ->
  node:string ->
  unit ->
  (unit, Tzfunc__.Rp.error) result

(** Initial storage as seen on the blockchain at the time of import. One possible use of this value is to copy it and modify it easily. *)
val initial_blockchain_storage : _storage
