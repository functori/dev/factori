(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module provides functions for compiling scenarios written in the
Factori DSL to different formats, including OCaml, TypeScript, and shell
scripts. The main functions include `ocaml_of_scenario`,
`typescript_of_scenario`, and `shell_of_scenario`, which compile scenarios to
their respective formats and pretty-print them to a specified formatter, with
optional parameters for including funding and emptying instructions in the
output. Warning: this short summary was automatically generated and could be
incomplete or misleading. If you feel this is the case, please open an issue. *)

open Scenario_dsl
open AstInterface
module AstToContextInstance = Ast_to_context.AstToContext (Context.Context)
open Format

(* Compilation to OCaml *)
let ocaml_of_scenario ?(funding = true) ?(emptying = true) ppf (s : scenario) =
  let context_from_ast = AstToContextInstance.compile (get_ast s) in
  fprintf ppf "%a"
    (Scenario_to_ocaml.pp_scenario_from_sequences_to_ocaml ~funding ~emptying)
    (List.concat
       (List.map Context.Context.context_to_sequences context_from_ast))

(* Compilation to Typescript *)
let typescript_of_scenario ?(funding = true) ?(emptying = true) ppf
    (s : scenario) =
  let context_from_ast = AstToContextInstance.compile (get_ast s) in
  fprintf ppf "%a"
    (Scenario_to_typescript.pp_scenario_from_sequences_to_typescript ~funding
       ~emptying)
    (List.concat
       (List.map Context.Context.context_to_sequences context_from_ast))

let shell_of_scenario ?(funding = true) ppf (s : scenario) =
  let context_from_ast = AstToContextInstance.compile (get_ast s) in
  fprintf ppf "%a"
    (Scenario_to_shell.pp_scenario_from_sequences_to_shell ~funding)
    (List.concat
       (List.map Context.Context.context_to_sequences context_from_ast))
