
from importlib.metadata import metadata
import getinterest_python_interface
import blockchain

def main():
    kt1 = getinterest_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= getinterest_python_interface._storage_generator(),
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 " + str(kt1) + " sent to a node.")
main()