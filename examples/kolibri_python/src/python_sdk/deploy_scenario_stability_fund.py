
from importlib.metadata import metadata
import stability_fund_python_interface
import blockchain

def exec():
    kt1 = stability_fund_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= stability_fund_python_interface._storage_generator(),
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 stability_fund " + str(kt1) + " sent to a node.")
    return kt1
