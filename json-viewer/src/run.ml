open Common

module Hash = struct
  {%%template|
  <p>{{ short }}</p>
  |}

  let%prop hash : string = { req }

  (* KT1abc..xyz *)
  let shortened = Pretty.shortened_by_the_middle ~max_len:11 ~front_len:6

  let%comp short this = shortened @@ Js.to_string this##.hash

  [%%comp { name = "v-hash"; conv; components }]
end

module Step = struct
  include Step

  {%%template|
  <i :title="title" :class="'bi ' + icon"/>
  |}

  let%prop step : t = { req }

  and info : string option = None

  let%comp icon this = icon @@ of_jsoo this##.step

  and title this =
    let title = pretty @@ of_jsoo this##.step in
    match Js.Optdef.to_option this##.info with
    | None -> title
    | Some info -> Format.sprintf "%s : %s" title @@ Js.to_string info

  [%%comp { name = "v-step"; conv; components }]
end

module StepIcon = struct
  {%%template|
  <v-step :step="step.kind" :style="'color: ' + color_result" :info="info" />
  |}

  let%prop step : Results.step = { req }

  let%comp color_result this =
    let open Results in
    let { result; _ } = step_of_jsoo this##.step in
    match result with
    | Result.Succeeded -> "green"
    | Result.Passed -> "grey"
    | Result.Failed _ -> "red"

  and info this =
    let open Results in
    let { time; _ } = step_of_jsoo this##.step in
    Format.asprintf "%a" Time.pp time

  [%%comp { name = "v-step-icon"; conv; components = [Step] }]
end

module TestLink = struct
  {%%template|
    <i title="link to test file" class="bi bi-search" />
  |}

  [%%comp { name = "v-test-link"; conv; components }]
end

module Timeout = struct
  {%%template|
    <i title="time-out" class="bi bi-clock" />
  |}

  [%%comp { name = "v-timeout"; conv; components }]
end

module ResultPill = struct
  {%%template|
  <div class="vertical-space">
    <a :href="log_link" target="_blank" class="vertical-space">
      <v-step-icon class="clickable" v-for="step in results.steps" :step="step" />
      <v-timeout v-if="timeout" class="clickable" style="color: red;" />
    </a>
    <a v-if="test_link != undefined" :href="test_link" target="_blank" class="clickable">
      <v-test-link />
    </a>
  </div>
  |}

  let%prop index : Index.t = { req }

  and results : Results.t = { req }

  and kt1 : string = { req }

  and test : Test.t = { req }

  let%comp log_link this : string =
    let Index.{ name; _ } = Index.of_jsoo this##.index in
    let kt1 = Js.to_string this##.kt1 in
    let test = Test.of_jsoo this##.test in
    Test.log_file name kt1 test

  and test_link this : string option =
    let Index.{ name; _ } = Index.of_jsoo this##.index in
    let kt1 = Js.to_string this##.kt1 in
    let test = Test.of_jsoo this##.test in
    Test.test_file name kt1 test

  and timeout this : bool = Results.(has_timeout @@ of_jsoo this##.results)

  [%%comp
  { name = "v-result-pill"; conv; components = [Timeout; TestLink; StepIcon] }]
end

module Tests = struct
  {%%template|
  <tr>
    <td>
      <span class="monospace" :title="kt1_entry.hash">#{{ short_hash }}</span>
    </td>
    <td>
      {{ kt1_entry.occurence }}
    </td>
    <td>
      <a :href="kt1_link" target="_blank" >
        <v-hash class="monospace" :hash="kt1_entry.kt1"/>
      </a>
    </td>
    <td v-for="test in tests">
      <v-result-pill
        v-if="results != undefined"
         :index="index"
         :results="test_results(test)"
         :kt1="kt1_entry.kt1"
         :test="test"
        />
    </td>
    <td>
      {{ kt1_entry.network }}
    </td>
  </tr>
  |}

  let%prop index : Index.t = { req }

  and kt1_entry : Kt1Entry.t = { req }

  and results : Outputs.results = { req }

  let%data tests : Test.t list = Test.all

  let%comp kt1_link this : string =
    let Kt1Entry.{ kt1; network; _ } = Kt1Entry.of_jsoo this##.kt1_entry_ in
    Format.sprintf "https://%s.tzkt.io/%s/code" network kt1

  and short_hash this : string =
    let Kt1Entry.{ hash; _ } = Kt1Entry.of_jsoo this##.kt1_entry_ in
    try String.sub hash 0 10 with _ -> hash

  let%meth test_results this test : Results.t option =
    TestMap.find_opt (Test.of_jsoo test)
    @@ Outputs.results_of_jsoo this##.results

  [%%comp { name = "v-tests"; conv; components = [Hash; ResultPill] }]
end

module OutputFilters = struct
  module ByStep = struct
    type t = {
      active : bool;
      step : Step.t;
      filter : Outputs.results -> bool;
    }
    [@@deriving jsoo]

    let make ~(test : Test.t) (step : Step.t) =
      let filter results =
        let open Results in
        match TestMap.find_opt test results with
        | Some { steps; _ } ->
          List.exists
            (function
              | { kind; result; _ } when step = kind -> Result.has_failed result
              | _ -> false)
            steps
        | None -> false in
      let active = false in
      { active; step; filter }

    let get_when_active { active; filter; _ } =
      if active then
        Some filter
      else
        None

    let%prop filter : t = { req }

    {%%template|
  <div class="btn-group-toggle d-inline-block">
    <label class="btn clickable" :style="filter.active ? 'color: red' : ''">
      <input type="checkbox" v-model="filter.active">
      <v-step :step="filter.step"/>
    </label>
  </div>
  |}

    [%%comp { name = "v-output-filter-by-step"; conv; components = [Step] }]
  end

  module ByTest = struct
    type t = {
      active : bool;
      test : Test.t;
      filter : Outputs.results -> bool;
    }
    [@@deriving jsoo]

    let make ~(test : Test.t) =
      let filter results =
        match TestMap.find_opt test results with
        | Some results -> Results.has_failed results
        | None -> false in
      let active = false in
      { active; test; filter }

    let get_when_active { active; filter; _ } =
      if active then
        Some filter
      else
        None

    let%prop filter : t = { req }

    {%%template|
  <div class="btn-group-toggle d-inline-block">
    <label class="btn clickable" :style="'font-weight: inherit;' + (filter.active ? 'color: red;' : '')">
      <input type="checkbox" v-model="filter.active">
      {{ pretty }}
    </label>
  </div>
  |}

    let%comp pretty this : string =
      Test.pretty @@ Test.of_jsoo this##.filter##.test

    [%%comp { name = "v-output-filter-by-test"; conv; components }]
  end

  type t = {
    test_filter : ByTest.t;
    steps_filters : ByStep.t list;
  }
  [@@deriving jsoo]

  let make (test : Test.t) =
    let test_filter = ByTest.make ~test in
    let steps_filters = List.map (ByStep.make ~test) (Test.steps test) in
    { test_filter; steps_filters }

  let get_when_active { test_filter; steps_filters } =
    ByTest.get_when_active test_filter
    :: List.map ByStep.get_when_active steps_filters
    |> List.filter_map @@ fun filter -> filter

  let%prop filters : t = { req }

  {%%template|
  <div class="vertical-space">
    <v-output-filter-by-test :filter="filters.test_filter" />
    <div class="vertical-space">
      <v-output-filter-by-step v-for="step_filter in filters.steps_filters" :filter="step_filter"/>
    </div>
  </div>
  |}

  [%%comp { name = "v-output-filters"; conv; components = [ByTest; ByStep] }]
end

module OutputCompare = struct
  type t = { compare : Outputs.output -> Outputs.output -> int }
  [@@deriving jsoo]

  let get { compare } = compare

  let by_hash =
    let compare (kt1_entry1, _) (kt1_entry2, _) =
      Kt1Entry.compare kt1_entry1 kt1_entry2 in
    { compare }

  let by_occurrence =
    let compare (kt1_entry1, _) (kt1_entry2, _) =
      Kt1Entry.compare_by_occurrence kt1_entry1 kt1_entry2 in
    { compare }
end

include Run

{%%template|
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand mr-2" href="https://functori.com">
    <img src="https://www.functori.com/assets/img/brand/favicon.png"
	 alt="Explorus"
	 class="mr-2"
	 style="height: 40px" >
    Functori
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/home">All Runs</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="https://functori.com/docs/factori">Factori Docs</a>
      </li>
    </ul>
    <v-version class="text-sm-left text-muted mr-1" :version="run.synopsis.version" />
    <form class="form-inline my-4 my-lg-0">
      <input
        type="text"
        class="form-control"
        placeholder="Search..."
        v-model="searching" />
    </form>

  </div>
</nav>


<div class="mx-4">
  <div id="stats" class="ml-5 mr-5 mt-4">
    <h5>
      <span :title="run.synopsis.nb_processed +' / ' + run.synopsis.nb_runs">
        {{ run.synopsis.nb_processed }}
      </span>
      Smart Contract Processed ({{ run_date }})
    </h5>
    <v-progress-bar
      :value="run.synopsis.nb_processed"
      :max="run.synopsis.nb_runs"
      />
    <table class="table text-center mt-3" style="table-layout: fixed;">
      <tr>
        <th style="border: none" v-for="test in tests">{{ pretty_test(test) }}</th>
      </tr>
      <tr>
        <td style="border: none" v-for="test in tests">
	  <v-progress-bar
            :value="test_success(test)"
            :max="run.synopsis.nb_processed"
            success
            />
        </td>
      </tr>
    </table>
  </div>
  <div style="
	      display: flex;
	      width: 100%;
	      justify-content: space-around;
	      background-color: #e4e4e4;
	      padding: 5px 10px;">
	<div v-for="step in steps" class="vertical-space">
	  <span>{{ pretty_step(step) }}:</span>
	  <span><v-step :step="step" /></span>
	</div>
	<div class="vertical-space">
	  <span>Time-out:</span>
	  <span><v-timeout /></span>
	</div>
	<div class="vertical-space">
	  <span>Link to test file:</span>
	  <span><v-test-link /></span>
	</div>
  </div>
  <table id="result-table" class="table table-striped text-center" style="table-layout: fixed">
    <thead class="thead-dark" style="opacity: 0.9">
      <tr>
        <th @click="sort_by_hash" scope="col">Hash</th>
        <th @click="sort_by_occurrence" scope="col">Occurrences</th>
        <th scope="col">KT1</th>
        <th v-for="filters in tests_filters" scope="col">
	  <v-output-filters :filters="filters"/>
	</th>
        <th scope="col">Network</th>
      </tr>
    </thead>
    <tbody>
      <v-tests
        v-for="output in filtered_outputs"
        :index="run.index"
        :kt1_entry="output[0]"
        :results="output[1]"
	/>
    </tbody>
  </table>
</div>
|}

let%prop run : t = { req }

let%data outputs : Outputs.t = Outputs.empty

and tests : Test.t list = Test.all

and steps : Step.t list = Step.all

and searching : string = ""

and ocaml_failed_filter : bool = false

and typescript_failed_filter : bool = false

and tests_filters : OutputFilters.t list = List.map OutputFilters.make Test.all

and output_compare : OutputCompare.t = OutputCompare.by_hash

let%comp[@noconv] filtered_outputs this : Outputs.jsoo Ezjs_min.t =
  let searching_regexp = new%js Js.regExp this##.searching##toLowerCase in
  let search s =
    let s = Js.string s in
    s##toLowerCase##search searching_regexp != -1 in
  let tests_filters =
    Ezjs_min.to_listf OutputFilters.of_jsoo this##.tests_filters_ in
  let output_compare = OutputCompare.of_jsoo this##.output_compare_ in
  let filters = List.concat_map OutputFilters.get_when_active tests_filters in
  let filter kt1_entry results =
    let pass_filters =
      match filters with
      | [] -> true
      | _ -> List.exists (fun filter -> filter results) filters in
    let search_filter =
      search @@ String.lowercase_ascii kt1_entry.Kt1Entry.kt1 in
    pass_filters && search_filter in
  Outputs.of_jsoo this##.outputs
  |> Kt1EntryMap.filter filter
  |> Outputs.to_sorted_jsoo ~compare:(OutputCompare.get output_compare)

and run_date this : string =
  Date.(to_string @@ of_jsoo this##.run##.index##.date)

let%meth pretty_test _this test = Test.(pretty @@ of_jsoo test)

and pretty_step _this step =
  String.capitalize_ascii Step.(pretty @@ of_jsoo step)

and test_success this test : int =
  number_of_test_success ~test:(Test.of_jsoo test) (of_jsoo this##.run)

and sort_by_hash this =
  this##.output_compare_ := OutputCompare.(to_jsoo by_hash)

and sort_by_occurrence this =
  this##.output_compare_ := OutputCompare.(to_jsoo by_occurrence)

let load_data this =
  EzLwtSys.run @@ fun () ->
  let name = Js.to_string this##.run##.index##.name in
  let outputs = Outputs.get name in
  Lwt.bind outputs @@ fun outputs ->
  this##.outputs := Outputs.to_jsoo outputs ;
  Lwt.return_unit

[%%beforeMount fun this -> load_data this]

[%%comp
{
  name = "v-run";
  conv;
  components =
    [Step; Timeout; TestLink; V_version; V_progress_bar; Tests; OutputFilters];
}]
