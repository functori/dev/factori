(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml module, part of the Factori project, provides types and functions
to generate TypeScript files including imports and functions, and includes
functions to print those constructs as strings. The module defines several types
and functions, and an example is given to demonstrate the usage of the
`typescript_file` type and the `print_ts_file` function. Warning: this short
summary was automatically generated and could be incomplete or misleading. If
you feel this is the case, please open an issue. *)

open Format
(* This file is intended to help write typescript files for
   a factori project *)

(* open Ez_file.FileString *)

type filename = string [@@deriving encoding]

type import_value = {
  imported : string list;
  source : string;
}
[@@deriving encoding]

(* import * as X from "../X"; *)
type import_as = {
  alias : string;
  source : filename;
}
[@@deriving encoding]

type import =
  | Import_as of import_as
  | Import_value of import_value
[@@deriving encoding]

let print_import_value ppf (i : import_value) =
  fprintf ppf {|import {
  %a} from %s
|}
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",\n")
       (fun ppf x -> fprintf ppf "%s" x))
    i.imported i.source

let print_import_as ppf (i : import_as) =
  fprintf ppf {|import * as %s
  from %s
|} i.alias i.source

let print_import ppf = function
  | Import_as ia -> print_import_as ppf ia
  | Import_value iv -> print_import_value ppf iv

let print_imports ppf (imps : import list) =
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
    (fun ppf x -> fprintf ppf "%a" print_import x)
    ppf imps

type ts_function = {
  export : bool;
  async : bool;
  name : string;
  body : string;
  args : (string * string) list;
}
[@@deriving encoding]

let print_ts_function ppf tsf =
  fprintf ppf {|
%s%sfunction %s(%a){%s}
|}
    (if tsf.export then
       "export "
     else
       "")
    (if tsf.async then
       "async "
     else
       "")
    tsf.name
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf ",")
       (fun ppf (arg, typ) -> fprintf ppf "%s : %s" arg typ))
    tsf.args tsf.body

let print_ts_functions ppf tsfuns =
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (fun ppf x -> print_ts_function ppf x)
    ppf tsfuns

type typescript_file = {
  imports : import list;
  preamble : string;
  functions : ts_function list;
  body : string;
}
[@@deriving encoding]

let print_ts_file ~ts_file ppf () =
  fprintf ppf "%a%s%a%s" print_imports ts_file.imports ts_file.preamble
    print_ts_functions ts_file.functions ts_file.body

let add_import (i : import) (t : typescript_file) =
  { t with imports = t.imports @ [i] }

(** Add content at end of a typescript function body  *)
let add_to_body ~fun_name content (t : typescript_file) =
  {
    t with
    functions =
      List.map
        (fun f ->
          if f.name = fun_name then
            { f with body = sprintf "%s%s" f.body content }
          else
            f)
        t.functions;
  }
