(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains a `test` function that analyzes Michelson code for
various issues, such as unserializable types in the storage, potential
reentrancy issues, and permission analysis. It imports several modules,
including `Unstack_micheline`, `Nested_arg`, `Inter_procedural`, `Auxilary`,
`Printer`, `Reentrancy`, `Inline_unstack`, `Perm_test`, `Live_analysis`, and
`Linter_warning`. Warning: this short summary was automatically generated and
could be incomplete or misleading. If you feel this is the case, please open an
issue. *)

module LiveInter = Inter_procedural.Make (Live_analysis)
module Permission = Inter_procedural.Make (Perm_test)
module Label = Inter_procedural.Label
module Test = Auxilary.Aux
module MapString = Auxilary.MapString
module SetInt = Auxilary.SetInt
module Printer = Unstack_printer.Make (Factori_representation.Nested_arg)
open Tzfunc.Proto
open Unstack_micheline
open Unstack_iterator
open Tezos_micheline
module ForbiddenAnnot = Set.Make (String)
module ExistingAnnot = Map.Make (String)
open Micheline
open Factori_analyze_types.Analyze_types
open Factori_representation
open Factori_micheline_lib

module Lint = struct
  let make_printable node =
    let node = Micheline.strip_locations node in
    let printable_node = Micheline_printer.printable (fun s -> s) node in
    printable_node

  let forbid_annot_set =
    List.fold_left
      (fun acc (s, _) -> ForbiddenAnnot.add (String.uncapitalize_ascii s) acc)
      ForbiddenAnnot.empty primitives

  let is_forbidden elt = ForbiddenAnnot.mem elt forbid_annot_set

  let dive_in_composition miche prim =
    let rec aux_dive acc = function
      | [] -> acc
      | (prof, node) :: next_prims -> (
        match node with
        | Prim (_, prim, args, annots) when prim = miche -> (
          let annot_opt = Factori_utils.get_name_annots annots in
          match annot_opt with
          | None ->
            let args = List.map (fun arg -> (prof + 1, arg)) args in
            aux_dive acc (List.rev_append args next_prims)
          | Some _ -> aux_dive ((prof, node) :: acc) next_prims)
        | Prim _ -> aux_dive ((prof, node) :: acc) next_prims
        | _ ->
          raise
            (Factori_errors.GenericError
               ("dive_in_composition", "Unexpected node for a type"))) in
    aux_dive [] [(0, prim)]

  let compute_next_node node args =
    match node with
    | Prim (loc, "pair", args, _) ->
      let next = dive_in_composition "pair" (Prim (loc, "pair", args, [])) in
      let simple_pair = List.for_all (fun (prof, _) -> prof = 1) next in
      let next_node = List.map (fun (_, prim) -> (not simple_pair, prim)) next in
      next_node
    | Prim (loc, "or", args, _) ->
      let next = dive_in_composition "or" (Prim (loc, "or", args, [])) in
      let next_node = List.map (fun (_, prim) -> (true, prim)) next in
      next_node
    | _ ->
      let next = List.map (fun prim -> (false, prim)) args in
      next

  let check_if_same_prim location annotation error_list origin prim node =
    let cannonical_node = Micheline.strip_locations node in
    let cannonical_prim = Micheline.strip_locations prim in
    if cannonical_node <> cannonical_prim then
      MultipleTypeForAnnotation
        {
          location;
          origin;
          annotation;
          expected = make_printable prim;
          actual = make_printable node;
        }
      :: error_list
    else
      error_list

  let verify_annotation_on_prim map p =
    let rec verify_prim_l map acc = function
      | [] -> (map, acc)
      | (need_annot, node) :: next_prims -> (
        match node with
        | Prim (location, _prim, args, annots) ->
          let name_annot = Factori_utils.get_name_annots annots in
          let acc =
            match name_annot with
            | None when need_annot ->
              MissingAnnotation { location; on = make_printable node } :: acc
            | Some e when is_forbidden e ->
              ForbiddenAnnotation
                { location; annotation = e; actual = make_printable node }
              :: acc
            | Some e ->
              let opt = ExistingAnnot.find_opt e map in
              Option.fold ~none:acc
                ~some:(fun (origin, prim) ->
                  check_if_same_prim location e acc origin prim node)
                opt
            | _ -> acc in
          let map =
            Option.fold ~none:map
              ~some:(fun annot ->
                ExistingAnnot.update annot
                  (function
                    | None -> Some (location, node)
                    | e -> e)
                  map)
              name_annot in
          let next_node = compute_next_node node args in
          verify_prim_l map acc (List.rev_append next_node next_prims)
        | _ -> verify_prim_l map acc next_prims) in
    verify_prim_l map [] [(false, p)]

  let verify_program _kt1 node_l warnings =
    let store =
      List.find_map
        (function
          | Prim (_, "storage", [node_store], _) -> Some node_store
          | _ -> None)
        node_l in
    let param =
      List.find_map
        (function
          | Prim (_, "parameter", [node_param], _) -> Some node_param
          | _ -> None)
        node_l in
    let map, acc_store =
      verify_annotation_on_prim ExistingAnnot.empty (Option.get store) in
    let _map, acc_param = verify_annotation_on_prim map (Option.get param) in
    match List.rev_append acc_param acc_store with
    | [] -> warnings
    | l ->
      List.fold_left
        (fun warnings w ->
          Factori_analyze_types.Analyze_types.add_warnings warnings w)
        warnings l

  let next_node_in_sc = function
    | "parameter" -> "storage"
    | "storage" -> "code"
    | "code" | "view" -> "view"
    | _ ->
      raise
        (Factori_errors.GenericError
           ( "next_node_in_sc",
             "Wrong primitive name in a smart contract michelson" ))

  let verify_order _kt1 node_l warnings =
    let rec aux_order before next = function
      | [] -> None
      | node :: next_node -> (
        match node with
        | Prim (_, prim, _, _) when prim = next ->
          aux_order (Some prim) (next_node_in_sc next) next_node
        | Prim (_, prim, _, _) -> Some (WrongOrder (before, next, prim))
        | _ ->
          raise
            (Factori_errors.GenericError
               ( "next_node_in_sc",
                 "Wrong primitive name in a smart contract michelson" ))) in
    let res_opt = aux_order None "parameter" node_l in
    match res_opt with
    | None -> warnings
    | Some err -> Factori_analyze_types.Analyze_types.add_warnings warnings err
end

module StorageCheck = struct
  module WrongUse = Map.Make (String)

  let add_wrong ty annot map =
    WrongUse.update ty
      (function
        | None -> Some (StorageCheck { nb = 1; ty; annot })
        | Some (StorageCheck { nb; ty; annot }) ->
          Some (StorageCheck { nb = nb + 1; ty; annot })
        | _ -> assert false)
      map

  let rec storage_check map = function
    | Mprim { prim = `map; args; annots } ->
      let map = add_wrong "map" (Factori_utils.get_name_annots annots) map in
      List.fold_left storage_check map args
    | Mprim { prim = `set; args; annots } ->
      let map = add_wrong "set" (Factori_utils.get_name_annots annots) map in
      List.fold_left storage_check map args
    | Mprim { prim = `list; args; annots } ->
      let map = add_wrong "list" (Factori_utils.get_name_annots annots) map in
      List.fold_left storage_check map args
    | Mprim { args; _ } -> List.fold_left storage_check map args
    | _ -> map

  let storage_check warnings storage =
    let checks = storage_check WrongUse.empty storage in
    WrongUse.fold
      (fun _ w ws ->
        match w with
        | AbsCheck
        | ReentrancyCheck
        | UnusedFieldCheck _
        | PermissionCheck _
        | MultipleTypeForAnnotation _
        | MissingAnnotation _
        | ForbiddenAnnotation _
        | WrongOrder (_, _, _) -> assert false (* should never happen *)
        | StorageCheck sc ->
          add_warnings ~initial_value:sc.nb ws (StorageCheck sc))
      checks warnings
end

module AbsCheck = struct
  let abs_folder =
    let abs_instructions fold () acc node =
      match node with
      | N_SIMPLE (`ABS, _, _, _, _) -> acc + 1
      | _ -> default_folder.node_fold fold () acc node in
    { default_folder with node_fold = abs_instructions }

  let abs_check sc warnings =
    let nb_abs = abs_folder.node_seq abs_folder () 0 sc.node_l in
    if nb_abs <> 0 then
      add_warnings ~initial_value:nb_abs warnings AbsCheck
    else
      warnings
end

module ReentrancyCheck = struct
  open Unstack_iterator
  open Unstack_micheline

  let folder_reentrancy =
    let node_fold folder env acc n =
      match n with
      | N_SIMPLE (`TRANSFER_TOKENS, [], [_; _; _], [_], _) -> acc + 1
      | _ -> default_folder.node_fold folder env acc n in
    { default_folder with node_fold }

  let reentrancy_check sc warnings =
    let nb_transfer =
      folder_reentrancy.node_seq folder_reentrancy () 0 sc.node_l in
    match nb_transfer with
    | 0 -> warnings
    | n -> add_warnings ~initial_value:n warnings ReentrancyCheck
end

module UnusedFieldCheck = struct
  module LiveInter = Inter_procedural.Make (Live_analysis)

  let unused_field_check new_sc warnings =
    let open Inter_procedural in
    let tool = Auxilary.Aux.info_smart new_sc in
    let use_inter = LiveInter.solveMF tool in
    let storage =
      match Nested_arg.gets_pair (Z.of_int 2) new_sc.begin_stack with
      | Error e -> raise e
      | Ok e -> e in
    let storage_fields = Nested_arg.name_string_list [storage] in
    let unused_field =
      Label.fold
        (fun lab _chain_map acc ->
          let fv = ChainMap.find_opt [] (Label.find lab use_inter) in
          match fv with
          | None -> acc
          | Some fv ->
            List.filter
              (fun field ->
                let res = Live_analysis.find field fv in
                Option.is_none res)
              acc)
        use_inter storage_fields in
    match unused_field with
    | [] -> warnings (* all fields are used *)
    | unused_fields ->
      List.fold_left
        (fun ws (s, m) ->
          add_warnings ws
            (UnusedFieldCheck
               { var_name = s; code = Micheline_lib.string_of_micheline m }))
        warnings unused_fields
end

module PermissionCheck = struct
  module Permission = Inter_procedural.Make (Perm_test)
  module MapString = Auxilary.MapString

  let find_special_var list_of_var prefix l =
    List.find_map
      (fun (depth, elt) ->
        match Nested_arg.equal elt l with
        | 0 ->
          let str = List.fold_left (Format.sprintf "%s.%s") prefix depth in
          Some (Nested_arg.string_field str l)
        | _ -> None)
      list_of_var

  let nested_print ~param_l ~storage_l ?(verbose = false) ppf l =
    let param = find_special_var param_l "param" l in
    let store = find_special_var storage_l "storage" l in
    match (param, store) with
    | None, None -> Nested_arg.print ~verbose ppf l
    | Some s, None | None, Some s -> Format.fprintf ppf "%s" s
    | _ -> failwith "Variable appear in parameter and storage"

  let permission_check new_sc warnings =
    let open Inter_procedural in
    let parameter =
      match Nested_arg.gets_pair (Z.of_int 1) new_sc.begin_stack with
      | Error e -> raise e
      | Ok e -> e in
    let tool = Auxilary.Aux.info_smart new_sc in
    let perm = Permission.solveMF tool in
    let entry_map = Perm_test.make_logic_entry_point parameter in
    let all_fail = Auxilary.Aux.all_fail tool in
    let res =
      let open Perm_test in
      List.fold_left
        (fun acc elt ->
          match elt with
          | [prec], arg, _lo ->
            let chain_map = Label.find prec perm in
            ChainMap.fold
              (fun chain_call (env, actual) acc ->
                let actual = Perm_test.formula_simplification actual in
                Result.add (prec, chain_call) (env, actual, arg) acc)
              chain_map acc
          | _ -> failwith "Not possible in theory")
        Result.empty all_fail in
    let test = Perm_test.replace_entry_point entry_map res in
    let permissions =
      MapString.fold
        (fun entrypoint (lab, parameter) permissions ->
          let lab =
            Perm_simp.simplification
              (fst (MapString.find entrypoint entry_map))
              lab in
          let result = List.map snd (Perm_test.Result.bindings lab) in
          { entrypoint; parameter; result } :: permissions)
        test [] in
    add_warnings warnings (PermissionCheck permissions)
end

(* Analysis *)
let check_abs_presence sc warnings = AbsCheck.abs_check sc warnings

let check_storage sc warnings = StorageCheck.storage_check warnings sc.storage

let check_reentrancy_detection sc warnings =
  ReentrancyCheck.reentrancy_check sc warnings

let check_unused_field sc warnings =
  UnusedFieldCheck.unused_field_check sc warnings

let check_permission sc warnings = PermissionCheck.permission_check sc warnings

let get_node_list = function
  | KT1 kt1 ->
    let network = Factori_network.network_of_string !Factori_options.network in
    let se = Factori_utils.get_code_from_KT1 (Some kt1) ~network () in
    Parse_michelson.script_expr_to_node_list se
  | MichelsonFile michelson_file -> (
    try Parse_michelson.parse_michelson_micheline ~file:michelson_file
    with Factori_errors.ParsingError _ ->
      let temp =
        Parse_michelson.script_expr_to_node_list
        @@ Factori_utils.deserialize_file ~silent:false ~filename:michelson_file
             ~enc:Tzfunc.Proto.script_expr_enc.json in
      Factori_utils.output_verbose ~level:0
        (Format.sprintf
           "[lint_michelson] Warning: You are importing from a Json Michelson \
            file, locations are not supported for now") ;
      temp)

let get_script = function
  | KT1 kt1 -> begin
    let network = Factori_network.network_of_string !Factori_options.network in
    let se = Factori_utils.get_code_from_KT1 (Some kt1) ~network () in
    match Micheline_transmo.decompile_smart se with
    | Error e -> raise e
    | Ok (res, _, _) -> res
  end
  | MichelsonFile michelson_file -> (
    let se =
      let res =
        try Parse_michelson.parse_michelson_file ~file:michelson_file
        with Factori_errors.ParsingError _ ->
          Factori_utils.deserialize_file ~silent:false ~filename:michelson_file
            ~enc:Tzfunc.Proto.script_expr_enc.json in
      res in
    match Micheline_transmo.decompile_smart se with
    | Error e -> raise e
    | Ok (res, _, _) -> res)

let run kt1_or_michelson =
  let node_l = get_node_list kt1_or_michelson in
  let sc = get_script kt1_or_michelson in
  let new_sc = Inline_unstack.transform_sc sc in
  let ppf = Format.err_formatter in
  let storage =
    match Nested_arg.gets_pair (Z.of_int 2) new_sc.begin_stack with
    | Error e -> raise e
    | Ok e -> e in
  let storage_pair = Nested_arg.break_pair storage in
  let nested_printer = PermissionCheck.nested_print ~storage_l:storage_pair in
  Warnings.empty
  |> Lint.verify_order kt1_or_michelson node_l
  |> Lint.verify_program kt1_or_michelson node_l
  |> check_storage sc |> check_abs_presence sc
  |> check_reentrancy_detection sc
  |> check_unused_field new_sc
  |> Factori_analyze_print.Analyze_print.report_lint_warning ~nested_printer
       kt1_or_michelson ppf ;
  (* Dirty hack cause checking permission takes much more time than others *)
  Warnings.empty |> check_permission new_sc
  |> Factori_analyze_print.Analyze_print.report_lint_warning ~nested_printer
       kt1_or_michelson ppf
