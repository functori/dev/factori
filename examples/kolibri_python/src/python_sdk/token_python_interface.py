from pytezos import pytezos
#from pytezos import ContractInterface
from pytezos.michelson.types.pair import PairType
from pytezos.michelson.types.sum import OrType
from typing import TypedDict, Optional, List, Tuple, Union
from pytezos.michelson.types.base import MichelsonType
import difflib
import blockchain
import factori_types

UpdateTokenMetadata = Tuple[factori_types.nat,factori_types.Map]

def updateTokenMetadata_generator() -> UpdateTokenMetadata:
	return (factori_types.tuple2_generator(factori_types.nat_generator,
(factori_types.map_generator(factori_types.string_generator,factori_types.bytes_generator))))()

def updateTokenMetadata_encode(arg : UpdateTokenMetadata) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.nat_encode,factori_types.map_encode(factori_types.string_encode,factori_types.bytes_encode))(arg) 
	except Exception as e:
		print('Error in updateTokenMetadata_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateTokenMetadata_decode(arg) -> UpdateTokenMetadata:
	try:
		return factori_types.tuple2_decode(factori_types.nat_decode,factori_types.map_decode(factori_types.string_decode,factori_types.bytes_decode))(arg) 
	except Exception as e:
		print('Error in updateTokenMetadata_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateTokenMetadata():
	temp=updateTokenMetadata_generator()
	temp_encoded=updateTokenMetadata_encode(temp)
	try:
		print('++++ updateTokenMetadata: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateTokenMetadata: " + str(temp))
	temp1=updateTokenMetadata_decode(temp_encoded)
	if temp1 == temp:
		print('updateTokenMetadata check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateTokenMetadata check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateTokenMetadata()
#except Exception as e:
#	print('Error in assert_identity_updateTokenMetadata' + str(e))




def callUpdateTokenMetadata(kt1 : str, _from : blockchain.identity, param : UpdateTokenMetadata, networkName = "ghostnet", debug=False):
	input = updateTokenMetadata_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateTokenMetadata", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

UpdateContractMetadata = Tuple[str,bytes]

def updateContractMetadata_generator() -> UpdateContractMetadata:
	return (factori_types.tuple2_generator(factori_types.string_generator,
factori_types.bytes_generator))()

def updateContractMetadata_encode(arg : UpdateContractMetadata) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.string_encode,factori_types.bytes_encode)(arg) 
	except Exception as e:
		print('Error in updateContractMetadata_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def updateContractMetadata_decode(arg) -> UpdateContractMetadata:
	try:
		return factori_types.tuple2_decode(factori_types.string_decode,factori_types.bytes_decode)(arg) 
	except Exception as e:
		print('Error in updateContractMetadata_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_updateContractMetadata():
	temp=updateContractMetadata_generator()
	temp_encoded=updateContractMetadata_encode(temp)
	try:
		print('++++ updateContractMetadata: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of updateContractMetadata: " + str(temp))
	temp1=updateContractMetadata_decode(temp_encoded)
	if temp1 == temp:
		print('updateContractMetadata check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('updateContractMetadata check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_updateContractMetadata()
#except Exception as e:
#	print('Error in assert_identity_updateContractMetadata' + str(e))




def callUpdateContractMetadata(kt1 : str, _from : blockchain.identity, param : UpdateContractMetadata, networkName = "ghostnet", debug=False):
	input = updateContractMetadata_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "updateContractMetadata", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

Value = factori_types.nat

def value_generator() -> Value:
	return factori_types.nat_generator ()

def value_encode (arg: Value):
	try:
		return factori_types.nat_encode(arg)
	except Exception as e:
		print('Error in value_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def value_decode (arg) -> Value:
	try:
		return factori_types.nat_decode(arg)
	except Exception as e:
		print('Error in value_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_value():
	temp=value_generator()
	temp_encoded=value_encode(temp)
	try:
		print('++++ value: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of value: " + str(temp))
	temp1=value_decode(temp_encoded)
	if temp1 == temp:
		print('value check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('value check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_value()
#except Exception as e:
#	print('Error in assert_identity_value' + str(e))




Constructor_to = factori_types.address

def _to_generator() -> Constructor_to:
	return factori_types.address_generator ()

def _to_encode (arg: Constructor_to):
	try:
		return factori_types.address_encode(arg)
	except Exception as e:
		print('Error in _to_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _to_decode (arg) -> Constructor_to:
	try:
		return factori_types.address_decode(arg)
	except Exception as e:
		print('Error in _to_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__to():
	temp=_to_generator()
	temp_encoded=_to_encode(temp)
	try:
		print('++++ _to: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _to: " + str(temp))
	temp1=_to_decode(temp_encoded)
	if temp1 == temp:
		print('_to check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_to check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__to()
#except Exception as e:
#	print('Error in assert_identity__to' + str(e))




class Transfer(TypedDict):
	value: Value
	_to: Constructor_to
	_from: Constructor_to

def transfer_generator() -> Transfer:
	return {'value' : value_generator (),'_to' : _to_generator (),'_from' : _to_generator ()}

def transfer_encode(arg : Transfer):
	try:
		return PairType.from_comb([(_to_encode(arg['_from'])), PairType.from_comb([(_to_encode(arg['_to'])), (value_encode(arg['value']))])])
	except Exception as e:
		print('Error in transfer_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def transfer_decode(arg) -> Transfer:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,(factori_types.tuple2_decode (_to_decode,value_decode))))(arg)
		return {'_from' : (before_projection[0]),
'_to' : (before_projection[1][0]),
'value' : (before_projection[1][1])}


	except Exception as e:
		print('Error in transfer_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_transfer():
	temp=transfer_generator()
	temp_encoded=transfer_encode(temp)
	try:
		print('++++ transfer: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of transfer: " + str(temp))
	temp1=transfer_decode(temp_encoded)
	if temp1 == temp:
		print('transfer check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('transfer check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_transfer()
#except Exception as e:
#	print('Error in assert_identity_transfer' + str(e))




def callTransfer(kt1 : str, _from : blockchain.identity, param : Transfer, networkName = "ghostnet", debug=False):
	input = transfer_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "transfer", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetPause = bool

def setPause_generator() -> SetPause:
	return factori_types.bool_generator ()

def setPause_encode (arg: SetPause):
	try:
		return factori_types.bool_encode(arg)
	except Exception as e:
		print('Error in setPause_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def setPause_decode (arg) -> SetPause:
	try:
		return factori_types.bool_decode(arg)
	except Exception as e:
		print('Error in setPause_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setPause():
	temp=setPause_generator()
	temp_encoded=setPause_encode(temp)
	try:
		print('++++ setPause: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setPause: " + str(temp))
	temp1=setPause_decode(temp_encoded)
	if temp1 == temp:
		print('setPause check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setPause check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setPause()
#except Exception as e:
#	print('Error in assert_identity_setPause' + str(e))




def callSetPause(kt1 : str, _from : blockchain.identity, param : SetPause, networkName = "ghostnet", debug=False):
	input = setPause_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setPause", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetGovernorContract = Constructor_to


def setGovernorContract_generator() -> SetGovernorContract:
	return _to_generator ()


def setGovernorContract_encode (arg: SetGovernorContract):
	try:
		return _to_encode(arg)
	except Exception as e:
		print('Error in setGovernorContract_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setGovernorContract_decode (arg) -> SetGovernorContract:
	try:
		return _to_decode(arg)
	except Exception as e:
		print('Error in setGovernorContract_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setGovernorContract():
	temp=setGovernorContract_generator()
	temp_encoded=setGovernorContract_encode(temp)
	try:
		print('++++ setGovernorContract: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setGovernorContract: " + str(temp))
	temp1=setGovernorContract_decode(temp_encoded)
	if temp1 == temp:
		print('setGovernorContract check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setGovernorContract check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setGovernorContract()
#except Exception as e:
#	print('Error in assert_identity_setGovernorContract' + str(e))




def callSetGovernorContract(kt1 : str, _from : blockchain.identity, param : SetGovernorContract, networkName = "ghostnet", debug=False):
	input = setGovernorContract_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setGovernorContract", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetDebtCeiling = Value


def setDebtCeiling_generator() -> SetDebtCeiling:
	return value_generator ()


def setDebtCeiling_encode (arg: SetDebtCeiling):
	try:
		return value_encode(arg)
	except Exception as e:
		print('Error in setDebtCeiling_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setDebtCeiling_decode (arg) -> SetDebtCeiling:
	try:
		return value_decode(arg)
	except Exception as e:
		print('Error in setDebtCeiling_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setDebtCeiling():
	temp=setDebtCeiling_generator()
	temp_encoded=setDebtCeiling_encode(temp)
	try:
		print('++++ setDebtCeiling: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setDebtCeiling: " + str(temp))
	temp1=setDebtCeiling_decode(temp_encoded)
	if temp1 == temp:
		print('setDebtCeiling check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setDebtCeiling check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setDebtCeiling()
#except Exception as e:
#	print('Error in assert_identity_setDebtCeiling' + str(e))




def callSetDebtCeiling(kt1 : str, _from : blockchain.identity, param : SetDebtCeiling, networkName = "ghostnet", debug=False):
	input = setDebtCeiling_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setDebtCeiling", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

SetAdministrator = Constructor_to


def setAdministrator_generator() -> SetAdministrator:
	return _to_generator ()


def setAdministrator_encode (arg: SetAdministrator):
	try:
		return _to_encode(arg)
	except Exception as e:
		print('Error in setAdministrator_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def setAdministrator_decode (arg) -> SetAdministrator:
	try:
		return _to_decode(arg)
	except Exception as e:
		print('Error in setAdministrator_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_setAdministrator():
	temp=setAdministrator_generator()
	temp_encoded=setAdministrator_encode(temp)
	try:
		print('++++ setAdministrator: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of setAdministrator: " + str(temp))
	temp1=setAdministrator_decode(temp_encoded)
	if temp1 == temp:
		print('setAdministrator check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('setAdministrator check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_setAdministrator()
#except Exception as e:
#	print('Error in assert_identity_setAdministrator' + str(e))




def callSetAdministrator(kt1 : str, _from : blockchain.identity, param : SetAdministrator, networkName = "ghostnet", debug=False):
	input = setAdministrator_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "setAdministrator", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Mint(TypedDict):
	address: Constructor_to
	value: Value

def mint_generator() -> Mint:
	return {'address' : _to_generator (),'value' : value_generator ()}

def mint_encode(arg : Mint):
	try:
		return PairType.from_comb([(_to_encode(arg['address'])), (value_encode(arg['value']))])
	except Exception as e:
		print('Error in mint_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def mint_decode(arg) -> Mint:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,value_decode))(arg)
		return {'address' : (before_projection[0]),
'value' : (before_projection[1])}


	except Exception as e:
		print('Error in mint_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_mint():
	temp=mint_generator()
	temp_encoded=mint_encode(temp)
	try:
		print('++++ mint: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of mint: " + str(temp))
	temp1=mint_decode(temp_encoded)
	if temp1 == temp:
		print('mint check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('mint check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_mint()
#except Exception as e:
#	print('Error in assert_identity_mint' + str(e))




def callMint(kt1 : str, _from : blockchain.identity, param : Mint, networkName = "ghostnet", debug=False):
	input = mint_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "mint", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetTotalSupply = Tuple[factori_types.unit,factori_types.contract]

def getTotalSupply_generator() -> GetTotalSupply:
	return (factori_types.tuple2_generator(factori_types.unit_generator,
factori_types.contract_generator))()

def getTotalSupply_encode(arg : GetTotalSupply) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.unit_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getTotalSupply_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getTotalSupply_decode(arg) -> GetTotalSupply:
	try:
		return factori_types.tuple2_decode(factori_types.unit_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getTotalSupply_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getTotalSupply():
	temp=getTotalSupply_generator()
	temp_encoded=getTotalSupply_encode(temp)
	try:
		print('++++ getTotalSupply: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getTotalSupply: " + str(temp))
	temp1=getTotalSupply_decode(temp_encoded)
	if temp1 == temp:
		print('getTotalSupply check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getTotalSupply check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getTotalSupply()
#except Exception as e:
#	print('Error in assert_identity_getTotalSupply' + str(e))




def callGetTotalSupply(kt1 : str, _from : blockchain.identity, param : GetTotalSupply, networkName = "ghostnet", debug=False):
	input = getTotalSupply_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getTotalSupply", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetBalance = Tuple[Constructor_to,factori_types.contract]

def getBalance_generator() -> GetBalance:
	return (factori_types.tuple2_generator(_to_generator,
factori_types.contract_generator))()

def getBalance_encode(arg : GetBalance) -> PairType:
	try:
		return factori_types.tuple2_encode(_to_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getBalance_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getBalance_decode(arg) -> GetBalance:
	try:
		return factori_types.tuple2_decode(_to_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getBalance_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getBalance():
	temp=getBalance_generator()
	temp_encoded=getBalance_encode(temp)
	try:
		print('++++ getBalance: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getBalance: " + str(temp))
	temp1=getBalance_decode(temp_encoded)
	if temp1 == temp:
		print('getBalance check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getBalance check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getBalance()
#except Exception as e:
#	print('Error in assert_identity_getBalance' + str(e))




def callGetBalance(kt1 : str, _from : blockchain.identity, param : GetBalance, networkName = "ghostnet", debug=False):
	input = getBalance_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getBalance", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Type3(TypedDict):
	owner: Constructor_to
	spender: Constructor_to

def type3_generator() -> Type3:
	return {'owner' : _to_generator (),'spender' : _to_generator ()}

def type3_encode(arg : Type3):
	try:
		return PairType.from_comb([(_to_encode(arg['owner'])), (_to_encode(arg['spender']))])
	except Exception as e:
		print('Error in type3_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def type3_decode(arg) -> Type3:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,_to_decode))(arg)
		return {'owner' : (before_projection[0]),
'spender' : (before_projection[1])}


	except Exception as e:
		print('Error in type3_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_type3():
	temp=type3_generator()
	temp_encoded=type3_encode(temp)
	try:
		print('++++ type3: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of type3: " + str(temp))
	temp1=type3_decode(temp_encoded)
	if temp1 == temp:
		print('type3 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('type3 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_type3()
#except Exception as e:
#	print('Error in assert_identity_type3' + str(e))




GetAllowance = Tuple[Type3,factori_types.contract]

def getAllowance_generator() -> GetAllowance:
	return (factori_types.tuple2_generator(type3_generator,
factori_types.contract_generator))()

def getAllowance_encode(arg : GetAllowance) -> PairType:
	try:
		return factori_types.tuple2_encode(type3_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getAllowance_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getAllowance_decode(arg) -> GetAllowance:
	try:
		return factori_types.tuple2_decode(type3_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getAllowance_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getAllowance():
	temp=getAllowance_generator()
	temp_encoded=getAllowance_encode(temp)
	try:
		print('++++ getAllowance: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getAllowance: " + str(temp))
	temp1=getAllowance_decode(temp_encoded)
	if temp1 == temp:
		print('getAllowance check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getAllowance check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getAllowance()
#except Exception as e:
#	print('Error in assert_identity_getAllowance' + str(e))




def callGetAllowance(kt1 : str, _from : blockchain.identity, param : GetAllowance, networkName = "ghostnet", debug=False):
	input = getAllowance_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getAllowance", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

GetAdministrator = Tuple[factori_types.unit,factori_types.contract]

def getAdministrator_generator() -> GetAdministrator:
	return (factori_types.tuple2_generator(factori_types.unit_generator,
factori_types.contract_generator))()

def getAdministrator_encode(arg : GetAdministrator) -> PairType:
	try:
		return factori_types.tuple2_encode(factori_types.unit_encode,factori_types.contract_encode)(arg) 
	except Exception as e:
		print('Error in getAdministrator_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def getAdministrator_decode(arg) -> GetAdministrator:
	try:
		return factori_types.tuple2_decode(factori_types.unit_decode,factori_types.contract_decode)(arg) 
	except Exception as e:
		print('Error in getAdministrator_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_getAdministrator():
	temp=getAdministrator_generator()
	temp_encoded=getAdministrator_encode(temp)
	try:
		print('++++ getAdministrator: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of getAdministrator: " + str(temp))
	temp1=getAdministrator_decode(temp_encoded)
	if temp1 == temp:
		print('getAdministrator check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('getAdministrator check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_getAdministrator()
#except Exception as e:
#	print('Error in assert_identity_getAdministrator' + str(e))




def callGetAdministrator(kt1 : str, _from : blockchain.identity, param : GetAdministrator, networkName = "ghostnet", debug=False):
	input = getAdministrator_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "getAdministrator", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Burn(Mint):
	pass


def burn_generator() -> Burn:
	return mint_generator ()


def burn_encode (arg: Burn):
	try:
		return mint_encode(arg)
	except Exception as e:
		print('Error in burn_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e


def burn_decode (arg) -> Burn:
	try:
		return mint_decode(arg)
	except Exception as e:
		print('Error in burn_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_burn():
	temp=burn_generator()
	temp_encoded=burn_encode(temp)
	try:
		print('++++ burn: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of burn: " + str(temp))
	temp1=burn_decode(temp_encoded)
	if temp1 == temp:
		print('burn check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('burn check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_burn()
#except Exception as e:
#	print('Error in assert_identity_burn' + str(e))




def callBurn(kt1 : str, _from : blockchain.identity, param : Burn, networkName = "ghostnet", debug=False):
	input = burn_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "burn", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Approve(TypedDict):
	spender: Constructor_to
	value: Value

def approve_generator() -> Approve:
	return {'spender' : _to_generator (),'value' : value_generator ()}

def approve_encode(arg : Approve):
	try:
		return PairType.from_comb([(_to_encode(arg['spender'])), (value_encode(arg['value']))])
	except Exception as e:
		print('Error in approve_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def approve_decode(arg) -> Approve:
	try:
		before_projection=(factori_types.tuple2_decode (_to_decode,value_decode))(arg)
		return {'spender' : (before_projection[0]),
'value' : (before_projection[1])}


	except Exception as e:
		print('Error in approve_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_approve():
	temp=approve_generator()
	temp_encoded=approve_encode(temp)
	try:
		print('++++ approve: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of approve: " + str(temp))
	temp1=approve_decode(temp_encoded)
	if temp1 == temp:
		print('approve check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('approve check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_approve()
#except Exception as e:
#	print('Error in assert_identity_approve' + str(e))




def callApprove(kt1 : str, _from : blockchain.identity, param : Approve, networkName = "ghostnet", debug=False):
	input = approve_encode(param)
	ophash = blockchain.callEntrypoint(_from, kt1, "approve", input.to_micheline_value(), 0,network=networkName,debug=debug)
	return ophash

class Type5(TypedDict):
	approvals: factori_types.Map
	balance: Value

def type5_generator() -> Type5:
	return {'approvals' : (factori_types.map_generator(_to_generator,value_generator)) (),'balance' : value_generator ()}

def type5_encode(arg : Type5):
	try:
		return PairType.from_comb([(factori_types.map_encode(_to_encode,value_encode)(arg['approvals'])), (value_encode(arg['balance']))])
	except Exception as e:
		print('Error in type5_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def type5_decode(arg) -> Type5:
	try:
		before_projection=(factori_types.tuple2_decode (factori_types.map_decode(_to_decode,value_decode),value_decode))(arg)
		return {'approvals' : (before_projection[0]),
'balance' : (before_projection[1])}


	except Exception as e:
		print('Error in type5_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity_type5():
	temp=type5_generator()
	temp_encoded=type5_encode(temp)
	try:
		print('++++ type5: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of type5: " + str(temp))
	temp1=type5_decode(temp_encoded)
	if temp1 == temp:
		print('type5 check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('type5 check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity_type5()
#except Exception as e:
#	print('Error in assert_identity_type5' + str(e))




class Constructor_storage(TypedDict):
	paused: SetPause
	metadata: factori_types.BigMap
	governorContractAddress: Constructor_to
	debtCeiling: Value
	administrator: Constructor_to
	balances: factori_types.BigMap
	token_metadata: factori_types.BigMap
	totalSupply: Value

def _storage_generator() -> Constructor_storage:
	return {'paused' : setPause_generator (),'metadata' : (factori_types.big_map_generator(factori_types.string_generator,factori_types.bytes_generator)) (),'governorContractAddress' : _to_generator (),'debtCeiling' : value_generator (),'administrator' : _to_generator (),'balances' : (factori_types.big_map_generator(_to_generator,type5_generator)) (),'token_metadata' : (factori_types.big_map_generator(value_generator,(factori_types.tuple2_generator(value_generator,
(factori_types.map_generator(factori_types.string_generator,factori_types.bytes_generator)))))) (),'totalSupply' : value_generator ()}

def _storage_encode(arg : Constructor_storage):
	try:
		return PairType.from_comb([PairType.from_comb([PairType.from_comb([(_to_encode(arg['administrator'])), (factori_types.big_map_encode(_to_encode,type5_encode)(arg['balances']))]), PairType.from_comb([(value_encode(arg['debtCeiling'])), (_to_encode(arg['governorContractAddress']))])]), PairType.from_comb([PairType.from_comb([(factori_types.big_map_encode(factori_types.string_encode,factori_types.bytes_encode)(arg['metadata'])), (setPause_encode(arg['paused']))]), PairType.from_comb([(factori_types.big_map_encode(value_encode,factori_types.tuple2_encode(value_encode,factori_types.map_encode(factori_types.string_encode,factori_types.bytes_encode)))(arg['token_metadata'])), (value_encode(arg['totalSupply']))])])])
	except Exception as e:
		print('Error in _storage_encode, with input:' + str(arg) + 'Error:' + str(e))
		raise e

def _storage_decode(arg) -> Constructor_storage:
	try:
		before_projection=(factori_types.tuple2_decode ((factori_types.tuple2_decode ((factori_types.tuple2_decode (_to_decode,factori_types.big_map_decode(_to_decode,type5_decode))),(factori_types.tuple2_decode (value_decode,_to_decode)))),(factori_types.tuple2_decode ((factori_types.tuple2_decode (factori_types.big_map_decode(factori_types.string_decode,factori_types.bytes_decode),setPause_decode)),(factori_types.tuple2_decode (factori_types.big_map_decode(value_decode,factori_types.tuple2_decode(value_decode,factori_types.map_decode(factori_types.string_decode,factori_types.bytes_decode))),value_decode))))))(arg)
		return {'administrator' : (before_projection[0][0][0]),
'balances' : (before_projection[0][0][1]),
'debtCeiling' : (before_projection[0][1][0]),
'governorContractAddress' : (before_projection[0][1][1]),
'metadata' : (before_projection[1][0][0]),
'paused' : (before_projection[1][0][1]),
'token_metadata' : (before_projection[1][1][0]),
'totalSupply' : (before_projection[1][1][1])}


	except Exception as e:
		print('Error in _storage_decode, with input: ' + str(arg.to_micheline_value()) + ', and error is:' + str(e))
		raise e

def assert_identity__storage():
	temp=_storage_generator()
	temp_encoded=_storage_encode(temp)
	try:
		print('++++ _storage: ' + str(temp_encoded.to_micheline_value()))
	except Exception as e:
		print("---- couldn't print encoded version of _storage: " + str(temp))
	temp1=_storage_decode(temp_encoded)
	if temp1 == temp:
		print('_storage check passed')
	else:
		if isinstance(temp, dict) and isinstance(temp1, dict):
			temp = dict(sorted(temp.items()))
			temp1 = dict(sorted(temp1.items()))
			diff_string = '\n'.join(difflib.Differ().compare(str(temp1).splitlines(), str(temp).splitlines()))
			print('_storage check failed: temp = '+str(temp)+' is not equal to '+str(temp1)+'\ndiff:\n'+diff_string)

#Use this for debugging:
#try:
#	assert_identity__storage()
#except Exception as e:
#	print('Error in assert_identity__storage' + str(e))




def deploy(_from : blockchain.identity, initial_storage : Constructor_storage, amount, network, debug = False):
	if(debug):
		print("Deploying contract token")
	storage_encoded = _storage_encode(initial_storage)
	if(debug):
		print(initial_storage)
	kt1 : str = blockchain.deployContract("src/python_sdk/token_code.json",_from, storage_encoded.to_micheline_value(), amount, network, debug)
	return kt1

def test_storage_download(kt1):
    c = pytezos.using(shell='mainnet').contract(kt1)
    return c#No KT1