open Tezos_micheline
open Factori_analyze_types.Analyze_types
open Factori_permission_lib
open Factori_micheline_lib
open Factori_representation
open Factori_utils
module Constant = Nested_arg.Constant

let print_formula print_prop ppf f =
  Format.fprintf ppf "%a"
    (Permission_tool.print_logic (Permission_tool.print_permission print_prop))
    f

let pp_permission_logic ~verbose
    ~(local_print_prop : ?verbose:bool -> Format.formatter -> prop -> unit) ppf
    f =
  let open Format in
  let open Permission_ast in
  let open Permission_tool in
  let new_print_prop ppf elt = local_print_prop ~verbose ppf elt in
  let rec aux_pp_permission_logic ppf f =
    match f with
    | Logic_not (P (Is_true (Propagate (p, l1, l2))))
      when Micheline_lib.comparison p ->
      let elt =
        P
          (Is_true
             (Propagate (Option.get @@ Micheline_lib.inverse_prim p, l1, l2)))
      in
      aux_pp_permission_logic ppf elt
    | Logic_not (P (Is_none elt)) ->
      Format.fprintf ppf "(%a is Some)" new_print_prop elt
    | Logic_not (P (Is_left elt)) ->
      Format.fprintf ppf "(%a is Right)" new_print_prop elt
    | Logic_not (P (Is_cons elt)) ->
      Format.fprintf ppf "(%a is nil)" new_print_prop elt
    | Logic_and l ->
      Format.fprintf ppf "(%a)"
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf " /\\ ")
           aux_pp_permission_logic)
        l
    | Logic_or l ->
      Format.fprintf ppf "(%a)"
        (pp_print_list
           ~pp_sep:(fun ppf _ -> fprintf ppf " \\/ ")
           aux_pp_permission_logic)
        l
    | f -> print_formula new_print_prop ppf f in
  aux_pp_permission_logic ppf f

let pp_result ?(verbose = false) ~local_print_prop ppf t =
  let print_formule_map ppf l =
    Format.fprintf ppf "@[<v 0>%a@]"
      (Format.pp_print_list
         ~pp_sep:(fun ppf () -> Format.fprintf ppf "@, else")
         (fun ppf (formula, elt) ->
           let print_fun =
             if Permission_tool.is_true formula then
               Format.fprintf ppf " %a"
             else
               Format.fprintf ppf " if %a then %a"
                 (pp_permission_logic ~verbose ~local_print_prop)
                 formula in
           print_fun local_print_prop elt))
      l in
  let print_element ppf set =
    let elements = Constant.bindings set in
    Format.pp_print_list
      ~pp_sep:(fun ppf () -> Format.fprintf ppf "@,@,")
      (fun ppf (_, (var, formule_map)) ->
        Format.fprintf ppf "%s :=%a" var print_formule_map formule_map)
      ppf elements in
  let env_print ppf env =
    if not (Constant.is_empty env) then
      Format.fprintf ppf "@.@.   Where @[<v 0>%a@]" print_element env
    else
      () in
  Format.fprintf ppf "%a"
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt "@. Else@.")
       (fun ppf ((a, prop), set) ->
         Format.fprintf ppf "  Fail with %a if %a%a"
           (local_print_prop ~verbose)
           prop
           (pp_permission_logic ~verbose ~local_print_prop)
           a env_print set))
    t

let pp_info ppf number =
  Format.fprintf ppf "%s%s%sInfo %d%s" Factori_utils.enable_underline
    Factori_utils.enable_bold Factori_utils.enable_yellow number
    Factori_utils.reset_format

let pp_warning ppf number =
  Format.fprintf ppf "%s%s%sWarning %d%s" Factori_utils.enable_underline
    Factori_utils.enable_bold Factori_utils.enable_yellow number
    Factori_utils.reset_format

let pp_description ppf number =
  Format.fprintf ppf "@.@[<2>%sTips%s: %s@]" Factori_utils.enable_underline
    Factori_utils.reset_format (description number)

let pp_permission ?(verbose = false) ?nested_printer ppf permission =
  let param_l = Nested_arg.break_pair permission.parameter in
  let local_print_prop =
    match nested_printer with
    | None -> Permission_ast.print_prop ?nested_printer:None
    | Some printer ->
      Permission_ast.print_prop ~nested_printer:(printer ~param_l) in
  let print_result = pp_result ~verbose ~local_print_prop in
  Format.fprintf ppf
    "@.@[<2>Permission for entrypoint: %s%s%s%s@. @[<2>Parameter:@.%a@]@. \
     Result:@.%a@]@."
    Factori_utils.enable_green Factori_utils.enable_italic permission.entrypoint
    Factori_utils.reset_format
    (Micheline_lib.print_micheline ~simple:true)
    (Nested_arg.deconstruct permission.parameter)
    print_result permission.result

let report_lint_warning_solo ?nested_printer ppf (lint_warning, occurence) =
  let number = number lint_warning in
  match lint_warning with
  | MultipleTypeForAnnotation { location; origin; annotation; expected; actual }
    ->
    Format.fprintf ppf
      "%a@.@[<2>%a: %a, annotation %S was defined like a %a, but %a the same \
       annotation is of type %a@] %a@."
      Micheline_parser.print_location origin pp_warning number
      Micheline_parser.print_location origin annotation
      Micheline_printer.print_expr expected Micheline_parser.print_location
      location Micheline_printer.print_expr actual pp_description number
  | MissingAnnotation { location; on } ->
    Format.fprintf ppf
      "%a@.@[<1>%a: there is no annotation on type %a (%d different \
       location(s))@] %a@."
      Micheline_parser.print_location location pp_warning number
      Micheline_printer.print_expr on occurence pp_description number
  | ForbiddenAnnotation { location; annotation; actual = _ } ->
    Format.fprintf ppf "%a@.@[<2>%a: %S is a forbidden annotation@] %a@."
      Micheline_parser.print_location location pp_warning number annotation
      pp_description number
  | WrongOrder (None, expected, actual) ->
    Format.fprintf ppf
      "@.@[<2>%a: the first section of a contract was expected to be the %s \
       but got the %s section@] %a@."
      pp_warning number expected actual pp_description number
  | WrongOrder (Some before, expected, actual) ->
    Format.fprintf ppf
      "@.@[<2>%a: after the %s section, the %s was expected but got the %s \
       section@] %a@."
      pp_warning number before expected actual pp_description number
  | AbsCheck ->
    Format.fprintf ppf "@.@[<2>%a: Abs is present %d time(s)@] %a@." pp_warning
      number occurence pp_description number
  | StorageCheck { ty; annot; nb } ->
    let annot =
      match annot with
      | None -> ""
      | Some a -> a in
    Format.fprintf ppf "@.@[<2>%a: StorageCheck: %s %S used %d time(s)@] %a@."
      pp_warning number ty annot nb pp_description number
  | ReentrancyCheck ->
    Format.fprintf ppf
      "@.@[<2>%a: There are %d \"CALL_CONTRACT\"\" in this contract, check for \
       reentrancy@] %a@."
      pp_warning number occurence pp_description number
  | UnusedFieldCheck { var_name; code } ->
    Format.fprintf ppf "@.@[<2>%a: Unused field: %S (%S)@] %a@." pp_warning
      number var_name code pp_description number
  | PermissionCheck permissions ->
    Format.fprintf ppf "@.@[<2>%a: Permission analysis:@.@]%a@.%a" pp_info
      number
      (Format.pp_print_list ~pp_sep:Format.pp_print_newline
         (pp_permission ?nested_printer))
      permissions pp_description number

let simple_print_result ppf ((princip_f, prop), cst) =
  let show_perm =
    Permission_tool.show_permission (string_to_print Permission_ast.show_prop)
  in
  let show_logic = Permission_tool.(show_logic (string_to_print show_perm)) in
  let pp_sep ppf () = Format.fprintf ppf ";" in
  let print_all_possible ppf all_possible =
    Format.pp_print_list ~pp_sep
      (fun ppf (logic, prop) ->
        Format.fprintf ppf "(%s,%s)" (show_logic logic)
          (Permission_ast.show_prop prop))
      ppf all_possible in
  let cst_print ppf cst =
    let cst = Constant.bindings cst in
    Format.fprintf ppf "Constant [%a]"
      (Format.pp_print_list ~pp_sep (fun ppf (nested, (name, all_possible)) ->
           Format.fprintf ppf "key = %s, value = (%s,%a)"
             (Nested_arg.to_string nested)
             name print_all_possible all_possible))
      cst in
  Format.fprintf ppf "Fail_if = %s, with = %s, etc = %a" (show_logic princip_f)
    (Permission_ast.show_prop prop)
    cst_print cst

let simple_warning_print ppf (lint_warning, occurence) =
  let print_optional ppf =
    Option.fold ~none:() ~some:(Format.fprintf ppf "%S,") in
  match lint_warning with
  | MultipleTypeForAnnotation { location; origin; annotation; expected; actual }
    ->
    Format.fprintf ppf "MultipleTypeForAnnotation(%a,%a,%S,%a,%a) %d"
      Micheline_parser.print_location location Micheline_parser.print_location
      origin annotation Micheline_printer.print_expr expected
      Micheline_printer.print_expr actual occurence
  | MissingAnnotation { location; on } ->
    Format.fprintf ppf "MissingAnnotation(%a,%a)"
      Micheline_parser.print_location location Micheline_printer.print_expr on
  | ForbiddenAnnotation { location; annotation; actual } ->
    Format.fprintf ppf "ForbiddenAnnotation(%a,%S,%a)"
      Micheline_parser.print_location location annotation
      Micheline_printer.print_expr actual
  | WrongOrder (before, expected, actual) ->
    Format.fprintf ppf "WrongOrder(%a%S,%S)" print_optional before expected
      actual
  | AbsCheck -> Format.fprintf ppf "AbsCheck(%d)" occurence
  | StorageCheck { ty; annot; nb } ->
    Format.fprintf ppf "StorageCheck(%s,%a%d)" ty print_optional annot nb
  | ReentrancyCheck -> Format.fprintf ppf "ReentrancyCheck(%d)" occurence
  | UnusedFieldCheck { var_name; code } ->
    Format.fprintf ppf "UnusedFieldCheck(%s,%s)" var_name code
  | PermissionCheck permissions ->
    Format.fprintf ppf "Permission analysis(%a)"
      (Format.pp_print_list ~pp_sep:Format.pp_print_newline (fun ppf p ->
           Format.fprintf ppf "(%s,%s,%a)" p.entrypoint
             (Nested_arg.to_string p.parameter)
             (Format.pp_print_list simple_print_result)
             p.result))
      permissions

let report_lint_warning ?nested_printer kt1_or_michelson ppf warnings =
  Warnings.iter
    (fun w occ ->
      let ty, name =
        match kt1_or_michelson with
        | MichelsonFile f -> ("File", f)
        | KT1 k -> ("Contract", k) in
      Format.fprintf ppf "%s%s %S, %a%s@." Factori_utils.enable_bold ty name
        (report_lint_warning_solo ?nested_printer)
        (w, occ) Factori_utils.reset_format)
    warnings
