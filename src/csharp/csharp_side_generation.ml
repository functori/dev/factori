(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions that generate C# code for testing the
Tezos client against various input types, including base, unary, and binary
types. It depends on the `Types` module and includes functions for generating
random and constant generators for different types. Warning: this short summary
was automatically generated and could be incomplete or misleading. If you feel
this is the case, please open an issue. *)

open Format
open Factori_utils
open Types

let encode_lib =
  {|
               public static IMicheline ListEncode<T>(List<T> l) where T : IFactoriT {
            var res = new MichelineArray();
            for (int i = 0; i < l.Count; i++){
                res.Add(l[i].Encode());
            }
            return res;
        }   
                  
                  public static IMicheline DictEncode<TKey,TValue>(Dictionary<TKey, TValue> x)  where TKey: IFactoriT where TValue: IFactoriT
            {
                        var res = new MichelineArray(x.Keys.Count());

                        foreach (KeyValuePair<TKey, TValue> kvPair in x)
                        {
                            var elem = new MichelinePrim
                            {
                                Prim = PrimType.Elt,
                                Args = new List<IMicheline>{
                                kvPair.Key.Encode(),
                                kvPair.Value.Encode()
                                }
                            };
                            res.Append(elem);

                        };
                        return res;

                  } |}

let decode_lib =
  {|

   public static List<T> ListDecode<T>(IMicheline im) where T : IFactoriT{
                    switch(im){
                        case MichelineArray ma:
                            List<T> res = new List<T>();
                            foreach (var item in ma)
                            {
                               res.Append<T>((T)T.GenericDecode(item));
                            }
                            return res;
                        default:
                            throw new DecodeError($"Error decoding {FactoriTypes.Utils.GetIMichelineString(im)} as a list");
                    }
        }

  public static KeyValuePair<TKey, TValue> DecodeMapElt<TKey, TValue>(IMicheline im) where TKey : notnull, IFactoriT where TValue : notnull, IFactoriT
        {
                switch (im)
                {
                    case MichelinePrim mp:
                        switch (mp.Prim)
                        {
                            case PrimType.Elt:
                                if(mp.Args == null){
                                    throw new DecodeError($"[DecodeMapElt] Error trying to decode a map element from {FactoriTypes.Utils.GetIMichelineString(im)}, {mp} has no Args field");
                                }
                                if (mp.Args.Count < 2)
                                {
                                    throw new DecodeError($"There should be two elements in Elt component {mp} of {FactoriTypes.Utils.GetIMichelineString(im)}");
                                }
                                else
                                {
                                    return new KeyValuePair<TKey, TValue>((TKey) TKey.GenericDecode(mp.Args[0]), (TValue) TValue.GenericDecode(mp.Args[1]));
                                }
                            default:
                                throw new DecodeError($"Error trying to decode a map element from {FactoriTypes.Utils.GetIMichelineString(im)}, {mp} is not an Elt");
                        }
                    default:
                        throw new DecodeError($"Error trying to decode a map element from {FactoriTypes.Utils.GetIMichelineString(im)}");

                }
        }

    public static Dictionary<TKey,TValue> DictDecode<TKey,TValue>(IMicheline im) where TKey: IFactoriT where TValue: IFactoriT{
            var res = new Dictionary<TKey,TValue>();
                switch(im){
                    case MichelineArray ma:
                        foreach (var item in ma)
                        {
                            res.Append(DecodeMapElt<TKey,TValue>(item));
                        }
                        return res;
                    default:
                        throw new DecodeError($"Error trying to decode a map from {Utils.GetIMichelineString(im)}");
                }
        }
|}

let decode_option =
  {|
   

public Option(IMicheline im){
   switch (im)
                {
                    case MichelinePrim mp:
                     switch (mp.Prim)
                     {
                        case PrimType.Some:
                        if(mp.Args == null){
                            throw new DecodeError($"Some should have an Args != null: {FactoriTypes.Utils.GetIMichelineString(mp)}");
                        }
                            _contentOption = new Some<T>((T) T.GenericDecode(mp.Args[0]));
                            break;
                        case PrimType.None:
                            _contentOption = new None<T>();
                            break;
                        default:
                            throw new DecodeError($"Micheline is not a Some prim: {FactoriTypes.Utils.GetIMichelineString(mp)}");
                     }
                     break;
                        default:
                            throw new DecodeError($"Micheline is not prim: {FactoriTypes.Utils.GetIMichelineString(im)}");
                }
}
|}

let decode_ticket =
  {|public Ticket(IMicheline im)
        {
            switch(im){
                case MichelinePrim m:
                    switch(m.Prim){
                        case PrimType.Pair:
                            if(m.Args == null || m.Args.Count < 3){
                                throw new DecodeError($"Cannot decode to a ticket {FactoriTypes.Utils.GetIMichelineString(m)}");
                            }
                            _addr = new Address(m.Args[0]);
                            _value = (T) T.GenericDecode(m.Args[1]);
                            _amount = new Nat(m.Args[2]);
                            break;
                        default:
                            throw new DecodeError($"Cannot decode to a ticket {FactoriTypes.Utils.GetIMichelineString(m)}");
                    }
                    break;
                default:
                    throw new DecodeError($"Cannot decode to a ticket {FactoriTypes.Utils.GetIMichelineString(im)}");
            }
   }
|}

let generic_base_decoder ppf b =
  let pp_csharp_base_type =
    pp_base_type ~language:Factori_config.Csharp ~abstract:false ?prefix:None
  in
  let literal = Base_type.str_of_lib_micheline (base_literal_type b) in
  match literal with
  | None -> (
    match b with
    | Unit ->
      Format.fprintf ppf
        "public Unit(IMicheline im){\n//TODO switch Prim Unit\n}"
    | Bool ->
      Format.fprintf ppf
        {|public Bool(IMicheline im){
     switch (im){
            case MichelinePrim m:
              switch (m.Prim)
              {
                  case(PrimType.True):
                  _value = true;
                  break;
                  case(PrimType.False):
                  _value = false;
                  break;
                  default:
                  throw new DecodeError($"Couldn't decode boolean{m}");
              }
              break;
           default:
             throw new DecodeError($"Failed to decode a boolean out of {FactoriTypes.Utils.GetIMichelineString(im)}");
            }
     }
|}
    | _ ->
      Format.fprintf ppf
        "public %a(IMicheline im) {  throw new Exception(\"No implemented \
         encoding for contract\"); }"
        pp_csharp_base_type b)
  | Some str ->
    Format.fprintf ppf
      "public %a(IMicheline im){\n\
       \tswitch(im){\n\
       \t\tcase Micheline%s mi:\n\
       \t\t\t_value = mi.Value;\n\
       \t\t\tbreak;\n\
       \t\tdefault:\n\
       \t\t\tthrow new DecodeError($\"A Micheline Int was expected, got \
       {FactoriTypes.Utils.GetIMichelineString(im)}\");}\n\
       }\n\n"
      pp_csharp_base_type b str

let build_decoder ppf kind =
  match kind with
  | ABase b -> generic_base_decoder ppf b
  | AContract ->
    fprintf ppf
      "public Contract(IMicheline im){ throw new Exception(\"No implemented \
       decoding for contract\"); }"
  | AUnary u -> (
    match u with
    | List ->
      fprintf ppf
        "public MList(IMicheline im){ _value = ListDecode<T>(im);}\n\n"
    | Set ->
      fprintf ppf "public Set(IMicheline im) : base(ListDecode<T>(im)) { }\n\n"
    | Option -> fprintf ppf "%s" decode_option
    | Ticket -> fprintf ppf "%s" decode_ticket)
  | ABinary b -> (
    match b with
    | Map ->
      fprintf ppf
        "public Map(IMicheline im) : base(DictDecode<TKey,TValue>(im)){ }\n\n"
    | BigMap ->
      fprintf ppf
        {|public BigMap(IMicheline im)
            {
                switch(im){
                    case MichelineInt mi:
                        _contentBigMap = new AbstractBigMap<TKey,TValue>(mi.Value);
                        break;
                    case MichelineArray ma:
                        var dict = new Dictionary<TKey,TValue>();
                        foreach (var item in ma)
                        {
                            dict.Append(DecodeMapElt<TKey,TValue>(item));
                        }
                        _contentBigMap = new LiteralBigMap<TKey,TValue>(dict);
                        break;
                    default:
                    throw new DecodeError($"Could not decode bigmap from {FactoriTypes.Utils.GetIMichelineString(im)}");
                    }
            }|}
    | Lambda -> ())
  | ATuple _k ->
    raise (Factori_errors.GenericError ("TuplesGeneration", "Unexpected Tuple"))

let generic_decode ppf k =
  let pp_csharp_base_type =
    pp_base_type ~language:Factori_config.Csharp ~abstract:false ?prefix:None
  in
  match k with
  | ABase b ->
    Format.fprintf ppf
      "public static IFactoriT GenericDecode(IMicheline im) { return new \
       %a(im);}\n\n"
      pp_csharp_base_type b
  | AUnary List ->
    Format.fprintf ppf
      "public static IFactoriT GenericDecode(IMicheline im) { return new \
       MList<T>(im);}\n\n"
  | AUnary u ->
    Format.fprintf ppf
      "public static IFactoriT GenericDecode(IMicheline im) { return new \
       %s<T>(im);}\n\n"
      (str_of_unary ~camelcase:true u)
  | ABinary bin ->
    Format.fprintf ppf
      "public static IFactoriT GenericDecode(IMicheline im) { return new \
       %s<TKey,TValue>(im);}\n\n"
      (str_of_binary ~camelcase:true bin)
  | AContract ->
    Format.fprintf ppf
      "public static IFactoriT GenericDecode(IMicheline im) { return new \
       Contract<T>(im);}\n\n"
  | ATuple _ -> () (* Handled in tuple_boilerplate *)

let generator_string_based b =
  match b with
  | Timestamp -> {|"2021-06-03T09:06:14.990-00:00"|}
  | Address | Keyhash -> {|"tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb"|}
  | Key -> {|"edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav"|}
  | Signature ->
    {|"edsigtkpiSSschcaCt9pUVrpNPf7TTcgvgDEDD6NCEHMy8NNQJCGnMfLZzYoQj74yLjo9wx6MPVV29CvVzgi7qEcEUok3k7AuMg"|}
  | Operation -> {|"op92MgaBZvgq2Rc4Aa4oTeZAjuAWHb63J2oEDP3bnFNWnGeja2T"|}
  | _ -> "new String(FactoriTypes.Utils.ShakespeareGenerator.GetRandomQuote())"

let generic_constructor ppf b =
  let pp_csharp_base_type =
    pp_base_type ~language:Factori_config.Csharp ~abstract:false ?prefix:None
  in
  let literal = Types.base_literal_type b in
  match literal with
  | Cint ->
    Format.fprintf ppf
      "public %a(){\n\
       \t_value = (BigInteger) (Utils.LocalRandom.randomInt(0,10));\n\
       }"
      pp_csharp_base_type b
  | Cbool ->
    Format.fprintf ppf
      "public %a(){\n\
       \t_value = Utils.chooseFrom(new List<bool>(){true, false});\n\
       }"
      pp_csharp_base_type b
  | Cbytes ->
    Format.fprintf ppf
      "public %a(){\n\t_value = Encoding.ASCII.GetBytes(\"ff\");\n}"
      pp_csharp_base_type b
  | Cunknown -> ()
  | Cstring ->
    Format.fprintf ppf "public %a(){\n\t_value = %s;\n}" pp_csharp_base_type b
      (generator_string_based b)

let build_generator ppf kind =
  match kind with
  | ABase b -> generic_constructor ppf b
  | AContract ->
    Format.fprintf ppf
      "public Contract() { throw new Exception(\"Can't create Contract outside \
       of a Smart Contract\"); }\n"
  | AUnary u -> (
    match u with
    | List -> fprintf ppf "public MList(){ _value = Utils.ListGenerator<T>(); }"
    | Set -> fprintf ppf "public Set() : base(Utils.ListGenerator<T>()) {  }"
    | Option ->
      fprintf ppf
        "public Option() { _contentOption = Utils.chooseFrom(new \
         List<Option<T>> {new Some<T>(),new None<T>()});\n\
         }\n"
    | Ticket ->
      fprintf ppf "public Ticket() : this(new Address(), new T(), new Nat()){}")
  | ABinary b -> (
    match b with
    | Map ->
      fprintf ppf
        "public  Map() : this(Utils.DictionaryGenerator<TKey,TValue>()) { }\n"
    | BigMap ->
      fprintf ppf
        "public BigMap() : this(new \
         LiteralBigMap<TKey,TValue>(Utils.DictionaryGenerator<TKey,TValue>())) \
         {} \n"
    | Lambda ->
      fprintf ppf
        "public Lambda() : this(new Netezos.Encoding.MichelineArray()){}\n")
  | ATuple _k ->
    raise (Factori_errors.GenericError ("TuplesGeneration", "Unexpected Tuple"))

let name_for_csharp special_prim ~min b =
  let min_name = min b in
  let min_name =
    if List.mem b special_prim then
      Format.sprintf "@%s" min_name
    else
      min_name in
  min_name

let build_micheline ppf kind =
  match kind with
  | ABase b ->
    let min_name = name_for_csharp Base_type.special_prim ~min:str_of_base b in
    Format.fprintf ppf
      "public static IMicheline GetMicheline(){\n\
       \tIMicheline ret = new MichelinePrim{Prim=PrimType.%s};\n\
       \treturn ret;\n\
       }\n"
      min_name
  | AContract ->
    Format.fprintf ppf
      "public static IMicheline GetMicheline(){\n\
       \tIMicheline ret = new MichelinePrim{Prim=PrimType.contract, Args= new \
       List<IMicheline>(){T.GetMicheline()}};\n\
       \treturn ret;\n\
       }\n"
  | AUnary b ->
    let min_name = name_for_csharp [] ~min:str_of_unary b in
    Format.fprintf ppf
      "public static IMicheline GetMicheline(){\n\
       \tIMicheline ret = new MichelinePrim{Prim=PrimType.%s, Args= new \
       List<IMicheline>(){T.GetMicheline()}};\n\
       \treturn ret;\n\
       }\n"
      min_name
  | ABinary b ->
    let min_name = name_for_csharp [] ~min:str_of_binary b in
    Format.fprintf ppf
      "public static IMicheline GetMicheline(){\n\
       \tIMicheline ret = new MichelinePrim{Prim=PrimType.%s, Args= new \
       List<IMicheline>(){TKey.GetMicheline(),TValue.GetMicheline()}};\n\
       \treturn ret;\n\
       }\n"
      min_name
  | ATuple _k ->
    raise (Factori_errors.GenericError ("TuplesGeneration", "Unexpected Tuple"))

let encode_ticket =
  {|public IMicheline Encode()
        {
            return new MichelinePrim {Prim = PrimType.Pair, Args = new List<IMicheline>{_addr.Encode(), _value.Encode(), _amount.Encode()}};
   }|}

let generic_encoder ppf b =
  let literal = Base_type.str_of_lib_micheline (base_literal_type b) in
  match literal with
  | None -> (
    match b with
    | Unit ->
      Format.fprintf ppf
        "public IMicheline Encode(){ return new \
         MichelinePrim{Prim=PrimType.Unit};}"
    | Bool ->
      Format.fprintf ppf
        {|
public IMicheline Encode(){
            if(_value){
                return new MichelinePrim {Prim = PrimType.True};
            }
            else{
                return new MichelinePrim {Prim = PrimType.False};
            }
}
|}
    | _ ->
      Format.fprintf ppf
        "public IMicheline Encode(){ throw new Exception(\"No implemented \
         encoding for this type\"); }")
  | Some str ->
    Format.fprintf ppf
      "public IMicheline Encode(){\nreturn new Micheline%s(_value);\n       }\n"
      str

let build_encoder ppf kind =
  match kind with
  | ABase b -> generic_encoder ppf b
  | AContract ->
    fprintf ppf
      "public IMicheline Encode(){ throw new Exception(\"No implemented \
       encoding for contract\"); }"
  | AUnary u -> (
    match u with
    | List ->
      fprintf ppf "public IMicheline Encode(){ return ListEncode<T>(_value);}"
    | Set ->
      fprintf ppf "public IMicheline Encode(){ return ListEncode<T>(this);}"
    | Option ->
      fprintf ppf
        "public IMicheline Encode(){ return _contentOption.subEncode();}"
    | Ticket -> fprintf ppf "%s" encode_ticket)
  | ABinary b -> (
    match b with
    | Map ->
      fprintf ppf
        "public IMicheline Encode(){\n\
         return DictEncode<TKey,TValue>(this);\n\
        \         }"
    | BigMap ->
      fprintf ppf
        {|public IMicheline Encode(){ return _contentBigMap.subEncode(); }|}
    | Lambda -> fprintf ppf "public IMicheline Encode() { return _body; }\n")
  | ATuple _k ->
    raise (Factori_errors.GenericError ("TuplesGeneration", "Unexpected Tuple"))
