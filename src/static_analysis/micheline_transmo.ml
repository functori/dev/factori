(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The OCaml file contains data types and functions related to control flow
graphs and their nodes in the Michelson language used in Tezos blockchain. It
defines several types and functions for determining the type of Michelson
instructions and decompiling Micheline instructions into control flow graphs.
The `decompile_smart` function is also defined which takes a `script_expr` and
decompiles it into a control flow graph. It is important to note that some
functions expect specific input types and users should have a good understanding
of the Michelson language to effectively use these functions. Warning: this
short summary was automatically generated and could be incomplete or misleading.
If you feel this is the case, please open an issue. *)

open Tzfunc.Proto
open Factori_errors
open Unstack_micheline
open Tzfunc.Rp
open Factori_micheline_lib
module List = Factori_utils.List

let print_stack l =
  Format.pp_print_list
    ~pp_sep:(fun format () -> Format.fprintf format " :: ")
    (fun format elt ->
      Format.fprintf format "%s" (Micheline_lib.string_of_micheline (snd elt)))
    l

let new_stack_item cpt miche = Micheline_lib.new_item_cpt cpt miche

(* Split a list at a z index, if z is bigger than the size of the list
   it returns None *)
let top_tail_of_z z l =
  let rec aux_split acc cpt l =
    if Z.compare cpt Z.zero <= 0 then
      Some (List.rev acc, l)
    else
      match l with
      | [] -> None
      | x :: xs -> aux_split (x :: acc) (Z.pred cpt) xs in
  aux_split [] z l

let no_annot node = node

(* In most of the functions, the return will have a label and a name counter to have unique
   variable name and unique location in the unstack michelson generated *)

(* Takes a stack and returns a PAIR constructor for the unstack michelson and return the new stack *)
let decomp_pair ~label ~name_cpt _annots (n : Z.t) stack =
  let res_opt = top_tail_of_z n stack in
  match res_opt with
  | None -> Error (UnstackMicheline "Not enough element in stack for PAIR")
  | Some (parameter, new_stack) ->
    let$ pair =
      Type_primitive.pair_or_simple
        (List.map (fun (_, miche) -> miche) parameter) in
    let$ new_cpt, return = new_stack_item name_cpt pair in
    (*let node = N_PAIRS (parameter, return) in*)
    let node = N_SIMPLE (`PAIR, [], parameter, [return], label) in
    Ok (Some (return :: new_stack), [node], label + 1, new_cpt)

(* Same as pair but for unpair, returns the node and the new stack *)
let decomp_unpair ~label ~name_cpt _annots n stack =
  match stack with
  | [] -> Error (UnstackMicheline "Empty stack for unpair")
  | (top, ty) :: xs ->
    let$ ty_l = Type_primitive.split_pair_at n ty in
    (* let tops, tl = Utility.split_at n ty_l in *)
    let$ new_cpt, l =
      List.fold_left_map_result
        (fun new_cpt ty ->
          let$ new_cpt, new_item = new_stack_item new_cpt ty in
          Ok (new_cpt, new_item))
        name_cpt ty_l in
    let node = N_SIMPLE (`UNPAIR, [], [(top, ty)], l, label) in
    Ok (Some (l @ xs), [node], label + 1, new_cpt)

(* Takes stack and returns the same stack bu with position 0 is the bottom of the stack *)
let stack_with_pos stack =
  let stack = List.rev stack in
  let rev_stack = List.mapi (fun i (name, ty) -> (i, name, ty)) stack in
  List.rev rev_stack

(* Takes two stacks and returns what has changed between the first and the second *)
let compare_stack stack1 stack2 =
  let stack1 = stack_with_pos stack1 and stack2 = stack_with_pos stack2 in
  let rec aux_stack cpt acc = function
    | [] -> acc
    | (pos1, name1, _ty1) :: xs ->
      let is_new =
        not
          (List.exists
             (fun (pos2, name2, _ty2) -> pos1 == pos2 && name1 = name2)
             stack1) in
      if is_new then
        aux_stack (cpt + 1) (cpt :: acc) xs
      else
        aux_stack (cpt + 1) acc xs in
  aux_stack 0 [] stack2

(* Apply a function f at a certain position in the stack *)
let apply_at_pos f pos stack =
  let opt = top_tail_of_z (Z.of_int pos) stack in
  match opt with
  | None | Some (_, []) ->
    Error (UnstackMicheline "Stack is too short apply_at")
  | Some (tops, x :: xs) -> Ok (tops @ (f x :: xs))
(* List.filter
   (fun (name1, ty1 ) ->
     not
       (List.exists
          (fun (name2, ty2) ->
            name1 = name2 && TypeMichelson.eq_michety ty1 ty2)
          stack1))
   stack2 *)

(*let change_fun (_, michety) = (name_stack (), michety)*)

(*let name_opt name opt =
  match opt with
  | Some (`VarAnnot e) -> name_from e
  | _ -> name*)

let decompile_if all_pos node stack =
  let rev_node = List.rev node in
  match rev_node with
  | N_END (_, lab) :: xs ->
    let recup stack =
      let size = List.length stack in
      List.fold_left
        (fun acc i ->
          if i >= size then
            acc
          else
            List.nth stack i :: acc)
        [] all_pos in
    let recup_stack = recup stack in
    let new_node = List.rev (N_END (recup_stack, lab) :: xs) in
    Ok new_node
  | _ -> Error (UnstackMicheline "IF branch should end with an END node")

(* This functions is here to replace the last node of a list of instruction in a if because
   decompiling only a list of instruction (for example all the 'then' instructions) will lead
   to miss returns. Two branches must returns the same thing so we need to combine what 'then'
   and 'else' return to build all_pos. All_pos is the list of the position that the new node
   should return *)
let compile_if all_pos node_then stack_then node_else stack_else =
  let rev_else = List.rev node_else in
  let rev_then = List.rev node_then in
  match (rev_then, rev_else) with
  | N_END (_, lab_then) :: xs, N_END (_, lab_end) :: ys ->
    let recup stack =
      let size = List.length stack in
      (* can fail if i is negative *)
      List.fold_left
        (fun acc i ->
          if i >= size then
            acc
          else
            List.nth stack i :: acc)
        [] all_pos in
    let recup_then = recup stack_then in
    let recup_else = recup stack_else in
    let node_then = List.rev (N_END (recup_then, lab_then) :: xs) in
    let node_else = List.rev (N_END (recup_else, lab_end) :: ys) in
    Ok (node_then, node_else)
  | _ -> Error (UnstackMicheline "IF branch should end with an END node")

let compile_binary ~label ~name_cpt stack cons type_fun =
  match stack with
  | (name1, ty1) :: (name2, ty2) :: xs ->
    let$ ty = type_fun ty1 ty2 in
    let$ new_cpt, top = new_stack_item name_cpt ty in
    let node = cons (name1, ty1) (name2, ty2) top in
    Ok (Some (top :: xs), [node], label + 1, new_cpt)
  | _ -> Error (UnstackMicheline "Stack is too short compile binary")

let compile_triple ~label ~name_cpt stack cons type_fun instr =
  match stack with
  | arg1 :: arg2 :: arg3 :: sub_stack ->
    let$ ty_l = type_fun (snd arg1) (snd arg2) (snd arg3) in
    let$ new_cpt, new_tops =
      List.fold_left_map_result
        (fun cpt_fold ty ->
          let$ new_cpt, new_item = new_stack_item cpt_fold ty in
          Ok (new_cpt, new_item))
        name_cpt ty_l in
    let$ node = cons arg1 arg2 arg3 new_tops in
    Ok (Some (new_tops @ sub_stack), [node], label + 1, new_cpt)
  | _ ->
    Error
      (UnstackMicheline (Format.sprintf "Wrong stack for %s instruction" instr))

let compile_eq_prim ~label ~name_cpt stack cons =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | x :: xs ->
    let ty = Mprim { prim = `bool; args = []; annots = [] } in
    let$ new_cpt, top = new_stack_item name_cpt ty in
    let node = cons x top in
    Ok (Some (top :: xs), [node], label + 1, new_cpt)

let compile_unary ~label ~name_cpt stack type_fun cons =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | (name_arg, ty_arg) :: xs ->
    let$ ty = type_fun ty_arg in
    let$ new_cpt, top = new_stack_item name_cpt ty in
    let node = cons (name_arg, ty_arg) top in
    Ok (Some (top :: xs), [node], label + 1, new_cpt)

let post_decomp ~new_stack ~name_cpt arg_l =
  let$ new_cpt_stack, recup =
    List.fold_left_map_result
      (fun (cpt_fold, result) i ->
        let _, ty = List.nth new_stack i in
        let$ new_cpt, new_item = new_stack_item cpt_fold ty in
        let$ stack =
          apply_at_pos (fun _ -> (*see if its a problem *) new_item) i result
        in
        Ok ((new_cpt, stack), new_item))
      (name_cpt, new_stack) arg_l in
  Ok (List.rev recup, new_cpt_stack)

(* let make_args ty =
   match ty with
   | Pair ty_l, [] ->
     List.map (fun ty -> (name_from "arg", ty\0)) (TypeMichelson.flat_ty_l ty_l)
   | _ -> [(name_from "arg", ty)] *)

let loop_left_post ~label_lf ~name_cpt top stack new_stack node_loop pat_left
    right =
  (*top is the top before loop left, stack is the stack before loop left without the top
     new stack is the stack after the loop left pat is the pattern use in the loop left
     and right is the type of the or right*)
  let label_pat_lf = label_lf + 1 in
  let label_pat_res = label_pat_lf + 1 in
  let label_node = label_pat_res + 1 in
  let$ new_cpt, pat_right = new_stack_item name_cpt right in
  match new_stack with
  | None ->
    let node =
      N_LOOP_LEFT
        {
          arg_lf = top;
          pat_lf = (pat_left, label_pat_lf);
          acc_lf = [];
          pat_acc_lf = [];
          node_loop;
          pat_res_lf = (pat_right, label_pat_res);
          label_lf;
        } in
    Ok (None, [FAIL_CONTROL (node, label_node)], label_node + 1, new_cpt)
  | Some new_stack -> (
    match new_stack with
    | [] ->
      Error (UnstackMicheline "Not implemented yet for loop_left instruction")
    | _ :: xs ->
      let$ new_cpt, res = new_stack_item new_cpt right in
      let arg_l = compare_stack stack xs in
      let acc = List.rev_map (fun i -> List.nth stack i) arg_l in
      let$ new_cpt, pat_acc =
        List.fold_left_map_result
          (fun cpt_fold (_, ty) ->
            let$ new_cpt, new_item = new_stack_item cpt_fold ty in
            Ok (new_cpt, new_item))
          new_cpt acc in
      let node_loop = Lib_unstack.replace_ident node_loop acc pat_acc in
      let$ recup, (new_cpt, new_stack) =
        post_decomp ~new_stack:xs ~name_cpt:new_cpt arg_l in
      let loop_l =
        N_LOOP_LEFT
          {
            arg_lf = top;
            pat_lf = (pat_left, label_pat_lf);
            pat_acc_lf = pat_acc;
            acc_lf = acc;
            node_loop;
            pat_res_lf = (pat_right, label_pat_res);
            label_lf;
          } in
      let node = N_START (res :: recup, loop_l, label_node) in
      let node_annot = no_annot node in
      Ok (Some (res :: new_stack), [node_annot], label_node + 1, new_cpt))

(* we must return the top of the stack even if it didn't change *)
let return_stack_special ~label ~name_cpt acc init_stack stack =
  match stack with
  | [] -> Error (UnstackMicheline "Empty return")
  | (name, ty) :: xs ->
    let ys = List.tl init_stack in
    let arg_l = compare_stack ys xs in
    let recup = List.fold_left (fun acc i -> List.nth xs i :: acc) [] arg_l in
    let new_recup_end = (name, ty) :: recup in
    Ok
      ( Some stack,
        List.rev (N_END (new_recup_end, label) :: acc),
        label + 1,
        name_cpt )

let car_cdr name_cpt stack _instr f =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | (name_top, ty) :: xs -> (
    let$ ty_l = Type_primitive.split_pair_at (Z.of_int 2) ty in
    match ty_l with
    | [ty1; ty2] ->
      let top = (name_top, ty) in
      let$ res, node, label, final_cpt = f name_cpt (ty1, ty2) top in
      Ok (Some (res :: xs), [node], label, final_cpt)
    | _ -> Error (UnstackMicheline "Unexpected result from split_pair_at"))

(* let choose_map arg_l =
   let size = List.length arg_l and zero_change = List.mem 0 arg_l in
   match (size, zero_change) with
   | 1, true -> MAP
   | _ -> FOLD_MAP *)

let rec michelson2node_if ~full_script ~name_cpt ~label_arg if_cons init_stack
    (instr_then, stack_then) (instr_else, stack_else) =
  let result_then =
    decompile_instr full_script ~name_cpt ~label_init:label_arg ~loop_left:false
      ~init_stack:stack_then ~instr_l:instr_then in
  let post_if ~label_post ~new_cpt recup node_then node_else new_stack =
    let node = N_START (recup, if_cons node_then node_else, label_post) in
    Ok (Some new_stack, [no_annot node], label_post + 1, new_cpt) in
  let$ new_stack_then, node_then, label_then, name_if_cpt = result_then in
  let result_else =
    decompile_instr full_script ~name_cpt:name_if_cpt ~label_init:label_then
      ~loop_left:false ~init_stack:stack_else ~instr_l:instr_else in
  let$ new_stack_else, node_else, label_else, name_else_cpt = result_else in
  match (new_stack_then, new_stack_else) with
  | None, None ->
    Ok
      ( None,
        [FAIL_CONTROL (if_cons node_then node_else, label_else)],
        label_else + 1,
        name_else_cpt )
  | None, Some stack ->
    let arg = compare_stack init_stack stack in
    let$ recup, (new_cpt, new_stack) =
      post_decomp ~new_stack:stack ~name_cpt:name_else_cpt arg in
    let$ node_else = decompile_if arg node_else stack in
    post_if ~label_post:label_else ~new_cpt recup node_then node_else new_stack
    (*let node = N_START (recup, if_cons node_then node_else) in
        Ok (Some new_stack, [no_annot node])*)
  | Some stack, None ->
    let arg = compare_stack init_stack stack in
    let$ recup, (new_cpt, new_stack) =
      post_decomp ~new_stack:stack ~name_cpt:name_else_cpt arg in
    let$ node_then = decompile_if arg node_then stack in
    post_if ~label_post:label_else ~new_cpt recup node_then node_else new_stack
    (*let node = N_START (recup, if_cons node_then node_else) in
        Ok (Some new_stack, [node])*)
  | Some new_stack_then, Some new_stack_else ->
    let arg_then = compare_stack new_stack_else new_stack_then in
    let arg_else = compare_stack new_stack_then new_stack_else in
    (*let arg_then = compare_stack init_stack new_stack_then in
      let arg_else = compare_stack init_stack new_stack_else in*)
    let all_pos =
      List.fold_left
        (fun acc arg ->
          if List.mem arg acc then
            acc
          else
            arg :: acc)
        arg_then arg_else in
    let$ node_then, node_else =
      compile_if all_pos node_then new_stack_then node_else new_stack_else in
    let$ recup, (new_cpt, new_stack) =
      post_decomp ~new_stack:new_stack_then ~name_cpt:name_else_cpt all_pos
    in
    let node = N_START (recup, if_cons node_then node_else, label_else) in
    Ok (Some new_stack, [node], label_else + 1, new_cpt)

and collection_application ~full_script ~label_start ~name_cpt ty_fun stack
    special instr_l cons =
  match stack with
  | [] -> Error (UnstackMicheline "Stack is empty")
  | coll :: xs -> (
    let$ ty = ty_fun (snd coll) in
    let$ new_cpt, top = new_stack_item name_cpt ty in
    let$ new_stack, node_l, label_end, final_name =
      decompile_instr full_script ~name_cpt:new_cpt ~label_init:label_start
        ~loop_left:false ~init_stack:(top :: xs) ~instr_l in
    match (new_stack, special) with
    | None, true ->
      Error
        (UnstackMicheline
           "FAIL cannot be the only instruction in the body. The proper type \
            of the return list cannot be inferred")
    | None, false ->
      Ok
        ( None,
          [FAIL_CONTROL (cons (coll, [], [], top, node_l), label_end)],
          label_end + 1,
          final_name )
    | Some new_stack, _ ->
      let stack_cmp =
        if special then
          List.tl new_stack
        else
          new_stack in
      let arg_l = compare_stack xs stack_cmp in
      let$ recup, (new_cpt, post_stack) =
        post_decomp ~new_stack:stack_cmp ~name_cpt:final_name arg_l in
      let accumulator =
        List.fold_left
          (fun acc i ->
            let item = List.nth xs i in
            item :: acc)
          [] arg_l in
      let$ cpt_after_acc, pat_acc =
        List.fold_left_map_result
          (fun cpt_fold (_, ty) ->
            let$ new_cpt, new_item = new_stack_item cpt_fold ty in
            Ok (new_cpt, new_item))
          new_cpt accumulator in
      let node_l = Lib_unstack.replace_ident node_l accumulator pat_acc in
      let$ recup, new_stack, cpt_special =
        if special then
          let$ ty =
            Type_primitive.type_map (snd (List.hd new_stack)) (snd coll) in
          let$ new_cpt, top = new_stack_item cpt_after_acc ty in
          Ok (top :: recup, top :: post_stack, new_cpt)
        else
          Ok (recup, post_stack, cpt_after_acc) in
      let node =
        N_START
          (recup, cons (coll, accumulator, pat_acc, top, node_l), label_end)
      in
      Ok (Some new_stack, [node], label_end + 1, cpt_special))

and decomp_code_loop ~full_script ~name_cpt ~label stack instr_l =
  match stack with
  | (name, Mprim { prim = `bool; args = []; annots }) :: xs -> (
    let label_start = label in
    let label_pat = label + 1 in
    let label_loop = label_pat + 1 in
    let result =
      decompile_instr full_script ~name_cpt ~label_init:(label_loop + 1)
        ~loop_left:false ~init_stack:xs ~instr_l in
    let$ new_stack, node_code, label_ret, final_name_cpt = result in
    match new_stack with
    | None ->
      let top = (name, prim `bool) in
      let ctrl_node =
        N_START
          ( [],
            N_LOOP
              {
                arg = top;
                acc = [];
                pat_acc = [];
                label_pat_l = label_pat;
                body = node_code;
                label_loop;
              },
            label_start ) in
      Ok (None, [ctrl_node], label_ret, final_name_cpt)
    | Some new_stack -> (
      let top = (name, Mprim { prim = `bool; args = []; annots }) in
      let arg_l = compare_stack stack new_stack in
      let acc = List.rev_map (fun i -> List.nth stack i) arg_l in
      let acc = List.tl acc in
      let$ cpt_after_acc, pat_acc =
        List.fold_left_map_result
          (fun cpt_fold (_, ty) ->
            let$ new_cpt, new_item = new_stack_item cpt_fold ty in
            Ok (new_cpt, new_item))
          final_name_cpt acc in
      let node_code = Lib_unstack.replace_ident node_code acc pat_acc in
      let$ res, (new_cpt, new_stack) =
        post_decomp ~new_stack ~name_cpt:cpt_after_acc arg_l in
      let node =
        N_LOOP
          {
            arg = top;
            acc;
            pat_acc;
            body = node_code;
            label_loop;
            label_pat_l = label_pat;
          } in
      let node = N_START (List.tl res, node, label_start) in
      match new_stack with
      | [] -> Error (UnstackMicheline "Stack is empty")
      | _ :: tl -> Ok (Some tl, [node], label_ret, new_cpt)))
  | _ -> Error (UnstackMicheline "Not a good stack for a LOOP instruction")

and decomp_dip ~full_script ~name_cpt ~label_start n stack instr_l =
  let opt = top_tail_of_z n stack in
  match opt with
  | None ->
    Error (UnstackMicheline "Stack is not long enough for a DIP instruction")
  | Some (tops, xs) -> (
    let result =
      decompile_instr full_script ~name_cpt ~label_init:label_start
        ~loop_left:false ~init_stack:xs ~instr_l in
    let$ new_stack, node_l, label_end, final_name_cpt = result in
    match new_stack with
    | None ->
      Error
        (UnstackMicheline "Fail instruction must appear at a terminal position")
    | Some new_stack -> (
      match List.rev node_l with
      | N_END _ :: xs ->
        Ok (Some (tops @ new_stack), xs, label_end, final_name_cpt)
      | _ ->
        Error
          (UnstackMicheline "Instruction list does not finish with end node")))

and decompile_instr ?stack_to_check full_script ~name_cpt ~label_init ~loop_left
    ~init_stack ~instr_l =
  let rec aux_decomp label cpt_fold acc stack instr_l =
    match instr_l with
    | [] -> (
      match stack with
      | None -> Ok (None, List.rev acc, label + 1, cpt_fold)
      | Some stack when loop_left ->
        return_stack_special ~label ~name_cpt:cpt_fold acc init_stack stack
      | Some stack ->
        let stack_check = Option.value stack_to_check ~default:init_stack in
        let arg_l = compare_stack stack_check stack in
        let recup =
          List.fold_left (fun acc i -> List.nth stack i :: acc) [] arg_l in
        Ok
          ( Some stack,
            List.rev (N_END (recup, label) :: acc),
            label + 1,
            cpt_fold ))
    | Mseq seq :: xs -> aux_decomp label cpt_fold acc stack (seq @ xs)
    | x :: xs -> (
      match stack with
      | None ->
        Error
          (UnstackMicheline "Fail instruction should be at a terminal position")
      | Some stack ->
        let$ new_stack, node_l, label, new_cpt =
          decompile full_script cpt_fold label stack x in
        aux_decomp label new_cpt (node_l @ acc) new_stack xs) in
  aux_decomp label_init name_cpt [] (Some init_stack) instr_l

and map_on_collection ~full_script ~name_cpt ~label stack instr_l =
  let label_pat = label + 1 in
  let label_pat_acc = label_pat + 1 in
  let cons (arg, accumulator, pat_acc, pat, node_iter) =
    match accumulator with
    | [] -> N_MAP (arg, label_pat, pat, node_iter, label)
    | _ ->
      N_FOLD_MAP
        {
          arg_fold_map = arg;
          acc_fold_map = accumulator;
          pat_acc_fold_map = (pat_acc, label_pat_acc);
          pat_fold_map = (pat, label_pat);
          body_fold_map = node_iter;
          label_map = label;
        } in
  collection_application ~full_script ~label_start:(label_pat_acc + 1) ~name_cpt
    Type_primitive.type_arg_map stack true instr_l cons

and decompile full_script name_cpt label stack instr =
  match instr with
  | Mprim { prim = `DROP; args = []; _ } -> (
    (* no use new_name so we don't need to return new_cpt *)
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for DROP instruction")
    | _ :: tl -> Ok (Some tl, [], label, name_cpt))
  | Mprim { prim = `DROP; args = [Mint n]; _ } -> (
    let stack = top_tail_of_z n stack in
    match stack with
    | None ->
      Error (UnstackMicheline "Not enough element for a DROP instruction")
    | Some (_, new_stack) -> Ok (Some new_stack, [], label, name_cpt))
  | Mprim { prim = `DUP; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for DUP instruction")
    | top :: _ -> Ok (Some (top :: stack), [], label, name_cpt))
  | Mprim { prim = `DUP; args = [Mint n]; _ } -> (
    let position = Z.pred n in
    match List.nth_opt stack (Z.to_int position) with
    | None ->
      Error (UnstackMicheline "Wrong argument/ stack for a DUP instruction")
    | Some top -> Ok (Some (top :: stack), [], label, name_cpt))
  | Mprim { prim = `SWAP; args = []; _ } -> (
    match stack with
    | x :: y :: xs -> Ok (Some (y :: x :: xs), [], label, name_cpt)
    | _ -> Error (UnstackMicheline "Not a good stack for a SWAP instruction"))
  | Mprim { prim = `DIG; args = [Mint n]; _ } -> (
    let opt = top_tail_of_z n stack in
    match opt with
    | None | Some (_, []) -> Error (UnstackMicheline "Empty stack")
    | Some (tops, x :: xs) -> Ok (Some (x :: (tops @ xs)), [], label, name_cpt))
  | Mprim { prim = `DIP; args = [Mint n; Mseq instr_l]; _ } ->
    decomp_dip ~full_script ~name_cpt ~label_start:label n stack instr_l
  | Mprim { prim = `DIP; args = [Mseq instr_l]; _ } ->
    decomp_dip ~full_script ~name_cpt ~label_start:label Z.one stack instr_l
  | Mprim { prim = `DUG; args = [Mint n]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Empty stack")
    | x :: xs -> (
      let opt = top_tail_of_z n xs in
      match opt with
      | None ->
        Error (UnstackMicheline "Not enough element for a DUG instruction")
      | Some (tops, bot) ->
        let stack = tops @ (x :: bot) in
        Ok (Some stack, [], label, name_cpt)))
  | Mprim { prim = `PUSH; args = [ty; data]; _ } ->
    (* use of new_name so we must return new_cpt *)
    let$ new_cpt, top = new_stack_item name_cpt ty in
    (*let node = N_PUSH (data, top) in*)
    let node = N_SIMPLE (`PUSH, [ty; data], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, new_cpt)
  | Mprim { prim = `SOME; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Empty stack")
    | (n, ty) :: xs ->
      let$ ty_opt = Type_primitive.make_option ty in
      let$ new_cpt, top = new_stack_item name_cpt ty_opt in
      let node = N_SIMPLE (`SOME, [], [(n, ty)], [top], label) in
      (*let node = N_SOME ((n, ty), top) in*)
      Ok (Some (top :: xs), [node], label + 1, new_cpt))
  | Mprim { prim = `NONE; args = [ty]; _ } ->
    let$ ty_opt = Type_primitive.make_option ty in
    let$ new_cpt, top = new_stack_item name_cpt ty_opt in
    (*let _node = N_NONE (ty, top) in*)
    let node = N_SIMPLE (`NONE, [ty], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, new_cpt)
  | Mprim { prim = `UNIT; args = []; _ } ->
    let ty = prim `unit in
    let$ new_cpt, top = new_stack_item name_cpt ty in
    let node = N_SIMPLE (`UNIT, [], [], [top], label) in
    (*let node = N_UNIT top in*)
    Ok (Some (top :: stack), [node], label + 1, new_cpt)
  | Mprim { prim = `UNPAIR; args = []; annots } ->
    decomp_unpair ~label ~name_cpt annots (Z.of_int 2) stack
  | Mprim { prim = `UNPAIR; args = [Mint n]; annots } ->
    decomp_unpair ~label ~name_cpt annots n stack
  | Mprim { prim = `PAIR; args = []; annots } ->
    decomp_pair ~label ~name_cpt annots (Z.of_int 2) stack
  | Mprim { prim = `PAIR; args = [Mint n]; annots } ->
    decomp_pair ~label ~name_cpt annots n stack
  | Mprim { prim = `NIL; args = [ty]; _ } ->
    let ty_l = prim `list ~args:[ty] in
    let$ new_cpt, top = new_stack_item name_cpt ty_l in
    (*let node = N_NIL (ty, top) in*)
    let node = N_SIMPLE (`NIL, [ty], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, new_cpt)
  | Mprim { prim = `IF; args = [Mseq instr_then; Mseq instr_else]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let label_then = label in
      let label_else = label_then + 1 in
      let label_arg = label_else + 1 in
      michelson2node_if ~full_script ~name_cpt ~label_arg
        (fun n_then n_else ->
          let node = N_IF (x, label_then, label_else, n_then, n_else) in
          no_annot node)
        xs (instr_then, xs) (instr_else, xs))
  | Mprim { prim = `IF_NONE; args = [Mseq instr_then; Mseq instr_else]; _ } -> (
    match stack with
    | (name_param, (Mprim { prim = `option; args = [pat_ty]; _ } as ty)) :: xs
      ->
      let$ cpt_some, top = new_stack_item name_cpt pat_ty in
      let label_then = label in
      let label_else = label_then + 1 in
      let label_pat = label_else + 1 in
      let label_next = label_pat + 1 in
      let stack_some = top :: xs in
      michelson2node_if ~full_script ~name_cpt:cpt_some ~label_arg:label_next
        (fun n_then n_else ->
          N_IF_NONE
            ( (name_param, ty),
              label_then,
              label_else,
              n_then,
              n_else,
              top,
              label_pat ))
        xs (instr_then, xs) (instr_else, stack_some)
    | _ -> Error (UnstackMicheline "Wrong stack for a IF_NONE instruction"))
  | Mprim { prim = `IF_CONS; args = [Mseq instr_c; Mseq instr_n]; _ } -> (
    match stack with
    | (name_param, (Mprim { prim = `list; args = [ty_x]; _ } as ty)) :: xs ->
      let$ new_cpt, top = new_stack_item name_cpt ty_x in
      let$ new_cpt, snd = new_stack_item new_cpt ty in
      let label_then = label in
      let label_else = label_then + 1 in
      let label_pat_x = label_else + 1 in
      let label_pat_xs = label_pat_x + 1 in
      let label_next = label_pat_xs + 1 in
      let stack_cons = top :: snd :: xs in
      michelson2node_if ~full_script ~name_cpt:new_cpt ~label_arg:label_next
        (fun n_then n_else ->
          N_IF_CONS
            ( (name_param, ty),
              label_then,
              label_else,
              n_then,
              n_else,
              top,
              label_pat_x,
              snd,
              label_pat_xs ))
        xs (instr_c, stack_cons) (instr_n, xs)
    | _ -> Error (UnstackMicheline "Wrong stack for a IF_CONS instruction"))
  | Mprim { prim = `ADD; args = []; _ } ->
    let type_fun = Type_primitive.type_add in
    let cons arg1 arg2 top =
      N_SIMPLE (`ADD, [], [arg1; arg2], [top], label)
      (*N_ADD (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `SUB; args = []; _ } ->
    let type_fun = Type_primitive.type_sub in
    let cons arg1 arg2 top =
      N_SIMPLE (`SUB, [], [arg1; arg2], [top], label)
      (*N_SUB (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `SUB_MUTEZ; args = []; _ } ->
    let type_fun = Type_primitive.type_sub_mutez in
    let cons arg1 arg2 top =
      N_SIMPLE (`SUB_MUTEZ, [], [arg1; arg2], [top], label)
      (*N_SUB_MUTEZ (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `MUL; args = []; _ } ->
    let type_fun = Type_primitive.type_mul in
    let cons arg1 arg2 top =
      N_SIMPLE (`MUL, [], [arg1; arg2], [top], label)
      (*N_MUL (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `CONCAT; args = []; _ } -> (
    match stack with
    | ((_, Mprim { prim = `list; args = [ty]; _ }) as arg) :: xs ->
      let$ ty = Type_primitive.type_sub_concat ty in
      let$ new_cpt, top = new_stack_item name_cpt ty in
      let node = N_CONCAT_LIST (arg, top, label) in
      Ok (Some (top :: xs), [node], label + 1, new_cpt)
    | _ ->
      let type_fun = Type_primitive.type_concat in
      let cons arg1 arg2 top = N_CONCAT (arg1, arg2, top, label) in
      compile_binary ~label ~name_cpt stack cons type_fun)
  | Mprim { prim = `EDIV; args = []; _ } ->
    let type_fun = Type_primitive.type_ediv in
    let cons arg1 arg2 top =
      N_SIMPLE (`EDIV, [], [arg1; arg2], [top], label)
      (*N_EDIV (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `COMPARE; args = []; _ } ->
    let type_fun _ _ = Ok (prim `int) in
    let cons arg1 arg2 top =
      N_SIMPLE (`COMPARE, [], [arg1; arg2], [top], label)
      (*N_COMPARE (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `ABS; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let ty = prim `nat in
      let$ new_cpt, top = new_stack_item name_cpt ty in
      (*let node = N_ABS (x, top) in*)
      let node = N_SIMPLE (`ABS, [], [x], [top], label) in
      Ok (Some (top :: xs), [node], label + 1, new_cpt))
  | Mprim { prim = `AND; args = []; _ } ->
    let type_fun = Type_primitive.type_and in
    let cons arg1 arg2 top =
      N_SIMPLE (`AND, [], [arg1; arg2], [top], label)
      (*N_AND (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `EQ; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`EQ, [], [arg1], [arg2], label)
      (*N_EQ (arg1, arg2)*) in
    compile_eq_prim ~label ~name_cpt stack cons
  | Mprim { prim = `GE; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`GE, [], [arg1], [arg2], label)
      (*N_GE (arg1, arg2)*) in
    compile_eq_prim ~label ~name_cpt stack cons
  | Mprim { prim = `GT; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`GT, [], [arg1], [arg2], label)
      (*N_GT (arg1, arg2)*) in
    compile_eq_prim ~label ~name_cpt stack cons
  | Mprim { prim = `LE; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`LE, [], [arg1], [arg2], label)
      (*N_LE (arg1, arg2)*) in
    compile_eq_prim ~label ~name_cpt stack cons
  | Mprim { prim = `LAMBDA; args = [arg; ret; Mseq instr_l]; _ } -> (
    let label_in = label + 1 in
    let$ new_cpt, pat_lambda = new_stack_item name_cpt arg in
    let ty = prim `lambda ~args:[arg; ret] in
    let$ new_cpt, top = new_stack_item new_cpt ty in
    let$ result =
      decompile_instr full_script ~label_init:(label_in + 1) ~name_cpt:new_cpt
        ~stack_to_check:[] ~loop_left:false ~init_stack:[pat_lambda] ~instr_l
    in
    match result with
    | Some [_], node_fun, label_ret, final_name_cpt
    | None, node_fun, label_ret, final_name_cpt ->
      let node =
        N_LAMBDA (pat_lambda, label, node_fun, top, label_in, label_ret) in
      Ok (Some (top :: stack), [node], label_ret + 1, final_name_cpt)
    | _ ->
      Error
        (UnstackMicheline
           "Error the stack for a LAMBDA instructions is not exactly one"))
  | Mprim { prim = `EXEC; args = []; _ } -> (
    match stack with
    | arg :: ((_, Mprim { prim = `lambda; args = [_; ret]; _ }) as lambda) :: xs
      ->
      let$ new_cpt, top = new_stack_item name_cpt ret in
      let label_ret = label + 1 in
      let node = N_EXEC (arg, lambda, top, label, label_ret) in
      Ok (Some (top :: xs), [node], label_ret + 1, new_cpt)
    | _ -> Error (UnstackMicheline "Not a good stack for a EXEC instruction"))
  | Mprim { prim = `APPLY; args = []; _ } -> (
    match stack with
    | arg :: lambda :: xs ->
      let$ ty = Type_primitive.type_apply (snd arg) (snd lambda) in
      let$ new_cpt, top = new_stack_item name_cpt ty in
      (*let node = N_APPLY (arg, lambda, top) in*)
      let node = N_SIMPLE (`APPLY, [], [arg; lambda], [top], label) in
      Ok (Some (top :: xs), [node], label + 1, new_cpt)
    | _ -> Error (UnstackMicheline "Stack is too short apply"))
  | Mprim { prim = `LT; args = []; _ } ->
    let cons arg1 arg2 =
      N_SIMPLE (`LT, [], [arg1], [arg2], label)
      (*N_LT (arg1, arg2)*) in
    compile_eq_prim ~label ~name_cpt stack cons
  | Mprim { prim = `CONS; args = []; _ } -> (
    match stack with
    | x :: (arg, ty) :: xs ->
      let$ new_cpt, top = new_stack_item name_cpt ty in
      (*let node = N_CONS (x, (arg, ty), top) in*)
      let node = N_SIMPLE (`CONS, [], [x; (arg, ty)], [top], label) in
      Ok (Some (top :: xs), [node], label + 1, new_cpt)
    | _ -> Error (UnstackMicheline "Stack is too short cons"))
  | Mprim { prim = `ITER; args = [Mseq instr_l]; _ } ->
    let label_iter = label in
    let label_pat = label_iter + 1 in
    let cons (x, accumulator, pat_acc, pat, node_iter) =
      N_ITER
        {
          arg_iter = x;
          acc_iter = accumulator;
          pat_acc_iter = pat_acc;
          pat = (pat, label_pat);
          body_iter = node_iter;
          label_iter;
        } in
    collection_application ~full_script ~label_start:(label_pat + 1) ~name_cpt
      Type_primitive.type_arg_collection stack false instr_l cons
  | Mprim { prim = `SIZE; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let$ next_cpt, top = new_stack_item name_cpt (prim `nat) in
      (*let node = N_SIZE (x, top) in*)
      let node = N_SIMPLE (`SIZE, [], [x], [top], label) in
      Ok (Some (top :: xs), [node], label + 1, next_cpt))
  | Mprim { prim = `EMPTY_SET; args = [ty]; _ } ->
    let ty = prim `set ~args:[ty] in
    (*let node = N_EMPTY_SET (name, ty) in*)
    let$ new_cpt, top = new_stack_item name_cpt ty in
    let node = N_SIMPLE (`EMPTY_SET, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, new_cpt)
  | Mprim { prim = `EMPTY_MAP; args = [key; elt]; _ } ->
    let ty_top = prim `map ~args:[key; elt] in
    let$ new_cpt, top = new_stack_item name_cpt ty_top in
    (*let node = N_EMPTY_MAP top in*)
    let node = N_SIMPLE (`EMPTY_MAP, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, new_cpt)
  | Mprim { prim = `EMPTY_BIG_MAP; args = [key; elt]; _ } ->
    let ty_top = prim `big_map ~args:[key; elt] in
    let$ next_cpt, top = new_stack_item name_cpt ty_top in
    (*let node = N_EMPTY_BIG_MAP top in*)
    let node = N_SIMPLE (`EMPTY_BIG_MAP, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `MAP; args = [Mseq instr_l]; _ } ->
    map_on_collection ~full_script ~name_cpt ~label stack instr_l
  | Mprim { prim = `MEM; args = []; _ } ->
    let type_fun = Type_primitive.type_mem in
    let cons arg1 arg2 top =
      N_SIMPLE (`MEM, [], [arg1; arg2], [top], label)
      (*N_MEM (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `LOOP; args = [Mseq instr_l]; _ } ->
    decomp_code_loop ~full_script ~label ~name_cpt stack instr_l
  | Mprim { prim = `LOOP_LEFT; args = [Mseq instr_l]; _ } -> (
    match stack with
    | ((_, Mprim { prim = `or_; args = [left; right]; _ }) as x) :: xs ->
      let$ next_cpt, pat = new_stack_item name_cpt left in
      let$ new_stack, node_loop, label_ret, final_cpt =
        decompile_instr full_script ~label_init:label ~name_cpt:next_cpt
          ~loop_left:true ~init_stack:(pat :: xs) ~instr_l in
      loop_left_post ~label_lf:label_ret ~name_cpt:final_cpt x xs new_stack
        node_loop pat right
    | _ -> Error (UnstackMicheline "Stack is empty"))
  | Mprim { prim = `NEVER; args = []; _ } -> (
    match stack with
    | ((_, Mprim { prim = `never; _ }) as top) :: _ ->
      let node = N_NEVER (top, label) in
      Ok (None, [node], label + 1, name_cpt)
    | _ -> Error (UnstackMicheline "Wrong stack"))
  | Mprim { prim = `CAR; args = []; _ } ->
    let f name_cpt (ty, cdr) top =
      let$ next_cpt, res = new_stack_item name_cpt ty in
      let node =
        N_SIMPLE (`CAR, [cdr], [top], [res], label)
        (*N_CAR (top, res, cdr)*) in
      Ok (res, node, label + 1, next_cpt) in
    car_cdr name_cpt stack instr f
  | Mprim { prim = `CDR; args = []; _ } ->
    let f name_cpt (car, ty) top =
      let$ next_cpt, res = new_stack_item name_cpt ty in
      let node =
        N_SIMPLE (`CDR, [car], [top], [res], label)
        (*N_CDR (top, res, car)*) in
      Ok (res, node, label + 1, next_cpt) in
    car_cdr name_cpt stack instr f
  | Mprim { prim = `LEFT; args = [right]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack empty")
    | (name_top, left) :: xs ->
      let ty = prim `or_ ~args:[left; right] in
      let$ next_cpt, res = new_stack_item name_cpt ty in
      (*let node = N_LEFT ((name_top, left), res) in*)
      let node = N_SIMPLE (`LEFT, [right], [(name_top, left)], [res], label) in
      Ok (Some (res :: xs), [node], label + 1, next_cpt))
  | Mprim { prim = `RIGHT; args = [left]; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack empty")
    | (name_top, right) :: xs ->
      let ty = prim `or_ ~args:[left; right] in
      let$ next_cpt, res = new_stack_item name_cpt ty in
      let node =
        N_SIMPLE (`RIGHT, [left], [(name_top, right)], [res], label)
        (* N_RIGHT ((name_top, right), res)*) in
      Ok (Some (res :: xs), [node], label + 1, next_cpt))
  | Mprim { prim = `IF_LEFT; args = [Mseq instr_left; Mseq instr_right]; _ }
    -> (
    match stack with
    | ((_, Mprim { prim = `or_; args = [left; right]; _ }) as top) :: xs ->
      let$ next_cpt, pat_left = new_stack_item name_cpt left in
      let$ next_cpt, pat_right = new_stack_item next_cpt right in
      let label_then = label in
      let label_else = label_then + 1 in
      let label_left = label_else + 1 in
      let label_right = label_left + 1 in
      let res =
        michelson2node_if ~full_script ~label_arg:(label_right + 1)
          ~name_cpt:next_cpt
          (fun n_then n_else ->
            let left = (pat_left, label_left, n_then) in
            let right = (pat_right, label_right, n_else) in
            N_IF_LEFT (top, label_then, label_else, left, right))
          xs
          (instr_left, pat_left :: xs)
          (instr_right, pat_right :: xs) in
      res
    | _ -> Error (UnstackMicheline "Stack is empty"))
  | Mprim { prim = `GET; args = []; _ } -> (
    match stack with
    | x :: y :: xs ->
      let$ _, value = Type_primitive.value_map (snd y) in
      let opt_result = prim `option ~args:[value] in
      let$ next_cpt, top = new_stack_item name_cpt opt_result in
      (*let node = N_GET (x, y, top) in*)
      let node = N_SIMPLE (`GET, [], [x; y], [top], label) in
      Ok (Some (top :: xs), [node], label + 1, next_cpt)
    | _ -> Error (UnstackMicheline "Stack is too short GET"))
  | Mprim { prim = `GET; args = [Mint n]; _ } ->
    let type_fun = Type_primitive.type_gets n in
    let cons arg top =
      N_SIMPLE (`GET, [Mint n], [arg], [top], label)
      (*N_GETS (n, arg, top)*) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `UPDATE; args = []; _ } -> (
    match stack with
    | x :: xs ->
      let cons arg coll top =
        N_SIMPLE (`UPDATE, [], [x; arg; coll], [top], label)
        (*N_UPDATE (x, arg, coll, top)*) in
      let type_fun = Type_primitive.type_update in
      compile_binary ~label ~name_cpt xs cons type_fun
    | _ -> Error (UnstackMicheline "Not a good stack for a UPDATE instruction"))
  | Mprim { prim = `UPDATE; args = [Mint n]; _ } ->
    let type_fun = Type_primitive.type_updates n in
    let cons arg1 arg2 top =
      N_SIMPLE (`UPDATE, [Mint n], [arg1; arg2], [top], label)
      (*N_UPDATES (n, arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `FAILWITH; args = []; _ } -> (
    match stack with
    | x :: _ ->
      let node = N_FAILWITH (x, label) in
      Ok (None, [node], label + 1, name_cpt)
    | _ -> Error (UnstackMicheline "Stack is empty"))
  | Mprim { prim = `CAST; args = [_michety]; _ } ->
    Ok (Some stack, [], label, name_cpt)
  | Mprim { prim = `RENAME; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for RENAME instruction")
    | x :: xs -> Ok (Some (x :: xs), [], label, name_cpt))
  | Mprim { prim = `SLICE; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for SLICE instruction")
    | x :: xs ->
      let ty_f = Type_primitive.type_slice (snd x) in
      let cons elt1 elt2 res =
        N_SIMPLE (`SLICE, [], [x; elt1; elt2], [res], label) in
      compile_binary ~label ~name_cpt xs cons ty_f)
  | Mprim { prim = `PACK; args = []; _ } ->
    let type_fun _ = Ok (prim `bytes) in
    let cons arg1 top =
      N_SIMPLE (`PACK, [], [arg1], [top], label)
      (*N_PACK (arg1, top)*) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `UNPACK; args = [mty]; _ } ->
    let type_fun arg =
      match arg with
      | Mprim { prim = `bytes; _ } -> Ok (prim `option ~args:[mty])
      | _ ->
        Error
          (UnstackMicheline "UNPACK instruction is not used on a bytes value")
    in
    let cons arg1 top =
      N_SIMPLE (`UNPACK, [], [arg1], [top], label)
      (*N_UNPACK (arg1, top)*) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `ISNAT; args = []; _ } ->
    let type_fun _ = Type_primitive.make_option (prim `nat) in
    let cons arg1 top =
      N_SIMPLE (`ISNAT, [], [arg1], [top], label)
      (*N_ISNAT (arg1, top)*) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `INT; args = []; _ } ->
    let type_fun _ = Ok (prim `int) in
    let cons arg1 top =
      N_SIMPLE (`INT, [], [arg1], [top], label)
      (*N_INT (arg1, top)*) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `NEG; args = []; _ } ->
    let cons arg1 top =
      N_SIMPLE (`NEG, [], [arg1], [top], label)
      (*N_NEG (arg1, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.type_negativity cons
  | Mprim { prim = `LSL; args = []; _ } ->
    let type_fun _ _ = Ok (prim `nat) in
    let cons arg1 arg2 top =
      N_SIMPLE (`LSL, [], [arg1; arg2], [top], label)
      (*N_LSL (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `LSR; args = []; _ } ->
    let type_fun _ _ = Ok (prim `nat) in
    let cons arg1 arg2 top =
      N_SIMPLE (`LSR, [], [arg1; arg2], [top], label)
      (*N_LSR (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `OR; args = []; _ } ->
    let cons arg1 arg2 top =
      N_SIMPLE (`OR, [], [arg1; arg2], [top], label)
      (*N_OR (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons Type_primitive.type_or_xor
  | Mprim { prim = `XOR; args = []; _ } ->
    let cons arg1 arg2 top =
      N_SIMPLE (`XOR, [], [arg1; arg2], [top], label)
      (*N_XOR (arg1, arg2, top)*) in
    compile_binary ~label ~name_cpt stack cons Type_primitive.type_or_xor
  | Mprim { prim = `NOT; args = []; _ } ->
    let cons arg1 top =
      N_SIMPLE (`NOT, [], [arg1], [top], label)
      (*N_NOT (arg1, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.type_not cons
  | Mprim { prim = `NEQ; args = []; _ } ->
    let cons arg1 top =
      N_SIMPLE (`NEQ, [], [arg1], [top], label)
      (*N_NEQ (arg1, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.type_neq cons
  | Mprim { prim = `SELF; args = []; annots } -> (
    let se = full_script in
    let entry = Factori_utils.get_name_annots annots in
    let opt =
      match entry with
      | None -> Factori_utils.get_parameter_from_script_expr se
      | Some e ->
        let entrypoints =
          Factori_blockchain_utils.wrap_get_all_entrypoints_michelson_file se
        in
        let sanitized_name = Factori_utils.sanitized_of_str e in
        List.find_map
          (fun (key, ty) ->
            if Factori_utils.eq_sanitized sanitized_name key then
              Some ty
            else
              None)
          entrypoints in
    match opt with
    | None ->
      Error
        (UnstackMicheline "Couldn't find the entrypoint for instruction SELF")
    | Some ty ->
      let ty_top = prim `contract ~args:[ty] in
      let$ next_cpt, top = new_stack_item name_cpt ty_top in
      let node = N_SELF (entry, top, label) in
      Ok (Some (top :: stack), [node], label + 1, next_cpt))
  | Mprim { prim = `SELF_ADDRESS; args = []; _ } ->
    let ty_top = prim `address in
    let$ next_cpt, top = new_stack_item name_cpt ty_top in
    (*let node = N_SELF_ADDRESS top in*)
    let node = N_SIMPLE (`SELF_ADDRESS, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `CONTRACT; args = [ty]; annots } ->
    let type_fun = Type_primitive.type_contract ty in
    let cons arg1 top =
      N_CONTRACT (Factori_utils.get_name_annots annots, arg1, top, label) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `TRANSFER_TOKENS; args = []; annots = _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let type_fun = Type_primitive.type_transfer (snd x) in
      let cons arg1 arg2 top =
        N_SIMPLE (`TRANSFER_TOKENS, [], [x; arg1; arg2], [top], label)
        (* N_TRANSFER_TOKENS (x, arg1, arg2, top)*) in
      compile_binary ~label ~name_cpt xs cons type_fun)
  | Mprim { prim = `NOW; args = []; _ } ->
    let ty_top = prim `timestamp in
    let$ next_cpt, top = new_stack_item name_cpt ty_top in
    (*let node = N_NOW top in*)
    let node = N_SIMPLE (`NOW, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `LEVEL; args = []; _ } ->
    let$ next_cpt, top = new_stack_item name_cpt (prim `nat) in
    let node = N_SIMPLE (`LEVEL, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `AMOUNT; args = []; _ } ->
    let$ next_cpt, top = new_stack_item name_cpt (prim `mutez) in
    (*let node = N_AMOUNT top in*)
    let node = N_SIMPLE (`AMOUNT, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `BALANCE; args = []; _ } ->
    let$ next_cpt, top = new_stack_item name_cpt (prim `mutez) in
    (*let node = N_BALANCE top in*)
    let node = N_SIMPLE (`BALANCE, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `CHECK_SIGNATURE; args = []; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty")
    | x :: xs ->
      let type_fun = Type_primitive.type_check_sig (snd x) in
      let cons arg1 arg2 top =
        N_SIMPLE (`CHECK_SIGNATURE, [], [x; arg1; arg2], [top], label)
        (*N_CHECK_SIGNATURE (x, arg1, arg2, top)*) in
      compile_binary ~label ~name_cpt xs cons type_fun)
  | Mprim { prim = `SOURCE; args = []; _ } ->
    let ty_top = prim `address in
    let$ next_cpt, top = new_stack_item name_cpt ty_top in
    let node = N_SIMPLE (`SOURCE, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `SENDER; args = []; _ } ->
    let ty_top = prim `address in
    let$ next_cpt, top = new_stack_item name_cpt ty_top in
    let node = N_SIMPLE (`SENDER, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `ADDRESS; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`ADDRESS, [], [arg], [top], label)
      (*N_ADDRESS (arg, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.type_address cons
  | Mprim { prim = `CHAIN_ID; args = []; _ } ->
    let ty = prim `chain_id in
    let$ next_cpt, top = new_stack_item name_cpt ty in
    (*let node = N_CHAIN_ID top in*)
    let node = N_SIMPLE (`CHAIN_ID, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `TOTAL_VOTING_POWER; args = []; _ } ->
    let ty = prim `nat in
    let$ next_cpt, top = new_stack_item name_cpt ty in
    (*let node = N_TOTAL_VOTING_POWER top in*)
    let node = N_SIMPLE (`TOTAL_VOTING_POWER, [], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `PAIRING_CHECK; args = []; _ } ->
    let type_fun = Type_primitive.type_pairing_check in
    let cons arg top =
      N_SIMPLE (`PAIRING_CHECK, [], [arg], [top], label)
      (*N_PAIRING_CHECK (arg, top)*) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `SET_DELEGATE; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`SET_DELEGATE, [], [arg], [top], label)
      (*N_SET_DELEGATE (arg, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.type_set_delegate cons
  | Mprim { prim = `VOTING_POWER; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`VOTING_POWER, [], [arg], [top], label)
      (*N_VOTING_POWER (arg, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.voting_power cons
  | Mprim { prim = `BLAKE2B as p; args = []; _ }
  | Mprim { prim = `KECCAK as p; args = []; _ }
  | Mprim { prim = `SHA3 as p; args = []; _ }
  | Mprim { prim = `SHA256 as p; args = []; _ }
  | Mprim { prim = `SHA512 as p; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (p, [], [arg], [top], label)
      (*N_SHA3 (arg, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.type_bytes_trans cons
  | Mprim { prim = `HASH_KEY; args = []; _ } ->
    let cons arg top =
      N_SIMPLE (`HASH_KEY, [], [arg], [top], label)
      (*N_HASH_KEY (arg, top)*) in
    compile_unary ~label ~name_cpt stack Type_primitive.type_hash_k cons
  | Mprim { prim = `SAPLING_EMPTY_STATE; args = [Mint n]; _ } ->
    let ty = prim `sapling_state ~args:[Mint n] in
    let$ next_cpt, top = new_stack_item name_cpt ty in
    (*let node = N_SAPLING_EMPTY_STATE (n, top) in*)
    let node = N_SIMPLE (`SAPLING_EMPTY_STATE, [Mint n], [], [top], label) in
    Ok (Some (top :: stack), [node], label + 1, next_cpt)
  | Mprim { prim = `VIEW; args = [Mstring str; ty]; _ } ->
    let cons x y top = N_CALL_VIEW (str, x, y, top, label) in
    let type_fun = Type_primitive.type_call_view ty in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `GET_AND_UPDATE; args = []; _ } -> (
    match stack with
    | key :: _ :: coll :: _ ->
      let$ _, value = Type_primitive.value_map (snd coll) in
      let opt_result = prim `option ~args:[value] in
      let$ next_cpt, top = new_stack_item name_cpt opt_result in
      let node = N_SIMPLE (`GET, [], [key; coll], [top], label) in
      let cons arg coll top =
        N_SIMPLE (`UPDATE, [], [key; arg; coll], [top], label + 1)
        (*N_UPDATE (key, arg, coll, top)*) in
      let type_fun = Type_primitive.type_update in
      let$ stack, node_l, label, final_cpt =
        compile_binary ~label:(label + 1) ~name_cpt:next_cpt (List.tl stack)
          cons type_fun in
      Ok
        ( Option.map (fun stack -> top :: stack) stack,
          node :: node_l,
          label,
          final_cpt )
    | _ -> Error (UnstackMicheline "Not a good stack for GET_AND_UPDATE"))
  (* This should check that the storage of the contract is of the same type as
     the initial storage in the stack *)
  | Mprim { prim = `CREATE_CONTRACT; args = [contract]; _ } ->
    let$ contract, final_lab, final_cpt =
      decompile_smart ~label_start:(label + 1) ~cpt_start:name_cpt
        (Micheline contract) in
    let cons opt_k mutz ty2 res_l =
      match res_l with
      | [ope; addr] ->
        Ok (N_CREATE_CONTRACT (opt_k, mutz, ty2, contract, ope, addr, label))
      | _ ->
        Error
          (UnstackMicheline "Wrong result for a CREATE_CONTRACT instruction")
    in
    compile_triple ~label:(final_lab + 1) ~name_cpt:final_cpt stack cons
      Type_primitive.type_create_contract "CREATE_CONTRACT"
  | Mprim { prim = `IMPLICIT_ACCOUNT; _ } ->
    let type_fun = Type_primitive.implicit_acc in
    let cons arg top =
      N_SIMPLE (`IMPLICIT_ACCOUNT, [], [arg], [top], label)
      (*N_GETS (n, arg, top)*) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `SAPLING_VERIFY_UPDATE; _ } ->
    let type_fun = Type_primitive.type_sapling_verify_update in
    let cons arg1 arg2 top =
      N_SIMPLE (`SAPLING_VERIFY_UPDATE, [], [arg1; arg2], [top], label) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `TICKET; _ } ->
    let type_fun = Type_primitive.type_ticket in
    let cons arg1 arg2 top =
      N_SIMPLE (`TICKET, [], [arg1; arg2], [top], label) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `READ_TICKET; _ } ->
    let double_stack =
      match stack with
      | [] -> Error (UnstackMicheline "Stack is empty")
      | x :: xs -> Ok (x :: x :: xs) in
    Result.bind double_stack (fun stack ->
        let type_fun = Type_primitive.type_read_ticket in
        let cons arg top = N_SIMPLE (`READ_TICKET, [], [arg], [top], label) in
        compile_unary ~label ~name_cpt stack type_fun cons)
  | Mprim { prim = `SPLIT_TICKET; _ } ->
    let type_fun = Type_primitive.type_split_ticket in
    let cons arg1 arg2 top =
      N_SIMPLE (`SPLIT_TICKET, [], [arg1; arg2], [top], label) in
    compile_binary ~label ~name_cpt stack cons type_fun
  | Mprim { prim = `JOIN_TICKETS; _ } ->
    let type_fun = Type_primitive.type_join_ticket in
    let cons arg top = N_SIMPLE (`JOIN_TICKETS, [], [arg], [top], label) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `OPEN_CHEST; _ } -> (
    match stack with
    | [] -> Error (UnstackMicheline "Stack is empty for OPEN_CHEST instruction")
    | x :: xs ->
      let type_fun = Type_primitive.type_open_chest (snd x) in
      let cons arg1 arg2 top =
        N_SIMPLE (`OPEN_CHEST, [], [x; arg1; arg2], [top], label) in
      compile_binary ~label ~name_cpt xs cons type_fun)
  | Mprim { prim = `EMIT; args = [ty]; _ } ->
    let type_fun _ty = Ok (prim `operation) in
    let cons arg top = N_SIMPLE (`EMIT, [ty], [arg], [top], label) in
    compile_unary ~label ~name_cpt stack type_fun cons
  | Mprim { prim = `macro _; _ } ->
    Error
      (UnstackMicheline
         "For now, Michelson macro is not supported by Factori analysis")
  | e ->
    Error
      (UnstackMicheline
         (Format.sprintf "Unsupported primitive %s"
            (try Micheline_lib.string_of_micheline e with _ -> "")))

and decompile_view full_script ~name_cpt ~label_start ~storage ~name_view
    ~param_view ~return_view ~code_view =
  let ty = prim `pair ~args:[param_view; storage] in
  let$ next_cpt, top = new_stack_item name_cpt ty in
  let init_stack = [top] in
  let$ node_result =
    decompile_instr full_script ~stack_to_check:[] ~name_cpt:next_cpt
      ~label_init:(label_start + 1) ~loop_left:false ~init_stack
      ~instr_l:code_view in
  match node_result with
  | Some [_], node_l, label_end, final_cpt | None, node_l, label_end, final_cpt
    ->
    let view =
      {
        view_label = label_start;
        node_name = name_view;
        view_stack = top;
        view_parameter = param_view;
        view_result = return_view;
        node_code_v = node_l;
      } in
    Ok (view, label_end, final_cpt)
  | _ ->
    Error
      (UnstackMicheline
         (Format.sprintf
            "View %s does not return the right number of element on the stack"
            name_view))

and decompile_smart ?(label_start = 0) ?(cpt_start = 0) se =
  let storage = Factori_utils.get_storage_from_script_expr se in
  let parameter = Factori_utils.get_parameter_from_script_expr se in
  let code = Factori_utils.get_code_from_script_expr se in
  let views = Factori_utils.get_views_from_script_expr se in
  match (storage, parameter, code) with
  | Some store, Some param, Some (Mseq code) -> (
    let make_view_l views final cpt =
      List.fold_left
        (fun acc elt ->
          Result.bind acc (fun (acc, label_cpt, name_cpt) ->
              match elt with
              | [Mstring name_view; param_view; return_view; Mseq code_view] ->
                let$ view, label_cpt, final_name =
                  decompile_view se ~name_cpt ~label_start:label_cpt
                    ~storage:store ~name_view ~param_view ~return_view
                    ~code_view in
                Ok (view :: acc, label_cpt, final_name)
              | _ -> Error (UnstackMicheline "Ill-formed view")))
        (Ok ([], final, cpt))
        views in
    let ty_top = prim `pair ~args:[param; store] in
    let$ next_cpt, top = new_stack_item cpt_start ty_top in
    let stack = [top] in
    let$ node_result =
      decompile_instr se ~name_cpt:next_cpt ~label_init:(label_start + 1)
        ~loop_left:false ~init_stack:stack ~instr_l:code in
    match node_result with
    | Some [_], node_l, final_lab, final_cpt
    | None, node_l, final_lab, final_cpt ->
      let$ view_l, label_view, cpt_view =
        make_view_l views final_lab final_cpt in
      Ok
        ( {
            begin_label = label_start;
            begin_stack = top;
            node_l;
            parameter = param;
            storage = store;
            views_node = view_l;
          },
          label_view,
          cpt_view )
    | _ -> Error (UnstackMicheline "Wrong final stack for the code section"))
  | _, None, _ -> Error (UnstackMicheline "There is no field parameter")
  | None, _, _ -> Error (UnstackMicheline "There is no field storage")
  | _, _, None -> Error (UnstackMicheline "There is no code section")
  | _ -> Error (GenericError ("decompile_smart", "Unexpected michelson file"))
