(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines types and functions for generating and manipulating
OCaml files, including defining types, printing OCaml code, and adding modules,
fields, and tables to OCaml files. It also includes functions for reading and
writing OCaml files and their backup files. Warning: this short summary was
automatically generated and could be incomplete or misleading. If you feel this
is the case, please open an issue. *)

open Format
open Factori_errors
open Factori_config

type filename = string [@@deriving encoding]

type open_modules = string list [@@deriving encoding]

let print_open_module ppf om =
  fprintf ppf "%a"
    (pp_print_list
       ~pp_sep:(fun ppf _ -> fprintf ppf "\n")
       (fun ppf x -> fprintf ppf "open %s" (String.capitalize_ascii x)))
    om

type let_body_content =
  | StrContent of string [@wrap "StrContent"]
  | RecordSet of (string * string) list [@wrap "RecordSet"]
  | LetOpen of ((string * let_body_content)[@wrap "LetOpen"])
  | SeqContent of let_body_content list [@wrap "SeqContent"]
  | ListConcat of string list [@wrap "listConcat"]
  | ListContent of string list [@wrap "listContent"]
[@@deriving encoding { recursive }]

type let_body_element = {
  name : string;
  args : string list;
  type_ : string option;
  content : let_body_content;
}
[@@deriving encoding]

let rec print_lbc ppf = function
  | StrContent str -> pp_print_string ppf str
  | RecordSet l ->
    fprintf ppf "{%a}"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf ";\n")
         (fun ppf (f, v) -> fprintf ppf "%s = %s" f v))
      l
  | LetOpen (s, body) -> fprintf ppf "let open %s in\n%a" s print_lbc body
  | SeqContent l ->
    (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ";\n") print_lbc) ppf l
  | ListConcat l ->
    (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf "@@\n") pp_print_string)
      ppf l
  | ListContent l ->
    fprintf ppf "[%a]"
      (pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ";\n") pp_print_string)
      l

let print_lbe ppf = function
  | { name; args; type_; content } ->
    fprintf ppf "let %s %a%s=\n%a" name
      (pp_print_list
         ~pp_sep:(fun ppf _ -> fprintf ppf " ")
         (fun ppf arg -> fprintf ppf "%s" arg))
      args
      (match type_ with
      | Some t -> sprintf " : %s" t
      | None -> "")
      print_lbc content

type field_element = {
  field_name : string;
  field_type : string;
}
[@@deriving encoding]

type sum_element = {
  sum_elt_name : string;
  sum_elt_type : string;
}
[@@deriving encoding]

type record_type = {
  record_name : string;
  record_fields : field_element list;
  record_ppx : string option;
}
[@@deriving encoding]

type sum_type = {
  sum_name : string;
  sum_fields : sum_element list;
  sum_ppx : string option;
}
[@@deriving encoding]

type string_type = {
  string_name : string;
  string_value : string;
  string_ppx : string option;
}
[@@deriving encoding]

type type_element =
  | TypeRecord of record_type
  | TypeSum of sum_type
  | TypeString of string_type
[@@deriving encoding]

let print_type_record ppf r =
  (pp_print_list
     ~pp_sep:(fun ppf _ -> fprintf ppf ";")
     (fun ppf f -> fprintf ppf "%s : %s" f.field_name f.field_type))
    ppf r

let print_sum ppf s =
  (pp_print_list
     ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
     (fun ppf s -> fprintf ppf "| %s of %s" s.sum_elt_name s.sum_elt_type))
    ppf s

let pp_ppx ppf = function
  | None -> ()
  | Some s -> fprintf ppf "[@@@@deriving %s]" s

let print_type_element ppf = function
  | TypeRecord rc ->
    fprintf ppf "type %s = {%a}%a" rc.record_name print_type_record
      rc.record_fields pp_ppx rc.record_ppx
  | TypeSum s ->
    fprintf ppf "type %s = %a%a" s.sum_name print_sum s.sum_fields pp_ppx
      s.sum_ppx
  | TypeString s ->
    fprintf ppf "type %s = %s%a" s.string_name s.string_value pp_ppx
      s.string_ppx

type ocaml_body_element =
  | Any of string
  | Let of let_body_element
[@@deriving encoding]

let print_ocaml_body_element ppf = function
  | Any s -> fprintf ppf "%s" s
  | Let lbe -> print_lbe ppf lbe

let print_ocaml_body ppf =
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (fun ppf x -> print_ocaml_body_element ppf x)
    ppf

let print_types ppf =
  pp_print_list
    ~pp_sep:(fun ppf _ -> fprintf ppf "\n\n")
    (fun ppf x -> print_type_element ppf x)
    ppf

(* To be refined on an as-needed basis  *)
type ocaml_file = {
  open_modules : open_modules;
  types : type_element list;
  body : ocaml_body_element list;
}
[@@deriving encoding]

let empty_ocaml_file = { open_modules = []; types = []; body = [] }

let print_ocaml_file ppf of_ =
  fprintf ppf "%a\n%a\n%a%!" print_open_module of_.open_modules print_types
    of_.types print_ocaml_body of_.body

let add_module of_ module_name =
  {
    of_ with
    open_modules =
      (if List.mem module_name of_.open_modules then
         of_.open_modules
       else
         of_.open_modules @ [module_name]);
  }

let ocaml_file_type_contains ~ocaml_file ~field =
  let field_name = Format.sprintf "ci_%s" field.field_name in
  List.exists
    (fun t ->
      match t with
      | TypeRecord rc ->
        List.exists (fun f -> f.field_name = field_name) rc.record_fields
      | _ -> false)
    ocaml_file.types

let add_field_to_type ~ocaml_file ~type_name ~field =
  if ocaml_file_type_contains ~ocaml_file ~field then
    ocaml_file
  else
    {
      ocaml_file with
      types =
        List.map
          (fun t ->
            match t with
            | TypeRecord rc when rc.record_name = type_name ->
              let field =
                {
                  field with
                  field_name = Format.sprintf "ci_%s" field.field_name;
                } in
              let rc = { rc with record_fields = rc.record_fields @ [field] } in
              TypeRecord rc
            | _ -> t)
          ocaml_file.types;
    }

let ocaml_file_of_row_contains ~ocaml_file ~field =
  let f = Format.sprintf "ci_%s" field.field_name in
  List.exists
    (fun t ->
      match t with
      | Let l when l.name = "info_of_row" -> begin
        match l.content with
        | RecordSet s -> List.exists (fun (name, _) -> name = f) s
        | _ -> false
      end
      | _ -> false)
    ocaml_file.body

let add_field_to_of_row ~ocaml_file ~field =
  if ocaml_file_of_row_contains ~ocaml_file ~field then
    ocaml_file
  else
    {
      ocaml_file with
      body =
        List.map
          (fun t ->
            match t with
            | Let l when l.name = "info_of_row" ->
              Let
                {
                  l with
                  content =
                    (match l.content with
                    | RecordSet s ->
                      let f = Format.sprintf "ci_%s" field.field_name in
                      let v =
                        Printf.sprintf
                          "Option.bind r#%s (fun s -> Some (Z.of_string s))"
                          field.field_name in
                      RecordSet (s @ [(f, v)])
                    | _ -> failwith "wrong content on info_of_row");
                }
            | _ -> t)
          ocaml_file.body;
    }

let ocaml_file_track_contains ~ocaml_file ~field =
  let f = Format.sprintf "ci_%s" field.field_name in
  List.exists
    (fun t ->
      match t with
      | Let l when l.name = "track_contract" -> begin
        match l.content with
        | RecordSet s -> List.exists (fun (name, _) -> name = f) s
        | _ -> false
      end
      | _ -> false)
    ocaml_file.body

let add_field_to_track ~ocaml_file ~field =
  if ocaml_file_track_contains ~ocaml_file ~field then
    ocaml_file
  else
    {
      ocaml_file with
      body =
        List.map
          (fun t ->
            match t with
            | Let l when l.name = "track_contract" ->
              let f = Format.sprintf "ci_%s" field.field_name in
              Let
                {
                  l with
                  args = ("?" ^ f) :: l.args;
                  content =
                    (match l.content with
                    | RecordSet s ->
                      let v = f in
                      RecordSet (s @ [(f, v)])
                    | _ -> failwith "wrong content on info_of_row");
                }
            | _ -> t)
          ocaml_file.body;
    }

let ocaml_file_contains_plugin_register ~ocaml_file ~contract_name =
  let cnt =
    Printf.sprintf "Plugins.register_mod (module %s)"
      (String.capitalize_ascii
      @@ OCaml_SDK.crawlori_plugin_basename ~contract_name) in
  List.exists
    (function
      | Let l when l.name = "register_all_plugins" -> begin
        match l.content with
        | LetOpen (_, SeqContent l) ->
          List.exists (fun s -> s = StrContent cnt) l
        | _ -> false
      end
      | _ -> false)
    ocaml_file.body

let add_plugin_register_to_seq ~ocaml_file ~contract_name =
  if ocaml_file_contains_plugin_register ~ocaml_file ~contract_name then
    ocaml_file
  else
    {
      ocaml_file with
      body =
        List.map
          (fun t ->
            match t with
            | Let l when l.name = "register_all_plugins" ->
              Let
                {
                  l with
                  content =
                    (match l.content with
                    | LetOpen (s, SeqContent l) ->
                      let cnt =
                        Printf.sprintf "Plugins.register_mod (module %s)"
                          (String.capitalize_ascii
                          @@ OCaml_SDK.crawlori_plugin_basename ~contract_name)
                      in
                      LetOpen (s, SeqContent (l @ [StrContent cnt]))
                    | _ -> failwith "wrong content on register_all_plugins");
                }
            | _ -> t)
          ocaml_file.body;
    }

let remove_plugin_register_to_seq ~ocaml_file ~contract_name =
  if not @@ ocaml_file_contains_plugin_register ~ocaml_file ~contract_name then
    ocaml_file
  else
    {
      ocaml_file with
      body =
        List.map
          (fun t ->
            match t with
            | Let l when l.name = "register_all_plugins" ->
              Let
                {
                  l with
                  content =
                    (match l.content with
                    | LetOpen (s, SeqContent l) ->
                      let cnt =
                        Printf.sprintf "Plugins.register_mod (module %s)"
                          (String.capitalize_ascii
                          @@ OCaml_SDK.crawlori_plugin_basename ~contract_name)
                      in
                      LetOpen
                        ( s,
                          SeqContent
                            (List.filter
                               (function
                                 | StrContent content when cnt = content ->
                                   false
                                 | _ -> true)
                               l) )
                    | _ -> failwith "wrong content on register_all_plugins");
                }
            | _ -> t)
          ocaml_file.body;
    }

let ocaml_file_contains_tables_upgrade_downgrade ~ocaml_file ~contract_name =
  let tables_upgrade =
    Printf.sprintf "%s.upgrade"
      (String.capitalize_ascii
      @@ OCaml_SDK.crawlori_tables_basename ~contract_name) in
  let tables_downgrade =
    Printf.sprintf "%s.downgrade"
      (String.capitalize_ascii
      @@ OCaml_SDK.crawlori_tables_basename ~contract_name) in
  let update = Printf.sprintf "(0, (%s, %s))" tables_upgrade tables_downgrade in
  List.exists
    (function
      | Let l when l.name = "all" -> begin
        match l.content with
        | ListContent l -> List.exists (fun s -> s = update) l
        | _ -> false
      end
      | _ -> false)
    ocaml_file.body

let remove_tables_to_upgrade_downgrade ~ocaml_file ~contract_name =
  if
    not
    @@ ocaml_file_contains_tables_upgrade_downgrade ~ocaml_file ~contract_name
  then
    ocaml_file
  else
    {
      ocaml_file with
      body =
        List.map
          (fun t ->
            match t with
            | Let l when l.name = "all" ->
              Let
                {
                  l with
                  content =
                    (match l.content with
                    | ListContent l ->
                      let tables_upgrade =
                        Printf.sprintf "%s.upgrade"
                          (String.capitalize_ascii
                          @@ OCaml_SDK.crawlori_tables_basename ~contract_name)
                      in
                      let tables_downgrade =
                        Printf.sprintf "%s.downgrade"
                          (String.capitalize_ascii
                          @@ OCaml_SDK.crawlori_tables_basename ~contract_name)
                      in
                      let update =
                        Printf.sprintf "(0, (%s, %s))" tables_upgrade
                          tables_downgrade in
                      ListContent (List.filter (fun s -> s <> update) l)
                    | _ -> failwith "wrong content upgrade");
                }
            | _ -> t)
          ocaml_file.body;
    }

let add_tables_to_upgrade_downgrade ~ocaml_file ~contract_name =
  if ocaml_file_contains_tables_upgrade_downgrade ~ocaml_file ~contract_name
  then
    ocaml_file
  else
    {
      ocaml_file with
      body =
        List.map
          (fun t ->
            match t with
            | Let l when l.name = "all" ->
              Let
                {
                  l with
                  content =
                    (match l.content with
                    | ListContent l ->
                      let tables_upgrade =
                        Printf.sprintf "%s.upgrade"
                          (String.capitalize_ascii
                          @@ OCaml_SDK.crawlori_tables_basename ~contract_name)
                      in
                      let tables_downgrade =
                        Printf.sprintf "%s.downgrade"
                          (String.capitalize_ascii
                          @@ OCaml_SDK.crawlori_tables_basename ~contract_name)
                      in
                      let update =
                        Printf.sprintf "(0, (%s, %s))" tables_upgrade
                          tables_downgrade in
                      ListContent (l @ [update])
                    | _ -> failwith "wrong content upgrade");
                }
            | _ -> t)
          ocaml_file.body;
    }

let ocaml_file_contains ocamlfile element =
  match element with
  | Any s ->
    List.exists
      (function
        | Any si -> Factori_utils.contains si s
        | Let _ -> false)
      ocamlfile.body
  | Let { name = x; _ } ->
    List.exists
      (function
        | Let { name = xi; _ } -> xi = x
        | _ -> false)
      ocamlfile.body

let append_to_body of_ new_body_chunk =
  if ocaml_file_contains of_ new_body_chunk then
    of_
  else
    { of_ with body = of_.body @ [new_body_chunk] }

(* let read_ocaml_file ?(check_if_already_there=true) ~filename () = *)

let read_backup_file ?(check_if_already_there = true) ~filename () =
  let backup_file = Factori_utils.get_backup_file_name ~filename in
  if not (Sys.file_exists backup_file) then
    if check_if_already_there then
      failwith
      @@ sprintf
           "[decode_ocaml_file] trying to decode a non-existing backup file %s"
           backup_file
    else
      empty_ocaml_file
  else
    try
      Factori_utils.deserialize_file ~silent:false ~filename:backup_file
        ~enc:ocaml_file_enc
    with CouldNotDestruct _ | CouldNotReadAsJson _ ->
      Factori_utils.output_verbose
        "Could not deserialize file, will behave as if it were not present" ;
      empty_ocaml_file

let read_enc_backup_file ?(check_if_already_there = true) ~filename ~enc () =
  let backup_file = Factori_utils.get_backup_file_name ~filename in
  if not (Sys.file_exists backup_file) then
    if check_if_already_there then
      failwith
      @@ sprintf
           "[decode_ocaml_file] trying to decode a non-existing backup file %s"
           backup_file
    else
      None
  else
    try
      Some
        (Factori_utils.deserialize_file ~silent:false ~filename:backup_file ~enc)
    with CouldNotDestruct _ | CouldNotReadAsJson _ ->
      Factori_utils.output_verbose
        "Could not deserialize file, will behave as if it were not present" ;
      None

(** Take the filename of the ocaml file, and write the backup file by
   its side *)
let write_backup_file ~ocaml_file ~filename =
  let backup_filename = Factori_utils.get_backup_file_name ~filename in
  Factori_utils.serialize_file ~filename:backup_filename ~enc:ocaml_file_enc
    ocaml_file

(** Write an OCaml file <path/filename.ml> and, by its side, write
   <path/filename.json> which contains a json encoded version of the OCaml
   file, making it easy to read and update it. If with_readable_backup
   is set to false, then no json file is written *)
let write_ocaml_file ?(with_readable_backup = true) ~ocaml_file ~path () =
  Factori_utils.write_file ~filename:path
    (asprintf "%a" print_ocaml_file ocaml_file) ;
  if with_readable_backup then write_backup_file ~ocaml_file ~filename:path

let add_modules_to_ocaml_file ?(file_must_exist = true) ~path module_names =
  let ocaml_file =
    read_backup_file ~check_if_already_there:file_must_exist ~filename:path ()
  in
  let new_ocaml_file =
    List.fold_right
      (fun modname of_ -> add_module of_ modname)
      module_names ocaml_file in
  write_ocaml_file ~ocaml_file:new_ocaml_file ~with_readable_backup:true ~path
    ()

let append_body_to_ocaml_file ?(file_must_exist = true) ~path body =
  let ocaml_file =
    read_backup_file ~check_if_already_there:file_must_exist ~filename:path ()
  in
  let new_ocaml_file = append_to_body ocaml_file body in
  write_ocaml_file ~ocaml_file:new_ocaml_file ~with_readable_backup:true ~path
    ()
