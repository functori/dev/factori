(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This file contains data types and functions related to control flow graphs
and their nodes in the Michelson language used in Tezos blockchain. It defines
several types and functions related to Michelson instructions, and some
functions expect specific input types. A more detailed understanding of the
Michelson language is required to effectively use and understand these
functions. Warning: this short summary was automatically generated and could be
incomplete or misleading. If you feel this is the case, please open an issue. *)

open Tzfunc.Proto
open Factori_errors
open Factori_micheline_lib

let pair_or_simple = function
  | [] -> Error (UnstackMicheline "Not enough element for a type pair")
  | [solo] -> Ok solo
  | args -> Ok (Mprim { prim = `pair; args; annots = [] })

let make_option miche =
  Ok (Mprim { prim = `option; args = [miche]; annots = [] })

let make_lambda arg ret =
  Ok (Mprim { prim = `lambda; args = [arg; ret]; annots = [] })

let rec type_gets n ty =
  if Z.equal n Z.zero then
    Ok ty
  else
    match ty with
    | Mprim { prim = `pair; args = x :: _; _ } when Z.equal n Z.one -> Ok x
    | Mprim { prim = `pair; args = _ :: sub_miche_l; _ } ->
      let new_n = Z.sub n (Z.of_int 2) in
      Result.bind (pair_or_simple sub_miche_l) (type_gets new_n)
    | _ -> Error (UnstackMicheline "Not good for a GET n instruction")

let is_lambda (m : micheline) : bool =
  match m with
  | Mprim { prim = `lambda; _ } -> true
  | _ -> false

let type_updates n new_ty ty =
  let rec aux_update acc n ty =
    if Z.equal n Z.zero then
      pair_or_simple (List.rev_append acc [ty])
    else
      match ty with
      | Mprim { prim = `pair; args = _ :: sub_miche_l; _ } when Z.equal n Z.one
        -> pair_or_simple (List.rev_append acc (new_ty :: sub_miche_l))
      | Mprim { prim = `pair; args = x :: sub_miche_l; _ } ->
        let new_n = Z.sub n (Z.of_int 2) in
        Result.bind (pair_or_simple sub_miche_l) (aux_update (x :: acc) new_n)
      | _ -> Error (UnstackMicheline "Not good for a UPDATE n instruction")
  in
  aux_update [] n ty

let type_arg_collection ty =
  match ty with
  | Mprim { prim = `list; args; _ }
  | Mprim { prim = `set; args; _ }
  | Mprim { prim = `map; args; _ } -> pair_or_simple args
  | _ -> Error (UnstackMicheline "Not an iterable type")

let type_arg_map ty =
  match ty with
  | Mprim { prim = `list; args; _ }
  | Mprim { prim = `set; args; _ }
  | Mprim { prim = `map; args; _ }
  | Mprim { prim = `option; args; _ } -> pair_or_simple args
  | _ -> Error (UnstackMicheline "Not a mappable type")

let type_result_map ty =
  match ty with
  | Mprim { prim = `list; args = [ty]; _ }
  | Mprim { prim = `set; args = [ty]; _ }
  | Mprim { prim = `map; args = [_; ty]; _ }
  | Mprim { prim = `option; args = [ty]; _ } -> Ok ty
  | _ -> Error (UnstackMicheline "Not a mappable type")

let type_map arg1 arg2 =
  match (arg1, arg2) with
  | new_value, Mprim { prim = `map; args = [k; _]; _ } ->
    Ok (Mprim { prim = `map; args = [k; new_value]; annots = [] })
  | _, Mprim { prim = `list; _ } ->
    Ok (Mprim { prim = `list; args = [arg1]; annots = [] })
  | _, Mprim { prim = `option; _ } ->
    Ok (Mprim { prim = `option; args = [arg1]; annots = [] })
  | _ -> Error (UnstackMicheline "Wrong stack for a MAP instruction")

let type_sapling_verify_update arg1 arg2 =
  match (arg1, arg2) with
  | ( Mprim { prim = `sapling_transaction; args = [Mint m]; _ },
      Mprim { prim = `sapling_state; args = [Mint n]; _ } )
    when m = n ->
    let state = prim `sapling_state ~args:[Mint n] in
    let integer = prim `int in
    let pair = prim `pair ~args:[integer; state] in
    let bytes = prim `bytes in
    let pair2 = prim `pair ~args:[bytes; pair] in
    make_option pair2
  | _ ->
    Error (UnstackMicheline "Wrong stack for SAPLING_VERIFY_UPDATE instruction")

let type_ticket arg1 arg2 =
  match (arg1, arg2) with
  | cty, Mprim { prim = `nat; args = []; _ } -> Ok (prim `ticket ~args:[cty])
  | _ -> Error (UnstackMicheline "Wrong stack for TICKET instruction")

let type_read_ticket arg =
  match arg with
  | Mprim { prim = `ticket; args = [cty]; _ } ->
    let addr = prim `address in
    let nat = prim `nat in
    let pair = prim `pair ~args:[addr; cty; nat] in
    Ok pair
  | _ -> Error (UnstackMicheline "Wrong stack for READ_TICKET instruction")

let type_create_contract opt_k mut ty2 =
  match (opt_k, mut, ty2) with
  | ( Mprim { prim = `option; args = [Mprim { prim = `key_hash; _ }]; _ },
      Mprim { prim = `mutez; args = []; _ },
      _ty2 ) ->
    let operation = prim `operation in
    let addr = prim `address in
    Ok [operation; addr]
  | _ -> Error (UnstackMicheline "Wrong stack for CREATE_CONTRACT instruction")

let type_split_ticket arg1 arg2 =
  let stack_error =
    Error (UnstackMicheline "Wrong stack for SPLIT_TICKET_INSTRUCTION") in
  match (arg1, arg2) with
  | Mprim { prim = `ticket; args = [cty]; _ }, Mprim { prim = `pair; args; _ }
    -> (
    match args with
    | [Mprim { prim = `nat; args = []; _ }; Mprim { prim = `nat; args = []; _ }]
      ->
      let ticket = prim `ticket ~args:[cty] in
      let pair = prim `pair ~args:[ticket; ticket] in
      make_option pair
    | _ -> stack_error)
  | _ -> stack_error

(* Need to check that cty1 is equal to cty2 (just on structure side not the annotation) *)
let type_join_ticket arg =
  match arg with
  | Mprim
      {
        prim = `pair;
        args =
          [
            Mprim { prim = `ticket; args = [cty1]; _ };
            Mprim { prim = `ticket; args = [_cty2]; _ };
          ];
        _;
      } ->
    let ticket = prim `ticket ~args:[cty1] in
    make_option ticket
  | _ -> Error (UnstackMicheline "Wrong stack for JOIN_TICKETS instruction")

let type_open_chest arg1 arg2 arg3 =
  match (arg1, arg2, arg3) with
  | ( Mprim { prim = `chest_key; args = []; _ },
      Mprim { prim = `chest; args = []; _ },
      Mprim { prim = `nat; args = []; _ } ) ->
    let bytes = prim `bytes in
    let bool = prim `bool in
    let or_res = prim `or_ ~args:[bytes; bool] in
    Ok or_res
  | _ -> Error (UnstackMicheline "Wrong stack for OPEN_CHEST instruction")

let split_pair_at n pair =
  let unpair_fun = function
    | Mprim { prim = `pair; args = ty_l; _ } -> Ok ty_l
    | e ->
      Error
        (UnstackMicheline
           (Format.sprintf "%s is not a pair"
              (Micheline_lib.string_of_micheline e))) in
  Factori_utils.split_pair_at_general n unpair_fun pair_or_simple pair

(*
  let get_entrypoint annot michety =
    let rec aux_or miche_l =
      match miche_l with
      | [] -> None
      | e :: xs -> (
        let recup_annot = field_opt (get_annot e) in
        match recup_annot with
        | Some name when name = annot -> Some e
        | _ -> (
          match uncompute_or_opt e with
          | None -> aux_or xs
          | Some (l, r) -> aux_or (l :: r :: xs))) in
    aux_or [michety]*)

let type_update ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `set; args = [_]; _ } -> Ok ty2
  | ( Mprim { prim = `option; args = [_]; _ },
      Mprim { prim = `map; args = [_; _]; _ } ) -> Ok ty2
  | ( Mprim { prim = `option; args = [_]; _ },
      Mprim { prim = `big_map; args = [_; _]; _ } ) -> Ok ty2
  | _ -> Error (UnstackMicheline "Wrong type for an UPDATE instruction")

let type_or arg1 arg2 =
  match (arg1, arg2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Wrong stack for or instruction")

let type_address ty1 =
  match ty1 with
  | Mprim { prim = `contract; _ } ->
    Ok (Mprim { prim = `address; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for address instruction")

let is_option ty =
  match ty with
  | Mprim { prim = `option; args = [arg]; _ } -> Some arg
  | _ -> None

let type_add ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `timestamp; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `timestamp; _ } ->
    Ok (Mprim { prim = `timestamp; args = []; annots = [] })
  | Mprim { prim = `mutez; _ }, Mprim { prim = `mutez; _ } ->
    Ok (Mprim { prim = `mutez; args = []; annots = [] })
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `bls12_381_fr; _ } ->
    Ok (Mprim { prim = `bls12_381_fr; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g1; _ }, Mprim { prim = `bls12_381_g1; _ } ->
    Ok (Mprim { prim = `bls12_381_g1; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g2; _ }, Mprim { prim = `bls12_381_g2; _ } ->
    Ok (Mprim { prim = `bls12_381_g2; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an addition")

let type_sub ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `timestamp; _ }, Mprim { prim = `timestamp; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `timestamp; _ }, Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `timestamp; args = []; annots = [] })
  | Mprim { prim = `mutez; args = []; _ }, Mprim { prim = `mutez; args = []; _ }
    -> Ok (prim `mutez)
  | _ ->
    Error
      (UnstackMicheline
         (Format.sprintf "Bad type for a substraction %s %s"
            (Micheline_lib.string_of_micheline ty1)
            (Micheline_lib.string_of_micheline ty2)))

let type_slice ty ty1 ty2 =
  match (ty, ty1, ty2) with
  | ( Mprim { prim = `nat; _ },
      Mprim { prim = `nat; _ },
      Mprim { prim = `string; _ } )
  | ( Mprim { prim = `nat; _ },
      Mprim { prim = `nat; _ },
      Mprim { prim = `bytes; _ } ) -> Ok (Proto.prim `option ~args:[ty2])
  | _ -> Error (UnstackMicheline "Bad type for a slice instruction")

let type_sub_mutez ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `mutez; _ }, Mprim { prim = `mutez; _ } ->
    make_option (Mprim { prim = `mutez; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for a mutez substraction")

let type_mul ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `mutez; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `nat; _ }, Mprim { prim = `mutez; _ } ->
    Ok (Mprim { prim = `mutez; args = []; annots = [] })
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `bls12_381_fr; _ }
  | Mprim { prim = `nat; _ }, Mprim { prim = `bls12_381_fr; _ }
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `bls12_381_fr; _ }, Mprim { prim = `int; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `bls12_381_fr; _ } ->
    Ok (Mprim { prim = `bls12_381_fr; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g1; _ }, Mprim { prim = `bls12_381_g1; _ } ->
    Ok (Mprim { prim = `bls12_381_g1; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g2; _ }, Mprim { prim = `bls12_381_g2; _ } ->
    Ok (Mprim { prim = `bls12_381_g2; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for a MUL instruction")

(* let concat (ty1,_) (ty2,_) =
   let ty_desc =
     match (ty1,ty2) with
     |String,String -> *)

let type_ediv ty1 ty2 =
  let type_sub_ediv ty1 ty2 =
    match (ty1, ty2) with
    | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
      Ok
        [
          Mprim { prim = `nat; args = []; annots = [] };
          Mprim { prim = `nat; args = []; annots = [] };
        ]
    | Mprim { prim = `nat; _ }, Mprim { prim = `int; _ }
    | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ }
    | Mprim { prim = `int; _ }, Mprim { prim = `int; _ } ->
      Ok
        [
          Mprim { prim = `int; args = []; annots = [] };
          Mprim { prim = `nat; args = []; annots = [] };
        ]
    | Mprim { prim = `mutez; _ }, Mprim { prim = `nat; _ } ->
      Ok
        [
          Mprim { prim = `mutez; args = []; annots = [] };
          Mprim { prim = `mutez; args = []; annots = [] };
        ]
    | Mprim { prim = `mutez; _ }, Mprim { prim = `mutez; _ } ->
      Ok
        [
          Mprim { prim = `nat; args = []; annots = [] };
          Mprim { prim = `mutez; args = []; annots = [] };
        ]
    | _ -> Error (UnstackMicheline "Bad type for an euclidean division") in
  let ty_l = type_sub_ediv ty1 ty2 in
  let pair = Result.bind ty_l pair_or_simple in
  Result.bind pair make_option

let type_and ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ }
  | Mprim { prim = `int; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an and")

(*let type_and ty1 ty2 =
    match (ty1, ty2) with
    | Bool, Bool -> Bool
    | Nat, Nat | Int, Nat -> Nat
    | _ -> failwith "Bad type for an and"*)

let type_not ty1 =
  match ty1 with
  | Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ } | Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an not")

let type_neq ty1 =
  match ty1 with
  | Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for not equal")

let type_or_xor ty1 ty2 =
  match (ty1, ty2) with
  | Mprim { prim = `bool; _ }, Mprim { prim = `bool; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | Mprim { prim = `nat; _ }, Mprim { prim = `nat; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Bad type for an or/xor")

(*let type_not ty1 =
      match ty1 with
      | Bool -> Bool
      | Nat | Int -> Int
      | _ -> failwith "Bad type for an not"

  let type_neq ty1 =
    match ty1 with
    | Int -> (Bool, [])
    | _ -> failwith "Bad type for not equal"

  let type_or_xor (ty1) (ty2) =
      match (ty1, ty2) with
      | Bool, Bool -> Bool
      | Nat, Nat -> Nat
      | _ -> failwith "Bad type for an or/xor"*)
let uncompute_pair = function
  | Mprim { prim = `pair; args; _ } -> Ok args
  | _ ->
    Error (UnstackMicheline "Trying to uncompute a type that is not a pair")

let sub_apply _ ret ty_l =
  match ty_l with
  | [] -> Error (UnstackMicheline "Error in apply")
  | _ :: xs ->
    let pair = pair_or_simple xs in
    Result.bind pair (fun arg -> make_lambda arg ret)

let type_apply arg lambda =
  match lambda with
  | Mprim { prim = `lambda; args = [ty_arg; ret]; _ } ->
    let ty_l = uncompute_pair ty_arg in
    Result.bind ty_l (sub_apply arg ret)
  | _ -> Error (UnstackMicheline "Not a lambda for an APPLY instruction")

let type_sub_concat arg1 =
  (* difference beetwen concat and concat_list is done elsewhere *)
  match arg1 with
  | Mprim { prim = `string; _ } ->
    Ok (Mprim { prim = `string; args = []; annots = [] })
  | Mprim { prim = `bytes; _ } ->
    Ok (Mprim { prim = `bytes; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Type mismatch for CONCAT instruction")

let type_concat arg1 arg2 =
  (* difference beetwen concat and concat_list is done elsewhere *)
  match (arg1, arg2) with
  | Mprim { prim = `string; _ }, Mprim { prim = `string; _ } ->
    Ok (Mprim { prim = `string; args = []; annots = [] })
  | Mprim { prim = `bytes; _ }, Mprim { prim = `bytes; _ } ->
    Ok (Mprim { prim = `bytes; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Type mismatch for CONCAT instruction")

let type_negativity arg1 =
  match arg1 with
  | Mprim { prim = `nat; _ } | Mprim { prim = `int; _ } ->
    Ok (Mprim { prim = `int; args = []; annots = [] })
  | Mprim { prim = `bls12_381_fr; _ } ->
    Ok (Mprim { prim = `bls12_381_fr; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g1; _ } ->
    Ok (Mprim { prim = `bls12_381_g1; args = []; annots = [] })
  | Mprim { prim = `bls12_381_g2; _ } ->
    Ok (Mprim { prim = `bls12_381_g2; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not good type for NEG instruction")

let type_contract ty arg1 =
  match arg1 with
  | Mprim { prim = `address; _ } ->
    make_option (Mprim { prim = `contract; args = [ty]; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for contract")

let type_transfer _arg1 arg2 arg3 =
  match (arg2, arg3) with
  | Mprim { prim = `mutez; _ }, Mprim { prim = `contract; args = [_ty]; _ } ->
    Ok (Mprim { prim = `operation; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not good types for a transfer operation")

let type_mem _arg1 arg2 =
  match arg2 with
  | Mprim { prim = `set; args = [_ty]; _ }
  | Mprim { prim = `map; args = [_ty; _]; _ }
  | Mprim { prim = `big_map; args = [_ty; _]; _ } ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for a MEM instruction")

let type_check_sig arg1 arg2 arg3 =
  match (arg1, arg2, arg3) with
  | ( Mprim { prim = `key; _ },
      Mprim { prim = `signature; _ },
      Mprim { prim = `bytes; _ } ) ->
    Ok (Mprim { prim = `bool; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for check_signature")

let type_pairing_check arg =
  let sub_pairing = function
    | [Mprim { prim = `bls12_381_g1; _ }; Mprim { prim = `bls12_381_g2; _ }] ->
      Ok (Mprim { prim = `bool; args = []; annots = [] })
    | _ ->
      Error (UnstackMicheline "Not good types for instruction PAIRING_CHECK")
  in
  match arg with
  | Mprim { prim = `list; args = [arg_ty]; _ } ->
    let pair = uncompute_pair arg_ty in
    Result.bind pair sub_pairing
  | _ -> failwith "Not a good type for pairing check"

let type_set_delegate arg =
  match arg with
  | Mprim { prim = `option; args = [Mprim { prim = `key_hash; _ }]; _ } ->
    Ok (Mprim { prim = `operation; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for set delegate instruction")

let type_bytes_trans arg =
  match arg with
  | Mprim { prim = `bytes; _ } ->
    Ok (Mprim { prim = `bytes; args = []; annots = [] })
  | _ ->
    Error (UnstackMicheline "Bytes transformation functions expect a bytes")

let voting_power arg =
  match arg with
  | Mprim { prim = `key_hash; _ } ->
    Ok (Mprim { prim = `nat; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for voting power")

let type_hash_k arg =
  match arg with
  | Mprim { prim = `key; _ } ->
    Ok (Mprim { prim = `key_hash; args = []; annots = [] })
  | _ -> Error (UnstackMicheline "Not a good type for HASH_KEY instruction")

let value_map arg =
  match arg with
  | Mprim { prim = `big_map; args = [key; value]; _ }
  | Mprim { prim = `map; args = [key; value]; _ } -> Ok (key, value)
  | _ -> Error (UnstackMicheline "Expected a map or a big_map")

let implicit_acc ty =
  match ty with
  | Mprim { prim = `key_hash; _ } ->
    let unit = prim `unit in
    Ok (prim `contract ~args:[unit])
  | _ ->
    Error (UnstackMicheline "Not a good type for IMPLICIT_ACCOUNT instruction")

let type_call_view ty _ arg2 =
  match arg2 with
  | Mprim { prim = `address; _ } -> make_option ty
  | _ -> Error (UnstackMicheline "Not a good type for CALL_VIEW instruction")
