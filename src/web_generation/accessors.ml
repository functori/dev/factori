(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file defines three types: `accessors`, `accessor_type`, and `pp`,
which are used to represent a path to a specific field or index in a data
structure and specify the format of the output when printing an accessor. The
file also includes several convenience functions for printing accessors in
different formats. Warning: this short summary was automatically generated and
could be incomplete or misleading. If you feel this is the case, please open an
issue. *)

open Factori_file

type accessors =
  | Field of string
  | Index of int

type accessor_type =
  | TS
  | HTML
  | FUNCTION

let pp fmt (acc, a) =
  if acc = [] then
    ()
  else
    let inversed_acc = List.rev acc in
    let root = List.hd inversed_acc in
    begin
      match root with
      | Field s -> File.write fmt "%s" s
      | Index _ -> File.write fmt "Index"
    end ;
    List.iter
      (fun fi ->
        match fi with
        | Field s -> begin
          match a with
          | TS -> File.write fmt ".%s" s
          | HTML -> File.write fmt "-%s" s
          | FUNCTION -> File.write fmt "_%s" s
        end
        | Index i -> begin
          match a with
          | TS -> File.write fmt "[%d]" i
          | HTML -> File.write fmt "-%d" i
          | FUNCTION -> File.write fmt "_%d" i
        end)
      (List.tl inversed_acc)

let pp_ts fmt acc = pp fmt (acc, TS)

let pp_html fmt acc =
  let escape s =
    let s = Str.global_replace (Str.regexp_string ".") "-" s in
    Str.global_replace (Str.regexp_string "_") "-" s in
  let escaped =
    List.map
      (fun a ->
        match a with
        | Field s -> Field (escape s)
        | Index _ -> a)
      acc in
  pp fmt (escaped, HTML)

let pp_fun fmt acc = pp fmt (acc, FUNCTION)
