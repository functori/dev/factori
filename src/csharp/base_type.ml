(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains functions that generate C# code for testing the
Tezos client against various input types, including base, unary, and binary
types. It depends on the `Types` module and includes functions for generating
random and constant generators for different types. Warning: this short summary
was automatically generated and could be incomplete or misleading. If you feel
this is the case, please open an issue. *)

open Types

let all_base =
  [
    Never;
    String;
    Nat;
    Int;
    Byte;
    Address;
    Signature;
    Unit;
    Bool;
    Timestamp;
    Keyhash;
    Key;
    Mutez;
    Operation;
    Chain_id;
    Sapling_state;
    Sapling_transaction_deprecated;
  ]

let all_unary = [Set; List; Option; Ticket]

let all_binary = [Map; BigMap; Lambda]

let special_prim = [Int; Bool; String; Byte]

let generate_all_kinds (* tuple_size_min tuple_size_max *) =
  [AContract]
  @ List.map (fun b -> ABase b) all_base
  @ List.map (fun b -> AUnary b) all_unary
  @ List.map (fun b -> ABinary b) all_binary

let str_of_lib_micheline s =
  match s with
  | Cstring -> Some "String"
  | Cint -> Some "Int"
  | Cbytes -> Some "Bytes"
  | _ -> None
