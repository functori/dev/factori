
from importlib.metadata import metadata
import token_python_interface
import blockchain

def exec():
    kt1 = token_python_interface.deploy(
        _from=blockchain.alice,
        initial_storage= token_python_interface._storage_generator(),
        amount=10000,
        network="flextesa",
        debug=False,
    )
    print("Deploy operation of KT1 token " + str(kt1) + " sent to a node.")
    return kt1
