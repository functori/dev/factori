(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** The `Context` module in OCaml provides a context for an Abstract Syntax Tree
(AST) with extra information to make it easier to linearize it, and it exports
various functions for manipulating and performing operations on sequences. Two
functions `collect_contract_names` and `collect_paying_addresses` are also
defined to perform specific operations on a sequence. No examples are provided.
Warning: this short summary was automatically generated and could be incomplete
or misleading. If you feel this is the case, please open an issue. *)

open Graph
open Format

module type CONTEXT = sig
  type context

  type u_id = Scenario_value.u_id

  type value = u_id Scenario_value.value [@@deriving show]

  val print_context : formatter -> context -> unit

  val init_context : unit -> context

  val add_node : context -> u_id -> value -> context

  val fresh_id : context -> u_id * context

  val build_cst : ?name:string option -> context -> value -> u_id * context

  val set_value : ?name:string option -> context -> value -> u_id * context

  val get_value : context -> u_id -> value option

  val get_universe : context -> Universes.universe

  (** Adds an arrow idfrom -> idto, i.e.: idto logically follows idfrom
     *)
  val add_dependency : context -> u_id -> u_id -> context

  (* Get a context for subuniverse i *)
  val subcontext : context -> int -> context

  val to_graph : context -> (u_id * value) graph
end

(* A context is a graph for an AST with extra information to make it
   easier to linearize it *)
module Context = struct
  open Universes
  open Scenario_value

  type u_id = Scenario_value.u_id

  module Nodes = Map.Make (struct
    type t = u_id

    let compare = Stdlib.compare
  end)

  module Names = Map.Make (struct
    type t = u_id

    let compare = Stdlib.compare
  end)

  module Edges = Set.Make (struct
    type t = u_id * u_id

    let compare = Stdlib.compare
  end)

  type value = u_id Scenario_value.value [@@deriving show]

  type names = string Names.t

  let get_name names u_id = Names.find_opt u_id names

  let print_names ppf names =
    fprintf ppf "%a"
      Factori_utils.(
        pp_print_list ~pp_sep:(tag ";") (fun ppf (u_id, s) ->
            fprintf ppf "%s,%s" (show_u_id u_id) s))
      (List.of_seq (Names.to_seq names))

  type context = {
    universe : int list;
    counter : int;
    nodes : value Nodes.t;
    dependencies : Edges.t;
    ctxt_names : names;
  }

  let add_node context u_id value =
    { context with nodes = Nodes.add u_id value context.nodes }

  let get_universe context = context.universe

  let print_context ppf ctxt =
    let open Scenario_value in
    fprintf ppf "{universe : %s;\ncounter : %d;\n nodes:\n%a;\n edges:\n%a\n}\n"
      (show_universe ctxt.universe)
      ctxt.counter
      (pp_print_list ~pp_sep:(Factori_utils.tag ";\n") (fun ppf (key, value) ->
           fprintf ppf "(%s -> %s)" (show_u_id key) (show_value pp_u_id value)))
      (List.of_seq @@ Nodes.to_seq ctxt.nodes)
      (pp_print_list ~pp_sep:(Factori_utils.tag ";\n") (fun ppf (id1, id2) ->
           fprintf ppf "(%s --> %s)" (show_u_id id1) (show_u_id id2)))
      (List.of_seq @@ Edges.to_seq ctxt.dependencies)

  let get_value_unsafe context u_id =
    try Nodes.find u_id context.nodes
    with Not_found ->
      Format.eprintf "[get_value_unsafe][before failwith] %a" print_context
        context ;
      failwith
        (Format.asprintf "[get_value_unsafe] Couldn't find id: %s\n"
           (show_u_id u_id))

  let get_value context u_id : value option = Nodes.find_opt u_id context.nodes

  type sequence = {
    seq : (u_id * value) list;
    seq_names : names;
    seq_universe : universe;
  }

  let to_graph context =
    let nodes =
      List.map (fun x -> (x, [])) @@ List.of_seq @@ Nodes.to_seq context.nodes
    in
    let insert_edge (x, y) g =
      let x = (x, get_value_unsafe context x) in
      let y = (y, get_value_unsafe context y) in
      let rec aux res = function
        | [] -> failwith "could not find node"
        | (h, edges) :: hs ->
          if h = x && not (List.mem y edges) then
            List.rev res @ ((h, y :: edges) :: hs)
          else
            aux ((h, edges) :: res) hs in
      aux [] g in
    Edges.fold
      (fun (x, y) acc -> insert_edge (x, y) acc)
      context.dependencies nodes

  let subsequence universe seq i =
    let new_universe = subuniverse universe i in
    let f (x : u_id) = { x with universe = new_universe } in
    List.map (fun ((x : u_id), y) -> (f x, map_value_id ~f y)) seq

  (* Update all names in names to subuniverse i *)
  let subnames universe i names =
    let new_universe = subuniverse universe i in
    let f (x : u_id) = { x with universe = new_universe } in
    Names.of_seq
      (Seq.map (fun ((x : u_id), y) -> (f x, y)) (Names.to_seq names))

  let subnodes universe i nodes =
    let new_universe = subuniverse universe i in
    let f (x : u_id) = { x with universe = new_universe } in
    Nodes.of_seq
      (Seq.map
         (fun ((x : u_id), y) -> (f x, map_value_id ~f y))
         (Nodes.to_seq nodes))

  let subdependencies universe i dependencies =
    let new_universe = subuniverse universe i in
    let f (x : u_id) = { x with universe = new_universe } in
    Edges.of_seq
      ((Seq.map (fun ((x : u_id), (y : u_id)) -> (f x, f y)))
         (Edges.to_seq dependencies))

  let subcontext context i =
    let new_universe = subuniverse context.universe i in
    {
      universe = new_universe;
      counter = context.counter;
      nodes = subnodes context.universe i context.nodes;
      ctxt_names = subnames context.universe i context.ctxt_names;
      dependencies = subdependencies context.universe i context.dependencies;
    }

  let duplicate_context k (context : context) =
    let integers = Factori_utils.list_integers ~from:0 (k - 1) in
    List.map (fun i -> subcontext context i) integers

  let duplicate_sequence universe k seq =
    let integers = Factori_utils.list_integers ~from:0 (k - 1) in
    List.map (fun i -> subsequence universe seq i) integers

  let rec is_action = function
    | Unary _ -> false
    | PrintUnary _ -> true
    | Deploy _ -> true
    | Call _ -> true
    | FailedCall _ -> true
    | Fork _ -> true (* for now; probably can be improved *)
    | Seq l -> List.exists (fun (_, a) -> is_action a) l
    | Checkpoint -> true

  (* Adds an arrow idfrom -> idto, i.e.: idto logically follows idfrom
     *)
  let add_dependency context idfrom idto =
    {
      context with
      dependencies = Edges.add (idfrom, idto) context.dependencies;
    }

  (* Make sure order is preserved between actions such as e.g. calling
     an entrypoint and getting the balance *)
  let add_dependencies_to_context (context : context) : context =
    Nodes.fold
      (fun u_id value accu ->
        Nodes.fold
          (fun u_id2 value2 accu2 ->
            if u_id.id < u_id2.id && is_action value && is_action value2 then
              add_dependency accu2 u_id u_id2
            else
              accu2)
          context.nodes accu)
      context.nodes context

  (* This function extracts actions and their dependencies
     from the line graph*)
  let context_to_sequences (context : context) : sequence list =
    (* Format.eprintf "entering linearize@." ; *)
    (* let universe = context.universe in *)
    let context = add_dependencies_to_context context in
    let graph = to_graph context in
    let l = toposort graph in
    let rec aux (res : (u_id * value) list) (uni : universe) (names : names) :
        (u_id * value) list -> sequence list = function
      | [] -> [{ seq = res; seq_names = names; seq_universe = uni }]
      | (_id, Fork _l) :: _xs -> failwith "Already handled at the AST level"
      | ((cur_id, _) as x) :: xs ->
        let x_connected_component = dfs graph [] x in
        let _ =
          let open Factori_utils in
          let id = get_fresh_id () in
          output_verbose ~id ~level:2
          @@ Format.asprintf
               "[linearize] Connected component of id %d universe %a %a\n@."
               cur_id.id print_universe uni
               (pp_print_list ~pp_sep:(tag ";") (fun ppf (u_id, _) ->
                    fprintf ppf "{id = %d}" u_id.id))
               x_connected_component in
        let new_res =
          if List.exists (fun (_, a) -> is_action a) x_connected_component then (
            Factori_utils.output_verbose ~level:2
            @@ sprintf "found action for id %d" cur_id.id ;
            res @ [x]
          ) else (
            Factori_utils.output_verbose ~level:2
            @@ sprintf "found no action for id %d" cur_id.id ;
            res
          ) in
        aux new_res uni names xs in
    let res = aux [] context.universe context.ctxt_names l in
    (* Format.eprintf "leaving linearize@." ; *)
    res

  let extract_contract_names_from_sequence (s : sequence) =
    let add x res =
      if List.mem x res then
        res
      else
        x :: res in
    let merge res1 res2 = List.fold_right add res2 res1 in
    List.fold_right
      (fun (_, v) res ->
        merge (Scenario_value.extract_contract_names_from_value v) res)
      s.seq []

  let extract_contract_names_from_sequence_list (s : sequence list) =
    let add x res =
      if List.mem x res then
        res
      else
        x :: res in
    let merge res1 res2 = List.fold_right add res2 res1 in
    List.fold_right
      (fun s res -> merge (extract_contract_names_from_sequence s) res)
      s []

  let print_name ?(underscore = false) ?(static = false) ?(alter = fun x -> x)
      names universe ppf u_id =
    let underscore =
      if underscore then
        "_"
      else
        "" in
    match get_name names u_id with
    | None ->
      eprintf
        "[Warning] Could not find object with id %s (associated universe %a) \
         in universe %a@. (complete list of available ids in this universe: \
         %a)"
        (show_u_id u_id) print_universe u_id.universe print_universe universe
        print_names names ;
      fprintf ppf "%sx_%a" underscore
        (pp_print_list ~pp_sep:(Factori_utils.tag "_") (fun ppf x ->
             fprintf ppf "%d" x))
        universe
    | Some name when static ->
      (* Format.eprintf "[print_name][static]%d, associated universe %a\n" u_id.id print_universe u_id.universe; *)
      let name = alter name in
      fprintf ppf "%s%s" underscore name
    | Some name ->
      (* Format.eprintf "[print_name][non-static]%d, associated universe %a\n" u_id.id print_universe u_id.universe; *)
      let name = alter name in
      fprintf ppf "%s%s_%a" underscore name
        (pp_print_list ~pp_sep:(Factori_utils.tag "_") (fun ppf x ->
             fprintf ppf "%d" x))
        universe

  let print_sequence ppf (seq : sequence) =
    let open Scenario_value in
    let str_of_name u_id =
      asprintf "%a" (print_name seq.seq_names seq.seq_universe) u_id in

    List.iter
      (fun (id, value) ->
        fprintf ppf "id : %s %s; value : %s\n" (show_u_id id) (str_of_name id)
          (show_value pp_u_id value))
      seq.seq

  let print_sequences ppf (seqs : sequence list) =
    List.iter (fun seq -> print_sequence ppf seq) seqs

  let init_context () =
    {
      universe = [];
      counter = 0;
      nodes = Nodes.empty;
      dependencies = Edges.empty;
      ctxt_names = Names.empty;
    }

  let fresh_id context =
    let c = context.counter in
    ({ universe = context.universe; id = c }, { context with counter = c + 1 })

  let add_name_to_context context u_id name =
    { context with ctxt_names = Names.add u_id name context.ctxt_names }

  let set_value ?(name = None) context x =
    let u_id, context = fresh_id context in
    let context =
      match name with
      | None -> context
      | Some name -> add_name_to_context context u_id name in
    (u_id, { context with nodes = Nodes.add u_id x context.nodes })

  let build_cst ?(name = None) context x = set_value ~name context x

  module ValueBalances = Map.Make (struct
    type t = value

    let compare = Stdlib.compare
  end)

  module AddrBalances = Map.Make (struct
    type t = address

    let compare = Stdlib.compare
  end)

  (* Reduce as in Map/Reduce, i.e. sum the amounts associated with the
     same tz1 *)
  let reduce_amounts (l : (value * value list) list) =
    let balances = ValueBalances.empty in
    let rec aux (balances : value list ValueBalances.t) = function
      | [] -> balances
      | (addr, amount) :: l ->
        let balances =
          match ValueBalances.find_opt addr balances with
          | None -> ValueBalances.add addr amount balances
          | Some b -> ValueBalances.add addr (amount @ b) balances in
        aux balances l in
    aux balances l

  let collect_contract_names (s : sequence) =
    let rec aux (res : string list) = function
      | [] -> res
      | (_id, Unary (FunctionCall (GetAddress, [_id1]))) :: tl -> aux res tl
      | (_id, Deploy deploy) :: tl ->
        let res =
          if not (List.mem deploy.contract_name res) then
            deploy.contract_name :: res
          else
            res in
        aux res tl
      | (_id, Call call) :: tl ->
        let res =
          if not (List.mem call.contract_name res) then
            call.contract_name :: res
          else
            res in
        aux res tl
      | _x :: xs -> aux res xs in
    aux [] s.seq

  let collect_paying_addresses (s : sequence) =
    let deployment_cost = Unary (Amount_uv (Z.of_string "11000000")) in
    let call_cost = Unary (Amount_uv (Z.of_string "5000000")) in
    let zero_cost = Unary (Amount_uv Z.zero) in
    let rec aux res = function
      | [] -> res
      | (_id, Unary (FunctionCall (GetAddress, [id]))) :: tl ->
        let res = (List.assoc id s.seq, [zero_cost]) :: res in
        aux res tl
      | (_id, Deploy deploy) :: tl ->
        let res =
          ( List.assoc deploy.from s.seq,
            [deployment_cost; List.assoc deploy.amount s.seq] )
          :: res in
        aux res tl
      | (_id, Call call) :: tl ->
        let res =
          (List.assoc call.from s.seq, [call_cost; List.assoc call.amount s.seq])
          :: res in
        aux res tl
      | (_id, FailedCall (Failed (_, _, call))) :: tl ->
        let res =
          (List.assoc call.from s.seq, [call_cost; List.assoc call.amount s.seq])
          :: res in
        aux res tl
      | _x :: xs -> aux res xs in
    reduce_amounts (aux [] s.seq)
end
