module FT = Factori_types

module Abstract = struct
  open Tzfunc.Proto

  type 'a astobj = 'a Ast.astobj

  type 'a id_or_concrete =
    | Concrete of 'a
    | Id of Scenario_id.id

  let lift x = Concrete x

  type nat = FT.nat id_or_concrete

  type bool' = bool id_or_concrete

  type unit' = unit id_or_concrete

  type string' = string id_or_concrete

  type 'a option' = 'a option id_or_concrete

  type 'a list' = 'a list id_or_concrete

  type int_michelson = FT.int_michelson id_or_concrete

  type address = FT.address id_or_concrete

  type bytes = FT.bytes id_or_concrete

  type ('a, 'b) map = ('a * 'b) id_or_concrete list id_or_concrete

  type ('a, 'b) pre_big_map =
    | Literal of ('a * 'b) id_or_concrete list
    | Abstract of A.zarith

  type ('a, 'b) big_map = ('a, 'b) pre_big_map id_or_concrete

  type lambda = FT.lambda id_or_concrete

  type timestamp = FT.timestamp id_or_concrete

  type contract = FT.contract id_or_concrete

  type operation = FT.operation id_or_concrete

  type signature = FT.signature id_or_concrete

  type key = FT.key id_or_concrete

  type tez = FT.int_michelson id_or_concrete

  type key_hash = FT.key_hash id_or_concrete

  type ('a, 'b) michelson_pair = ('a, 'b) FT.michelson_pair id_or_concrete

  type 'a set = 'a FT.set id_or_concrete

  type never_type = FT.never_type id_or_concrete

  type chain_id = FT.chain_id id_or_concrete

  type 'a ticket = 'a FT.ticket id_or_concrete

  type 'x1 tuple1 = 'x1 id_or_concrete

  type ('x1, 'x2) tuple2 = ('x1 * 'x2) id_or_concrete

  type ('x1, 'x2, 'x3) tuple3 = ('x1 * 'x2 * 'x3) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4) tuple4 = ('x1 * 'x2 * 'x3 * 'x4) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5) tuple5 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5, 'x6) tuple6 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5 * 'x6) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5, 'x6, 'x7) tuple7 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5 * 'x6 * 'x7) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5, 'x6, 'x7, 'x8) tuple8 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5 * 'x6 * 'x7 * 'x8) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5, 'x6, 'x7, 'x8, 'x9) tuple9 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5 * 'x6 * 'x7 * 'x8 * 'x9) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5, 'x6, 'x7, 'x8, 'x9, 'x10) tuple10 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5 * 'x6 * 'x7 * 'x8 * 'x9 * 'x10) id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5, 'x6, 'x7, 'x8, 'x9, 'x10, 'x11) tuple11 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5 * 'x6 * 'x7 * 'x8 * 'x9 * 'x10 * 'x11)
    id_or_concrete

  type ('x1, 'x2, 'x3, 'x4, 'x5, 'x6, 'x7, 'x8, 'x9, 'x10, 'x11, 'x12) tuple12 =
    ('x1 * 'x2 * 'x3 * 'x4 * 'x5 * 'x6 * 'x7 * 'x8 * 'x9 * 'x10 * 'x11 * 'x12)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13)
       tuple13 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14)
       tuple14 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15)
       tuple15 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16)
       tuple16 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17)
       tuple17 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18)
       tuple18 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18,
         'x19)
       tuple19 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18
    * 'x19)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18,
         'x19,
         'x20)
       tuple20 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18
    * 'x19
    * 'x20)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18,
         'x19,
         'x20,
         'x21)
       tuple21 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18
    * 'x19
    * 'x20
    * 'x21)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18,
         'x19,
         'x20,
         'x21,
         'x22)
       tuple22 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18
    * 'x19
    * 'x20
    * 'x21
    * 'x22)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18,
         'x19,
         'x20,
         'x21,
         'x22,
         'x23)
       tuple23 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18
    * 'x19
    * 'x20
    * 'x21
    * 'x22
    * 'x23)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18,
         'x19,
         'x20,
         'x21,
         'x22,
         'x23,
         'x24)
       tuple24 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18
    * 'x19
    * 'x20
    * 'x21
    * 'x22
    * 'x23
    * 'x24)
    id_or_concrete

  type ('x1,
         'x2,
         'x3,
         'x4,
         'x5,
         'x6,
         'x7,
         'x8,
         'x9,
         'x10,
         'x11,
         'x12,
         'x13,
         'x14,
         'x15,
         'x16,
         'x17,
         'x18,
         'x19,
         'x20,
         'x21,
         'x22,
         'x23,
         'x24,
         'x25)
       tuple25 =
    ('x1
    * 'x2
    * 'x3
    * 'x4
    * 'x5
    * 'x6
    * 'x7
    * 'x8
    * 'x9
    * 'x10
    * 'x11
    * 'x12
    * 'x13
    * 'x14
    * 'x15
    * 'x16
    * 'x17
    * 'x18
    * 'x19
    * 'x20
    * 'x21
    * 'x22
    * 'x23
    * 'x24
    * 'x25)
    id_or_concrete

  open Abstract_value

  let lift_nat x : nat = lift x

  let lift_bool' x : bool' = lift x

  let lift_unit' x : unit' = lift x

  let lift_string' x : string' = lift x

  let lift_int_michelson x : int_michelson = lift x

  let lift_address x : address = lift x

  let lift_bytes x : bytes = lift x

  let lift_timestamp x : timestamp = lift x

  let lift_contract x : contract = lift x

  let lift_operation x : operation = lift x

  let lift_signature x : signature = lift x

  let lift_key x : key = lift x

  let lift_tez x : tez = lift x

  let lift_key_hash x : key_hash = lift x

  let lift_never_type x : never_type = lift x

  let lift_chain_id x : chain_id = lift x

  let lift_ticket (type a b) (_lift_type : a -> b) =
    let open Factori_utils.Ticket in
    (function
     | Ticket i -> Concrete (Ticket i)
      : a FT.ticket -> b FT.ticket id_or_concrete)

  let lift_list' lift_type l = Concrete (List.map lift_type l)

  let lift_map l1 l2 l : ('a, 'b) map =
    Concrete (List.map (fun (x, y) -> Concrete (l1 x, l2 y)) l)

  let lift_lambda _l1 _l2 (l : FT.lambda) : lambda =
    match l with
    | Lambda {from; to_; body} -> Concrete (Lambda {from; to_; body})
    | SeqLambda m -> Concrete (SeqLambda m)

  let lift_set f l : 'a set = Concrete (List.map (fun x -> f x) l)

  let lift_option' f (x : 'a option) : 'b option' =
    match x with
    | None -> Concrete None
    | Some x -> Concrete (Some (f x))

  let lift_big_map l1 l2 (l : ('a, 'b) FT.big_map) : ('c, 'd) big_map =
    match l with
    | Literal l ->
      Concrete (Literal (List.map (fun (x, y) -> Concrete (l1 x, l2 y)) l))
    | Abstract z -> Concrete (Abstract z)

  let lift_tuple1 lift1 (x1 : 'a1) = Concrete (lift1 x1)

  let lift_tuple2 lift1 lift2 ((x1, x2) : 'a1 * 'a2) =
    Concrete (lift1 x1, lift2 x2)

  let lift_tuple3 lift1 lift2 lift3 ((x1, x2, x3) : 'a1 * 'a2 * 'a3) =
    Concrete (lift1 x1, lift2 x2, lift3 x3)

  let lift_tuple4 lift1 lift2 lift3 lift4
      ((x1, x2, x3, x4) : 'a1 * 'a2 * 'a3 * 'a4) =
    Concrete (lift1 x1, lift2 x2, lift3 x3, lift4 x4)

  let lift_tuple5 lift1 lift2 lift3 lift4 lift5
      ((x1, x2, x3, x4, x5) : 'a1 * 'a2 * 'a3 * 'a4 * 'a5) =
    Concrete (lift1 x1, lift2 x2, lift3 x3, lift4 x4, lift5 x5)

  let lift_tuple6 lift1 lift2 lift3 lift4 lift5 lift6
      ((x1, x2, x3, x4, x5, x6) : 'a1 * 'a2 * 'a3 * 'a4 * 'a5 * 'a6) =
    Concrete (lift1 x1, lift2 x2, lift3 x3, lift4 x4, lift5 x5, lift6 x6)

  let lift_tuple7 lift1 lift2 lift3 lift4 lift5 lift6 lift7
      ((x1, x2, x3, x4, x5, x6, x7) : 'a1 * 'a2 * 'a3 * 'a4 * 'a5 * 'a6 * 'a7) =
    Concrete
      (lift1 x1, lift2 x2, lift3 x3, lift4 x4, lift5 x5, lift6 x6, lift7 x7)

  let lift_tuple8 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8
      ((x1, x2, x3, x4, x5, x6, x7, x8) :
        'a1 * 'a2 * 'a3 * 'a4 * 'a5 * 'a6 * 'a7 * 'a8) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8 )

  let lift_tuple9 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9) :
        'a1 * 'a2 * 'a3 * 'a4 * 'a5 * 'a6 * 'a7 * 'a8 * 'a9) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9 )

  let lift_tuple10 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9, x10) :
        'a1 * 'a2 * 'a3 * 'a4 * 'a5 * 'a6 * 'a7 * 'a8 * 'a9 * 'a10) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10 )

  let lift_tuple11 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11) :
        'a1 * 'a2 * 'a3 * 'a4 * 'a5 * 'a6 * 'a7 * 'a8 * 'a9 * 'a10 * 'a11) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11 )

  let lift_tuple12 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12) :
        'a1 * 'a2 * 'a3 * 'a4 * 'a5 * 'a6 * 'a7 * 'a8 * 'a9 * 'a10 * 'a11 * 'a12)
      =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12 )

  let lift_tuple13 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13 )

  let lift_tuple14 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14 )

  let lift_tuple15 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15 )

  let lift_tuple16 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16
      ((x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15, x16) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16 )

  let lift_tuple17 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17 )

  let lift_tuple18 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18 )

  let lift_tuple19 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18 lift19
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18,
         x19 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18
        * 'a19) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18,
        lift19 x19 )

  let lift_tuple20 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18 lift19 lift20
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18,
         x19,
         x20 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18
        * 'a19
        * 'a20) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18,
        lift19 x19,
        lift20 x20 )

  let lift_tuple21 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18 lift19 lift20
      lift21
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18,
         x19,
         x20,
         x21 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18
        * 'a19
        * 'a20
        * 'a21) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18,
        lift19 x19,
        lift20 x20,
        lift21 x21 )

  let lift_tuple22 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18 lift19 lift20
      lift21 lift22
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18,
         x19,
         x20,
         x21,
         x22 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18
        * 'a19
        * 'a20
        * 'a21
        * 'a22) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18,
        lift19 x19,
        lift20 x20,
        lift21 x21,
        lift22 x22 )

  let lift_tuple23 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18 lift19 lift20
      lift21 lift22 lift23
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18,
         x19,
         x20,
         x21,
         x22,
         x23 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18
        * 'a19
        * 'a20
        * 'a21
        * 'a22
        * 'a23) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18,
        lift19 x19,
        lift20 x20,
        lift21 x21,
        lift22 x22,
        lift23 x23 )

  let lift_tuple24 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18 lift19 lift20
      lift21 lift22 lift23 lift24
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18,
         x19,
         x20,
         x21,
         x22,
         x23,
         x24 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18
        * 'a19
        * 'a20
        * 'a21
        * 'a22
        * 'a23
        * 'a24) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18,
        lift19 x19,
        lift20 x20,
        lift21 x21,
        lift22 x22,
        lift23 x23,
        lift24 x24 )

  let lift_tuple25 lift1 lift2 lift3 lift4 lift5 lift6 lift7 lift8 lift9 lift10
      lift11 lift12 lift13 lift14 lift15 lift16 lift17 lift18 lift19 lift20
      lift21 lift22 lift23 lift24 lift25
      (( x1,
         x2,
         x3,
         x4,
         x5,
         x6,
         x7,
         x8,
         x9,
         x10,
         x11,
         x12,
         x13,
         x14,
         x15,
         x16,
         x17,
         x18,
         x19,
         x20,
         x21,
         x22,
         x23,
         x24,
         x25 ) :
        'a1
        * 'a2
        * 'a3
        * 'a4
        * 'a5
        * 'a6
        * 'a7
        * 'a8
        * 'a9
        * 'a10
        * 'a11
        * 'a12
        * 'a13
        * 'a14
        * 'a15
        * 'a16
        * 'a17
        * 'a18
        * 'a19
        * 'a20
        * 'a21
        * 'a22
        * 'a23
        * 'a24
        * 'a25) =
    Concrete
      ( lift1 x1,
        lift2 x2,
        lift3 x3,
        lift4 x4,
        lift5 x5,
        lift6 x6,
        lift7 x7,
        lift8 x8,
        lift9 x9,
        lift10 x10,
        lift11 x11,
        lift12 x12,
        lift13 x13,
        lift14 x14,
        lift15 x15,
        lift16 x16,
        lift17 x17,
        lift18 x18,
        lift19 x19,
        lift20 x20,
        lift21 x21,
        lift22 x22,
        lift23 x23,
        lift24 x24,
        lift25 x25 )

  let bind_abstract_value :
        'a 'b. ('a -> 'b value) -> 'a id_or_concrete -> 'b value =
    fun (type b) (f : 'a -> b value) x ->
     match x with
     | Id id -> VAbstract (ScenId id)
     | Concrete x -> f x

  let value_of_nat : nat -> FT.nat value = bind_abstract_value (fun n -> VInt n)

  let value_of_unit' : unit' -> unit value =
    bind_abstract_value (fun () -> VUnit ())

  let value_of_bool' : bool' -> bool value =
    bind_abstract_value (fun b -> VBool b)

  let value_of_string' : string' -> string value =
    bind_abstract_value (fun s -> VString s)

  let value_of_never_type : never_type -> 'a =
   fun _ -> failwith "never say never"

  let value_of_option' (f : 'a -> 'b value) =
    bind_abstract_value (fun (o : 'a option) ->
        match o with
        | None -> VOption None
        | Some a -> VOption (Some (f a)))

  let value_of_list' (f : 'a -> 'b value) =
    bind_abstract_value (fun (l : 'a list) -> VList (List.map f l))

  let value_of_int_michelson : int_michelson -> FT.int_michelson value =
    bind_abstract_value (fun x -> VInt x)

  let value_of_address : address -> FT.address value =
    bind_abstract_value (fun a -> VAddress a)

  let value_of_timestamp : address -> FT.timestamp value =
    bind_abstract_value (fun a -> VTimestamp a)

  let value_of_bytes : bytes -> FT.bytes value =
    bind_abstract_value (fun b -> VBytes b)

  let value_of_map f g (m : ('a, 'b) map) =
    bind_abstract_value
      (fun m ->
        VMap
          (List.map
             (fun xy ->
               match xy with
               | Concrete (x, y) -> SP2 (f x, g y)
               | Id i -> SP (VAbstract (ScenId i)))
             m))
      m

  let value_of_big_map f g (x : ('a, 'b) big_map) =
    bind_abstract_value
      (fun (m : ('a, 'b) pre_big_map) ->
        match m with
        | Literal m ->
          VLiteralBigMap
            (List.map
               (fun xy ->
                 match xy with
                 | Concrete (x, y) -> SP2 (f x, g y)
                 | Id i -> SP (VAbstract (ScenId i)))
               m)
        | Abstract i -> VOnChainBigMap i)
      x

  let value_of_contract : contract -> FT.contract value =
    bind_abstract_value (fun (_c : FT.contract) : FT.contract value ->
        failwith "TODO: value_of_contract")

  let value_of_operation : operation -> FT.operation value =
    bind_abstract_value (fun op -> VOperation op)

  let value_of_signature : signature -> FT.signature value =
    bind_abstract_value (fun signature -> VSignature signature)

  let value_of_key : key -> FT.key value =
    bind_abstract_value (fun key -> VKey key)

  let value_of_tez : tez -> FT.tez value =
    bind_abstract_value (fun tez -> VTez tez)

  let value_of_key_hash : key_hash -> FT.key_hash value =
    bind_abstract_value (fun kh -> VKeyHash kh)

  (* let value_of_michelson_pair _ _ _ = failwith "TODO: michelson_pair" *)

  let value_of_set f = bind_abstract_value (fun s -> VSet (List.map f s))

  (* let value_of_never _ =
   *   bind_abstract_value
   *     (fun _ -> failwith "TODO: value_of_never") *)

  let value_of_chain_id : chain_id -> FT.chain_id value =
    bind_abstract_value (fun ci -> VChain_id ci)

  let value_of_ticket (type a b) (_f : a -> b) =
    let open Factori_utils.Ticket in
    (bind_abstract_value (function Ticket t -> VTicket (Ticket t))
      : a FT.ticket id_or_concrete -> a ticket value)

  let value_of_lambda _f1 _f2 :
      Lambda.lambda id_or_concrete -> Lambda.lambda value =
    bind_abstract_value (function
        | Lambda.Lambda {from; to_; body} -> VLambda (Lambda {from; to_; body})
        | Lambda.SeqLambda m -> VLambda (SeqLambda m))

  let value_of_tuple1 f = bind_abstract_value (fun a -> VTuple (Single (f a)))

  let value_of_tuple2 f1 f2 =
    bind_abstract_value (fun (x1, x2) -> VTuple (Multi (f1 x1, Single (f2 x2))))

  let value_of_tuple3 f1 f2 f3 =
    bind_abstract_value (fun (x1, x2, x3) ->
        VTuple (Multi (f1 x1, Multi (f2 x2, Single (f3 x3)))))

  let value_of_tuple4 f1 f2 f3 f4 =
    bind_abstract_value (fun (x1, x2, x3, x4) ->
        VTuple (Multi (f1 x1, Multi (f2 x2, Multi (f3 x3, Single (f4 x4))))))

  let value_of_tuple5 f1 f2 f3 f4 f5 =
    bind_abstract_value (fun (x1, x2, x3, x4, x5) ->
        VTuple
          (Multi
             (f1 x1, Multi (f2 x2, Multi (f3 x3, Multi (f4 x4, Single (f5 x5)))))))

  let value_of_tuple6 f1 f2 f3 f4 f5 f6 =
    bind_abstract_value (fun (x1, x2, x3, x4, x5, x6) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi (f3 x3, Multi (f4 x4, Multi (f5 x5, Single (f6 x6))))
                 ) )))

  let value_of_tuple7 f1 f2 f3 f4 f5 f6 f7 =
    bind_abstract_value (fun (x1, x2, x3, x4, x5, x6, x7) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         (f4 x4, Multi (f5 x5, Multi (f6 x6, Single (f7 x7))))
                     ) ) )))

  let value_of_tuple8 f1 f2 f3 f4 f5 f6 f7 f8 =
    bind_abstract_value (fun (x1, x2, x3, x4, x5, x6, x7, x8) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi (f6 x6, Multi (f7 x7, Single (f8 x8))) ) )
                     ) ) )))

  let value_of_tuple9 f1 f2 f3 f4 f5 f6 f7 f8 f9 =
    bind_abstract_value (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi (f7 x7, Multi (f8 x8, Single (f9 x9)))
                                 ) ) ) ) ) )))

  let value_of_tuple10 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 =
    bind_abstract_value (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         (f8 x8, Multi (f9 x9, Single (f10 x10)))
                                     ) ) ) ) ) ) )))

  let value_of_tuple11 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 =
    bind_abstract_value (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi (f10 x10, Single (f11 x11))
                                             ) ) ) ) ) ) ) ) )))

  let value_of_tuple12 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 =
    bind_abstract_value
      (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     (f11 x11, Single (f12 x12))
                                                 ) ) ) ) ) ) ) ) ) )))

  let value_of_tuple13 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 =
    bind_abstract_value
      (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Single (f13 x13) ) )
                                                 ) ) ) ) ) ) ) ) ) )))

  let value_of_tuple14 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 =
    bind_abstract_value
      (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Single (f14 x14)
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple15 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 =
    bind_abstract_value
      (fun (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15) ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Single
                                                                     (f15 x15)
                                                                 ) ) ) ) ) ) )
                                     ) ) ) ) ) ) )))

  let value_of_tuple16 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 =
    bind_abstract_value
      (fun
        (x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15, x16)
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Single
                                                                         (f16
                                                                            x16)
                                                                     ) ) ) ) )
                                                 ) ) ) ) ) ) ) ) ) )))

  let value_of_tuple17 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Single
                                                                             (f17
                                                                                x17)
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple18 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Single
                                                                                (
                                                                                f18
                                                                                x18)
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple19 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 f19 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18,
          x19 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Multi
                                                                                ( 
                                                                                f18
                                                                                x18,
                                                                                Single
                                                                                (
                                                                                f19
                                                                                x19)
                                                                                )
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple20 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 f19 f20 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18,
          x19,
          x20 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Multi
                                                                                ( 
                                                                                f18
                                                                                x18,
                                                                                Multi
                                                                                ( 
                                                                                f19
                                                                                x19,
                                                                                Single
                                                                                (
                                                                                f20
                                                                                x20)
                                                                                )
                                                                                )
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple21 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 f19 f20 f21 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18,
          x19,
          x20,
          x21 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Multi
                                                                                ( 
                                                                                f18
                                                                                x18,
                                                                                Multi
                                                                                ( 
                                                                                f19
                                                                                x19,
                                                                                Multi
                                                                                ( 
                                                                                f20
                                                                                x20,
                                                                                Single
                                                                                (
                                                                                f21
                                                                                x21)
                                                                                )
                                                                                )
                                                                                )
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple22 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 f19 f20 f21 f22 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18,
          x19,
          x20,
          x21,
          x22 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Multi
                                                                                ( 
                                                                                f18
                                                                                x18,
                                                                                Multi
                                                                                ( 
                                                                                f19
                                                                                x19,
                                                                                Multi
                                                                                ( 
                                                                                f20
                                                                                x20,
                                                                                Multi
                                                                                ( 
                                                                                f21
                                                                                x21,
                                                                                Single
                                                                                (
                                                                                f22
                                                                                x22)
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple23 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 f19 f20 f21 f22 f23 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18,
          x19,
          x20,
          x21,
          x22,
          x23 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Multi
                                                                                ( 
                                                                                f18
                                                                                x18,
                                                                                Multi
                                                                                ( 
                                                                                f19
                                                                                x19,
                                                                                Multi
                                                                                ( 
                                                                                f20
                                                                                x20,
                                                                                Multi
                                                                                ( 
                                                                                f21
                                                                                x21,
                                                                                Multi
                                                                                ( 
                                                                                f22
                                                                                x22,
                                                                                Single
                                                                                (
                                                                                f23
                                                                                x23)
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple24 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 f19 f20 f21 f22 f23 f24 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18,
          x19,
          x20,
          x21,
          x22,
          x23,
          x24 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Multi
                                                                                ( 
                                                                                f18
                                                                                x18,
                                                                                Multi
                                                                                ( 
                                                                                f19
                                                                                x19,
                                                                                Multi
                                                                                ( 
                                                                                f20
                                                                                x20,
                                                                                Multi
                                                                                ( 
                                                                                f21
                                                                                x21,
                                                                                Multi
                                                                                ( 
                                                                                f22
                                                                                x22,
                                                                                Multi
                                                                                ( 
                                                                                f23
                                                                                x23,
                                                                                Single
                                                                                (
                                                                                f24
                                                                                x24)
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))

  let value_of_tuple25 f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16
      f17 f18 f19 f20 f21 f22 f23 f24 f25 =
    bind_abstract_value
      (fun
        ( x1,
          x2,
          x3,
          x4,
          x5,
          x6,
          x7,
          x8,
          x9,
          x10,
          x11,
          x12,
          x13,
          x14,
          x15,
          x16,
          x17,
          x18,
          x19,
          x20,
          x21,
          x22,
          x23,
          x24,
          x25 )
      ->
        VTuple
          (Multi
             ( f1 x1,
               Multi
                 ( f2 x2,
                   Multi
                     ( f3 x3,
                       Multi
                         ( f4 x4,
                           Multi
                             ( f5 x5,
                               Multi
                                 ( f6 x6,
                                   Multi
                                     ( f7 x7,
                                       Multi
                                         ( f8 x8,
                                           Multi
                                             ( f9 x9,
                                               Multi
                                                 ( f10 x10,
                                                   Multi
                                                     ( f11 x11,
                                                       Multi
                                                         ( f12 x12,
                                                           Multi
                                                             ( f13 x13,
                                                               Multi
                                                                 ( f14 x14,
                                                                   Multi
                                                                     ( f15 x15,
                                                                       Multi
                                                                         ( f16
                                                                             x16,
                                                                           Multi
                                                                             ( f17
                                                                                x17,
                                                                               Multi
                                                                                ( 
                                                                                f18
                                                                                x18,
                                                                                Multi
                                                                                ( 
                                                                                f19
                                                                                x19,
                                                                                Multi
                                                                                ( 
                                                                                f20
                                                                                x20,
                                                                                Multi
                                                                                ( 
                                                                                f21
                                                                                x21,
                                                                                Multi
                                                                                ( 
                                                                                f22
                                                                                x22,
                                                                                Multi
                                                                                ( 
                                                                                f23
                                                                                x23,
                                                                                Multi
                                                                                ( 
                                                                                f24
                                                                                x24,
                                                                                Single
                                                                                (
                                                                                f25
                                                                                x25)
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                                )
                                                                             )
                                                                         ) ) )
                                                             ) ) ) ) ) ) ) ) )
                         ) ) ) )))
end
