open Factori_network
open Factori_config

module OCaml = struct
  let get_storage ~network ~kt1_name ~kt1 =
    Format.sprintf
      {|
let _ =
  %s_ocaml_interface.test_storage_download
    ~kt1:"%s"
    ~node:"%s"
    ()
  |}
      (String.capitalize_ascii kt1_name)
      kt1 (get_raw_network network)
end

module Typescript = struct
  let get_storage ~network ~kt1_name ~kt1 =
    Format.sprintf
      {|import {
  TezosToolkit,
  BigMapAbstraction,
  MichelsonMap,
  OriginationOperation,
  OpKind,
  createTransferOperation,
  TransferParams,
  RPCOperation,
  createRevealOperation
} from "@taquito/taquito"

import { MichelsonV1Expression } from "@taquito/rpc"

import { encodeOpHash } from "@taquito/utils"

import * as %s_interface from "./%s_interface"

const tezosKit_%s = new TezosToolkit('%s');

export async function main() {
  const %s = await tezosKit_%s.contract.at("%s")
  let storage_%s = await %s.storage() as %s_interface.Storage_type
 }

let res = main ()
console.log("Battery of Typescript tests successful.")
|}
      kt1_name kt1_name
      (string_of_network network)
      (get_raw_network network) kt1_name
      (string_of_network network)
      kt1 kt1_name kt1_name kt1_name
end

module Python = struct
  let get_storage ~kt1_name ~kt1 =
    Format.sprintf
      {|
from %s import %s_python_interface

storage_%s = %s_python_interface.test_storage_download("%s")
|}
      Python_SDK.interface_dir_name kt1_name kt1_name kt1_name kt1
end

module Csharp = struct
  let get_storage ~network ~kt1_name ~kt1 =
    Format.sprintf {|
await %s.Test.test_storage_download("%s", "%s");
|}
      kt1_name kt1
      (string_of_network network)
end
