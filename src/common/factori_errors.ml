(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains a list of exceptions that can be raised during the
execution of the Factori program, along with two functions to handle these
exceptions. The file also includes documentation for each exception and the
functions. Warning: this short summary was automatically generated and could be
incomplete or misleading. If you feel this is the case, please open an issue. *)

open Tzfunc.Proto

exception NoDirectoryProvided

exception NotADirectory of string * string

exception NotAFile of string * string

exception DirectoryNotEmpty of string

exception NotOverwritingFile of string

exception NoKT1Provided

exception TzfuncError of Tzfunc.Rp.error

exception NoMichelsonFileProvided

exception NoScript of string

exception GenericError of string * string

exception UnexpectedMichelson of string

exception ParsingError of string

exception CouldNotDestruct of string

exception CouldNotReadAsJson of string

(* Used in Infer_entrypoints *)
exception Nothandled of string

exception NotAType of Tzfunc.Proto.micheline

exception NotAFactoriRepo of string

exception ElementShouldHaveName of string

exception NoContractNameProvided

exception TooManyLanguages

exception NoLanguageProvided

exception UnknownStorageType of string

exception UnstackMicheline of string

exception WrongNumberOfArg of primitive_or_macro

exception UnknownContract of string * string

let stack_overflow_handle exn =
  match exn with
  | Stack_overflow ->
    Format.eprintf "Contract is too big to analyze with factori analyze"
  | _ -> raise exn

(** the function to handle exceptions is a reference so we can add
   more inside the code of Factori *)
let hookable_handle_exception =
  let open Format in
  ref @@ function
  | NoDirectoryProvided -> eprintf "No directory provided.\n%!"
  | NotADirectory (d, msg) -> eprintf "[%s] Not a directory: %s.\n%!" msg d
  | NotAFile (f, msg) -> eprintf "[%s] Not a file: %s.\n%!" msg f
  | DirectoryNotEmpty d ->
    eprintf
      "Directory %s not empty. If you still wish to overwrite its contents, \
       please use option --force.\n\
       %!"
      d
  | NotOverwritingFile file -> eprintf "Not overwriting file %s.\n%!" file
  | NoKT1Provided -> eprintf "No KT1 was provided.\n%!"
  | NoMichelsonFileProvided -> eprintf "No Michelson file was provided.\n%!"
  | NoScript s -> eprintf "No script for %s" s
  | GenericError (prg, err) -> eprintf "[%s] %s\n%!" prg err
  | UnexpectedMichelson mic -> eprintf "Unexpected Michelson: %s" mic
  | TzfuncError err ->
    eprintf "[Tzfunc error]: %s\n" (Tzfunc.Rp.string_of_error err)
  | ParsingError s -> eprintf "Parsing Error: %s" s
  | Nothandled s -> eprintf "Not handled: %s" s
  | NotAType m ->
    eprintf "%s is not a type."
      (EzEncoding.construct Tzfunc.Proto.micheline_enc.json m)
  | ElementShouldHaveName s -> eprintf "%s should have a name." s
  | CouldNotDestruct s -> eprintf "Could not destruct %s." s
  | CouldNotReadAsJson s -> eprintf "Could not read %s as a JSON value." s
  | NoContractNameProvided ->
    eprintf "No contract name was provided, please provide one."
  | TooManyLanguages ->
    eprintf
      "Please select exactly one programming language for this operation.\n%!"
  | NoLanguageProvided ->
    eprintf
      "No programming language was provided. Please provide at least one \
       programming language (e.g. --ocaml, --typescript).\n\
       %!"
  | UnknownStorageType s ->
    eprintf
      "Unknown storage type \"%s\". Please choose either \"random\" or \
       \"blockchain\"\n\
       %!"
      s
  | UnstackMicheline e -> Format.eprintf "Unstacking Michelson error : %s" e
  | UnknownContract (s, config_file) ->
    Format.eprintf
      "The contract specified %S does not belong to the project or has not \
       been correctly added to the local configuration file (%s)"
      s config_file
  | NotAFactoriRepo dir ->
    Format.eprintf
      "%s is not a factori repo, please move to a factori repo if you want to \
       use this command"
      dir
  | Sys_error s -> eprintf "System error: %s" s
  | e -> raise e

(** General function for handling exceptions inside Factori  *)
let handle_exception e = !hookable_handle_exception e

(** Hook to handle a future exception when added  *)
let add_exception_handler (f : exn -> unit) =
  let previous_f = !hookable_handle_exception in
  hookable_handle_exception := fun e -> try previous_f e with e -> f e
