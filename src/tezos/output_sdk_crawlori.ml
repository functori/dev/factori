(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml code provides functions for extracting metadata from Michelson
types for generating SQL code for Crawlori, a Tezos indexer. It includes
functions for sanitizing and extracting information from Michelson types,
generating SQL queries for creating tables and indexes, dropping tables, and
inserting rows. Example usage is also provided throughout the code. Warning:
this short summary was automatically generated and could be incomplete or
misleading. If you feel this is the case, please open an issue. *)

open Format
open Types
open Infer_entrypoints
open Factori_config
open Factori_utils

(* TODO maybe use a prefix with less chance of collision *)
let bm_col_prefix ~contract_name =
  sprintf "bmid_%s" (show_sanitized_name contract_name)

let bm_table_prefix ~contract_name =
  sprintf "bm_%s" (show_sanitized_name contract_name)

let bm_key_prefix = "key"

let bm_value_prefix = "value"

let rec column_name (name : string) =
  if String.length name > 54 then
    let len = String.length name - 30 in
    column_name (String.sub name 0 20 ^ String.sub name 30 len)
  else
    String.lowercase_ascii name

let rec table_name (name : string) =
  if String.length name > 54 then
    let len = String.length name - 30 in
    table_name (String.sub name 0 20 ^ String.sub name 30 len)
  else
    String.lowercase_ascii name

let basetype_to_crawlori_type = function
  | Never -> None
  | String -> Some CVarchar
  | Nat -> Some CZarith
  | Int -> Some CZarith
  | Byte -> Some CBytea
  | Address -> Some CVarchar
  | Signature -> Some CVarchar
  | Unit -> None
  | Bool -> Some CBoolean
  | Timestamp -> Some CTimestamp
  | Keyhash -> Some CVarchar
  | Key -> Some CVarchar
  | Mutez -> Some CZarith
  | Operation -> None
  | Chain_id | Sapling_state | Sapling_transaction_deprecated -> None

let rec extract_crawlori_columns_from_define_type ~contract_name ~nullable
    ~annoted ~last_name ~name = function
  | Annot (t, aname) ->
    let name =
      if name = [] then
        [aname]
      else
        name @ [aname] in
    extract_crawlori_columns_from_define_type ~contract_name ~nullable
      ~annoted:true ~last_name:aname ~name t
  | TVar (aname, t) ->
    extract_crawlori_columns_from_define_type ~contract_name ~nullable
      ~annoted:true ~last_name:aname ~name t
  | Pair l ->
    snd
    @@ List.fold_left
         (fun (index, acc) t ->
           let name = name @ [sanitized_of_str @@ string_of_int index] in
           let last_name =
             if annoted then
               last_name
             else
               sanitized_of_str
                 (String.concat "_" (List.map show_sanitized_name name)) in
           ( index + 1,
             acc
             @ extract_crawlori_columns_from_define_type ~contract_name
                 ~nullable ~annoted ~last_name ~name t ))
         (0, []) l
  | Base t ->
    Option.fold ~none:[]
      ~some:(fun t ->
        [
          {
            crawlori_column_name =
              column_name
              @@ String.concat "_" (List.map show_sanitized_name name);
            crawlori_column_bm_name = None;
            crawlori_column_dipdup_bm_name = None;
            crawlori_column_type = t;
            crawlori_column_option = nullable;
            crawlori_column_index = None;
            crawlori_column_source = Mic_parameter;
          };
        ])
      (basetype_to_crawlori_type t)
  | Record r ->
    Pair_type.fold_paired_type
      ~f:(fun n t acc ->
        let name =
          if name = [] then
            [n]
          else
            name @ [n] in
        acc
        @ extract_crawlori_columns_from_define_type ~contract_name ~nullable
            ~annoted ~last_name:n ~name t)
      [] r
  | Binary (BigMap, _) ->
    let dd_bm_name =
      match name with
      | [] -> "big_map"
      | [i] -> begin
        try
          ignore @@ int_of_string (show_sanitized_name i) ;
          "big_map"
        with _ -> show_sanitized_name i
      end
      | i :: tl -> begin
        try
          ignore @@ int_of_string (show_sanitized_name i) ;
          String.concat "." (List.map show_sanitized_name tl)
        with _ -> String.concat "." (List.map show_sanitized_name name)
      end in
    [
      {
        crawlori_column_name =
          column_name
          @@ sprintf "%s_%s"
               (bm_col_prefix ~contract_name)
               (String.concat "_" (List.map show_sanitized_name name));
        crawlori_column_bm_name =
          Some (String.lowercase_ascii (show_sanitized_name last_name));
        crawlori_column_dipdup_bm_name = Some dd_bm_name;
        crawlori_column_type = CZarith;
        crawlori_column_option = true;
        crawlori_column_index = None;
        crawlori_column_source = Mic_parameter;
      };
    ]
  | Binary (Map, _) | Unary (Set, _) ->
    [] (* TODO: can't we do the same thing as with bigmaps? *)
  | Unary (List, t) ->
    extract_crawlori_columns_from_define_type ~contract_name ~nullable ~annoted
      ~last_name ~name t
  | Or o ->
    Or_type.fold_or_type
      (fun t acc ->
        extract_crawlori_columns_from_define_type ~contract_name ~nullable:true
          ~annoted ~last_name ~name t
        @ acc)
      [] o
  | Binary (Lambda, _) -> []
  | Unary (Option, _) -> []
  | Contract _ -> []
  | Unary (Ticket, _) -> []

let rec extract_crawlori_bigmaps_from_define_type ~name = function
  | Annot (t, aname) ->
    let name = name @ [aname] in
    extract_crawlori_bigmaps_from_define_type ~name t
  | TVar (_name, t) -> extract_crawlori_bigmaps_from_define_type ~name t
  | Pair l ->
    List.flatten
    @@ List.mapi
         (fun i t ->
           let name = name @ [sanitized_of_str @@ string_of_int i] in
           extract_crawlori_bigmaps_from_define_type ~name t)
         l
  | Base _t -> []
  | Record (LeafP (aname, t)) ->
    let name = [aname] in
    extract_crawlori_bigmaps_from_define_type ~name t
  | Record (PairP l) ->
    List.flatten
    @@ List.map
         (fun t -> extract_crawlori_bigmaps_from_define_type ~name (Record t))
         l
  | Binary (BigMap, (key_t, value_t)) -> [(name, key_t, value_t)]
  | Binary (Map, _) | Unary (Set, _) ->
    [] (* TODO: can't we do the same thing as with bigmaps? *)
  | Unary (List, t) -> extract_crawlori_bigmaps_from_define_type ~name t
  | Or o ->
    Or_type.fold_or_type
      (fun t acc -> extract_crawlori_bigmaps_from_define_type ~name t @ acc)
      [] o
  | Binary (Lambda, _) -> []
  | Unary (Option, _) -> []
  | Contract _ -> []
  | Unary (Ticket, _) -> []

let michelson_column ~storage_name name =
  {
    crawlori_column_name =
      (column_name
      @@
      if name = storage_name then
        sprintf "%s_content" (show_sanitized_name storage_name)
      else
        Printf.sprintf "%s_parameter" (show_sanitized_name name));
    crawlori_column_bm_name = None;
    crawlori_column_dipdup_bm_name = None;
    crawlori_column_type = CJson;
    crawlori_column_option = false;
    crawlori_column_index = None;
    crawlori_column_source = Mic_parameter;
  }

let rec extract_table_from_instruction ~storage_name ~contract_name acc =
  function
  | Define_type (name, type_) ->
    let columns =
      if name = storage_name then
        extract_crawlori_columns_from_define_type ~contract_name ~nullable:false
          ~annoted:false ~last_name:(sanitized_of_str "") ~name:[] type_
      else
        [michelson_column ~storage_name name] in
    {
      crawlori_table_name = table_name (show_sanitized_name name);
      crawlori_table_is_bm = false;
      crawlori_table_entrypoint_name =
        (if name = storage_name then
           None
         else
           Some (show_sanitized_name name));
      crawlori_table_fields = columns;
      crawlori_table_type = Some type_;
    }
    :: acc
  | Deploy -> acc
  | Seq ii ->
    List.fold_left
      (fun acc i ->
        extract_table_from_instruction ~storage_name ~contract_name acc i)
      acc ii
  | Entrypoint _ -> acc
  | Big_map_access _ -> acc

let rec extract_bm_tables_from_instruction ~storage_name ~contract_name acc =
  function
  | Define_type (n, type_) when n = storage_name ->
    let bms = extract_crawlori_bigmaps_from_define_type ~name:[] type_ in
    (* Format.eprintf "%s : %d@." "storage" (List.length bms); *)
    List.fold_left
      (fun acc (name, _key_type, _value_type) ->
        let name =
          sanitized_of_str
          @@ String.concat "_" (List.map show_sanitized_name name) in
        let key_name =
          sanitized_of_str
          @@ Format.sprintf "%s_%s" bm_key_prefix (show_sanitized_name name)
        in
        let key_columns = [michelson_column ~storage_name key_name] in
        let value_name =
          sanitized_of_str
          @@ Format.sprintf "%s_%s" bm_value_prefix (show_sanitized_name name)
        in
        let value_columns = [michelson_column ~storage_name value_name] in
        {
          crawlori_table_name =
            table_name
            @@ sprintf "%s_%s"
                 (bm_table_prefix ~contract_name)
                 (show_sanitized_name name);
          crawlori_table_is_bm = true;
          crawlori_table_entrypoint_name = None;
          crawlori_table_fields = key_columns @ value_columns;
          crawlori_table_type = Some type_;
        }
        :: acc)
      acc bms
  | Define_type _ -> acc
  | Deploy -> acc
  | Seq ii ->
    List.fold_left
      (fun acc i ->
        extract_bm_tables_from_instruction ~storage_name ~contract_name acc i)
      acc ii
  | Entrypoint _ -> acc
  | Big_map_access _ -> acc

let add_generic_blockchain_fields table =
  let contract_field =
    {
      crawlori_column_name = "contract";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let transaction_field =
    {
      crawlori_column_name = "transaction";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = None;
      crawlori_column_source = Operation;
    } in
  let block_field =
    {
      crawlori_column_name = "block";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let index_field =
    {
      crawlori_column_name = "index";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CInteger;
      crawlori_column_option = false;
      crawlori_column_index = None;
      crawlori_column_source = Operation;
    } in
  let level_field =
    {
      crawlori_column_name = "level";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CInteger;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let tsp_field =
    {
      crawlori_column_name = "tsp";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CTimestamp;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let main_field =
    {
      crawlori_column_name = "main";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CBoolean;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let extra_fields =
    [
      contract_field;
      transaction_field;
      block_field;
      index_field;
      level_field;
      tsp_field;
      main_field;
    ] in
  {
    table with
    crawlori_table_fields = extra_fields @ table.crawlori_table_fields;
  }

let state_table () =
  let level_field =
    {
      crawlori_column_name = "level";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CInteger;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let chain_id_field =
    {
      crawlori_column_name = "chain_id";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let tsp_field =
    {
      crawlori_column_name = "tsp";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CTimestamp;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let columns = [level_field; chain_id_field; tsp_field] in
  {
    crawlori_table_name = "state";
    crawlori_table_is_bm = false;
    crawlori_table_entrypoint_name = None;
    crawlori_table_fields = columns;
    crawlori_table_type = None;
  }

let contracts_table bm_ids =
  let address_field =
    {
      crawlori_column_name = "address";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let kind_field =
    {
      crawlori_column_name = "kind";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let transaction_field =
    {
      crawlori_column_name = "transaction";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = None;
      crawlori_column_source = Operation;
    } in
  let block_field =
    {
      crawlori_column_name = "block";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let index_field =
    {
      crawlori_column_name = "index";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CInteger;
      crawlori_column_option = false;
      crawlori_column_index = None;
      crawlori_column_source = Operation;
    } in
  let level_field =
    {
      crawlori_column_name = "level";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CInteger;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let tsp_field =
    {
      crawlori_column_name = "tsp";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CTimestamp;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let main_field =
    {
      crawlori_column_name = "main";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CBoolean;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let columns =
    [
      address_field;
      kind_field;
      transaction_field;
      block_field;
      index_field;
      level_field;
      tsp_field;
      main_field;
    ]
    @ bm_ids in
  {
    crawlori_table_name = "contracts";
    crawlori_table_is_bm = false;
    crawlori_table_entrypoint_name = None;
    crawlori_table_fields = columns;
    crawlori_table_type = None;
  }

let all_operations_table () =
  let address_field =
    {
      crawlori_column_name = "contract";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let kind_field =
    {
      crawlori_column_name = "kind";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let transaction_field =
    {
      crawlori_column_name = "transaction";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = None;
      crawlori_column_source = Operation;
    } in
  let block_field =
    {
      crawlori_column_name = "block";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CVarchar;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let index_field =
    {
      crawlori_column_name = "index";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CInteger;
      crawlori_column_option = false;
      crawlori_column_index = None;
      crawlori_column_source = Operation;
    } in
  let level_field =
    {
      crawlori_column_name = "level";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CInteger;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let tsp_field =
    {
      crawlori_column_name = "tsp";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CTimestamp;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Operation;
    } in
  let main_field =
    {
      crawlori_column_name = "main";
      crawlori_column_bm_name = None;
      crawlori_column_dipdup_bm_name = None;
      crawlori_column_type = CBoolean;
      crawlori_column_option = false;
      crawlori_column_index = Some No_order;
      crawlori_column_source = Fun_parameter;
    } in
  let columns =
    [
      address_field;
      kind_field;
      transaction_field;
      block_field;
      index_field;
      level_field;
      tsp_field;
      main_field;
    ] in
  {
    crawlori_table_name = "all_operations";
    crawlori_table_is_bm = false;
    crawlori_table_entrypoint_name = None;
    crawlori_table_fields = columns;
    crawlori_table_type = None;
  }

let extract_tables ~storage_name ~contract_name ~interface ~entrypoints =
  let all =
    List.fold_left
      (extract_table_from_instruction ~storage_name ~contract_name)
      [] interface in
  let storage =
    List.find_opt
      (fun t -> t.crawlori_table_name = show_sanitized_name storage_name)
      all in
  match storage with
  | None -> failwith "crawlori: can't find storage type to build the table"
  | Some storage_table ->
    let bms =
      List.fold_left
        (extract_bm_tables_from_instruction ~storage_name ~contract_name)
        [] interface in
    let storage_table_without_bm =
      {
        storage_table with
        crawlori_table_name =
          table_name
          @@ sprintf "%s_%s"
               (show_sanitized_name contract_name)
               (show_sanitized_name storage_name);
        crawlori_table_fields = [michelson_column ~storage_name storage_name];
      } in
    let storage_table_without_bm =
      add_generic_blockchain_fields storage_table_without_bm in
    let bm_ids =
      List.filter
        (fun field ->
          match field.crawlori_column_bm_name with
          | None -> false
          | Some _ -> true)
        storage_table.crawlori_table_fields in
    ( storage_table_without_bm,
      List.map add_generic_blockchain_fields
      @@ List.map (fun t ->
             {
               t with
               crawlori_table_name =
                 table_name
                 @@ sprintf "%s_%s"
                      (show_sanitized_name contract_name)
                      t.crawlori_table_name;
             })
      @@ List.filter
           (fun t ->
             List.exists
               (fun ep ->
                 t.crawlori_table_name = show_sanitized_name ep
                 || t.crawlori_table_name = "_default")
               entrypoints)
           all,
      List.map add_generic_blockchain_fields bms,
      bm_ids )

let rec pp_crawlori_type ppf = function
  | CVarchar -> pp_print_string ppf "varchar"
  | CZarith -> pp_print_string ppf "zarith"
  | CInteger -> pp_print_string ppf "int"
  | CJson -> pp_print_string ppf "jsonb"
  | CTimestamp -> pp_print_string ppf "timestamp"
  | CBoolean -> pp_print_string ppf "boolean"
  | CBigint -> pp_print_string ppf "bigint"
  | CBytea -> pp_print_string ppf "bytea"
  | CArray typ -> fprintf ppf "[%a]" pp_crawlori_type typ

let pp_field ppf field =
  fprintf ppf "%s %a%s" field.crawlori_column_name pp_crawlori_type
    field.crawlori_column_type
    (if field.crawlori_column_option then
       ""
     else
       " not null")

let pp_fields ppf fields =
  pp_print_list ~pp_sep:(fun ppf _ -> fprintf ppf ", \\\n") pp_field ppf fields

let pp_create_table ppf table =
  fprintf ppf "create table %s(\\\n%a)" table.crawlori_table_name pp_fields
    table.crawlori_table_fields

let pp_index table_name ppf (field_name, index) =
  fprintf ppf "\"create index %s_%s_index on %s(%s%s)\"" table_name field_name
    table_name field_name
    (match index with
    | None -> ""
    | Some No_order -> ""
    | Some Asc -> " asc"
    | Some Desc -> " desc")

let pp_create_indexes ppf table =
  let indexes =
    List.map (fun f -> (f.crawlori_column_name, f.crawlori_column_index))
    @@ List.filter
         (fun f -> f.crawlori_column_index <> None)
         table.crawlori_table_fields in
  fprintf ppf "%a%s"
    (pp_print_list
       ~pp_sep:(fun ppf _ ->
         fprintf ppf " ;" ;
         pp_print_newline ppf ())
       (pp_index table.crawlori_table_name))
    indexes
    (if indexes <> [] then
       " ;"
     else
       "")

let pp_table ppf table =
  fprintf ppf "\"%a\" ;%a%a%a" pp_create_table table pp_print_newline ()
    pp_print_newline () pp_create_indexes table

let pp_drop_table ppf table =
  fprintf ppf "\"drop %a cascade\";" pp_print_string table.crawlori_table_name

let pp_tables ppf tables =
  pp_print_list
    ~pp_sep:(fun ppf _ ->
      pp_print_newline ppf () ;
      pp_print_newline ppf ())
    pp_table ppf tables

let pp_drop_tables ppf tables =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
    pp_drop_table ppf tables

let pp_zarith ppf () =
  let zarith_stuff =
    {|
  "create domain zarith as numeric";
  "create or replace function zadd(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 + z2; end; $$ language plpgsql" ;
  "create or replace function zsub(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 - z2; end; $$ language plpgsql" ;
  "create or replace function zmul(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 * z2; end; $$ language plpgsql" ;
  "create or replace function zdiv(z1 zarith, z2 zarith) returns zarith as $$ \
   begin return z1 / z2; end; $$ language plpgsql" ;
|}
  in
  pp_print_string ppf zarith_stuff

let process_tables ppf tables =
  fprintf ppf "let downgrade = [" ;
  pp_print_newline ppf () ;
  pp_drop_tables ppf tables ;
  pp_print_newline ppf () ;
  fprintf ppf "]" ;
  pp_print_newline ppf () ;
  pp_print_newline ppf () ;
  fprintf ppf "let upgrade = [" ;
  pp_tables ppf tables ;
  pp_print_newline ppf () ;
  fprintf ppf "]"

let pp_insert_key ppf field = pp_print_string ppf field.crawlori_column_name

let pp_insert_value_from_op ~subs ppf field =
  let name =
    match
      List.find_opt (fun (n, _sub) -> n = field.crawlori_column_name) subs
    with
    | None -> Format.sprintf "{op.%s}" field.crawlori_column_name
    | Some (_n, sub) -> sub in
  if field.crawlori_column_option then
    fprintf ppf "$?%a" pp_print_string name
  else
    fprintf ppf "$%a" pp_print_string name

let pp_insert_value_from_fun_param ~subs ppf field =
  let name =
    match
      List.find_opt (fun (n, _sub) -> n = field.crawlori_column_name) subs
    with
    | None -> field.crawlori_column_name
    | Some (_n, sub) -> sub in
  if field.crawlori_column_option then
    fprintf ppf "$?%a" pp_print_string name
  else
    fprintf ppf "$%a" pp_print_string name

let pp_insert_value_from_mic_param ~subs ppf field =
  let name =
    match
      List.find_opt (fun (n, _sub) -> n = field.crawlori_column_name) subs
    with
    | None -> "p"
    | Some (_n, sub) -> sub in
  if field.crawlori_column_option then
    fprintf ppf "$?%a" pp_print_string name
  else
    fprintf ppf "$%a" pp_print_string name

let pp_insert_value ~subs ppf field =
  match field.crawlori_column_source with
  | Operation -> pp_insert_value_from_op ~subs ppf field
  | Mic_parameter -> pp_insert_value_from_mic_param ~subs ppf field
  | Fun_parameter -> pp_insert_value_from_fun_param ~subs ppf field

let pp_insert ~pp ppf table =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_string ppf ",")
    pp ppf table.crawlori_table_fields

let pp_header_fun ppf table =
  fprintf ppf "let insert_%a ?(forward=false) ~dbh ~contract ~op parameter"
    pp_print_string table.crawlori_table_name

let default_subs =
  [
    ("main", "forward");
    ("transaction", "{op.bo_hash}");
    ("block", "{op.bo_block}");
    ("index", "{op.bo_index}");
    ("level", "{op.bo_level}");
    ("tsp", "{op.bo_tsp}");
  ]

let pp_insert_table ~storage_name ~contract_name ppf table =
  let subs = default_subs in
  if table.crawlori_table_is_bm then
    let table_without_bm_fields =
      {
        table with
        crawlori_table_fields =
          List.filter
            (fun f ->
              not
              @@ String.starts_with ~prefix:bm_key_prefix f.crawlori_column_name
              && not
                 @@ String.starts_with ~prefix:bm_value_prefix
                      f.crawlori_column_name)
            table.crawlori_table_fields;
      } in
    fprintf ppf
      "iter (fun (k, v) ->\n\
      \       let k = EzEncoding.construct micheline_enc.json k in\n\
      \       let v = EzEncoding.construct micheline_enc.json v in\n\
      \       [%%pgsql dbh \"insert into %a(%a) values (%a,$k,$v)\"]) parameter"
      pp_print_string table.crawlori_table_name
      (pp_insert ~pp:pp_insert_key)
      table
      (pp_insert ~pp:(pp_insert_value ~subs))
      table_without_bm_fields
  else if table.crawlori_table_name = "all_operations" then
    fprintf ppf
      "let kind = parameter in%a\n\
      \       [%%pgsql dbh \"insert into %a(%a) values (%a)\"]" pp_print_newline
      () pp_print_string table.crawlori_table_name
      (pp_insert ~pp:pp_insert_key)
      table
      (pp_insert ~pp:(pp_insert_value ~subs))
      table
  else if
    table.crawlori_table_name
    = table_name
      @@ sprintf "%s_%s"
           (show_sanitized_name contract_name)
           (show_sanitized_name storage_name)
  then
    fprintf ppf
      {|match parameter with
  | None -> rerr @@@@ `generic ("node_error", "no storage")
  | Some storage ->
    let p = EzEncoding.construct micheline_enc.json storage in
    [%%pgsql dbh "insert into %a(%a) values (%a)"]|}
      pp_print_string table.crawlori_table_name
      (pp_insert ~pp:pp_insert_key)
      table
      (pp_insert ~pp:(pp_insert_value ~subs))
      table
  else
    fprintf ppf
      "let p = EzEncoding.construct micheline_enc.json parameter in%a[%%pgsql \
       dbh \"insert into %a(%a) values (%a)\"]"
      pp_print_newline () pp_print_string table.crawlori_table_name
      (pp_insert ~pp:pp_insert_key)
      table
      (pp_insert ~pp:(pp_insert_value ~subs))
      table

let pp_body_fun ~storage_name ~contract_name ppf table =
  fprintf ppf "%a" (pp_insert_table ~storage_name ~contract_name) table

let pp_insert_fun ~storage_name ~contract_name ppf table =
  fprintf ppf "%a =%a%a" pp_header_fun table pp_print_newline ()
    (pp_body_fun ~storage_name ~contract_name)
    table

let pp_inserts ~storage_name ~contract_name ppf tables =
  let tables =
    List.filter
      (fun t ->
        t.crawlori_table_name <> "contracts" && t.crawlori_table_name <> "state")
      tables in
  pp_print_list
    ~pp_sep:(fun ppf _ ->
      pp_print_newline ppf () ;
      pp_print_newline ppf ())
    (pp_insert_fun ~storage_name ~contract_name)
    ppf tables

let pp_header storage_name ppf contract_name =
  let interface =
    String.capitalize_ascii @@ OCaml_SDK.interface_basename ~contract_name in
  fprintf ppf
    {|
open Pg
open Crp
open %s
open Proto
open Common
open Info

type txn = Pg.txn
type init_acc = Pg.init_acc
include E

let mic_type_storage_ftype =
  match Mtyped.parse_type %s.%s_micheline with
  | Ok ftype -> ftype
  | Error _ -> failwith "can't parse factory generated storage type"

let mic_type_storage_type = Mtyped.short mic_type_storage_ftype

let always = true
let name = %S
let forward_end _config _level = rok ()
|}
    interface interface
    (show_sanitized_name storage_name)
    (show_sanitized_name contract_name)

let pp_init ppf name =
  fprintf ppf
    {|
let init _config acc =
let l = [ 0, (%s_tables.upgrade, %s_tables.downgrade) ] in
rok (acc @@ l)%a
|}
    (String.capitalize_ascii (show_sanitized_name name))
    (String.capitalize_ascii (show_sanitized_name name))
    pp_print_newline ()

(* TODO set_main (with clean_up) *)
let pp_set_main ppf () =
  fprintf ppf
    {|
let set_main ?(forward=false) _info _dbh _m =
  if not forward then
    rok @@@@ fun () -> rok ()
  else rok @@@@ fun () -> rok ()
|}

let pp_bm_list ppf bms =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_string ppf ",")
    (fun ppf bm -> pp_print_string ppf bm.crawlori_column_name)
    ppf bms

let pp_get_bm_id ppf bm =
  let bm_name =
    match bm.crawlori_column_bm_name with
    | None -> failwith "no bm_name on bm fields"
    | Some s -> s in
  fprintf ppf
    {|let$ %s =
  match Bm_utils.find_opt_storage_field
    ~allocs ~fields ~storage_type ~storage_value %S with
| _, Some { bm_id ; _} -> Ok bm_id
| _ -> Error (`generic ("parse_error", "storage.%s")) in|}
    bm.crawlori_column_name bm_name bm_name

let pp_get_bms_id ppf bms =
  match bms with
  | [] ->
    fprintf ppf "ignore (allocs, fields, storage_type, storage_value); Ok()"
  | _ ->
    fprintf ppf "%a%aOk(%a)"
      (pp_print_list
         ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
         pp_get_bm_id)
      bms pp_print_newline () pp_bm_list bms

let pp_get_bm_updates ppf bm =
  fprintf ppf
    {|
let get_%s_updates bm_id l =
  Bm_utils.get_bm_updates
    ~bm_id
    l|}
    bm.crawlori_column_name

let pp_get_bms_updates ppf bms =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
    pp_get_bm_updates ppf bms

let pp_insert_op_bms_updates ~contract_name ppf bms =
  match bms with
  | [] -> fprintf ppf "ignore info;"
  | _ ->
    pp_print_list
      ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
      (fun ppf bm ->
        let insert_name =
          match bm.crawlori_column_bm_name with
          | None -> failwith "crawlori: column is not of bigmap type"
          | Some s ->
            table_name @@ sprintf "%s_%s" (bm_table_prefix ~contract_name) s
        in
        fprintf ppf
          "let %s_updates = get_%s_updates (Option.get info.ci_%s) \
           meta.op_lazy_storage_diff in%alet>? () = insert_%s ~dbh ?forward \
           ~op ~contract %s_updates in"
          bm.crawlori_column_name bm.crawlori_column_name
          bm.crawlori_column_name pp_print_newline () insert_name
          bm.crawlori_column_name)
      ppf bms

let pp_insert_ori_bms_updates ~contract_name ppf bms =
  pp_print_list
    ~pp_sep:(fun ppf _ -> pp_print_newline ppf ())
    (fun ppf bm ->
      let insert_name =
        match bm.crawlori_column_bm_name with
        | None -> failwith "crawlori: column is not of bigmap type"
        | Some s ->
          table_name @@ sprintf "%s_%s" (bm_table_prefix ~contract_name) s in
      fprintf ppf
        "let %s_updates = get_%s_updates %s meta.op_lazy_storage_diff \
         in%alet>? () = insert_%s ~dbh ~forward ~op ~contract:address \
         %s_updates in"
        bm.crawlori_column_name bm.crawlori_column_name bm.crawlori_column_name
        pp_print_newline () insert_name bm.crawlori_column_name)
    ppf bms

let pp_register_block ~storage_name ~contract_name ~bms ppf () =
  fprintf ppf
    {|
let get_storage_bms_id ?(allocs=[]) ~fields ~storage_type storage_value =
    %a

let get_storage_ori ~allocs script =
  try
    let storage = %s_decode script.storage in
    let$ fields = Bm_utils.get_storage_fields script in
    let storage_type = mic_type_storage_ftype in
    let$ storage_value =
      Mtyped.(parse_value mic_type_storage_type script.storage) in
  let$ (%a) =
    get_storage_bms_id ~allocs ~fields ~storage_type storage_value in
  Ok (storage%s%a)
  with exn ->
    let s = Printf.sprintf "decode_storage: %%s" @@@@ Printexc.to_string exn in
    Error (`generic ("parse_error", s))

%a

let insert_origination ?dbh ?(forward = false) config op ori =
  let address = Tzfunc.Crypto.op_to_KT1 op.bo_hash in
  if op.bo_op.source = config.originator_address then
    begin
    match op.bo_meta with
      | None -> rerr @@@@ `generic ("node_error", "no metadata")
      | Some meta ->
        let allocs = Bm_utils.big_map_allocs meta.op_lazy_storage_diff in
        match get_storage_ori ~allocs ori.script with
        | Error e ->
          Format.eprintf "Error %%s@@." @@@@ Tzfunc.Rp.string_of_error e ;
          rok ()
        | Ok (s%s%a) ->
          Format.printf "\027[0;93morigination %%s\027[0m@@." (short address) ;
          use dbh @@@@ fun dbh ->
          let storage = Some (%s_encode s) in
          let>? () = insert_%s ~dbh ~forward ~contract:address ~op storage in
          let>? () =
            Contracts.insert_all_operations
              ~forward ~dbh ~op ~contract:address "origination" in
          %a
          Contracts.%s_register ~forward ~dbh config address %S op %a
    end
  else rok ()

let register_block ?forward config dbh b =
  let>? _ =
    fold (fun index op ->
        fold (fun index c ->
            match c.man_metadata with
            | None -> Lwt.return_ok index
            | Some meta ->
              if meta.man_operation_result.op_status = `applied then
                let>? index = match c.man_info.kind with
                  | Origination ori ->
                    let op = {
                      bo_block = b.hash; bo_level = b.header.shell.level;
                      bo_tsp = b.header.shell.timestamp; bo_hash = op.op_hash;
                      bo_op = c.man_info; bo_index = index; bo_indexes = (0l, 0l, 0l);
                      bo_meta = Option.map (fun m -> m.man_operation_result) c.man_metadata;
                      bo_numbers = Some c.man_numbers; bo_nonce = None;
                      bo_counter = c.man_numbers.counter } in
                    let|>? () = insert_origination ?forward config ~dbh op ori in
                    Int32.succ index
                  | _ -> Lwt.return_ok index in
                fold (fun index iop ->
                    if iop.in_result.op_status = `applied then
                      match iop.in_content.kind with
                      | Origination ori ->
                        let bo_meta = Some {
                            iop.in_result with
                            op_lazy_storage_diff =
                              iop.in_result.op_lazy_storage_diff @@
                              (Option.fold ~none:[] ~some:(fun m ->
                                   m.man_operation_result.op_lazy_storage_diff)
                                  c.man_metadata) } in
                        let op = {
                          bo_block = b.hash; bo_level = b.header.shell.level;
                          bo_tsp = b.header.shell.timestamp; bo_hash = op.op_hash;
                          bo_op = iop.in_content; bo_index = index; bo_indexes = (0l, 0l, 0l); bo_meta;
                          bo_numbers = Some c.man_numbers; bo_nonce = Some iop.in_nonce;
                          bo_counter = c.man_numbers.counter } in
                        let|>? () = insert_origination ?forward config ~dbh op ori in
                        Int32.succ index
                      | _ -> Lwt.return_ok index
                    else
                      Lwt.return_ok index
                  ) index meta.man_internal_operation_results
              else Lwt.return_ok index
          ) index op.op_contents
      ) 0l b.operations in
  [%%pgsql dbh
      "insert into state (level, tsp, chain_id) \
       values (${b.header.shell.level}, ${b.header.shell.timestamp}, \
       ${b.chain_id})"]
|}
    pp_get_bms_id bms
    (show_sanitized_name storage_name)
    pp_bm_list bms
    (match bms with
    | [] -> ""
    | _ -> ",")
    pp_bm_list bms pp_get_bms_updates bms
    (match bms with
    | [] -> ""
    | _ -> ",")
    pp_bm_list bms
    (show_sanitized_name storage_name)
    (table_name
    @@ sprintf "%s_%s"
         (show_sanitized_name contract_name)
         (show_sanitized_name storage_name))
    (pp_insert_ori_bms_updates ~contract_name)
    bms
    (show_sanitized_name contract_name)
    (show_sanitized_name contract_name)
    (pp_print_list
       ~pp_sep:(fun ppf _ -> pp_print_string ppf " ")
       (fun ppf bm -> pp_print_string ppf bm.crawlori_column_name))
    bms

let pp_filter ppf ep =
  match ep.crawlori_table_entrypoint_name with
  | Some epname ->
    fprintf ppf
      {|| EPnamed %S ->
      let>? () = Contracts.insert_all_operations ?forward ~dbh ~op ~contract %S in
      insert_%s ?forward ~dbh ~contract ~op p.value|}
      epname ep.crawlori_table_name ep.crawlori_table_name
  | None -> ()

let pp_filter_epname ~storage_name ~contract_name ppf tables =
  let ep =
    List.filter
      (fun t ->
        (not @@ t.crawlori_table_is_bm)
        && (not (t.crawlori_table_name = "state"))
        && (not (t.crawlori_table_name = "contracts"))
        && (not
              (t.crawlori_table_name
              = table_name
                @@ sprintf "%s_%s"
                     (show_sanitized_name contract_name)
                     (show_sanitized_name storage_name)))
        && not (t.crawlori_table_name = "all_operations"))
      tables in
  pp_print_list ~pp_sep:(fun ppf _ -> pp_print_newline ppf ()) pp_filter ppf ep

let pp_register_operation ~storage_name ~contract_name ~bms ppf tables =
  fprintf ppf
    {|
  let insert_operation ?forward dbh ~op ~contract parameter =
  match parameter with
  | None -> rok ()
  | Some p ->
    match p.entrypoint with
    %a
    | _ ->
      let str =
        Format.sprintf "unexpected entrypoint %%S" @@@@
        EzEncoding.construct entrypoint_enc.json p.entrypoint in
      rerr @@@@ `generic ("operation_error", str)

  let register_operation ?forward info dbh op =
  match op.bo_op.kind, op.bo_meta with
  | _, None -> rerr @@@@ `generic ("node_error", "no metadata")
  | Transaction {destination=contract; parameters ; _}, Some meta ->
    begin
      match List.find_opt (fun c -> c.ci_address = contract && c.ci_kind = %S) @@@@ CSet.elements info.contracts with
    | None -> rok ()
    | Some info ->
      Format.printf "\027[0;35m[%%s] transaction %%s %%s\027[0m@@."
        name
        (String.sub op.bo_hash 0 10)
        (Option.fold ~none:"" ~some:(fun p ->
             EzEncoding.construct entrypoint_enc.json p.entrypoint) parameters) ;
      let>? () = insert_operation ?forward dbh ~op ~contract parameters in
      %a
      insert_%s ~dbh ?forward ~contract ~op meta.op_storage
    end
  | _, _ ->
    rok ()

|}
    (pp_filter_epname ~storage_name ~contract_name)
    tables
    (show_sanitized_name contract_name)
    (pp_insert_op_bms_updates ~contract_name)
    bms
    (table_name
    @@ sprintf "%s_%s"
         (show_sanitized_name contract_name)
         (show_sanitized_name storage_name))

let process_plugin ~storage_name ~contract_name ~bms ppf tables =
  fprintf ppf "%a%a%a%a%a%a%a%a%a%a%a" (pp_header storage_name) contract_name
    pp_print_newline ()
    (pp_inserts ~storage_name ~contract_name)
    tables pp_print_newline ()
    (pp_register_block ~storage_name ~contract_name ~bms)
    () pp_print_newline ()
    (pp_register_operation ~storage_name ~contract_name ~bms)
    tables pp_print_newline () pp_set_main () pp_print_newline () pp_init
    contract_name
