(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2023 Functori --- <contact@functori.com>                    *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** This OCaml file contains Python code for interacting with the Tezos
blockchain using the PyTezos library. It defines functions for deploying and
interacting with smart contracts on the blockchain, as well as utility functions
for working with Tezos-specific hash functions and address formats. It also
includes an example of using the `deployContract` function to deploy a smart
contract on the Tezos mainnet. Warning: this short summary was automatically
generated and could be incomplete or misleading. If you feel this is the case,
please open an issue. *)

let blockchain_py =
  {|
from typing import TypedDict, TypeVar, Callable
from pytezos import pytezos
from pytezos import ContractInterface
from pytezos import micheline_to_michelson
from pytezos.operation.group import OperationGroup
from pytezos.michelson.types.base import MichelsonType
from pytezos.michelson.types.big_map import BigMapType
import json
import hashlib
import base58

class identity(TypedDict):
    sk : str
    pk : str
    pkh : str


NODE_URLS = {
    'sandbox_node': 'http://127.0.0.1:18731',
    'flextesa_node': 'http://localhost:20000',
    'ithaca_node': 'https://ithacanet.tezos.marigold.dev',
    'ithacanet_node': 'https://ithacanet.tezos.marigold.dev',
    'ghostnet_node': 'https://ghostnet.tezos.marigold.dev',
    'jakarta_node': 'https://jakartanet.ecadinfra.com',
    'lima_node': 'https://limanet.ecadinfra.com',
    'jakartanet_node': 'https://jakartanet.ecadinfra.com',
    'mainnet_node': 'https://tz.functori.com',
    'default_node': 'http://localhost:20000',
}

def get_node(network):
    return NODE_URLS.get(network + "_node", NODE_URLS['default_node'])


alice : identity = { 'sk' : "edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq", 'pk' : 'edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn', 'pkh' : 'tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb' }
bob : identity = { 'sk' : "edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt", 'pk' : 'edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4', 'pkh' : 'tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6' }

def originated_contract_hash(opg_hash, index):
    opg_hash = base58.b58decode_check(opg_hash)
    opg_hash = opg_hash[2:]
    index_bytes = index.to_bytes(4,'big')
    msg = opg_hash + index_bytes
    digest = hashlib.blake2b(msg, digest_size=20).digest()
    prefix = prefix = bytearray([2, 90, 121])
    return base58.b58encode_check(prefix + digest).decode('utf-8')

def compute_kt1(opg_hash) -> str:
    return originated_contract_hash(opg_hash,0)

def callEntrypoint(_from: identity, kt1: str, entrypoint: str, param, amount, network, debug=False) -> str:
    tezos = pytezos.using(shell=get_node(network), key=_from['sk'])
    if(debug):
        print("param" + str(param))
    op = tezos.transaction(source=_from['sk'], destination=kt1,parameters={"entrypoint": entrypoint, "value": param},amount=amount)
    opg = tezos.bulk(
        op).fill().sign().inject(_async=False)
    try:
        op_hash: str = opg['hash']
    except Exception as e:
        raise ValueError("Operation could not go through")
    return op_hash

def deployContract(codeFileName: str, _from: identity, initial_storage, amount, network, debug=False) -> str | None:
    with open(codeFileName, "r") as file:
        micheline_code = json.load(file)
        code = micheline_to_michelson(micheline_code)
    contract: ContractInterface = ContractInterface.from_michelson(code)
    if(debug):
        print("network is:" + network)
        print("node is:" + get_node(network))
    tezos = pytezos.using(shell=get_node(network), key=_from['sk'])
    context = tezos.context
    opg = OperationGroup(context).origination(script ={
            'code': contract.code,
            'storage': initial_storage
        })
    opg = tezos.bulk(
        opg).fill().sign().inject(_async=False)
    op_hash = opg['hash']
    kt1 = compute_kt1(op_hash)
    return kt1

KT = TypeVar('KT')
VT = TypeVar('VT')

def find_big_map(
    key: KT,
    big_map: BigMapType,
    encode: Callable[[KT], MichelsonType],
    decode: Callable[[MichelsonType], VT],
    network,
) -> MichelsonType | None:
    tezos = pytezos.using(shell=get_node(network))
    big_map.attach_context(tezos.context)
    encoded_key = encode(key)
    optional_value = big_map.get(encoded_key)
    if optional_value is None:
        return None
    else:
        return decode(optional_value)

def storage_download_decode(kt1,decode,network):
    c = pytezos.using(shell=get_node(network)).contract(kt1).storage.data
    return decode(c)
|}
